/*=================================================================
 *
 * Modified from
 * mexMinSumWithAppearance.cpp
 * by Huayan Wang <huayanw@cs.stanford.edu>
 * and Andrew Chou <achou@cs.stanford.edu>
 *
 *=================================================================*/
#ifndef min_sum_with_appearance
#define min_sum_with_appearance

#include <math.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "svm_struct_api_types.h"
#include <pthread.h>

#if !defined(MAX)
#define MAX(A, B) ((A) > (B) ? (A) : (B))
#endif

#if !defined(MIN)
#define MIN(A, B) ((A) < (B) ? (A) : (B))
#endif


#define NUM_COLOR_BUCKETS 16
#define TOTAL_COLOR_BUCKETS (NUM_COLOR_BUCKETS*NUM_COLOR_BUCKETS*NUM_COLOR_BUCKETS)
#define NUM_VALS_PER_BUCKET (256/NUM_COLOR_BUCKETS)

struct buckets{
  int r,g,b;
};

typedef struct appmodel{
  int m[NUM_COLOR_BUCKETS][NUM_COLOR_BUCKETS][NUM_COLOR_BUCKETS];
  // the 3 color channels
  int c[3][NUM_COLOR_BUCKETS];
  // the 17 texture channels
  int t[NTEXTURES][NUM_COLOR_BUCKETS];
} Appmodel;
typedef struct retenergy{
  double e1,e2,e3,e4,e5;
} RetEnergy;

double minSumWithAppearance(PATTERN *x, LOSS *loss, STRUCT_LEARN_PARM *param,
    CONFIGS *config, int *hids, FEATURES *features, double *nlogn);
double calculateEnergy(PATTERN *x, int *hids, STRUCT_LEARN_PARM *param,
    CONFIGS *config, FEATURES *features, double *nlogn,
    RetEnergy *retenergy = NULL,
    struct buckets *allbuckets = NULL, int numnlogn = -1,
    bool print = false);
#endif
