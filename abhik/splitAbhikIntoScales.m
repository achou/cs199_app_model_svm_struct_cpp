colors = 'ymcrgbwk';
load('PARSEandrew_unary_boxes_55566665555555566665555555.mat');
I = [1:100,201:405];
ramananpath = '../../cs199_appearance_model/Ramanan_image/';
for i=I
  i
  if i<=100
    img = imread(sprintf([ramananpath, 'im0%.3d.png'], i));
  else
    img = imread(sprintf([ramananpath, 'im0%.3d.png'], i-100));
  end
  npix = size(img,1)*size(img,2);
  for s=1:10 %scale
    FG = [];
    im = img;
    for p=1:26 %part
      %continue
      fgpart = [];
      for h=1:50
        box = boxes{i}(p,1:4,h,s);
        center = (box(3:4) + box(1:2))/2;
        box(3:4) = box(3:4) - box(1:2);
        SHRINK = 1.2 * 1.0746^s;
        a = box(4)/(2*SHRINK);
        b = box(3)/(2*SHRINK);
        assert(a==b);
        x = repmat(1:size(im,2),    size(im,1),1);
        y = repmat([1:size(im,1)]', 1, size(im,2));
        x = reshape(x, npix, 1);
        y = reshape(y, npix, 1);
        x = x-center(1);
        y = y-center(2);
        z = x.^2+y.^2;
        fg = find(z<=a*a);
        fgpart = [fgpart; [p*ones(length(fg),1), h*ones(length(fg),1), fg]];
        continue;
        channel = 1+mod(p,3);
        im(:,:,channel) = ...
          ellipseMatrix( center(2), center(1), a, b, 0, im(:,:,channel), 255);
        keyboard
      end
      FG = [FG; fgpart];
    end
    %{
    imshow(im)
    for p=1:26 %part
      box = boxes{i}(p,1:4,h,s);
      box(3:4) = box(3:4) - box(1:2);
      rectangle('position', box, 'EdgeColor', colors(1+mod(p,length(colors))));
    end
    pause(1)
    continue
    tosave = boxes{i}(:,:,:,s);
    if i<=100
      save(sprintf('scale%d/unary%d_scale%d.mat',s,i,s), 'tosave');
    else
      save(sprintf('scale%d/unary%d_scale%d.mat',s,i-100,s), 'tosave');
    end
    %}
  end
end
