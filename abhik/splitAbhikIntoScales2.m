SAVEPAIR = 0;
outdir = '../../abhik/'
if SAVEPAIR
  load([outdir, 'PARSEandrew_pairwise_scores_55566665555555566665555555.mat']);
end
load([outdir, 'PARSEandrew_unary_boxes_55566665555555566665555555.mat']);
colors = 'ymcrgbwk';
Imgs = [1:100,201:405];
ramananpath = '../../../scr/cs199_appearance_model/Ramanan_image/';
I = [1   1   2   2   3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20];
J = [3   15  10  22  10 12 22 24 12 14 24 26 3  5  15 17 5  7  17 19 1  2];
S = [1/2 1/2 1/2 1/2 1  1  1  1  1  1  1  1  1  1  1  1  1  1  1  1  1  1];
A26to20 = full(sparse(I,J,S,20,26));
% Tree structure for 26 parts: pa(i) is the parent of part i
% This structure is implicity assumed during data preparation
% (PARSE_data.m) and evaluation (PARSE_eval_pcp)
PA = [0 1 2 3 4 5 6 3 8 9 10 11 12 13 2 15 16 17 18 15 20 21 22 23 24 25];
% train and test GT stickmen
globals;
[train test] = PARSE_data();
% Create ground truth keypoints for model training                                     
% We augment the original 14 joint positions with midpoints of joints,                 
% defining a total of 26 keypoints                                                     
I = [1  2  3  4   4   5  6   6   7  8   8   9   9   10 11  11  12 13  13  14 ...       
    15 16  16  17 18  18  19 20  20  21  21  22 23  23  24 25  25  26];         
J = [14 13 9  9   8   8  8   7   7  9   3   9   3   3  3   2   2  2   1   1 ...        
    10 10  11  11 11  12  12 10  4   10  4   4  4   5   5  5   6   6];          
S = [1  1  1  1/2 1/2 1  1/2 1/2 1  2/3 1/3 1/3 2/3 1  1/2 1/2 1  1/2 1/2 1 ...        
    1  1/2 1/2 1  1/2 1/2 1  2/3 1/3 1/3 2/3 1  1/2 1/2 1  1/2 1/2 1];          
A14to26 = full(sparse(I,J,S,26,14));
for i=1:length(test)
  test(i).point = A14to26*test(i).point;
end
gt = [train(1:100), test];

for i=Imgs
  i
  if i<=100
    iadj = i;
  else
    iadj = i-100;
  end
  img = imread(sprintf([ramananpath, 'im0%.3d.png'], iadj));
  nneigh = zeros(26,1);
  totaldist = zeros(26,1);
  for p=2:26
    pa = PA(p);
    rad = norm(gt(iadj).point(p,:) - gt(iadj).point(pa,:));
    %TODO get a better estimate
    sidelen = sqrt(pi*rad^2); % a square with the same area as the circle
    nneigh(p) = nneigh(p) + 1;
    nneigh(pa) = nneigh(pa) + 1;
    totaldist(p) = totaldist(p) + sidelen;
    totaldist(pa) = totaldist(pa) + sidelen;
  end
  totaldist = totaldist./nneigh;
  gtstick = zeros(4,26);
  for p=1:26
    dxy = [0, totaldist(p)/2];
    tmp2 = [gt(iadj).point(p,:)-dxy, gt(iadj).point(p,:)+dxy];
    gtstick(:,p) = tmp2;
  end
  npix = size(img,1)*size(img,2);
  for s=1:10 %scale
    FG = []; 
    im = img;
    cor = zeros(26,50);
    for p=1:26 %part
      %continue
      fgpart = [];
      for h=1:50
        box = boxes{i}(p,1:4,h,s);
        center = (box(3:4) + box(1:2))/2;
        box(3:4) = box(3:4) - box(1:2);
        SHRINK = 1.2 * 1.0746^s;
        a = box(4)/(2*SHRINK);
        b = box(3)/(2*SHRINK);
        assert(abs(a-b)<1e-7);
        x = repmat(1:size(im,2),    size(im,1),1);
        y = repmat([1:size(im,1)]', 1, size(im,2));
        x = reshape(x, npix, 1);
        y = reshape(y, npix, 1);
        x = x-center(1);
        y = y-center(2);
        z = x.^2+y.^2;
        fg = find(z<=a*a);
        fgpart = [fgpart; [p*ones(length(fg),1), h*ones(length(fg),1), fg]];
        % do gt comparison
        dxy = [0, a];
        pstick = [center-dxy, center+dxy]';
        cor(p,h) = overlap( gtstick(:,p), pstick);

        %
        continue;
        channel = 1+mod(p,3);
        im(:,:,channel) = ...
        ellipseMatrix( center(2), center(1), a, b, 0, im(:,:,channel), 255);
        keyboard
      end
      FG = [FG; fgpart];
    end
    %keyboard
    boxesforscale = boxes{i}(:,:,:,s);
    index = 1+(s-1)*50;
    indices = index:index+49;
    if SAVEPAIR
      pairwise_for_scale = pairwise_score_matrices{i}(:,indices,indices);
    end
    %keyboard
    %save(sprintf('scale%d/unary%d_scale%d.mat',s,iadj,s), 'boxesforscale');
    if SAVEPAIR
      save([outdir, sprintf('scale%d/pairwise%d_scale%d.mat',s,iadj,s)], 'pairwise_for_scale');
    end
    save([outdir, sprintf('scale%d/fg%d_scale%d.mat',s,iadj,s)], 'FG');
    save([outdir, sprintf('scale%d/gt%d_scale%d.mat',s,iadj,s)], 'cor');
  end
end
