function [] = results()
baseline = 1.0 - .5620;
name = ['weights10em/', 'results.txt'];
em = loadResults(name);
name = ['weights10abcd1/', 'results.txt'];
v1 = loadResults(name);
name = ['weights10abcd2/', 'results.txt'];
v2 = loadResults(name);
name = ['weights10abcd3/', 'results.txt'];
v3 = loadResults(name);
name = ['weights10abcd4/', 'results.txt'];
v4 = loadResults(name);
name = ['weights10abcd5/', 'results.txt'];
v5 = loadResults(name);

ph=plot(...
  em(:,1), 100*baseline*ones(3,1),...
  em(:,1), 100*em(:,2),...
  v1(:,1), 100*v1(:,2),...
  v2(:,1), 100*v2(:,2),...
  v3(:,1), 100*v3(:,2),...
  v4(:,1), 100*v4(:,2),...
  v5(:,1), 100*v5(:,2)...
  )
set(ph, 'LineWidth', 3)
xlabel('log(c)', 'FontWeight','bold');
ylabel('Percent Error', 'FontWeight','bold');
legend('Andriluka','Alternating','Block size 1','Block size 2','Block size 3','Block size 4','Block size 5','Location','NorthEast')
title('Performance using Learned Weights (10 hyps)', 'FontWeight','bold');

% 20 hyps
basedir = 'weights20abcdoverlap/weights20abcdoverlap';
resultsdir = '/results.txt';
name = [basedir, 'EM', resultsdir]
em = loadResults(name)
name = [basedir, '1', resultsdir];
v1 = loadResults(name)
name = [basedir, '2', resultsdir];
v2 = loadResults(name)
name = [basedir, '3', resultsdir];
v3 = loadResults(name)
name = [basedir, '4', resultsdir];
v4 = loadResults(name)
name = [basedir, '5', resultsdir];
v5 = loadResults(name)

figure
ph=plot(...
  em(:,1), 100*baseline*ones(3,1),...
  em(:,1), 100*em(:,2),...
  v1(:,1), 100*v1(:,2),...
  v2(:,1), 100*v2(:,2),...
  v3(:,1), 100*v3(:,2),...
  v4(:,1), 100*v4(:,2),...
  v5(:,1), 100*v5(:,2)...
  )
set(ph, 'LineWidth', 3)
xlabel('log(c)', 'FontWeight','bold');
ylabel('Percent Error', 'FontWeight','bold');
legend('Andriluka','Alternating','Block size 1','Block size 2','Block size 3','Block size 4','Block size 5','Location','NorthEast')
title('Performance using Learned Weights including Overlap (20 hyps)', 'FontWeight','bold');

% 20 hyps
%name = ['weights20em/', 'results.txt'];
%em = loadResults(name);
name = ['weights20abcd1/', 'results.txt'];
v1 = loadResults(name);
name = ['weights20abcd2/', 'results.txt'];
v2 = loadResults(name);
name = ['weights20abcd3/', 'results.txt'];
v3 = loadResults(name);
name = ['weights20abcd4/', 'results.txt'];
v4 = loadResults(name);
name = ['weights20abcd5/', 'results.txt'];
v5 = loadResults(name);

figure
ph=plot(...
  em(:,1), 100*baseline*ones(3,1),...
  v1(:,1), 100*v1(:,2),...
  v2(:,1), 100*v2(:,2),...
  v3(:,1), 100*v3(:,2),...
  v4(:,1), 100*v4(:,2),...
  v5(:,1), 100*v5(:,2)...
  )
set(ph, 'LineWidth', 3)
xlabel('log(c)', 'FontWeight','bold');
ylabel('Percent Error', 'FontWeight','bold');
legend('Andriluka','Block size 1','Block size 2','Block size 3','Block size 4','Block size 5','Location','NorthEast')
title('Performance using Learned Weights (20 hyps)', 'FontWeight','bold');

v1=zeros(2,2);
v2=zeros(52,2);
v3=zeros(52,2);
v4=zeros(52,2);
v5=zeros(52,2);
IMGNUMS = 101:305;
for i=101:305
  name = ['testlogs10_1/', 'log_', int2str(i), '.txt'];
  v1 = v1 + load(name);
  name = ['testlogs10_2/', 'log_', int2str(i), '.txt'];
  v2 = v2 + load(name);
  name = ['testlogs10_3/', 'log_', int2str(i), '.txt'];
  v3 = v3 + load(name);
  name = ['testlogs10_4/', 'log_', int2str(i), '.txt'];
  v4 = v4 + load(name);
  name = ['testlogs10_5/', 'log_', int2str(i), '.txt'];
  v5 = v5 + load(name);
end
figure
len = length(v2);
x=1:len;
v1 = [v1; ones(len-length(v1),1)*v1(end,:)];
FACTOR = 1e6*length(IMGNUMS);
ph=plot(...
  x, v1(:,2)/FACTOR,...
  x, v2(:,2)/FACTOR,...
  x, v3(:,2)/FACTOR,...
  x, v4(:,2)/FACTOR,...
  x, v5(:,2)/FACTOR...
  );
set(ph, 'LineWidth', 3)
legend('Block size 1','Block size 2','Block size 3','Block size 4','Block size 5','Location','NorthWest')
xlabel('Number of Iterations', 'FontWeight','bold');
ylabel('Seconds per Image', 'FontWeight','bold');
title('Runtime', 'FontWeight','bold');
end
function [test, train] = loadResults(name)
fid = fopen(name,'rt');
C = textscan(fid,'%s %f %s %s %f','Delimiter',' ','CollectOutput',1);
fclose(fid);
test  = [C{2}(1:2:end), C{4}(1:2:end)];
train = [C{2}(2:2:end), C{4}(2:2:end)];
test(:,1) = log10(test(:,1)/10000);
train(:,1) = log10(train(:,1)/10000);
end
