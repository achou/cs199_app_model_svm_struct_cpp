#ifndef FEATURES_H
#define FEATURES_H
#include "svm_struct_api_types.h"
#include "svm_struct/svm_struct_common.h"
using namespace std;
extern int *Overlap_edges;

void initOverlapEdges();
void initOverlapEdgesRamanan();
void freeOverlapEdges();
void extraFeatures( EXAMPLE *example, int s);
void freeFeatures( EXAMPLE *example, int s);
#endif
