/*=================================================================
 * minSumWithAppearance.cpp
 * by Huayan Wang <huayanw@cs.stanford.edu>
 * and Andrew Chou <achou@cs.stanford.edu>
 *=================================================================*/
#include "common.h"
#include "computeU.h"
#include "features.h"
#include "featureBank.h"
#include "minSumWithAppearance.h"
#include "svm_struct_api.h"
#include "inference/graphs.h"
#include <sys/time.h>
#include "limits.h"
#include <algorithm>
#include <sstream>

const bool ENFORE_INCREASING = false;

double cumuapp = 0.0;
int cumufg = 0;
int cumubg = 0;
double cumupair = 0.0;
const bool APPD = 0;
const int DEBUGFG= 0;
const int DEBUG = 0;
const int DEBUG5 = 0;
const int DEBUG7 = 0; // for debugging inference
const int DEBUG11 = 0;
const int VALIDATE_INFERENCE = 0;
// Constants that are used to make the config weights close to 1.0
const double DEF_SINGLETON_WEIGHT = 1.0;
const double DEF_PAIRWISE_WEIGHT = 1.0;
const double DEF_APPEARANCE_WEIGHT = 1.0;
const double DEF_SYMMETRIC_WEIGHT = 0.0;
const double DEF_OVERLAP_WEIGHT = 1.0;
// A few globals that dont change after they are set at the beginning
int IMGSIZE;
// Config params
int MODEL_BLUR_INV;
int APP_MODEL_PRIOR;
int USE_SYMMETRY;
int APPROX_INFERENCE;
int INFERENCE_ITERS;
int MODEL_BLUR;
int HILLCLIMB;
int PLOT_INFERENCE;
// weights
double SINGLETON_WEIGHTS[NPARTS];
double PAIRWISE_WEIGHTS[NJOINTS];
double APPEARANCE_WEIGHTS[NAPP];
double SYMMETRIC_WEIGHTS[NSYMS];
double OVERLAP_WEIGHTS[NOVERLAP];
// means
double SINGLETON_MEANS[NPARTS];
double PAIRWISE_MEANS[NJOINTS];
double APPEARANCE_MEANS[NAPP];
double SYMMETRIC_MEANS[NSYMS];
double OVERLAP_MEANS[NOVERLAP];
// std dev inverses
double SINGLETON_SDEVINV[NPARTS];
double PAIRWISE_SDEVINV[NJOINTS];
double APPEARANCE_SDEVINV[NAPP];
double SYMMETRIC_SDEVINV[NSYMS];
double OVERLAP_SDEVINV[NOVERLAP];

curIsBetterFactorCallback isBetter;
double WORST;
int INFERENCE_METHOD = -1;
int ABCD_BLOCK_SIZE = -1;
// # of times the outer loop is run. A new block is selected for each iteration.
int ABCD_ITERS = -1;
// Max # of seconds the inference is run for.
int ABCD_INF_TIME = -1;
// If we want to include the parts in the block that arent part of the edge.
// This assumes that many hyps overlap, or not many parts are changing.
int INC_BLOCK;
int NAIVE_INIT;// initially use hids=1 rather than EM init.
const int NTHREADS = 1;
const int ADD_APP_MODEL_MASK = 0x1;
const int SUBTRACT_APP_MODEL_MASK = 0x2;

void allocAppmodels(Appmodel *models[NPARTS]){
  for(int i=0;i<NPARTS;i++){
    models[i] = (Appmodel *)malloc(NHYPS*sizeof(Appmodel));
    if(!models[i]){
      Error("Could not allocate heap space in allocAppmodels.");
    }
  }
}
void freeAppmodels(Appmodel models[NPARTS][NHYPS]){
  for(int i=0;i<NPARTS;i++) free(models[i]);
}
// special case of setModel for perf reasons.
// TODO see how much this actually speeds things up
void initModel(Appmodel *m){
  for(int i=0; i<NTEXTURES; i++)
    for(int j=0; j<NUM_COLOR_BUCKETS; j++)
      m->t[i][j] = APP_MODEL_PRIOR;
  for(int i=0; i<NUM_COLOR_BUCKETS; i++){
    for(int j=0; j<NUM_COLOR_BUCKETS; j++){
      for(int k=0; k<NUM_COLOR_BUCKETS; k++){
        m->m[i][j][k] = APP_MODEL_PRIOR;
      }
    }
  }
  // dont init m->l, m->a, and m->b. that will be done later
}
// calledFrom is just for debugging
void discretizeAll(struct buckets *b, unsigned char *img){
  assert(IMGSIZE>0);
  for(int pixel=0; pixel<IMGSIZE; pixel++){
    b[pixel].r = ((unsigned int)img[3*pixel+0])/NUM_VALS_PER_BUCKET;
    //printf("%d %u\n", img[3*pixel+0], b[pixel].r);
    b[pixel].g = ((unsigned int)img[3*pixel+1])/NUM_VALS_PER_BUCKET;
    b[pixel].b = ((unsigned int)img[3*pixel+2])/NUM_VALS_PER_BUCKET;
    if(b[pixel].r<0 || b[pixel].g<0 || b[pixel].b<0 ||
        b[pixel].r>=NUM_COLOR_BUCKETS || b[pixel].g>=NUM_COLOR_BUCKETS
        || b[pixel].b>=NUM_COLOR_BUCKETS || pixel<0 || pixel>=IMGSIZE){
      printf("pixel: %d, imgsize: %d\n", pixel, IMGSIZE);
      printf("r %d, g %d, b %d, num buckets %d\n", b->r,b->g,b->b,NUM_COLOR_BUCKETS);
      Error("problem in discretize");
    }
  }
}

double inline normalize(double p){
  if(p>1.0 || p<0.0){
    printf("p: %lf\n", p);
    Error("p is not in the range [0,1].");
  }
  if(p == 1.0) return 1.0-FLT_EPSILON;
  if(p == 0.0) return FLT_EPSILON;
  return p;
}
// pl = log2( p/(1-p) )
double inline divergencePrecomp(double p, double pl, double q){
  // could use log2f
  //double l = log2((p-pq) / (q-pq));
  double div = (p-q)*( pl + log2( 1.0/q - 1.0) );
  //These wont match since one is ln and one is log2
  //double div2 = divergence(p,q);
  //if(fabs(div - div2) > FLT_EPSILON) Error("A");
  return div;
}
void getAppearanceWeightForPixel(Appmodel *fg, Appmodel *bg,
    PATTERN *x, int pixel, double *fgll, double *bgll,
    struct buckets *allbuckets, int appi){
  //assert(pixel >=0);
  //Error("Make this formula match computeU. 1.");
  //Error("Make sure the sign is correct here.");
  assert(NCOLOR==4);//if not then we need to change this code
  int nfg, nbg;
  if(appi==0){
    nfg = fg->m[allbuckets[pixel].r][allbuckets[pixel].g][allbuckets[pixel].b];
    nbg = bg->m[allbuckets[pixel].r][allbuckets[pixel].g][allbuckets[pixel].b];
  }else if(appi==1){
    nfg = fg->c[0][allbuckets[pixel].r];
    nbg = bg->c[0][allbuckets[pixel].r];
  }else if(appi==2){
    nfg = fg->c[1][allbuckets[pixel].g];
    nbg = bg->c[1][allbuckets[pixel].g];
  }else if(appi==3){
    nfg = fg->c[2][allbuckets[pixel].b];
    nbg = bg->c[2][allbuckets[pixel].b];
  }else{
    int t = appi-NCOLOR;
    int tbucket = x->texture[t][pixel];
    nfg = fg->t[t][tbucket];
    nbg = bg->t[t][tbucket];
  }
  assert(nfg >= APP_MODEL_PRIOR);
  assert(nbg >= APP_MODEL_PRIOR);
  *fgll += log2(nfg);
  *bgll += log2(nbg);
  if(*fgll != *fgll || *bgll != *bgll){
    Error("problem in getAppearanceWeightForPixel.");
  }
}

int getModelTotal(int *m){
  int total = 0;
  for(int i=0; i<TOTAL_COLOR_BUCKETS; i++){
    total += m[i];
  }
  return total;
}
void setTotals(int totals[NPARTS][NHYPS], Appmodel appmodels[NPARTS][NHYPS]){
  for(int p=0; p<NPARTS; p++){
    for(int h=0; h<NHYPS; h++){
      totals[p][h] = getModelTotal(appmodels[p]+h);
    }
  }
}
inline double getSingletonFeature(int i, double f){
  return SINGLETON_SDEVINV[i]*(DEF_SINGLETON_WEIGHT*f - SINGLETON_MEANS[i]);
}
inline double getPairwiseFeature(int i, double f){
  return PAIRWISE_SDEVINV[i]*(DEF_PAIRWISE_WEIGHT*f - PAIRWISE_MEANS[i]);
}
inline double getAppearanceFeature(int i, double f){
  return APPEARANCE_SDEVINV[i]*(DEF_APPEARANCE_WEIGHT*f - APPEARANCE_MEANS[i]);
}
inline double getSymmetricFeature(int i, double f){
  return SYMMETRIC_SDEVINV[i]*(DEF_SYMMETRIC_WEIGHT*f - SYMMETRIC_MEANS[i]);
}
inline double getOverlapFeature(int i, double f){
  return OVERLAP_SDEVINV[i]*(DEF_OVERLAP_WEIGHT*f - OVERLAP_MEANS[i]);
}
inline double getSingletonEnergy(int i, double f){
  return SINGLETON_WEIGHTS[i]*getSingletonFeature(i, f);
}
inline double getPairwiseEnergy(int i, double f){
  return PAIRWISE_WEIGHTS[i]*getPairwiseFeature(i, f);
}
inline double getAppearanceEnergy(int i, double f){
  return APPEARANCE_WEIGHTS[i]*getAppearanceFeature(i, f);
}
inline double getSymmetricEnergy(int i, double f){
  return SYMMETRIC_WEIGHTS[i]*getSymmetricFeature(i, f);
}
inline double getOverlapEnergy(int i, double f){
  return OVERLAP_WEIGHTS[i]*getOverlapFeature(i, f);
}
void printWeights(){
  printf("Singleton weights:\n");
  for(int i=0; i<NPARTS; i++)
    printf("%lf, ", SINGLETON_WEIGHTS[i]);
  printf("\nPairwise weights:\n");
  for(int i=0; i<NJOINTS; i++)
    printf("%lf, ", PAIRWISE_WEIGHTS[i]);
  printf("\nAppearance weights:\n");
  for(int i=0; i<NAPP; i++)
    printf("%lf, ", APPEARANCE_WEIGHTS[i]);
  printf("\nSymmetric weights:\n");
  for(int i=0; i<NSYMS; i++)
    printf("%lf, ", SYMMETRIC_WEIGHTS[i]);
  printf("\nOverlap weights:\n");
  for(int i=0; i<NOVERLAP; i++)
    printf("%lf, ", OVERLAP_WEIGHTS[i]);
  printf("\n");
}
void printMeans(){
  printf("Singleton means:\n");
  for(int i=0; i<NPARTS; i++)
    printf("%lf, ", SINGLETON_MEANS[i]);
  printf("\nPairwise means:\n");
  for(int i=0; i<NJOINTS; i++)
    printf("%lf, ", PAIRWISE_MEANS[i]);
  printf("\nAppearance means:\n");
  for(int i=0; i<NAPP; i++)
    printf("%lf, ", APPEARANCE_MEANS[i]);
  printf("\nSymmetric means:\n");
  for(int i=0; i<NSYMS; i++)
    printf("%lf, ", SYMMETRIC_MEANS[i]);
  printf("\nOverlap means:\n");
  for(int i=0; i<NOVERLAP; i++)
    printf("%lf, ", OVERLAP_MEANS[i]);
  printf("\n");
}
void printStddevinv(){
  printf("Singleton sdi:\n");
  for(int i=0; i<NPARTS; i++)
    printf("%lf, ", SINGLETON_SDEVINV[i]);
  printf("\nPairwise sdi:\n");
  for(int i=0; i<NJOINTS; i++)
    printf("%lf, ", PAIRWISE_SDEVINV[i]);
  printf("\nAppearance sdi:\n");
  for(int i=0; i<NAPP; i++)
    printf("%lf, ", APPEARANCE_SDEVINV[i]);
  printf("\nSymmetric sdi:\n");
  for(int i=0; i<NSYMS; i++)
    printf("%lf, ", SYMMETRIC_SDEVINV[i]);
  printf("\nOverlap sdi:\n");
  for(int i=0; i<NOVERLAP; i++)
    printf("%lf, ", OVERLAP_SDEVINV[i]);
  printf("\n");
}
void printImage(unsigned char *img, int H, int W, int C){
  // Note that the rows are contiguous, not the columns. So this loop
  // is suboptimal, but it's just for debugging anyway.
  for(int r=0;r<H;r++){
    for(int c=0;c<W;c++){
      printf("[");
      for(int color = 0; color<C;color++)
        printf("%d ",img[r*C+c*H*C+color]);
      printf("] ");
    }
    printf("\n");
  }
}
void printMap(struct map_entry *map, int numMappings){
  double *m = (double*)map;
  for(int i=0;i<numMappings;i++,m+=4){
    printf("%d, pid: %f, hid: %f, pixel: %f, prob: %f\n",
        i, m[0], m[1], m[2], m[3]);
  }
}
void printModels(Appmodel *fg, Appmodel *bg){
  for(int r=0;r<NUM_COLOR_BUCKETS;r++){
    for(int g=0;g<NUM_COLOR_BUCKETS;g++){
      for(int b=0;b<NUM_COLOR_BUCKETS;b++){
        printf("[%d, %d] ", fg->m[r][g][b], bg->m[r][g][b]);
      }
      printf("\n");
    }
    printf("____________________________________________________________\n");
  }
}
void computeHypotheses(vector<vector<double> > &factor, int *hids){
  for(int i=0; i<NPARTS; i++){// iterate through the parts
    hids[i] = 0;
    double val = WORST;
    if(0) printf("i: %d val: %f\n", i, val);
    for(int j=0; j<NHYPS; j++){// iterate through the hypotheses
     //printf("val: %lf, factor: %lf\n", val, factor[i][j]);
      if(isBetter(val, factor[i][j])){
      //if( factor[i][j] < val){
        val = factor[i][j];
        hids[i] = j+1;
      }
    }
    // Check that there are no blatantly wrong hypotheses
    if(hids[i]<=0 || hids[i]>NHYPS){
      printf("hids[i]: %d\n", hids[i]);
      printf("i: %d, val: %lf\n", i, val);
      for(int j=0; j<NHYPS; j++){// iterate through the hypotheses
        printf("%f, ", factor[i][j]);
      }
      printf("\n");
      Error("problem in computeHypotheses");
    }
  }
}
void setGlobals(PATTERN *x, STRUCT_LEARN_PARM *param, CONFIGS *config){
  if(NPARTS != param->nparts) Error("Wrong number of parts.");
  if(NHYPS != param->nhyp) Error("Wrong number of hypotheses per part.");
  if(NJOINTS != param->njoints) Error("Wrong number of joints, not including symmetric appearance.");
  if(NSYMS != param->nsyms) Error("Wrong number of symmetric joints.");
  if(E != NJOINTS+NSYMS) Error("E is not the sum of NJOINTS and NSYMS.");
  MODEL_BLUR_INV = config->model_blur_inv;
  //NUM_COLOR_BUCKETS = (int)config->;
  APP_MODEL_PRIOR = config->app_model_prior;
  USE_SYMMETRY = config->use_symmetry;
  APPROX_INFERENCE = config->approx_inference;
  INFERENCE_ITERS = config->inference_iters;
  MODEL_BLUR = config->model_blur;
  HILLCLIMB = config->hillclimb;
  PLOT_INFERENCE = config->plot_inference;
  INFERENCE_METHOD = config->inference_method;
  ABCD_BLOCK_SIZE = config->abcd_block_size;
  ABCD_ITERS = config->abcd_iters;
  ABCD_INF_TIME = config->abcd_inf_time;
  INC_BLOCK = config->include_block;
  NAIVE_INIT = config->naive_init;
  IMGSIZE = x->imgsize;
  for(int i=0; i<NPARTS; i++){
    SINGLETON_WEIGHTS[i] = config->singleton_weights[i];
    SINGLETON_MEANS[i] = config->singleton_means[i];
    SINGLETON_SDEVINV[i] = config->singleton_sdevinv[i];
  }
  for(int i=0; i<NJOINTS; i++){
    PAIRWISE_WEIGHTS[i] = config->pairwise_weights[i];
    PAIRWISE_MEANS[i] = config->pairwise_means[i];
    PAIRWISE_SDEVINV[i] = config->pairwise_sdevinv[i];
  }
  for(int i=0; i<NAPP; i++){
    APPEARANCE_WEIGHTS[i] = config->appearance_weights[i];
    APPEARANCE_MEANS[i] = config->appearance_means[i];
    APPEARANCE_SDEVINV[i] = config->appearance_sdevinv[i];
  }
  for(int i=0; i<NSYMS; i++){
    SYMMETRIC_WEIGHTS[i] = config->symmetric_weights[i];
    SYMMETRIC_MEANS[i] = config->symmetric_means[i];
    SYMMETRIC_SDEVINV[i] = config->symmetric_sdevinv[i];
  }
  for(int i=0; i<NOVERLAP; i++){
    OVERLAP_WEIGHTS[i] = config->overlap_weights[i];
    OVERLAP_MEANS[i] = config->overlap_means[i];
    OVERLAP_SDEVINV[i] = config->overlap_sdevinv[i];
    //printf("%f %f\n", OVERLAP_MEANS[i], OVERLAP_SDEVINV[i]);
  }
  //exit(0);
  isBetter = config->isBetter;
  WORST = config->worst;
}
void checkWeights(){
  if(RAMANAN) return;//TODO
  if(SINGLETON_WEIGHTS[0] != SINGLETON_WEIGHTS[3]){
    Error("SINGLETON_WEIGHTS 0 and 3 must be equal.");
  } else if(SINGLETON_WEIGHTS[1] != SINGLETON_WEIGHTS[2]){
    Error("SINGLETON_WEIGHTS 1 and 2 must be equal.");
  } else if(SINGLETON_WEIGHTS[4] != SINGLETON_WEIGHTS[7]){
    Error("SINGLETON_WEIGHTS 4 and 7 must be equal.");
  } else if(SINGLETON_WEIGHTS[5] != SINGLETON_WEIGHTS[6]){
    Error("SINGLETON_WEIGHTS 5 and 6 must be equal.");
  } else if(PAIRWISE_WEIGHTS[0] != PAIRWISE_WEIGHTS[2]){
    Error("PAIRWISE_WEIGHTS 0 and 2 must be equal.");
  } else if(PAIRWISE_WEIGHTS[1] != PAIRWISE_WEIGHTS[3]){
    Error("PAIRWISE_WEIGHTS 1 and 3 must be equal.");
  } else if(PAIRWISE_WEIGHTS[4] != PAIRWISE_WEIGHTS[6]){
    Error("PAIRWISE_WEIGHTS 4 and 6 must be equal.");
  } else if(PAIRWISE_WEIGHTS[5] != PAIRWISE_WEIGHTS[7]){
    Error("PAIRWISE_WEIGHTS 5 and 7 must be equal.");
  } else if(OVERLAP_WEIGHTS[0] != OVERLAP_WEIGHTS[17]){
    Error("OVERLAP_WEIGHTS 0 and 17 must be equal.");
  } else if(OVERLAP_WEIGHTS[1] != OVERLAP_WEIGHTS[10]){
    Error("OVERLAP_WEIGHTS 1 and 10 must be equal.");
  } else if(OVERLAP_WEIGHTS[3] != OVERLAP_WEIGHTS[27]){
    Error("OVERLAP_WEIGHTS 3 and 27 must be equal.");
  } else if(OVERLAP_WEIGHTS[4] != OVERLAP_WEIGHTS[26]){
    Error("OVERLAP_WEIGHTS 4 and 26 must be equal.");
  } else if(OVERLAP_WEIGHTS[5] != OVERLAP_WEIGHTS[25]){
    Error("OVERLAP_WEIGHTS 5 and 25 must be equal.");
  } else if(OVERLAP_WEIGHTS[6] != OVERLAP_WEIGHTS[24]){
    Error("OVERLAP_WEIGHTS 6 and 24 must be equal.");
  } else if(OVERLAP_WEIGHTS[7] != OVERLAP_WEIGHTS[28]){
    Error("OVERLAP_WEIGHTS 7 and 28 must be equal.");
  } else if(OVERLAP_WEIGHTS[8] != OVERLAP_WEIGHTS[29]){
    Error("OVERLAP_WEIGHTS 8 and 29 must be equal.");
  } else if(OVERLAP_WEIGHTS[11] != OVERLAP_WEIGHTS[21]){
    Error("OVERLAP_WEIGHTS 11 and 21 must be equal.");
  } else if(OVERLAP_WEIGHTS[12] != OVERLAP_WEIGHTS[20]){
    Error("OVERLAP_WEIGHTS 12 and 20 must be equal.");
  } else if(OVERLAP_WEIGHTS[13] != OVERLAP_WEIGHTS[19]){
    Error("OVERLAP_WEIGHTS 13 and 19 must be equal.");
  } else if(OVERLAP_WEIGHTS[14] != OVERLAP_WEIGHTS[18]){
    Error("OVERLAP_WEIGHTS 14 and 18 must be equal.");
  } else if(OVERLAP_WEIGHTS[15] != OVERLAP_WEIGHTS[22]){
    Error("OVERLAP_WEIGHTS 15 and 22 must be equal.");
  } else if(OVERLAP_WEIGHTS[16] != OVERLAP_WEIGHTS[23]){
    Error("OVERLAP_WEIGHTS 16 and 23 must be equal.");
  } else if(OVERLAP_WEIGHTS[30] != OVERLAP_WEIGHTS[39]){
    Error("OVERLAP_WEIGHTS 30 and 39 must be equal.");
  } else if(OVERLAP_WEIGHTS[31] != OVERLAP_WEIGHTS[36]){
    Error("OVERLAP_WEIGHTS 31 and 36 must be equal.");
  } else if(OVERLAP_WEIGHTS[33] != OVERLAP_WEIGHTS[42]){
    Error("OVERLAP_WEIGHTS 33 and 42 must be equal.");
  } else if(OVERLAP_WEIGHTS[34] != OVERLAP_WEIGHTS[43]){
    Error("OVERLAP_WEIGHTS 34 and 43 must be equal.");
  } else if(OVERLAP_WEIGHTS[37] != OVERLAP_WEIGHTS[40]){
    Error("OVERLAP_WEIGHTS 37 and 40 must be equal.");
  } else if(OVERLAP_WEIGHTS[38] != OVERLAP_WEIGHTS[41]){
    Error("OVERLAP_WEIGHTS 38 and 41 must be equal.");
  }
  // Note: There are no contraints for the appearance weights
  //       or symmetric weights.
}
void inline setLogs(double *logs, int *cur, int want){
  if(*cur >= want) return;
  if( *cur &/*bitwise*/ 1){ // if odd
    for(int i=*cur+2; i<=want; i+=2)
      logs[i] = log2(i);
    //calculate the even logs using the odds
    for(int i=*cur+1; i<=want; i+=2)
      logs[i] = 1+logs[i>>1];
  } else {
    for(int i=*cur+1; i<=want; i+=2)
      logs[i] = log2(i);
    //calculate the even logs using the odds
    for(int i=*cur+2; i<=want; i+=2)
      logs[i] = 1+logs[i>>1];
  }
  /*
  for(int i=*cur+1; i<=want; i++)
    logs[i] = log2(i);
    */
  *cur = MAX(*cur, want);
}
// first bit of mode means ADD.
// second bit of mode means SUBTRACT.
void transferAppearnceNoBlur(Appmodel *fg, Appmodel *bg, PATTERN *x,
    int *mask, int pid, int hidold, int hidnew, struct buckets *allbuckets,
    int mode = 3);
void transferAppearnceNoBlur(Appmodel *fg, Appmodel *bg, PATTERN *x,
    int *mask, int pid, int hidold, int hidnew, struct buckets *allbuckets,
    int mode){
  if(hidold<0 || hidold>=NHYPS || hidnew<0 || hidnew>=NHYPS){//TODO
    printf("hidold %d, hidnew %d\n", hidold, hidnew);
    Error("hid is wrong in transferAppearnceNoBlur. Should be 0 indexed.");
  }
  if(false) printf("pid %d, hidold %d, hidnew %d\n", pid, hidold, hidnew);
  int debug_transfer = 0;
  // remove the old part
  int index, nextIndex;
  if(SUBTRACT_APP_MODEL_MASK & mode){// single &
    index = x->fgmapIndices[pid][hidold];
    nextIndex = getNextIndex(x, pid, hidold);
    for(; index<nextIndex; index++){
      int pixel = (int)(x->fgmap[index].pixel)-1;
      mask[pixel] --;
      if(mask[pixel] >= 1) continue; // still in the fg
      if(debug_transfer && mask[pixel] < 0){
        printf("pid %d, hidold %d, hidnew %d\n", pid, hidold, hidnew);
        printf("pix: %d, mask: %d\n", pixel, mask[pixel]);
        Error("bad mask 1");
      }
      // subtract from the fg
      fg->m[allbuckets[pixel].r][allbuckets[pixel].g][allbuckets[pixel].b] --;
      // add to the bg
      bg->m[allbuckets[pixel].r][allbuckets[pixel].g][allbuckets[pixel].b] ++;
      for(int t=0; t<NTEXTURES; t++){
        int tbucket = x->texture[t][pixel];
        fg->t[t][tbucket]--;
        bg->t[t][tbucket]++;
      }
      if(debug_transfer){
        assert(fg->m[allbuckets[pixel].r][allbuckets[pixel].g][allbuckets[pixel].b] >= APP_MODEL_PRIOR);
        assert(bg->m[allbuckets[pixel].r][allbuckets[pixel].g][allbuckets[pixel].b] >= APP_MODEL_PRIOR+1);
      }
    }
  }
  // add the new part
  if(ADD_APP_MODEL_MASK & mode){//single &
    index = x->fgmapIndices[pid][hidnew];
    nextIndex = getNextIndex(x, pid, hidnew);
    for(; index<nextIndex; index++){
      int pixel = (int)(x->fgmap[index].pixel)-1;
      mask[pixel] ++;
      if(mask[pixel] > 1) continue; // already in the fg
      if(debug_transfer && mask[pixel] < 0){
        printf("pid %d, hidold %d, hidnew %d\n", pid, hidold, hidnew);
        printf("pix: %d, mask: %d\n", pixel, mask[pixel]);
        Error("bad mask 2");
      }
      // add to the fg
      fg->m[allbuckets[pixel].r][allbuckets[pixel].g][allbuckets[pixel].b] ++;
      // subtract from the bg
      bg->m[allbuckets[pixel].r][allbuckets[pixel].g][allbuckets[pixel].b] --;
      for(int t=0; t<NTEXTURES; t++){
        int tbucket = x->texture[t][pixel];
        fg->t[t][tbucket]++;
        bg->t[t][tbucket]--;
      }
      if(debug_transfer){
        assert(fg->m[allbuckets[pixel].r][allbuckets[pixel].g][allbuckets[pixel].b] >= APP_MODEL_PRIOR+1);
        assert(bg->m[allbuckets[pixel].r][allbuckets[pixel].g][allbuckets[pixel].b] >= APP_MODEL_PRIOR);
      }
    }
  }
}
void printHids(int *hids){
  printf("HIDs: ");
  for(int i=0; i<NPARTS; i++)
    printf("%d, ", hids[i]);
  printf("\n");
}
void computeFgAppearnceNoBlur(Appmodel *fg, PATTERN *x,
    int *mask, int *hids, struct buckets *allbuckets, struct block *b = NULL);
void computeFgAppearnceNoBlur(Appmodel *fg, PATTERN *x,
    int *mask, int *hids, struct buckets *allbuckets, struct block *b){
  // Loop through the hypotheses for each part and add some weight to the foregound
  // for pixels in the map for that part/hypothesis.
  for(int pid=0; pid<NPARTS; pid++){
    // only count the parts NOT in the block.
    if(b && b->b[pid]) continue;
    // hids is 1 indexed, but so is map[index].hid
    int hid = hids[pid]-1;
    if(hid<0 || hid>=NHYPS){
      printf("hid %d\n",hid);
      Error("hid is wrong in computeFgAppearnceNoBlur. Should be 0 indexed.");
    }
    if(0) printf("pid %d, hid %d\n", pid, hid);
    int index = x->fgmapIndices[pid][hid];
    int nextIndex = getNextIndex(x, pid, hid);
    for(; index<nextIndex; index++){
      int pixel = (int)(x->fgmap[index].pixel)-1;
      //printf("%d\n", pixel+1);
      assert(pixel >= 0 && pixel<x->imgsize);
      mask[pixel] ++;
      if(mask[pixel] > 1) continue; // dont double count
      fg->m[allbuckets[pixel].r][allbuckets[pixel].g][allbuckets[pixel].b] ++;
      for(int t=0; t<NTEXTURES; t++){
        int tbucket = x->texture[t][pixel];
        fg->t[t][tbucket]++;
      }
    }
  }
}
inline void computeBgAppearnceNoBlur(Appmodel *bg, PATTERN *x,
    int *mask, struct buckets *allbuckets){
  for(int p=0;p<IMGSIZE;p++){
    if(mask[p] > 0) continue;
    bg->m[allbuckets[p].r][allbuckets[p].g][allbuckets[p].b] ++;
    for(int t=0; t<NTEXTURES; t++){
      int tbucket = x->texture[t][p];
      bg->t[t][tbucket]++;
    }
  }
}
void calculateAppModelsNoBlur(Appmodel *fg, Appmodel *bg, PATTERN *x,
    int *hids, struct buckets *allbuckets){
  initModel(fg);
  initModel(bg);
  int *mask = (int*)calloc(IMGSIZE,sizeof(int));
  computeFgAppearnceNoBlur(fg, x, mask, hids, allbuckets);
  if(DEBUGFG){
    printHids(hids);
    int sum=0;
    for(int i=0; i<IMGSIZE; i++) if(mask[i]) sum++;
    printf("n in mask: %d\n", sum);
    //exit(0);
  }

  computeBgAppearnceNoBlur(bg, x, mask, allbuckets);
  free(mask);
}
double getOverlapEnergies( int *hids, PATTERN *x, FEATURES *f){
  double energy = 0.0;
  for(int o=0; o<NOVERLAP; o++){
    int p1 = Overlap_edges[o];
    int p2 = Overlap_edges[o+NOVERLAP];
    int h1 = hids[p1]-1;
    int h2 = hids[p2]-1;
    assert(h1>=0 && h1<NHYPS && h2>=0 && h2<NHYPS);
    if(f){
      f->overlap[o] = getOverlapFeature(o, x->overlap[o][h1][h2]);
      //printf("%f\n", f->overlap[o]);
      //printf("%f\n", x->overlap[o][h1][h2]);
      //exit(0);
    }
    energy += getOverlapEnergy(o, x->overlap[o][h1][h2]);
    if(energy!=energy){
      printf("energy: %lf\n", energy);
      printWeights();
      printMeans();
      printStddevinv();
      printf("o: %d, overlap weight: %lf, overlap: %lf, h1: %d, h2: %d\n", o,
          OVERLAP_WEIGHTS[o], x->overlap[o][h1][h2], h1, h2);
      printf("NFEATURES: %d\n", NFEATURES);
      Error("problem in getOverlapEnergies");
    }
  }
  return energy;
}
void setLABChannels( Appmodel *m){
  for(int c=0; c<NCOLOR-1; c++){
    for(int i=0; i<NUM_COLOR_BUCKETS; i++){
      m->c[c][i] = APP_MODEL_PRIOR*(1-NUM_COLOR_BUCKETS*NUM_COLOR_BUCKETS);
    }
  }
  for(int i=0; i<NUM_COLOR_BUCKETS; i++){
    for(int j=0; j<NUM_COLOR_BUCKETS; j++){
      for(int k=0; k<NUM_COLOR_BUCKETS; k++){
        m->c[0][i] += m->m[i][j][k];
        m->c[1][j] += m->m[i][j][k];
        m->c[2][k] += m->m[i][j][k];
      }
    }
  }
  for(int c=0; c<NCOLOR-1; c++){
    for(int i=0; i<NUM_COLOR_BUCKETS; i++){
      assert(m->c[c][i] >= APP_MODEL_PRIOR);
    }
  }
}
double calculateEnergy(PATTERN *x, int *hids, STRUCT_LEARN_PARM *param,
    CONFIGS *config, FEATURES *features, double *nlogn,
    RetEnergy *retenergy,
    struct buckets *allbuckets, int numnlogn, bool print){
  if(config) setGlobals(x, param, config);
  checkWeights();
  bool givenAllbuckets = bool(allbuckets);
  // Calculate the symmetric energies
  //bool givenSyms = bool(symAndPair);
  //if(!givenSyms) symAndPair =
  //  (double(*)[NHYPS][NHYPS]) malloc(sizeof(double)*E*NHYPS*NHYPS);
  // a few are unused but this makes indexing easier
  Appmodel fg,bg;
  int DEBUG_CE = 0;
  int USE_APP = 1;
  if(USE_APP){
    // for background and foreground models
    if(!givenAllbuckets){
      //Error("allbuckets");
      allbuckets = (struct buckets*)
        malloc(sizeof(struct buckets)*IMGSIZE);
      discretizeAll(allbuckets, x->image);
    }
    initModel(&fg);
    initModel(&bg);
    calculateAppModelsNoBlur(&fg, &bg, x, hids, allbuckets);
    if(!givenAllbuckets) free(allbuckets);
  }
  // Calculate the features
  double f1[NPARTS]; // Singleton Features
  for(int pid=0; pid<NPARTS; pid++){
    int hid = hids[pid]-1;
    if(hid<0 || hid>=NHYPS){
      printf("hid %d\n",hid);
      Error("hid is wrong in calculateEnergy. Should be 0 indexed.");
    }
    //f1[pid] = DEF_SINGLETON_WEIGHT*x->singleton[pid][hid];
    f1[pid] = getSingletonFeature(pid, x->singleton[pid][hid]);
    if(features){
      features->singleton[pid] = f1[pid];
      //printf("pid: %d, feature: %lf, sing: %lf, app: %lf\n",
      //    pid, f1[pid], features->singleton[pid], features->appearance[pid]);
    }
  }
  double f3[NAPP]; // Appearance Features
  if(USE_APP){
    //Error("Need to add up ALL fg and bg app models");
    setLABChannels( &fg);
    setLABChannels( &bg);
    double app = 0.0;
    int fgtotal = getModelTotal( &fg);
    int bgtotal = getModelTotal( &bg);
    assert(nlogn);
    if(nlogn){
      if(numnlogn >=0){
        assert(fgtotal<=numnlogn && bgtotal<=numnlogn);
      }
      app = computeU7( (int*)fg.m, (int*)bg.m, fgtotal, bgtotal, nlogn);
    }else{
      app = computeU( &fg, &bg, fgtotal, bgtotal);
    }
    if(DEBUGFG){
      printf("app: %lf, %d %d\n", app, fgtotal, bgtotal);
      /*
      for(int i=0; i<NUM_COLOR_BUCKETS; i++){
        for(int j=0; j<NUM_COLOR_BUCKETS; j++){
          for(int k=0; k<NUM_COLOR_BUCKETS; k++){
            printf("%d ", fg.m[i][j][k]);
          }
          printf("\n");
        }
        printf("%d ^^^__________\n", i);
      }
      printf("\nXXXXXXX\n");
      */
      for(int i=0; i<NUM_COLOR_BUCKETS; i++)
        printf("%d ", fg.c[0][i]);
      printf("\n");
      for(int i=0; i<NUM_COLOR_BUCKETS; i++)
        printf("%d ", fg.c[1][i]);
      printf("\n");
      for(int i=0; i<NUM_COLOR_BUCKETS; i++)
        printf("%d ", fg.c[2][i]);
      printf("\n");
    }
    f3[0] = getAppearanceFeature(0, app);
    assert(f3[0]==f3[0]);
    for(int i=0; i<NCOLOR-1; i++){
      if(nlogn){
        if(numnlogn >=0){
          assert(fgtotal<=numnlogn && bgtotal<=numnlogn);
        }
        app = computeULAB2( fg.c[i], bg.c[i], nlogn);
      }else{
        app = computeULAB( fg.c[i], bg.c[i]);
      }
      f3[i+1] = getAppearanceFeature(i+1, app);
      if(DEBUGFG) printf("color %d, app: %lf\n", i, app);
      assert(f3[i+1]==f3[i+1]);
    }
    for(int i=0; i<NTEXTURES; i++){
      if(nlogn){
        if(numnlogn >=0){
          assert(fgtotal<=numnlogn && bgtotal<=numnlogn);
        }
        app = computeULAB2( fg.t[i], bg.t[i], nlogn);
      }else{
        app = computeULAB( fg.t[i], bg.t[i]);
      }
      f3[i+NCOLOR] = getAppearanceFeature(i+NCOLOR, app);
      if(DEBUGFG) printf("text %d, app: %lf\n", i, app);
      assert(f3[i+NCOLOR]==f3[i+NCOLOR]);
    }
      /*
      if(0 && APPD) printf("app: %lf, mean: %lf, inv: %lf, f3: %lf\n", app,
          APPEARANCE_MEANS[i], APPEARANCE_SDEVINV[i], f3[i]);
      if(0 && print){
        cumuapp += app;
        cumufg += fgtotal;
        cumubg += bgtotal;
        if(0) printf("fgtotal %d, bgtotal %d, cumufg %d, cumubg %d\n",
          fgtotal, bgtotal, cumufg, cumubg);
        printf("%lf, %lf\n", app, cumuapp);
      }
      if(f3[i]!=f3[i]){
        printf("fgtotal %d, bgtotal %d\n", fgtotal, bgtotal);
        printf("i %d, app: %lf, f3: %lf, APP WEIGHT: %f\n",
            i, app, f3[i], APPEARANCE_WEIGHTS[i]);
        Error("f3 is NaN.");
      }
      */
  }
  if(features){
    for(int i=0; i<NAPP; i++)
      features->appearance[i] = f3[i];
  }
  double f2[NJOINTS]; // Pairwise Features
  double f2all = 0.0;
  for(int i=0; i<NJOINTS; i++){
    // Joints is 1 indexed
    int from = param->joints[i].j1 - 1;
    int to   = param->joints[i].j2 - 1;
    if(from<0 || from>=NPARTS || to<0 || to>=NPARTS){
      printf("from %d, to %d\n", from, to);
      Error("from or to is wrong in calculateEnergy. Should be 0 indexed.");
    }
    // hids is 1 indexed
    int from_hid = hids[from]-1;
    int to_hid = hids[to]-1;
    if(from_hid<0 || from_hid>=NHYPS || to_hid<0 || to_hid>=NHYPS){
      printf("from_hid %d, to_hid %d\n", from_hid, to_hid);
      Error("from_hid or to_hid is wrong in calculateEnergy. Should be 0 indexed.");
    }
    //f2[i] = x->pairwise[i][to_hid][from_hid];
    f2[i] = getPairwiseFeature(i, x->symAndPair[NSYMS+i][from_hid][to_hid]);
    f2all += x->symAndPair[NSYMS+i][from_hid][to_hid];
    //printf("f2: %d, %lf, %lf\n", i, f2[i], x->pairwise[i][from_hid][to_hid]);
    if(features) features->pairwise[i] = f2[i];
  }
  if(0 && print){
    cumupair += f2all;
    printf("%lf, %lf\n", f2all, cumupair);
  }
  double f4[NSYMS]; // Symmetric Features
  for(int i=0; i<NSYMS; i++){
    // Syms is 1 indexed
    int from = param->syms[i].j1 - 1;
    int to   = param->syms[i].j2 - 1;
    if(from<0 || from>=NPARTS || to<0 || to>=NPARTS){
      printf("from %d, to %d\n", from, to);
      Error("from or to is wrong in calculateEnergy. Should be 0 indexed.");
    }
    // hids is 1 indexed
    int from_hid = hids[from]-1;
    int to_hid = hids[to]-1;
    if(from_hid<0 || from_hid>=NHYPS || to_hid<0 || to_hid>=NHYPS){
      printf("from_hid %d, to_hid %d\n", from_hid, to_hid);
      Error("from_hid or to_hid is wrong in calculateEnergy. Should be 0 indexed.");
    }
    f4[i] = 0.0;
    if(USE_APP){
      f4[i] = getSymmetricFeature(i, x->symAndPair[i][from_hid][to_hid]);
      if(DEBUG_CE && USE_SYMMETRY && f4[i] == 0.0){
        printSymAndPair( x->symAndPair, true);
        printf("sym feature is 0. f: %lf, i: %d, from_hid: %d, to_hid: %d\n",
            f4[i], i, from_hid, to_hid);
        printf("part1: %d, part2: %d\n",
            param->syms[i].j1-1, param->syms[i].j2-1);
      }
    }
    if(features){
      if(USE_SYMMETRY) features->sym[i] = f4[i];
      else             features->sym[i] = 0.0;
    }
  }
  if(features && 0){
    printf("sym features %d: %lf %lf %lf %lf\n", NSYMS, features->sym[0],
        features->sym[1], features->sym[2], features->sym[3]);
  }
  if(DEBUG_CE && USE_SYMMETRY && (f4[0]==0.0 || f4[1]==0.0 || f4[2]==0.0 || f4[3]==0.0)){
    printf("sym features %d: %lf %lf %lf %lf\n", NSYMS, f4[0], f4[1], f4[2], f4[3]);
    //assert(false);
    //Error("See why some symAndPairs are 0.0");
  }
  double e1 = 0.0;
  double e2 = 0.0;
  double e3 = 0.0;
  double e4 = 0.0;
  double e5 = getOverlapEnergies( hids, x, features);
  for(int i=0; i<NPARTS; i++){
    e1 += SINGLETON_WEIGHTS[i]*f1[i];
    /*if(retenergy)
      printf("i: %d, SINGLETON_WEIGHTS[i]: %lf, f1[i]: %lf, hids[i]: %d\n",
          i, SINGLETON_WEIGHTS[i], f1[i], hids[i]);*/
  }
  for(int i=0; i<NJOINTS; i++) {
    e2 += PAIRWISE_WEIGHTS[i]*f2[i];
    /*if(retenergy && 0)
      printf("i: %d, PAIRWISE_WEIGHTS[i]: %lf, f2[i]: %lf\n",
          i, PAIRWISE_WEIGHTS[i], f2[i]);*/
    //printf("E: %lf\n",energy);
  }
  //printf("%f, %f\n", (float)(f2[0]+f2[2]), f2[1]+f2[3]);
  for(int i=0; i<NAPP; i++)
    e3 += APPEARANCE_WEIGHTS[i]*f3[i];
  if(USE_SYMMETRY){
    if(DEBUG_CE) printf("sym features %d: %lf %lf %lf %lf\n", NSYMS, f4[0], f4[1], f4[2], f4[3]);
    for(int i=0; i<NSYMS; i++){
      if(DEBUG_CE) printf("sym weight %d: %lf, f: %lf\n", i, SYMMETRIC_WEIGHTS[i], f4[i]);
      e4 += SYMMETRIC_WEIGHTS[i]*f4[i];
    }
  }
  if(retenergy){
    retenergy->e1 = e1;
    retenergy->e2 = e2;
    retenergy->e3 = e3;
    retenergy->e4 = e4;
    retenergy->e5 = e5;
    if(APPD) printf("e1 %lf, e2 %lf, e3 %lf, e4 %lf, e5 %lf\n", e1, e2, e3, e4, e5);
  }
  /*
  printHids( hids);
  //for(int i=0; i<NAPP; i++)
  for(int i=0; i<NCOLOR; i++)
    printf("%lf, ", f3[i]);
  printf("\n");
  printf("e1 %lf, e2 %lf, e3 %lf, e4 %lf, e5 %lf\n", e1, e2, e3, e4, e5);
  */
  //if(print) printf("e1 %lf, e2 %lf, e3 %lf, e4 %lf, e5 %lf\n", e1, e2, e3, e4, e5);
  double energy = e1 + e2 + e3 + e4 + e5;

  // 1e11, since there are 10 parts and each could have energy as high as 1e10
  // when the energy is artificially increased to only choose correct ones
  if(energy > (NHYPS*1e10+1e9) || energy!=energy){
    printf("Energy: %f, %f, %f, %f, %f, %f\n", energy, e1, e2, e3, e4, e5);
    Error("energy is excessively high in calculateEnergy.");
  }
  //if(!givenSyms) free(symAndPair);
  return energy;
}
double getLoss(int y[][NHYPS], int *hids){
  /* loss for correct label y and predicted label ybar. The loss for
     y==ybar has to be zero. sparm->loss_function is set with the -l option. */
  double loss = 0.0;
  for(int i=0; i<NPARTS; i++){
    int hid = hids[i]-1;
    loss += (1.0 - y[i][hid]);
  }
  // Scale to be within the 0-1 loss range
  return LOSS_SCALE*loss;
}
double getLoss(LOSS *loss, int *hids){
  double l = 0.0;
  for(int i=0; i<NPARTS; i++){
    int hid = hids[i] - 1;
    l += loss->l[i][hid];
  }
  return l;
}
void calculateSymAndPairEnergies(double symAndPairEnergies[E][NHYPS][NHYPS],
    PATTERN *x){
    //double symAndPair[E][NHYPS][NHYPS]){
  for(int jidx = 0; jidx<NSYMS; jidx++){
    for (int i=0; i<NHYPS; i++){ // all states of "from"
      for (int j=0; j<NHYPS; j++){ // all states of "to"
        symAndPairEnergies[jidx][i][j] =
          getSymmetricEnergy(jidx, x->symAndPair[jidx][i][j]);
      }
    }
  }
  // combine pairwise/symAndPair
  for(int jidx = 0; jidx<NJOINTS; jidx++){
    for (int i=0; i<NHYPS; i++){
      for (int j=0; j<NHYPS; j++){
        // i is the hypothesis of the first part #
        // j is the hypothesis of the second part #
        symAndPairEnergies[jidx+NSYMS][i][j] =
          getPairwiseEnergy( jidx, x->symAndPair[jidx+NSYMS][i][j]);
      }
    }
  }
}
void approxInference(vector<vector<double> > &factor, PATTERN *x, JOINT *joints,
    int *edges, int *hids, int *degree,
    Appmodel *fg, Appmodel *bg,
    STRUCT_LEARN_PARM *param, double *nlogn){
  // init messages to 0 = log(1)
  // one msg in each direction for each (edge,state pair)
  double (*old_msg)[E][NHYPS] =
    (double(*)[E][NHYPS]) malloc(sizeof(double)*2*E*NHYPS);
  // one msg in each direction for each (edge,state pair)
  double (*msg)[E][NHYPS] =
    (double(*)[E][NHYPS]) malloc(sizeof(double)*2*E*NHYPS);
  for(int e=0; e<E; e++){
    for(int k=0; k<NHYPS; k++){
        msg[0][e][k] = msg[1][e][k] = 0.0;
        old_msg[0][e][k] = old_msg[1][e][k] = 0.0;
    }
  }

  // then run approxInference
  //Appmodel fg,bg; // for debugging to make sure energy decreases
  double energy, prev_energy = INFINITY;
  if(PLOT_INFERENCE || DEBUG7){
    computeHypotheses(factor, hids);
    printHids(hids);
    Error("Need to pass STRUCT_LEARN_PARM to approx inference before calling calc energy.");
    /*energy = calculateEnergy(x, hids);
    push_vec_double(inference_energies, energy);
    printf("Energy before approx inference: %f\n", energy);
    */
  }
  for(int iter=0; iter<INFERENCE_ITERS; iter++){
    // choose a direction
    //int d = rand()%2;
#if RAMANAN
    for(int d=1; d>=0; d--){
#else
    for(int d=0; d<2; d++){
#endif
      // choose an edge
      //int e = rand()%E;
      // Reverse edges go in the other order so that iterating through the same as
      // exact inference.
      for(int e = ((d==0) ? 0 : E-1); e<E && e >=0; e+=(1-2*d)){
        // to and from are part numbers
        int from = edges[e + ((d+0)%2)*E];
        int to   = edges[e + ((d+1)%2)*E];
        if(from<0 || from>=NPARTS || to<0 || to>=NPARTS){
          printf("from %d, to %d\n", from, to);
          Error("from or to is wrong in approxInference. Should be 0 indexed.");
        }
        // for each state that the "to" node could be in
        for(int t=0; t<NHYPS; t++){
          // for each state that the "from" node could be in
          double message = WORST;
          for(int f=0; f<NHYPS; f++){
            double m = factor[from][f] - msg[(d+1)%2][e][f];
            if(e<NSYMS) m += getSymmetricEnergy(e, x->symAndPair[e][(d?t:f)][(d?f:t)]);
            else m += getPairwiseEnergy(e-NSYMS, x->symAndPair[e][(d?t:f)][(d?f:t)]);
            if(isBetter(message, m)) message = m;
          }
          double diff = message - msg[d][e][t];
          //printf("%f, %f, %f\n", message, msg[d][e][t], diff);
          factor[to][t] += diff;
          msg[d][e][t] = message;
          if(DEBUG5) printf("approx: d %d, e %d, to %d, msg: %f\n",d,e,t,msg[d][e][t]);
        }
        if(PLOT_INFERENCE){
          computeHypotheses(factor, hids);
          //calculateAppModels(fg, bg, img, map, hids, degree,
          //  factor, joints, singleton, numMappings);
          Error("Need to pass STRUCT_LEARN_PARM to approx inference before calling calc energy.");
          energy = calculateEnergy(x, hids, NULL/*param*/, NULL, NULL, nlogn);
          //inference_energies.push_back(energy);
          if(DEBUG7) printf("Approx inference energy: %f, iter %d, d %d, e %d\n",
            energy,iter,d,e);
        }
      }
    }
    //calculateAppModels(fg, bg, img, map, hids, degree,
    //    factor, joints, singleton, numMappings);
    computeHypotheses(factor, hids);
    if(DEBUG7) printHids(hids);
    //Error("Need to pass STRUCT_LEARN_PARM to approx inference before calling calc energy.");
    energy = calculateEnergy(x, hids, param, NULL, NULL, nlogn);
    if(energy!=energy)
      Error("Energy is NaN in approxInference.");
    if(DEBUG7) printf("Energy after approx inference iter %d: %f\n", iter, energy);
    if(energy >= prev_energy){
      //printf("Breaking after %d iters. Energy %f, Prev %f\n", 1+iter, energy, prev_energy);
      break;
    }
    prev_energy = energy;
  }
  // Finalize the factors
  for(int d=0; d<2; d++){
    // choose an edge
    for(int e=0; e<E; e++){
      int from = edges[e + ((d+0)%2)*E];
      int to   = edges[e + ((d+1)%2)*E];
      if(from<0 || from>=NPARTS || to<0 || to>=NPARTS){
        printf("from %d, to %d\n", from, to);
        Error("from or to is wrong in approxInference finalize factors. Should be 0 indexed.");
      }
      // for each state that the "to" node could be in
      for(int t=0; t<NHYPS; t++){
        // for each state that the "from" node could be in
        //printf("%f\n", msg[d][e][t]);
        //factor[to + t*NPARTS] += msg[d][e][t];
        int msg_i = d*E + e + t*2*E;
        if(msg_i<0 || (USE_SYMMETRY && msg_i>=2*E*NHYPS) ||
            (!USE_SYMMETRY && msg_i>=2*NJOINTS*NHYPS)){
          printf("msg_i is bad: %d. USE_SYMMETRY: %d, NJOINTS %d, E %d, NHYPS %d\n",
              msg_i, USE_SYMMETRY, NJOINTS, E, NHYPS);
        }
      }
    }
  }
  free(msg);
  free(old_msg);
  // debug
  if(0) printSymAndPair( x->symAndPair, false);
}
void exactInference(vector<vector<double> > &factor, PATTERN *x, JOINT *joints,
    int *hids){
  // for keeping the forward messages
  double *fwdmsg = (double*)malloc(sizeof(double)*NJOINTS*NHYPS);

  // forward pass
  //for(int jidx = 0; jidx<NJOINTS; jidx++){
  //TODO this is a hack. should really just change
  //     the joint ordering so that inference is
  //     done in the right order.
  int INITJIDX = (RAMANAN ? NJOINTS-1 : 0);
  int JIDXDIFF = (RAMANAN ? -1 : 1);
  int JIDXMULT = (RAMANAN ? -1 : 1);
  int JIDXLAST = (RAMANAN ? 0 : NJOINTS-1);
  for(int jidx = INITJIDX; JIDXMULT*jidx<=JIDXLAST; jidx+=JIDXDIFF){
    int from = joints[jidx].j1-1;
    int to   = joints[jidx].j2-1;
    //printf("from %d, to %d\n", from, to);
    if(from<0 || from>=NPARTS || to<0 || to>=NPARTS){
      printf("from %d, to %d\n", from, to);
      Error("from or to is wrong in exactInference. Should be 0 indexed.");
    }
    for (int i=0; i<NHYPS; i++){ // all states of "to"
      double msg = WORST;
      for (int j=0; j<NHYPS; j++){ // all states of "from"
        double m = factor[from][j] +
          getPairwiseEnergy(jidx, x->symAndPair[NSYMS+jidx][j][i]);
        //printf("%lf\n", getPairwiseEnergy(jidx, x->symAndPair[NSYMS+jidx][j][i]));
        if(isBetter(msg, m)) msg = m;
      }
      if(msg == WORST) Error("msg is WORST in exact Inference.");
      if(DEBUG5) printf("exact: d 0, e %d, to %d, msg: %f\n",jidx,i,msg);
      fwdmsg[jidx*NHYPS+i] = msg;
      if(DEBUG) printf("message %f\n",msg);
      int msg_i = jidx+i*2*NJOINTS;
      if(msg_i<0 || (USE_SYMMETRY && msg_i>=2*E*NHYPS) ||
          (!USE_SYMMETRY && msg_i>=2*NJOINTS*NHYPS)){
        printf("msg_i is bad: %d. USE_SYMMETRY: %d, NJOINTS %d, E %d, NHYPS %d\n",
            msg_i, USE_SYMMETRY, NJOINTS, E, NHYPS);
      }
      factor[to][i] += msg;
    }
  }

  // backward pass
  //for(int jidx = NJOINTS-1; jidx >= 0; jidx--){
  INITJIDX = (!RAMANAN ? NJOINTS-1 : 0);
  JIDXDIFF = (!RAMANAN ? -1 : 1);
  JIDXMULT = (!RAMANAN ? -1 : 1);
  JIDXLAST = (!RAMANAN ? 0 : NJOINTS-1);
  for(int jidx = INITJIDX; JIDXMULT*jidx<=JIDXLAST; jidx+=JIDXDIFF){
  //for(int jidx = 0; jidx<NJOINTS; jidx++){
    int from = joints[jidx].j2-1;
    int to   = joints[jidx].j1-1;
    //printf("from %d, to %d\n", from, to);
    if(from<0 || from>=NPARTS || to<0 || to>=NPARTS){
      printf("from %d, to %d\n", from, to);
      Error("from or to is wrong in exactInference backward pass. Should be 0 indexed.");
    }

    for (int i=0; i<NHYPS; i++){ // all states of "to"
      double msg = WORST;
      for (int j=0; j<NHYPS; j++){ // all states of "from"
        double m = factor[from][j] - fwdmsg[jidx*NHYPS+j] +
            getPairwiseEnergy( jidx, x->symAndPair[NSYMS+jidx][i][j]);
        if(isBetter(msg, m)) msg = m;
      }
      if(msg == WORST) Error("msg is WORST in exact Inference.");
      if(DEBUG5) printf("exact: d 1, e %d, to %d, msg: %f\n",jidx,i,msg);
      int msg_i = NJOINTS+jidx+i*2*NJOINTS;
      if(msg_i<0 || (USE_SYMMETRY && msg_i>=2*E*NHYPS) ||
          (!USE_SYMMETRY && msg_i>=2*NJOINTS*NHYPS)){
        printf("msg_i is bad: %d. USE_SYMMETRY: %d, NJOINTS %d, E %d, NHYPS %d\n",
            msg_i, USE_SYMMETRY, NJOINTS, E, NHYPS);
      }
      factor[to][i] += msg;
    }
  }
  free(fwdmsg);
  //computeHypotheses(factor, hids);
  //printHids(hids);
  //exit(0);
}
void validateModel( Appmodel *m){
  for(int i=0; i<NUM_COLOR_BUCKETS; i++){
    for(int t=0; t<NTEXTURES; t++){
      assert(m->t[t][i] >= APP_MODEL_PRIOR);
    }
    for(int j=0; j<NUM_COLOR_BUCKETS; j++){
      for(int k=0; k<NUM_COLOR_BUCKETS; k++){
        assert(m->m[i][j][k] >= APP_MODEL_PRIOR);
      }
    }
  }
}
void addLossToSingleton(vector<vector<double> > &factor, PATTERN *x, LOSS *loss){
  for(int pid=0; pid<NPARTS; pid++){
    for(int hid=0; hid<NHYPS; hid++){
      factor[pid][hid] =
        getSingletonEnergy(pid, x->singleton[pid][hid]);
      factor[pid][hid] += loss->l[pid][hid];
      //printf("%lf, ", factor[pid][hid]);
    }
    //printf("\n");
  }
}
void setFactors(vector<vector<double> > &factor, PATTERN *x, LOSS *loss, Appmodel *fg,
    Appmodel *bg, int iter, struct buckets *allbuckets, double *nlogn){
  addLossToSingleton( factor, x, loss);
  //printf("FACTOR:\n");
  if(iter == 0) return; //since we dont have fg and bg models yet.
  if(DEF_APPEARANCE_WEIGHT == 0.0) return;

  int *mask = (int*)calloc(IMGSIZE,sizeof(int));
  initModel(fg);
  initModel(bg);
  int hids[NPARTS];
  for(int pid=0; pid<NPARTS; pid++) hids[pid]=1;
  computeFgAppearnceNoBlur(fg, x, mask, hids, allbuckets);
  computeBgAppearnceNoBlur(bg, x, mask, allbuckets);
  validateModel( fg);
  validateModel( bg);
  for(int pid=0; pid<NPARTS; pid++){
    for(int hid=0; hid<NHYPS; hid++){
      if(hid==0)
        transferAppearnceNoBlur( fg, bg, x, mask, pid, hids[pid]-1, 0, allbuckets);
      else
        transferAppearnceNoBlur( fg, bg, x, mask, pid, hid-1, hid, allbuckets);
      validateModel( fg);
      validateModel( bg);
      setLABChannels( fg);
      setLABChannels( bg);
      int fgtotal = getModelTotal( (int*)fg->m);
      int bgtotal = getModelTotal( (int*)bg->m);
      double appEnergy = getAppearanceEnergy( 0,
            computeU7( (int*)fg->m, (int*)bg->m, fgtotal, bgtotal, nlogn));
      for(int c=0; c<NCOLOR-1; c++){
        appEnergy += getAppearanceEnergy( c+1,
            computeULAB2( fg->c[c], bg->c[c], nlogn));
      }
      for(int t=0; t<NTEXTURES; t++){
        appEnergy += getAppearanceEnergy( t+NCOLOR,
            computeULAB2( fg->t[t], bg->t[t], nlogn));
      }
      if(appEnergy != appEnergy){ // checks if it's nan
        Error("log likelihood is NaN.");
      }
      factor[pid][hid] += appEnergy;
      /*
      if(DEBUG11){
        printf("pid: %d, hid: %d, singleton: %f, app: %f\n",
          pid, hid, getSingletonFeature(pid, x->singleton[pid][hid]),
          getAppearanceFeature(i, app));
      }
      */
    }
    transferAppearnceNoBlur( fg, bg, x, mask, pid, NHYPS-1, hids[pid]-1, allbuckets);
    //printf("\n");
  }
  free(mask);
  mask = (int*)calloc(IMGSIZE,sizeof(int));
  computeFgAppearnceNoBlur(fg, x, mask, hids, allbuckets);
  computeBgAppearnceNoBlur(bg, x, mask, allbuckets);
  free(mask);
}
double blockHillClimb(vector<vector<double> > &factor, PATTERN *x,
    int *hids, STRUCT_LEARN_PARM *param, CONFIGS *config, LOSS *loss,
    struct block *b, double *nlogn){
  //printf("Starting block hill climb.\n");
  double final_energy = WORST;
  bool improved = true;
  while(improved){
    double prevenergy = final_energy;
    improved = false;
    for(int pi=0; pi<ABCD_BLOCK_SIZE; pi++){
      int p = b->p[pi];
      if(p<0 || p>=NPARTS){
        printf("abcd block size: %d, psize: %lu, p: %d, pi: %d\n",
            ABCD_BLOCK_SIZE, b->p.size(), p, pi);
        Error("pid out of bounds in blockHillClimb");
      }
      int best_h = -1;
      double best_energy = WORST;
      // hids is 1 indexed
      for(int h=1; h<=NHYPS; h++){
        hids[p] = h;
        double energy = calculateEnergy(x, hids, param, NULL, NULL,
            nlogn);
        // add loss
        for(int i=0; i<NPARTS; i++)
          energy += loss->l[i][hids[i]-1];

        //printf("energy for p %d, h %d: %f\n", p, h, energy);
        if(isBetter(best_energy, energy)){
          //printf("best_energy: %lf, energy: %lf\n", best_energy, energy);
        //if(energy < best_energy){
          best_energy = energy;
          best_h = h;
        }
      }
      if(best_h <= 0) Error("Problem in blockHillClimb.");
      hids[p] = best_h;
      if(isBetter(final_energy, best_energy)){
        if(0) printf("changing pid %d to hypothesis %d, with energy %lf\n",
            p+1, best_h, best_energy);
      }
      final_energy = best_energy;
    }
    if(isBetter(prevenergy, final_energy)){
      improved = true;
    }
  }
  return final_energy;
}
void printPartEnergies( int imgnum, int part,
    vector<vector<double> > &factor, PATTERN *x,
    int *hids, STRUCT_LEARN_PARM *param, CONFIGS *config, LOSS *loss,
    double *nlogn){
  if(APPD) printf("img: %d\n", x->imgnum);
  if(x->imgnum != imgnum) return;
  for(int p=0; p<NPARTS; p++){
    if(part >= 0  && p!=part) continue;
    int old_h = hids[p];
    // hids is 1 indexed
    for(int h=1; h<=NHYPS; h++){
      hids[p] = h;
      RetEnergy re;
      double energy = calculateEnergy(x, hids, param, config, NULL,
          nlogn, &re);
      // add loss
      for(int i=0; i<NPARTS; i++)
        energy += loss->l[i][hids[i]-1];
      if(APPD) printf("energy for p %d, h %d: %f\n", p, h, energy);
    }
    hids[p] = old_h;
  }
}
double hillClimb(vector<vector<double> > &factor, PATTERN *x,
    int *hids, STRUCT_LEARN_PARM *param, CONFIGS *config, LOSS *loss,
    double *nlogn = NULL);
double hillClimb(vector<vector<double> > &factor, PATTERN *x,
    int *hids, STRUCT_LEARN_PARM *param, CONFIGS *config, LOSS *loss,
    double *nlogn){
  //printHids(hids);
  double final_energy = WORST;
  bool improved = true;
  while(improved){
    double prevenergy = final_energy;
    improved = false;
    for(int p=0; p<NPARTS; p++){
      int best_h = -1;
      double best_energy = WORST;
      // hids is 1 indexed
      for(int h=1; h<=NHYPS; h++){
        hids[p] = h;
        double energy = calculateEnergy(x, hids, param, config, NULL,
            nlogn);
        // add loss
        for(int i=0; i<NPARTS; i++)
          energy += loss->l[i][hids[i]-1];
        //printf("energy for p %d, h %d: %f\n", p, h, energy);
        if(isBetter(best_energy, energy)){
          //printf("best_energy: %lf, energy: %lf\n", best_energy, energy);
          best_energy = energy;
          best_h = h;
        }
      }
      if(best_h <= 0) Error("Problem in hillClimb.");
      hids[p] = best_h;
      if(isBetter(final_energy, best_energy)){
        if(0) printf("changing pid %d to hypothesis %d, with energy %lf\n",
            p+1, best_h, best_energy);
      }
      final_energy = best_energy;
    }
    if(isBetter(prevenergy, final_energy)){
      improved = true;
    }
  }
  //printHids(hids);
  return final_energy;
}
double globalCheck(vector<vector<double> > &factor, PATTERN *x,
    int *hids, STRUCT_LEARN_PARM *param, CONFIGS *config, LOSS *loss,
    double *nlogn){
  int besth[NPARTS];
  long N = (long)pow(NHYPS,NPARTS);
  for(int i=0; i<NPARTS; i++) hids[i] = 1;
  double best_energy = WORST;
  for(long i=0; i<N; i++){
    if(i%100000000 == 0) printf("%ld\n", i);
    double energy = calculateEnergy(x, hids, param, NULL, NULL, nlogn);
    // add loss
    for(int i=0; i<NPARTS; i++)
      energy += loss->l[i][hids[i]-1];
    if(isBetter(best_energy, energy)){
      printf("best_energy: %lf, energy: %lf\n", best_energy, energy);
      best_energy = energy;
      for(int j=0; j<NPARTS; j++) besth[j] = hids[j];
    }
    hids[NPARTS-1]++;
    int k=NPARTS-1;
    while(hids[k] > NPARTS){
      hids[k] = 1;
      hids[k-1]++;
      k--;
    }
    //printHids(hids);
  }
  for(int j=0; j<NPARTS; j++) hids[j] = besth[j];
  return best_energy;
}
double hillClimb3(PATTERN *x,
    int *hids, STRUCT_LEARN_PARM *param, LOSS *loss, double *nlogn){
  int oldhids[NPARTS];
  for(int i=0; i<NPARTS; i++) oldhids[i] = hids[i];
  double bestenergy = calculateEnergy(x, hids, param, NULL, NULL, nlogn);
  bestenergy += getLoss(loss, hids);
  for(int p1=0; p1<NPARTS; p1++){
  for(int h1=1; h1<=NHYPS; h1++){
    hids[p1] = h1;
  for(int p2=p1+1; p2<NPARTS; p2++){
  for(int h2=1; h2<=NHYPS; h2++){
    hids[p2] = h2;
  for(int p3=p2+1; p3<NPARTS; p3++){
  for(int h3=1; h3<=NHYPS; h3++){
    hids[p3] = h3;
    //printHids(hids);
    double energy = calculateEnergy(x, hids, param, NULL,
        NULL, nlogn);
    // add loss
    energy += getLoss(loss, hids);
    if(isBetter(bestenergy, energy)){
      if(fabs(bestenergy-energy) < FLT_EPSILON){
        energy = bestenergy;
      }else{
        printf("bestenergy: %lf energy: %lf\n", bestenergy, energy);
        Error("Problem in hillClimb3.");
      }
    }
  }
  hids[p3] = oldhids[p3];
  }}
  hids[p2] = oldhids[p2];
  }}
  hids[p1] = oldhids[p1];
  }
  return bestenergy;
}
static int hash(const int *s, int n){
  unsigned long hashcode = 0;
  for (int i=0; i<n; i++)
    hashcode = hashcode * -1664117991L + s[i];
  return hashcode;
}
void logEnergies(vector<double> &energies, vector<int64_t> &times,
    PATTERN *x, CONFIGS *config, STRUCT_LEARN_PARM *param){
  if(!config->writeLog) return;
  assert(times.size() == energies.size());
  stringstream ss (stringstream::in | stringstream::out);
  ss << param->logname;
  ss << NHYPS;
  if(NAIVE_INIT) ss << "n"; // n for naive
  ss << "_" << ABCD_BLOCK_SIZE << "/log_" << x->imgnum << ".txt";
  //cout << ss.str() << endl;
  FILE *fp = fopen( ss.str().c_str(), "w");
  if(!fp){
    cout << "Couldnt open: "<< ss.str() << endl;
    exit(0);
  }
  for(unsigned int i=0; i<energies.size(); i++)
    fprintf(fp, "%lf %ld\n", energies[i], times[i]);
  fclose(fp);
}
void doInference(vector<vector<double> > &factor, PATTERN *x, STRUCT_LEARN_PARM *param,
    int *edges, int *hids, int *degree,
    Appmodel *fg, Appmodel *bg,
    double *nlogn){
  if(PLOT_INFERENCE){
    Error("dont use calculateAppModels right now");
    //calculateAppModelsNoBlur(fg, bg, x, hids);
  }
  if(USE_SYMMETRY || APPROX_INFERENCE){
    //Error("dont use approxInference right now");
    approxInference(factor, x, param->joints,
        edges, hids, degree, fg, bg, param, nlogn);
  } else {
    exactInference(factor, x, param->joints, hids);
  }
}
void inferenceEM(PATTERN *x, LOSS *loss, STRUCT_LEARN_PARM *param,
    CONFIGS *config, int *hids, FEATURES *features, int *edges,
    struct buckets *allbuckets, double *nlogn){
  for(int i=0; i<NPARTS; i++) hids[i] = 1;

  // Get pointers to the outputs
  vector<vector<double> > factor;
  factor.assign( NPARTS, vector<double>(NHYPS, 0.0));
  //double (*factor)[NHYPS] =
  //  (double (*)[NHYPS]) malloc(sizeof(double)*NPARTS*NHYPS);

  // for background and foreground models
  Appmodel fg,bg;
  //initModel(&fg);
  //initModel(&bg);

  int degree[NPARTS]; // the degree of each body part in the body graph
  for(int i=0;i<NPARTS;i++) degree[i] = 0;
  for(int jidx=0; jidx<NJOINTS; jidx++){
    int from = param->joints[jidx].j1-1;
    int to   = param->joints[jidx].j2-1;
    if(from<0 || from>=NPARTS || to<0 || to>=NPARTS){
      Error("from or to is wrong here. Should be 0 indexed.");
      printf("from %d, to %d\n", from, to);
    }
    degree[from]++;
    degree[to]++;
  }

  int EM_ITERS = config->em_iters;
  // To tell when EM converged
  int visited[EM_ITERS];

  //printHids(hids);
  vector<double> energies;
  vector<int64_t> times;
  int64_t t1 = GetTimeInMicrosecs();
  int iter;
  for(iter=0; iter<EM_ITERS; iter++){
    if(INFERENCE_METHOD==1){
      double energy = calculateEnergy(x, hids, param, config,
          features, nlogn);
      energies.push_back(energy);
      times.push_back(GetTimeInMicrosecs() - t1);
    }
    // copy the marginal from the singleton
    // and add in the log likelihood from the appearance model
    setFactors(factor, x, loss, &fg, &bg, iter, allbuckets, nlogn);

    // Inference
    int64_t t1 = GetTimeInMicrosecs();
    doInference(factor, x, param, edges,
        hids, degree, &fg, &bg, nlogn);
    int64_t t2 = GetTimeInMicrosecs();
    if(0) printf("doInference time: %ld\n", t2-t1);

    computeHypotheses(factor, hids);
    //printHids(hids);
    if(config->sample_learning_features){
      FEATURES features;
      calculateEnergy(x, hids, param, NULL, &features,
          nlogn);
      SVECTOR *vec = convertFeaturesToSVEC( &features);
      double l = getLoss(x->y, hids);
      storeFeature(vec, l);
      freeSvec(vec);
    }

    if(HILLCLIMB && iter>0){
      //double energy = hillClimb(factor, x, hids, param, config, loss);
    }

    if(iter >= EM_ITERS) break;
    //If the predicted hypothesis ids dont change then stop
    int key = hash(hids, NPARTS);
    int shouldexit = 0;
    for(int i=0; i<iter; i++){
      //printf("i: %d, visited[i]: %d, key: %d\n", i, visited[i], key);
      if(key == visited[i]){
        shouldexit = 1;
        break;
      }
    }
    if(shouldexit) break;
    visited[iter] = key;
    // compute the appearance model given the parts.
    // mask is the percentage that each pixel is in the foreground
    //calculateAppModelsNoBlur(&fg, &bg, x, hids, allbuckets);
    if(0) printf("DONE WITH ITER %d\n",iter);
  }
  if(INFERENCE_METHOD==1){
    double energy = calculateEnergy(x, hids, param, config,
        features, nlogn);
    energies.push_back(energy);
    times.push_back(GetTimeInMicrosecs() - t1);
    logEnergies(energies, times, x, config, param);
  }

  //printHids(hids);
  //printf("TOOK %d ITERATIONS\n", iter+1);
  if(VALIDATE_INFERENCE){
    double energy = calculateEnergy(x, hids, param, config,
        features, nlogn);
    int oldhids[NPARTS];
    for(int i=0; i<NPARTS; i++) oldhids[i] = hids[i];
    double hcenergy = hillClimb(factor, x, hids, param, config, loss);
    for(int i=0; i<NPARTS; i++){
      if(oldhids[i] != hids[i]){
        for(int j=0; j<NPARTS; j++){
          printf("old: %3d, new: %3d\n", oldhids[j], hids[j]);
        }
        printf("energy: %lf, hcenergy: %lf\n",
            energy+getLoss(loss, oldhids), hcenergy);
        Error("hillclimb improved energy.");
      }
    }
    if(fabs(energy)>1.0){

      printf("Starting globalCheck: energy: %lf\n", energy);
      /*
      double genergy = globalCheck(factor, x, hids, param, config, loss);
      for(int i=0; i<NPARTS; i++){
        if(oldhids[i] != hids[i]){
          for(int j=0; j<NPARTS; j++){
            printf("old: %d, new: %d\n", oldhids[i], hids[i]);
          }
          printf("energy: %lf, genergy: %lf\n", energy, genergy);
          Error("globalCheck improved energy.");
        }
      }*/

    }
    double hc3energy = hillClimb3(x, hids, param, loss, nlogn);
    for(int i=0; i<NPARTS; i++){
      if(oldhids[i] != hids[i]){
        for(int j=0; j<NPARTS; j++){
          printf("old: %3d, new: %3d\n", oldhids[j], hids[j]);
        }
        printf("energy: %lf, hc3energy: %lf\n", energy, hc3energy);
        Error("hillClimb3 improved energy.");
      }
    }
  }
  //free(factor);
}
void chooseABCDblock( struct block *b){
  for(int i=0; i<NPARTS; i++){
    b->b[i] = false;
    b->i[i] = -1;
  }
  b->p.assign(ABCD_BLOCK_SIZE, -1);
  for(int i=0; i<ABCD_BLOCK_SIZE; i++){
    int r;
    bool repeat = true;
    while(repeat){
      repeat = false;
      r = rand()%NPARTS;
      if(b->b[r]) repeat = true;
    }
    b->b[r] = true;
    b->p[i] = r;
  }
  sort(b->p.begin(), b->p.end());
  for(int i=1; i<ABCD_BLOCK_SIZE; i++){
    if(b->p[i] <= b->p[i-1])
      Error("sort b->p and make it a vector, and make ABCD_BLOCK_SIZE a const int.");
  }
  for(int i=0; i<ABCD_BLOCK_SIZE; i++) b->i[b->p[i]] = i;
}
// Move app weight from FROM to TO.
/*
void transferToFG( Appmodel *fg, Appmodel *bg, Appmodel *m, int *mask){
  for(int i=0; i<NUM_COLOR_BUCKETS; i++){
    for(int j=0; j<NUM_COLOR_BUCKETS; j++){
      for(int k=0; k<NUM_COLOR_BUCKETS; k++){
        //TODO have to check that mask is not set.
        Error("transferToFG not done");
        if(m->m[i][j][k] < 0.0)
          Error("m is negative.");
        if(m->m[i][j][k] > bg->m[i][j][k]){
          printf("ijk: %d %d %d\n", i,j,k);
          printf("m: %lf, bg: %lf\n", m->m[i][j][k], bg->m[i][j][k]);
          Error("m greater than 'bg'");
        }
        bg->m[i][j][k] -= m->m[i][j][k];
        fg->m[i][j][k] += m->m[i][j][k];
      }
    }
  }
}
*/
void transferAppModels( Appmodel *to, Appmodel *from,
    Appmodel appmodels[NPARTS][NHYPS], int p, int h,
    int totals[NPARTS][NHYPS], int *to_total, int *from_total){
  Appmodel *m = appmodels[p]+h;
  for(int i=0; i<NUM_COLOR_BUCKETS; i++){
    for(int j=0; j<NUM_COLOR_BUCKETS; j++){
      for(int k=0; k<NUM_COLOR_BUCKETS; k++){
        /*
        if(m->m[i][j][k] < 0.0)
          Error("m is negative.");
        if(m->m[i][j][k] > from->m[i][j][k]){
          printf("ijk: %d %d %d\n", i,j,k);
          printf("m: %lf, from: %lf\n", m->m[i][j][k], from->m[i][j][k]);
          Error("m greater than 'from'");
        }
        */
        //TODO the totals dont get adjusted by this MAX.
        // shouldnt really matter tho.
        from->m[i][j][k] =
          MAX(APP_MODEL_PRIOR, from->m[i][j][k] - m->m[i][j][k]);
        to->m[i][j][k] += m->m[i][j][k];
      }
    }
  }
  if(totals){
    *to_total += totals[p][h];
    *from_total -= totals[p][h];
  }
}
void addAppModels( Appmodel *dest, Appmodel *a, Appmodel *b){
  for(int i=0; i<NUM_COLOR_BUCKETS; i++){
    for(int j=0; j<NUM_COLOR_BUCKETS; j++){
      for(int k=0; k<NUM_COLOR_BUCKETS; k++){
        dest->m[i][j][k] = a->m[i][j][k] + b->m[i][j][k];
      }
    }
  }
}
void subtractAppModels( Appmodel *dest, Appmodel *a, Appmodel *b){
  for(int i=0; i<NUM_COLOR_BUCKETS; i++){
    for(int j=0; j<NUM_COLOR_BUCKETS; j++){
      for(int k=0; k<NUM_COLOR_BUCKETS; k++){
        dest->m[i][j][k] =
          MAX(APP_MODEL_PRIOR, a->m[i][j][k] - b->m[i][j][k]);
      }
    }
  }
}
void changeAppModels( Appmodel *sum, Appmodel *a, Appmodel *diff, Appmodel *b,
    Appmodel *other){
  for(int i=0; i<NUM_COLOR_BUCKETS; i++){
    for(int j=0; j<NUM_COLOR_BUCKETS; j++){
      for(int k=0; k<NUM_COLOR_BUCKETS; k++){
        sum->m[i][j][k] = a->m[i][j][k] + other->m[i][j][k];
        diff->m[i][j][k] = b->m[i][j][k] - other->m[i][j][k];
      }
    }
  }
}
void checkBlock(struct block *b){
  for(int pi=0; pi<ABCD_BLOCK_SIZE; pi++){
    int p = b->p[pi];
    if(p<0 || p>=NPARTS){
      printf("abcd block size: %d, psize: %lu, p: %d\n",
          ABCD_BLOCK_SIZE, b->p.size(), p);
      Error("pid out of bounds in checkBlock");
    }
  }
}
void printBlock(struct block *b){
  printf("Block: ");
  for(int i=0; i<NPARTS; i++) printf("%d ", b->b[i]);
  printf("\n");
  checkBlock(b);
}
void inline appCopy(Appmodel *dest, Appmodel *src){
  memcpy(dest->m, src->m, sizeof(struct appmodel));
  /*
  Appmodel test;
  for(int i=0; i<NUM_COLOR_BUCKETS; i++){
    for(int j=0; j<NUM_COLOR_BUCKETS; j++){
      for(int k=0; k<NUM_COLOR_BUCKETS; k++){
        dest->m[i][j][k] = src->m[i][j][k];
      }
    }
  }
  //memcpy(test.m, src->m, sizeof(struct appmodel));
  for(int i=0; i<NUM_COLOR_BUCKETS; i++){
    for(int j=0; j<NUM_COLOR_BUCKETS; j++){
      for(int k=0; k<NUM_COLOR_BUCKETS; k++){
        if(dest->m[i][j][k] != test.m[i][j][k]) Error("uh.");
        //printf("%lf %lf %lf\n", dest->m[i][j][k], src->m[i][j][k], test.m[i][j][k]);
      }
    }
  }*/
}
/*
// returns the new value of nlogs
typedef struct threadStuff {
  int p1,p2,h1,nlogs;
  Appmodel (*appmodels)[NHYPS];
  int (*totals)[NHYPS];
  double *logs;
  Appmodel *fg, *bg;
  int fgtotal, bgtotal;
  double (*blockpairs)[NPARTS][NHYPS][NHYPS];
  bool *done;
  pthread_mutex_t getNextLock;
} ThreadStuff;
typedef struct threadStuff2 {
  int p1,p2,nlogs;
  Appmodel (*appmodels)[NHYPS];
  int (*totals)[NHYPS];
  double *logs;
  Appmodel *fg, *bg;
  int fgtotal, bgtotal;
  double (*blockpairs)[NPARTS][NHYPS][NHYPS];
  bool *done;
  pthread_mutex_t getNextLock;
} ThreadStuff2;
typedef struct threadStuff3 {
  int p1,p2,h1,nlogs;
  Appmodel (*appmodels)[NHYPS];
  int (*totals)[NHYPS];
  double *logs;
  Appmodel *fg, *bg;
  int fgtotal, bgtotal;
  double (*blockpairs)[NPARTS][NHYPS][NHYPS];
  bool *done;
  pthread_mutex_t getNextLock;
  // Used for signalling that the thread is done with all the h2s for
  // a particular h1.
  pthread_mutex_t doneLock;
  pthread_cond_t threadDoneCond;
  pthread_cond_t mainDoneCond;
  int ndone, maindone, hasmore;
} ThreadStuff3;
void *threadModelDivergence(void *ptr){
  // unpack thread stuff
  ThreadStuff *ts = (ThreadStuff*)ptr;
  int p1 = ts->p1;
  int p2 = ts->p2;
  int h1 = ts->h1;
  Appmodel (*appmodels)[NHYPS] = ts->appmodels;
  int (*totals)[NHYPS] = ts->totals;
  double *logs = ts->logs;
  int nlogs = ts->nlogs;
  Appmodel *fg = ts->fg;
  Appmodel *bg = ts->bg;
  int fgtotal = ts->fgtotal;
  int bgtotal = ts->bgtotal;
  double (*blockpairs)[NPARTS][NHYPS][NHYPS] = ts->blockpairs;
  bool *done = ts->done;

  //for(int h2=0; h2<NHYPS; h2++){
  int h2 = 0;
  while(1){
    pthread_mutex_lock(&ts->getNextLock);
    while(h2<NHYPS && done[h2]) h2++;
    done[h2] = 1;
    pthread_mutex_unlock(&ts->getNextLock);
    if(h2 >= NHYPS){
      break;
    }
    Appmodel fgh2, bgh2;
    addAppModels( &fgh2, fg, appmodels[p2]+h2);
    subtractAppModels( &bgh2, bg, appmodels[p2]+h2);
    int fgth2 = fgtotal + totals[p2][h2];
    int bgth2 = bgtotal - totals[p2][h2];

    blockpairs[p1][p2][h1][h2] =
      computeModelDivergence5( &fgh2, &bgh2, fgth2, bgth2, logs, nlogs);
    blockpairs[p2][p1][h2][h1] = blockpairs[p1][p2][h1][h2];
    int TEST = 0;
    if(TEST){
      double bb=computeModelDivergence2( &fgh2, &bgh2, fgth2, bgth2);
      double cc=computeModelDivergence3( &fgh2, &bgh2, fgth2, bgth2);
      double ee=computeModelDivergence5( &fgh2, &bgh2, fgth2, bgth2, logs, nlogs);
      if(fabs(cc - bb)>FLT_EPSILON) Error("c\n");
      if(fabs(ee - bb)>FLT_EPSILON) Error("e\n");
    }
  }
  return NULL;
}
void *threadModelDivergence2(void *ptr){
  // unpack thread stuff
  ThreadStuff2 *ts = (ThreadStuff2*)ptr;
  int p1 = ts->p1;
  int p2 = ts->p2;
  Appmodel (*appmodels)[NHYPS] = ts->appmodels;
  int (*totals)[NHYPS] = ts->totals;
  double *logs = ts->logs;
  int nlogs = ts->nlogs;
  Appmodel *fg = ts->fg;
  Appmodel *bg = ts->bg;
  int fgtotal = ts->fgtotal;
  int bgtotal = ts->bgtotal;
  double (*blockpairs)[NPARTS][NHYPS][NHYPS] = ts->blockpairs;
  bool *done = ts->done;
  // save the apps
  Appmodel fgh1, bgh1;
  appCopy( &fgh1, fg);
  appCopy( &bgh1, bg);
  int fgth1 = fgtotal;
  int bgth1 = bgtotal;

  int h1 = 0;
  while(1){
    pthread_mutex_lock(&ts->getNextLock);
    while(h1<NHYPS && done[h1]) h1++;
    done[h1] = 1;
    pthread_mutex_unlock(&ts->getNextLock);
    if(h1 >= NHYPS){
      break;
    }
    transferAppModels( &fgh1, &bgh1, appmodels, p1, h1, totals, &fgth1, &bgth1);
    for(int h2=0; h2<NHYPS; h2++){
      Appmodel fgh2, bgh2;
      addAppModels( &fgh2, &fgh1, appmodels[p2]+h2);
      subtractAppModels( &bgh2, &bgh1, appmodels[p2]+h2);
      int fgth2 = fgth1 + totals[p2][h2];
      int bgth2 = bgth1 - totals[p2][h2];

      blockpairs[p1][p2][h1][h2] =
        computeModelDivergence5( &fgh2, &bgh2, fgth2, bgth2, logs, nlogs);
      blockpairs[p2][p1][h2][h1] = blockpairs[p1][p2][h1][h2];
      int TEST = 0;
      if(TEST){
        double bb=computeModelDivergence2( &fgh2, &bgh2, fgth2, bgth2);
        double cc=computeModelDivergence3( &fgh2, &bgh2, fgth2, bgth2);
        double ee=computeModelDivergence5( &fgh2, &bgh2, fgth2, bgth2, logs, nlogs);
        if(fabs(cc - bb)>FLT_EPSILON) Error("c\n");
        if(fabs(ee - bb)>FLT_EPSILON) Error("e\n");
      }
    }
    // reset the apps
    appCopy( &fgh1, fg);
    appCopy( &bgh1, bg);
    fgth1 = fgtotal;
    bgth1 = bgtotal;
  }
  return NULL;
}
void *threadModelDivergence3(void *ptr){
  ThreadStuff3 *ts = (ThreadStuff3*)ptr;
  Appmodel (*appmodels)[NHYPS] = ts->appmodels;
  int (*totals)[NHYPS] = ts->totals;
  double *logs = ts->logs;
  Appmodel *fg = ts->fg;
  Appmodel *bg = ts->bg;
  double (*blockpairs)[NPARTS][NHYPS][NHYPS] = ts->blockpairs;
  bool *done = ts->done;
  int DEBUGT3 = 0;
  if(DEBUGT3) printf("a\n");
  // start off waiting
  pthread_mutex_lock(&ts->doneLock);
  while(!ts->maindone)
    pthread_cond_wait(&ts->threadDoneCond, &ts->doneLock);
  pthread_mutex_unlock(&ts->doneLock);
  while(ts->hasmore){
    if(DEBUGT3) printf("b\n");
    // unpack thread stuff
    int p1 = ts->p1;
    int p2 = ts->p2;
    int h1 = ts->h1;
    int nlogs = ts->nlogs;
    int fgtotal = ts->fgtotal;
    int bgtotal = ts->bgtotal;

    //for(int h2=0; h2<NHYPS; h2++){
    int h2 = 0;
    while(1){
      pthread_mutex_lock(&ts->getNextLock);
      while(h2<NHYPS && done[h2]) h2++;
      done[h2] = 1;
      pthread_mutex_unlock(&ts->getNextLock);
      if(h2 >= NHYPS){
        break;
      }
      Appmodel fgh2, bgh2;
      addAppModels( &fgh2, fg, appmodels[p2]+h2);
      subtractAppModels( &bgh2, bg, appmodels[p2]+h2);
      int fgth2 = fgtotal + totals[p2][h2];
      int bgth2 = bgtotal - totals[p2][h2];

      blockpairs[p1][p2][h1][h2] =
        computeModelDivergence5( &fgh2, &bgh2, fgth2, bgth2, logs, nlogs);
      blockpairs[p2][p1][h2][h1] = blockpairs[p1][p2][h1][h2];
      int TEST = 0;
      if(TEST){
        double bb=computeModelDivergence2( &fgh2, &bgh2, fgth2, bgth2);
        double cc=computeModelDivergence3( &fgh2, &bgh2, fgth2, bgth2);
        double ee=computeModelDivergence5( &fgh2, &bgh2, fgth2, bgth2, logs, nlogs);
        if(fabs(cc - bb)>FLT_EPSILON) Error("c\n");
        if(fabs(ee - bb)>FLT_EPSILON) Error("e\n");
      }
    }
    //printf("%d %d %d %d\n", p1, p2, h1, h2);
    if(DEBUGT3) printf("c\n");
    pthread_mutex_lock(&ts->doneLock);
    ts->ndone++;
    if(ts->ndone == NTHREADS){
      pthread_cond_signal(&ts->mainDoneCond);
    }
    pthread_cond_wait(&ts->threadDoneCond, &ts->doneLock);
    pthread_mutex_unlock(&ts->doneLock);
    if(DEBUGT3) printf("d\n");
  }
  if(DEBUGT3) printf("e\n");
  return NULL;
}
*/
/*
// returns the new value of nlogs
int setBlockPairwiseEdges2( int isset[NPARTS][NPARTS],
    double blockpairs[NPARTS][NPARTS][NHYPS][NHYPS],
    Appmodel appmodels[NPARTS][NHYPS], struct block *b,
    int totals[NPARTS][NHYPS], Appmodel *fg, Appmodel *bg, int *hids,
    double *logs, int nlogs){
  int fgtotal = getModelTotal( fg);
  int bgtotal = getModelTotal( bg);
  int DEBUG_EDGES = 0;
  // save the apps
  Appmodel fgp2, bgp2;
  appCopy( &fgp2, fg);
  appCopy( &bgp2, bg);
  int fgtp2 = fgtotal;
  int bgtp2 = bgtotal;
  printf("%d %d\n", fgtotal, bgtotal);
  //
  for(int p1=0; p1<NPARTS; p1++){
    if(!b->b[p1]) continue;
    if(DEBUG_EDGES) printf("P1: %d\n", p1);
    for(int p2=p1+1; p2<NPARTS; p2++){
      if(!b->b[p2]) continue;
      if(DEBUG_EDGES) printf("  P2: %d\n", p2);
      if(isset[p1][p2]) continue;
      if(DEBUG_EDGES) printBlock(b);
      // move weights from other parts in the block
      for(int i=0; i<NPARTS; i++){
        if(!b->b[i] || i==p1 || i==p2) continue;
        int hid = hids[i]-1;
        if(DEBUG_EDGES) printf("transfer: %d %d\n", i,hid);
        transferAppModels(fg, bg, appmodels, i, hid, totals, &fgtotal, &bgtotal);
      }
      // save the apps
      Appmodel fgh1, bgh1;
      appCopy( &fgh1, fg);
      appCopy( &bgh1, bg);
      int fgth1 = fgtotal;
      int bgth1 = bgtotal;

      int maxtotal = 0;
      int mintotal = INT_MAX;
      for(int h2=0; h2<NHYPS; h2++){
        maxtotal = MAX(maxtotal, totals[p2][h2]);
        mintotal = MIN(mintotal, totals[p2][h2]);
      }
      for(int h1=0; h1<NHYPS; h1++){
        if(DEBUG_EDGES) printf("    H1: %d\n", h1);
        transferAppModels(fg, bg, appmodels, p1, h1, totals, &fgtotal, &bgtotal);
        // must set logs before the threading
        setLogs(logs, &nlogs, max(fgtotal+maxtotal, bgtotal-mintotal));

        pthread_t threads[NTHREADS];
        ThreadStuff *ts = (ThreadStuff*) malloc(sizeof(ThreadStuff));
        ts->p1 = p1;
        ts->p2 = p2;
        ts->h1 = h1;
        ts->appmodels = appmodels;
        ts->totals = totals;
        ts->logs = logs;
        ts->nlogs = nlogs;
        ts->fg = fg;
        ts->bg = bg;
        ts->fgtotal = fgtotal;
        ts->bgtotal = bgtotal;
        ts->blockpairs = blockpairs;
        ts->done = (bool*) calloc(sizeof(bool), NHYPS+1);
        pthread_mutex_init(&ts->getNextLock, NULL);
        for(int t=0;t<NTHREADS;t++)
          pthread_create(threads+t, NULL, threadModelDivergence, (void*) ts);
        for(int t=0;t<NTHREADS;t++)
          pthread_join(threads[t], NULL);
        pthread_mutex_destroy(&ts->getNextLock);
        free(ts->done);
        free(ts);
        // reset the apps
        appCopy( fg, &fgh1);
        appCopy( bg, &bgh1);
        fgtotal = fgth1;
        bgtotal = bgth1;
        if(DEBUG_EDGES) printf("    H1 done\n");
      }
      // reset the apps
      appCopy( fg, &fgp2);
      appCopy( bg, &bgp2);
      fgtotal = fgtp2;
      bgtotal = bgtp2;
      isset[p1][p2] = 1;
    }
  }
  return nlogs;
}
// returns the new value of nlogs
int setBlockPairwiseEdges3( int isset[NPARTS][NPARTS],
    double blockpairs[NPARTS][NPARTS][NHYPS][NHYPS],
    Appmodel appmodels[NPARTS][NHYPS], struct block *b,
    int totals[NPARTS][NHYPS], Appmodel *fg, Appmodel *bg, int *hids,
    double *logs, int nlogs){
  int fgtotal = getModelTotal( fg);
  int bgtotal = getModelTotal( bg);
  int DEBUG_EDGES = 0;
  // save the apps
  Appmodel fgp2, bgp2;
  appCopy( &fgp2, fg);
  appCopy( &bgp2, bg);
  int fgtp2 = fgtotal;
  int bgtp2 = bgtotal;
  printf("%d %d\n", fgtotal, bgtotal);
  //
  for(int p1=0; p1<NPARTS; p1++){
    if(!b->b[p1]) continue;
    if(DEBUG_EDGES) printf("P1: %d\n", p1);
    int maxtotalh1 = 0;
    int mintotalh1 = INT_MAX;
    for(int h1=0; h1<NHYPS; h1++){
      maxtotalh1 = MAX(maxtotalh1, totals[p1][h1]);
      mintotalh1 = MIN(mintotalh1, totals[p1][h1]);
    }
    for(int p2=p1+1; p2<NPARTS; p2++){
      if(!b->b[p2]) continue;
      if(DEBUG_EDGES) printf("  P2: %d\n", p2);
      if(isset[p1][p2]) continue;
      if(DEBUG_EDGES) printBlock(b);
      // move weights from other parts in the block
      for(int i=0; i<NPARTS; i++){
        if(!b->b[i] || i==p1 || i==p2) continue;
        int hid = hids[i]-1;
        if(DEBUG_EDGES) printf("transfer: %d %d\n", i,hid);
        transferAppModels(fg, bg, appmodels, i, hid, totals, &fgtotal, &bgtotal);
      }
      int maxtotalh2 = 0;
      int mintotalh2 = INT_MAX;
      for(int h2=0; h2<NHYPS; h2++){
        maxtotalh2 = MAX(maxtotalh2, totals[p2][h2]);
        mintotalh2 = MIN(mintotalh2, totals[p2][h2]);
      }
      //TODO
      // must set logs before the threading
      setLogs(logs, &nlogs, fgtp2+maxtotalh1+maxtotalh2);
      setLogs(logs, &nlogs, bgtp2-mintotalh1-mintotalh2);
      // run the threads
      pthread_t threads[NTHREADS];
      ThreadStuff2 *ts = (ThreadStuff2*) malloc(sizeof(ThreadStuff2));
      ts->p1 = p1;
      ts->p2 = p2;
      ts->nlogs = nlogs;
      ts->appmodels = appmodels;
      ts->totals = totals;
      ts->logs = logs;
      ts->fg = fg;
      ts->bg = bg;
      ts->fgtotal = fgtotal;
      ts->bgtotal = bgtotal;
      ts->blockpairs = blockpairs;
      // +1 is just to make the unlocking code cleaner
      ts->done = (bool*) calloc(sizeof(bool), NHYPS+1);
      pthread_mutex_init(&ts->getNextLock, NULL);
      for(int t=0;t<NTHREADS;t++)
        pthread_create(threads+t, NULL, threadModelDivergence2, (void*) ts);
      for(int t=0;t<NTHREADS;t++)
        pthread_join(threads[t], NULL);
      pthread_mutex_destroy(&ts->getNextLock);
      free(ts->done);
      free(ts);
      // reset the apps
      appCopy( fg, &fgp2);
      appCopy( bg, &bgp2);
      fgtotal = fgtp2;
      bgtotal = bgtp2;
      isset[p1][p2] = 1;
    }
  }
  return nlogs;
}
//ThreadStuff3* initThreads(
// returns the new value of nlogs
int setBlockPairwiseEdges4( int isset[NPARTS][NPARTS],
    double blockpairs[NPARTS][NPARTS][NHYPS][NHYPS],
    Appmodel appmodels[NPARTS][NHYPS], struct block *b,
    int totals[NPARTS][NHYPS], Appmodel *fg, Appmodel *bg, int *hids,
    double *logs, int nlogs){
  pthread_t threads[NTHREADS];
  ThreadStuff3 *ts = (ThreadStuff3*) malloc(sizeof(ThreadStuff3));
  ts->appmodels = appmodels;
  ts->totals = totals;
  ts->logs = logs;
  ts->nlogs = nlogs;
  ts->fg = fg;
  ts->bg = bg;
  ts->blockpairs = blockpairs;
  ts->done = (bool*)calloc(sizeof(bool), NHYPS+1);
  ts->maindone = 0;
  ts->ndone = 0;
  pthread_mutex_init(&ts->getNextLock, NULL);
  pthread_mutex_init(&ts->doneLock, NULL);
  pthread_cond_init(&ts->threadDoneCond, NULL);
  pthread_cond_init(&ts->mainDoneCond, NULL);
  for(int t=0;t<NTHREADS;t++)
    pthread_create(threads+t, NULL, threadModelDivergence3, (void*) ts);
  int fgtotal = getModelTotal( fg);
  int bgtotal = getModelTotal( bg);
  int DEBUG_EDGES = 0;
  // save the apps
  Appmodel fgp2, bgp2;
  appCopy( &fgp2, fg);
  appCopy( &bgp2, bg);
  int fgtp2 = fgtotal;
  int bgtp2 = bgtotal;
  printf("%d %d\n", fgtotal, bgtotal);
  //
  for(int p1=0; p1<NPARTS; p1++){
    if(!b->b[p1]) continue;
    if(DEBUG_EDGES) printf("P1: %d\n", p1);
    for(int p2=p1+1; p2<NPARTS; p2++){
      if(!b->b[p2]) continue;
      if(DEBUG_EDGES) printf("  P2: %d\n", p2);
      if(isset[p1][p2]) continue;
      if(DEBUG_EDGES) printBlock(b);
      // move weights from other parts in the block
      for(int i=0; i<NPARTS; i++){
        if(!b->b[i] || i==p1 || i==p2) continue;
        int hid = hids[i]-1;
        if(DEBUG_EDGES) printf("transfer: %d %d\n", i,hid);
        transferAppModels(fg, bg, appmodels, i, hid, totals, &fgtotal, &bgtotal);
      }
      // save the apps
      Appmodel fgh1, bgh1;
      appCopy( &fgh1, fg);
      appCopy( &bgh1, bg);
      int fgth1 = fgtotal;
      int bgth1 = bgtotal;
      int maxtotal = 0;
      int mintotal = IMGSIZE;
      for(int h2=0; h2<NHYPS; h2++){
        maxtotal = MAX(maxtotal, totals[p2][h2]);
        mintotal = MIN(mintotal, totals[p2][h2]);
      }
      for(int h1=0; h1<NHYPS; h1++){
        if(DEBUG_EDGES) printf("    H1: %d\n", h1);
        transferAppModels(fg, bg, appmodels, p1, h1, totals, &fgtotal, &bgtotal);
        //TODO
        // must set logs before the threading
        //validateModel(&bgh2);
        setLogs(logs, &nlogs, fgtotal+maxtotal);
        setLogs(logs, &nlogs, bgtotal-mintotal);
        ts->p1 = p1;
        ts->p2 = p2;
        ts->h1 = h1;
        ts->nlogs = nlogs;
        ts->fgtotal = fgtotal;
        ts->bgtotal = bgtotal;
        ts->hasmore = 1;
        memset(ts->done, 0, sizeof(int)*(NHYPS+1));
        ts->maindone = 1;
        // signal the threads to start
        pthread_mutex_lock(&ts->doneLock);
        pthread_cond_broadcast(&ts->threadDoneCond);
        // wait for the threads to finish
        while(ts->ndone != NTHREADS)
          pthread_cond_wait(&ts->mainDoneCond, &ts->doneLock);
        pthread_mutex_unlock(&ts->doneLock);
        ts->ndone = 0;
        //printf("main %d %d %d\n", p1, p2, h1);
        ts->maindone = 0;
        // reset the apps
        appCopy( fg, &fgh1);
        appCopy( bg, &bgh1);
        fgtotal = fgth1;
        bgtotal = bgth1;
        if(DEBUG_EDGES) printf("    H1 done\n");
      }
      // reset the apps
      appCopy( fg, &fgp2);
      appCopy( bg, &bgp2);
      fgtotal = fgtp2;
      bgtotal = bgtp2;
      isset[p1][p2] = 1;
    }
  }
  // tell the threads they are done
  ts->hasmore = 0;
  ts->maindone = 1;
  pthread_mutex_lock(&ts->doneLock);
  pthread_cond_broadcast(&ts->threadDoneCond);
  pthread_mutex_unlock(&ts->doneLock);
  // finalize thread stuff
  for(int t=0;t<NTHREADS;t++)
    pthread_join(threads[t], NULL);
  pthread_mutex_destroy(&ts->getNextLock);
  pthread_mutex_destroy(&ts->doneLock);
  pthread_cond_destroy(&ts->threadDoneCond);
  pthread_cond_destroy(&ts->mainDoneCond);
  free(ts->done);
  free(ts);
  return nlogs;
}
*/
// slightly different model
int setBlockPairwiseEdgesU( double blockpairs[NPARTS][NPARTS][NHYPS][NHYPS],
    Appmodel appmodels[NPARTS][NHYPS], struct block *b,
    int totals[NPARTS][NHYPS], Appmodel *fg, Appmodel *bg, int *hids,
    double *logs, int nlogs, PATTERN *x, struct buckets *allbuckets){
  bool DEBUG_EDGES = false;
  int alltotal = 0;
  if(DEBUG_EDGES){
    calculateAppModelsNoBlur( fg, bg, x, hids, allbuckets);
    alltotal = getModelTotal( fg) + getModelTotal( bg);
  }
  //TODO reverse the order of p2 and h1 in blockpairs
  for(int p1i=0; p1i<ABCD_BLOCK_SIZE; p1i++){
    int p1 = b->p[p1i];
    if(DEBUG_EDGES) printf("P1: %d\n", p1);
    int origh1 = hids[p1];
    for(int h1=0; h1<NHYPS; h1++){
      hids[p1] = h1+1;
      if(DEBUG_EDGES) printf("  H1: %d\n", h1);
      for(int p2i=p1i+1; p2i<ABCD_BLOCK_SIZE; p2i++){
        int p2 = b->p[p2i];
        if(DEBUG_EDGES) printf("    P2: %d\n", p2);
        int origh2 = hids[p2];
        for(int h2=0; h2<NHYPS; h2++){
          hids[p2] = h2+1;
          if(DEBUG_EDGES) printf("      H2: %d\n", h2);
          calculateAppModelsNoBlur( fg, bg, x, hids, allbuckets);
          int fgtotal = getModelTotal( fg);
          int bgtotal = getModelTotal( bg);
          if(DEBUG_EDGES){
            assert(alltotal == fgtotal+bgtotal);
            //printf("%d\n", fgtotal+bgtotal);
          }
          setLogs(logs, &nlogs, max(fgtotal, bgtotal));
          blockpairs[p1][p2][h1][h2] =
            computeU3( fg,bg, fgtotal, bgtotal, logs, nlogs);
          bool TEST = false;
          if(TEST){
            double aa = computeU( fg,bg, fgtotal, bgtotal);
            double bb = computeU2( fg,bg, fgtotal, bgtotal, logs, nlogs);
            double cc = computeU3( fg,bg, fgtotal, bgtotal, logs, nlogs);
            assert(fabs(aa-bb) < FLT_EPSILON);
            assert(fabs(cc-bb) < FLT_EPSILON);
          }
          blockpairs[p2][p1][h2][h1] = blockpairs[p1][p2][h1][h2];

          if(DEBUG_EDGES) printf("      H2 done\n");
        }
        if(DEBUG_EDGES) printf("    H1 done\n");
        hids[p2] = origh2;
      }
    }
    hids[p1] = origh1;
  }
  return nlogs;
}
struct PartAndSize{
  int pid, size;
};
bool partSizeSort( PartAndSize pas1, PartAndSize pas2){
  return pas1.size > pas2.size;
}
// The idea here is to put the smaller parts in the inner loop for faster swapping
void getPartsInDecreasingSize(PATTERN *x, struct block *b, vector<int> &parts){
  assert(b->p.size() == (unsigned int) ABCD_BLOCK_SIZE);
  assert(parts.size() == (unsigned int) 0);
  vector<PartAndSize> pasv;
  for(int i=0; i<ABCD_BLOCK_SIZE; i++){
    PartAndSize pas;
    pas.pid = b->p[i];
    assert(pas.pid>=0 && pas.pid<NPARTS);
    assert(NHYPS >= 2);
    pas.size = x->fgmapIndices[pas.pid][1] - x->fgmapIndices[pas.pid][0];
    pasv.push_back(pas);
  }
  sort(pasv.begin(), pasv.end(), partSizeSort);
  for(int i=0; i<ABCD_BLOCK_SIZE; i++){
    //printf("%d: %d, %d\n", i, pasv[i].pid, pasv[i].size);
    parts.push_back(pasv[i].pid);
  }
}
void setBlockPairwiseEdgesU3( double blockpairs[NPARTS][NPARTS][NHYPS][NHYPS],
    struct block *b,
    int *hids, double *logs, int nlogs, PATTERN *x, double *nlogn, int *numnlogn,
    struct buckets *allbuckets){
  vector<int> parts;
  getPartsInDecreasingSize( x, b, parts);
  //for(int i=0; i<ABCD_BLOCK_SIZE; i++) parts[i] = b->p[i];
  //init models and mask
  Appmodel *fg = (Appmodel*)malloc(sizeof(Appmodel));
  Appmodel *bg = (Appmodel*)malloc(sizeof(Appmodel));
  int *mask = (int*)calloc(IMGSIZE,sizeof(int));
  initModel(fg);
  initModel(bg);
  // all parts are in fg
  if(INC_BLOCK) computeFgAppearnceNoBlur(fg, x, mask, hids, allbuckets);
  // only the non-block parts are in fg
  else computeFgAppearnceNoBlur(fg, x, mask, hids, allbuckets, b);
  computeBgAppearnceNoBlur(bg, x, mask, allbuckets);
  bool DEBUG_EDGES = false;
  int alltotal = getModelTotal( fg) + getModelTotal( bg);
  int TEST = 0;//TODO remove
  if(TEST){ validateModel(fg); validateModel(bg); }
  //TODO reverse the order of p2 and h1 in blockpairs
  for(int p1i=0; p1i<ABCD_BLOCK_SIZE; p1i++){
    int p1 = parts[p1i];
    if(DEBUG_EDGES) printf("P1: %d\n", p1);
    int origh1 = hids[p1];
    if(!INC_BLOCK) transferAppearnceNoBlur( fg, bg, x, mask, p1, 0/*dummy*/,
        origh1-1, allbuckets, 1/*add*/);
    for(int h1=0; h1<NHYPS; h1++){
      transferAppearnceNoBlur( fg, bg, x, mask, p1, hids[p1]-1, h1, allbuckets);
      if(TEST){ validateModel(fg); validateModel(bg); }
      hids[p1] = h1+1;
      if(DEBUG_EDGES) printf("  H1: %d\n", h1);
      int p2total = getModelTotal( fg) + getModelTotal( bg);
      for(int p2i=p1i+1; p2i<ABCD_BLOCK_SIZE; p2i++){
        int p2 = parts[p2i];
        if(DEBUG_EDGES) printf("    P2: %d\n", p2);
        int origh2 = hids[p2];
        if(!INC_BLOCK) transferAppearnceNoBlur( fg, bg, x, mask, p2, 0/*dummy*/,
            origh2-1, allbuckets, 1/*add*/);
        for(int h2=0; h2<NHYPS; h2++){
          transferAppearnceNoBlur( fg, bg, x, mask, p2, hids[p2]-1, h2, allbuckets);
          if(TEST){ validateModel(fg); validateModel(bg); }
          hids[p2] = h2+1;
          if(DEBUG_EDGES) printf("      H2: %d\n", h2);
          // TODO speed up the computation of fgtotal and bgtotal
          int fgtotal = getModelTotal( (int*)fg->m);
          int bgtotal = p2total - fgtotal;
          assert(p2total == getModelTotal( (int*)fg->m) + getModelTotal( (int*)bg->m));
          assert(p2total == 2*APP_MODEL_PRIOR*TOTAL_COLOR_BUCKETS+IMGSIZE);
          // aggregate buckets into channels
          setLABChannels( fg);
          setLABChannels( bg);
          //Error("setBlockPairwiseEdgesU3 needs to include texture features");
          blockpairs[p1][p2][h1][h2] =
            getAppearanceEnergy( 0, //TODO change this
                //computeU( fg,bg, fgtotal, bgtotal));
                computeU7( (int*)fg->m, (int*)bg->m, fgtotal, bgtotal, nlogn));
          for(int c=0; c<NCOLOR-1; c++){
            blockpairs[p1][p2][h1][h2] +=
              getAppearanceEnergy( c+1,
                  computeULAB2( fg->c[c], bg->c[c], nlogn));
            if(TEST){
              double aa = computeULAB( fg->c[c], bg->c[c]);
              double bb = computeULAB2( fg->c[c], bg->c[c], nlogn);
              assertEqual(aa,bb);
            }
          }
          for(int t=0; t<NTEXTURES; t++){
            blockpairs[p1][p2][h1][h2] +=
              getAppearanceEnergy( t+NCOLOR,
                  computeULAB2( fg->t[t], bg->t[t], nlogn));
            if(TEST){
              double aa = computeULAB( fg->t[t], bg->t[t]);
              double bb = computeULAB2( fg->t[t], bg->t[t], nlogn);
              assertEqual(aa,bb);
            }
          }
          blockpairs[p2][p1][h2][h1] = blockpairs[p1][p2][h1][h2];
          if(TEST){
            double aa = computeU( fg,bg, fgtotal, bgtotal);
            /*
            double bb = computeU2( fg,bg, fgtotal, bgtotal, logs, nlogs);
            double cc = computeU3( fg,bg, fgtotal, bgtotal, logs, nlogs);
            double dd = computeU4( (int*)fg->m, (int*)bg->m, fgtotal, bgtotal, logs, nlogs);
            double ee = computeU5( (int*)fg->m, (int*)bg->m, fgtotal, bgtotal, logs, nlogs);
            double ff = computeU6( (int*)fg->m, (int*)bg->m, fgtotal, bgtotal, logs, nlogn);
            */
            double gg = computeU7( (int*)fg->m, (int*)bg->m, fgtotal, bgtotal, nlogn);
            //printf("%lf, %lf, %d, %d\n", aa, bb, fgtotal, bgtotal);
            /*
            assert(fabs(aa-bb) < FLT_EPSILON);
            assert(fabs(cc-bb) < FLT_EPSILON);
            assert(fabs(dd-bb) < FLT_EPSILON);
            assert(fabs(ee-bb) < FLT_EPSILON);
            assert(fabs(ff-bb) < FLT_EPSILON);
            */
            assert(fabs(gg-aa) < FLT_EPSILON);
          }

          if(DEBUG_EDGES) printf("      H2 done\n");
        }
        if(INC_BLOCK) transferAppearnceNoBlur( fg, bg, x, mask, p2, hids[p2]-1,
            origh2-1, allbuckets);
        else transferAppearnceNoBlur( fg, bg, x, mask, p2, hids[p2]-1,
            0/*dummy*/, allbuckets, 2/*subtract*/);
        assert(p2total == getModelTotal(fg) + getModelTotal(bg));
        hids[p2] = origh2;
      }
      if(DEBUG_EDGES) printf("    H1 done\n");
    }
    if(INC_BLOCK) transferAppearnceNoBlur( fg, bg, x, mask, p1, hids[p1]-1,
        origh1-1, allbuckets);
    else transferAppearnceNoBlur( fg, bg, x, mask, p1, hids[p1]-1,
        0/*dummy*/, allbuckets, 2/*subtract*/);
    assert(alltotal == getModelTotal(fg) + getModelTotal(bg));
    hids[p1] = origh1;
  }
  free(fg);
  free(bg);
  free(mask);
}
/*
void computeSymmetricPyramid(
    Appmodel sym_models[NPARTS][NHYPS][SUBPART_LEVELS][MAX_PARTS],
    double symPyramid[SUBPART_LEVELS][NSYMS][NHYPS][NHYPS]){
  // and symPyramid[l-2][i][h1][h2]==symPyramid[l-2][i][h2][h1]
  // and speed up computation by only computing for h2<=h1.

  // Compute the divergence between all the subparts
  for(int i=0;i<NSYMS;i++){
    // symm_joints is 1 indexed
    int p1 = int(symm_joints[i]) - 1;
    int p2 = int(symm_joints[i+NSYMS]) - 1;
    for(int l=2; l<=SUBPART_LEVELS; l++){
      for(int h1=0; h1<K; h1++){// iterate through hypotheses for part 1
        double total1 = getModelTotal(sym_models[0][p1]+h1);
        for(int h2=0; h2<K; h2++){// iterate through hypotheses for part 2
          double total2 = getModelTotal(sym_models[0][p2]+h2);
          //printf("i: %d, %d, %d\n", i, h1, h2);
          symPyramid[l-2][i][h1][h2] = 0.0;
          for(int sp=0; sp<pow(l,2); sp++){
            symPyramid[l-2][i][h1][h2] +=
              computeModelDivergence( sym_models[l-1][p1]+(sp*K+h1),
                  sym_models[l-1][p2]+(sp*K+h2), total1, total2);
          }
        }
      }
    }
  }
  Error("computeSymmetricPyramid not done.");
}
*/
/*
void checkEdges(int *edges){
  for(int e=0; e<E; e++){
    if(edges[e] >= edges[e+E]) Error("Edge error.");
  }
}
*/
void setEdgeIndices(vector< vector<int> > &edgeIndices, int *edges){
  //checkEdges(edges);
  int nset = 0;
  for(int i=0; i<NPARTS; i++)
    for(int j=0; j<NPARTS; j++)
      edgeIndices[i][j] = -1;
  for(int i=0; i<NPARTS; i++){
    for(int j=0; j<NPARTS; j++){
      for(int e=0; e<E; e++){
        if(edges[e] == i && edges[e+E] == j){
          edgeIndices[i][j] = e;
          nset++;
        }
      }
    }
  }
  if(0){
    printf("edge indices:\n");
    for(int i=0; i<NPARTS; i++){
      for(int j=0; j<NPARTS; j++){
        printf("%3d ", edgeIndices[i][j]);
      }
      printf("\n");
    }
  }
  if(nset != E) Error("error in setEdges");
}
//returns the initial energy
double initializeHids(PATTERN *x, LOSS *loss, STRUCT_LEARN_PARM *param,
    CONFIGS *config, int *hids,
    int *edges,
    double *nlogn, struct buckets *allbuckets){
  int DEBUG_INIT = 0;
  for(int i=0; i<NPARTS; i++) hids[i] = 1;
  FEATURES features;
  double naiveenergy = calculateEnergy(x, hids, param, NULL,
      &features, nlogn);
  if(config->sample_learning_features){
    SVECTOR *vec = convertFeaturesToSVEC( &features);
    double l = getLoss(x->y, hids);
    storeFeature(vec, l);
    freeSvec(vec);
  }
  if(NAIVE_INIT) return naiveenergy;

  int64_t t1 = GetTimeInMicrosecs();
  inferenceEM( x, loss, param, config, hids, NULL,
      edges, allbuckets, nlogn);
  int64_t t2 = GetTimeInMicrosecs();
  if(0) printf("inferenceEM time: %ld\n", t2-t1);
  double initenergy = calculateEnergy(x, hids, param, NULL,
      &features, nlogn);
  if(config->sample_learning_features){
    SVECTOR *vec = convertFeaturesToSVEC( &features);
    double l = getLoss(x->y, hids);
    storeFeature(vec, l);
    freeSvec(vec);
  }
  if(DEBUG_INIT) printf("naiveenergy: %lf, initenergy: %lf\n", naiveenergy, initenergy);
  if(VALIDATE_INFERENCE){
    int oldhids[NPARTS];
    for(int i=0; i<NPARTS; i++) oldhids[i] = hids[i];
    double hc3energy = hillClimb3(x, hids, param, loss, nlogn);
    for(int i=0; i<NPARTS; i++){
      if(oldhids[i] != hids[i]){
        for(int j=0; j<NPARTS; j++){
          printf("old: %d, new: %d\n", oldhids[i], hids[i]);
        }
        printf("initenergy: %lf, hc3energy: %lf\n", initenergy, hc3energy);
        Error("hillClimb3 improved energy.");
      }
    }
  }
  return initenergy;
}
void printRetEnergy(RetEnergy *r){
  printf("retenergy: %lf, %lf, %lf, %lf, %lf\n",
      r->e1, r->e2, r->e3, r->e4, r->e5);
}
void printPairwise(double symAndPair[E][NHYPS][NHYPS]){
  printf("symAndPair:\n");
  for(int i=0; i<E; i++){
    printf("edge %d\n", i);
    for(int j=0; j<NHYPS; j++){
      for(int k=0; k<NHYPS; k++){
        printf("%lf ", symAndPair[i][j][k]);
      }
      printf("\n");
    }
  }
}
void createCondensedPairwise(vector< vector<double> > &condensed,
    int *hids, struct block *b, int *edges, PATTERN *x){
  int debug = 0;
  if(debug) printf("starting createCondensedPairwise\n");
  condensed.clear();
  condensed.assign( ABCD_BLOCK_SIZE, vector<double>(NHYPS, 0.0));
  // condense the pairwise
  for(int e=0; e<E; e++){
    int from = edges[e];
    int to   = edges[e+E];
    if(debug) printf("from %3d to %3d\n", from, to);
    if(to<0 || from<0 || to>=NPARTS || from>=NPARTS)
      Error("Error in createCondensedPairwise.");
    if((b->b[from] && b->b[to]) || (!b->b[from] && !b->b[to])) continue;
    if(debug) printf("from %3d to %3d, adding\n", from, to);
    if(b->b[from]){
      int hyp = hids[to] - 1;
      for(int h=0; h<NHYPS; h++){
        assert(hyp>=0 && hyp<NHYPS);
        if(e<NSYMS){
          condensed[b->i[from]][h] +=
            getSymmetricEnergy(e, x->symAndPair[e][h][hyp]);
            //edge_weights[e]*symAndPair[e][h][hyp];
        }else{
          condensed[b->i[from]][h] +=
            getPairwiseEnergy( e-NSYMS, x->symAndPair[e][h][hyp]);
        }
      }
    }else{
      assert(b->b[to]);
      int hyp = hids[from] - 1;
      for(int h=0; h<NHYPS; h++){
        assert(hyp>=0 && hyp<NHYPS);
        if(e<NSYMS){
          condensed[b->i[to]][h] +=
            getSymmetricEnergy(e, x->symAndPair[e][hyp][h]);
            //edge_weights[e]*symAndPair[e][hyp][h];
        }else{
          condensed[b->i[to]][h] +=
            getPairwiseEnergy( e-NSYMS, x->symAndPair[e][hyp][h]);
        }
      }
    }
  }
  // condense the overlap
  for(int o=0; o<NOVERLAP; o++){
    int from = Overlap_edges[o];
    int to = Overlap_edges[o+NOVERLAP];
    if((b->b[from] && b->b[to]) || (!b->b[from] && !b->b[to])) continue;
    if(b->b[from]){
      int hyp = hids[to] - 1;
      for(int h=0; h<NHYPS; h++){
        assert(hyp>=0 && hyp<NHYPS);
        condensed[b->i[from]][h] += getOverlapEnergy(o, x->overlap[o][h][hyp]);
      }
    }else{
      assert(b->b[to]);
      int hyp = hids[from] - 1;
      for(int h=0; h<NHYPS; h++){
        assert(hyp>=0 && hyp<NHYPS);
        condensed[b->i[to]][h] += getOverlapEnergy(o, x->overlap[o][hyp][h]);
      }
    }
  }
  assert(condensed.size() == (unsigned int) ABCD_BLOCK_SIZE);
  assert(condensed[0].size() == NHYPS);
  if(0){
    printf("condensed:\n");
    for(int i=0; i<ABCD_BLOCK_SIZE; i++){
      for(int j=0; j<NHYPS; j++){
        printf("%lf ", condensed[i][j]);
      }
      printf("\n");
    }
  }
}
double getFixedEnergy( int *hids, struct block *b, PATTERN *x,
    LOSS *loss, int *edges){
  double energy = 0.0;
  //////// Energy for parts NOT in the block ////////////
  for(int i=0; i<NPARTS; i++){
    if(b->b[i]) continue;
    int h = hids[i] - 1;
    assert(h>=0 && h<NHYPS);
    energy += getSingletonEnergy(i, x->singleton[i][h]);
    energy += loss->l[i][h];
  }
  for(int e=0; e<E; e++){
    int from = edges[e];
    int to   = edges[e+E];
    if(to<0 || from<0 || to>=NPARTS || from>=NPARTS)
      Error("Error in getFixedEnergy.");
    if(b->b[from] || b->b[to]) continue;
    int hfrom = hids[from] - 1;
    int hto   = hids[to]   - 1;
    energy += x->symAndPair[e][hfrom][hto];
  }
  return energy;
}
void validateBlockpairs( double blockpairs[NPARTS][NPARTS][NHYPS][NHYPS], struct block *b){
  for(int p1i=0; p1i<ABCD_BLOCK_SIZE; p1i++){
    int p1 = b->p[p1i];
    for(int p2i=p1i+1; p2i<ABCD_BLOCK_SIZE; p2i++){
      int p2 = b->p[p2i];
      for(int h1=0; h1<NHYPS; h1++){
        for(int h2=0; h2<NHYPS; h2++){
          //printf("%d %d %d %d\n", p1, p2, h1, h2);
          assertEqual(blockpairs[p2][p1][h2][h1], blockpairs[p1][p2][h1][h2]);
        }
      }
    }
  }
}
void addExtraFeatures( double blockpairs[NPARTS][NPARTS][NHYPS][NHYPS],
    PATTERN *x){
  for(int o=0; o<NOVERLAP; o++){
    int p1 = Overlap_edges[o];
    int p2 = Overlap_edges[o+NOVERLAP];
    for(int h1=0; h1<NHYPS; h1++)
      for(int h2=0; h2<NHYPS; h2++){
        blockpairs[p1][p2][h1][h2] +=
          getOverlapEnergy(o, x->overlap[o][h1][h2]);
        blockpairs[p2][p1][h2][h1] +=
          getOverlapEnergy(o, x->overlap[o][h1][h2]);
      }
  }
}
void doHuayansInference(int *hids, struct block *b,
    vector< vector<double> > &factor, int *edges,
    double blockpairs[NPARTS][NPARTS][NHYPS][NHYPS],
    vector< vector<int> > &edgeIndices,
    LOSS *loss, PATTERN *x, STRUCT_LEARN_PARM *param,
    double *logs, int *nlogs){
  double (*symAndPairEnergies)[NHYPS][NHYPS] =
    (double (*)[NHYPS][NHYPS]) malloc(sizeof(double)*E*NHYPS*NHYPS);
  calculateSymAndPairEnergies(symAndPairEnergies, x);
  vector<vector<double> > condensed;
  createCondensedPairwise( condensed, hids, b, edges, x);
  cp_graph G = cp_graph(2 /*fully connected graph type*/, ABCD_BLOCK_SIZE);
  validateBlockpairs( blockpairs, b);
  G.set_weights( NHYPS, b, factor, condensed, blockpairs,
      symAndPairEnergies, edgeIndices);
  cp_problem cp = cp_problem(G);
  cp.cycle_method = 2;
  cp.dual_method = 1;
  cp.pbca_update = 0;
  cp.pbca_minmarginal = 2;
  cp.damping = 0.5;
  cp.warmstart_cycle = 1;
  cp.initialize();

  free(symAndPairEnergies);

  //TODO
  int running_mode = 0; // 0==iteration, 1==time
  double running_duration = 1; // iterations / seconds

  bool DEBUG_HUAYAN = false;
  if(DEBUG_HUAYAN) printf("running for %d iterations\n", ABCD_INF_TIME);
  bool KEEPLOG = false;
  const char *output = "log.txt";

  for(int ite=0; ite<ABCD_INF_TIME; ite++){
    if(DEBUG_HUAYAN) printf("%d: ", ite);
    fflush(stdout);

    cp.run(running_mode, running_duration);
    if(DEBUG_HUAYAN)
      printf("| (%.1f) %.3f ", cp.running_time_per_iteration, cp.dual_obj);
      //printf("(%ld/%.1f) %.3f|%.3f ", cp.iter, cp.running_time, cp.primal_obj, cp.dual_obj);
    fflush(stdout);
    if(DEBUG_HUAYAN) printf("\n");

    if(KEEPLOG){
      FILE *fp = fopen(output, "a");
      fprintf(fp, "%f %f %f %ld %d ", cp.primal_obj, cp.dual_obj, cp.running_time, cp.iter, (int)cp.CY.size()); // keep log
      fprintf(fp, "\n");
      fclose(fp);
    }
    if(fabs(cp.primal_obj-cp.dual_obj)<FLT_EPSILON) // experiment converged
      break;
  }

  ///////// ANDREW ///////////////////////////////////////////////
  // cp.assignment is the optimal assignment
  // cp.obj is minimal energy
  ////////////////////////////////////////////////////////////////

  double fixedEnergy = getFixedEnergy(hids, b, x, loss, edges);
  double origenergy = G.getEnergyForHypotheses(hids, b);
  if(DEBUG_HUAYAN) cp.print_statistics();
  int DEBUG_HUAYAN2 = 0;
  for(unsigned int i=0; i<cp.assignment.size(); i++){
    if(DEBUG_HUAYAN2) printf("%u: %d\n", i, cp.assignment[i]);
    hids[b->p[i]] = cp.assignment[i] + 1;
    assert(cp.assignment[i]>=0 && cp.assignment[i]<NHYPS);
  }
  double huayanenergy = G.getEnergyForHypotheses(hids, b);
  if(ABCD_BLOCK_SIZE == 2) assert(huayanenergy <= origenergy);
  double huayanenergy2 = cp.primal_obj;
  if( fabs(huayanenergy-huayanenergy2) > FLT_EPSILON){
    printf("huayanenergy %lf, huayanenergy2 %lf\n", huayanenergy, huayanenergy2);
    Error(":/");
  }
  double fixedEnergy2 = getFixedEnergy(hids, b, x, loss, edges);
  assert(fabs(fixedEnergy-fixedEnergy2) < FLT_EPSILON);
  //
  if(ABCD_BLOCK_SIZE == 2){
    int h1, h2;
    double size2energy = G.getBestHypotheses( &h1, &h2);
    if(DEBUG_HUAYAN2) printf("size2energy: %lf, h1: %d, h2: %d\n", size2energy, h1, h2);
    assert(fabs(size2energy-huayanenergy) < FLT_EPSILON);
    assert(cp.assignment[0] == h1);//TODO check if the indices are off by 1
    assert(cp.assignment[1] == h2);//TODO check if the indices are off by 1
  }
  if(DEBUG_HUAYAN2){
    printf("huayanenergy %lf, origenergy %lf\n", huayanenergy, origenergy);
    huayanenergy = fixedEnergy-huayanenergy;
    origenergy = fixedEnergy-origenergy;
    printf("including fixed: %lf huayanenergy %lf, origenergy %lf\n",
      fixedEnergy, huayanenergy, origenergy);
  }
  /*double curenergy = calculateEnergy(x, hids, param, NULL,
      NULL, nlogn);
  printf("curenergy: %lf\n", curenergy);*/
  //cp.exhaustive_search();
  //Error("doHuayansInference NOT IMPLEMENTED");
}
void approxBlockCoordDescent( PATTERN *x, STRUCT_LEARN_PARM *param,
    int *hids, LOSS *loss, int *edges, CONFIGS *config,
    struct buckets *allbuckets, double *nlogn){
  int nlogs = 0;
  int numnlogn = 0;
  double *logs = allocLogs(x, MODEL_BLUR_INV);
  //double *nlogn = allocLogs(x, MODEL_BLUR_INV);// not to be confused with nlogs
  // just set this to the max at the beggining with
  // 2*APP_MODEL_PRIOR*TOTAL_COLOR_BUCKETS+IMGSIZE logs
  setLogsn(logs, nlogn, &nlogs, &numnlogn,
      2*APP_MODEL_PRIOR*TOTAL_COLOR_BUCKETS + IMGSIZE);
  // a few are unused but this makes indexing easier
  //Appmodel (*appmodels)[NHYPS] =
  //  (Appmodel(*)[NHYPS]) malloc(sizeof(Appmodel)*NPARTS*NHYPS);
  vector<double> energies;
  vector<int64_t> times;
  //assert(ABCD_BLOCK_SIZE == 2);//just for debugging, since this makes it exact.
  int DEBUG_ABCD = 0;
  int TIME_ABCD = 0;
  // 2nd order terms between parts
  // TODO make blockpairs just ABCD_BLOCK_SIZE x ABCD_BLOCK_SIZE
  double (*blockpairs)[NPARTS][NHYPS][NHYPS] =
    (double(*)[NPARTS][NHYPS][NHYPS])
    malloc(sizeof(double)*NPARTS*NPARTS*NHYPS*NHYPS);
  vector< vector<int> > edgeIndices( NPARTS, vector<int>(NPARTS, 0));
  setEdgeIndices(edgeIndices, edges);
  vector<vector<double> > factor;
  factor.assign( NPARTS, vector<double>(NHYPS, 0.0));
  addLossToSingleton( factor, x, loss);
  double initenergy =
    initializeHids( x, loss, param, config, hids,
      edges, nlogn, allbuckets);
  initenergy += getLoss(loss, hids);
  double prevenergy = initenergy;

  int64_t t0 = GetTimeInMicrosecs();
  int64_t t1,t4, t6, t7, t8, t9, t9_1;
  t1 = GetTimeInMicrosecs();
  energies.push_back(initenergy);
  times.push_back(GetTimeInMicrosecs() - t1);

  int orig_abcd_block_size = ABCD_BLOCK_SIZE;
  while(ABCD_BLOCK_SIZE >0){
    if(TIME_ABCD) printf("time 1: %ld\n", t1-t0);
    //applySymAndPairWeights( symAndPair);
    //printPairwise(symAndPair);
    if(ABCD_BLOCK_SIZE == 1){
      if(orig_abcd_block_size==1){
        double hcenergy = hillClimb(factor, x, hids, param, config, loss,
            nlogn);
        //printf("prevenergy: %lf, hcenergy: %lf\n", prevenergy, hcenergy);
        energies.push_back(hcenergy);
        times.push_back(GetTimeInMicrosecs() - t1);
      }
      ABCD_BLOCK_SIZE = orig_abcd_block_size;
      logEnergies(energies, times, x, config, param);
      free(logs);
      free(blockpairs);
      return;
    }
    int64_t t2 = GetTimeInMicrosecs();
    if(TIME_ABCD) printf("time 2: %ld\n", t2-t1);
    // do a b c d
    int64_t t3 = GetTimeInMicrosecs();
    if(TIME_ABCD) printf("time 3: %ld\n", t3-t2);
    for(int iter=0; iter<ABCD_ITERS; iter++){
      //double hcenergy = hillClimb(factor, x, hids, param, config, loss);
      //energies.push_back(hcenergy);
      //times.push_back(GetTimeInMicrosecs() - t1);
      if(DEBUG_ABCD) printHids(hids);
      t4 = GetTimeInMicrosecs();
      struct block b;
      chooseABCDblock( &b);
      if(DEBUG_ABCD) printBlock(&b);
      checkBlock(&b);
      t6 = GetTimeInMicrosecs();
      if(TIME_ABCD) printf("time 4: %ld\n", t6-t4);
      int ALGNUM = 7;
      switch(ALGNUM){
        case 7:
          setBlockPairwiseEdgesU3( blockpairs, &b,
              hids, logs, nlogs, x, nlogn, &numnlogn, allbuckets);
          break;
        default:
          Error("no such algnum case.");
      }
      validateBlockpairs( blockpairs, &b);
      addExtraFeatures( blockpairs, x);
      checkBlock(&b);
      t7 = GetTimeInMicrosecs();
      if(TIME_ABCD) printf("time 5: %ld\n", t7-t6);
      vector<int> oldhids(NPARTS,-1);
      for(int i=0; i<NPARTS; i++) oldhids[i] = hids[i];
      checkBlock(&b);
      doHuayansInference( hids, &b, factor, edges, blockpairs,
          edgeIndices, loss, x, param, logs, &nlogs);
      t8 = GetTimeInMicrosecs();
      if(TIME_ABCD) printf("time 6: %ld\n", t8-t7);
      RetEnergy retenergy;
      if(DEBUG_ABCD) printHids(hids);
      FEATURES features;
      double curenergy = calculateEnergy(x, hids, param, NULL,
          &features, nlogn, &retenergy);
      curenergy += getLoss(loss, hids);
      if(DEBUG_ABCD) printf("after iter: %d\n", iter);
      if(DEBUG_ABCD) printf("initenergy: %lf, prevenergy: %lf, curenergy: %lf, curloss: %lf\n",
          initenergy, prevenergy, curenergy, getLoss(loss, hids));
      t9_1 = GetTimeInMicrosecs();
      if(TIME_ABCD) printf("time 7: %ld\n", t9_1-t8);
      if(2 == ABCD_BLOCK_SIZE){
        checkBlock(&b);
        double bhcenergy = blockHillClimb(factor, x, hids, param, config, loss, &b, nlogn);
        checkBlock(&b);
        if(DEBUG_ABCD) printHids(hids);
        if(DEBUG_ABCD) printRetEnergy(&retenergy);
        double bhcenergy2 = calculateEnergy(x, hids, param,
            NULL, NULL, nlogn, &retenergy);
        bhcenergy2 += getLoss(loss, hids);
        if(DEBUG_ABCD) printHids(hids);
        if(DEBUG_ABCD) printRetEnergy(&retenergy);
        if(DEBUG_ABCD) printWeights();
        if(DEBUG_ABCD) printf("bhcenergy: %lf\n", bhcenergy);
        assertEqual( bhcenergy, bhcenergy2);
        assertEqual( bhcenergy, curenergy);
        if(ENFORE_INCREASING)
          assert(curenergy >= prevenergy);//TODO this SHOULD be used
      }
      if(ENFORE_INCREASING && curenergy < prevenergy){//reset the hids
        // these are really the negative energies so they should go up.
        if(DEBUG_ABCD) printf("Objective went down! Resetting hids!\n");
        for(int i=0; i<NPARTS; i++) hids[i] = oldhids[i];
      }else{
        prevenergy = curenergy;
      }
      energies.push_back(prevenergy);
      times.push_back(GetTimeInMicrosecs() - t1);
      if(DEBUG_ABCD) printHids(hids);
      t9 = GetTimeInMicrosecs();
      if(TIME_ABCD) printf("time 8: %ld\n", t9-t9_1);
      if(config->sample_learning_features){
        SVECTOR *vec = convertFeaturesToSVEC( &features);
        double l = getLoss(x->y, hids);
        storeFeature(vec, l);
        freeSvec(vec);
      }
    }
    ABCD_BLOCK_SIZE--;
  }
  assert(false);
  /*
  if(DEBUG_ABCD) printHids(hids);
  double hcenergy = hillClimb(factor, x, hids, param, config, loss,
      nlogn);

  //printPartEnergies( 261, 8, factor, x, hids, param, config, loss, nlogn);
  if(APPD) printHids(hids);
  if(config->sample_learning_features){
    FEATURES features;
    calculateEnergy(x, hids, param, NULL, &features,
        nlogn);
    SVECTOR *vec = convertFeaturesToSVEC( &features);
    double l = getLoss(x->y, hids);
    storeFeature(vec, l);
    freeSvec(vec);
  }
  //if(DEBUG_ABCD) printf("prevenergy: %lf, hcenergy: %lf\n", prevenergy, hcenergy);
  if(DEBUG_ABCD) printHids(hids);
  energies.push_back(hcenergy);
  times.push_back(GetTimeInMicrosecs() - t1);
  int64_t t10 = GetTimeInMicrosecs();
  if(TIME_ABCD) printf("time 9: %ld\n", t10-t9);
  logEnergies(energies, times, x, config, param);
  free(logs);
  free(blockpairs);
  //Error("Approx block coord descent not yet implemented.");
  */
}
/*
void allocSymModels(Appmodel sym_models[NPARTS][NHYPS][SUBPART_LEVELS][NPARTS]){
  for(int i=0;i<SUBPART_LEVELS; i++){
    for(int j=0;j<N; j++){
      sym_models[i][j] = (Appmodel*)malloc(pow(i+1,2)*NHYPS*sizeof(Appmodel));
      if(!sym_models[i][j]){
        Error("Could not allocate heap space for sym_models.");
      }
    }
  }
}
void freeSymModels((Appmodel*) sym_models[SUBPART_LEVELS][NPARTS]){
  for(int i=0;i<SUBPART_LEVELS; i++){
    for(int j=0;j<N; j++){
      free(sym_models[i][j]);
    }
  }
}
*/
// PATTERN x: the image and singleton/pairwise detection scores.
// returns the energy
double minSumWithAppearance(PATTERN *x, LOSS *loss, STRUCT_LEARN_PARM *param,
    CONFIGS *config, int *hids, FEATURES *features, double *nlogn){
  //srand(12345);
  // make the seed different for every image so that there isnt the same order
  // every time abcd is called.
  srand(x->imgnum);
  setGlobals(x, param, config);
  checkWeights();
  if(0 && x->imgnum==1){
    printf("img %d:\n", x->imgnum);
    printWeights();
  }
  ////////////////////////
  /*
  Appmodel fg,bg;
  initModel( &fg);
  initModel( &bg);
  int fgtotal = getModelTotal( &fg);
  int bgtotal = getModelTotal( &bg);
  printf("\n");
  printf("totals: %d %d\n", fgtotal, bgtotal);
  double a1 = computeModelDivergence( &fg, &bg, getModelTotal( &fg), getModelTotal( &bg));
  fg.m[0][0][0] = 41;
  bg.m[1][0][0] = 41;
  double a4 = computeU7( (int*)fg.m, (int*)bg.m,
      getModelTotal( &fg), getModelTotal( &bg), nlogn);
  double a2 = computeModelDivergence( &fg, &bg, getModelTotal( &fg), getModelTotal( &bg));
  fg.m[0][0][0] = 21;
  fg.m[1][0][0] = 21;
  bg.m[0][0][0] = 21;
  bg.m[1][0][0] = 21;
  double a3 = computeModelDivergence( &fg, &bg, getModelTotal( &fg), getModelTotal( &bg));
  printf("%lf, %lf, %lf, %lf\n", a1, a2, a3, a4);
  exit(0);*/

  // combine joints/symm_joints into edges
  int edges[2*E]; // edges is 0 indexed
  for(int jidx=0; jidx<NSYMS; jidx++){
    // symm_joints is 1 indexed
    edges[jidx]     = param->syms[jidx].j1 - 1;
    edges[jidx + E] = param->syms[jidx].j2 - 1;
  }
  for(int jidx=0; jidx<NJOINTS; jidx++){
    edges[jidx + NSYMS]     = param->joints[jidx].j1 - 1;
    edges[jidx + NSYMS + E] = param->joints[jidx].j2 - 1;
    //printf("%d %d\n", edges[jidx + NSYMS], edges[jidx + NSYMS + E]);
  }
  //exit(0);

  struct buckets *allbuckets = (struct buckets*)
    malloc(sizeof(struct buckets)*IMGSIZE);
  discretizeAll(allbuckets, x->image);
  // each of these methods should set the hids properly
  switch(INFERENCE_METHOD){
    case 1: // EM
      inferenceEM( x, loss, param, config, hids, features,
          edges, allbuckets, nlogn);
      break;
    case 2: // approx block coord descent
      approxBlockCoordDescent( x, param, hids, loss,
          edges, config, allbuckets, nlogn);
      break;
    default:
      Error("Inference method is unspecified.");
  }
  free(allbuckets);
  /*
  double tmp = 0.0;
  double tmp2 = 0.0;
  for(int i=0; i<NPARTS; i++){
    tmp += x->singleton[i][0];
    tmp2 += x->singleton[i][hids[i]-1];
  }
  for(int i=0; i<NJOINTS; i++){
    tmp += x->symAndPair[i][0][0];
    int h1 = hids[param->joints[i].j1-1]-1;
    int h2 = hids[param->joints[i].j2-1]-1;
    //printf("%d %d\n", h1+1, h2+1);
    tmp2 += x->symAndPair[i][h1][h2];
  }*/
  //printf("%lf %lf\n", tmp, tmp2);

  //for (int i=0; i<NPARTS; i++) hids[i] = 1;
  double energy = calculateEnergy(x, hids, param, config,
      features, nlogn, NULL, NULL, -1, true);
  return energy;
}

// symmetric appearance between parts. arms-arms, legs-legs
// use hypothesis energy to weight appearance
// Try using a different representation of the color channels.
//
// make a configuration file to run experiments
// compare appearance model and overall model on ground truth, best possible, and current guess.
// also visualize the comparison
//
// read approximate inference in koller's book
// get own book
//
// try hill climbing by changing just one part at a time
//
// visualizations for each symmetry part
//
// Do hillclimbing on the parameters (after doing hillclimbing on the parts)
//
// Directly compare symmetric parts using a pixel by pixel comparison with squared error
// plot energy over approx inference iterations. then do hillclimbing to show it doesnt improve that much.
//
// Fix the approx inference or the symmetry weight or something.
