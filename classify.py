import random, os, sys
H = [10, 20, 50, 100, 500]
B = range(1,6)

random.seed(123)
#x = random.randint(1,9) # x in [2,4]
x = 1
for incb in [1]:
  for c in [0.1]:
    for b in B:
      for i in [50]: #100
        for randf in [1]:
          for learnf in [1]:
            for h in H:
              if x==2: x += 1
              if x==8: x += 1
              if x>9: x = 1
              cmd = 'qsub -q daglab -v '
              cmd += 'cval='+str(c)+',bval='+str(b)
              cmd += ',ival='+str(i)+',incbval='+str(incb)
              cmd += ',randf='+str(randf)+',learnf='+str(learnf)
              cmd += ',hval='+str(h)
              cmd += ',icval='+str(int(10000*c)) # int version of c
              cmd += ' c10.script -l nodes=dag'+str(x)+'.stanford.edu'
              print cmd
              os.system(cmd)
              x += 1
              #sys.exit()
