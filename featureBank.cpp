#include "common.h"
#include "featureBank.h"
#include <assert.h>

using namespace std;

const int USE_WEIGHTS = 0;
// used to compute the mean and variance of the features.
vector<vector<double> > featureBank;
vector<double> lastMeans;
vector<double> lastSdevinv;
// used to count some instances as more/less important
vector<double> weights;
bool loaded;

void printVector(vector<double> &v, char *s){
  printf("%s\n", s);
  for(unsigned int i=0; i<v.size(); i++){
    printf("%lf ", v[i]);
  }
  printf("\n");
}
void clearFeatureBank(){
  featureBank.clear();
  weights.clear();
}
void initFeatureBank(){
  loaded = false;
  lastMeans.clear();
  lastMeans.assign(NFEATURES, 0.0);
  lastSdevinv.clear();
  lastSdevinv.assign(NFEATURES, 1.0);
  clearFeatureBank();
}
void storeFeature(SVECTOR *sv, double loss){
  vector<double> f;
  for(int i=0; sv->words[i].wnum>0; i++){
    f.push_back(sv->words[i].weight);
  }
  //printVector(f, "storing");
  //printf("NFEATURES %d, %u\n", NFEATURES, f.size());
  assert(NFEATURES == f.size());
  featureBank.push_back(f);
  if(USE_WEIGHTS) weights.push_back(loss);
}
vector<double> featureBankMean(){
  assert(featureBank.size() > 0);
  if(USE_WEIGHTS) assert(featureBank.size() == weights.size());
  assert(NFEATURES == featureBank[0].size());
  double totalweight = 0.0;
  if(USE_WEIGHTS){
    for(unsigned int i=0; i<weights.size(); i++){
      //printf("w: %lf\n", weights[i]);
      totalweight += weights[i];
    }
  }else{
    totalweight = featureBank.size();
  }

  vector<double> m(NFEATURES, 0.0);
  for(unsigned int i=0; i<featureBank.size(); i++){
    assert(featureBank[i].size() == m.size());
    for(unsigned int j=0; j<m.size(); j++){
      if(USE_WEIGHTS) m[j] += weights[i]*featureBank[i][j];
      else m[j] += featureBank[i][j];
    }
  }
  for(unsigned int j=0; j<m.size(); j++) m[j] /= totalweight;
  return m;
}
vector<double> featureBankSdevInv(vector<double> &m){
  vector<double> sdi(NFEATURES, 0.0);
  // compute the variance
  for(unsigned int i=0; i<featureBank.size(); i++){
    assert(featureBank[i].size() == sdi.size());
    // add the squares of the deviation from the mean
    for(unsigned int j=0; j<sdi.size(); j++){
      double diff = featureBank[i][j]-m[j];
      //printf("%d, %lf\n", j, diff);
      // TODO should this be weights^2?
      if(USE_WEIGHTS) sdi[j] += weights[i]*pow(diff, 2);
      else sdi[j] += pow(diff, 2);
    }
  }
  double totalweight = 0.0;
  if(USE_WEIGHTS){
    for(unsigned int i=0; i<weights.size(); i++) totalweight += weights[i];
  }else{
    totalweight = featureBank.size();
  }
  for(unsigned int j=0; j<sdi.size(); j++) sdi[j] /= totalweight;
  // want the inverse stddev
  for(unsigned int j=0; j<sdi.size(); j++){
    //hack since sometimes all features are equal, so the stddev is 0
    assert(sdi[j] >= 0.0);
    if(sdi[j] < FLT_EPSILON) sdi[j] = 1.0;
    sdi[j] = 1.0 / sqrt(sdi[j]);
  }
  return sdi;
}
void applyFeatureBank(){
  bool verbose = false;
  if(verbose) printf("Applying feature bank\n");
  vector<double> m = featureBankMean();
  if(verbose) printVector(m, "mean");
  vector<double> sdi = featureBankSdevInv(m);
  if(verbose) printVector(m, "mean");
  if(verbose) printVector(sdi, "std dev inv");
  for(unsigned int i=0; i<lastMeans.size(); i++){
    assert(lastSdevinv[i] > 0.0);
    lastMeans[i] += m[i]/lastSdevinv[i];
  }
  if(verbose) printVector(lastMeans, "final mean");
  for(unsigned int i=0; i<lastSdevinv.size(); i++){
    assert(sdi[i] > 0.0);
    lastSdevinv[i] *= sdi[i];
  }
  if(verbose) printVector(lastSdevinv, "final std dev inv");
  clearFeatureBank();
}
void saveFeatureBank(string outfile){
  if(loaded){
    printf("Already have a saved feature bank. So do nothing.\n");
    return;
  }
  FILE *f = fopen(outfile.c_str(),"w");
  if(!f){
    printf("\nCouldnt open file: %s\n", outfile.c_str());
    assert(0);
  }
  const int DEBUG = 0;
  if(DEBUG) printf("means:\n");
  for(unsigned int i=0; i<lastMeans.size(); i++){
    fprintf(f, "%.10lf\n", lastMeans[i]);
    if(DEBUG) printf("%.10lf\n", lastMeans[i]);
  }
  if(DEBUG) printf("std dev invs:\n");
  for(unsigned int i=0; i<lastSdevinv.size(); i++){
    fprintf(f, "%.10lf\n", lastSdevinv[i]);
    if(DEBUG) printf("%.10lf\n", lastSdevinv[i]);
  }
  fclose(f);
}
void loadFeatureBank(string infile){
  initFeatureBank();// default values
  FILE *f = fopen(infile.c_str(),"r");
  if(!f){
    printf("\nCouldnt open file: %s\n", infile.c_str());
    printf("Keeping default values for MEAN and STD DEV INV\n");
    return;
  }
  const int DEBUG = 0;
  if(DEBUG) printf("means:\n");
  for(unsigned int i=0; i<lastMeans.size(); i++){
    if(1!= fscanf(f, "%lf\n", &lastMeans[i])) Error("lastMeans Read Error.");
    if(DEBUG) printf("%.10lf\n", lastMeans[i]);
  }
  if(DEBUG) printf("std dev invs:\n");
  for(unsigned int i=0; i<lastSdevinv.size(); i++){
    if(1!= fscanf(f, "%lf\n", &lastSdevinv[i])) Error("lastSdevinv Read Error.");
    if(DEBUG) printf("%.10lf\n", lastSdevinv[i]);
  }
  fclose(f);
  loaded = true;
}
vector<double> getLastMeans(){ return lastMeans; }
vector<double> getSdevinv(){ return lastSdevinv; }
