clear;
% if you want to change the shape of the person, look into this
Params = SetParameters();
DISPLAY = false;

best_accuracy_possible = [];
accuracy = [];
% load data for all images
IMG_NUMS = 101:305;
for img_num=IMG_NUMS
  if img_num == 180
    continue; % something is wrong with this one
  end
  img_name = sprintf('Ramanan_test/im0%d.proprocessed.mat',img_num);
  load (img_name);
  [H W C] = size(Image);

  % now we have 10 candidates for each of the 10 body parts, with the
  % pairwise energies between each pair of them pre-computed
  % GtBbox: ground truth annotation
  % Image: rgb image
  % PartHypo: pre-filtered 10 candidates for each of the 10 body parts
  % JointCost: pre-computed pairwise energies between them

  % get the mapping to foreground image pixels
  if DISPLAY
    Coverage = GetPartCoverage(H, W, PartHypo, Params);
    % This array specifies (for each body part hypothesis) which pixels it maps
    % to (with a weight). For example to draw the 3rd hypothesis for the 2nd
    % body part, just do this:
    pid = 2;
    hid = 3;
    C = Coverage(Coverage(:,1)==pid, :);
    C = C(C(:,2)==hid,:);
    I = zeros(H*W,1);
    I(C(:,3)) = C(:,4);
    imshow(reshape(I, [H W]));
  end

  % specify random singleton energies 
  Energy.Singleton = rand(10,10);
  Energy.Struct = Params.joints;
  Energy.Edge = JointCost;

  % infer the most probable configuration
  [X E] = SolveTreeStructuredMultiLabelEnergy(Energy);

  % evaluate
  [correctness Bbox] = EvaluateRamanan(X, PartHypo, GtBbox, Params);
  accuracy = [accuracy; mean(correctness)];

  [correctness Bbox] = EstimateRoofUsingGroundTruth(PartHypo, GtBbox, Params);
  best_accuracy_possible = [best_accuracy_possible; mean(correctness)];

  if DISPLAY
    % draw results
    figure
    ResultImage = DrawPose(Image, Bbox, correctness); % red for incorrect
    subplot(1,2,1);
    imshow(ResultImage);

    % draw ground truth
    GTImage = DrawPose(Image, GtBbox, ones(10,1));
    subplot(1,2,2);
    imshow(GTImage);
  end
end
accuracy=mean(accuracy)
best_accuracy_possible=mean(best_accuracy_possible)
