function [correct bbox] = EvaluateRamanan(Sol, PartHypo, GtBbox, Params)
correct = zeros(Params.nParts,1);
bbox = cell(Params.nParts, 1);
for pidx = 1:Params.nParts
  ridx = PartHypo(pidx, Sol(pidx), 1);
  y = PartHypo(pidx, Sol(pidx), 2);
  x = PartHypo(pidx, Sol(pidx), 3);
  [correct(pidx) bbox{pidx}] = EvaluateBodyPart([ridx, y, x], pidx, GtBbox{pidx}, Params);
end
end

function [correct bbox] = EvaluateBodyPart(sol, part_idx, gt_bbox, Params)

rot_idx = sol(1);
y_idx = sol(2);
x_idx = sol(3);

scale = Params.scale;
rot_step_size = (Params.max_rot-Params.min_rot)/Params.rot_steps * pi/180;

% construct predicted bbox
bbox.part_pos = [x_idx y_idx];
rot = Params.min_rot*pi/180 + rot_step_size*(0.5+rot_idx);
bbox.part_x_axis = [cos(rot) sin(rot)];
bbox.part_y_axis = [-sin(rot) cos(rot)];
rect_width = scale*Params.window_size_x(part_idx);
rect_height = scale*Params.window_size_y(part_idx);

bbox.min_proj_x = -scale*Params.pos_offset_x(part_idx);
bbox.min_proj_y = -scale*Params.pos_offset_y(part_idx);

bbox.max_proj_x = bbox.min_proj_x + rect_width;
bbox.max_proj_y = bbox.min_proj_y + rect_height;

bbox.min_proj_y = bbox.min_proj_y + scale*Params.ext_y_neg(part_idx);
bbox.max_proj_y = bbox.max_proj_y - scale*Params.ext_y_pos(part_idx);
bbox.min_proj_x = bbox.min_proj_x * 0.7;
bbox.max_proj_x = bbox.max_proj_x * 0.7;

% match bboxes
correct = is_gt_match(gt_bbox, bbox);
end

function correct = is_gt_match(gt_bbox, bbox)

[gt_seg_len, gt_endpoint_top, gt_endpoint_bottom] = get_bbox_endpoints(gt_bbox);
[detect_seg_len, detect_endpoint_top, detect_endpoint_bottom] = get_bbox_endpoints(bbox);

match_top = (norm(gt_endpoint_top-detect_endpoint_top) < 0.5 * gt_seg_len);
match_bottom = (norm(gt_endpoint_bottom-detect_endpoint_bottom) < 0.5 * gt_seg_len);
correct = match_top && match_bottom;
end

function [len, endpoint_top, endpoint_bottom] = get_bbox_endpoints(bbox)
len = bbox.max_proj_y - bbox.min_proj_y;
endpoint_top = bbox.part_pos + bbox.min_proj_y*bbox.part_y_axis;
endpoint_bottom = bbox.part_pos + bbox.max_proj_y*bbox.part_y_axis;
end
