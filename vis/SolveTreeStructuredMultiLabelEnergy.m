%%
function [X E] = SolveTreeStructuredMultiLabelEnergy(Energy)

% Here we use the general O(N*K^2) algorithm, as we only use K=10
% To use a larger number of hypotheses, we need to switch to the
% Felzenswalb algorithm (distance transform) with complexity O(N*K)

[X E] = mexMinSum(Energy.Singleton, Energy.Struct, Energy.Edge);
