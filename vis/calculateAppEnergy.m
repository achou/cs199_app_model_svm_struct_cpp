function appEnergy = calculateAppEnergy( image, fg, bg, coverage, config, Params)
% Bucket the pixels
NPIXELS_PER_BUCKET = round(256/config(3));
fgtotal = sum(fg);
bgtotal = sum(bg);
% Add the prior scaled so that no evidence in either fg or bg
% leads to 0 energy.
prior = config(4);
fg = fg + prior*(fgtotal/bgtotal);
bg = bg + prior;
appWeight = config(32:41);
if length(appWeight) ~= Params.nParts
  disp('The number of appWeights must match nParts in calculateAppEnergy');
  keyboard
end
appEnergy = zeros( Params.nParts, Params.nHypotheses);
% Only use pixels that are >= FG_THRESH in the fg
FG_THRESH = config(5);
%coverage = coverage(coverage(:,4) >= FG_THRESH,1:3);
%coverage = coverage(:,1:3);
% iterate through each part
for i=1:Params.nParts
  covi = coverage(coverage(:,1) == i,2:4);
  % iterate through each hypothesis
  for j=1:Params.nHypotheses
    pixels = covi(covi(:,1) == j,2);
    probs = covi(covi(:,1) == j,3);
    %keyboard
    % iterate through each pixel
    cols = ceil( pixels / size(image,1));
    rows = mod( pixels-1, size(image,1)) + 1;
    %fprintf('%d, %d, %d\n', length(rows), min(pixels), max(pixels));
    %fprintf('%d, %d, %d, %d\n', min(cols), max(cols), min(rows), max(rows));
    for pi=1:length(cols)
      r = int32(floor(double(image(rows(pi), cols(pi), 1)) / NPIXELS_PER_BUCKET));
      g = int32(floor(double(image(rows(pi), cols(pi), 2)) / NPIXELS_PER_BUCKET));
      b = int32(floor(double(image(rows(pi), cols(pi), 3)) / NPIXELS_PER_BUCKET));
      index = (NPIXELS_PER_BUCKET*NPIXELS_PER_BUCKET*r) + ...
        int32(NPIXELS_PER_BUCKET*g) + int32(b) + 1;
      %fprintf('%d, %d, %d, %d, %d\n', r,g,b, index, NPIXELS_PER_BUCKET);
      if index > length(fg)
        disp('index out of bounds in calculateAppWeights');
        keyboard
      end
      %fprintf('%f, %f, %f, %f\n', fg(index), fgtotal, bg(index), bgtotal);
      fgprob = fg(index)/fgtotal;
      bgprob = bg(index)/bgtotal;
      %fprintf('%f, %f\n', fgprob, bgprob);
      ll = log(fgprob/bgprob);
      % scale the contribution by the probability of the pixel being in the fg
      appEnergy(i,j) = appEnergy(i,j) - 0.0015*appWeight(i)*probs(pi)*ll;
    end
  end
end
end
