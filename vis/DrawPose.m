function I = DrawPose(I, Bbox, correct)

for pidx = 1:length(Bbox);
    
    bbox = Bbox{pidx};
    
    %     part_width = Params.partdims(pidx, 1);
    %     part_length = Params.partdims(pidx, 2);
    %     if pidx == 9
    %         part_length = part_length * 0.7;
    %     end
    %     hidx = Sol.Pose(pidx);
    %     rot = PartHypo(pidx, hidx, 1);
    %     angle = (Params.min_rot+ (Params.max_rot-...
    %         Params.min_rot)/Params.rot_steps * (0.5+rot))/180*pi;
    %     part_x_axis = [ sin(angle) cos(angle) ];
    %     part_y_axis = [ cos(angle) -sin(angle)];
    %     y = PartHypo(pidx, hidx, 2);
    %     x = PartHypo(pidx, hidx, 3);
    %
    %     corner1 = [y, x] + part_y_axis * part_length/2 + ...
    %         part_x_axis * part_width/2 * 0.7;
    %     corner2 = [y, x] + part_y_axis * part_length/2 - ...
    %         part_x_axis * part_width/2 * 0.7;
    %     corner3 = [y, x] - part_y_axis * part_length/2 + ...
    %         part_x_axis * part_width/2 * 0.7;
    %     corner4 = [y, x] - part_y_axis * part_length/2 - ...
    %         part_x_axis * part_width/2 * 0.7;
    
    corner1 = bbox.part_pos + bbox.part_x_axis*bbox.min_proj_x + bbox.part_y_axis*bbox.min_proj_y;
    corner2 = bbox.part_pos + bbox.part_x_axis*bbox.min_proj_x + bbox.part_y_axis*bbox.max_proj_y;
    corner3 = bbox.part_pos + bbox.part_x_axis*bbox.max_proj_x + bbox.part_y_axis*bbox.min_proj_y;
    corner4 = bbox.part_pos + bbox.part_x_axis*bbox.max_proj_x + bbox.part_y_axis*bbox.max_proj_y;
    
    if correct(pidx)
        I = DrawWideLine(I, int32(corner1(2)), int32(corner1(1)), int32(corner2(2)), int32(corner2(1)), reshape([0 255 0], [1 1 3]));
        I = DrawWideLine(I, int32(corner2(2)), int32(corner2(1)), int32(corner4(2)), int32(corner4(1)), reshape([0 255 0], [1 1 3]));
        I = DrawWideLine(I, int32(corner4(2)), int32(corner4(1)), int32(corner3(2)), int32(corner3(1)), reshape([0 255 0], [1 1 3]));
        I = DrawWideLine(I, int32(corner3(2)), int32(corner3(1)), int32(corner1(2)), int32(corner1(1)), reshape([0 255 0], [1 1 3]));
    else
        I = DrawWideLine(I, int32(corner1(2)), int32(corner1(1)), int32(corner2(2)), int32(corner2(1)), reshape([255 0 0], [1 1 3]));
        I = DrawWideLine(I, int32(corner2(2)), int32(corner2(1)), int32(corner4(2)), int32(corner4(1)), reshape([255 0 0], [1 1 3]));
        I = DrawWideLine(I, int32(corner4(2)), int32(corner4(1)), int32(corner3(2)), int32(corner3(1)), reshape([255 0 0], [1 1 3]));
        I = DrawWideLine(I, int32(corner3(2)), int32(corner3(1)), int32(corner1(2)), int32(corner1(1)), reshape([255 0 0], [1 1 3]));
    end
end

 