%clear;
% if you want to change the shape of the person, look into this
Params = SetParameters();
DISPLAY = false;
%DISPLAY = true;
rand('seed',12345);
np1 = 0;
np2 = 0;
np3 = 0;
best_accuracy_possible = [];
baseline_accuracy = [];
baseline_total = zeros(Params.nParts,1);
appearance_total = zeros(Params.nParts,1);
best_total = zeros(Params.nParts,1);
accuracy = [];
singEnergies.pred= zeros(Params.nParts,1); 
singEnergies.best= zeros(Params.nParts,1);
pairEnergies.pred = zeros(Params.nJoints,1);
pairEnergies.best = zeros(Params.nJoints,1);
appEnergy.pred = []; appEnergy.best = []; appEnergy.gt=[];
appEnergies.pred = []; appEnergies.best = []; appEnergies.gt=[];
symEnergies.pred = [];%zeros(Params.nSym,1);
symEnergies.best = [];%zeros(Params.nSym,1);
%symEnergies.gt = zeros(Params.nSym,1);
% load data for all images
IMG_NUMS = 261:261;
for i=1:length(IMG_NUMS)
  imgnum=IMG_NUMS(i);
  if mod(imgnum,Params.print)==0
    disp(sprintf('Image number: %d',imgnum));
  end
  %img_name = sprintf('../../cs199_appearance_model/Ramanan_dataset/Ramanan_test_%d/im0%d.proprocessed.mat',...
  img_name = sprintf('matfiles%d/im%d.mat', Params.nHypotheses, imgnum);
  load (img_name);
  [H W C] = size(Image);
  if imgnum == 180 % it's black and white so replicate it 3 times
    tmp = Image;
    Image(:,:,1) = tmp;
    Image(:,:,2) = tmp;
    Image(:,:,3) = tmp;
  end

  % now we have Params.nHypotheses candidates for each of the 
  % Params.nParts body parts, with the
  % pairwise energies between each pair of them pre-computed
  % GtBbox: ground truth annotation
  % Image: rgb image
  % PartHypo: pre-filtered Params.nHypotheses candidates for each of 
  %           the Params.nParts body parts
  % JointCost: pre-computed pairwise energies between them

  % get the mapping to foreground image pixels
  Coverage = GetPartCoverage(H, W, PartHypo, Params);
  Coverage = Coverage(Coverage(:,4)>=0.5, :);
  if 0
    % This array specifies (for each body part hypothesis) which pixels it maps
    % to (with a weight). For example to draw the 3rd hypothesis for the 2nd
    % body part, just do this:
    pid = 10;
    hid = 2;
    %C = Coverage(Coverage(:,1)==pid, :);
    C = Coverage(Coverage(:,4)>=0.3,:);
    C = C(C(:,2)==hid,:);
    I = zeros(H*W,1);
    I(C(:,3)) = C(:,4);
    subplot(1,2,1);
    imshow(reshape(I, [H W]));
    subplot(1,2,2);
    imshow(Image);
    return
  end

  % specify random singleton energies 
  %Energy.Singleton = -(PartHypo(:,:,4)-max(max(PartHypo(:,:,4))));
  Singleton = -PartHypo(:,:,4);
  Energy.Singleton = Singleton;
  Energy.Struct = Params.joints;
  Energy.Edge = JointCost;
  Energy.FG_Map = Coverage;
  Energy.Symm = Params.symm;

  % infer the most probable configuration while taking appearance into account
  %[X1 E1 fg_vis1 bg_vis1 fg1 bg1 msg1 marginal1 appEnergy1] = ...
  %TODO
  X1 = loadHyps(i);
  X1'
  for h=1:10
    X1(9) = h;
    [correctness1 Bbox1] = EvaluateRamanan(X1, PartHypo, GtBbox, Params);
    accuracy = [accuracy; mean(correctness1)];
    appearance_total = appearance_total + correctness1;

    ResultImage = DrawPose(Image, Bbox1, correctness1); % red for incorrect
    subplot(2,5,h);
    pid = 10;
    hid = 2;
    %C = Coverage(Coverage(:,1)==pid, :);
    C = Coverage(Coverage(:,4)>=0.3,:);
    C = C(C(:,2)==h,:);
    I = zeros(H*W,1);
    I(C(:,3)) = C(:,4);
    I = reshape(I, [H W]);
    for dim=1:3
      %ResultImage(:,:,dim) = uint8(double(ResultImage(:,:,dim)).*I);
    end
    imshow(ResultImage);
    title(sprintf('hypothesis %d',h));
  end
end
