function Params = SetParameters()

% if you want to change the shape/size of the person, change these
Params.scale = 0.75; % size
Params.partdims = [25 45; 30 45; 30 45; 25 45; 20 30; 20 30; 20 30; 20 30; 25 10; 60 50]; % dimensions of body parts
Params.shape_threshold = 0.1; % threshold to cut off the Gaussian mask for each body part
% These spreads make sure there are approximately the same number
% of pixels in each bucket of the symmetric appearance pyramid.
Params.spreadx = 1.9;
Params.spready = 0.7;

% THESE ARE USED BY THE EVALUATION CODE, DO NOT CHANGE
Params.rot_steps = 24;
Params.min_rot = -180;
Params.max_rot = 180;
Params.joints = [1 2; 2 10; 4 3; 3 10; 5 6; 6 10; 8 7; 7 10; 9 10];
Params.symm = [1 4; 2 3; 5 8; 6 7]; % symmetric appearances
Params.pos_offset_x = [15 15 15 15 15 15 15 15 20 30];
Params.pos_offset_y = [24 23 23 24 16 16 16 16 24 40];
Params.window_size_x = [30 30 30 30 30 30 30 30 40 60];
Params.window_size_y = [49 46 46 49 33 32 32 33 49 80];
Params.ext_y_neg = [0 0 0 0 0 0 0 0 10 10];
Params.ext_y_pos = [0 0 0 0 0 0 0 0 10 10];

% use Lab instead of RGB (for appearance and contrast)
Params.useLab = 1;

%
Params.nParts = size(Params.partdims,1);
Params.nJoints = size(Params.joints,1);
Params.nSym = size(Params.symm,1);
Params.nHypotheses = 20;
Params.print = 1;
% the degree of each vertex
Params.degree = zeros(Params.nParts,1);
for i=1:length(Params.joints)
    Params.degree(Params.joints(i,1)) = Params.degree(Params.joints(i,1)) + 1;
    Params.degree(Params.joints(i,2)) = Params.degree(Params.joints(i,2)) + 1;
end

end
