function [np] = getNumPixels(Coverage, X, Params)
  % This array specifies (for each body part hypothesis) which pixels it maps
  % to (with a weight). For example to draw the 3rd hypothesis for the 2nd
  % body part, just do this:
  pix = [];
  for pid=1:Params.nParts
    hid = X(pid);
    C = Coverage(:,1:3);
    C = C(C(:,1)==pid,:);
    C = C(C(:,2)==hid,:);
    pix = [pix; C(:,3)];
  end
  np = length(unique(pix));
end
