function [X] = loadHyps(name, imgnum)
%name = ['../weights20overlapnorm/weights20abcdoverlapnorm', '3'];
%name = [name, '/test20_c_0.1_b_3_i_50_incb_1_samp_11.txt1'];
%name = '../tmptest';
X = load(name);
X = X(imgnum, :)';
%keyboard
end
