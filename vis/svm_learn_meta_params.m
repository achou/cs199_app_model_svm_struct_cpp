function [w] = svm_learn_meta_params()
% Uses SVM-struct to learn the weights used to combine 4 energy values: 
%   energy for singleton, pairwise, appearance, and symmetric appearance.

  addpath('~/classes/cs199/svm-struct-matlab');
  randn('state',0) ;
  rand('state',0) ;

  % Get the config params
  addpath('configs');
  [param.featureConfig, config_str, config_types] = configFeatures();
  [param.constraintConfig, config_str, config_types] = configConstraints();

  % ------------------------------------------------------------------
  %                                                      Generate data
  % ------------------------------------------------------------------

  Params = SetParameters();
  try
    load('cache/svm_learn_meta_params.mat');
  catch
    training_i = 1:100;
    training_i = 191:200;

    patterns = {} ;
    labels = {} ;
    for i=1:length(training_i)
      img_name = sprintf('Ramanan_test/im0%d.proprocessed.mat',training_i(i));
      load (img_name);
      if training_i(i) == 180 % it's black and white so replicate it 3 times
        tmp = Image;
        Image(:,:,1) = tmp;
        Image(:,:,2) = tmp;
        Image(:,:,3) = tmp;
      end
      [H W C] = size(Image);
      Energy.Singleton = -PartHypo(:,:,4);
      Energy.Struct = Params.joints;
      Energy.Edge = JointCost;
      Coverage = GetPartCoverage(H, W, PartHypo, Params);
      Energy.FG_Map = Coverage;
      Energy.Symm = Params.symm;
      patterns{i}.Energy = Energy;
      patterns{i}.Image = Image;
      patterns{i}.imgnum = training_i(i);
      [correctness Bbox corr_all] = ...
        EstimateRoofUsingGroundTruth(PartHypo, GtBbox, Params);
      labels{i} = corr_all;
    end
    save('cache/svm_learn_meta_params.mat','patterns','labels');
  end

  % ------------------------------------------------------------------
  %                                                    Run SVM struct
  % ------------------------------------------------------------------

  param.patterns = patterns ;
  param.labels = labels ;
  param.lossFn = @lossCB ;
  param.constraintFn  = @constraintCB ;
  param.featureFn = @featureCB ;
  param.dimension = 4 ;
  param.nParts = 10;
  param.nHyp = 10;
  param.nEdges = size(Params.joints,1);
  param.verbose = 1 ;
  param.c = 1.0;
  model = svm_struct_learn(sprintf(' -c %f -o 1 -v 1 ', param.c), param) ;
  w = model.w ;
end

% ------------------------------------------------------------------
%                                               SVM struct callbacks
% ------------------------------------------------------------------

function loss = lossCB(param, y, ybar)
  fprintf('Entering lossCB\n');
% y and ybar are both N*K binary matrices where 1 means the hypothesis is
% correct for that part. 
% y is the ground truth. ybar is a set of 10 hypotheses (1 per part).
% N = # of parts
% K = # of hypotheses per part
  loss = param.nParts - sum(sum(y.*ybar));
  nY = sum(sum(y));
  nYbar = sum(sum(ybar)); nYbar=nYbar(1,1);
  % sanity check
  P=param.nParts; H=param.nHyp; E=param.nEdges;
  if (nY~=param.nParts && nYbar~=param.nParts) || ...
      logical(min([size(y)~=[P,H], size(ybar)~=[P,H]]))
    disp('Problem in lossCB');
    keyboard
  end
  if param.verbose
    fprintf('delta = loss(%3d, %3d) = %f\n', full(nY), full(nYbar), full(loss)) ;
  end
  fprintf('Exiting lossCB\n');
end

function [] = checkConfig(c)
  weight_indices = [3,7,9,14];
  if min(c(weight_indices)) <= eps
    disp('Must change config');
    keyboard
  end
end
function psi = featureCB(param, x, y)
  fprintf('Entering featureCB\n');
% Need to check the config to make sure all weights are positive so that dividing
% by the weight doesnt result in a negative or NaN energy.
  checkConfig(param.featureConfig);
% Set Energy to be x.Energy but with the non-hypotheses of y set to 1e10 energy
  [Energy] = EliminateWrongUsingGroundTruth( param, x.Energy, y);
  fprintf('Running featureCB on image %d\n', x.imgnum);
  [hyp totalEnergy fg_model bg_model fg bg msg marg appWeight featEnergy] = ...
    SolveTreeEnergyWithAppearanceModel(Energy, x.Image, param.featureConfig);
  if param.verbose
    % check something?
  end
  % combine 4 values: energy for 
  % singleton, pairwise, appearance, and symmetric appearance.
  P=param.nParts; H=param.nHyp; E=param.nEdges;
  e.singleton = index(x.Energy.Singleton, 1:P, hyp);
  edges1 = x.Energy.Struct(:,1);
  edges2 = x.Energy.Struct(:,2);
  e.pairwise = index3d(x.Energy.Edge, 1:E, hyp(edges1), hyp(edges2));
  e.appearance = featEnergy(1)/param.featureConfig(3);
  e.symmetric = featEnergy(2)/param.featureConfig(9);
  %{
  e.singleton = rand();
  e.pairwise = rand();
  e.appearance = rand();
  e.symmetric = rand();
  %}
  psi = sparse([e.singleton;e.pairwise;e.appearance;e.symmetric]);
  fprintf('Exiting featureCB\n');
end
function config = updateConfigWeights(config, weights)
  if size(weights,1)~=4 || size(weights,2)~=1
    disp('weights is an unexpected size in updateConfigWeights');
    keyboard
  end
  w.singleton = weights(1);
  w.pairwise = weights(2);
  w.appearance = weights(3);
  w.symmetric = weights(4);
  disp('need to check config indices in updateConfigWeights');
  keyboard
  config(7) = w.singleton;
  config(14) = w.pairwise;
  config(3) = w.appearance;
  config(9) = w.symmetric;
  fprintf('Using weights: %f, %f, %f, %f\n',...
    weights(1),weights(2),weights(3),weights(4));
end
function [] = printHypotheses(h)
  for i=1:length(h)
    fprintf('%d ',h(i));
  end
  fprintf('\n');
end

function yhat = constraintCB(param, model, x, y)
  fprintf('Entering constraintCB\n');
% slack resaling: argmax_y delta(yi, y) (1 + <psi(x,y), w> - <psi(x,yi), w>)
% margin rescaling: argmax_y delta(yi, y) + <psi(x,y), w>

% USING MARGIN RESCALING
% Set Energy to be x.Energy but with the non-hypotheses of y set to the
% loss weight
  loss_weight = param.c;
  [Energy] = EliminateWrongUsingGroundTruth( param, x.Energy, y, loss_weight);
% Use the model weights to set the config weights
  config = updateConfigWeights(param.constraintConfig, model.w);
% Compute the most likely hypothesis
  fprintf('Running constraintCB on image %d\n', x.imgnum);
  [hyp totalEnergy fg_model bg_model fg bg msg marg appWeight featEnergy] = ...
    SolveTreeEnergyWithAppearanceModel(Energy, x.Image, config);
  printHypotheses(hyp);
% Transform hypotheses into full prediction matrix
  P=param.nParts; H=param.nHyp; E=param.nEdges;
  yhat = sparse(1:P,hyp,1,P,H);
  fprintf('Exiting constraintCB\n');
end
