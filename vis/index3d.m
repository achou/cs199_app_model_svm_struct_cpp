function [v] = index3d(M,r,c,d)
% pull out the <row,col,dim> pairs from M
  v = zeros(length(r),1);
  for i=1:length(r)
    v(i) = M(r(i),c(i),d(i));
  end
end
