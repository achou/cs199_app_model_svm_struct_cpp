/*=================================================================
 *
 * mexMinSum.cpp
 * by Huayan Wang <huayanw@cs.stanford.edu>
 *
 *=================================================================*/

#include <math.h>
#include "mex.h"
#include <stdio.h>
#include <stdlib.h>

/* Input Arguments */

#define Singleton_IN prhs[0]
#define Joints_IN  prhs[1]
#define Pairwise_IN prhs[2]

/* Output Arguments */

#define Label_OUT   plhs[0]
#define Energy_OUT   plhs[1]
#define Marginal_OUT  plhs[2]
#define Msg_OUT    plhs[3]

#if !defined(MAX)
#define MAX(A, B) ((A) > (B) ? (A) : (B))
#endif

#if !defined(MIN)
#define MIN(A, B) ((A) < (B) ? (A) : (B))
#endif

void mexFunction( int nlhs, mxArray *plhs[],
    int nrhs, const mxArray*prhs[] )

{

  /* Check for proper number of arguments */

  if (nrhs != 3) {
    mexErrMsgTxt("Three input arguments required.");
  } else if (nlhs > 4) {
    mexErrMsgTxt("Too many output arguments.");
  }

  /* Check the dimensions */

  // number of nodes
  int N = mxGetDimensions(Singleton_IN)[0];
  // number of possible assignments to each node
  int K = mxGetDimensions(Singleton_IN)[1];
  // number of edges
  int M = mxGetDimensions(Joints_IN)[0];

  /* Create matrices for the return argument */
  Marginal_OUT = mxCreateDoubleMatrix(N, K, mxREAL);
  Label_OUT = mxCreateDoubleMatrix(N, 1, mxREAL);
  Energy_OUT = mxCreateDoubleMatrix(1, 1, mxREAL);
  Msg_OUT = mxCreateDoubleMatrix(2*M, K, mxREAL);

  /* Assign pointers to the various parameters */

  double *singleton, *joints, *pairwise;
  singleton = mxGetPr(Singleton_IN);
  joints = mxGetPr(Joints_IN);
  pairwise = mxGetPr(Pairwise_IN);
  double *factor= mxGetPr(Marginal_OUT);
  double *label = mxGetPr(Label_OUT);
  double *energy = mxGetPr(Energy_OUT);

  // for keeping the forward messages
  double *fwdmsg = (double*)malloc(sizeof(double)*M*K);
  double *msg = (double*)malloc(sizeof(double)*K);

  int i, j, jidx;

  double *Msges = mxGetPr(Msg_OUT);

  // copy the marginal from the singleton
  for(i=0; i<N; i++){
    for(j=0; j<K; j++){
      factor[i+j*N] = singleton[i+j*N];
    }
  }

  int pfrom, pto;
  // forward pass
  for(jidx = 0; jidx < M; jidx++){
    pfrom = int(joints[jidx]) -1;
    pto = int(joints[jidx+M]) -1;

    for (i=0; i<K; i++){ // all states of "to"
      msg[i] = factor[pfrom+0*N] + pairwise[jidx+0*M+i*M*K];
      for (j=1; j<K; j++){ // all states of "from"
        if(factor[pfrom+j*N] + pairwise[jidx+j*M+i*M*K] < msg[i])
          msg[i] = factor[pfrom+j*N] + pairwise[jidx+j*M+i*M*K];
      }
      fwdmsg[jidx*K+i] = msg[i];
      Msges[jidx+i*2*M] = msg[i];
      factor[pto+i*N] += msg[i];
    }
    //printf("from %d, to %d\n", pfrom, pto);
  }

  // backward pass
  for(jidx = M-1; jidx >= 0; jidx--){
    pfrom = int(joints[jidx+M])-1;
    pto = int(joints[jidx])-1;

    for (i=0; i<K; i++){ // all states of "to"
      msg[i] = factor[pfrom+0*N] - fwdmsg[jidx*K+0] + pairwise[jidx+i*M+0*M*K];
      for (j=1; j<K; j++){ // all states of "from"
        if( factor[pfrom+j*N] - fwdmsg[jidx*K+j] + pairwise[jidx+i*M+j*M*K] < msg[i])
          msg[i] =factor[pfrom+j*N] - fwdmsg[jidx*K+j] + pairwise[jidx+i*M+j*M*K];

      }
      Msges[M+jidx+i*2*M] = msg[i];
      factor[pto+i*N] += msg[i];
    }
    //printf("from %d, to %d\n", pfrom, pto);
  }


  // decode the min-marginals
  double val;
  for(i=0; i<N; i++){
    label[i] = 1;
    val = factor[i+0*N];
    for(j=1; j<K; j++){
      if( factor[i+j*N] < val){
        val = factor[i+j*N];
        label[i] = j+1;
      }
    }
    //printf("%lf ", label[i]);
  }

  double E=0;
  int label1, label2;
  for (i=0; i<N; i++){
    E += singleton[int(i+(label[i]-1)*N)];
  }
  for(i=0; i<M; i++){
    label1 = (int)label[(int)joints[i]-1]-1;
    label2 = (int)label[(int)joints[i+M]-1]-1;
    E += pairwise[i+label1*M+label2*M*K];
  }

  energy[0] = E;

  free(fwdmsg);
  free(msg);
  return;
}


