%clear;
% if you want to change the shape of the person, look into this
Params = SetParameters();
DISPLAY = false;
%DISPLAY = true;
rand('seed',12345);
np1 = 0;
np2 = 0;
np3 = 0;
best_accuracy_possible = [];
baseline_accuracy = [];
baseline_total = zeros(Params.nParts,1);
appearance_total = zeros(Params.nParts,1);
best_total = zeros(Params.nParts,1);
accuracy = [];
singEnergies.pred= zeros(Params.nParts,1); 
singEnergies.best= zeros(Params.nParts,1);
pairEnergies.pred = zeros(Params.nJoints,1);
pairEnergies.best = zeros(Params.nJoints,1);
appEnergy.pred = []; appEnergy.best = []; appEnergy.gt=[];
appEnergies.pred = []; appEnergies.best = []; appEnergies.gt=[];
symEnergies.pred = [];%zeros(Params.nSym,1);
symEnergies.best = [];%zeros(Params.nSym,1);
%symEnergies.gt = zeros(Params.nSym,1);
% load data for all images
IMG_NUMS = 1:305;
%IMG_NUMS = 1:1;
for i=1:length(IMG_NUMS)
  imgnum=IMG_NUMS(i);
  if mod(imgnum,Params.print)==0
    disp(sprintf('Image number: %d',imgnum));
  end
  %img_name = sprintf('../../cs199_appearance_model/Ramanan_dataset/Ramanan_test_%d/im0%d.proprocessed.mat',...
  img_name = sprintf('matfiles%d/im%d.mat', Params.nHypotheses, imgnum);
  load (img_name);
  if imgnum<=100
    Image = imread(sprintf('../../cs199_appearance_model/Ramanan_image/im0%.3d.png', imgnum));
    %Image = double(Image)/256;
    %Image = imresize(Image, 4.0/3);
    %Image = uint8(Image*256);
    SCALE = 0.75;
    PartHypo(:,:,2:3) = SCALE*PartHypo(:,:,2:3);
    for j=1:10
      GtBbox{j}.part_pos = SCALE*GtBbox{j}.part_pos;
      GtBbox{j}.min_proj_x = SCALE*GtBbox{j}.min_proj_x;
      GtBbox{j}.min_proj_y = SCALE*GtBbox{j}.min_proj_y;
      GtBbox{j}.max_proj_x = SCALE*GtBbox{j}.max_proj_x;
      GtBbox{j}.max_proj_y = SCALE*GtBbox{j}.max_proj_y;
    end
  end
  [H W C] = size(Image);
  if C ==1 % it's black and white so replicate it 3 times
    tmp = Image;
    Image(:,:,1) = tmp;
    Image(:,:,2) = tmp;
    Image(:,:,3) = tmp;
  end

  % now we have Params.nHypotheses candidates for each of the 
  % Params.nParts body parts, with the
  % pairwise energies between each pair of them pre-computed
  % GtBbox: ground truth annotation
  % Image: rgb image
  % PartHypo: pre-filtered Params.nHypotheses candidates for each of 
  %           the Params.nParts body parts
  % JointCost: pre-computed pairwise energies between them

  % get the mapping to foreground image pixels
  Coverage = GetPartCoverage(H, W, PartHypo, Params);
  Coverage = Coverage(Coverage(:,4)>=0.5, :);
  if false
  %if true
    % This array specifies (for each body part hypothesis) which pixels it maps
    % to (with a weight). For example to draw the 3rd hypothesis for the 2nd
    % body part, just do this:
    pid = 10;
    hid = 1;
    %C = Coverage(Coverage(:,1)==pid, :);
    C = Coverage(Coverage(:,4)>=0.3,:);
    C = C(C(:,2)==hid,:);
    I = zeros(H*W,1);
    I(C(:,3)) = C(:,4);
    subplot(1,2,1);
    imshow(reshape(I, [H W]));
    subplot(1,2,2);
    imshow(Image);
    return
  end

  % specify random singleton energies 
  %Energy.Singleton = -(PartHypo(:,:,4)-max(max(PartHypo(:,:,4))));
  Singleton = -PartHypo(:,:,4);
  Energy.Singleton = Singleton;
  Energy.Struct = Params.joints;
  Energy.Edge = JointCost;
  Energy.FG_Map = Coverage;
  Energy.Symm = Params.symm;

  % infer the most probable configuration while taking appearance into account
  %[X1 E1 fg_vis1 bg_vis1 fg1 bg1 msg1 marginal1 appEnergy1] = ...
  %TODO
  if i>100
    X1 = loadHyps('../tmptest', i-100);
  else 
    X1 = loadHyps('../tmptrain', i);
  end
  X1'
  [correctness1 Bbox1] = EvaluateRamanan(X1, PartHypo, GtBbox, Params);
  accuracy = [accuracy; mean(correctness1)];
  appearance_total = appearance_total + correctness1;
  singEnergies.pred = singEnergies.pred + ...
    index(Singleton, 1:Params.nParts, X1);
  pairEnergies.pred = pairEnergies.pred +...
    index3d(JointCost, 1:Params.nJoints, ...
                       X1(Energy.Struct(:,1)), ...
                       X1(Energy.Struct(:,2)));

  % infer the most probable configuration
  [X2 E2] = SolveTreeStructuredMultiLabelEnergy(Energy);
  [correctness2 Bbox2] = EvaluateRamanan(X2, PartHypo, GtBbox, Params);
  baseline_accuracy = [baseline_accuracy; mean(correctness2)];
  baseline_total = baseline_total + correctness2;
  
  % What is the best possible we can do with the given detections
  [correctness3_tmp Bbox3_tmp corr_all] = ...
    EstimateRoofUsingGroundTruth(PartHypo, GtBbox, Params);

  % find the minimum energy model out of the best possible hypotheses
  [X3 E3] = SolveTreeEnergyForRoof( Params, Energy, corr_all);
  [correctness3 Bbox3] = EvaluateRamanan(X3, PartHypo, GtBbox, Params);
  best_accuracy_possible = [best_accuracy_possible; mean(correctness3)];
  best_total = best_total + correctness3;

  % This array specifies (for each body part hypothesis) which pixels it maps
  % to (with a weight). For example to draw the 3rd hypothesis for the 2nd
  % body part, just do this:
  if false
    pid = 3;
    hid = 1;
    C = gtCoverage(gtCoverage(:,1)==pid, :);
    C = C(C(:,2)==hid,:);
    I = zeros(H*W,1);
    I(C(:,3)) = C(:,4);
    figure
    subplot(1,2,1);
    imshow(reshape(I, [H W]));
    subplot(1,2,2);
    GTImage = DrawPose(Image, GtBbox, ones(Params.nParts,1));
    imshow(GTImage);
    return;
  end

  if true
    xorcor = correctness1 + correctness2;
    %only look at the differnces in legs
    xorcor = xorcor(1:4);
    xorcor = (xorcor == 1);
    if ~xorcor
      %continue
    end

    % draw results
    hfig = figure(1);
    ResultImage = DrawPose(Image, Bbox1, correctness1); % red for incorrect
    subaxis(1,4,1, 'Spacing', 0.03, 'Padding', 0, 'Margin', 0, 'PaddingTop', 0.05);
    imshow(ResultImage);
    title(sprintf('Our Method (%d Hyps)', Params.nHypotheses), 'FontWeight','bold');

    ResultImage = DrawPose(Image, Bbox2, correctness2); % red for incorrect
    subaxis(1,4,2, 'Spacing', 0.03, 'Padding', 0, 'Margin', 0, 'PaddingTop', 0.05);
    imshow(ResultImage);
    title(sprintf('Baseline (%d Hyps)', Params.nHypotheses), 'FontWeight','bold');

    ResultImage = DrawPose(Image, Bbox3, correctness3); % red for incorrect
    subaxis(1,4,3, 'Spacing', 0.03, 'Padding', 0, 'Margin', 0, 'PaddingTop', 0.05);
    imshow(ResultImage);
    title(sprintf('Best Possible (%d Hyps)', Params.nHypotheses), 'FontWeight','bold');

    % draw ground truth
    GTImage = DrawPose(Image, GtBbox, ones(Params.nParts,1));
    subaxis(1,4,4, 'Spacing', 0.03, 'Padding', 0, 'Margin', 0, 'PaddingTop', 0.05);
    imshow(GTImage);
    title('Ground Truth', 'FontWeight','bold');
    %set(hfig, 'Position', [100,100, 600, 1500]);
    hfig = figure(1);
    saveas(hfig, sprintf('../imgs/singapptext/%d.png', imgnum));
    close all
  end

  np1 = np1 + getNumPixels(Coverage, X1, Params);
  np2 = np2 + getNumPixels(Coverage, X2, Params);
  np3 = np3 + getNumPixels(Coverage, X3, Params);

  %title(sprintf('image %d', imgnum));

  %{
  % draw appearance model for my predictions
  subplot(3,4,5);
  imshow(fg_vis1);
  subplot(3,4,9);
  imshow(bg_vis1);
  % draw appearance model for best possible
  subplot(3,4,7);
  imshow(fg_vis4);
  subplot(3,4,11);
  imshow(bg_vis4);
  % draw appearance model for ground truth
  subplot(3,4,8);
  imshow(fg_vis5);
  subplot(3,4,12);
  imshow(bg_vis5);
  %}
end
% 595990      573243      585162
[np1, np2, np3]
