function Img = DrawWideLine(Img, X0, Y0, X1, Y1, nG)

Img = func_DrawLine(Img, X0, Y0, X1, Y1, nG);
Img = func_DrawLine(Img, X0+1, Y0, X1+1, Y1, nG);
Img = func_DrawLine(Img, X0, Y0+1, X1, Y1+1, nG);
Img = func_DrawLine(Img, X0, Y0-1, X1, Y1-1, nG);
Img = func_DrawLine(Img, X0-1, Y0, X1-1, Y1, nG);