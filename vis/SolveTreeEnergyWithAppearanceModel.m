% fg_vis and bg_vis are visualizations of the fg and bg models
function [Hypotheses totalEnergy fg_vis bg_vis fg bg msg Marginal...
  appEnergy featEnergy inferenceEnergy symEnergy] = ...
  SolveTreeEnergyWithAppearanceModel( Energy, Image, config, Params)

% Here we use the general O(N*K^2) algorithm, as we only use K=10
% To use a larger number of hypotheses, we need to switch to the
% Felzenswalb algorithm (distance transform) with complexity O(N*K)

% Adjust the Singleton Energies by the image sizes
%{
for p=1:Param.nParts
  for h=1:Params.nHypotheses
    n = length(find(Energy.FG_Map(:,1)==p & Energy.FG_Map(:,2)==h));
    Energy.Singleton(p,h) = Energy.Singleton(p,h)/n;
  end
end
%}

% For some reason mex puts columns contiguously instead of rows
[H W C] = size(Image);
imgsize = prod(size(Image));
iv = zeros(imgsize,1);
r = reshape(Image(:,:,1),H*W,1);
g = reshape(Image(:,:,2),H*W,1);
b = reshape(Image(:,:,3),H*W,1);
iv(1:3:imgsize-2) = r;
iv(2:3:imgsize-1) = g;
iv(3:3:imgsize  ) = b;
iv = uint8(iv);

%disp('Calling mex');
[Hypotheses, totalEnergy, Marginal, msg, Orig_Marginal, fg, bg, ...
  appEnergy, featEnergy, inferenceEnergy, symEnergy] = ...
  mexMinSumWithAppearance(Energy.Singleton, Energy.Struct, ...
    Energy.Edge, iv, Energy.FG_Map', Energy.Symm, config);
symDims = [Params.nSym,Params.nHypotheses,Params.nHypotheses];
symEnergy= reshape( symEnergy, symDims);
%disp('Returned from mex');
[fg_vis, bg_vis] = computeAppModel(fg,bg,50);
end

% fg and bg are the models. 
% P is the desired number of pixels on the edge of the visual of the model.
function [fg_vis, bg_vis] = computeAppModel(fg,bg,P)
%disp('starting computeAppModel');
N = round(length(fg)^(1/3));
%{
if N~=16
  disp('N is wrong');
  keyboard
end
%}
N2 = N*N;
P*P;
fg_vis = zeros(P*P,3);
%disp('D');
bg_vis = zeros(P*P,3);
%disp('E');
vppf = sum(fg)/(P*P); % value per pixel for foreground
vppb = sum(bg)/(P*P); % value per pixel for background
%{
if vppf<eps || vppf>1e12 || vppb<eps || vppb>1e12
  disp('vppf or vppb out of range');
  keyboard
end
%}
fi = 1; bi = 1;
for i=0:length(fg)-1
  %fprintf('computeAppModel iter %d of %d\n',i,length(fg));
  num_fg = floor(fg(i+1)/vppf);
  if num_fg > 0
    %disp('J');
    fg_vis(fi:fi+num_fg-1, 1) = floor(i/N2);
    %disp('K');
    fg_vis(fi:fi+num_fg-1, 2) = floor(mod(i,N2)/N);
    %disp('L');
    fg_vis(fi:fi+num_fg-1, 3) = mod(i,N);
    %disp('M');
    fi = fi + num_fg;
    %disp('N');
  end
  num_bg = floor(bg(i+1)/vppb);
  if num_bg > 0
    %disp('Q');
    bg_vis(bi:bi+num_bg-1, 1) = floor(i/N2);
    %disp('R');
    bg_vis(bi:bi+num_bg-1, 2) = floor(mod(i,N2)/N);
    %disp('S');
    bg_vis(bi:bi+num_bg-1, 3) = mod(i,N);
    %disp('T');
    bi = bi + num_bg;
  end
  %disp('V');
end
%disp('W');
fg_vis_tmp = (fg_vis+0.5)*(255/N);
%disp('X');
bg_vis_tmp = (bg_vis+0.5)*(255/N);
%disp('Y');
fg_vis = uint8(reshape(fg_vis_tmp, [P,P,3]));
bg_vis = uint8(reshape(bg_vis_tmp, [P,P,3]));
%disp('Z');
%imshow(fg_vis);
%keyboard
end
