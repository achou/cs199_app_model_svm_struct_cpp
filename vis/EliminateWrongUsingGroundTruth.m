function [Energy] = EliminateWrongUsingGroundTruth( Params, Energy, correct,...
  loss_weight)
if nargin < 4
  loss_weight = 1e10;
end
cor_pid = sum(correct,2);
for pid=1:Params.nParts
  if cor_pid(pid) == 0
    continue;
  end
  for hid=1:Params.nHypotheses
    if ~correct(pid,hid)
      Energy.Singleton(pid,hid) = Energy.Singleton(pid,hid) + loss_weight;
    end
  end
end
end
