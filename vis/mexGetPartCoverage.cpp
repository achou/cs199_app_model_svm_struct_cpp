// Identify pixels covered to each part hypothesis
// by Huayan Wang <huayanw@cs.stanford.edu>

// calling syntax in Matlab:
// [Coverage EdgeCnt] = mexGetPartCOverage(H, W, PartHypo, PartDims, Params);

// INPUT:
// H: image height
// W: image width
// PartHypo: K x M x 3, K is the number of parts,
//           M is the number of hypotheses per part,
// PartDims: K x 2, [length, width] of each part
// Params: various parameters

// OUTPUT:
// Coverage: [ part_idx, hypo_idx, pixel_idx, weight ;
//                ...... ;
//                ...... ]
// EdgeCnt: number of rows in the above matrix

#include <math.h>
#include "mex.h"
#include <stdio.h>
#include <stdlib.h>
#include "globals.h"

#if !defined(MAX)
#define MAX(A, B) ((A) > (B) ? (A) : (B))
#endif

#if !defined(MIN)
#define MIN(A, B) ((A) < (B) ? (A) : (B))
#endif

long EstimateMaxNumEdges(int K, int M, double scale, double* partdims)
{
  long N = 0;
  for(int pid = 0; pid<K; pid++){
    double wid = scale*partdims[pid];
    double len = scale*partdims[pid+K];
    N += long(len*wid);
  }
  return (long)(N*M*2.0);
  return (long)(N*M*1.5);
}

void mexFunction( int nlhs, mxArray *plhs[],
  int nrhs, const mxArray*prhs[] ){
  /* Check for proper number of arguments */
  if (nrhs != 5) {
    mexErrMsgTxt("Incorrect number of input arguments.");
  } else if (nlhs > 2) {
    mexErrMsgTxt("Too many output arguments.");
  }

  if(mxGetNumberOfDimensions(prhs[0]) != 2)
   mexErrMsgTxt("Arg 0 not of the right dimensions");
  if(mxGetNumberOfDimensions(prhs[1]) != 2)
   mexErrMsgTxt("Arg 1 not of the right dimensions");
  if(mxGetNumberOfDimensions(prhs[2]) != 3)
   mexErrMsgTxt("Arg 2 not of the right dimensions");
  if(mxGetNumberOfDimensions(prhs[3]) != 2)
   mexErrMsgTxt("Arg 3 not of the right dimensions");
  if(mxGetNumberOfDimensions(prhs[4]) != 2)
   mexErrMsgTxt("Arg 4 not of the right dimensions");
  int K = mxGetDimensions(prhs[2])[0];  // number of parts
  int M = mxGetDimensions(prhs[2])[1];  // number of hypotheses per part

  mxAssert(1==mxGetDimensions(prhs[0])[0], "1!=mxGetDimensions(prhs[0])[0]");
  mxAssert(1==mxGetDimensions(prhs[0])[1], "1!=mxGetDimensions(prhs[0])[1]");
  mxAssert(1==mxGetDimensions(prhs[1])[0], "1!=mxGetDimensions(prhs[1])[0]");
  mxAssert(1==mxGetDimensions(prhs[1])[1], "1!=mxGetDimensions(prhs[1])[1]");
  mxAssert(K==mxGetDimensions(prhs[3])[0], "K!=mxGetDimensions(prhs[3])[0]");
  mxAssert(2==mxGetDimensions(prhs[3])[1], "2!=mxGetDimensions(prhs[3])[1]");
  mxAssert(8==mxGetDimensions(prhs[4])[0], "6!=mxGetDimensions(prhs[4])[0]");

  double *parthypo, *partdims, *params, *coverage, *edgecnt;
  int h = mxGetPr(prhs[0])[0];
  int w = mxGetPr(prhs[1])[0];
  parthypo = mxGetPr(prhs[2]);
  partdims = mxGetPr(prhs[3]);
  params = mxGetPr(prhs[4]);

  double scale = params[0];
  double num_rot_steps = params[1];
  double min_rot = params[2];
  double max_rot = params[3];
  double pi = params[4];
  double shape_threshold = params[5];
  double spreadx = params[6];
  double spready = params[7];
  long N = EstimateMaxNumEdges(K, M, scale, partdims);

  plhs[0] = mxCreateDoubleMatrix(N, 3+MAX_SUBPART_LEVEL, mxREAL);
  plhs[1] = mxCreateDoubleMatrix(1, 1, mxREAL);

  coverage = mxGetPr(plhs[0]);
  edgecnt = mxGetPr(plhs[1]);

  long edgeCnt = 0;
  double rot_step = (max_rot-min_rot)/(double)num_rot_steps;
  for(int pid = 0; pid < K; pid ++){
    double wid = scale*partdims[pid]/2.0;
    double len = scale*partdims[pid+K]/2.0;
    for(int hid = 0; hid < M; hid++){
      double rot = parthypo[pid + hid*K];
      double ypos = parthypo[pid + hid*K + K*M];
      double xpos = parthypo[pid + hid*K + 2*K*M];

      double angle = (min_rot + rot_step * (0.5+(double)rot))/180*pi;
      double x0 = sin(angle); double x1 = cos(angle);
      double y0 = cos(angle); double y1 = -sin(angle);
      // Iterate over every pixel in the image
      for (int x=0; x<w; x++){
        for(int y=0; y<h; y++){
          double xrot = (x-xpos)*x1 + (y-ypos)*x0;
          double yrot = (x-xpos)*y1 + (y-ypos)*y0;
          // the confidence fraction that this pixel is in the fg
          double d2skeleton =
            fabs(xrot) / (wid/2.0);
          double d2end =
            MAX(fabs(yrot) - len, 0) / (wid/2.0);
          double a = exp(-(d2skeleton*d2skeleton + d2end*d2end));
          // Only write pixels that are reasonably likely to be fg
          if(a > shape_threshold){
            // write pixels
            coverage[edgeCnt+0*N] = pid+1;
            coverage[edgeCnt+1*N] = hid+1;
            coverage[edgeCnt+2*N] = y+x*h+1;
            coverage[edgeCnt+3*N] = a;
            // These spreads make sure there are approximately the same number
            // of pixels in each bucket.
            double xpct = MIN(MAX( (spreadx*xrot+wid)/(2*wid), 0), 1.0-FLT_EPSILON);
            double ypct = MIN(MAX( (spready*yrot+len)/(2*len), 0), 1.0-FLT_EPSILON);
            // subparts
            for(int i=2; i<=MAX_SUBPART_LEVEL; i++){
              // x and y percentages of the way through the part
              coverage[edgeCnt+(2+i)*N] = i*(int(i*xpct)) + int(i*ypct) + 1;
            }
            edgeCnt++;
            mxAssert(edgeCnt < N, "edge count overflow");
          }
        }
      }
    }
  }
  edgecnt[0] = edgeCnt;
}
