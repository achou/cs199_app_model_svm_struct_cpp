function [v] = index(M,r,c)
% pull out the <row,col> pairs from M
  v = zeros(length(r),1);
  for i=1:length(r)
    v(i) = M(r(i),c(i));
  end
end
