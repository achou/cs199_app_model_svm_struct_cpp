function [] = saveData(folder)
unix(sprintf('mkdir -p %s', folder));
unix(sprintf('mkdir -p %s/groundtruth/', folder));
unix(sprintf('mkdir -p %s/singletons/', folder));
unix(sprintf('mkdir -p %s/pairwise/', folder));
unix(sprintf('mkdir -p %s/images/', folder));
unix(sprintf('mkdir -p %s/fgmaps/', folder));
Params = SetParameters();
IMG_NUMS = 101:305;
Params.nHypotheses = 500;
IMG_NUMS = 101:109;
addpath('configs');
e1=[]; e4=[]; e5=[];
for i=1:length(IMG_NUMS)
  imgnum=IMG_NUMS(i);
  img_name = sprintf('Ramanan_dataset/Ramanan_test_%d/im0%d.proprocessed.mat',...
    Params.nHypotheses, imgnum);
  load (img_name);
  [H W C] = size(Image);
  if imgnum == 180 % it's black and white so replicate it 3 times
    tmp = Image;
    Image(:,:,1) = tmp;
    Image(:,:,2) = tmp;
    Image(:,:,3) = tmp;
  end

  % now we have 10 candidates for each of the 10 body parts, with the
  % pairwise energies between each pair of them pre-computed
  % GtBbox: ground truth annotation
  % Image: rgb image
  % PartHypo: pre-filtered 10 candidates for each of the 10 body parts
  % JointCost: pre-computed pairwise energies between them

  % get the mapping to foreground image pixels
  Coverage = GetPartCoverage(H, W, PartHypo, Params);
  Singleton = -PartHypo(:,:,4);
  [correctness Bbox corr_all] = ...
    EstimateRoofUsingGroundTruth(PartHypo, GtBbox, Params);
  % write files
  writeGroundTruth(corr_all,imgnum,folder);
  writeSingleton( Singleton, imgnum,folder);
  writePairwise(JointCost,imgnum,folder);
  writeImage(Image,imgnum,folder);
  writeFgMap(Coverage,imgnum,folder);
end
end
function [] = writeGroundTruth(g,imgnum,folder)
filename = sprintf('%s/groundtruth/groundtruth_%d.txt', folder,imgnum);
fid = fopen(filename,'w');
formatstring = '\n';
for i=1:size(g,2)
  formatstring = ['%d ' formatstring];
end
fprintf(fid,formatstring, g');
fclose(fid);
end
function [] = writeSingleton(s,imgnum,folder)
filename = sprintf('%s/singletons/singleton_%d.txt', folder,imgnum);
fid = fopen(filename,'w');
formatstring = '\n';
for i=1:size(s,2)
  formatstring = ['%f ' formatstring];
end
fprintf(fid,formatstring, s');
fclose(fid);
end
function [] = writePairwise(p,imgnum,folder)
filename = sprintf('%s/pairwise/pairwise_%d.txt', folder,imgnum);
[a b c] = size(p);
formatstring = '\n';
for i=1:c
  formatstring = ['%f ' formatstring];
end
fid = fopen(filename,'w');
for i=1:a
  for j=1:b
    fprintf(fid, formatstring, reshape(p(i,j,:),c,1));
  end
end
fclose(fid);
end
function [] = writeImage(im,imgnum,folder)
filename = sprintf('%s/images/image_%d.txt', folder,imgnum);
[a b c] = size(im);
if c~=3
  disp('Theres a problem in writeImage');
  keyboard
end
imgsize = a*b;
formatstring = '%d %d %d\n';
fid = fopen(filename,'w');
fprintf(fid, '%d\n', imgsize);
for i=1:a
  for j=1:b
    fprintf(fid, formatstring, reshape(im(i,j,:),c,1));
  end
end
fclose(fid);
end
function [] = writeFgMap(m,imgnum,folder)
I = find(m(:,4)>=0.5);
if size(m,2)~=7
  disp('Problem in writeFgMap.');
  disp('Must change m = m(I,[1:3, 5:7]); indices.');
  disp('Also need to change formatstring.');
end
m = m(I,[1:3, 5:7]);
filename = sprintf('%s/fgmaps/fgmap_%d.txt', folder,imgnum);
[a b] = size(m);
formatstring = '%d %d %d %d %d %d\n';
fid = fopen(filename,'w');
fprintf(fid, '%d\n', a);
fprintf(fid, formatstring, m');
fclose(fid);
end
