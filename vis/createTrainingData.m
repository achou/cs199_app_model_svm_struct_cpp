Params = SetParameters();

NEWH = 20;
if NEWH >= Params.nHypotheses
  disp('NEWH must be less than old h');
  return
end
IMG_NUMS = 101:305;
IMG_NUMS = 0:99;
for i=1:length(IMG_NUMS)
  imgnum=IMG_NUMS(i);
  if mod(imgnum,Params.print)==0
    disp(sprintf('Image number: %d',imgnum));
  end
  %img_name = sprintf('../../cs199_appearance_model/Ramanan_dataset/Ramanan_test_%d/im0%d.proprocessed.mat',...
  img_name = sprintf('../../cs199_appearance_model/Ramanan_dataset/Ramanan_train_%d/img0%.3d.proprocessed.mat',...
    Params.nHypotheses, imgnum);
  load (img_name);
  JointCost = JointCost(:, 1:NEWH, 1:NEWH);
  PartHypo = PartHypo(:, 1:NEWH, :);

  num = imgnum;
  if IMG_NUMS(1) == 0
    num = imgnum+1;
  end
  save(sprintf('matfiles20/im%d.mat', num), 'GtBbox', 'Image', 'JointCost', 'PartHypo');
end
