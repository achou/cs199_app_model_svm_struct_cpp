function Coverage = GetGtCoverage(H, W, GtBbox, Params)

% Identify the pixels covered by each part hypothesis:
gt = zeros(Params.nParts,1,3);
for p=1:Params.nParts
  % rot is is degrees
  rot = (180/pi)*atan2(GtBbox{p}.part_x_axis(2), GtBbox{p}.part_x_axis(1));
  rot = rot - Params.min_rot;
  rot = rot / (360/Params.rot_steps);
  gt(p,1,1) = round(rot);
  gt(p,1,3) = GtBbox{p}.part_pos(1);
  gt(p,1,2) = GtBbox{p}.part_pos(2);
  Params.partdims(p,1) = GtBbox{p}.max_proj_y-GtBbox{p}.min_proj_y;
  Params.partdims(p,2) = GtBbox{p}.max_proj_x-GtBbox{p}.min_proj_x;
end

para = [Params.scale; ...
    Params.rot_steps; ...
    Params.min_rot; ...
    Params.max_rot; ...
    pi; ...
    Params.shape_threshold;...
    Params.spreadx;...
    Params.spready;...
    ];

[Coverage EdgeCnt] = mexGetPartCoverage(H, W, gt, Params.partdims, para);
Coverage = Coverage(1:EdgeCnt,:);
end
