function [] = writeLogs(config, config_str, config_types, Params, accuracy, ...
  baseline_accuracy, best_accuracy_possible, totals, appEnergy, appEnergies)
%TODO
fid = fopen('logs/log.txt','a');
for i=1:length(config)
  fprintf(fid,['%s: ' config_types{i} '\n'],config_str{i},config(i));
end
fprintf(fid,'              ACCURACY: %f\n',accuracy);
fprintf(fid,'     BASELINE ACCURACY: %f\n',baseline_accuracy);
fprintf(fid,'BEST POSSIBLE ACCURACY: %f\n',best_accuracy_possible);
fprintf(fid,'Scores For:\n');
fprintf(fid,'Prediction, Baseline, Best Possible\n');
fprintf(fid,'    %4d,      %4d,      %4d\n',totals(:,1:3)');
%fprintf(fid,'App Energy: pred %f, best %f, gt %f\n',...
%  appEnergy.pred, appEnergy.best, appEnergy.gt);
fprintf(fid,'AppEnergy for part %d: Pred: %f, Best: %f, GT: %f\n',...
  [1:Params.nParts; sum(appEnergies.pred,1); sum(appEnergies.best,1); ...
  sum(appEnergies.gt,1)]);
fprintf(fid,'___________________________________________________\n');
fclose(fid);
end
