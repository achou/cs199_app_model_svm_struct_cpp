/***********************************************************************/
/*   svm_struct_api.c*/
/*   Definition of API for attaching implementing SVM learning of*/
/*   structures (e.g. parsing, multi-label classification, HMM)*/
/**/
/*   Author: Thorsten Joachims*/
/*   Date: 03.07.04*/
/**/
/*   Copyright (c) 2004  Thorsten Joachims - All rights reserved*/
/**/
/*   This software is available for non-commercial use only. It must   */
/*   not be modified and distributed without prior permission of the   */
/*   author. The author is not responsible for implications from the   */
/*   use of this software.*/
/***********************************************************************/

#include <limits>
#include <fstream>
#include <sstream>
#include <string>
#include <fcntl.h>
#include <sys/wait.h>
#include <stdio.h>
#include <iostream>
#include <sys/mman.h>
#include <unistd.h>
#include "svm_struct/svm_struct_common.h"
#include "svm_struct_api.h"
#include "common.h"
#include "features.h"
#include "featureBank.h"

using namespace std;
//TODO put these somewhere
int nlogs, numnlogn;
double *logs = NULL;
double *nlogn = NULL;

const int READ_BINARY= 1;
const int DEBUG = 1;
const int DEBUG_LOSS= 0;
/*
const int best_ramanan_scales[] = {
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16};
  */
const int best_ramanan_scales[] = {
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
//1
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
//2
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
//3
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
//4
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
//5
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
//6
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
//7
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0 //748
};
#if RAMANAN
const int DEFAULT_SCALE = 4;
const int singleton_indices[] = {0,1, // head+neck
  2,3,4,5,6, // left arm
  7,8,9, // left leg
  2,3,4,5,6, // right arm
  7,8,9}; // right leg
// # of duplicates in each position of singleton_indices
const int singleton_duplicates[] = {1,1, // head+neck
  2,2,2,2,2,
  2,2,2,
  2,2,2,2,2,
  2,2,2};
const int pairwise_indices[] = {0, // neck
  0,0,0,0,0, // left arm
  0,0,0, // left leg
  0,0,0,0,0, // right arm
  0,0,0}; // left leg
const int pairwise_duplicates[] = {17,//neck
  17,17,17,17,17,
  17,17,17,
  17,17,17,17,17,
  17,17,17};
const int overlap_indices[] = { 0, 0, 0, 0, // arms
                                0,0,0,0,
                                0,0,0,0,
                                0,0,0,0};
const int overlap_duplicates[] = {16,16,16,16,16,16,16,16,
                                  16,16,16,16,16,16,16,16};
#else
const int DEFAULT_SCALE = 0; // in the middle
const int singleton_indices[] = {0,1,1,0,2,3,3,2,4,5};
// # of duplicates in each position of singleton_indices
const int singleton_duplicates[] = {2,2,2,2,2,2,2,2,1,1};
const int pairwise_indices[] = {0,0,0,0,0,0,0,0,0};
const int pairwise_duplicates[] = {9,9,9,9,9,9,9,9,9};
/*
const int pairwise_indices[] = {0,1,0,1,2,3,2,3,4};
const int pairwise_duplicates[] = {2,2,2,2,2,2,2,2,1};
*/
//const int appearance_indices[] = {0,1,1,0,2,3,3,2,4,5};
//const int symmetric_indices[] = {
const int overlap_indices[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                0, 0, 0, 0, 0, 0, 0, 0,
                                0, 0, 0, 0, 0, 0, 0,
                                0, 0, 0, 0, 0, 0,
                                0, 0, 0, 0, 0,
                                0, 0, 0, 0,
                                0, 0, 0,
                                0, 0,
                                0};
const int overlap_duplicates[] = {45,45,45,45,45,45,45,45,45,
                                  45,45,45,45,45,45,45,45,
                                  45,45,45,45,45,45,45,
                                  45,45,45,45,45,45,
                                  45,45,45,45,45,
                                  45,45,45,45,
                                  45,45,45,
                                  45,45,
                                  45};
/*
const int overlap_indices[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8,//part 0
                                9, 1,10,11,12,13,14,15,   //part 1
                                0,13,12,11,10,14,15,      //part 2
                                6, 5, 4, 3, 7, 8,         //part 3
                                16,17,18,19,20,           //part 4
                                21,17,22,23,              //part 5
                                16,22,23,                 //part 6
                                19,20,                    //part 7
                                24};                      //part 8
const int overlap_duplicates[] = {2,2,1,2,2,2,2,2,2,
                                  1,2,2,2,2,2,2,2,
                                  2,2,2,2,2,2,2,
                                  2,2,2,2,2,2,
                                  2,2,1,2,2,
                                  1,2,2,2,
                                  2,2,2,
                                  2,2,
                                  1};
                                  */
#endif
// LAB, L, A, B, textures. A and B share a weight as do the 17 textures.
const int appearance_indices[] = {
  0,1,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3};
const int appearance_duplicates[] = {
  1,1,2,2,17,17,17,17,17,17,17,17,17,17,17,
  17,17,17,17,17,17,17,17,17,17,17};
void printHypotheses(int *hids){
  printf("Hypotheses: ");
  for(int i=0; i<NPARTS; i++)
    printf("%d ", hids[i]);
  printf("\n");
}
void printXY(PATTERN *x){
  for(int i=0; i<NPARTS; i++){
    for(int j=0; j<NHYPS; j++)
      printf("%d ", x->y[i][j]);
    printf("\n");
  }
}
void printY(LABEL *y){
  printf("Y: ");
  for(int i=0; i<NPARTS; i++){
    printf("%d ", y->y[i]);
    /*
    for(int j=0; j<NHYPS; j++){
      printf("%d ", y->y[i][j]);
    }*/
  }
  printf("\n");
}
void printWeights(STRUCTMODEL *sm){
  printf("%ld weights:\n", sm->sizePsi);
  // w is indexed starting at 1
  for(int i=1; i<=sm->sizePsi; i++)
    printf("%lf ", sm->w[i]);
  printf("\n");
}
void printStructModel(STRUCTMODEL *sm){
  printf("STRUCTMODEL: \n");
  printWeights(sm);
  printf("%lx\n", (unsigned long)sm->svm_model);
}
void printSvec(SVECTOR *v){
  printf("SVECTOR:\n");
  printf("words: %lx\n", (unsigned long)v->words);
  printf("twonorm_sq: %lf\n", v->twonorm_sq);
  printf("userdefined: %lx\n", (unsigned long)v->userdefined);
  printf("kernel_id: %ld\n", v->kernel_id);
  printf("next: %lx\n", (unsigned long)v->next);
  printf("factor: %lf\n", v->factor);
}
void printSvec2(SVECTOR *vec){
  for(int i=0; i<NFEATURES; i++) printf("%lf ", vec->words[i].weight);
  printf("\n");
}
void printFeatures(FEATURES *f){
  printf("FEATURES:\nSingleton: ");
  for(int i=0; i<NPARTS; i++)
    printf("%f ", f->singleton[i]);
  printf("\nPairwise: ");
  for(int i=0; i<NJOINTS; i++)
    printf("%f ", f->pairwise[i]);
  printf("\nAppearance: ");
  for(int i=0; i<NAPP; i++)
    printf("%f ", f->appearance[i]);
  printf("\nSymmetry: ");
  for(int i=0; i<NSYMS; i++)
    printf("%f ", f->sym[i]);
  printf("\nOverlap: ");
  for(int i=0; i<NOVERLAP; i++)
    printf("%f ", f->overlap[i]);
  printf("\n");
}
void printSingleton(PATTERN *x){
  printf("SINGLETON: \n");
  for(int i=0; i<NPARTS; i++){
    for(int j=0; j<NHYPS; j++){
      printf("%f ", x->singleton[i][j]);
    }
    printf("\n");
  }
}

void        svm_struct_learn_api_init(int argc, char* argv[]){
  /* Called in learning part before anything else is done to allow
     any initializations that might be necessary. */
}

void        svm_struct_learn_api_exit(){
  /* Called in learning part at the very end to allow any clean-up
     that might be necessary. */
}

void        svm_struct_classify_api_init(int argc, char* argv[]){
  /* Called in prediction part before anything else is done to allow
     any initializations that might be necessary. */
}

void        svm_struct_classify_api_exit(){
  /* Called in prediction part at the very end to allow any clean-up
     that might be necessary. */
}
// Callback functions that are used to change whether minSumWithAppearance
// actually uses a min or a max.
int minIsBetter(double old, double cur){
  return cur<old;
}
int maxIsBetter(double old, double cur){
  return cur>old;
}
void readmmap(char *data, int filesize, const string &path){
  //printf("trying to open %s. of size %d\n", path.c_str(), filesize);
  int fd = open(path.c_str(), O_RDONLY);
  if (fd < 0){
    cout << path << endl;
    Error("Error opening file for reading");
  }
  char *map = (char*) mmap(0, filesize, PROT_READ, MAP_SHARED, fd, 0);
  if (map == MAP_FAILED) {
    close(fd);
    Error("Error mmapping the file");
  }
  memcpy(data, map, filesize);
  if (munmap(map, filesize) == -1) {
    Error("Error un-mmapping the file");
  }
  close(fd);
}
void writemmap(char *data, int filesize, const string &path){
  /* Open a file for writing.
   *  - Creating the file if it doesn't exist.
   *  - Truncating it to 0 size if it already exists. (not really needed)
   * Note: "O_WRONLY" mode is not sufficient when mmaping.
   */
  cout << "Writing to: "<< path <<endl;
  int fd = open( path.c_str(), O_RDWR | O_CREAT | O_TRUNC, (mode_t)0600);
  if (fd == -1) {
    cout << path <<endl;
    Error("Error opening file for writing");
  }

  // Stretch the file size to the size of the (mmapped) array of ints
  int result = lseek(fd, filesize-1, SEEK_SET);
  if (result == -1) {
    close(fd);
    Error("Error calling lseek() to 'stretch' the file");
  }

  /* Something needs to be written at the end of the file to
   * have the file actually have the new size.
   * Just writing an empty string at the current file position will do.
   * Note:
   *  - The current position in the file is at the end of the stretched
   *  file due to the call to lseek().
   *  - An empty string is actually a single '\0' character, so a zero-byte
   *  will be written at the last byte of the file.
   */
  result = write(fd, "", 1);
  if (result != 1) {
    close(fd);
    Error("Error writing last byte of the file");
  }

  // Now the file is ready to be mmapped.
  char *map = (char*) mmap(0, filesize,
      PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  if (map == MAP_FAILED) {
    close(fd);
    Error("Error mmapping the file");
  }

  memcpy( map, data, filesize);

  // Don't forget to free the mmapped memory
  if (munmap(map, filesize) == -1) {
    Error("Error un-mmapping the file");
  }
  // Un-mmaping doesn't close the file, so we still need to do that.
  close(fd);
}
void setFgMapPtrs(PATTERN *x){
  int index = 0;
  int prevp = 0;
  int prevh = 0;
  int previ = 0;
  for(int p=1; p<=NPARTS; p++){
    for(int h=1; h<=NHYPS; h++){
      //while(index < x->numMappings && x->fgmap[index].pid == prevp && x->fgmap[index].hid == prevh){
      while(x->fgmap[index].pid == prevp && x->fgmap[index].hid == prevh){
        //printf("pid %d, hid %d\n", x->fgmap[index].pid, x->fgmap[index].hid);
        index++;
      }
      //printf("p %d, h %d, index %d\n", p, h, index);
      // Penalize parts that don't have any fg inside the image
      if(index == previ) x->singleton[p-1][h-1] += NO_FG_PENALTY;
      if(index >= x->numMappings) {
        printf("index %d, numMappings: %d\n", index, x->numMappings);
        Error("Problem in setFgMapPtrs.");
      }
      x->fgmapIndices[p-1][h-1] = index;
      prevp = p;
      prevh = h;
      previ = index;
    }
  }
}
void setExampleY(EXAMPLE *example){
  // arbitrarily choose some to be the hypotheses that features are
  // calculated from. Choose the first correct one by default.
  // TODO These choices can be better. Ex. Can use the best hypotheses for
  // singleton and pairwise weights of 1 and appearance and symmetry weights of 0.
  int s = (RAMANAN ? best_ramanan_scales[example->x->imgnum-1] : DEFAULT_SCALE);
  example->y.scale = s;
  for(int i=0; i<NPARTS; i++){
    example->y.y[i] = 0;
    for(int j=0; j<NHYPS; j++){
      if(example->x[s].y[i][j]){
        example->y.y[i] = j+1;
        break;
      }
    }
    if(0 == example->y.y[i]) example->y.y[i] = 1; // default to 1
  }
}
void read_struct_example_binary( EXAMPLE *example, char *dirname,
    int imgnum, int s){// s is scale
  //Error("Need to update the paths in read_struct_example_binary to accommodate Abhik.");
  // alloc space after reading in the example since the saved
  // addresses are meaningless.
  if(s==0){
    stringstream ss (stringstream::in | stringstream::out);
    ss << dirname << "/binary/ex_" << imgnum;
    readmmap( (char*)example, sizeof(EXAMPLE), ss.str());
    //
    example->x[s].image = (color*)malloc(3*example->x[s].imgsize*sizeof(color));
    if(!example->x[s].image){
      printf("\nCouldnt allocate space for image %d of size %d. 2.\n",
          imgnum, example->x[s].imgsize);
      exit(0);
    }
    stringstream ss2 (stringstream::in | stringstream::out);
    ss2 << dirname << "/binary/im_" << imgnum;
    readmmap( (char*)example->x[s].image,
        3*sizeof(color)*example->x[s].imgsize, ss2.str());
    example->x[s].texture = NULL;
    /*
    example->x[s].texture = (int(*)[NTEXTURES])
      malloc(example->x[s].imgsize*NTEXTURES*sizeof(int));
    stringstream ss4 (stringstream::in | stringstream::out);
    ss4 << dirname << "/binary/textb_" << imgnum;
    readmmap( (char*)example->x[s].texture,
        NTEXTURES*sizeof(int)*example->x[s].imgsize, ss4.str());
    */
  }else{
    example->x[s].image = example->x[0].image;
    example->x[s].texture = example->x[0].texture;
  }
  example->x[s].fgmap = (MAPENTRY*)malloc(example->x[s].numMappings*sizeof(MAPENTRY));
  if(!example->x[s].fgmap){
    printf("\nCouldnt allocate space for fgmap %d of size %d.\n",
        imgnum, example->x[s].numMappings);
    exit(0);
  }
  stringstream ss3 (stringstream::in | stringstream::out);
  ss3 << dirname;
  if(RAMANAN) ss3 << "/scale" << (s+1);
  ss3 << "/binary/fgmap_" << imgnum;
  readmmap( (char*)example->x[s].fgmap,
      sizeof(MAPENTRY)*example->x[s].numMappings, ss3.str());
  if(1){
    for(int i=0; i<example->x[s].numMappings; i++){
      assert(example->x[s].fgmap[i].pid > 0);
      assert(example->x[s].fgmap[i].hid > 0);
      assert(example->x[s].fgmap[i].pixel > 0);
      assert(example->x[s].fgmap[i].pixel <= example->x[s].imgsize );
    }
    if(0) printf("s %d, %d, %d\n", s, example->x[s].numMappings,
        example->x[s].imgsize);
  }
  setExampleY(example);
}
void saveAsBinary( EXAMPLE *example, char *dirname, int imgnum,
    int s){// s is scale
  // write out in binary format
  if(s==0){// the image is the same for all scales
    stringstream ss (stringstream::in | stringstream::out);
    ss << dirname << "/binary/ex_" << imgnum;
    writemmap( (char*)example, sizeof(EXAMPLE), ss.str());
    // image
    stringstream ss2 (stringstream::in | stringstream::out);
    ss2 << dirname << "/binary/im_" << imgnum;
    writemmap( (char*)example->x[s].image,
        3*sizeof(color)*example->x[s].imgsize, ss2.str());
    /*
    stringstream ss4 (stringstream::in | stringstream::out);
    ss4 << dirname << "/binary/textb_" << imgnum;
    writemmap( (char*)example->x[s].texture,
        NTEXTURES*sizeof(int)*example->x[s].imgsize, ss4.str());
    */
  }
  stringstream ss3 (stringstream::in | stringstream::out);
  ss3 << dirname;
  if(RAMANAN) ss3 << "/scale" << (s+1);
  ss3 << "/binary/fgmap_" << imgnum;
  writemmap( (char*)example->x[s].fgmap,
      sizeof(MAPENTRY)*example->x[s].numMappings, ss3.str());
}
void calculateSymmetries( EXAMPLE *example, STRUCT_LEARN_PARM *sparm){
  assert(!RAMANAN);
  assert(NSCALES==1); // dont use symmetry for RAMANAN yet
  // Calculate symmetry
  // Includes the multiple levels of symmetric appearance pyramid.
  // Head and body are unused but this makes indexing easier.
  // TODO This can be extended to part-wise appearance models later.
  // An array of pointers.
  Appmodel (*sym_models)[NHYPS][SUBPART_LEVELS][MAX_PARTS] =
    (Appmodel (*)[NHYPS][SUBPART_LEVELS][MAX_PARTS])
    malloc(sizeof(Appmodel)*NPARTS*NHYPS*SUBPART_LEVELS*MAX_PARTS);
  computeModels( &example->x[0], sym_models, &sparm->constraint_config);
  computeModelDivergences( &example->x[0], sym_models, sparm->syms, nlogn);
  free(sym_models);
  //Error("must calc syms in svm_struct_api.cpp");
}
void read_struct_example( EXAMPLE *example, char *dirname, int imgnum,
    STRUCT_LEARN_PARM *sparm, int s){ //s is scale
  int READDEBUG = 0;
  example->x[s].scale = s;
  example->x[s].imgnum = imgnum;
  stringstream ss (stringstream::in | stringstream::out);
  ss << dirname;
  if(RAMANAN) ss << "/scale" << (s+1);
  ss << "/singletons/singleton_" << imgnum << ".txt";
  // Read singleton
  if(READDEBUG) printf("READING SINGLETON\n");
  FILE *f = fopen(ss.str().c_str(),"r");
  if(!f){
    printf("\nCouldnt open file: %s\n", ss.str().c_str());
    exit(0);
  }
  for(int i=0; i<NPARTS; i++){
    for(int j=0; j<NHYPS; j++){
      double num;
      int ret = fscanf(f, "%lf", &num);
      if(1 != ret){
        printf("num ret: %d\n", ret);
        Error("Read Error 1.");
      }
      if (RAMANAN) num = -num;//TODO THIS IS A HACK
      example->x[s].singleton[i][j] = num;
      if(READDEBUG) printf("%f ",num);
    }
    if(READDEBUG) printf("\n");
  }
  fclose(f);
  if(READDEBUG) exit(0);
  // Read Pairwise
  if(READDEBUG) printf("READING PAIRWISE\n");
  ss.str(string());
  ss << dirname;
  if(RAMANAN) ss << "/scale" << (s+1);
  ss << "/pairwise/pairwise_" << imgnum << ".txt";
  f = fopen(ss.str().c_str(),"r");
  if(!f){
    printf("\nCouldnt open file: %s\n", ss.str().c_str());
    exit(0);
  }
  // to make the appearance symmetric.
  // contains symmetric joints and then the normal joints.
  // For e<NSYMS:
  //   symAndPair[e][i][j] contains the divergence between the model for
  //   edges[e] part i and edges[e+E] part j.
  for(int i=0; i<NJOINTS; i++){
    for(int j=0; j<NHYPS; j++){
      for(int k=0; k<NHYPS; k++){
        double num;
        if(1 != fscanf(f, "%lf", &num)) Error("Read Error 2.");
        if (RAMANAN) num = -num;//TODO THIS IS A HACK
        example->x[s].symAndPair[NSYMS+i][j][k] = num;
        if(READDEBUG) printf("%f ",num);
      }
      if(READDEBUG) printf("\n");
    }
    if(READDEBUG) printf("________________________________\n");
  }
  fclose(f);
  // Read image
  if(s==0){
    if(READDEBUG) printf("READING IMAGE\n");
    ss.str(string());
    ss << dirname;
    ss << "/images/image_" << imgnum << ".txt";
    printf("%s\n", ss.str().c_str());
    f = fopen( ss.str().c_str(),"r");
    if(!f){
      printf("\nCouldnt open file: %s\n", ss.str().c_str());
      exit(0);
    }
    if(1 != fscanf(f, "%d", &example->x[s].imgsize))
      Error("Read Error 3.");
    if(READDEBUG) printf("IMAGE size: %d\n", example->x[s].imgsize);
    example->x[s].image = (color*)malloc(3*example->x[s].imgsize*sizeof(color));
    if(!example->x[s].image){
      printf("\nCouldnt allocate space for image %d of size %d. 1.\n",
          imgnum, example->x[s].imgsize);
      exit(0);
    }
    for(int i=0; i<example->x[s].imgsize; i++){
      int r,g,b, ret;
      ret = fscanf(f, "%d %d %d", &r, &g, &b);
      if(3 != ret){
        printf("i: %d, ret: %d, size: %d\n", i, ret, example->x[s].imgsize);
        Error("Read Error 4.");
      }
      //printf("%d %d %d\n", r,g,b);
      example->x[s].image[3*i+0] = r;
      example->x[s].image[3*i+1] = g;
      example->x[s].image[3*i+2] = b;
    }
    fclose(f);
    // Read Texture
    if(READDEBUG) printf("READING TEXTURE\n");
    /*
    ss.str(string());
    ss << dirname;
    ss << "/textures/text_" << imgnum;
    f = fopen( ss.str().c_str(),"r");
    if(!f){
      printf("\nCouldnt open file: %s\n", ss.str().c_str());
      exit(0);
    }*/
    example->x[s].texture = (int(*)[NTEXTURES])
      malloc(example->x[s].imgsize*NTEXTURES*sizeof(int));
    if(!example->x[s].texture){
      printf("\nCouldnt allocate space for texture %d of size %d. 1.\n",
          imgnum, example->x[s].imgsize);
      exit(0);
    }
    for(int i=0; i<example->x[s].imgsize; i++){
      for(int j=0; j<NTEXTURES; j++){
        int t, ret;
        ret = fscanf(f, "%d", &t);
        if(1 != ret){
          printf("i: %d, j: %d, ret: %d, size: %d\n",
              i, j, ret, example->x[s].imgsize);
          Error("Read Error 4t.");
        }
        //printf("%d ", t);
        example->x[s].texture[i][j] = t;
      }
      //printf("\n");
    }
    //fclose(f);
  }else{
    example->x[s].image = example->x[0].image;
    example->x[s].imgsize = example->x[0].imgsize;
    example->x[s].texture = example->x[0].texture;
  }
  // Read foreground map
  if(READDEBUG) printf("READING FGMAP\n");
  ss.str(string());
  ss << dirname;
  if(RAMANAN) ss << "/scale" << (s+1);
  ss << "/fgmaps/fgmap_" << imgnum << ".txt";
  f = fopen( ss.str().c_str(),"r");
  if(!f){
    printf("\nCouldnt open file: %s\n", ss.str().c_str());
    exit(0);
  }
  if(1 != fscanf(f, "%d", &example->x[s].numMappings))
    Error("Read Error 5.");

  //printf("sizeof(MAPENTRY): %d, %d\n", sizeof(MAPENTRY), example->x[s].numMappings);
  if(READDEBUG) printf("fgmap size: %d\n", example->x[s].numMappings);
  example->x[s].fgmap = (MAPENTRY*)malloc(example->x[s].numMappings*sizeof(MAPENTRY));
  if(!example->x[s].fgmap){
    printf("\nCouldnt allocate space for fgmap %d of size %d.\n",
        imgnum, example->x[s].numMappings);
    exit(0);
  }
  for(int i=0; i<example->x[s].numMappings; i++){
    int pid,hid,pixel;
    if(RAMANAN){
      int ret = fscanf(f, "%d %d %d", &pid, &hid, &pixel);
      if(3 != ret) Error("Read Error 6.");
      example->x[s].fgmap[i].pid = pid;
      example->x[s].fgmap[i].hid = hid;
      example->x[s].fgmap[i].pixel = pixel;
      // add in the subparts
      //example->x[s].fgmap[i].subparts[0] = 0; // there's only 1 part
    }else{
      int subpart2, subpart3, subpart4;
      int ret = fscanf(f, "%d %d %d %d %d %d", &pid, &hid, &pixel,
          &subpart2, &subpart3, &subpart4);
      if(6 != ret) Error("Read Error 6.");
      example->x[s].fgmap[i].pid = pid;
      example->x[s].fgmap[i].hid = hid;
      example->x[s].fgmap[i].pixel = pixel;
      // add in the subparts
      example->x[s].fgmap[i].subparts[0] = 0; // there's only 1 part
      if(SUBPART_LEVELS >= 2) example->x[s].fgmap[i].subparts[1] = subpart2 - 1;
      if(SUBPART_LEVELS >= 3) example->x[s].fgmap[i].subparts[2] = subpart3 - 1;
      if(SUBPART_LEVELS >= 4) example->x[s].fgmap[i].subparts[3] = subpart4 - 1;
      if(SUBPART_LEVELS >= 5) Error("SUBPART_LEVELS too high.");
      for(int j=0; j<SUBPART_LEVELS; j++){
        if(example->x[s].fgmap[i].subparts[j] < 0 ||
            example->x[s].fgmap[i].subparts[j] >= pow(j+1,2))
          Error("subpart out of bounds.");
      }
    }
    if(READDEBUG) printf("%d %d %d\n", pid,hid,pixel);
  }
  fclose(f);
  // Read ground truth
  if(READDEBUG) printf("READING Ground Truth\n");
  ss.str(string());
  ss << dirname;
  if(RAMANAN) ss << "/scale" << (s+1);
  ss << "/groundtruth/groundtruth_" << imgnum << ".txt";
  f = fopen( ss.str().c_str(),"r");
  if(!f){
    printf("\nCouldnt open file: %s\n", ss.str().c_str());
    exit(0);
  }
  for(int i=0; i<NPARTS; i++){
    for(int j=0; j<NHYPS; j++){
      int num;
      if(1 != fscanf(f, "%d", &num)) Error("Read Error 7.");
      example->x[s].y[i][j] = num;
      if(READDEBUG) printf("%d ",num);
    }
    if(READDEBUG) printf("\n");
  }
  fclose(f);
  // set Y
  //if(DEFAULT_SCALE == s) setExampleY(example);
  if(s==(RAMANAN ? best_ramanan_scales[example->x->imgnum-1] : DEFAULT_SCALE)) {
    setExampleY(example);
  }
  setFgMapPtrs(&example->x[s]);
}
void readInt(ifstream &f, int *i){
  assert(f.good());
  f >> *i;
  assert(f.good());
  string tmp;
  f >> tmp;// this is just the description
  cout << *i << " " << tmp << endl;
  assert(f.good());
}
// Read a configuration file for minSumWithAppearance.
void readConfig(CONFIGS *c, const char *dirname, string &configname){
  string filename = dirname;
  filename += "/";
  filename += configname;
  cout << filename<<endl;
  ifstream f(filename.c_str(), ifstream::in);
  //FILE *f = fopen(filename.c_str(),"r");
  if(!f.good()){
    printf("\nCouldnt open file: %s\n", filename.c_str());
    exit(0);
  }
  readInt(f, &c->num_color_buckets);
  readInt(f, &c->em_iters);
  readInt(f, &c->model_blur);
  readInt(f, &c->model_blur_inv);
  readInt(f, &c->app_model_prior);
  readInt(f, &c->use_symmetry);
  readInt(f, &c->approx_inference);
  readInt(f, &c->inference_iters);
  readInt(f, &c->hillclimb);
  readInt(f, &c->plot_inference);
  readInt(f, &c->inference_method);
  readInt(f, &c->abcd_block_size);
  readInt(f, &c->abcd_iters);
  readInt(f, &c->abcd_inf_time);
  readInt(f, &c->writeLog);
  readInt(f, &c->include_block);
  readInt(f, &c->naive_init);
  readInt(f, &c->sample_random_features);
  readInt(f, &c->sample_learning_features);
  f.close();

  printf("%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n",
      c->num_color_buckets,
      c->em_iters,
      c->model_blur,
      c->model_blur_inv,
      c->app_model_prior,
      c->use_symmetry,
      c->approx_inference,
      c->inference_iters,
      c->hillclimb,
      c->plot_inference,
      c->inference_method,
      c->abcd_block_size,
      c->abcd_iters,
      c->abcd_inf_time,
      c->writeLog,
      c->sample_random_features,
      c->sample_learning_features
      );
  //This is already done by the saveData.m script.
  //fscanf(f, "%d", &c->fg_thresh);
  c->isBetter = NULL;
}
void initParams(STRUCT_LEARN_PARM *param, const char *dirname){
  param->loss_function = 0;
  param->nparts = NPARTS;
  param->nhyp = NHYPS;
  param->njoints = NJOINTS;
  param->nsyms = NSYMS;
  if(RAMANAN){
    assert(NJOINTS==17);
    assert(NPARTS==18);
    // joints is 1 indexed
    int p[NPARTS];
    for(int i=0; i<NPARTS; i++) p[i] = i+1;
    int par[] = {0, 1, 2, 3, 4, 5, 6, 3, 8, 9,  2, 11, 12, 13, 14, 11, 16, 17};
    //int par[] = {0, 1, 2, 3, 4, 5, 6, 3, 8, 9, 10, 11, 12, 13, 2,
    //  15, 16, 17, 18, 15, 20, 21, 22, 23, 24, 25};
    param->joints = (JOINT*) malloc(NJOINTS*sizeof(JOINT));
    for(int i=0; i<NJOINTS; i++){
      // child is first, then parent
      param->joints[i].j1 = p[i+1];
      param->joints[i].j2 = par[i+1];
      assert( p[i+1] > par[i+1]);
      assert(param->joints[i].j1 >= 1 && param->joints[i].j1<=NPARTS);
      assert(param->joints[i].j2 >= 1 && param->joints[i].j2<=NPARTS);
      //printf("joint: %d, %d\n", param->joints[i].j1, param->joints[i].j2);
    }
    param->syms = NULL; //TODO
  }else{
    param->joints = (JOINT*) malloc(NJOINTS*sizeof(JOINT));
    param->joints[0].j1 = 1;
    param->joints[0].j2 = 2;
    param->joints[1].j1 = 2;
    param->joints[1].j2 = 10;
    param->joints[2].j1 = 4;
    param->joints[2].j2 = 3;
    param->joints[3].j1 = 3;
    param->joints[3].j2 = 10;
    param->joints[4].j1 = 5;
    param->joints[4].j2 = 6;
    param->joints[5].j1 = 6;
    param->joints[5].j2 = 10;
    param->joints[6].j1 = 8;
    param->joints[6].j2 = 7;
    param->joints[7].j1 = 7;
    param->joints[7].j2 = 10;
    param->joints[8].j1 = 9;
    param->joints[8].j2 = 10;
    param->syms = (JOINT*) malloc(NSYMS*sizeof(JOINT));
    param->syms[0].j1 = 1;
    param->syms[0].j2 = 4;
    param->syms[1].j1 = 2;
    param->syms[1].j2 = 3;
    param->syms[2].j1 = 5;
    param->syms[2].j2 = 8;
    param->syms[3].j1 = 6;
    param->syms[3].j2 = 7;
  }
  readConfig(&param->constraint_config, dirname, param->configfile);
  param->constraint_config.isBetter = maxIsBetter;
  param->constraint_config.worst = -INFINITY;
}
string getFileName(const char *dirname, string prefix, STRUCT_LEARN_PARM *sparm){
  int c = (int)(10000*sparm->C);
  CONFIGS cc = sparm->constraint_config;
  stringstream ss2 (stringstream::in | stringstream::out);
  ss2 << dirname << "/" << prefix << "_c_" << c << "_constraint";
  ss2 << cc.sample_random_features << cc.sample_learning_features << ".txt";
  return ss2.str();
}
SAMPLE read_struct_examples(char *dirname, STRUCT_LEARN_PARM *sparm,
    char *modeldir){
  sparm->weights_dir = modeldir;
  sparm->example_dir = dirname;
  //printf("dirname: %s\n", dirname); //train10
  //printf("modeldir: %s\n", modeldir);//weights10
  printf("\n");//still on an old line for some reason
  /* Reads struct examples and returns them in sample. The number of
     examples must be written into sample.n */
  // Init params
  //initParams(sparm, dirname);
  initParams(sparm, ".");

  int start, end;
  string rangename = dirname;
  rangename += "/range";
  FILE *f = fopen(rangename.c_str(),"r");
  if(!f){
    printf("\nCouldnt open file: %s\n", rangename.c_str());
    exit(0);
  }
  int every;
  int offset;
  int retval = fscanf(f, "%d %d %d %d", &start, &end, &every, &offset);
  if(4 != retval && 3 != retval && 2 != retval) Error("Range read Error.");
  if (2 >= retval) every = 1;
  if (3 >= retval) offset = 0;
  SAMPLE   sample;  /* sample */
  //end   = 305;
  long n = end-start+1;
  printf("start: %d, end: %d, every: %d\n", start, end, every);
  int n_ = 0;
  for(int i=0; i<n; i++){
    if (i%every == offset) n_ ++;
  }


  EXAMPLE *examples = (EXAMPLE *)my_malloc(sizeof(EXAMPLE)*n_);

  /* fill in your code here */
  if(RAMANAN) initOverlapEdgesRamanan();
  if(!RAMANAN) initOverlapEdges();
  int ii = 0;
  for(int i=0; i<n; i++){
    if (i%every != offset) continue;
    printf("Reading example %d of %d. Number %d\n", ii, n_, i+start);
    for(int s=0; s<NSCALES; s++){
      //printf("reading %d, at scale %d\n", i, s);
      //if (s>0 && ONLY_ONE_SCALE && RAMANAN) s = best_ramanan_scales[n];
      if(READ_BINARY){
        read_struct_example_binary( examples+ii, dirname, start+i, s);
      }else
        read_struct_example( examples+ii, dirname, start+i, sparm, s);
      extraFeatures( examples+ii, s);
      //if (s>0 && ONLY_ONE_SCALE && RAMANAN) break;
    }
    ii++;
  }
  printf("Done Reading. Computing stuff...\n");

  sample.n=n_;
  sample.examples=examples;
  sparm->end = end;
  //string fbname = getFileName( modeldir, "featurebank", sparm);
  //loadFeatureBank(fbname);
  // init log stuff
  nlogs = numnlogn = 0;
  logs = allocLogs(&sample, sparm->constraint_config.model_blur_inv);
  nlogn = allocLogs(&sample, sparm->constraint_config.model_blur_inv);
  if(!nlogn) Error("Couldnt alloc nlogn.");
  int maximgsize = 0;
  for(int i=0; i<n_; i++)
    maximgsize = max(maximgsize, sample.examples[i].x[0].imgsize);
  int want = 2*sparm->constraint_config.app_model_prior*TOTAL_COLOR_BUCKETS
    + maximgsize;
  setLogsn(logs, nlogn, &nlogs, &numnlogn, want);
  if(!READ_BINARY){ // needs to be after nlogn is computed
    int ii = 0;
    for(int i=0; i<n; i++){
      if (i%every != offset) continue;
      if(!RAMANAN && sparm->constraint_config.use_symmetry)
        calculateSymmetries( examples+ii, sparm);
      for(int s=0; s<NSCALES; s++){
        saveAsBinary( examples+ii, dirname, start+i, s);
      }
      ii++;
    }
  }
  printf("Done reading struct examples.\n");
  return(sample);
}

void init_struct_model(SAMPLE sample, STRUCTMODEL *sm,
    STRUCT_LEARN_PARM *sparm, LEARN_PARM *lparm,
    KERNEL_PARM *kparm){
  /* Initialize structmodel sm. The weight vector w does not need to be
     initialized, but you need to provide the maximum size of the
     feature space in sizePsi. This is the maximum number of different
     weights that can be learned. Later, the weight vector w will
     contain the learned weights for the model. */

  sm->sizePsi = NFEATURES; /* replace by appropriate number of features */
}

CONSTSET init_struct_constraints(SAMPLE sample, STRUCTMODEL *sm,
    STRUCT_LEARN_PARM *sparm){
  /* Initializes the optimization problem. Typically, you do not need
     to change this function, since you want to start with an empty
     set of constraints. However, if for example you have constraints
     that certain weights need to be positive, you might put that in
     here. The constraints are represented as lhs[i]*w >= rhs[i]. lhs
     is an array of feature vectors, rhs is an array of doubles. m is
     the number of constraints. The function returns the initial
     set of constraints. */
  CONSTSET c;

  if(1) { /* normal case: start with empty set of constraints */
    c.lhs=NULL;
    c.rhs=NULL;
    c.m=0;
  } else { /* add constraints so that all learned weights are
            positive. WARNING: Currently, they are positive only up to
            precision epsilon set by -e. */
    long     sizePsi=sm->sizePsi;
    printf("SIZE psi: %ld\n", sizePsi);
    WORD     words[2];
    c.lhs= (DOC**)my_malloc(sizeof(DOC *)*sizePsi);
    c.rhs= (double*)my_malloc(sizeof(double)*sizePsi);
    for(int i=0; i<sizePsi; i++) {
      words[0].wnum=i+1;
      if(i<NUNIQUE_SINGLETON+NUNIQUE_PAIRWISE)
        words[0].weight= -1.0;
      else
        words[0].weight= -0.1;
      words[1].wnum=0;
      /* the following slackid is a hack. we will run into problems,
         if we have move than 1000000 slack sets (ie examples) */
      c.lhs[i] = create_example(i,0,1000000+i,1,create_svector(words,"",1.0));
      c.rhs[i] = 1.0;
    }
    c.m = sizePsi;
    printf("c.m: %d\n", c.m);
  }
  return(c);
}
// Avoid duplicate weights. Ex. The weights for the two fore-arms should be the same.
void setWeights(CONFIGS *config, STRUCTMODEL *sm){
  vector<double> means = getLastMeans();
  vector<double> sdevinv = getSdevinv();
  // w is indexed starting at 1
  const double DEFAULT_MEAN = 0.0;
  const double DEFAULT_STDDEVINV = 10.0;
  const int TEST = 0;
  const int TESTS = 0;
  int N = 1;
  //printf("means size: %u, sdevinv size: %u\n", means.size(), sdevinv.size());
  for(int i=0; i<NPARTS; i++){
    int index = singleton_indices[i];
    assert(N+index <= NFEATURES);
    config->singleton_weights[i] = sm->w[N+index];
    //printf("i %d, N %d, index %d\n", i, N, index);
    config->singleton_means[i] = 0.0;
      //means[N+index-1] / singleton_duplicates[i];
    if(TEST) config->singleton_means[i] =
      DEFAULT_MEAN / singleton_duplicates[i];
    config->singleton_sdevinv[i] = 1.0;//sdevinv[N+index-1];
    if(TESTS) config->singleton_sdevinv[i] = DEFAULT_STDDEVINV;
  }
  N += NUNIQUE_SINGLETON;
  for(int i=0; i<NJOINTS; i++){
    int index = pairwise_indices[i];
    assert(N+index <= NFEATURES);
    config->pairwise_weights[i] = sm->w[N+index];
    config->pairwise_means[i] = 0.0;
      //means[N+index-1] / pairwise_duplicates[i];
    if(TEST) config->pairwise_means[i] =
      DEFAULT_MEAN / pairwise_duplicates[i];
    config->pairwise_sdevinv[i] = 1.0;//sdevinv[N+index-1];
    if(TESTS) config->pairwise_sdevinv[i] = DEFAULT_STDDEVINV;
  }
  N += NUNIQUE_PAIRWISE;
  assert(N <= NFEATURES);
  for(int i=0; i<NAPP; i++){
    int index = appearance_indices[i];
    assert(N+index <= NFEATURES);
    config->appearance_weights[i] = sm->w[N+index];
    config->appearance_means[i] = 0.0;
      //means[N+index-1] / appearance_duplicates[i];
    if(TEST) config->appearance_means[i] =
      DEFAULT_MEAN / appearance_duplicates[i];
    config->appearance_sdevinv[i] = 1.0;//sdevinv[N+index-1];
    if(TESTS) config->appearance_sdevinv[i] = DEFAULT_STDDEVINV;
  }
  N += NUNIQUE_APPEARANCE;
  for(int i=0; i<NSYMS; i++){
    assert(N+i <= NFEATURES);
    config->symmetric_weights[i] = sm->w[N+i];
    config->symmetric_means[i] = 0.0;//means[N+i-1] / 1;
    if(TEST) config->symmetric_means[i] = DEFAULT_MEAN / 1;
    config->symmetric_sdevinv[i] = 1.0;//sdevinv[N+i-1];
    if(TESTS) config->symmetric_sdevinv[i] = DEFAULT_STDDEVINV;
  }
  N += NSYMS;
  for(int i=0; i<NOVERLAP; i++){
    int index = overlap_indices[i];
    assert(N+index <= NFEATURES);
    config->overlap_weights[i] = sm->w[N+index];
    config->overlap_means[i] = 0.0;
      //means[N+index-1] / overlap_duplicates[i];
    if(TEST) config->overlap_means[i] =
      DEFAULT_MEAN / overlap_duplicates[i];
    config->overlap_sdevinv[i] = 1.0;//sdevinv[N+index-1];
    if(TESTS) config->overlap_sdevinv[i] = DEFAULT_STDDEVINV;
    if(0) printf("i %d, index %d, N %d, N+index-1: %d, mean: %lf, dup: %d\n",
        i, index, N, N+index-1, means[N+index-1], overlap_duplicates[i]);
  }
  N += NUNIQUE_OVERLAP;
}
void setWeights(STRUCTMODEL *sm){
  for(int i=1; i<=sm->sizePsi; i++){
    if(i<=6) sm->w[i] = -1.0;
    else      sm->w[i] =  -0.00000001;
  }
  return;
  // w is indexed starting at 1
  for(int i=1; i<=sm->sizePsi; i++)
    sm->w[i] = -1.0;
}
// Penalize incorrect hypotheses.
// Very slightly penalize later hypotheses to break ties.
void createLossMatrix( PATTERN *x, LOSS *loss){
  for(int i=0; i<NPARTS; i++){
    for(int j=0; j<NHYPS; j++){
      loss->l[i][j] = LOSS_SCALE*(1 - x->y[i][j]) - j*FLT_EPSILON;
      if(DEBUG_LOSS) printf("%lf ", loss->l[i][j]);
    }
    if(DEBUG_LOSS) printf("\n");
  }
}
void clearLossMatrix( LOSS *loss){
  for(int i=0; i<NPARTS; i++)
    for(int j=0; j<NHYPS; j++)
      loss->l[i][j] = 0.0;
}
// FEATURES contains separate terms for features like the two fore-arms,
// two upper-arms, etc. These similar terms are combined when copied over to
// the SVECTOR.
SVECTOR* convertFeaturesToSVEC(FEATURES *f){
  //printf("NFEATURES %d\n", NFEATURES);
  SVECTOR *vec = (SVECTOR*)malloc(sizeof(SVECTOR));
  vec->words = (WORD*) malloc(sizeof(WORD)*(NFEATURES+1));
  for(int i=0; i<NFEATURES; i++) vec->words[i].wnum = i+1;// needs to be non 0
  vec->words[NFEATURES].wnum = 0; // so that it knows when to stop looking through words
  int N = 0;
  for(int i=N; i<N+NFEATURES; i++) vec->words[N+i].weight = 0.0;
  for(int i=0; i<NPARTS; i++){
    int index = singleton_indices[i];
    assert(N+index < NFEATURES);
    vec->words[N+index].weight += f->singleton[i];
    if(vec->words[N+index].wnum != N+index+1) Error("Error in convertFeaturesToSVEC.");
  }
  N += NUNIQUE_SINGLETON;
  for(int i=0; i<NJOINTS; i++){
    int index = pairwise_indices[i];
    assert(N+index < NFEATURES);
    vec->words[N+index].weight += f->pairwise[i];
    if(vec->words[N+index].wnum != N+index+1) Error("Error in convertFeaturesToSVEC.");
  }
  N += NUNIQUE_PAIRWISE;
  for(int i=0; i<NAPP; i++){
    int index = appearance_indices[i];
    assert(N+index < NFEATURES);
    vec->words[N+index].weight += f->appearance[i];
    if(vec->words[N+index].wnum != N+index+1) Error("Error in convertFeaturesToSVEC.");
  }
  N += NUNIQUE_APPEARANCE;
  for(int i=0; i<NSYMS; i++){
    assert(N+i < NFEATURES);
    vec->words[N+i].weight = f->sym[i];
    if(vec->words[N+i].wnum != N+i+1) Error("Error in convertFeaturesToSVEC.");
  }
  N += NSYMS;
  for(int i=0; i<NOVERLAP; i++){
    int index = overlap_indices[i];
    assert(N+index < NFEATURES);
    vec->words[N+index].weight += f->overlap[i];
    if(vec->words[N+index].wnum != N+index+1) Error("Error in convertFeaturesToSVEC.");
  }
  N += NUNIQUE_OVERLAP;

  vec->twonorm_sq = -1;
  vec->userdefined = NULL;
  vec->kernel_id = -1;
  vec->next = NULL;
  vec->factor = 1.0;
  return vec;
}
void freeSvec(SVECTOR *svec){
  free(svec->words);
  free(svec);
}
// compare the returned energy to the energy gotten from wTv
void compareEnergies(double energy, SVECTOR *vec, STRUCTMODEL *sm){
  double e = 0.0;
  for(int i=0; i<NFEATURES; i++){
    //printf("e: %lf, w: %lf, f: %lf\n", e, sm->w[i+1], vec->words[i].weight);
    e += vec->words[i].weight*sm->w[i+1];
  }
  // e and energy are computed using doubles so they should be within float precision.
  if(fabs(e-energy) >= FLT_EPSILON){
    printf("e: %lf, energy: %lf\n", e, energy);
    Error("Problem in compareEnergies.");
  }
}

LABEL classify_struct_example(PATTERN *x, STRUCTMODEL *sm,
            STRUCT_LEARN_PARM *sparm)
{
  /* Finds the label yhat for pattern x that scores the highest
     according to the linear evaluation function in sm, especially the
     weights sm.w. The returned label is taken as the prediction of sm
     for the pattern x. The weights correspond to the features defined
     by psi() and range from index 1 to index sm->sizePsi. If the
     function cannot find a label, it shall return an empty label as
     recognized by the function empty_label(y). */
  if(DEBUG) printf("CLASSIFYING STRUCT EXAMPLE %d\n", x->imgnum);
  // Create an empty loss matrix
  LOSS *lossmat = (LOSS*) malloc(sizeof(LOSS));
  //Error("Need a different lossmat for each scale");
  // Set the proper weights
  //setWeights(sm); //TODO remove
  setWeights( &sparm->constraint_config, sm);
  if(DEBUG) printWeights(sm);
  // Find the best hypothesis
  FEATURES bestfeatures;
  LABEL besty;
  double bestenergy = -numeric_limits<double>::infinity();
  for(int s=0; s<NSCALES; s++){
    if(RAMANAN && ONLY_ONE_SCALE)
      s=  best_ramanan_scales[x->imgnum-1];
    clearLossMatrix(lossmat);
    double LOSS_FACTOR= 0;//1e6;
    for(int i=0; i<NPARTS; i++){
      for(int j=0; j<NHYPS; j++){
        if(x[s].y[i][j] == 0) lossmat->l[i][j] -= LOSS_FACTOR;
      }
    }
    LABEL y;
    y.scale = s;
    FEATURES features;
    if(0) printf("Image %d, scale: %d, %d\n",
        x[s].imgnum, s, x[s].scale);
    assert( x[s].scale == s);
    //////////// TODO REMOVE!!!!!!!!!!! /////////////////
    /*
    for(int i=0; i<NPARTS; i++) y.y[i] = 1;
    double energy = calculateEnergy( x+s, y.y, sparm,
        &sparm->constraint_config, &features, nlogn);
        */
    double energy = minSumWithAppearance( x+s, lossmat, sparm,
        &sparm->constraint_config, y.y, &features, nlogn);
    double lossforscale = loss( x, &y, sparm);
    printf("Image %d, scale: %d, energy: %lf, loss: %lf\n",
        x[s].imgnum, s+1, energy, lossforscale);
    printHypotheses(y.y);
    energy -= LOSS_FACTOR*lossforscale;
    if(energy > bestenergy){
      bestenergy = energy;
      besty = y;
      bestfeatures = features;
    }
    if(RAMANAN && ONLY_ONE_SCALE) break;
  }
  printf("Image %d, scale: %d, best: %lf\n", x->imgnum, besty.scale+1, bestenergy);
  if(1){
    SVECTOR *vec = convertFeaturesToSVEC( &bestfeatures);
    printSvec2(vec);
    free(vec->words);
    free(vec);
  }
  free(lossmat);
  if(DEBUG) printHypotheses(besty.y);
  return(besty);
}

LABEL       find_most_violated_constraint_slackrescaling(PATTERN *x, LABEL *y,
                 STRUCTMODEL *sm,
                 STRUCT_LEARN_PARM *sparm)
{
  /* Finds the label ybar for pattern x that that is responsible for
     the most violated constraint for the slack rescaling
     formulation. For linear slack variables, this is that label ybar
     that maximizes

            argmax_{ybar} loss(y,ybar)*(1-psi(x,y)+psi(x,ybar))

     Note that ybar may be equal to y (i.e. the max is 0), which is
     different from the algorithms described in
     [Tschantaridis/05]. Note that this argmax has to take into
     account the scoring function in sm, especially the weights sm.w,
     as well as the loss function, and whether linear or quadratic
     slacks are used. The weights in sm.w correspond to the features
     defined by psi() and range from index 1 to index
     sm->sizePsi. Most simple is the case of the zero/one loss
     function. For the zero/one loss, this function should return the
     highest scoring label ybar (which may be equal to the correct
     label y), or the second highest scoring label ybar, if
     Psi(x,ybar)>Psi(x,y)-1. If the function cannot find a label, it
     shall return an empty label as recognized by the function
     empty_label(y). */

  /* insert your code for computing the label ybar here */
  printf("Constraint slackrescaling not yet implemented");
  exit(0);
  LABEL ybar;
  return(ybar);
}
LABEL       find_most_violated_constraint_marginrescaling(PATTERN *x, LABEL *y,
                 STRUCTMODEL *sm,
                 STRUCT_LEARN_PARM *sparm)
{
  /* Finds the label ybar for pattern x that that is responsible for
     the most violated constraint for the margin rescaling
     formulation. For linear slack variables, this is that label ybar
     that maximizes

            argmax_{ybar} loss(y,ybar)+psi(x,ybar)

     Note that ybar may be equal to y (i.e. the max is 0), which is
     different from the algorithms described in
     [Tschantaridis/05]. Note that this argmax has to take into
     account the scoring function in sm, especially the weights sm.w,
     as well as the loss function, and whether linear or quadratic
     slacks are used. The weights in sm.w correspond to the features
     defined by psi() and range from index 1 to index
     sm->sizePsi. Most simple is the case of the zero/one loss
     function. For the zero/one loss, this function should return the
     highest scoring label ybar (which may be equal to the correct
     label y), or the second highest scoring label ybar, if
     Psi(x,ybar)>Psi(x,y)-1. If the function cannot find a label, it
     shall return an empty label as recognized by the function
     empty_label(y). */
  if(DEBUG) printf("%d MARGIN RESCALING XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n", x->imgnum);
  /*
  for(int i=0; i<NPARTS; i++)
    ybar.y[i] = 1;
  if(x->imgnum < 109) return ybar;
  */
  //setWeights(sm); //TODO remove
  // Create the loss matrix
  LOSS *lossmat = (LOSS*) malloc(sizeof(LOSS));
  if(DEBUG) printY( y);
  setWeights( &sparm->constraint_config, sm);
  // Save the hypotheses and features to check against those found in psi.
  // Find the best hypothesis
  LABEL besty;
  FEATURES bestfeatures;
  double bestenergy = -numeric_limits<double>::infinity();
  for(int s=0; s<NSCALES; s++){
    if(RAMANAN && ONLY_ONE_SCALE)
      s=  best_ramanan_scales[x->imgnum-1];
    createLossMatrix( x+s, lossmat);
    FEATURES features;
    LABEL ybar;
    ybar.scale = s;
    double energy = minSumWithAppearance( x+s, lossmat, sparm,
        &sparm->constraint_config, ybar.y, &features, nlogn);
    energy += loss( x, &ybar, sparm);
    if(energy > bestenergy){
      bestenergy = energy;
      besty = ybar;
      bestfeatures = features;
    }
    if(RAMANAN && ONLY_ONE_SCALE) break;
  }
  free(lossmat);
  if(DEBUG) printHypotheses(besty.y);
  if(DEBUG) printFeatures(&bestfeatures);
  //if(x->imgnum == 305) exit(0);
  return(besty);
}

void validateY( LABEL *y){
  for(int i=0; i<NPARTS; i++){
    if(y->y[i] <= 0 || y->y[i] > NHYPS){
      printf("Erroneous y: %d, i: %d.\n", y->y[i], i);
      assert(0);
    }
  }
}
int         empty_label(LABEL *y)
{
  /* Returns true, if y is an empty label. An empty label might be
     returned by find_most_violated_constraint_???(x, y, sm) if there
     is no incorrect label that can be found for x, or if it is unable
     to label x at all */
  // There should never be an empty y in this implementation.
  validateY(y);
  return(0);
}
void randomlySampleFeatures(PATTERN *x, STRUCTMODEL *sm,
    STRUCT_LEARN_PARM *sparm, int scale){
  srand(x->imgnum);
  for(int sample=0; sample<99; sample++){
    int hids[NPARTS];
    for(int p=0; p<NPARTS; p++){
      hids[p] = rand()%NHYPS + 1;
      assert(hids[p]>=1 && hids[p]<=NHYPS);
    }
    FEATURES features;
    double energy = calculateEnergy( x, hids, sparm,
        &sparm->constraint_config, &features, nlogn, NULL,
        NULL, numnlogn);
    SVECTOR *vec = convertFeaturesToSVEC( &features);

    LABEL y;
    y.scale = scale;
    for(int i=0; i<NPARTS; i++) y.y[i] = hids[i];
    double l = loss(x, &y, NULL);
    storeFeature(vec, l);
    freeSvec(vec);
  }
}
SVECTOR* psi(PATTERN *x, LABEL *y, STRUCTMODEL *sm,
     STRUCT_LEARN_PARM *sparm)
{
  /* Returns a feature vector describing the match between pattern x
     and label y. The feature vector is returned as a list of
     SVECTOR's. Each SVECTOR is in a sparse representation of pairs
     <featurenumber:featurevalue>, where the last pair has
     featurenumber 0 as a terminator. Featurenumbers start with 1 and
     end with sizePsi. Featuresnumbers that are not specified default
     to value 0. As mentioned before, psi() actually returns a list of
     SVECTOR's. Each SVECTOR has a field 'factor' and 'next'. 'next'
     specifies the next element in the list, terminated by a NULL
     pointer. The list can be thought of as a linear combination of
     vectors, where each vector is weighted by its 'factor'. This
     linear combination of feature vectors is multiplied with the
     learned (kernelized) weight vector to score label y for pattern
     x. Without kernels, there will be one weight in sm.w for each
     feature. Note that psi has to match
     find_most_violated_constraint_???(x, y, sm) and vice versa. In
     particular, find_most_violated_constraint_???(x, y, sm) finds
     that ybar!=y that maximizes psi(x,ybar,sm)*sm.w (where * is the
     inner vector product) and the appropriate function of the
     loss + margin/slack rescaling method. See that paper for details. */
  if(DEBUG) printf("%d PSI XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n", x->imgnum);
  validateY(y);
  if(DEBUG) printXY(x);
  if(DEBUG) printWeights(sm);
  // Set the proper weights
  setWeights( &sparm->constraint_config, sm);
  // Find the best hypothesis
  FEATURES features;
  double energy = calculateEnergy( x+y->scale, y->y, sparm, &sparm->constraint_config,
      &features, nlogn, NULL);
  //double curloss = loss( x, y, sparm);
  if(DEBUG) printFeatures(&features);
  if(DEBUG) printHypotheses(y->y);
  if(DEBUG) printY(y);
  if(DEBUG) printFeatures(&features);
  SVECTOR *vec = convertFeaturesToSVEC( &features);
  double l = loss(x+y->scale, y, NULL);
  storeFeature(vec, l);
  // compare the returned energy to the energy gotten from wTv
  compareEnergies( energy, vec, sm);
  //printSvec(vec);
  // Randomly sample some features.
  if(sparm->constraint_config.sample_random_features)
    randomlySampleFeatures( x+y->scale, sm, sparm, y->scale);

  //if(x->imgnum == sparm->end) applyFeatureBank();
  return(vec);
}

bool first=true;
int pcp[NPARTS];
int nexamples = 0;

// This is kind of hacky since y and ybar are of different types.
double loss(PATTERN *x, LABEL *ybar, STRUCT_LEARN_PARM *sparm){
  if(first){
    first=false;
    for(int p=0; p<NPARTS; p++) pcp[p]=0;
  }
  nexamples++;

  assert(ybar->scale>=0 && ybar->scale<NSCALES);
  int (*y)[NHYPS] = x[ybar->scale].y;
  /* loss for correct label y and predicted label ybar. The loss for
     y==ybar has to be zero. sparm->loss_function is set with the -l option. */
  double loss = 0.0;
  double optloss = NPARTS;
  double worstloss = 0.0;
  for(int i=0; i<NPARTS; i++){
    double optl = 0.0;
    double worstl = 0.0;
    for(int j=0; j<NHYPS; j++){
      if(y[i][j]) optl = -1.0;
      if(!y[i][j]) worstl = 1.0;
    }
    optloss += optl;
    worstloss += worstl;
    int hid = ybar->y[i]-1;
    loss += (1.0 - y[i][hid]);
    pcp[i] += y[i][hid];
    if(0) printf("scale %d, p %d, h %d, loss %lf\n",
        ybar->scale, i, hid, (1.0 - y[i][hid]));
  }
  if(DEBUG) printf("LOSS %lf, optloss %lf, worstloss: %lf, scale: %d\n",
      loss, optloss, worstloss, ybar->scale);
  if(sparm) sparm->optloss = optloss;
  // Scale to be within the 0-1 loss range
  return LOSS_SCALE*loss;
}

int iter = 0;
int         finalize_iteration(double ceps, int cached_constraint,
             SAMPLE sample, STRUCTMODEL *sm,
             CONSTSET cset, double *alpha,
             STRUCT_LEARN_PARM *sparm)
{
  string wname = getFileName( sparm->weights_dir.c_str(), "w", sparm);
  stringstream ss (stringstream::in | stringstream::out);
  ss << wname << "iter_" << iter;
  string newwname = ss.str();
  printf("moving %s to %s\n", wname.c_str(),
      newwname.c_str());
  printf("%s\n", sparm->weights_dir.c_str());
  stringstream ss2 (stringstream::in | stringstream::out);
  if(iter>0){
    int cpid = fork();//pid of the child
    if(cpid==0){
      // this is the child
      execlp("mv", "mv", wname.c_str(), newwname.c_str(), NULL);
      exit(0);
    }else{
      waitpid(cpid, NULL, 0);
    }
  }
  //return 0;
  write_struct_model( sparm->weights_dir.c_str(), sm, sparm, false);
  int cpid = fork();
  if(cpid==0){
    ss2 << sparm->weights_dir << "/iter_" << iter;
    string resultfile = ss2.str();
    stringstream ss3 (stringstream::in | stringstream::out);
    ss3 << sparm->C;
    printf("%s\n", resultfile.c_str());
    printf("%s\n", sparm->configfile.c_str());
    //exit(0);
    execlp("./svm_empty_classify", "./svm_empty_classify",
        "--c", ss3.str().c_str(),
        "--x", sparm->configfile.c_str(),
        "--l", sparm->logname.c_str(),
        sparm->example_dir.c_str(),
        sparm->weights_dir.c_str(), resultfile.c_str(), NULL);
    exit(0);
  }else{
    waitpid(cpid, NULL, 0);
  }

  /* This function is called just before the end of each cutting plane
   * iteration. ceps is the amount by which the most violated constraint found
   * in the current iteration was violated. cached_constraint is true if the
   * added constraint was constructed from the cache. If the return value is
   * FALSE, then the algorithm is allowed to terminate. If it is TRUE, the
   * algorithm will keep iterating even if the desired precision
   * sparm->epsilon is already reached. */
  iter++;
  return(0);
}

void print_struct_learning_stats(SAMPLE sample, STRUCTMODEL *sm,
    CONSTSET cset, double *alpha,
    STRUCT_LEARN_PARM *sparm)
{
  /* This function is called after training and allows final touches to
     the model sm. But primarly it allows computing and printing any
     kind of statistic (e.g. training error) you might want. */
}

void print_struct_testing_stats(SAMPLE sample, STRUCTMODEL *sm,
    STRUCT_LEARN_PARM *sparm,
    STRUCT_TEST_STATS *teststats){
  /* This function is called after making all test predictions in
     svm_struct_classify and allows computing and printing any kind of
     evaluation (e.g. precision/recall) you might want. You can use
     the function eval_prediction to accumulate the necessary
     statistics for each prediction. */

  int c = (int) (10000*sparm->C);
  CONFIGS cc = sparm->constraint_config;
  stringstream ss (stringstream::in | stringstream::out);
  ss << sparm->weights_dir << "/results.txt";
  string filename = ss.str();
  cout << filename << endl;
  FILE *f = fopen(filename.c_str(),"a");//append
  if(!f){
    printf("\nCouldnt open file: %s\n", filename.c_str());
    exit(0);
  }
  fprintf(f, "c %d constraint %d%d: %f\n", c,
      cc.sample_random_features, cc.sample_learning_features,
      teststats->loss/teststats->n);
  fclose(f);
}

void eval_prediction(long exnum, EXAMPLE *ex, LABEL ypred,
          STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm,
          STRUCT_TEST_STATS *teststats)
{
  /* This function allows you to accumlate statistic for how well the
     predicition matches the labeled example. It is called from
     svm_struct_classify. See also the function
     print_struct_testing_stats. */
  if(exnum == 0) { /* this is the first time the function is
          called. So initialize the teststats */
    teststats->n = 0;
    teststats->loss = 0.0;
    teststats->optloss = 0.0;
  }
  teststats->n ++;
  double curloss = loss(ex->x, &ypred, sparm);
  teststats->loss += curloss;
  teststats->optloss += sparm->optloss;
  //printY( &ypred);
  //printf("img %d, loss %lf\n", ex->x.imgnum, curloss);
}

void write_struct_model(const char *modeldir, STRUCTMODEL *sm,
    STRUCT_LEARN_PARM *sparm, bool doFeatureBank){
  /* Writes structural model sm to file file. */
  string outfile = getFileName( modeldir, "w", sparm);
  FILE *f = fopen(outfile.c_str(),"w");
  if(!f){
    printf("\nCouldnt open file: %s\n", outfile.c_str());
    exit(0);
  }
  // w is indexed starting at 1
  if(doFeatureBank) printf("\n");
  for(int i=1; i<=sm->sizePsi; i++){
    if(doFeatureBank) printf("Weight %d: %.10lf\n", i, sm->w[i]);
    fprintf(f, "%.10lf\n", sm->w[i]);
  }
  fclose(f);
  if(doFeatureBank) applyFeatureBank();
  string fbname = getFileName( modeldir, "featurebank", sparm);
  if(0) saveFeatureBank(fbname);
}
STRUCTMODEL read_struct_model(char *modeldir, STRUCT_LEARN_PARM *sparm)
{
  /* Reads structural model sm from file file. This function is used
     only in the prediction module, not in the learning module. */
  // Init params
  initParams(sparm, ".");
  sparm->weights_dir = modeldir;
  string infile = getFileName( modeldir, "w", sparm);
  FILE *f = fopen(infile.c_str(),"r");
  if(!f){
    printf("\nCouldnt open file: %s\n", infile.c_str());
    exit(0);
  }
  STRUCTMODEL sm;
  sm.sizePsi = NFEATURES;
  //TODO
  sm.svm_model = (MODEL*) malloc(sizeof(MODEL));
  sm.svm_model->lin_weights = (double*)malloc(sizeof(double)*(NFEATURES+1));
  sm.w = sm.svm_model->lin_weights;
  sm.svm_model->sv_num = 0;
  //long    at_upper_bound;
  //double  b;
  sm.svm_model->supvec = NULL;
  sm.svm_model->alpha = NULL;
  sm.svm_model->index = NULL;       /* index from docnum to position in model */
  sm.svm_model->totwords = 0;     /* number of features */
  sm.svm_model->totdoc = 0;       /* number of training documents */
  //KERNEL_PARM kernel_parm; /* kernel */
  //sm.svm_model->kernel_parm.kernel_type = LINEAR;
  sm.svm_model->kernel_parm.kernel_type = -1;
  //double  loo_error,loo_recall,loo_precision;
  //double  xa_error,xa_recall,xa_precision;
  //double  maxdiff;

  // w is indexed starting at 1
  for(int i=1; i<=sm.sizePsi; i++){
    if(1!= fscanf(f, "%lf\n", sm.w + i)) Error("Read Error.");
    printf("Weight %d: %f\n", i, sm.w[i]);
  }
  if(DEBUG) printWeights(&sm);
  fclose(f);
  //string fbname = getFileName( modeldir, "featurebank", sparm);
  //loadFeatureBank(fbname);
  return sm;
}

void write_label(FILE *fp, LABEL y){
  /* Writes label y to file handle fp. */
  fprintf(fp, "%d ", y.scale);
  for(int i=0; i<NPARTS; i++)
    fprintf(fp, "%d ", y.y[i]);
  fprintf(fp, "\n");
}

void free_pattern(PATTERN *x, int scale) {
  x = x + scale;
  /* Frees the memory of x. */
  free(x->fgmap);
  if(scale==0){
    free(x->image);
    free(x->texture);
  }
  //TODO HACK
  if(nexamples==0) return;
  printf("PART BY PART PCP:\n");
  for(int p=0; p<NPARTS; p++)
    printf("%lf ", ((float)pcp[p])/nexamples);
  printf("\n");
  first=true;
  nexamples=0;
}

void free_label(LABEL y) {
  /* Frees the memory of y. */
  // Do nothing
}

void free_struct_model(STRUCTMODEL sm){
  /* Frees the memory of model. */
  /* if(sm.w) free(sm.w); */ /* this is free'd in free_model */
  if(sm.svm_model) free_model(sm.svm_model,1);
  /* add free calls for user defined data here */
  // w doesnt need to be freed because it is just a ptr into sm.svm_model
}

void free_struct_sample(SAMPLE sample){
  /* Frees the memory of sample sample. */
  for(int i=0;i<sample.n;i++) {
    for(int s=0; s<NSCALES; s++){
      free_pattern(sample.examples[i].x, s);
      freeFeatures(sample.examples+i, s);
    }
  }
  freeOverlapEdges();
  free(sample.examples);
  free(logs);
  free(nlogn);
}
void free_params(STRUCT_LEARN_PARM *param){
  free(param->joints);
  free(param->syms);
  //TODO configfile is leaking for some reason
}

void print_struct_help(){
  /* Prints a help text that is appended to the common help text of
     svm_struct_learn. */
  printf("         --* string  -> custom parameters that can be adapted for struct\n");
  printf("                        learning. The * can be replaced by any character\n");
  printf("                        and there can be multiple options starting with --.\n");
}

void parse_struct_parameters(STRUCT_LEARN_PARM *sparm){
  sparm->configfile = "constraint_config";
  sparm->logname = "logs";
  // TODO this is a problem since it overrides the -c arg.
  // sparm->C = 0.01; // also the default in training
  /* Parses the command line parameters that start with -- */
  for(int i=0;(i<sparm->custom_argc) && ((sparm->custom_argv[i])[0] == '-');i++) {
    switch ((sparm->custom_argv[i])[2])
      {
      case 'a': i++; /* strcpy(learn_parm->alphafile,argv[i]); */ break;
      case 'e': i++; /* sparm->epsilon=atof(sparm->custom_argv[i]); */ break;
      case 'k': i++; /* sparm->newconstretrain=atol(sparm->custom_argv[i]); */ break;
      case 'c': i++; sparm->C = atoi(sparm->custom_argv[i]); break;
      case 'x': i++; sparm->configfile = sparm->custom_argv[i]; break;
      case 'l': i++; sparm->logname = sparm->custom_argv[i]; break;
      default: printf("\nUnrecognized option %s!\n\n",sparm->custom_argv[i]);
         exit(0);
      }
  }
  printf("C: %lf\n", sparm->C);
}

void        print_struct_help_classify(){
  /* Prints a help text that is appended to the common help text of
     svm_struct_classify. */
  printf("         --* string -> custom parameters that can be adapted for struct\n");
  printf("                       learning. The * can be replaced by any character\n");
  printf("                       and there can be multiple options starting with --.\n");
  printf("         --c double -> Regularization parameter (for classification only)\n");
  printf("                       so we know which weights file to load.\n");
  printf("         --l string -> log file\n");
  printf("         --x string -> config file\n");
}

void parse_struct_parameters_classify(STRUCT_LEARN_PARM *sparm){
  /* Parses the command line parameters that start with -- for the
     classification module */
  sparm->configfile = "constraint_config";
  sparm->logname = "logs";
  for(int i=0;(i<sparm->custom_argc) && ((sparm->custom_argv[i])[0] == '-');i++) {
    switch ((sparm->custom_argv[i])[2]){
      // Just to know which model file to load
      case 'c': i++; sscanf(sparm->custom_argv[i], "%lf", &sparm->C); break;
      case 'x': i++; sparm->configfile = sparm->custom_argv[i]; break;
      case 'l': i++; sparm->logname = sparm->custom_argv[i]; break;
      default:
        printf("\nUnrecognized option %s!\n\n",sparm->custom_argv[i]);
        exit(0);
    }
  }
  printf("C: %lf\n", sparm->C);
}
