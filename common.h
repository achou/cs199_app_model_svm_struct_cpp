#ifndef COMMON_H
#define COMMON_H
#include "minSumWithAppearance.h"
#include "svm_struct_api_types.h"
#include "svm_struct/svm_struct_common.h"
#include <algorithm>
using namespace std;

void assertEqual(double a, double b);
void Error(const char *s);
int64_t GetTimeInMicrosecs(void);
double* allocLogs(SAMPLE *s, int model_blur_inv);
double* allocLogs(PATTERN *x, int model_blur_inv);
void setLogsn(double *logs, double *nlogn, int *cur,
    int *curnlogn, int want);
int getNextIndex(PATTERN *x, int pid, int hid);
double inline divergence(double p, double q){
  /*
  p = normalize(p);
  q = normalize(q);
  */
  double lpq = log2(p/q);
  double lnpq = log2((1-p)/(1-q));
  //double div = p*lpq + (1-p)*lnpq - q*lpq + (q-1)*lnpq;
  //double div = p*lpq - p*lnpq - q*lpq + q*lnpq;
  double div = (p-q)*(lpq-lnpq);
  //double div2 = p*log2(p/q) + (1-p)*log2((1-p)/(1-q))
  //  + q*log2(q/p) + (1-q)*log2((1-q)/(1-p));
  //if(fabs(div - div2) > FLT_EPSILON) Error("A");
  if(div != div){ // checks if it's nan
    printf("%f, %f\n", p, q);
    Error("div is NaN in function: divergence.");
  }
  return div;
}
double inline divergence2(double p, double q){
  double pq = p*q;
  // could use log2f
  double l = log2((p-pq) / (q-pq));
  double div = (p-q)*l;
  //These wont match since one is ln and one is log2
  //double div2 = divergence(p,q);
  //if(fabs(div - div2) > FLT_EPSILON) Error("A");

  return div;
}

//TODO move these into a symmetry file
void printSymAndPair(double symAndPair[E][NHYPS][NHYPS], bool symonly);
int getModelTotal(Appmodel *m);
void setModel(Appmodel *m, int initval);
void discretize(struct buckets *b, unsigned char *img, int pixel, int calledFrom);
void computeModels(PATTERN *x, //Appmodel appmodels[NPARTS][NHYPS],
    Appmodel sym_models[NPARTS][NHYPS][SUBPART_LEVELS][MAX_PARTS],
    CONFIGS *config);
double computeModelDivergence(Appmodel *m1, Appmodel *m2,
    int total1, int total2);
void computeModelDivergences( PATTERN *x,
    Appmodel sym_models[NPARTS][NHYPS][SUBPART_LEVELS][MAX_PARTS],
    JOINT *symm_joints, double *nlogn);
#endif
