from scipy import stats
def getAllVals(name, nlines):
  vals = []
  f=open(name, 'rt')
  n=0
  for line in f:
    line = line.split()
    line = [float(x) for x in line]
    n+=1
    vals.append(line)
    if n==nlines: break
  f.close()
  return vals
def betterAll(optapp, curapp):
  print 'XXXXXXXXXX'
  for j in range(0,41):
    if j==0: print 'Singleton:'
    elif j==6: print 'Pairwise:'
    elif j==11: print 'Appearance:'
    elif j==12: print 'Symmetry:'
    elif j==16: print 'Overlap:'
    optbetter = 0
    optsame = 0
    optworse = 0
    for i in range(0,len(optapp)):
      #print optapp[i][j], curapp[i][j]
      if optapp[i][j] < curapp[i][j]: optbetter+=1
      elif optapp[i][j] == curapp[i][j]: optsame+=1
      else: optworse+=1
    print ' ', optbetter, optsame, optworse, \
        '\t', stats.binom_test(optbetter, optbetter+optworse)

dir='outfeatures/'
print 'Tests are all with:'
print 'NUM_COLOR_BUCKETS = 8'
print 'SUBPART_LEVELS = 3'
for h in [10,20,50,100,500]:
  print 'Test Set (', h, 'Hypotheses ):'
  try:
    optapp = getAllVals(dir+'testbest'+str(h), 205)
    curapp = getAllVals(dir+'testnaive'+str(h), 205)
    betterAll(optapp, curapp)
  except IOError:
    pass
  try:
    print 'Train Set (', h, 'Hypotheses ):'
    optapp = getAllVals(dir+'trainbest'+str(h), 100)
    curapp = getAllVals(dir+'trainnaive'+str(h), 100)
    betterAll(optapp, curapp)
  except IOError:
    pass
