% CLASSIFIEREXAMPLE     Example of using an svlClassifier in Matlab
% Stephen Gould <sgould@stanford.edu>
%

% add SVL to the Matlab path
run('../../svl/scripts/addSVLPaths.m');

% generate random training examples
data = rand(50, 2);
labels = prod(data, 2) - data(:, 1);
labels = double(labels > mean(labels));

% show dither plot of features
ditherPlot(data, labels);

% train logistic classifier
options = struct('verbose', 1);
model = mexTrainClassifier(data, labels, [], options);

% evaluate classifier (and adjust for zero-indexing)
marginals = mexEvalClassifier(model, data);
[pr, predictedLabels] = max(marginals, [], 2);
predictedLabels = predictedLabels - 1;

% show confusion matrix
disp(confusionMatrix(labels, predictedLabels));
