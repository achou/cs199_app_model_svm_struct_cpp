/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2009, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    filterImages.cpp
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
** DESCRIPTION:
**   Given an XML file and a sequence of images, removes all the images
** unused in the detections file
*****************************************************************************/

#include <cstdlib>
#include <string>
#include <limits>
#include <iomanip>
#include <fstream>
#include <map>
#include <vector>

#include "svlBase.h"
#include "svlVision.h"

using namespace std;

void usage()
{
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./filterImages <objects xml> <image seq> <output image seq>" << endl;
    cerr << "OPTIONS:" << endl
	 << endl;
}


#define NUM_REQUIRED_PARAMETERS 3

int main(int argc, char **argv)
{
  if (argc < NUM_REQUIRED_PARAMETERS) {
    usage();
    return -1;
  }

  const char *gtFilename = argv[1];
  const char *imageSeqFilename = argv[2];
  const char *outputImageSeq = argv[3];

  ofstream ofs;

  svlObject2dSequence objects;
  SVL_LOG(SVL_LOG_VERBOSE, "Reading from " << gtFilename);
	
  if (!objects.read(gtFilename)) {
    SVL_LOG(SVL_LOG_ERROR, "...failed to read " << gtFilename);
  }

  svlImageSequence images;
  images.load(imageSeqFilename);

  svlImageSequence imagesToKeep;
  imagesToKeep.setDirectory(images.directory());

  for (unsigned i = 0; i < images.size(); i++) {
    map<string, svlObject2dFrame>::const_iterator it = objects.find(strBaseName(images[i]));
    if (it == objects.end() || it->second.empty()) continue;
    imagesToKeep.push_back(images[i]);
  }

  imagesToKeep.save(outputImageSeq);

  return 0;
}

