//Author: Ian Goodfellow <ia3n@cs.stanford.edu>

#include "svlVision.h"

void usage()
{
  cout << "bin/summarizeExtractor <extractor.xml>" << endl;
  cout << "(Prints out a list of the number/type of features in the extractor)" << endl;
}

int main(int argc, char ** argv)
{
  if (argc != 2)
    {
      usage();
      return -1;
    }

  const char * filename = argv[1];


  svlFeatureExtractor * extractor = svlFeatureExtractorFactory().load(filename);

  assert(extractor);

  cout << extractor->numFeatures() << endl;

  cout << extractor->summary() << endl;
}
