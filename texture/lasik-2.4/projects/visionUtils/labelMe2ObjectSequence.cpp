/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2009, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    labelMe2ObjectSequence.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
** DESCRIPTION:
**  Converts from LabelMe XML format to SVL Object Sequence.
*****************************************************************************/

#include <cstdlib>
#include <string>
#include <limits>
#include <iomanip>
#include <fstream>
#include <map>
#include <vector>

#include "xmlParser/xmlParser.h"

#include "svlBase.h"
#include "svlVision.h"

using namespace std;

void usage()
{
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./labelMe2ObjectSequence [OPTIONS] <dirIn> <objSeqOut>\n";
    cerr << "OPTIONS:\n"
         << SVL_STANDARD_OPTIONS_USAGE
	 << endl;
}

int main(int argc, char **argv)
{
    const int NUM_REQUIRED_PARAMETERS = 2;

    SVL_BEGIN_CMDLINE_PROCESSING(argc, argv)
    SVL_END_CMDLINE_PROCESSING(usage())

    if (SVL_CMDLINE_ARGC != NUM_REQUIRED_PARAMETERS) {
        usage();
        return -1;
    }

    const char *inDir = SVL_CMDLINE_ARGV[0];
    const char *outSeq = SVL_CMDLINE_ARGV[1];

    // process files
    svlObject2dSequence seq;
    vector<string> files = svlDirectoryListing(inDir, ".xml");
       
    for (vector<string>::const_iterator it = files.begin(); it != files.end(); it++) {
        SVL_LOG(SVL_LOG_VERBOSE, "Processing file " << *it << "...");
        
        XMLNode root = XMLNode::parseFile(it->c_str(), "annotation");
        XMLNode node = root.getChildNode("filename");
        if (node.isEmpty()) {
            SVL_LOG(SVL_LOG_WARNING, "could not parse filename for annotations file " << *it);
            continue;
        }

        string baseName = strBaseName(node.getText());
        svlObject2dFrame frame;
        for (int i = 0; i < root.nChildNode("object"); i++) {
            node = root.getChildNode("object", i);
            XMLNode subNode = node.getChildNode("name");
            if (node.isEmpty() || subNode.isEmpty()) {
                SVL_LOG(SVL_LOG_WARNING, i << "-th object is empty in " << *it);                
                continue;
            }

            svlObject2d object((double)SVL_INT_MAX, (double)SVL_INT_MAX, 0.0, 0.0);
            object.name = string(subNode.getText());
            
            subNode = node.getChildNode("polygon");
            for (int j = 0; j < subNode.nChildNode("pt"); j++) {
                XMLNode ptNode = subNode.getChildNode("pt", j);
                int x = atoi(ptNode.getChildNode("x").getText());
                int y = atoi(ptNode.getChildNode("y").getText());
                if (x < object.x) object.x = x;
                if (x > object.w) object.w = x;
                if (y < object.y) object.y = y;
                if (y > object.h) object.h = y;
            }

            object.w -= object.x;
            object.h -= object.y;
            frame.push_back(object);
        }
        
        seq[baseName] = frame;
    }

    // save output sequence
    SVL_LOG(SVL_LOG_VERBOSE, "Writing object sequence to " << outSeq << "...");
    seq.write(outSeq);
    
    svlCodeProfiler::print(cerr);
    return 0;
}

