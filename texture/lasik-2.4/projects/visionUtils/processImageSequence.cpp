/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2009, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    processImageSequence.cpp
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
** DESCRIPTION:
**    A set of utilities for processing image sequence xml files
*****************************************************************************/

#include <cstdlib>
#include <string>
#include <limits>
#include <iomanip>
#include <fstream>
#include <map>
#include <vector>

#include "svlBase.h"
#include "svlVision.h"

using namespace std;

void usage()
{
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./processImageSequence [OPTIONS] <imageSequenceIn> <imageSequenceOut>" << endl;
    cerr << "OPTIONS (performed in this order):" << endl
	 << "  -add <filename>  :: adds in another image sequence (can be multiple)" << endl
	 << "  -subtract <filename> :: removes all images that occur in filename (can be multiple; doesn't work with regions)" << endl
	 << "  -randomize       :: randomizes (currently keeps all regions of the same image together still" << endl
	 << "  -g <n>           :: during randomization keeps images together in groups of n (default: 1)" << endl
	 << "  -n <n>           :: truncate at n images/regions " << endl
	 << "  -folds <n>       :: split the images into n separate files (imageSequenceOut.#.xml)" << endl
	 << "  -perFold <n>     :: number of examples to put in each fold (default: split all evenly)" << endl
	 << "  -restPerFold <filename> :: dump all remaining examples from each fold into filename.#.xml" << endl
	 << "  -rest <filename> :: dump all of the remaining examples from truncating/splitting into folds" << endl
	 << endl;
}

#define NUM_REQUIRED_PARAMETERS 2

int main(int argc, char **argv)
{
  vector<const char *> toAdd;
  vector<const char *> toSubtract;
  bool bRandomize = false;
  int groupSize = 1;
  //bool bSplit = false;
  int numFolds = 1;
  int numPerFold = -1;
  const char *restFilename = NULL;
  const char *restPerFoldFilestem = NULL;
  int maxNum = INT_MAX;

  SVL_BEGIN_CMDLINE_PROCESSING(argc, argv)
    SVL_CMDLINE_VEC_OPTION("-add", toAdd)
    SVL_CMDLINE_VEC_OPTION("-subtract", toSubtract)    
    SVL_CMDLINE_BOOL_OPTION("-randomize", bRandomize)
    SVL_CMDLINE_INT_OPTION("-g", groupSize)
    SVL_CMDLINE_INT_OPTION("-folds", numFolds)
    SVL_CMDLINE_INT_OPTION("-perFold", numPerFold)
    SVL_CMDLINE_STR_OPTION("-rest", restFilename)
    SVL_CMDLINE_STR_OPTION("-restPerFold", restPerFoldFilestem)
    SVL_CMDLINE_INT_OPTION("-n", maxNum)
  SVL_END_CMDLINE_PROCESSING(usage())

  if (SVL_CMDLINE_ARGC < NUM_REQUIRED_PARAMETERS) {
    usage();
    return -1;
  }

  const char *inputSeq = SVL_CMDLINE_ARGV[0];
  const char *outputSeq = SVL_CMDLINE_ARGV[1];

  svlImageRegionsSequence seq;
  seq.load(inputSeq);

  /////////////////////////////////
  // add
  ////////////////////////////////

  for(unsigned i = 0; i < toAdd.size(); i++) {
    svlImageRegionsSequence seq2;
    seq2.load(toAdd[i]);
    seq.add(seq2);
  }

  /////////////////////////////////
  // subtract
  /////////////////////////////////

  for(unsigned i = 0; i < toSubtract.size(); i++) {
    svlImageRegionsSequence seq2;
    seq2.load(toSubtract[i]);
    seq.subtract(seq2);
  }

  /////////////////////////////////
  // randomize
  /////////////////////////////////
 
  seq.shuffleImages();

  ////////////////////////////////
  // truncate
  ///////////////////////////////
  svlImageRegionsSequence remaining;
  if (restFilename) {
    seq.truncateWithRemainder(maxNum, remaining);
  } else {
    seq.truncate(maxNum);
  }


  //////////////////////////////////
  // folds
  //////////////////////////////////
  if (numFolds > 1) {
    if (numPerFold < 0)
      numPerFold = seq.size() / numFolds;

    svlImageRegionsSequence firstPart;
    svlImageRegionsSequence lastPart = seq;

    for (int i = 0; i < numFolds; i++) {
      svlImageRegionsSequence fold;
      fold = lastPart;
      fold.truncateWithRemainder(numPerFold, lastPart);
      
      stringstream filename;
      filename << outputSeq << "." << i << ".xml";
      fold.save(filename.str().c_str());

      svlImageRegionsSequence remainder = firstPart;
      remainder.add(lastPart);
      stringstream filename2;
      filename2 << restPerFoldFilestem << "." << i << ".xml";
      remainder.save(filename2.str().c_str());

      firstPart.add(fold);
    }

    if (restFilename) {
      remaining.add(lastPart);
      remaining.save(restFilename);
    }
  } else {
    seq.save(outputSeq);
    remaining.save(restFilename);
  }

  return 0;
}

