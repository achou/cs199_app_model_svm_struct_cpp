/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2009, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    computeDetectionStatistics.cpp
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
** DESCRIPTION:
**   Quick application for counting the number of each type of object
**   among the given images, etc.
*****************************************************************************/

#include <cstdlib>
#include <string>
#include <limits>
#include <iomanip>
#include <fstream>
#include <map>
#include <vector>

#include "svlBase.h"
#include "svlVision.h"

using namespace std;

void usage()
{
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./computeDetectionStatistics <objects xml> <imageSequence>" << endl;
}


struct info {
  int count;
  float sum_ratios;
  int sum_widths;
  int sum_heights;
  int min_width;
  int min_height;

  info(int w, int h) :
    count(0), sum_ratios(0), sum_widths(0), sum_heights(0),
    min_width(INT_MAX), min_height(INT_MAX)
  {
    update(w, h);
  }

  void update(int w, int h) {
    count++;
    sum_ratios += ((float)w / h);
    sum_widths += w;
    sum_heights += h;
    min_width = min(min_width, w);
    min_height = min(min_height, h);
  }

  void print() {
    cerr << "\tNum: " << count << endl;
    cerr << "\tAvg. ratio: " << sum_ratios / count << endl;
    cerr << "\tMin. size: " << min_width << " x " << min_height << endl;
    cerr << "\tAvg. size: " << (float)sum_widths / count
	 << " x " << (float)sum_heights / count << endl;
  }
};



#define NUM_REQUIRED_PARAMETERS 2

int main(int argc, char **argv)
{
  char *inputFilename;
  char *imageSeq;

  char **args = argv + 1;
  while (--argc > NUM_REQUIRED_PARAMETERS) {
    if (**args == '-') {
      SVL_LOG(SVL_LOG_FATAL, "unrecognized option " << *args);
    } else {
      break;
    }
    args++;
  }

  if (argc < NUM_REQUIRED_PARAMETERS) {
    usage();
    return -1;
  }

  inputFilename = args[0];
  imageSeq = args[1];

  svlObject2dSequence objects;
  SVL_LOG(SVL_LOG_VERBOSE, "Reading from " << inputFilename);
	
  if (!objects.read(inputFilename)) {
    SVL_LOG(SVL_LOG_ERROR, "...failed to read " << inputFilename);
  }

  svlImageSequence sequence;
  SVL_ASSERT_MSG(strExtension(imageSeq).compare("xml") == 0, imageSeq << " is not an image sequence");
  sequence.load(imageSeq);
 
  map<string, info> data;
  map<string, info>::iterator it;

  for (size_t level = 0; level < sequence.size(); level++) {
    map<string, svlObject2dFrame>::iterator iter = objects.find(strBaseName(sequence[level]));
    // no objects in this image
    if (iter == objects.end()) continue;

    svlObject2dFrame &curr_objects = iter->second;
    for (size_t i = 0; i < curr_objects.size(); i++) {
      svlObject2d object = curr_objects[i];

      it = data.find(object.name);
      if (it == data.end()) {
	data.insert(make_pair(object.name, info((int)object.w, (int)object.h)));
      } else {
	it->second.update((int)object.w, (int)object.h);
      }
    }
  }

  cerr << "Num images: " << sequence.size() << endl;

  cerr << "Object statistics:" << endl;
  for (it = data.begin(); it != data.end(); it++) {
    cerr << it->first << ": " << endl;
    it->second.print();
  }

}

