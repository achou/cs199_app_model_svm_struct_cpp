/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2008, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    countSlidingWindows.cpp
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
** DESCRIPTION:
**
*****************************************************************************/

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <string.h>

#include "cv.h"
#include "cxcore.h"
#include "highgui.h"

#include "svlBase.h"
#include "svlML.h"
#include "svlVision.h"

#include <sys/time.h>
#include <ctime>

using namespace std;




// main -----------------------------------------------------------------------

void usage()
{
  //assert(false);
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./countSlidingWindows [OPTIONS] <dictionary> <imageSequence>" << endl
	 << "  -dscale               :: delta_scale for the classifier (default: 1.2)" << endl
	 << "  -dshift               :: delta_shift for the classifier (default: 4)" << endl
	 << endl;
}



int main(int argc, char *argv[])
{
  svlLogger::setLogLevel(SVL_LOG_MESSAGE);
    
  float delta_scale = svlSlidingWindowDetector::DELTA_SCALE;
  int delta_shift_x = svlSlidingWindowDetector::DELTA_X;
  int delta_shift_y = svlSlidingWindowDetector::DELTA_Y;

  char **args = argv + 1;
  while (--argc) {
    if (!strcmp(*args, "-dscale")) {
	delta_scale = atof(*(++args));         argc--;
    } else if (!strcmp(*args, "-dshift")) {
      delta_shift_x = atoi(*(++args));         argc--;
      delta_shift_y = delta_shift_x;
   } else if ((*args)[0] == '-') {
      SVL_LOG(SVL_LOG_FATAL, "unrecognized option " << *args);
    } else {
      break;
    }
    args++;
  }

  if (argc < 2) {
    usage();
    return -1;
  }

    // read commandline parameters
  const char *dictionaryFilename = args[0];
  const char *imageFilename = args[1];
        
  svlImageSequence sequence;
  sequence.load(imageFilename);
  assert(sequence.size() > 0);

  string imageName = sequence.directory() + string("/") + sequence[0];

  // initialize object detector
  svlSlidingWindowDetector detector;
  svlPatchDictionary dictionary;
  dictionary.load(dictionaryFilename);
  detector.setFeatureExtractor(&dictionary);
  
  detector.setDeltas(delta_shift_x, delta_shift_y, delta_scale);

  map<double, vector<CvPoint> > locations;

  IplImage *image = cvLoadImage(imageName.c_str());
  detector.createAllWindowLocations(image->width, image->height, locations);

  int num_total = 0;
  for (map<double, vector<CvPoint> >::const_iterator it = locations.begin();
       it != locations.end(); it++) {
    //cerr << it->second.size() << endl;
    num_total += it->second.size();
  }

  cerr << num_total << endl;
  return 0;
}

