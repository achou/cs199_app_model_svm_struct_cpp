/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2009, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    splitImages.cpp
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
** DESCRIPTION:
**   Allows one to work with very large images by splitting them into more
** manageable-sized components (alternatively, can reverse by combining them
** back into one large image)
*****************************************************************************/

#include <cstdlib>
#include <string>
#include <limits>
#include <iomanip>
#include <fstream>
#include <map>
#include <vector>

#include "svlBase.h"
#include "svlVision.h"

using namespace std;

static void saveImage(const char *filename, IplImage *img, svlColorType type)
{
  switch (type) {
  case SVL_COLOR_UNDEFINED:
    writeMatrixAsIplImage(img, filename);
    break;
  default:
    cvSaveImage(filename, img);
  }
}

void usage()
{
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./splitImages [OPTIONS] (<image seq> | <image>*)" << endl;
    cerr << "OPTIONS:" << endl
	 << "  -n <n>            :: split each image into n^2 blocks (default: 1)" << endl
	 << "  -reverse          :: combines the n^2 blocks named as by this program back into one" << endl
	 << SVL_STANDARD_OPTIONS_USAGE 
	 << endl;
}


#define NUM_REQUIRED_PARAMETERS 1

int main(int argc, char **argv)
{
  int n = 1;
  bool reverse = false;
  SVL_BEGIN_CMDLINE_PROCESSING(argc, argv)
    SVL_CMDLINE_INT_OPTION("-n", n)
    SVL_CMDLINE_BOOL_OPTION("-reverse", reverse)
  SVL_END_CMDLINE_PROCESSING(usage());

 if (SVL_CMDLINE_ARGC < NUM_REQUIRED_PARAMETERS) {
    usage();
    return -1;
  }

 svlImageLoader loader(false, true);
 for (unsigned chNum = 0; chNum < loader.numChannels(); chNum++) {
   if (loader.channelTypes(chNum) == SVL_CHANNEL_EDGE) 
     SVL_LOG(SVL_LOG_WARNING, "Edge channel will be ignored");
 }

 loader.readCommandLine(SVL_CMDLINE_ARGV, SVL_CMDLINE_ARGC);
 unsigned numImages = loader.numFrames();
  
 for (unsigned index = 0; index < numImages; index++) 
    {
      string basename = strFilename(loader.getFrameBasename(index));
      SVL_LOG(SVL_LOG_VERBOSE, "Processing " << basename << "...");

      if (!reverse) {
	// split the given image into n^2 components
	vector<IplImage *> images;
	bool success = loader.getFrame(index, images);
	if (!success) continue;
	
	for (unsigned chNum = 0; chNum < loader.numChannels(); chNum++) {
	  if (loader.channelTypes(chNum) == SVL_CHANNEL_EDGE) continue;
	  string ext = loader.extensions(chNum);

	  IplImage *img = images[chNum];
	  int w = img->width / n;
	  int h = img->height / n;
	  IplImage *tmp = cvCreateImage(cvSize(w, h), img->depth, img->nChannels);
	  unsigned count = 0;
	  for (int i = 0; i < n; i++) {
	    for (int j = 0; j < n; j++, count++) {
	      cvSetImageROI(img, cvRect(i*w, j*h, w, h));
	      cvCopy(img, tmp);
	      cvResetImageROI(img);
	      char filename[256];
	      sprintf(filename, "%s_%i%s", basename.c_str(), count, ext.c_str());
	      cvSaveImage(filename, tmp);
	    }
	  }
	  cvReleaseImage(&tmp);
	  cvReleaseImage(&img);
	}
      } else {
	unsigned count = 0;
	vector<IplImage *> images(loader.numChannels(), NULL);
	int w = -1;
	int h = -1;

	for (int i = 0; i < n; i++) {
	  for (int j = 0; j < n; j++, count++) {
	    // obtain this area of the image
	    vector<string> filenames;
	    for (unsigned chNum = 0; chNum < loader.numChannels(); chNum++) {
	      string ext = loader.extensions(chNum);
	      
	      char filename[256];
	      sprintf(filename, "%s_%i%s", basename.c_str(), count, ext.c_str());
	      filenames.push_back(string(filename));
	    }
	    vector<IplImage *> imageRegion;
	    loader.getFrameFromFiles(filenames, imageRegion);

	    for (unsigned chNum = 0; chNum < loader.numChannels(); chNum++) {
	      if (loader.channelTypes(chNum) == SVL_CHANNEL_EDGE) continue;

	      if (count == 0) {
		w = imageRegion[0]->width;
		h = imageRegion[0]->height;
		images[chNum] = cvCreateImage(cvSize(w*n, h*n),
					      imageRegion[chNum]->depth,
					      imageRegion[chNum]->nChannels);
	      }

	      cvSetImageROI(images[chNum], cvRect(i*w, j*h, w, h));
	      cvCopy(imageRegion[chNum], images[chNum]);
	      cvResetImageROI(images[chNum]);
	    }
	  }
	}

	// now images is populated with the appropriate images for each channel
	for (unsigned chNum = 0; chNum < loader.numChannels(); chNum++) {
	  if (loader.channelTypes(chNum) == SVL_CHANNEL_EDGE) continue;
	  string ext = loader.extensions(chNum);
	  char filename[256];
	  sprintf(filename, "%s%s", basename.c_str(), ext.c_str());
	  saveImage(filename, images[chNum], loader.colorType(chNum));
	}
      }
    }
  return 0;
}

