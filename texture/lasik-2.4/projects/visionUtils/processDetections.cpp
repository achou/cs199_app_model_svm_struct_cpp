/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2009, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    processDetections.cpp
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
** DESCRIPTION:
**  A set of utilities for processing detection files (Object2dSequences)
**
*****************************************************************************/

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <string.h>

#include "cv.h"
#include "cxcore.h"
#include "highgui.h"

#include "svlBase.h"
#include "svlML.h"
#include "svlVision.h"

using namespace std;

// main -----------------------------------------------------------------------

void usage()
{
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./processDetections <detections filename> <output filename> " << endl;
    cerr << "OPTIONS (performed in this order!):" << endl
	 << "  -removeBelow <n>      :: removes all detections below this probability" << endl
	 << "  -removeUnused <image_seq>    :: removes all detections from frames not in the imageSequence" << endl
	 << "  -removeRegion <xmin> <xmax> <ymin> <ymax>    :: removes all detections in the specified rectangle" << endl
	 << "  -removeSmall <w> <h>   :: removes all detections smaller than w x h" << endl
	 << "  -expand <n>            :: expand all detection by n pixels" << endl
         << "  -scale <scale>         :: multiplies all detections by that scale" << endl
	 << "  -splitByName           :: generates one file for each object name (<output filename><$object.xml>" << endl
	 << SVL_STANDARD_OPTIONS_USAGE
	 << endl;
}

#define NUM_REQUIRED_PARAMETERS 2

int main(int argc, char *argv[])
{		
  const char *onlyin = NULL;
  int xmin = INT_MAX, xmax = 0, ymin = INT_MIN, ymax = 0;
  bool bRemoveRegion = false;
  int minWidth = INT_MAX, minHeight = INT_MAX;
  bool bRemoveSmall = false;
  int numPixels = 0;
  float scale = 1.0;
  bool bSplitByName = false;
  float minProb = FLT_MIN;

  SVL_BEGIN_CMDLINE_PROCESSING(argc, argv)
    SVL_CMDLINE_REAL_OPTION("-removeBelow", minProb)
    SVL_CMDLINE_STR_OPTION("-removeUnused", onlyin)
    SVL_CMDLINE_OPTION_BEGIN("-removeRegion", ptr)
      xmin = atoi(ptr[0]);
      xmax = atoi(ptr[1]);
      ymin = atoi(ptr[2]);
      ymax = atoi(ptr[3]);
      bRemoveRegion = true;
    SVL_CMDLINE_OPTION_END(4)
    SVL_CMDLINE_OPTION_BEGIN("-removeSmall", ptr)
      minWidth = atoi(ptr[0]);
      minHeight = atoi(ptr[1]);
      bRemoveSmall = true;
    SVL_CMDLINE_OPTION_END(2)
    SVL_CMDLINE_INT_OPTION("-expand", numPixels)
    SVL_CMDLINE_REAL_OPTION("-scale", scale)
    SVL_CMDLINE_BOOL_OPTION("-splitByName", bSplitByName)
  SVL_END_CMDLINE_PROCESSING(usage())

  if (SVL_CMDLINE_ARGC < NUM_REQUIRED_PARAMETERS)  {
    usage();
    return -1;
  }
  
  const char *detectionsFilename = SVL_CMDLINE_ARGV[0];
  const char *outputFilename = SVL_CMDLINE_ARGV[1];
  
  svlObject2dSequence detections;
  SVL_LOG(SVL_LOG_VERBOSE, "Reading from " << detectionsFilename);
  if (!detections.read(detectionsFilename)) {
    SVL_LOG(SVL_LOG_ERROR, "...failed to read " << detectionsFilename);
  }

  /////////////////////////
  // remove below
  ///////////////////////
  for (svlObject2dSequence::iterator it = detections.begin(); it != detections.end(); it++) {
    removeBelowProbability(it->second, minProb);
  }
  /////////////////////////////
  // only in
  ////////////////////////////
  if (onlyin) {
    svlImageSequence images;
    images.load(onlyin);

    svlObject2dSequence objectsToKeep;
  
    for (unsigned i = 0; i < images.size(); i++) {
      map<string, svlObject2dFrame>::const_iterator it = detections.find(strBaseName(images[i]));
      if (it == detections.end()) continue;
      objectsToKeep[it->first] = it->second;
    }
    
    detections.clear();
    detections = objectsToKeep;
  }

  //////////////////////////////
  // removeRegion
  /////////////////////////////
  if (bRemoveRegion) {
    for (svlObject2dSequence::iterator it = detections.begin(); it != detections.end(); it++) {
      svlObject2dFrame objects = it->second;

      it->second.clear();
      for (unsigned j = 0; j < objects.size(); j++) {
	if (objects[j].x >= xmin && objects[j].x <= xmax && objects[j].y >= ymin && objects[j].y <= ymax)
	  continue;
	
	it->second.push_back(objects[j]);
      }
    }
  }

  /////////////////////////////
  // removeSmall
  /////////////////////////////
  if (bRemoveSmall) {
    for (map<string, svlObject2dFrame>::iterator it = detections.begin(); it != detections.end(); it++) {
      removeSmallObjects(it->second, minWidth, minHeight);
    }
  }

  ///////////////////////////
  // expand
  ///////////////////////////

  if (numPixels > 0) {
    for (svlObject2dSequence::iterator it = detections.begin(); it != detections.end(); it++) {
      for (unsigned i = 0; i < it->second.size(); i++) {
	it->second[i].x -= numPixels;
	it->second[i].y -= numPixels;
	it->second[i].w += 2*numPixels;
	it->second[i].h += 2*numPixels;
	
	if (it->second[i].x < 0) {
	  it->second[i].x = 0;
	  it->second[i].w -= numPixels;
	}
	if (it->second[i].y < 0) {
	  it->second[i].y = 0;
	  it->second[i].h -= numPixels;
	}
      }
    }
  }

  //////////////////////////////
  // scaling
  /////////////////////////////
  scaleObject2dSequence(detections, scale, scale);

  /////////////////////////////
  // split by name
  ////////////////////////////
  if (bSplitByName) {
    set<string> names;
    for (svlObject2dSequence::iterator it = detections.begin(); it != detections.end(); it++) {
      svlObject2dFrame &objects = it->second;
      for (unsigned i = 0; i < objects.size(); i++) {
	names.insert(objects[i].name);
      }
    }
     
    SVL_LOG(SVL_LOG_MESSAGE, "Found " << names.size() << " object classes");
    for (set<string>::iterator names_it = names.begin(); names_it != names.end(); names_it++) {
      string name = *names_it;
      set<string> notinclude = names;
      notinclude.erase(name);
      svlObject2dSequence named_objects = detections;
      for (svlObject2dSequence::iterator it = named_objects.begin(); it != named_objects.end(); it++) {
	removeMatchingObjects(it->second, notinclude);
      }
      
      const string filename = string(outputFilename) + name + string(".xml");
      named_objects.write(filename.c_str());
    }
  }

  else {
    detections.write(outputFilename);
  }
  return 0;
}
