#!/bin/csh
#
# STAIR VISION LIBRARY
# Copyright (c) 2007-2010, Stanford University
#
# FILENAME:    install.sh
# AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
# DESCRIPTION:
#   Installs required external libraries: OpenCV, wxWidgets, eigen.
#

set CODEBASE = `pwd`/..

# wxWidgets
if (! -e wxGTK) then
    set VERSION = "2.8.11"
    wget -c http://prdownloads.sourceforge.net/wxwindows/wxGTK-${VERSION}.tar.gz || exit 1
    tar xzvf wxGTK-${VERSION}.tar.gz
    cd wxGTK-${VERSION}
    mkdir buildgtk
    cd buildgtk
    ../configure --disable-shared --with-opengl --enable-monolithic || exit 2
    make || exit 3
    cd ../..
    ln -s wxGTK-${VERSION} ${CODEBASE}/external/wxGTK
endif

# eigen
if (! -e Eigen) then
    set VERSION = "2.0.12"
    wget -c http://bitbucket.org/eigen/eigen/get/${VERSION}.tar.bz2 -O eigen-${VERSION}.tar.bz2 || exit 1
    bunzip2 eigen-${VERSION}.tar.bz2
    tar xvf eigen-${VERSION}.tar
    mv eigen eigen-${VERSION}
    ln -s eigen-${VERSION}/Eigen ${CODEBASE}/external/Eigen
endif
