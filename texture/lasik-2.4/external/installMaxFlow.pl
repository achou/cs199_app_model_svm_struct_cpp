#!/usr/bin/perl
#
# STAIR VISION LIBRARY
# Copyright (c) 2007-2009, Stanford University
#
# FILENAME:    installMaxFlow.pl
# AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
# DESCRIPTION:
#   Installs Vladimir Kolmogorov's maxflow implementation for fast
#   graphical models inference on vision problems. The implementation
#   uses the GPL license and so has to be installed separately. Run
#   from the external directory.
#

use strict;
use Cwd;

my $SVLBASE = getcwd() . "/..";
my $EXTDIR = "${SVLBASE}/external/";
chdir($EXTDIR);

# download and build
#my $URL = "http://www.adastral.ucl.ac.uk/~vladkolm/software/";
my $URL = "http://www.cs.ucl.ac.uk/staff/V.Kolmogorov/software/";

my $SRC = "maxflow-v3.0.src";
`wget ${URL}${SRC}.tar.gz` unless (-e "${SRC}.tar.gz");
`tar zxvf ${SRC}.tar.gz`;

chdir($SRC);
open MAKE, ">Makefile";

# --------------------------------------------------------------
print MAKE <<'EOM';
# Makefile for maxflow library
# Stephen Gould <sgould@stanford.edu>
#

all : maxflow.a

# to force 32-bit compile (set to 1 in make.local)
FORCE32BIT = 0
ifeq (,$(MACHTYPE))
  MACHTYPE = $(shell uname -im)
endif
ifeq (,$(findstring x86_64,$(MACHTYPE)))
  FORCE32BIT = 1
endif

# include local user make.local to override any settings
ifeq ($(wildcard ../../make.local), ../../make.local)
  -include ../../make.local
endif

ifeq ($(FORCE32BIT), 0)
  CPP = g++ -O2 -fPIC -frepo -DNDEBUG
else
  CPP = g++ -O2 -m32 -frepo -DNDEBUG
endif

maxflow.a: maxflow.o graph.o
	ar rucs $@ $?

maxflow.o: maxflow.cpp block.h graph.h
	$(CPP) -c -o maxflow.o maxflow.cpp

graph.o: graph.cpp block.h graph.h
	$(CPP) -c -o graph.o graph.cpp

clean:
	-rm *~ *.o *.a



EOM
# --------------------------------------------------------------

close MAKE;

`make`;

# update make.local
chdir($SVLBASE);
open MAKE, ">>make.local";
# --------------------------------------------------------------
print MAKE <<'EOM';

EXTRA_CFLAGS += -DKOLMOGOROV_MAXFLOW
EXTRA_LFLAGS += ${EXT_PATH}/maxflow-v3.0.src/maxflow.a \
        ${EXT_PATH}/maxflow-v3.0.src/maxflow.o \
        ${EXT_PATH}/maxflow-v3.0.src/graph.o
EOM
# --------------------------------------------------------------

close MAKE;

