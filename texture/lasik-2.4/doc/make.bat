@ECHO OFF
REM  STAIR VISION LIBRARY DOCUMENTATION WIN32 MAKE SCRIPT
REM  Stephen Gould <sgould@stanford.edu>

SET JPG_SRC= 1_9_s.image.jpg 1_9_s.labels.jpg 1_9_s.seg.jpg ^
	gradient.jpg gradient.rainbow.jpg gradient.hot.jpg ^
	gradient.cool.jpg gradient.greenred.jpg
SET PNG_SRC= regionlabeler001.png regionlabeler002.png regionlabeler003.png ^
	hm.img.png hm.edges.png hm.heat.png

SET DOC_SRC= svlBook.tex svlMatlabCRFTutorial.tex ^
	svlTrainingObjectDetectors.tex

if "%1" == "clean" GOTO clean
if "%1" == "figs" GOTO figs
GOTO pdf

:figs
FOR %%F IN ( %JPG_SRC% ) DO ECHO %%F
FOR %%F IN ( %PNG_SRC% ) DO ECHO %%F
GOTO end

:pdf
FOR %%D IN ( %DOC_SRC% ) DO (
	pdflatex -interaction=nonstopmode %%D
	bibtex %%~nD
	pdflatex -interaction=nonstopmode %%D
	pdflatex -interaction=nonstopmode %%D
)
GOTO end

:clean
FOR %%D IN ( %DOC_SRC% ) DO (
	del %%~nD.aux
	del %%~nD.bbl
	del %%~nD.blg
	del %%~nD.dvi
	del %%~nD.log
	del %%~nD.pdf
)
GOTO end

:end