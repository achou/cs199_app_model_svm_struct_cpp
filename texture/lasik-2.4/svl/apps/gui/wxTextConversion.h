/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    wxTextConversion.h
** AUTHOR(S):   Ian Goodfellow <ia3n@cs@stanford.edu>
** DESCRIPTION: Functions used by wx gui apps to convert between different
**              string formats
**
**
*****************************************************************************/


#pragma once

#include "wx/wx.h"
#include <string>
using namespace std;

//None of these are complicated functions;
//they are just intended to improve readability
//by using names that should be meaningful to
//lasik authors


inline string wxStr2stlStr(const wxString & str)
{
  return string(str.mb_str(wxConvUTF8));
}

//This one needs to be a macro or the c string will get ruined when
//the function returns
#define wxStr2cStr( str )( (const char *) str.mb_str(wxConvUTF8))

//Similar issue for this one
#define stlStr2wxCharStar( str ) stlStr2wxStr( str ).GetData()


inline wxString stlStr2wxStr(const string & str)
{
  return wxString(str.c_str(), wxConvUTF8);
}

inline wxString cStr2wxStr(const char * str)
{
  return wxString(str, wxConvUTF8);
}
