/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    classifyImages.cpp
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
**
** DESCRIPTION:
**  Application for verifying Cuda functions.
**
*****************************************************************************/

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <map>
#include <string.h>

#include "cv.h"
#include "cxcore.h"
#include "highgui.h"

#include "svlBase.h"
#include "svlML.h"
#include "svlVision.h"
#include "svlCuda.h"

using namespace std;

#define GET_ELAPSED_MS(tic) ( float(clock()-(tic))*1000.0f/float(CLOCKS_PER_SEC) )
#define GET_ELAPSED_MS2(tic,toc) ( float((toc)-(tic))*1000.0f/float(CLOCKS_PER_SEC) )
#define PRINT_ELAPSED_MS(tic) printf("%0.3f\n", float(clock()-tic)*1000.0f/float(CLOCKS_PER_SEC));
#define PRINT_PADDED1(w1,f) cout << setw(w1) << setfill(' ') << int(f) << "." << setw(1) << setfill('0') << int(10*f)%10;
#define PRINT_PADDED3(w1,f) cout << setw(w1) << setfill(' ') << int(f) << "." << setw(3) << setfill('0') << int(1000*f)%1000;
#define PP_DEF(f) PRINT_PADDED3(5,f)

template<typename T> void print_bench_grid(const char *title, const char *label, vector<T> &cols, vector<T> &rows, vector<float> &ts, vector<float> *versus = NULL);

int main(int argc, char *argv[])
{
	svlCudaMemHandler *cm;
	cm->setTracking(true);
	svlCudaTimer::setEnabled(true);
	svlLogger::setLogLevel(SVL_LOG_FATAL);

	// Usage
	if ( argc <= 1 ) {
		cout << "USAGE: ./cudaBenchmark [options]\n"
			<< "OPTIONS:\n"
			<< "  -all :: runs all tests\n"
			<< "  -names :: prints names of all tests\n"
			<< "  -off [names] :: turns specified tests off\n"
			<< "  -on [names] :: turns specified tests on\n";;
		return 0;
	}

	// By default, run no tests.
	map<string,bool> runs;
	runs[string("integral")] = false;
	runs[string("conv")] = false;
	runs[string("conv_ccoeff_normed")] = false;
	runs[string("conv32")] = false;
	runs[string("conv_super")] = false;
	runs[string("SWD")] = false;

	// Parse command line arguments.
	for ( int ai=1; ai<argc; ++ai ) {
		if ( strcmp(argv[ai],"-all") == 0 ) {
			for ( map<string,bool>::iterator m=runs.begin(); m!=runs.end(); ++m )
				m->second = true;
		}
		else if ( strcmp(argv[ai],"-names") == 0 ) {
			printf("Names of all tests:\n");
			for ( map<string,bool>::iterator m=runs.begin(); m!=runs.end(); ++m )
				printf("  %s\n", m->first.c_str());
			return 0;
		}
		else if ( strcmp(argv[ai],"-off") == 0 ) {
			// Turn off the following listed names.
			for ( ++ai; ai<argc; ++ai ) {
				if ( *argv[ai] != '-' ) {
					runs[string(argv[ai])] = false;
				} else {
					// Stepping on another option; backtrack.
					ai--;
					break;
				}
			}
		}
		else if ( strcmp(argv[ai],"-on") == 0 ) {
			// Turn on the following listed names.
			for ( ++ai; ai<argc; ++ai ) {
				if ( *argv[ai] != '-' ) {
					runs[string(argv[ai])] = true;
				} else {
					// Stepping on another option; backtrack.
					ai--;
					break;
				}
			}
		}
	}
#if 0
	printf("\nRunning tests:\n");
	for ( map<string,bool>::const_iterator m=runs.begin(); m!=runs.end(); ++m )
		if ( m->second )
			printf(" %s\n", m->first.c_str());
#endif

	// Which sizes to use.
	const int N = 2;
	int ws[] = {201, 640};
	int hs[] = {201, 480};
	int zs[] = {5, 3};

	int szs[N], sz3s[N];
	srand(time(NULL));
	// Random data.
	float *datas[N], *datas_2[N], *tests[N], *verfs[N],
		*data3s[N], *data3s_2[N], *test3s[N], *verf3s[N];
	IplImage *datasI[N], *datasI_2[N];
	for ( unsigned j=0; j<N; ++j ) {
		szs[j] = ws[j] * hs[j];
		sz3s[j] = ws[j] * hs[j] * zs[j];
		datas[j] = (float*)malloc(sizeof(float)*szs[j]);
		datas_2[j] = (float*)malloc(sizeof(float)*szs[j]);
		datasI[j] = cvCreateImage(cvSize(ws[j],hs[j]), IPL_DEPTH_32F, 1);
		datasI_2[j] = cvCreateImage(cvSize(ws[j],hs[j]), IPL_DEPTH_32F, 1);
		tests[j] = (float*)malloc(sizeof(float)*szs[j]);
		verfs[j] = (float*)malloc(sizeof(float)*szs[j]);
		data3s[j] = (float*)malloc(sizeof(float)*sz3s[j]);
		data3s_2[j] = (float*)malloc(sizeof(float)*sz3s[j]);
		test3s[j] = (float*)malloc(sizeof(float)*sz3s[j]);
		verf3s[j] = (float*)malloc(sizeof(float)*sz3s[j]);
		for ( unsigned i=0; i<szs[j]; ++i ) {
			CV_IMAGE_ELEM(datasI[j], float, i/ws[j], i%ws[j]) = datas[j][i] = float(rand()) / float(RAND_MAX);
			CV_IMAGE_ELEM(datasI_2[j], float, i/ws[j], i%ws[j]) = datas_2[j][i] = float(rand()) / float(RAND_MAX);
		}
		for ( unsigned i=0; i<sz3s[j]; ++i )
			data3s[j][i] = float(rand()) / float(RAND_MAX);
	}
	// Just do last, largest size by default.
	int j_start = N-1, j_end = N;
	// Default number of trials to run Cuda and OpenCV for.
	int cuda_tris = 99, cv_tris = 10;

	vector<const char*> test_names;
	vector<bool> passeds;
	float gf, gf2;
	clock_t tic, toc;
	svlCudaConvolution *cc;

	if ( runs["integral"] ) {
		// *** Benchmark integral images ***
		// Regular integral images.
		int j_start = 0;
		int cuda_tris = 499, cv_tris = 99;

		// Loop over image sizes.
		for ( unsigned j=j_start; j<j_end; ++j ) {
			svlCudaPitch *A = cm->allocPitch(ws[j], hs[j]),
				*INTEG = cm->allocPitch(ws[j], hs[j]),
				*TEMP = cm->allocPitch(ws[j], hs[j]),
				*INTEG2 = cm->allocPitch(ws[j], hs[j]);
			cm->pitchToGPU(A, datas[j]);

			IplImage *integ = cvCreateImage(cvSize(ws[j]+1,hs[j]+1), IPL_DEPTH_64F, 1),
				*integ2 = cvCreateImage(cvSize(ws[j]+1,hs[j]+1), IPL_DEPTH_64F, 1);

			// *** Run just integral trials.
			printf("\n\n*** Benchmarking integral image with %d-by-%d image ***\n\n", ws[j], hs[j]);

			// OpenCV:
			tic = clock();
			for ( int i=0; i<cv_tris; ++i )
				cvIntegral(datasI[j], integ);
			printf("OpenCV integral [ms]:   %0.3f ms\n", gf = GET_ELAPSED_MS(tic)/float(cv_tris));

			// Cuda.
			tic = clock();
			for ( int i=0; i<cuda_tris; ++i )
				svlCudaMath::integral(A, INTEG, TEMP);
			toc = clock();
			gf2 = svlCudaTimer::getLastMedianClear();
			if ( gf2 == 0.0f ) gf2 = GET_ELAPSED_MS2(tic,toc)/float(cuda_tris);
			printf("Cuda integral [ms (x)]: %0.3f ms (%0.1fx)\n", gf2, gf/gf2 );

			// *** Run integral and integral-squared trials.
			tic = clock();
			for ( int i=0; i<cv_tris; ++i )
				cvIntegral(datasI[j], integ, integ2);
			printf("\nOpenCV int&int2 [ms]:   %0.3f ms\n", gf = GET_ELAPSED_MS(tic)/float(cv_tris));

			// Cuda.
			tic = clock();
			for ( int i=0; i<cuda_tris; ++i )
				svlCudaMath::integral(A, INTEG, TEMP, INTEG2);
			toc = clock();
			gf2 = svlCudaTimer::getLastMedianClear();
			if ( gf2 == 0.0f ) gf2 = GET_ELAPSED_MS2(tic,toc)/float(cuda_tris);
			printf("Cuda int&int2 [ms (x)]: %0.3f ms (%0.1fx)\n", gf2, gf/gf2 );

			cvReleaseImage(&integ);
			cvReleaseImage(&integ2);
			cm->freePitch(A);
			cm->freePitch(INTEG);
			cm->freePitch(TEMP);
			cm->freePitch(INTEG2);

			printf("\n");
		}
	}


	// *** Benchmark convolution functions ***
	{
		// How many trials to run of each.
#if 1
		int cuda_tris = 9, cv_tris = 5; // For old machines.
#elif 0
		int cuda_tris = 99, cv_tris = 20; // For good machines.
#else
		int cuda_tris = 999, cv_tris = 100; // For frighteningly good machines.
#endif

		// Which size trials to run.
		int j_start = 1, j_end = 2;
		int temp[] = {4, 8, 12, 16};
		vector<int> pws(temp,temp+4), phs(temp,temp+4);

		if ( runs["conv"] ) {
			// Test plain two-dimensional convolution.
			// Loop over image sizes.
			for ( unsigned j=j_start; j<j_end; ++j ) {
				svlCudaPitch *A = cm->allocPitch(ws[j], hs[j]),
					*SAME = cm->allocPitch(ws[j], hs[j]);
				cm->pitchToGPU(A, datas[j]);
				vector<float> cv;
				vector<vector<float> > cudas;
				for ( int k=0; k<2; ++k )
					cudas.push_back(vector<float>());
				// Loop over patch sizes.
				for ( int ph=4; ph<=16; ph+=4 ) {
					for ( int pw=4; pw<=16; pw+=4 ) {
						float *pat = (float*)malloc(sizeof(float)*pw*ph);
						IplImage *pati = cvCreateImage(cvSize(pw,ph), IPL_DEPTH_32F, 1);
						for ( int i=0; i<pw*ph; ++i ) {
							pat[i] = 2.0f*( float(rand())/float(RAND_MAX) ) + 1.0f;
							CV_IMAGE_ELEM(pati, float, i/pw, i%pw) = pat[i];
						}

						IplImage *res = cvCreateImage(cvSize(ws[j]-pw+1, hs[j]-ph+1), IPL_DEPTH_32F, 1);
						svlCudaPitch *FULL = cm->allocPitch(ws[j]+pw-1, hs[j]+ph-1);

						// Run OpenCV trials.
						tic = clock();
						for ( int i=0; i<cv_tris; ++i )
							cvMatchTemplate(datasI[j], pati, res, CV_TM_CCORR);
						cv.push_back(GET_ELAPSED_MS(tic)/float(cv_tris));
						//printf("OpenCV reference: %0.3f ms\n", cv_time = GET_ELAPSED_MS(tic)/float(cv_tris));

						// Same convolution.
						tic = clock();
						for ( int i=0; i<cuda_tris; ++i )
							cc->cudaMatchTemplate(A, pat, pw, ph, SAME, CUDA_TM_CCORR);
						toc = clock();
						gf2 = svlCudaTimer::getLastMedianClear();
						if ( gf2 == 0.0f ) gf2 = GET_ELAPSED_MS2(tic,toc)/float(cuda_tris);
						//printf("Cuda 'same':      %0.3f ms (%0.1fx)\n", gf = svlCudaTimer::getLastMedian(), cv_time / gf );
						cudas[0].push_back(gf2);

						// Full convolution.
						tic = clock();
						for ( int i=0; i<cuda_tris; ++i )
							cc->cudaMatchTemplate(A, pat, pw, ph, FULL, CUDA_TM_CCORR, CUDA_TM_FULL);
						toc = clock();
						gf2 = svlCudaTimer::getLastMedianClear();
						//printf("%d %d -> %0.6f %0.6f\n", tic, toc, gf2, GET_ELAPSED_MS2(tic,toc));
						if ( gf2 == 0.0f ) gf2 = GET_ELAPSED_MS2(tic,toc)/float(cuda_tris);
						cudas[1].push_back(gf2);

						cvReleaseImage(&res);
						cm->freePitch(FULL);
					}
				}
				cm->freePitch(A);
				cm->freePitch(SAME);

				// Print times.
				printf("\n\n*** Benchmarking CCORR convolution with %d-by-%d image ***\n\n", ws[j], hs[j]);
				print_bench_grid<int>("OpenCV [ms]:", "Height\\width", pws, phs, cv);
				print_bench_grid<int>("\nCuda 'same' [ms (speedup)]:", "Height\\width", pws, phs, cudas[0], &cv);
				print_bench_grid<int>("\nCuda 'full' [ms (speedup)]:", "Height\\width", pws, phs, cudas[1], &cv);
				printf("\n");
			}
		}


		if ( runs["conv_ccoeff_normed"] ) {
			// Benchmark normalized correlation coefficient.
			// Loop over images sizes.
			for ( unsigned j=j_start; j<j_end; ++j ) {
				svlCudaPitch *A = cm->allocPitch(ws[j], hs[j]),
					*SAME = cm->allocPitch(ws[j], hs[j]);

				cm->pitchToGPU(A, datas[j]);
				vector<float> cv;
				vector<vector<float> > cudas;
				for ( int k=0; k<1; ++k )
					cudas.push_back(vector<float>());
				// Loop over patch sizes.
				for ( int ph=4; ph<=16; ph+=4 ) {
					for ( int pw=4; pw<=16; pw+=4 ) {
						float *pat = (float*)malloc(sizeof(float)*pw*ph);
						IplImage *pati = cvCreateImage(cvSize(pw,ph), IPL_DEPTH_32F, 1);
						for ( int i=0; i<pw*ph; ++i ) {
							pat[i] = 2.0f*( float(rand())/float(RAND_MAX) ) + 1.0f;
							CV_IMAGE_ELEM(pati, float, i/pw, i%pw) = pat[i];
						}
						IplImage *res = cvCreateImage(cvSize(ws[j]-pw+1, hs[j]-ph+1), IPL_DEPTH_32F, 1);

						// Run OpenCV trials.
						tic = clock();
						for ( int i=0; i<cv_tris; ++i )
							cvMatchTemplate(datasI[j], pati, res, CV_TM_CCOEFF_NORMED);
						cv.push_back(GET_ELAPSED_MS(tic)/float(cv_tris));

						// Same convolution.
						for ( int i=0; i<cuda_tris; ++i )
							cc->cudaMatchTemplate(A, pat, pw, ph, SAME, CUDA_TM_CCOEFF_NORMED);
						cudas[0].push_back(svlCudaTimer::getLastMedianClear());

						cvReleaseImage(&res);
					}
				}
				cm->freePitch(A);
				cm->freePitch(SAME);

				// Print times.
				printf("\n\n*** Benchmarking CCOEFF_NORMED convolution with %d-by-%d image ***\n\n", ws[j], hs[j]);
				print_bench_grid<int>("OpenCV [ms]:", "Height\\width", pws, phs, cv);
				print_bench_grid<int>("\nCuda 'same' [ms (speedup)]:", "Height\\width", pws, phs, cudas[0], &cv);
				printf("\n");
			}
		}

		if ( runs["conv32"] ) {
			// Benchmark normalized correlation coefficient.
			int temp[] = {24, 32};
			vector<int> pws(temp,temp+2), phs(temp,temp+2);

			// Loop over images sizes.
			for ( unsigned j=j_start; j<j_end; ++j ) {
				svlCudaPitch *A = cm->allocPitch(ws[j], hs[j]),
					*SAME = cm->allocPitch(ws[j], hs[j]),
					*TEMP = cm->allocPitch(ws[j], hs[j]);

				cm->pitchToGPU(A, datas[j]);
				vector<float> cv;
				vector<vector<float> > cudas;
				for ( int k=0; k<1; ++k )
					cudas.push_back(vector<float>());
				// Loop over patch sizes.
				for ( vector<int>::const_iterator pw=pws.begin(); pw!=pws.end(); ++pw ) {
					for ( vector<int>::const_iterator ph=phs.begin(); ph!=phs.end(); ++ph ) {
						float *pat = (float*)malloc(sizeof(float) * *pw * *ph);
						IplImage *pati = cvCreateImage(cvSize(*pw,*ph), IPL_DEPTH_32F, 1);
						for ( int i=0; i<*pw * *ph; ++i ) {
							pat[i] = 2.0f*( float(rand())/float(RAND_MAX) ) + 1.0f;
							CV_IMAGE_ELEM(pati, float, i/ (*pw), i%*pw) = pat[i];
						}
						IplImage *res = cvCreateImage(cvSize(ws[j] - *pw + 1, hs[j] - *ph + 1), IPL_DEPTH_32F, 1);

						// Run OpenCV trials.
						tic = clock();
						for ( int i=0; i<cv_tris; ++i )
							cvMatchTemplate(datasI[j], pati, res, CV_TM_CCORR);
						cv.push_back(GET_ELAPSED_MS(tic)/float(cv_tris));

						// Same convolution.
						for ( int i=0; i<cuda_tris; ++i )
							cc->cudaMatchTemplate(A, pat, *pw, *ph, SAME, CUDA_TM_CCORR, CUDA_TM_VALID, -1, -1, TEMP);
						cudas[0].push_back(svlCudaTimer::getLastMedianClear());

						cvReleaseImage(&res);
					}
				}
				cm->freePitch(A);
				cm->freePitch(SAME);
				cm->Free(TEMP);

				// Print times.
				printf("\n\n*** Benchmarking CCORR-32 convolution with %d-by-%d image ***\n\n", ws[j], hs[j]);
				print_bench_grid<int>("OpenCV [ms]:", "Height\\width", pws, phs, cv);
				print_bench_grid<int>("\nCuda 'same' [ms (speedup)]:", "Height\\width", pws, phs, cudas[0], &cv);
				printf("\n");
			}
		}


		if ( runs["conv_super"] ) {
			// Benchmark normalized correlation coefficient.
			int temp[] = {16, 32, 48, 64, 80, 96, 112, 128};
			vector<int> pws(temp,temp+8), phs(temp,temp+8);

			// Loop over images sizes.
			for ( unsigned j=j_start; j<j_end; ++j ) {
				svlCudaPitch *A = cm->allocPitch(ws[j], hs[j]),
					*R = cm->allocPitch(ws[j], hs[j]);

				cm->pitchToGPU(A, datas[j]);
				vector<float> cv;
				vector<vector<float> > cudas;
				for ( int k=0; k<1; ++k )
					cudas.push_back(vector<float>());
				// Loop over patch sizes.
				for ( vector<int>::const_iterator pw=pws.begin(); pw!=pws.end(); ++pw ) {
					for ( vector<int>::const_iterator ph=phs.begin(); ph!=phs.end(); ++ph ) {
						//printf("size: %d,%d\n", *pw, *ph);
						IplImage *pati = cvCreateImage(cvSize(*pw,*ph), IPL_DEPTH_32F, 1);
						for ( int i=0; i<*pw * *ph; ++i )
							CV_IMAGE_ELEM(pati, float, i/ (*pw), i%*pw) = 2.0f*( float(rand())/float(RAND_MAX) ) + 1.0f;
						svlCudaPitch *P = cm->alloc(pati);
						IplImage *res = cvCreateImage(cvSize(ws[j] - *pw + 1, hs[j] - *ph + 1), IPL_DEPTH_32F, 1);

						// Run OpenCV trials.
						tic = clock();
						for ( int i=0; i<cv_tris; ++i )
							cvMatchTemplate(datasI[j], pati, res, CV_TM_CCORR);
						cv.push_back(GET_ELAPSED_MS(tic)/float(cv_tris));
						//printf("cv: %0.3f\n", cv.back()*1000.0f);

						// Same convolution.
						for ( int i=0; i<cuda_tris; ++i )
							cc->cudaPitchConv(A, P, R);
						cudas[0].push_back(svlCudaTimer::getLastMedianClear());
						//printf("cuda: %0.3f\n", cudas[0].back()*1000.0f);

						cvReleaseImage(&pati);
						cvReleaseImage(&res);
						cm->Free(P);
					}
				}
				cm->Free(A);
				cm->Free(R);

				// Print times.
				printf("\n\n*** Benchmarking super convolution with %d-by-%d image ***\n\n", ws[j], hs[j]);
				print_bench_grid<int>("OpenCV [ms]:", "Height\\width", pws, phs, cv);
				print_bench_grid<int>("\nCuda 'same' [ms (speedup)]:", "Height\\width", pws, phs, cudas[0], &cv);
				printf("\n");

				/*FILE *fo = fopen("btimes.out","wb");
				fwrite(&cv[0],sizeof(float),cv.size(),fo);
				fwrite(&cudas[0][0],sizeof(float),cudas[0].size(),fo);
				fclose(fo);*/
			}
		}
	}


	if ( runs["SWD"] ) {
		// *** Benchmark sliding window detector ***
		int j_start = 0;
		int cuda_tris = 1, cv_tris = 1;

		// Loop over image sizes.
		const char *dict = "../tests/input/testDict45.xml"; // TODO paulb: Create random dictionary.
		const char *dtree = "../tests/input/cuda_tree.dtree";
		for ( unsigned j=j_start; j<j_end; ++j ) {
			printf("\n\n*** Benchmarking SWD with %d-by-%d image ***\n\n", ws[j], hs[j]);

			// Create image stack.
			vector<IplImage*> images;
			IplImage *img = cvCreateImage(cvSize(ws[j], hs[j]), IPL_DEPTH_32F, 1),
				*edge = cvCreateImage(cvSize(ws[j], hs[j]), IPL_DEPTH_32F, 1),
				*dep = cvCreateImage(cvSize(ws[j], hs[j]), IPL_DEPTH_32F, 1);
			for ( int r=0; r<hs[j]; ++r ) {
				for ( int c=0; c<ws[j]; ++c ) {
					CV_IMAGE_ELEM(img, float, r, c) = float(rand())/float(RAND_MAX);
					CV_IMAGE_ELEM(edge, float, r, c) = float(rand())/float(RAND_MAX);
					CV_IMAGE_ELEM(dep, float, r, c) = 2.0f*( float(rand())/float(RAND_MAX) ) + 1.0f;
				}
			}
			images.push_back(img);
			images.push_back(edge);
			images.push_back(dep);

			// Declare feature extractors.
			svlSlidingWindowDetector det("thing", ws[j], hs[j]);
			det.load(dict);
			svlCudaSWD cuda_det("thing", ws[j], hs[j]);
			cuda_det.load(dict);
			svlCudaSWD cuda_det2("thing", ws[j], hs[j]);
			cuda_det2.load(dict, dtree);

			// Get features.
			svlFeatureVectors F, CDF;
			tic = clock();
			det.classifyImage(images, NULL, &F);
			printf("SWD (dict %d) [ms]:   %0.3f ms\n", 45, gf = GET_ELAPSED_MS(tic)/float(cv_tris));
			svlFeatureVectorsF CF, FF;
			tic = clock();
			cuda_det.classifyImageF(images, NULL, &CF);
			printf("Cuda [ms (x)]: %0.3f ms (%0.1fx)\n", gf2 = GET_ELAPSED_MS(tic)/float(cuda_tris), gf/gf2 );
			tic = clock();
			svlObject2dFrame objects;
			cuda_det2.classifyImageF(images, &objects);
			printf("Cuda objects [ms (x)]: %0.3f ms (%0.1fx)\n", gf2 = GET_ELAPSED_MS(tic)/float(cuda_tris), gf/gf2 );

			cvReleaseImage(&img);
			cvReleaseImage(&edge);
			cvReleaseImage(&dep);

			//// OpenCV:
			//tic = clock();
			//for ( int i=0; i<cv_tris; ++i )
			//	cvIntegral(datasI[j], integ);

			//// Cuda.
			//tic = clock();
			//for ( int i=0; i<cuda_tris; ++i )
			//	svlCudaMath::integral(A, INTEG, TEMP);
			//toc = clock();
			//gf2 = svlCudaTimer::getLastMedianClear();
			//if ( gf2 == 0.0f ) gf2 = GET_ELAPSED_MS2(tic,toc)/float(cuda_tris);
			//printf("Cuda integral [ms (x)]: %0.3f ms (%0.1fx)\n", gf2, gf/gf2 );

			//cvReleaseImage(&integ);
			//cvReleaseImage(&integ2);
			//cm->freePitch(A);
			//cm->freePitch(INTEG);
			//cm->freePitch(TEMP);
			//cm->freePitch(INTEG2);

			printf("\n");
		}
	}


	printf("\n");
	cm->printAllAllocations();

	return 0;
}

// Print a grid of benchmark values. If versus is provide, use that to present a speedup.
template<typename T>
void print_bench_grid(const char *title, const char *label, vector<T> &cols, vector<T> &rows, vector<float> &ts, vector<float> *versus)
{
	// Print start.
	cout << title << endl << label;
	// Print column labels.
	int nc = int(cols.size()), nr = int(rows.size());
	int col_wid = versus ? 18 : 9;
	int row_wid = string(label).size();
	for ( int i=0; i<nc; ++i ) 
		cout << setw(col_wid) << setfill(' ') << cols[i];
	cout << endl;

	// Print values.
	float gf, gf2;
	if ( !versus ) {
		vector<float>::const_iterator t = ts.begin();
		for ( int j=0; j<nr; ++j ) {
			cout << setw(row_wid) << setfill(' ') << rows[j];
			for ( int i=0; i<nc; ++i ) {
				gf = *t++; PP_DEF(gf);
			}
			cout << endl;
		}
	} else {
		vector<float>::const_iterator t = ts.begin();
		vector<float>::const_iterator v = versus->begin();
		vector<float> xs; // Save speedup values as we go.
		for ( int j=0; j<nr; ++j ) {
			cout << setw(row_wid) << setfill(' ') << rows[j];
			for ( int i=0; i<nc; ++i ) {
				gf = *t;
				xs.push_back(gf2 = *v++/ *t++);
				PP_DEF(gf);
				cout << " ("; PRINT_PADDED1(3,gf2); cout << "x)";
			}
			cout << endl;
		}

		cout << "Speedup min, mean, max: ";
		float x_min = xs[0], x_max = xs[0], x_mean = xs[0];
		for ( unsigned i=1; i<xs.size(); ++i ) {
			x_min = min( x_min, xs[i] );
			x_max = max( x_max, xs[i] );
			x_mean += xs[i];
		}
		x_mean /= float(xs.size());
		PRINT_PADDED1(2,x_min); cout << "x -- "; PRINT_PADDED1(2,x_mean); cout << "x -- "; PRINT_PADDED1(2,x_max); cout << "x\n";
	}
}


