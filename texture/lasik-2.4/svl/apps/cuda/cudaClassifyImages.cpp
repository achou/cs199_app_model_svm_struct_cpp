/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    classifyImages.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**              Olga Russakovsky <olga@cs.stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
** DESCRIPTION:
**  Application for classifying images.
**
*****************************************************************************/

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <string.h>

#include "cv.h"
#include "cxcore.h"
#include "highgui.h"

#include "svlBase.h"
#include "svlML.h"
#include "svlVision.h"
#include "svlDevel.h"
#include "svlCuda.h"

using namespace std;

#define WINDOW_NAME "cudaClassifyImages"
#define MULTI_PASS
#define PASSES 1
#define TRACK_ALLOC 0
//#undef USE_CUDA

// main -----------------------------------------------------------------------

void usage()
{
	//assert(false);
	cerr << SVL_USAGE_HEADER << endl;
	cerr << "USAGE: ./classifyImages [OPTIONS] [<extractor>] <model> (<image>* | <imageSequence>)" << endl;
	cerr << "OPTIONS:" << endl
		<< "  -c <type>             :: classifier type: GABOR, PATCH, GENERAL (default)" << endl
		<< "  -b                    :: only search at base scale" << endl
		<< "  -n <object name>      :: name of object" << endl
		<< "  -o <filename>         :: output to file" << endl
		<< "  -profile              :: turn on SVL code profiling" << endl
		<< "  -r <x> <y> <w> <h>    :: scan subregion of image" << endl
		<< "  -R <filename>         :: only scan rectangles in file (<x> <y> <w> <h>)" <<endl
		<< "  -s <w> <h>            :: resize image to <w>-by-<h> before scanning" << endl
		<< "  -t <threshold>        :: classifier threshold (default: 0.5)" << endl
		<< "  -delta_scale <d>      :: sliding window scaling parameter (default: 1.2, must be > 1)" << endl
		<< "  -delta_shift <d>      :: sliding window shifting paramter (default: 4, must be >= 1)" << endl
		<< "  -v                    :: verbose" << endl
		<< "  -x                    :: display image and matches" << endl
		<< "  -baseExt              :: sets the file extension that should be stripped off of the input filenames in order to change channels" << endl
		<< "  -dmsize <width> <height> :: dimensions of the depth map"<< endl
		<< "  -ch <type> <ext>      :: adds a channel" << endl
		<< "DEPRECATED OPTIONS: " << endl
		<< "  -ch n <string1>..<stringn> :: n input channels, along with types for each of them"<< endl
		<< "  -imageExt <string> :: extension of the image data"<< endl
		<< "  -depthMapExt <string> :: extension of the depth map data if any"<< endl

		<< endl;
}

enum ClassifierType
{
	CT_GABOR,
	CT_PATCH,
	CT_GENERAL
};



int main(int argc, char *argv[])
{		
	svlLogger::setLogLevel(SVL_LOG_MESSAGE);
	const unsigned MAX_RESULTS_TO_SHOW = 16;
	const unsigned MAX_WIDTH = 1024;

	// read commandline parameters
	const char *extractorFilename = NULL;
	const char *classifierFilename = NULL;
	const char *outputFilename = NULL;
	const char *objectName = "[unknown]";
	const char *classifierType = "general";
	const char *cudaDTree = NULL;
	const char *cudaDTree28 = NULL;
	const char *depthChanOver = NULL;
	const char *dictDecimate = NULL;
	double threshold = 0.5;
	bool bDisplayImage = false;
	int width = -1;
	int height = -1;
	bool bBaseScaleOnly = false;
	CvRect subRegion = cvRect(0, 0, 0, 0);
	char *regionsFile = NULL;

	double delta_scale = svlSlidingWindowDetector::DELTA_SCALE;
	int delta_shift_x = svlSlidingWindowDetector::DELTA_X;
	int delta_shift_y = svlSlidingWindowDetector::DELTA_Y;
	//int nChannels = 2;
	//int depthMapWidth = 640;
	//int depthMapHeight = 480;

	svlPatchUtil util(SVL_PATCH_CLI_BASE_EXT | SVL_PATCH_DEFAULT_EDGE);
	
	char **args = argv + 1;
	while (--argc) {
		if (util.parseCommandLineItem(argc, args)) {
			//Do nothing, util has handled this flag 
		}
		else if (!strcmp(*args, "-n")) {
			objectName = *(++args);              argc--;
		} else if (!strcmp(*args, "-b")) {
			bBaseScaleOnly = true;
		} else if (!strcmp(*args, "-c")) {	 
			classifierType = *(++args);          argc--;
		} else if (!strcmp(*args, "-o")) {
			outputFilename = *(++args);          argc--;
		} else if (!strcmp(*args, "-profile")) {
			svlCodeProfiler::enabled = true;
			svlCodeProfiler::tic(svlCodeProfiler::getHandle("main"));
		} else if (!strcmp(*args, "-r")) {
			subRegion.x = atoi(*(++args));       argc--;
			subRegion.y = atoi(*(++args));       argc--;
			subRegion.width = atoi(*(++args));   argc--;
			subRegion.height = atoi(*(++args));  argc--;
		} else if (!strcmp(*args, "-s")) {
			width = atoi(*(++args));             argc--;
			height = atoi(*(++args));            argc--;
		} else if (!strcmp(*args, "-t")) {
			threshold = atof(*(++args));         argc--;
		} else if (!strcmp(*args, "-R")) {
			regionsFile = *(++args);             argc--;
		} else if (!strcmp(*args, "-delta_scale")) {
			delta_scale = atof(*(++args));         argc--;
		} else if (!strcmp(*args, "-delta_shift")) {
			delta_shift_x = atoi(*(++args));         argc--;
			delta_shift_y = delta_shift_x;
		} else if (!strcmp(*args, "-v")) {
			svlLogger::setLogLevel(SVL_LOG_VERBOSE);
		} else if (!strcmp(*args, "-x")) {
			bDisplayImage = true;
		} else if (!strcmp(*args, "-dtree")) {
			cudaDTree = *(++args); argc--;
		} else if (!strcmp(*args, "-dtree28")) {
			cudaDTree28 = *(++args); argc--;
		} else if (!strcmp(*args, "-depthChanOver")) {
			depthChanOver = *(++args); argc--;
		} else if (!strcmp(*args, "-dictDecimate")) {
			dictDecimate = *(++args); argc--;
		} else if ((*args)[0] == '-') {
			SVL_LOG(SVL_LOG_FATAL, "unrecognized option " << *args);
		} else {
			break;
		}
		args++;	
	}

	ClassifierType cType = CT_GENERAL;
	if (!strcasecmp(classifierType,"gabor"))
		cType = CT_GABOR;
	else if (!strcasecmp(classifierType,"patch"))
		cType = CT_PATCH;
	else if (!strcasecmp(classifierType, "general"))
		cType = CT_GENERAL;
	else
		SVL_LOG(SVL_LOG_FATAL, "Unrecognized classifier type");

	bool needsExtractor = (cType == CT_PATCH || cType == CT_GENERAL);

	if (argc < 2 + needsExtractor) {
		usage();
		return -1;
	}

	if (needsExtractor) {
		extractorFilename = *args++; argc--;
	}
	classifierFilename = *args++; argc--;
	util.finishCommandLine();

	// *** End processing input arguments ***

	util.setResizeSize(width, height);

	int classifyHandle = svlCodeProfiler::getHandle("classification");
	// Process image names.
	vector<string> imageNames;
	for ( int index = 0; index < argc; ++index ) {
		string imageFilename = string(args[index]);
		if (strExtension(imageFilename).compare("xml") == 0) {	
			svlImageSequence sequence;
			sequence.load(imageFilename.c_str());
			for ( unsigned i = 0; i < sequence.size(); ++i ) {
				imageNames.push_back(sequence.directory() + string("/") + sequence[i]);
			}
		} else {
			imageNames.push_back(imageFilename);
		}
	}

	if (regionsFile) {
		SVL_LOG(SVL_LOG_FATAL, "region scanning not implemented yet");
	}

	// Track device allocations to verify that there are no memory leaks.
	svlCudaMemHandler::setTracking(TRACK_ALLOC);
	
	// Initialize cuda detector with patch dictionary and model.
	svlCudaSlidingWindowDetector cc(640, 480, 3, objectName); // Using 3 channels (image, edge, depth).
	printf("Reading dictionary: '%s' ...\n", extractorFilename);
	cc.readDictionary(extractorFilename);
	if ( dictDecimate ) {
		// Read in decimate indices.
		printf("Decimating dictionary with '%s' ...", dictDecimate);
		int org_sz = cc.getDict()->numFeatures();
		FILE *fix = fopen(dictDecimate,"rb");
		if ( !fix ) {
			cout << "\nNULL FP\n";
		} else {
			int n;
			fread(&n,sizeof(int),1,fix);
			int *ixes = (int*)malloc(sizeof(int)*n);
			fread(ixes,sizeof(int),n,fix);
			fclose(fix);
			vector<int> ix(ixes, ixes+n); // Create vector from indices.
			cc.getDict()->decimate(ix);
			free(ixes);
			printf(" from size %d to size %d.\n", org_sz, cc.getDict()->numFeatures());
		}
	}
	//cc.printDictionary(); // To see that the patches have been read in correctly.
	cc.readModel(classifierFilename);
	cc.setThreshold(threshold);
	cc.setDeltas(delta_shift_x, delta_shift_y, delta_scale);
	cc.padAndCachePatches();
	if ( cudaDTree ) {
		printf("Loading Cuda decision tree: '%s' ...\n", cudaDTree);
		cc.loadCudaDTree(cudaDTree);
	} else if ( cudaDTree28 ) {
		printf("Loading 28-node Cuda decision tree: '%s' ...\n", cudaDTree28);
		cc.loadCudaDTree15(cudaDTree28);
	}

	// Initialize plain object detector
	svlSlidingWindowDetector *classifier = NULL;
	if (cType == CT_PATCH) {
		classifier = new svlPatchBasedObjectDetector(objectName);
		SVL_LOG(SVL_LOG_VERBOSE, "Loading dictionary " << extractorFilename << "...");
		((svlPatchBasedObjectDetector *)classifier)->readDictionary(extractorFilename);
	}
	else if (cType == CT_GENERAL) {
		classifier = new svlGeneralObjectDetector(objectName);
		assert(classifier);
		printf("Extractor: %s\n", extractorFilename);

		dynamic_cast<svlGeneralObjectDetector *>(classifier)->readFeatures(extractorFilename);

		SVL_LOG(SVL_LOG_VERBOSE, "Loading feature extractor file " << extractorFilename << "...");
	}
	assert(classifier != NULL);
	SVL_LOG(SVL_LOG_VERBOSE, "Loading classifier " << classifierFilename << "...");
	classifier->readModel(classifierFilename);
	classifier->setThreshold(threshold);

	classifier->setDeltas(delta_shift_x, delta_shift_y, delta_scale);

	// run classifier on all supplied images
	vector<IplImage *> annotatedImages;
	annotatedImages.resize(((imageNames.size() < MAX_RESULTS_TO_SHOW) ?
		imageNames.size() : MAX_RESULTS_TO_SHOW), NULL);
	if (sqrt((double)annotatedImages.size()) * width > MAX_WIDTH) {
		annotatedImages.resize(1);
	}
	IplImage *resultsImage = NULL;

	if (bDisplayImage) {
		cvNamedWindow(WINDOW_NAME, 0);
	}

	ofstream ofs;
	if (outputFilename) {
		ofs.open(outputFilename);
		assert(!ofs.fail());
		ofs << "<Object2dSequence>" << endl;
	}

	SVL_LOG(SVL_LOG_MESSAGE, "Running classifier on " << imageNames.size() << " images/sequences...");

#ifdef MULTI_PASS
	svlTimer timer;
	timer.tic();
	cout << "Timer on ...\n";

	for ( int ix=0; ix<PASSES; ++ix ) {
#endif
	for (unsigned index = 0; index < imageNames.size(); index++) 
	{
		//TODO-- share code with buildPatchDictionary and buildPatchResponseCache
		//TODO-- new implementation does not prevent repeated loading of same file. restore that feature

		SVL_LOG(SVL_LOG_VERBOSE, "Processing " << imageNames[index] << "...");

		string baseName = imageNames[index];

		vector<IplImage *> images;

		bool success = util.getFrameFromBasename(baseName, images);

		// Depth hack: add image itself as depth channel if none supplied.
		if ( int(images.size()) == 2 && depthChanOver ) {
			printf("Loading depth channel: '%s' ...\n", depthChanOver);
			IplImage *dep = cvCreateImage( cvSize(640,480), IPL_DEPTH_32F, 1 );
			FILE *fo = fopen(depthChanOver,"rb");
			if ( !fo ) {
				cout << "NULL FP\n";
			} else {
				float *data = (float*)malloc(sizeof(float)*640*480);
				fread(data,sizeof(float),640*480,fo);
				fclose(fo);
				float *ptr = data;
				for ( int r=0; r<480; ++r )
					for ( int c=0; c<640; ++c )
						CV_IMAGE_ELEM(dep, float, r, c) = *ptr++;
				free(data);
				images.push_back(dep);
			}
		}
		
		if (!success)
			continue;

		// classify image
		svlObject2dFrame objects, objects2;

		svlCodeProfiler::tic(classifyHandle);
		if (bBaseScaleOnly)
		{
			vector<CvPoint> locations;
			if ((subRegion.width > 0) && (subRegion.height > 0)) {
				classifier->createWindowLocations(subRegion.width, subRegion.height, locations);
				for (vector<CvPoint>::iterator it = locations.begin(); it != locations.end(); ++it) {
					it->x += subRegion.x;
					it->y += subRegion.y;
				}
			} 
			else 
			{
				classifier->createWindowLocations(images[0]->width, images[0]->height, locations);
				}
			objects = classifier->classifyImage(images, locations);
		}
		else if ((subRegion.width > 0) && (subRegion.height > 0)) {
			objects = classifier->classifySubImage(images, subRegion);
		} 
		else {
#ifdef USE_CUDA
			objects = cc.classifyImageCuda(images);
			//objects = cc.classifyImageCuda_inline(images);
#else
			objects = classifier->classifyImage(images);
#endif
		}
		svlCodeProfiler::toc(classifyHandle);

		// output objects
		if (outputFilename) {
			objects.write(ofs,  strBaseName(imageNames[index]).c_str());
		}

		if (bDisplayImage) {
			IplImage *colourImage = cvCreateImage(cvSize(images[0]->width, images[0]->height),
				IPL_DEPTH_8U, 3);
			if (images[0]->nChannels == 1) {
				cvCvtColor(images[0], colourImage, CV_GRAY2RGB);
			} else if (images[0]->nChannels == 3) {
				cvCopy(images[0], colourImage);
			}
			if (resultsImage != NULL) {	
				cvReleaseImage(&resultsImage);
			}

			for (unsigned i = 0; i < objects.size(); i++) {
				cvRectangle(colourImage, cvPoint((int)objects[i].x, (int)objects[i].y),
					cvPoint((int)(objects[i].x + objects[i].w - 1), 
					(int)(objects[i].y + objects[i].h - 1)),
					CV_RGB(0, (unsigned char)(255 * (objects[i].pr - threshold) / (1.0 - threshold)), 0), 3, 8);
				cvRectangle(colourImage, cvPoint((int)objects[i].x, (int)objects[i].y),
					cvPoint((int)(objects[i].x + objects[i].w - 1), 
					(int)(objects[i].y + objects[i].h - 1)),
					CV_RGB(255, 255, 255), 1);
			}

			cvRectangle(colourImage, cvPoint(subRegion.x, subRegion.y),
				cvPoint(subRegion.x + subRegion.width - 1, subRegion.y + subRegion.height - 1), 
				CV_RGB(0, 0, 255), 1);

			if (annotatedImages[index % annotatedImages.size()] != NULL) {
				cvReleaseImage(&annotatedImages[index % annotatedImages.size()]);
			}
			annotatedImages[index % annotatedImages.size()] = colourImage;
			resultsImage = combineImages(annotatedImages);
			cvShowImage(WINDOW_NAME, resultsImage);
			int c = cvWaitKey(index != imageNames.size() - 1 ? 100 : 0);
			//int c = cvWaitKey(0);

			if (c == (int)'s') {
				stringstream filename;
				filename << "classifyImage" << index << ".jpg";
				cvSaveImage(filename.str().c_str(), resultsImage);
			}
		}

		// free image
		for (unsigned i = 0; i < images.size(); i++) {
			cvReleaseImage(&images[i]);
		}

	}//image names in image sequence loop

	if (outputFilename) {
		ofs << "</Object2dSequence>" << endl;
		ofs.close();
	}
#ifdef MULTI_PASS
	timer.toc();
	cout << "End to end: ";	timer.print();

	svlCodeProfiler::print(cout);

	if ( TRACK_ALLOC )
		svlCudaMemHandler::printAllAllocations();

	timer.tic();
	}
	cout << "Multi end to end: ";	timer.print_per(float(PASSES));
#endif

#ifdef MULTI_PASS
	exit(-1);
#endif

	// free classifier and results
	delete classifier;
	if (resultsImage != NULL) {
		cvReleaseImage(&resultsImage);
	}
	for (unsigned i = 0; i < annotatedImages.size(); i++) {
		if (annotatedImages[i] != NULL) {
			cvReleaseImage(&annotatedImages[i]);
		}
	}

	cvDestroyAllWindows();
	svlCodeProfiler::toc(svlCodeProfiler::getHandle("main"));
	svlCodeProfiler::print(cerr);

	return 0;
}

