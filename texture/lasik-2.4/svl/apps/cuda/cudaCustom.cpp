/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    cudaCustom.cpp
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
**
** DESCRIPTION:
**  Application with example of Cuda application, linking to Cuda library,
**  and compiling its own .cu files.
**
*****************************************************************************/

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <string.h>

#include "cv.h"
#include "cxcore.h"
#include "highgui.h"

#include "svlBase.h"
#include "svlML.h"
#include "svlVision.h"
#include "svlCuda.h"
#include "cudaCustom.h"

using namespace std;

int main(int argc, char *argv[])
{
	svlCudaMemHandler *cm;

	int w = 7, h = 5;
	svlCudaPitch *Pe = cm->alloc(w, h), *Pm = cm->alloc(w, h);
	float *data = new float[w*h];
	
	printf("Explicit function output:\n");
	customSetSequence_explicit(Pe, w, h);
	cm->pitchFromGPU(data, Pe);
	float *ptr = data;
	for ( int r=0; r<h; ++r ) {
		printf("%d", int(*ptr++));
		for ( int c=1; c<w; ++c )
			printf(" %d", int(*ptr++));
		printf("\n");
	}

	printf("\nMacro function output:\n");
	customSetSequence_macro(Pm, w, h);
	cm->pitchFromGPU(data, Pm);	
	ptr = data;
	for ( int r=0; r<h; ++r ) {
		printf("%d", int(*ptr++));
		for ( int c=1; c<w; ++c )
			printf(" %d", int(*ptr++));
		printf("\n");
	}

	delete[] data;

	return 0;
}



