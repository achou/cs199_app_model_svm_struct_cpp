/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    classifyImages.cpp
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
**
** DESCRIPTION:
**  Application for verifying Cuda functions.
**
*****************************************************************************/

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <string.h>

#include "cv.h"
#include "cxcore.h"
#include "highgui.h"

#include "svlBase.h"
#include "svlML.h"
#include "svlVision.h"
#include "svlCuda.h"

using namespace std;

#define PRINT_NAMED_STATUS(name,status) printf("%s -- ",name); if (status) { printf("PASSED\n"); } else { printf("*** FAILED ***\n"); }
#define PRINT_STATUS PRINT_NAMED_STATUS(test_names.back(),gb)
#define TRANFER_TOL 1e-8

void minMax_gold(const float *data, float *minf, float *maxf, int w, int h, int mw = 4, int mh = 4);
void mean_gold(const float *data, float *outf, int w, int h, int mw = 32, int mh = 32);
void integral_gold(const float *data, float *integ, float *integ2, int w, int h);
void integralLine_gold(const float *data, float *integ, int w, int dim, bool do_cumsum);
void integralBlock_gold(const float *data, float *integ, float *integ2, int w, int h, int dim);
void rowIntegralBlock_gold(const float *data, float *integ, float *integ2, int w, int h, int dim);
void sub2ind(CvSize sz, const vector<CvPoint> &pts, vector<int> &ret);
void sortix(const vector<int> &vec, vector<int> &ix);
void conv2d_same(const float *data, int dw, int dh, const float *pat, int pw, int ph, float *res);
void conv2d_full(const float *data, int dw, int dh, const float *pat, int pw, int ph, float *res);
void ccoeff_normed_gold(const float *data, int dw, int dh, const float *pat, int pw, int ph, float *res);
void sobel_gold(const float *data, int dw, int dh, float *res);
void compare_svlFVs(svlFeatureVectors &G, svlFeatureVectorsF &F, unsigned &total, double &max_d, double &mean_d);

void scaleLocsFeatsF2File(const char *fname, const svlFeatureVectorsF &F);
void scaleLocsFeats2File(const char *fname, const svlFeatureVectors &F);

int main(int argc, char *argv[])
{
	svlCudaMemHandler *cm;
	cm->setTracking(true);

	// Usage
	if ( argc <= 1 ) {
		cout << "USAGE: ./cudaVerify [options]\n"
				 << "OPTIONS:\n"
				 << "  -all         :: runs all tests\n"
				 << "  -tol         :: prints tolerances with all tests\n"
				 << "  -names       :: prints names of all tests\n"
				 << "  -off [names] :: turns specified tests off\n"
				 << "  -on [names]  :: turns specified tests on\n";
		return 0;
	}

	// By default, run no tests.
	map<string,bool> runs;
	runs[string("alloc")] = false;
	runs[string("math")] = false;
	runs[string("minmax")] = false;
	runs[string("integral")] = false;
	runs[string("lines")] = false;
	runs[string("conv")] = false;
	runs[string("conv32")] = false;
	runs[string("convsuper")] = false;
	runs[string("filter")] = false;
	runs[string("dtree")] = false;
	runs[string("SWD")] = false;
	bool PRINT_TOL = false; // Whether to print numerical tolerances with tests or not.

	// Parse command line arguments.
	for ( int ai=1; ai<argc; ++ai ) {
		if ( strcmp(argv[ai],"-all") == 0 ) {
			for ( map<string,bool>::iterator m=runs.begin(); m!=runs.end(); ++m )
				m->second = true;
		}
		else if ( strcmp(argv[ai],"-tol") == 0 ) {
			PRINT_TOL = true;
		}
		else if ( strcmp(argv[ai],"-names") == 0 ) {
			printf("Names of all tests:\n");
			for ( map<string,bool>::iterator m=runs.begin(); m!=runs.end(); ++m )
				printf("  %s\n", m->first.c_str());
			return 0;
		}
		else if ( strcmp(argv[ai],"-off") == 0 ) {
			// Turn off the following listed names.
			for ( ++ai; ai<argc; ++ai ) {
				if ( *argv[ai] != '-' ) {
					runs[string(argv[ai])] = false;
				} else {
					// Stepping on another option; backtrack.
					ai--;
					break;
				}
			}
		}
		else if ( strcmp(argv[ai],"-on") == 0 ) {
			// Turn on the following listed names.
			for ( ++ai; ai<argc; ++ai ) {
				if ( *argv[ai] != '-' ) {
					runs[string(argv[ai])] = true;
				} else {
					// Stepping on another option; backtrack.
					ai--;
					break;
				}
			}
		}
	}
	/*printf("\nRunning tests:\n");
	for ( map<string,bool>::const_iterator m=runs.begin(); m!=runs.end(); ++m )
	if ( m->second )
	printf(" %s\n", m->first);*/

	// Which sizes to use.
	const unsigned N = 4;
	unsigned ws[] = {201, 256, 353, 640};
	unsigned hs[] = {201, 256, 212, 480};
	unsigned zs[] = {5, 3, 2, 2};

	unsigned szs[N], sz3s[N];
	srand(time(NULL));

	// Random data.
	float *datas[N], *datas_2[N], *tests[N], *verfs[N],
		*data3s[N], *data3s_2[N], *test3s[N], *verf3s[N];
	for ( unsigned j=0; j<N; ++j ) {
		szs[j] = ws[j] * hs[j];
		sz3s[j] = ws[j] * hs[j] * zs[j];
		datas[j] = (float*)malloc(sizeof(float)*szs[j]);
		datas_2[j] = (float*)malloc(sizeof(float)*szs[j]);
		tests[j] = (float*)malloc(sizeof(float)*szs[j]);
		verfs[j] = (float*)malloc(sizeof(float)*szs[j]);
		data3s[j] = (float*)malloc(sizeof(float)*sz3s[j]);
		data3s_2[j] = (float*)malloc(sizeof(float)*sz3s[j]);
		test3s[j] = (float*)malloc(sizeof(float)*sz3s[j]);
		verf3s[j] = (float*)malloc(sizeof(float)*sz3s[j]);
		for ( unsigned i=0; i<szs[j]; ++i ) {
			datas[j][i] = float(rand()) / float(RAND_MAX);
			datas_2[j][i] = float(rand()) / float(RAND_MAX);
		}
		for ( unsigned i=0; i<sz3s[j]; ++i )
			data3s[j][i] = float(rand()) / float(RAND_MAX);
	}

	vector<const char*> test_names;
	vector<bool> passeds;
	bool gb;
	float gf;


	if ( runs["alloc"] ) {
		// *** Test memory allocation and transfer.
		// Test lines.
		{
			test_names.push_back("Line allocation and transfer");
			gb = true;
			for ( unsigned j=0; gb && j<N; ++j ) {
				svlCudaLine *ln = cm->allocLine(ws[j]);
				cm->lineToGPU(ln, datas[j]);
				cm->lineFromGPU(tests[j], ln);
				float max_d = 0.0, sum_d = 0.0;
				for ( unsigned i=0; i<ws[j]; ++i ) {
					sum_d += fabs( datas[j][i] - tests[j][i] );
					max_d = max( max_d, fabs( datas[j][i] - tests[j][i] ) );
				}
				if ( sum_d >= TRANFER_TOL || max_d >= TRANFER_TOL )
					gb = false;
				cm->freeLine(ln);
			}
			passeds.push_back(gb);
			PRINT_STATUS
		}

		// Test pitches.
		{
			test_names.push_back("Pitch allocation and transfer");
			gb = true;
			for ( unsigned j=0; gb && j<N; ++j ) {
				svlCudaPitch *p = cm->allocPitch(ws[j], hs[j]);
				cm->pitchToGPU(p, datas[j]);
				cm->pitchFromGPU(tests[j], p);
				float max_d = 0.0, sum_d = 0.0;
				for ( unsigned i=0; i<szs[j]; ++i ) {
					sum_d += fabs( datas[j][i] - tests[j][i] );
					max_d = max( max_d, fabs( datas[j][i] - tests[j][i] ) );
				}
				if ( sum_d >= TRANFER_TOL || max_d >= TRANFER_TOL )
					gb = false;
				cm->freePitch(p);
			}
			passeds.push_back(gb);
			PRINT_STATUS
		}

		// Test 3d pitches.
		{
			test_names.push_back("3D pitch allocation and transfer");
			gb = true;
			for ( unsigned j=0; gb && j<N; ++j ) {
				svlCudaPitch3d *p = cm->allocPitch3d(ws[j], hs[j], zs[j]);
				cm->pitch3dToGPU(p, data3s[j]);
				cm->pitch3dFromGPU(test3s[j], p);
				float max_d = 0.0, sum_d = 0.0;
				for ( unsigned i=0; i<sz3s[j]; ++i ) {
					sum_d += fabs( data3s[j][i] - test3s[j][i] );
					max_d = max( max_d, fabs( data3s[j][i] - test3s[j][i] ) );
				}
				if ( sum_d >= TRANFER_TOL || max_d >= TRANFER_TOL )
					gb = false;
				cm->freePitch3d(p);
			}
			passeds.push_back(gb);
			PRINT_STATUS
		}
	}


	if ( runs["math"] ) {
		// *** Test general math functions.
		// Test addition.
		{
			test_names.push_back("Pitch add");
			gb = true;
			int n_reps = 5;
			float gf;
			for ( unsigned j=0; gb && j<N; ++j ) {
				svlCudaPitch *A = cm->allocPitch(ws[j], hs[j]),
					*B = cm->allocPitch(ws[j], hs[j]),
					*C = cm->allocPitch(ws[j], hs[j]);

				// Test two-pitch destructive add.
				cm->pitchToGPU(B, datas_2[j]);
				for ( int k=0; gb && k<n_reps; ++k ) {
					cm->pitchToGPU(A, datas[j]);
					float fac = 2.0f * float(rand())/float(RAND_MAX) - 1.0f;
					// Performs <A += fac*B>.
					svlCudaMath::add(A, B, fac);

					// Compute  gold result.
					cm->pitchFromGPU(tests[j], A);
					float max_d = 0.0, mean_d = 0.0;
					for ( unsigned i=0; i<szs[j]; ++i ) {
						gf = datas[j][i] + fac*datas_2[j][i];
						mean_d += fabs( gf - tests[j][i] );
						max_d = max( max_d, fabs( gf - tests[j][i] ) );
					}
					mean_d /= float(A->size());
					//printf("mean: %0.9f; max: %0.9f\n", mean_d, max_d);
					if ( mean_d >= 1e-7 || max_d >= 1e-6 )
						gb = false;
				}

				// Test three-pitch add.
				cm->pitchToGPU(A, datas[j]);
				cm->pitchToGPU(B, datas_2[j]);
				for ( int k=0; gb && k<n_reps; ++k ) {
					float fac_a = 2.0f * float(rand())/float(RAND_MAX) - 1.0f,
						fac_b = 2.0f * float(rand())/float(RAND_MAX) - 1.0f;
					// Performs <C = fac_a*A + fac_b*B>.
					svlCudaMath::add(C, A, B, fac_a, fac_b);

					// Compute  gold result.
					cm->pitchFromGPU(tests[j], C);
					float max_d = 0.0, mean_d = 0.0;
					for ( unsigned i=0; i<szs[j]; ++i ) {
						gf = fac_a * datas[j][i] + fac_b * datas_2[j][i];
						mean_d += fabs( gf - tests[j][i] );
						max_d = max( max_d, fabs( gf - tests[j][i] ) );
					}
					mean_d /= float(A->size());
					//printf("mean: %0.9f; max: %0.9f\n", mean_d, max_d);
					if ( mean_d >= 1e-7 || max_d >= 1e-6 )
						gb = false;
				}

				cm->freePitch(A);
				cm->freePitch(B);
				cm->freePitch(C);
			}
			passeds.push_back(gb);
			PRINT_STATUS
		}

		// Test multiplication.
		{
			test_names.push_back("Pitch multiply");
			gb = true;
			float gf;
			int n_reps = 5;
			for ( unsigned j=0; gb && j<N; ++j ) {
				svlCudaPitch *A = cm->allocPitch(ws[j], hs[j]),
					*B = cm->allocPitch(ws[j], hs[j]),
					*C = cm->allocPitch(ws[j], hs[j]);

				// Test two-pitch destructive mult.
				cm->pitchToGPU(B, datas_2[j]);
				for ( int k=0; gb && k<n_reps; ++k ) {
					cm->pitchToGPU(A, datas[j]);
					float fac = 2.0f * float(rand())/float(RAND_MAX) - 1.0f;
					// Performs <A *= fac*B>.
					svlCudaMath::mult(A, B, fac);

					// Compute  gold result.
					cm->pitchFromGPU(tests[j], A);
					float max_d = 0.0, mean_d = 0.0;
					for ( unsigned i=0; i<szs[j]; ++i ) {
						gf = datas[j][i] * fac*datas_2[j][i];
						mean_d += fabs( gf - tests[j][i] );
						max_d = max( max_d, fabs( gf - tests[j][i] ) );
					}
					mean_d /= float(A->size());
					//printf("mean: %0.9f; max: %0.9f\n", mean_d, max_d);
					if ( mean_d >= 1e-7 || max_d >= 1e-6 )
						gb = false;
				}

				// Test three-pitch mult.
				cm->pitchToGPU(A, datas[j]);
				cm->pitchToGPU(B, datas_2[j]);
				for ( int k=0; gb && k<n_reps; ++k ) {
					float fac = 2.0f * float(rand())/float(RAND_MAX) - 1.0f;
					// Performs <C = fac*A*B>.
					svlCudaMath::mult(C, A, B, fac);

					// Compute  gold result.
					cm->pitchFromGPU(tests[j], C);
					float max_d = 0.0, mean_d = 0.0;
					for ( unsigned i=0; i<szs[j]; ++i ) {
						gf = datas[j][i] * fac * datas_2[j][i];
						mean_d += fabs( gf - tests[j][i] );
						max_d = max( max_d, fabs( gf - tests[j][i] ) );
					}
					mean_d /= float(A->size());
					//printf("mean: %0.9f; max: %0.9f\n", mean_d, max_d);
					if ( mean_d >= 1e-7 || max_d >= 1e-6 )
						gb = false;
				}

				cm->freePitch(A);
				cm->freePitch(B);
				cm->freePitch(C);
			}
			passeds.push_back(gb);
			PRINT_STATUS
		}

		// Test division.
		{
			test_names.push_back("Pitch divide");
			gb = true;
			float gf;
			int n_reps = 5;
			for ( unsigned j=0; gb && j<N; ++j ) {
				svlCudaPitch *A = cm->allocPitch(ws[j], hs[j]),
					*B = cm->allocPitch(ws[j], hs[j]),
					*C = cm->allocPitch(ws[j], hs[j]);

				float *temp = (float*)malloc(sizeof(float)*szs[j]);
				memcpy(temp, datas_2[j], sizeof(float)*szs[j]);
				// Add 1 to datas_2 for stability with division.
				for ( unsigned i=0; i<szs[j]; ++i )
					temp[i] += 1.0f;
				cm->pitchToGPU(B, temp);

				// Test two-pitch destructive divide.
				for ( int k=0; gb && k<n_reps; ++k ) {
					cm->pitchToGPU(A, datas[j]);
					float fac = 2.0f * float(rand())/float(RAND_MAX) - 1.0f;
					// Performs <A *= fac/B>.
					svlCudaMath::div(A, B, fac);

					// Compute  gold result.
					cm->pitchFromGPU(tests[j], A);
					float max_d = 0.0, mean_d = 0.0;
					for ( unsigned i=0; i<szs[j]; ++i ) {
						gf = datas[j][i] * fac / temp[i];
						mean_d += fabs( gf - tests[j][i] );
						max_d = max( max_d, fabs( gf - tests[j][i] ) );
					}
					mean_d /= float(A->size());
					//printf("mean: %0.9f; max: %0.9f\n", mean_d, max_d);
					if ( mean_d >= 1e-7 || max_d >= 1e-6 )
						gb = false;
				}

				// Test three-pitch divide.
				cm->pitchToGPU(A, datas[j]);
				for ( int k=0; gb && k<n_reps; ++k ) {
					float fac = 2.0f * float(rand())/float(RAND_MAX) - 1.0f;
					// Performs <C = fac*A/B>.
					svlCudaMath::div(C, A, B, fac);

					// Compute  gold result.
					cm->pitchFromGPU(tests[j], C);
					float max_d = 0.0, mean_d = 0.0;
					for ( unsigned i=0; i<szs[j]; ++i ) {
						gf = datas[j][i] * fac / temp[i];
						mean_d += fabs( gf - tests[j][i] );
						max_d = max( max_d, fabs( gf - tests[j][i] ) );
					}
					mean_d /= float(A->size());
					//printf("mean: %0.9f; max: %0.9f\n", mean_d, max_d);
					if ( mean_d >= 1e-7 || max_d >= 1e-6 )
						gb = false;
				}

				free(temp);
				cm->freePitch(A);
				cm->freePitch(B);
				cm->freePitch(C);
			}
			passeds.push_back(gb);
			PRINT_STATUS
		}

		// Test scale and offset.
		{
			test_names.push_back("Pitch scale and offset");
			gb = true;
			float gf;
			int n_reps = 10;
			for ( unsigned j=0; gb && j<N; ++j ) {
				svlCudaPitch *A = cm->allocPitch(ws[j], hs[j]);

				for ( int k=0; gb && k<n_reps; ++k ) {
					cm->pitchToGPU(A, datas[j]);
					float scale = 2.0f * float(rand())/float(RAND_MAX) - 1.0f,
						offset = 10.0f * float(rand())/float(RAND_MAX) - 1.0f;
					// Performs <A = scale*A + offset>.
					svlCudaMath::scaleOffset(A, scale, offset);

					// Compare with gold result.
					cm->pitchFromGPU(tests[j], A);
					float max_d = 0.0, mean_d = 0.0;
					for ( unsigned i=0; i<szs[j]; ++i ) {
						gf = scale * datas[j][i] + offset;
						mean_d += fabs( gf - tests[j][i] );
						max_d = max( max_d, fabs( gf - tests[j][i] ) );
					}
					mean_d /= float(A->size());
					//printf("mean: %0.9f; max: %0.9f\n", mean_d, max_d);
					if ( mean_d >= 3e-7 || max_d >= 3e-6 )
						gb = false;
				}

				cm->freePitch(A);
			}
			passeds.push_back(gb);
			PRINT_STATUS
		}

		// Test decimation.
		{
			test_names.push_back("Pitch decimation");
			gb = true;
			for ( unsigned j=0; gb && j<N; ++j ) {
				svlCudaPitch *A = cm->allocPitch(ws[j], hs[j]);
				svlCudaPitch *B = cm->allocPitch(ws[j], hs[j]);
				cm->pitchToGPU(A, datas[j]);

				for ( int dx=2; gb && dx<=8; dx*=2 ) {
					for ( int dy=2; gb && dy<=8; dy*=2 ) {
						// Performs decimation.
						svlCudaMath::decimate(A, B, dx, dy);
						cm->pitchFromGPU(tests[j], B);

						// Compute gold result.
						float max_d = 0.0, sum_d = 0.0;
						for ( unsigned r=0; r<hs[j]/dy; ++r ) {
							for ( unsigned c=0; c<ws[j]/dx; ++c ) {
								gf = datas[j][ r*dy*ws[j] + c*dx ] - tests[j][ r*ws[j] + c ];
								sum_d += fabs(gf);
								max_d = max( max_d, fabs(gf) );
							}
						}

						if ( sum_d >= TRANFER_TOL || max_d >= TRANFER_TOL )
							gb = false;
					}
				}

				cm->freePitch(A);
				cm->freePitch(B);
			}
			passeds.push_back(gb);
			PRINT_STATUS
		}

		// Test quadrature add.
		{
			test_names.push_back("Pitch quadrature add");
			gb = true;
			for ( unsigned j=0; gb && j<N; ++j ) {
				svlCudaPitch *A = cm->allocPitch(ws[j], hs[j]);
				svlCudaPitch *B = cm->allocPitch(ws[j], hs[j]);
				cm->pitchToGPU(A, datas[j]);
				cm->pitchToGPU(B, datas_2[j]);

				// Performs <A = sqrt(A.^2 + B.^2)>.
				svlCudaMath::addQuadrature(A, B);
				cm->pitchFromGPU(tests[j], A);

				float max_d = 0.0f;
				for ( unsigned i=0; i<szs[j]; ++i ) {
					gf = sqrt( datas[j][i]*datas[j][i] + datas_2[j][i]*datas_2[j][i] );
					max_d = max( max_d, fabs( gf - tests[j][i] ) );
				}
				if ( max_d >= 1e-6 )
					gb = false;

				cm->freePitch(A);
				cm->freePitch(B);
			}
			passeds.push_back(gb);
			PRINT_STATUS
		}

		// Test 32-by-32 mean.
		{
			test_names.push_back("32-by-32 mean");
			gb = true;
			float max_d;
			for ( unsigned j=0; gb && j<N; ++j ) {
				svlCudaPitch *A = cm->allocPitch(ws[j], hs[j]),
					*B = cm->allocPitch(ws[j], hs[j]),
					*T = cm->allocPitch(ws[j], hs[j]);
				cm->pitchToGPU(A, datas[j]);

				svlCudaMath::mean32x32(A, B, T);
				cm->pitchFromGPU(tests[j], B);

				mean_gold(datas[j], verfs[j], ws[j], hs[j], 32, 32);

				max_d = 0.0f;			
				for ( unsigned i=0; i<szs[j]; ++i )
					max_d = max( max_d, fabs( verfs[j][i] - tests[j][i] ) );
				if ( max_d >= 1e-5 )
					gb = false;

				cm->freePitch(A);
				cm->freePitch(B);
				cm->freePitch(T);
			}
			passeds.push_back(gb);
			PRINT_STATUS
		}
	}


	if ( runs["minmax"] ) {
		// *** Test min/max functions.
		// Test 4-by-4 max.
		{
			test_names.push_back("4-by-4 min/max");
			gb = true;
			for ( unsigned j=0; gb && j<N; ++j ) {
				svlCudaPitch *A = cm->allocPitch(ws[j], hs[j]);
				svlCudaPitch *MINP = cm->allocPitch(ws[j], hs[j]);
				svlCudaPitch *MAXP = cm->allocPitch(ws[j], hs[j]);
				cm->pitchToGPU(A, datas[j]);

				// Minimum
				svlCudaMath::minMax4x4(A, MINP, NULL);
				cm->pitchFromGPU(tests[j], MINP);
				float max_d = 0.0;
				minMax_gold(datas[j], verfs[j], NULL, ws[j], hs[j], 4, 4);
				for ( unsigned i=0; i<szs[j]; ++i )
					max_d = max( max_d, fabs( verfs[j][i] - tests[j][i] ) );

				// Maximum
				svlCudaMath::minMax4x4(A, NULL, MAXP);
				cm->pitchFromGPU(tests[j], MAXP);
				minMax_gold(datas[j], NULL, verfs[j], ws[j], hs[j], 4, 4);
				for ( unsigned i=0; i<szs[j]; ++i )
					max_d = max( max_d, fabs( verfs[j][i] - tests[j][i] ) );

				if ( max_d >= 1e-9 )
					gb = false;

				cm->freePitch(A);
				cm->freePitch(MINP);
				cm->freePitch(MAXP);
			}
			passeds.push_back(gb);
			PRINT_STATUS
		}

		// Test 32-by-32 max.
		{
			test_names.push_back("32-by-32 min/max");
			gb = true;
			for ( unsigned j=0; gb && j<N; ++j ) {
				svlCudaPitch *A = cm->allocPitch(ws[j], hs[j]),
					*MINP = cm->allocPitch(ws[j], hs[j]),
					*MAXP = cm->allocPitch(ws[j], hs[j]),
					*TEMP = cm->allocPitch(ws[j], hs[j]);
				cm->pitchToGPU(A, datas[j]);

				// Minimum
				svlCudaMath::minMax32x32(A, TEMP, MINP, NULL);
				cm->pitchFromGPU(tests[j], MINP);
				float max_d = 0.0;
				minMax_gold(datas[j], verfs[j], NULL, ws[j], hs[j], 32, 32);
				for ( unsigned i=0; i<szs[j]; ++i )
					max_d = max( max_d, fabs( verfs[j][i] - tests[j][i] ) );
				cm->pitchToGPU(MINP, verfs[j]);

				// Maximum
				svlCudaMath::minMax32x32(A, TEMP, NULL, MAXP);
				cm->pitchFromGPU(tests[j], MAXP);
				minMax_gold(datas[j], NULL, verfs[j], ws[j], hs[j], 32, 32);
				for ( unsigned i=0; i<szs[j]; ++i )
					max_d = max( max_d, fabs( verfs[j][i] - tests[j][i] ) );

				if ( max_d >= 1e-9 )
					gb = false;

				cm->freePitch(A);
				cm->freePitch(MINP);
				cm->freePitch(MAXP);
				cm->freePitch(TEMP);
			}
			passeds.push_back(gb);
			PRINT_STATUS
		}
	}


	if ( runs["integral"] ) {
		// *** Test integration functions.
		// Test line integration
		{
			test_names.push_back("Line integrals");
			gb = true;

			unsigned w = 8192;
			svlCudaLine *ln = cm->allocLine(w),
				*integ = cm->allocLine(w);
			cm->lineToGPU(ln, datas[0]);
			float max_d = 0.0f;
			for ( int i=0; i<2; ++i ) {
				for ( int dim=32; dim<=1024; dim*=2 ) {
					bool do_cumsum = i==0;
					svlCudaMath::integralLine(ln, integ, w, dim, do_cumsum);
					cm->lineFromGPU(tests[0], integ);

					// Compute gold.
					integralLine_gold(datas[0], verfs[0], w, dim, do_cumsum);
					for ( unsigned j=0; j<w; ++j )
						max_d = max( max_d, fabs( tests[0][j] - verfs[0][j] ) );

					if ( max_d >= 1e-3 )
						gb = false;
				}
			}
			cm->freeLine(ln);
			cm->freeLine(integ);
			passeds.push_back(gb);
			PRINT_STATUS
		}

		// Test pitch block integration
		{
			test_names.push_back("Pitch block integrals");
			gb = true;

			float max_d = 0.0f, tol;
			for ( unsigned j=0; gb && j<N; ++j ) {
				svlCudaPitch *A = cm->allocPitch(ws[j], hs[j]),
					*INTEG = cm->allocPitch(ws[j]+1, hs[j]+1),
					*INTEG2 = cm->allocPitch(ws[j]+1, hs[j]+1),
					*TEMP1 = cm->allocPitch(ws[j]+1, hs[j]+1),
					*TEMP2 = cm->allocPitch(ws[j]+1, hs[j]+1);
				cm->pitchToGPU(A, datas[j]);
				// Must zeralize integral pitches beforehand as first row and column are not set in functions.
				cm->pitchSet(INTEG, 0);
				cm->pitchSet(INTEG2, 0);

				int isz = (ws[j]+1)*(hs[j]+1);
				float *verf = (float*)malloc(sizeof(float)*isz),
					*integ = (float*)malloc(sizeof(float)*isz),
					*integ2 = (float*)malloc(sizeof(float)*isz);

				for ( int dim=32; gb && dim<=512; dim*=2 ) {
					tol = 1e-6 * float(dim)*float(dim);
					//printf("tol: %0.9f\n", tol);
					svlCudaMath::integralBlock(A, INTEG, TEMP1, INTEG2, TEMP2, ws[j], hs[j], dim);
					integralBlock_gold(datas[j], integ, integ2, ws[j], hs[j], dim);

					cm->pitchFromGPU(verf, INTEG);
					max_d = 0.0f;
					for ( int i=0; i<isz; ++i )
						max_d = max( max_d, fabs( verf[i] - integ[i] ) );
					//printf("max: %0.9f\n", max_d);
					if ( max_d > tol )
						gb = false;

					cm->pitchFromGPU(verf, INTEG2);
					max_d = 0.0f;
					for ( int i=0; i<isz; ++i )
						max_d = max( max_d, fabs( verf[i] - integ2[i] ) );
					//printf("max: %0.9f\n", max_d);
					if ( max_d > tol )
						gb = false;
				}

				free(integ);
				free(integ2);
				cm->freePitch(A);
				cm->freePitch(INTEG);
				cm->freePitch(INTEG2);
				cm->freePitch(TEMP1);
				cm->freePitch(TEMP2);
			}
			passeds.push_back(gb);
			PRINT_STATUS
		}

		// Test pitch block row integration
		{
			test_names.push_back("Integral images");
			gb = true;

			float max_d = 0.0f, tol;
			for ( unsigned j=0; gb && j<N; ++j ) {
				tol = 2e-6 * float(szs[j]);
				if ( PRINT_TOL ) printf("tol: %0.9f\n", tol);
				svlCudaPitch *A = cm->allocPitch(ws[j], hs[j]),
					*INTEG = cm->allocPitch(ws[j]+1, hs[j]+1),
					*TEMP = cm->allocPitch(ws[j]+1, hs[j]+1),
					*INTEG2 = cm->allocPitch(ws[j]+1, hs[j]+1);
				cm->pitchToGPU(A, datas[j]);

				int isz = (ws[j]+1)*(hs[j]+1);
				float *verf = (float*)malloc(sizeof(float)*isz),
					*integ = (float*)malloc(sizeof(float)*isz),
					*integ2 = (float*)malloc(sizeof(float)*isz);

				// Compute device answer and reference result.
				svlCudaMath::integral(A, INTEG, TEMP, INTEG2, NULL, ws[j], hs[j]);
				integral_gold(datas[j], integ, integ2, ws[j], hs[j]);

				cm->pitchFromGPU(verf, INTEG);
				max_d = 0.0f;
				for ( int i=0; i<isz; ++i )
					max_d = max( max_d, fabs( verf[i] - integ[i] ) );
				if ( PRINT_TOL ) printf("max: %0.9f\n", max_d);
				if ( max_d > tol )
					gb = false;

				cm->pitchFromGPU(verf, INTEG2);
				max_d = 0.0f;
				for ( int i=0; i<isz; ++i )
					max_d = max( max_d, fabs( verf[i] - integ2[i] ) );
				if ( PRINT_TOL ) printf("max: %0.9f\n", max_d);
				if ( max_d > tol )
					gb = false;

				free(integ);
				free(integ2);
				cm->freePitch(A);
				cm->freePitch(INTEG);
				cm->freePitch(TEMP);
				cm->freePitch(INTEG2);
			}
			passeds.push_back(gb);
			PRINT_STATUS
		}
	}


	if ( runs["lines"] ) {
		// Test transferring CvPoint arrays to lines.
		{
			test_names.push_back("CvPoint to line transfer");
			gb = true;
			for ( unsigned j=0; gb && j<2; ++j ) {
				svlCudaLine *ln = cm->allocLine(2*ws[j]);

				vector<CvPoint> pts(ws[j]);
				for ( unsigned i=0; i<ws[j]; ++i ) {
					pts[i].x = rand()%1000;
					pts[i].y = rand()%1000;
				}
				int *verf = (int*)malloc(sizeof(int)*2*ws[j]);
				for ( int k=0; k<2; ++k ) {
					bool yx = k==0;
					cm->CvPoint2IntLine(pts, ln, yx);
					cm->lineFromGPU((float*)verf, ln);
					int n_diff = 0;
					for ( unsigned i=0; i<ws[j]; ++i ) {
						if ( yx ? verf[2*i] != pts[i].y || verf[2*i+1] != pts[i].x
							: verf[2*i+1] != pts[i].y || verf[2*i] != pts[i].x ) {
								++n_diff;
						}
					}
					//printf("n_diff: %d\n", n_diff);
					if ( n_diff )
						gb = false;
				}
				free(verf);
				cm->freeLine(ln);
			}
			passeds.push_back(gb);
			PRINT_STATUS
		}

		// Test indexed pitch to line lookup.
		{
			test_names.push_back("Write to line from indexed pitch");
			gb = true;
			for ( unsigned j=0; gb && j<2; ++j ) {
				svlCudaPitch *A = cm->allocPitch(ws[j], hs[j]);
				svlCudaLine *ix = cm->allocLine(2*ws[j]),
					*res = cm->allocLine(2*4*ws[j]);
				cm->pitchToGPU(A, datas[j]);
				int *ixes = (int*)malloc(sizeof(int)*2*ws[j]);

				for ( int k=0; gb && k<2; ++k ) {
					bool yx = k==0;
					for ( int line_off=0; gb && line_off<15; ++line_off ) {
						for ( int line_stride=1; gb && line_stride<=2; ++line_stride ) {
							// Create random lookup indices.
							for ( unsigned i=0; i<ws[j]; ++i ) {
								int x = rand()%ws[j], y = rand()%hs[j];
								ixes[2*i] = yx ? y : x;
								ixes[2*i+1] = yx ? x : y;
								verfs[j][i] = datas[j][ y*ws[j] + x ];
							}
							cm->lineToGPU(ix, (float*)ixes);

							cm->pitchIx2line(A, ix, res, ws[j], yx, line_off, line_stride);
							cm->lineFromGPU((float*)tests[j], res);
							int n_diff = 0;
							for ( unsigned i=0; i<ws[j]; ++i )
								if ( tests[j][ line_stride*i + line_off ] != verfs[j][i] )
									++n_diff;

							//printf("n_diff: %d\n", n_diff);
							if ( n_diff )
								gb = false;
						}
					}
				}

				free(ixes);
				cm->freePitch(A);
				cm->freeLine(ix);
				cm->freeLine(res);
			}
			passeds.push_back(gb);
			PRINT_STATUS
		}

		// TODO: Write to indexed pitch from line.
		// TODO: Write to indexed pitch from line.
	}


	if ( runs["conv"] ) {
		// *** Test convolution functions.
		// Test plain two-dimensional convolution.
		{
			test_names.push_back("Valid, same, and full CCORR convolution");
			gb = true;

			svlCudaConvolution *cc;
			// Generate random patch.
			//const unsigned pw = 4 + (rand()%12), ph = 4 + (rand()%12);
			const int pw = 16, ph = 16;
			unsigned psz = pw*ph;
			float *pat = (float*)malloc(sizeof(float)*psz);
			IplImage *pati = cvCreateImage(cvSize(pw,ph), IPL_DEPTH_32F, 1);
			for ( unsigned i=0; i<psz; ++i ) {
				pat[i] = 2.0f*( float(rand())/float(RAND_MAX) ) - 1.0f;
				CV_IMAGE_ELEM(pati, float, i/pw, i%pw) = pat[i];
			}

			const float TOL = 2e-4;
			for ( unsigned j=0; gb && j<N; ++j ) {
				svlCudaPitch *A = cm->allocPitch(ws[j], hs[j]),
					*VALID = cm->allocPitch(ws[j]-pw+1, hs[j]-ph+1),
					*SAME = cm->allocPitch(ws[j], hs[j]),
					*FULL = cm->allocPitch(ws[j]+pw-1, hs[j]+ph-1);
				cm->pitchToGPU(A, datas[j]);

				float *full_verf = (float*)malloc(sizeof(float)*( ws[j]+pw-1 )*( hs[j]+ph-1 )),
					*full_test = (float*)malloc(sizeof(float)*( ws[j]+pw-1 )*( hs[j]+ph-1 ));

				// Compute results with float* arguments.
				cc->cudaMatchTemplate(A, pat, pw, ph, SAME, CUDA_TM_CCORR, CUDA_TM_SAME);
				cc->cudaMatchTemplate(A, pat, pw, ph, VALID, CUDA_TM_CCORR, CUDA_TM_VALID);
				conv2d_same(datas[j], ws[j], hs[j], pat, pw, ph, verfs[j]);
				cc->cudaMatchTemplate(A, pat, pw, ph, FULL, CUDA_TM_CCORR, CUDA_TM_FULL);
				conv2d_full(datas[j], ws[j], hs[j], pat, pw, ph, full_verf);

				// Valid
				cm->pitchFromGPU(tests[j], VALID);
				float max_d = 0.0f;
				for ( unsigned r=0; r<hs[j]-ph+1; ++r )
					for ( unsigned c=0; c<ws[j]-pw+1; ++c )
						max_d = max( max_d, fabs( tests[j][ r*(ws[j]-pw+1) + c ] - verfs[j][ r*ws[j] + c ] ) );
				if ( PRINT_TOL ) printf("max: %0.9f\n", max_d);
				if ( max_d >= TOL )
					gb = false;

				// Same
				cm->pitchFromGPU(tests[j], SAME);
				max_d = 0.0f;
				for ( unsigned i=0; i<ws[j]*hs[j]; ++i )
					max_d = max( max_d, fabs( tests[j][i] - verfs[j][i] ) );
				if ( PRINT_TOL ) printf("max: %0.9f\n", max_d);
				if ( max_d >= TOL )
					gb = false;

				// Full
				cm->pitchFromGPU(full_test, FULL);
				max_d = 0.0f;
				for ( unsigned i=0; i<(ws[j]+pw-1)*(hs[j]+ph-1); ++i )
					max_d = max( max_d, fabs( full_test[i] - full_verf[i] ) );
				if ( PRINT_TOL ) printf("max: %0.9f\n", max_d);
				if ( max_d >= TOL )
					gb = false;

				// Compute results with IplImage* arguments.
				cc->cudaMatchTemplate(A, pati, SAME, CUDA_TM_CCORR, CUDA_TM_SAME);
				cc->cudaMatchTemplate(A, pati, VALID, CUDA_TM_CCORR, CUDA_TM_VALID);
				cc->cudaMatchTemplate(A, pati, FULL, CUDA_TM_CCORR, CUDA_TM_FULL);

				// Valid
				cm->pitchFromGPU(tests[j], VALID);
				max_d = 0.0f;
				for ( unsigned r=0; r<hs[j]-ph+1; ++r )
					for ( unsigned c=0; c<ws[j]-pw+1; ++c )
						max_d = max( max_d, fabs( tests[j][ r*(ws[j]-pw+1) + c ] - verfs[j][ r*ws[j] + c ] ) );
				if ( PRINT_TOL ) printf("max: %0.9f\n", max_d);
				if ( max_d >= TOL )
					gb = false;

				// Same
				cm->pitchFromGPU(tests[j], SAME);
				max_d = 0.0f;
				for ( unsigned i=0; i<ws[j]*hs[j]; ++i )
					max_d = max( max_d, fabs( tests[j][i] - verfs[j][i] ) );
				if ( PRINT_TOL ) printf("max: %0.9f\n", max_d);
				if ( max_d >= TOL )
					gb = false;

				// Full
				cm->pitchFromGPU(full_test, FULL);
				max_d = 0.0f;
				for ( unsigned i=0; i<(ws[j]+pw-1)*(hs[j]+ph-1); ++i )
					max_d = max( max_d, fabs( full_test[i] - full_verf[i] ) );
				if ( PRINT_TOL ) printf("max: %0.9f\n", max_d);
				if ( max_d >= TOL )
					gb = false;

				free(full_verf);
				free(full_test);
				cm->freePitch(A);
				cm->freePitch(VALID);
				cm->freePitch(SAME);
				cm->freePitch(FULL);
			}
			passeds.push_back(gb);
			PRINT_STATUS
		}

		// Test normalized correlation coefficient calculation.
		{
			test_names.push_back("Valid and same CCOEFF_NORMED convolution");
			gb = true;

			svlCudaConvolution *cc;
			// Generate random patch.
			const unsigned pw = 4 + (rand()%12), ph = 4 + (rand()%12);
			//const int pw = 16, ph = 16;
			unsigned psz = pw*ph;
			float *pat = (float*)malloc(sizeof(float)*psz);
			IplImage *pati = cvCreateImage(cvSize(pw,ph), IPL_DEPTH_32F, 1);
			for ( unsigned i=0; i<psz; ++i ) {
				pat[i] = 2.0f*( float(rand())/float(RAND_MAX) ) - 1.0f;
				CV_IMAGE_ELEM(pati, float, i/pw, i%pw) = pat[i];
			}

			const float TOL = 8e-6;
			for ( unsigned j=0; gb && j<N; ++j ) {
				svlCudaPitch *A = cm->allocPitch(ws[j], hs[j]),
					*VALID = cm->allocPitch(ws[j]-pw+1, hs[j]-ph+1),
					*SAME = cm->allocPitch(ws[j], hs[j]);
				cm->pitchToGPU(A, datas[j]);

				// Compute results with float* argument.
				cc->cudaMatchTemplate(A, pat, pw, ph, SAME, CUDA_TM_CCOEFF_NORMED, CUDA_TM_SAME);
				cc->cudaMatchTemplate(A, pat, pw, ph, VALID, CUDA_TM_CCOEFF_NORMED, CUDA_TM_VALID);
				ccoeff_normed_gold(datas[j], ws[j], hs[j], pat, pw, ph, verfs[j]);

				// Valid
				float max_d = 0.0f;
				cm->pitchFromGPU(tests[j], VALID);
				max_d = 0.0f;
				for ( unsigned r=0; r<hs[j]-ph+1; ++r )
					for ( unsigned c=0; c<ws[j]-pw+1; ++c )
						max_d = max( max_d, fabs( tests[j][ r*(ws[j]-pw+1) + c ] - verfs[j][ r*ws[j] + c ] ) );
				if ( PRINT_TOL ) printf("max: %0.9f\n", max_d);
				if ( max_d >= TOL )
					gb = false;

				// Same
				cm->pitchFromGPU(tests[j], SAME);
				max_d = 0.0f;
				for ( unsigned i=0; i<szs[j]; ++i )
					max_d = max( max_d, fabs( tests[j][i] - verfs[j][i] ) );
				if ( PRINT_TOL ) printf("max: %0.9f\n", max_d);
				if ( max_d >= TOL )
					gb = false;

				// Compute results with IplImage* argument.
				cc->cudaMatchTemplate(A, pati, SAME, CUDA_TM_CCOEFF_NORMED, CUDA_TM_SAME);
				cc->cudaMatchTemplate(A, pati, VALID, CUDA_TM_CCOEFF_NORMED, CUDA_TM_VALID);

				// Valid
				cm->pitchFromGPU(tests[j], VALID);
				max_d = 0.0f;
				for ( unsigned r=0; r<hs[j]-ph+1; ++r )
					for ( unsigned c=0; c<ws[j]-pw+1; ++c )
						max_d = max( max_d, fabs( tests[j][ r*(ws[j]-pw+1) + c ] - verfs[j][ r*ws[j] + c ] ) );
				if ( PRINT_TOL ) printf("max: %0.9f\n", max_d);
				if ( max_d >= TOL )
					gb = false;

				// Same
				cm->pitchFromGPU(tests[j], SAME);
				max_d = 0.0f;
				for ( unsigned i=0; i<szs[j]; ++i )
					max_d = max( max_d, fabs( tests[j][i] - verfs[j][i] ) );
				if ( PRINT_TOL ) printf("max: %0.9f\n", max_d);
				if ( max_d >= TOL )
					gb = false;

				cm->freePitch(A);
				cm->freePitch(SAME);
				cm->freePitch(VALID);
			}

			passeds.push_back(gb);
			PRINT_STATUS
		}
	}


	if ( runs["conv32"] ) {
		// *** Test convolution functions of patch size 32-by-32 ***
		// Test plain two-dimensional convolution.
		{
			test_names.push_back("24-, 32-size valid and same CCORR convolution");
			gb = true;

			for ( int pw=24; pw<=32; pw+=8 ) {
				for ( int ph=24; ph<=32; ph+=8 ) {
					svlCudaConvolution *cc;
					// Generate random patch.
					unsigned psz = pw*ph;
					float *pat = (float*)malloc(sizeof(float)*psz);
					IplImage *pati = cvCreateImage(cvSize(pw,ph), IPL_DEPTH_32F, 1);
					for ( unsigned i=0; i<psz; ++i )
						CV_IMAGE_ELEM(pati, float, i/pw, i%pw) = pat[i] = 2.0f*( float(rand())/float(RAND_MAX) ) - 1.0f;

					const float MAX_TOL = 5e-3, MEAN_TOL = 5e-4;
					for ( unsigned j=0; gb && j<N; ++j ) {
						svlCudaPitch *A = cm->allocPitch(ws[j], hs[j]),
							*VALID = cm->allocPitch(ws[j]-pw+1, hs[j]-ph+1),
							*T_VALID = cm->allocPitch(ws[j], hs[j]),
							*SAME = cm->allocPitch(ws[j], hs[j]),
							*T_SAME = cm->allocPitch(ws[j], hs[j]);
						cm->pitchToGPU(A, datas[j]);

						float *full_verf = (float*)malloc(sizeof(float)*( ws[j]+pw-1 )*( hs[j]+ph-1 )),
							*full_test = (float*)malloc(sizeof(float)*( ws[j]+pw-1 )*( hs[j]+ph-1 ));

						// Compute results with float* arguments.
						cc->cudaMatchTemplate(A, pat, pw, ph, SAME, CUDA_TM_CCORR, CUDA_TM_SAME, -1, -1, T_SAME);
						cc->cudaMatchTemplate(A, pat, pw, ph, VALID, CUDA_TM_CCORR, CUDA_TM_VALID, -1, -1, T_VALID);
						conv2d_same(datas[j], ws[j], hs[j], pat, pw, ph, verfs[j]);

						// Valid
						cm->pitchFromGPU(tests[j], VALID);
						float max_d = 0.0f, mean_d = 0.0f;
						for ( unsigned r=0; r<hs[j]-ph+1; ++r ) {
							for ( unsigned c=0; c<ws[j]-pw+1; ++c ) {
								gf = fabs( tests[j][ r*(ws[j]-pw+1) + c ] - verfs[j][ r*ws[j] + c ] );
								mean_d += gf;
								max_d = max( max_d, gf );
							}
						}
						mean_d /= float(szs[j]);
						if ( PRINT_TOL ) printf("max: %0.9f (tol: %0.9f); mean: %0.9f (tol: %0.9f)\n", max_d, MAX_TOL, mean_d, MEAN_TOL);
						if ( max_d >= MAX_TOL || mean_d >= MEAN_TOL )
							gb = false;

						// Same
						cm->pitchFromGPU(tests[j], SAME);
						max_d = 0.0f;
						mean_d = 0.0f;
						for ( unsigned i=0; i<szs[j]; ++i ) {
							gf = fabs( tests[j][i] - verfs[j][i] );
							mean_d += gf;
							max_d = max( max_d, gf );
						}
						mean_d /= float(szs[j]);
						if ( PRINT_TOL ) printf("max: %0.9f (tol: %0.9f); mean: %0.9f (tol: %0.9f)\n", max_d, MAX_TOL, mean_d, MEAN_TOL);
						if ( max_d >= MAX_TOL || mean_d >= MEAN_TOL )
							gb = false;

						free(full_verf);
						free(full_test);
						cm->freePitch(A);
						cm->freePitch(VALID);
						cm->freePitch(T_VALID);
						cm->freePitch(SAME);
						cm->freePitch(T_SAME);
					}
				}
			}
			passeds.push_back(gb);
			PRINT_STATUS
		}
	}


	if ( runs["convsuper"] ) {
		// *** Test super-sized convolution ***
		// Only have valid-sized interface for the moment.
		{
			test_names.push_back("Super-size valid CCORR convolution");
			gb = true;

			svlCudaConvolution *cc;
			const float MAX_TOL = 4e-4, MEAN_TOL = 5e-5;
			for ( unsigned j=0; gb && j<N; ++j ) {
				// Create IplImage versions of images.
				IplImage *A = cvCreateImage(cvSize(ws[j],hs[j]), IPL_DEPTH_32F, 1);
				for ( unsigned i=0; i<ws[j]*hs[j]; ++i )
					CV_IMAGE_ELEM(A, float, i/ws[j], i%ws[j]) = datas[j][i];
				svlCudaPitch *AA = cm->alloc(A);

				// Generate random patch.
				for ( int w=16; w<=128; w+=8 ) {
					int pw = w + ( (w%16) == 0 ? 0 : (rand()%15) - 7 );
					IplImage *P = cvCreateImage(cvSize( pw, 16 + rand()%112 ), IPL_DEPTH_32F, 1);
					//printf("patch: %d,%d\n", P->width, P->height);
					unsigned psz = pw*P->height;
					for ( unsigned i=0; i<psz; ++i )
						CV_IMAGE_ELEM(P, float, i/pw, i%pw) = 2.0f*( float(rand())/float(RAND_MAX) ) - 1.0f;
					svlCudaPitch *PP = cm->alloc(P), *PP_pad;

					IplImage *R = cvCreateImage(cvSize( A->width - P->width + 1, A->height - P->height + 1 ), IPL_DEPTH_32F, 1);
					svlCudaPitch *RR = cm->alloc(R->width, R->height);

					cvMatchTemplate(A, P, R, CV_TM_CCORR);

					svlCudaMemHandler::alignPitchWidth(PP, PP_pad, 16);
					cc->cudaPitchConv(AA, PP_pad, RR, PP);
					IplImage *RRR = cvCreateImage(cvSize(RR->w, RR->h), IPL_DEPTH_32F, 1);
					cm->IplFromGPU(RRR, RR);

					// Calculate difference.
					float mean_d = 0.0f, max_d = 0.0f;
					for ( int r=0; r<R->height; ++r ) {
						for ( int c=0; c<R->width; ++c ) {
							gf = fabs( CV_IMAGE_ELEM(R, float, r, c) - CV_IMAGE_ELEM(RRR, float, r, c) );
							mean_d += gf;
							max_d = max( max_d, gf );
						}
					}
					mean_d /= float(R->height*R->width);
					// Normalize mean and max to patch size.
					mean_d *= gf = float(64*64)/float(P->width*P->height);
					max_d *= gf;
					if ( PRINT_TOL ) printf("mean: %0.6f; max: %0.6f\n", mean_d, max_d);
					if ( mean_d >= MEAN_TOL || max_d >= MAX_TOL )
						gb = false;

					cvReleaseImage(&P);
					cvReleaseImage(&R);
					cvReleaseImage(&RRR);
					if ( PP ) cm->Free(PP);
					if ( PP_pad ) cm->Free(PP_pad);
					cm->Free(RR);
				}
				//exit(-1);
				cvReleaseImage(&A);
				cm->Free(AA);
			}
			passeds.push_back(gb);
			PRINT_STATUS
		}
	}


	if ( runs["filter"] ) {
		// Test Sobel filter.
		{
			test_names.push_back("Sobel filter");
			gb = true;

			for ( unsigned j=0; gb && j<N; ++j ) {
				svlCudaPitch *A = cm->allocPitch(ws[j], hs[j]),
					*E = cm->allocPitch(ws[j], hs[j]),
					*T1 = cm->allocPitch(ws[j], hs[j]),
					*T2 = cm->allocPitch(ws[j], hs[j]);
				cm->pitchToGPU(A, datas[j]);

				// Computer results and gold result.
				svlCudaFilter::sobel(A, E, T1, T2);
				sobel_gold(datas[j], ws[j], hs[j], verfs[j]);

				cm->pitchFromGPU(tests[j], E);
				float max_d = 0.0f;
				for ( unsigned i=0; i<ws[j]*hs[j]; ++i )
					max_d = max( max_d, fabs( tests[j][i] - verfs[j][i] ) );
				//printf("max: %0.9f\n", max_d);
				if ( max_d >= 1e-6 )
					gb = false;

				cm->freePitch(A);
				cm->freePitch(E);
				cm->freePitch(T1);
				cm->freePitch(T2);
			}

			passeds.push_back(gb);
			PRINT_STATUS
		}
	}


	if ( runs["dtree"] ) {
		// Test a random decision tree.
		{
			test_names.push_back("Random decision trees");
			gb = true;

			const unsigned feature_len = 1200, n = 100;
			// Create random features.
			float *f = (float*)malloc(sizeof(float)*feature_len*n);
			for ( unsigned i=0; i<feature_len*n; ++i )
				f[i] = 2.0f*( float(rand())/float(RAND_MAX) ) - 1.0f;
			float *ref = (float*)malloc(sizeof(float)*n),
				*test = (float*)malloc(sizeof(float)*n);

			// Declare decision tree and special feature line.
			svlCudaDecisionTreeSet dtree("../tests/input/cuda_tree.dtree");
			svlCudaFeatureLine feats(feature_len, dtree.getNtrees());
			feats.alloc(n); // Allocate space.
			cm->lineToGPU(feats._features, f);

			// Run device computation and reference computation.
			feats.evaluate(&dtree);
			dtree.hostEvaluate(f, NULL, ref, feature_len, feature_len, n);
			cm->lineFromGPU(test, feats._results);

			float max_d = 0.0f;
			for ( unsigned i=0; i<n; ++i )
				max_d = max( max_d, fabs( ref[i] - test[i] ) );
			if ( PRINT_TOL ) printf("max: %0.9f\n", max_d);
			if ( max_d > 5e-5 )
				gb = false;

			free(f);
			free(ref);
			free(test);

			passeds.push_back(gb);
			PRINT_STATUS
		}

		// Test the standard decision tree with for I/O / conversion purposes.
		{
			test_names.push_back("Input decision trees");
			gb = true;

			// Declare decision tree and special feature line.
			const char *treeSet = "../tests/input/randTree50.xml";
			svlCudaDecisionTreeSet dtree;
			dtree.svlClassifier::load(treeSet);

			const int feature_len = 45, n = 1200;
			svlCudaFeatureLine feats(feature_len, dtree.getNtrees());
			feats.alloc(n); // Allocate space.
			const int stride = feats._feature_stride; // Get feature stride so we can pack data correctly.
			// Create random features.
			float *f = new float[stride*n];
			memset(f, 0, stride*n*sizeof(float));
			MatrixXd X(n,feature_len);
			for ( int i=0; i<n; ++i ) {
				float *fptr = f + stride*i;
				for ( int j=0; j<feature_len; ++j )
					X(i,j) = fptr[j] = 2.0f*( float(rand())/float(RAND_MAX) ) - 1.0f;
			}
			float *ref = new float[n], *test = new float[n], *ref2 = new float[n];
			// Move data to feature line.
			cm->lineToGPU(feats._features, f);

			// Run device computation and reference computation.
			feats.evaluate(&dtree);
			dtree.hostEvaluate(f, NULL, ref, feature_len, stride, n);
			cm->lineFromGPU(test, feats._results);
			dtree.hostEvaluate(X, ref2);
			/*svlBinaryWriteLine("test.out", test, n);
			svlBinaryWriteLine("ref.out", ref, n);
			svlBinaryWriteLine("ref2.out", ref2, n);*/

			float max_d = 0.0f;
			// Just check max difference, since this should be terribly small.
			for ( int i=0; i<n; ++i ) {
				max_d = max( max_d, fabs( ref2[i] - test[i] ) );
				max_d = max( max_d, fabs( ref[i] - test[i] ) );
			}
			if ( PRINT_TOL ) printf("max: %0.9f\n", max_d);
			if ( max_d > 5e-5 )
				gb = false;

			delete[] f;
			delete[] ref;
			delete[] test;

			passeds.push_back(gb);
			PRINT_STATUS
		}
	}


	if ( runs["SWD"] ) {
		// Test sliding window feature extractor.
		{
			test_names.push_back("SWD feature extraction");
			gb = true;

			const char *dict = "../tests/input/testDict45.xml";
			for ( unsigned j=0; gb && j<N; ++j ) {
				vector<IplImage*> images;
				IplImage *img = cvCreateImage(cvSize(ws[j], hs[j]), IPL_DEPTH_32F, 1),
					*edge = cvCreateImage(cvSize(ws[j], hs[j]), IPL_DEPTH_32F, 1),
					*dep = cvCreateImage(cvSize(ws[j], hs[j]), IPL_DEPTH_32F, 1);
				for ( unsigned r=0; r<hs[j]; ++r ) {
					memcpy( &img->imageData[ img->widthStep*r ], &datas[j][ r*ws[j] ], sizeof(float)*ws[j] );
					memcpy( &edge->imageData[ img->widthStep*r ], &datas_2[j][ r*ws[j] ], sizeof(float)*ws[j] );
					for ( unsigned c=0; c<ws[j]; ++c )
						CV_IMAGE_ELEM(dep, float, r, c) = CV_IMAGE_ELEM(img, float, r, c) + CV_IMAGE_ELEM(edge, float, r, c);
				}
				images.push_back(img);
				images.push_back(edge);
				images.push_back(dep);

				// Declare feature extractors.
				/*svlCudaSlidingWindowDetector cuda_det(ws[j], hs[j]);
				cuda_det.readDictionary(dict, false);
				cuda_det.setDeltas(del_x, del_y, del_scale);
				cuda_det.padAndCachePatches();*/
				svlSlidingWindowDetector det("thing", ws[j], hs[j]);
				det.load(dict);

				svlCudaSWD cuda_det("thing", ws[j], hs[j]);
				cuda_det.load(dict);

				// Get features.
				svlFeatureVectorsF CF, FF;
				cuda_det.classifyImageF(images, NULL, &CF);

				svlFeatureVectors F, CDF;
				det.classifyImage(images, NULL, &F);

#if 0
				vector<vector<CvPoint> > &clocs = CF._locations;
				vector<vector<vector<float> > > &cfeats = CF._features;
				vector<vector<CvPoint> > &locs = F._locations;
				vector<vector<vector<double> > > &feats = F._features;
				printf("size: %d,%d > %d,%d\n", clocs.size(), cfeats.size(), clocs.size() > 0 ? clocs[0].size() : -1, cfeats.size() > 0 ? cfeats[0].size() : -1 );
				printf("size: %d,%d > %d,%d\n", locs.size(), feats.size(), locs.size() > 0 ? locs[0].size() : -1, feats.size() > 0 ? feats[0].size() : -1 );
				scaleLocsFeatsF2File("F_cuda.out", CF);
				scaleLocsFeats2File("F_gold.out", F);
#endif

				// If you want to test the conversion double->float, float->double versions.
				/*cuda_det.classifyImage(images, NULL, &CDF);
				det.classifyImageF(images, NULL, &FF);
				scaleLocsFeats2File("F_c2dub.out", CDF);
				scaleLocsFeatsF2File("F_g2flo.out", FF);*/

				// Compute location intersection to compare features.
				double max_d, mean_d;
				unsigned total_sz = 0;
				compare_svlFVs(F, CF, total_sz, max_d, mean_d);
				if ( PRINT_TOL ) printf("total: %d; max: %0.9f; mean: %0.9f\n", total_sz, max_d, mean_d);
				if ( total_sz < 1000 || max_d >= 2.5e-4 || mean_d >= 2.5e-6 )
					gb = false;

				cvReleaseImage(&img);
				cvReleaseImage(&edge);
				cvReleaseImage(&dep);
			}
			passeds.push_back(gb);
			PRINT_STATUS
		}
	}


#ifdef TEST_MULTIAA_SWD
	// Test multi-aspect, multi-angle sliding window feature extractor.
	{
		test_names.push_back("Multi-AA SWD feature extraction");
		gb = true;

		const char *dict = "../tests/input/testDict45.xml";
		// Just do the first, smallest image size at multiple aspects and scales.
		vector<IplImage*> images;
		int j = 2;
		int w = ws[j], h = hs[j];
		IplImage *img = cvCreateImage(cvSize(w, h), IPL_DEPTH_32F, 1),
			*edge = cvCreateImage(cvSize(w, h), IPL_DEPTH_32F, 1),
			*dep = cvCreateImage(cvSize(w, h), IPL_DEPTH_32F, 1);
		for ( unsigned r=0; r<h; ++r ) {
			memcpy( &img->imageData[ img->widthStep*r ], &datas[j][ r*w ], sizeof(float)*w );
			memcpy( &edge->imageData[ img->widthStep*r ], &datas_2[j][ r*w ], sizeof(float)*w );
			for ( unsigned c=0; c<w; ++c )
				CV_IMAGE_ELEM(dep, float, r, c) = CV_IMAGE_ELEM(img, float, r, c) + CV_IMAGE_ELEM(edge, float, r, c);
		}
		images.push_back(img);
		images.push_back(edge);
		images.push_back(dep);

		// Declare feature extractors.
		svlSlidingWindowDetector det("thing", w, h);
		det.load(dict);
		svlCudaSWD cuda_det("thing", w, h);
		cuda_det.load(dict);

		// Use these aspects and angles.
		const float asps[3] = {1.0f, 2.0f, 0.5f};
		vector<float> aspects = vector<float>(asps, asps+3);
		//vector<float> aspects = vector<float>(1, 1.0f);
		const float angs[3] = {0.0f, -30.0f, 45.0f};
		vector<float> angles = vector<float>(angs, angs+3);

		// Use multi-AA SWD to get features.
		svlMultiAspectAngleSWD maa;
		maa.setDetector(&cuda_det);
		vector<vector<svlFeatureVectorsF> > F;
		maa.classifyImageF(images, NULL, &F, &aspects, &angles);
		maa.setDetector(&det);
		vector<vector<svlFeatureVectors> > G;
		maa.classifyImage(images, NULL, &G, &aspects, &angles);

		double max_d = 0.0, mean_d = 0.0;
		unsigned total_sz = 0;
		for ( unsigned i=0; i<aspects.size(); ++i ) {
			for ( unsigned j=0; j<angles.size(); ++j ) {
				char c[32];
				sprintf(c,"G_%d_%d.out",i,j); scaleLocsFeats2File(c, G[i][j]);
				sprintf(c,"F_%d_%d.out",i,j); scaleLocsFeatsF2File(c, F[i][j]);

				double t_max_d, t_mean_d;
				unsigned t_total_sz;
				compare_svlFVs(G[i][j], F[i][j], t_total_sz, t_max_d, t_mean_d);
				if ( PRINT_TOL ) printf("temp total: %d; max: %0.9f; mean: %0.9f\n", t_total_sz, t_max_d, t_mean_d);
				total_sz += t_total_sz;
				max_d = max( max_d, t_max_d );
				mean_d += t_mean_d;
			}
		}
		mean_d /= double(aspects.size() * angles.size());
		if ( PRINT_TOL ) printf("FIN total: %d; max: %0.9f; mean: %0.9f\n", total_sz, max_d, mean_d);
		if ( total_sz < 1000 || max_d >= 2.5e-4 || mean_d >= 2.5e-6 )
			gb = false;

		cvReleaseImage(&img);
		cvReleaseImage(&edge);
		cvReleaseImage(&dep);

		passeds.push_back(gb);
		PRINT_STATUS
	}
#endif


	int n_passed = 0;
	for ( unsigned i=0; i<test_names.size(); ++i )
		if ( passeds[i] )
			++n_passed;
	printf("\nPassed %d; Failed %d%c\n", n_passed, int(test_names.size())-n_passed,
		n_passed!=int(test_names.size()) ? ':' : ' ');
	// Print failed tests again.
	if ( n_passed != int(test_names.size()) ) {
		for ( unsigned i=0; i<test_names.size(); ++i )
			if ( !passeds[i] )
				printf("  %s\n", test_names[i]);
	}

	if ( cm->activeAllocations() ) {
		printf("\n");
		cm->printAllAllocations();
	}
	return 0;
}

// Compute min/max of variable size.
void minMax_gold(const float *data, float *minf, float *maxf, int w, int h, int mw, int mh)
{
	float gf;
	for ( int r=0; r<h; ++r ) {
		for ( int c=0; c<w; ++c ) {
			gf = data[ r*w + c ];
			if ( minf ) {
				for ( int rr=0; rr<mh && r+rr < h; ++rr )
					for ( int cc=0; cc<mw && c+cc < w; ++cc )
						gf = min( gf, data[ ( r + rr )*w + c + cc ] );
				minf[ r*w + c ] = gf;
			}
			if ( maxf ) {
				for ( int rr=0; rr<mh && r+rr < h; ++rr )
					for ( int cc=0; cc<mw && c+cc < w; ++cc )
						gf = max( gf, data[ ( r + rr )*w + c + cc ] );
				maxf[ r*w + c ] = gf;
			}
		}
	}
}

// Compute mean of variable size.
void mean_gold(const float *data, float *outf, int w, int h, int mw, int mh)
{
	float gf, fac = 1.0f/float(mw*mh);
	for ( int r=0; r<h; ++r ) {
		for ( int c=0; c<w; ++c ) {
			gf = 0.0f;
			for ( int i=0; i<mh && r + i<h; ++i )
				for ( int j=0; j<mw && c + j<w; ++j )
					gf += data[ (r+i)*w + c + j ];
			outf[ r*w + c ] = fac * gf;
		}
	}
}


// Compute integral image.
void integral_gold(const float *data, float *integ, float *integ2, int w, int h)
{
	float gf, gf2;
	memset(integ, 0, (w+1)*(h+1)*sizeof(float));
	memset(integ2, 0, (w+1)*(h+1)*sizeof(float));
	for ( int r=0; r<h; ++r ) {
		gf = gf2 = 0.0f;
		for ( int c=0; c<w; ++c ) {
			gf += data[ r*w + c ];
			gf2 += data[ r*w + c ] * data[ r*w + c ];
			integ[ (r+1)*(w+1) + c + 1 ] = gf + integ[ r*(w+1) + c + 1 ];
			integ2[ (r+1)*(w+1) + c + 1 ] = gf2 + integ2[ r*(w+1) + c + 1 ];
		}
	}
}

// Compute integral line.
void integralLine_gold(const float *data, float *integ, int w, int dim, bool do_cumsum)
{
	float gf;
	for ( int i=0; i<w; i+=dim ) {
		gf = 0.0f;
		for ( int j=i; j<i+dim; ++j ) {
			integ[j] = gf + ( do_cumsum ? data[j] : 0.0f );
			gf += data[j];
		}
	}
}

// Compute integral block.
void integralBlock_gold(const float *data, float *integ, float *integ2, int w, int h, int dim)
{
	float gf, gf2;
	memset(integ, 0, (w+1)*(h+1)*sizeof(float));
	memset(integ2, 0, (w+1)*(h+1)*sizeof(float));
	for ( int rr=0; rr<(int)ceil((float)h/dim); ++rr ) {
		for ( int cc=0; cc<(int)ceil((float)w/dim); ++cc ) {
			for ( int r=rr*dim; r<(rr+1)*dim && r<h; ++r ) {
				gf = gf2 = 0.0f;
				for ( int c=cc*dim; c<(cc+1)*dim && c<w; ++c ) {
					gf += data[ r*w + c ];
					gf2 += data[ r*w + c ] * data[ r*w + c ];
					integ[ (r+1)*(w+1) + c + 1 ] = gf + ( r > rr*dim ? integ[ r*(w+1) + c + 1 ] : 0.0f );
					integ2[ (r+1)*(w+1) + c + 1 ] = gf2 + ( r > rr*dim ? integ2[ r*(w+1) + c + 1 ] : 0.0f );
				}
			}
		}
	}
}

// Compute row integral block.
void rowIntegralBlock_gold(const float *data, float *integ, float *integ2, int w, int h, int dim)
{
	float gf, gf2;
	memset(integ, 0, (w+1)*h*sizeof(float));
	memset(integ2, 0, (w+1)*h*sizeof(float));
	for ( int r=0; r<h; ++r ) {
		for ( int cc=0; cc<(int)ceil((float)w/dim); ++cc ) {
			gf = gf2 = 0.0f;
			for ( int c=cc*dim; c<(cc+1)*dim && c<w; ++c ) {
				integ[ r*(w+1) + c + 1 ] = gf += data[ r*w + c ];
				integ2[ r*(w+1) + c + 1 ] = gf2 += data[ r*w + c ] * data[ r*w + c ];
			}
		}
	}
}

void scaleLocsFeatsF2File(const char *fname, const svlFeatureVectorsF &F)
{
	FILE *fo = fopen(fname,"wb");
	const vector<vector<CvPoint> > &locs = F._locations;
	const vector<vector<vector<float> > > &feats = F._features;
	int gi, feature_stride = feats[0][0].size();
	fwrite(&feature_stride,sizeof(int),1,fo);
	for ( int i=0; i<int(locs.size()); ++i ) {
		fwrite(&(gi=locs[i].size()),sizeof(int),1,fo);
		// Write locations.
		for ( vector<CvPoint>::const_iterator c=locs[i].begin(); c!=locs[i].end(); ++c ) {
			fwrite(&c->x,sizeof(int),1,fo);
			fwrite(&c->y,sizeof(int),1,fo);
		}
		// Write features.
		for ( vector<vector<float> >::const_iterator fv=feats[i].begin(); fv!=feats[i].end(); ++fv ) {
			for ( vector<float>::const_iterator f=fv->begin(); f!=fv->end(); ++f ) {
				fwrite(&(*f),sizeof(float),1,fo);
			}
		}
	}
	fclose(fo);
}

void scaleLocsFeats2File(const char *fname, const svlFeatureVectors &F)
{
	FILE *fo = fopen(fname,"wb");
	const vector<vector<CvPoint> > &locs = F._locations;
	const vector<vector<vector<double> > > &feats = F._features;
	int gi, feature_stride = feats[0][0].size();
	fwrite(&feature_stride,sizeof(int),1,fo);
	for ( int i=0; i<int(locs.size()); ++i ) {
		fwrite(&(gi=locs[i].size()),sizeof(int),1,fo);
		// Write locations.
		for ( vector<CvPoint>::const_iterator c=locs[i].begin(); c!=locs[i].end(); ++c ) {
			fwrite(&c->x,sizeof(int),1,fo);
			fwrite(&c->y,sizeof(int),1,fo);
		}
		// Write features.
		for ( vector<vector<double> >::const_iterator fv=feats[i].begin(); fv!=feats[i].end(); ++fv ) {
			for ( vector<double>::const_iterator f=fv->begin(); f!=fv->end(); ++f ) {
				fwrite(&(*f),sizeof(double),1,fo);
			}
		}
	}
	fclose(fo);
}

// Subscript to indices conversion.
void sub2ind(CvSize sz, const vector<CvPoint> &pts, vector<int> &ret)
{
	ret.resize(pts.size());
	for ( unsigned i=0; i<pts.size(); ++i )
		ret[i] = pts[i].y*sz.width + pts[i].x;
}

// Return sort indices.
void sortix(const vector<int> &vec, vector<int> &ix)
{
	vector<pair<int,int> > tosort;
	tosort.reserve(vec.size());
	for ( unsigned i=0; i<vec.size(); ++i )
		tosort.push_back(pair<int,int>(vec[i],i));

	sort(tosort.begin(), tosort.end());
	ix.resize(vec.size());
	for ( unsigned i=0; i<tosort.size(); ++i )
		ix[i] = tosort[i].second;
}

// Returns a convolution result.
void conv2d_same(const float *data, int dw, int dh, const float *pat, int pw, int ph, float *res)
{
	float gf;
	for ( int r=0; r<dh; ++r ) {
		for ( int c=0; c<dw; ++c ) {
			gf = 0.0f;
			for ( int i=0; i<ph && r+i<dh; ++i )
				for ( int j=0; j<pw && c+j<dw; ++j )
					gf += data[ (r+i)*dw + c+j ] * pat[ i*pw + j ];
			res[ r*dw + c ] = gf;
		}
	}
}

// The above, with "full."
void conv2d_full(const float *data, int dw, int dh, const float *pat, int pw, int ph, float *res)
{
	float gf;
	for ( int r=0; r<dh+ph-1; ++r ) {
		for ( int c=0; c<dw+pw-1; ++c ) {
			gf = 0.0f;
			for ( int i=max(0, r-ph+1); i<min(dh, r+1); ++i )
				for ( int j=max(0, c-pw+1); j<min(dw, c+1); ++j )
					gf += data[ i*dw + j ] * pat[ ( i - (r-ph+1) )*pw + j - (c-pw+1) ];
			res[ r*(dw+pw-1) + c ] = gf;
		}
	}
}

// Normalized correlation coefficient.
void ccoeff_normed_gold(const float *data, int dw, int dh, const float *pat, int pw, int ph, float *res)
{
	// Compute mean and std of patch.
	int psz = pw*ph;
	float mean = 0.0f, std = 0.0f;
	for ( int i=0; i<psz; ++i ) {
		mean += pat[i];
		std += pat[i] * pat[i];
	}
	mean /= float(psz);
	std = sqrtf( std - float(psz)*mean*mean );

	float gf, sum, sum2;
	for ( int r=0; r<dh; ++r ) {
		for ( int c=0; c<dw; ++c ) {
			gf = sum = sum2 = 0.0f;
			for ( int i=0; i<ph && r+i<dh; ++i ) {
				for ( int j=0; j<pw && c+j<dw; ++j ) {
					const float d = data[ (r+i)*dw + c+j ];
					gf += d * pat[ i*pw + j ];
					sum += d;
					sum2 += d * d;
				}
			}
			res[ r*dw + c ] = ( gf - mean * sum ) / sqrtf( sum2 - sum*sum/float(psz) ) / std;
		}
	}
}

// Sobel filter.
void sobel_gold(const float *data, int dw, int dh, float *edge)
{
	int N = dw*dh;
	float *t1 = (float*)malloc(sizeof(float)*N),
		*t2 = (float*)malloc(sizeof(float)*N);

	// Do col [1 2 1] on data to t1, and col [1 0 -1]' on data to t2.
	for ( int c=0; c<dw; ++c ) {
		for ( int r=0; r<dh; ++r ) {
			int ix = r*dw + c;
			t1[ix] = ( r > 0 ? data[ix - dw] : 0.0f ) + 2.0f * data[ix] + ( r < dh-1 ? data[ix + dw] : 0.0f );
			t2[ix] = ( r > 0 ? data[ix - dw] : 0.0f ) - ( r < dh-1 ? data[ix + dw] : 0.0f );
		}
	}

	// Do row [1 0 -1] on t1 to edge
	for ( int r=0; r<dh; ++r ) {
		for ( int c=0; c<dw; ++c ) {
			int ix = r*dw + c;
			edge[ix] = ( c > 0 ? t1[ix - 1] : 0.0f ) - ( c < dw-1 ? t1[ix + 1] : 0.0f );
		}
	}

	// Do row [1 2 1] on t2 to t1.
	for ( int r=0; r<dh; ++r ) {
		for ( int c=0; c<dw; ++c ) {
			int ix = r*dw + c;
			t1[ix] = ( c > 0 ? t2[ix - 1] : 0.0f ) + 2.0f * t2[ix] + ( c < dw-1 ? t2[ix + 1] : 0.0f );
		}
	}

	// Add t1 to edge in quadrature.
	for ( int r=0; r<dh; ++r ) {
		for ( int c=0; c<dw; ++c ) {
			int ix = r*dw + c;
			edge[ix] = sqrt( edge[ix]*edge[ix] + t1[ix]*t1[ix] );
		}
	}

	free(t1);
	free(t2);
}

// Compare a set of feature vectors computed with SWDs and return total number of correspondences, and max and mean difference.
void compare_svlFVs(svlFeatureVectors &G, svlFeatureVectorsF &F, unsigned &total, double &max_d, double &mean_d)
{
	// Compute location intersection to compare features.
	max_d = 0.0;
	mean_d = 0.0;
	total = 0;
	double gd;
	vector<int> lpts, cpts, lix, cix;

	// Rename for easy access via reference variables.
	vector<vector<CvPoint> >& locs = G._locations;
	vector<vector<CvPoint> >& clocs = F._locations;
	vector<vector<vector<double> > >& feats = G._features;
	vector<vector<vector<float> > >& cfeats = F._features;

	for ( unsigned i=0; i<locs.size() && i<clocs.size(); ++i ) {
		sub2ind(cvSize(1000,1000), locs[i], lpts);
		sortix(lpts, lix);
		sub2ind(cvSize(1000,1000), clocs[i], cpts);
		sortix(cpts, cix);

		// Compute intersection.
		vector<int>::const_iterator lit = lix.begin(), cit = cix.begin();
		while ( lit != lix.end() && cit != cix.end() ) {
			if ( lpts[*lit] < cpts[*cit] ) {
				++lit;
			} else if ( cpts[*cit] < lpts[*lit] ) {
				++cit;
			} else {
				// Accumulate difference.
				++total;
				for ( unsigned k=0; k<feats[i][*lit].size(); ++k ) {
					gd = fabs( feats[i][*lit][k] - double(cfeats[i][*cit][k]) );
					mean_d += gd;
					max_d = max( max_d, gd );
				}
				++lit;
				++cit;
			}
		}
	}
	mean_d /= float(total*feats[0][0].size());
}



