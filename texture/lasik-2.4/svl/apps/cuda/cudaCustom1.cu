/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2009, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    cudaCustom.cu
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  .cu file for the cudaCustom project.
**
*****************************************************************************/

//#include "svlCudaCommon.h"
#include "cudaCustom.h"

__global__ void set_seq_slice(float *pin, int pitch, int w, int h)
{
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;
#if 1
	if ( x >= w || y >= h )
		return;
#endif

	// Set pitch value to product of x and y.
#if 0
	float *pf = PITCH_GET(pin, pitch, y);
	pf[x] = float(x) * float(y);
#else
	PITCH_GET_X(pin,pitch,y,x) = float(x) * float(y);
#endif
}

// Set the entries in a pitch in a distinctive way.
void customSetSequence_explicit(svlCudaPitch *pin, int img_w, int img_h)
{
	if ( img_w == -1 ) img_w = pin->w;
	if ( img_h == -1 ) img_h = pin->h;
	dim3 block(16, 16);
	dim3 grid( iDivUp( img_w, block.x ), iDivUp( img_h, block.y ) );
	set_seq_slice<<< grid, block >>>(pin->data, pin->pitch, img_w, img_h);
}

// Function using some macros.
void customSetSequence_macro(svlCudaPitch *pin, int img_w, int img_h)
{
	SVLCUDA_DEFAULT_SIZE( img_w, img_h, pin );
	dim3 block(16, 16);
	SVLCUDA_BLOCK2GRID( img_w, img_h, block, grid );
	set_seq_slice<<< grid, block >>>(pin->data, pin->pitch, img_w, img_h);
}



