/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    classifyImages.cpp
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
**
** DESCRIPTION:
**  Application for verifying Cuda functions.
**
*****************************************************************************/

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <string.h>
#include <bitset>

#include "cv.h"
#include "cxcore.h"
#include "highgui.h"

#include "svlBase.h"
#include "svlML.h"
#include "svlVision.h"
#include "svlCuda.h"

using namespace std;

int main(int argc, char *argv[])
{
	svlCudaMemHandler *cm;
	svlCudaConvolution *cc;

#if 1
	{
		IplImage *img = svlBinaryReadIpl("hat.dat");
		svlCudaPitch *IMG = cm->alloc(img), 
					 *MINP = cm->alloc(img->width, img->height),
					 *MAXP = cm->alloc(img->width, img->height),
					 *RESP = cm->alloc(img->width, img->height);
		//cm->pitch2file("img.out", IMG);

		// Get min and max of image.
		svlCudaMath::minMax32x32(IMG, RESP, MINP, MAXP);
		/*cm->pitch2file("min.out", MINP);
		cm->pitch2file("max.out", MAXP);
		cm->pitch2file("temp.out", RESP);*/

		// Perform <max -= min;>.
		svlCudaMath::add(MAXP, MINP, -1.0f);
		//cm->pitch2file("diff.out", MAXP);

		// Decimate by factors to retrieve only needed data; maxp to TEMP2.
		svlCudaMath::decimate(MAXP, RESP, 2, 2);
		//cm->pitch2file("resp2.out", RESP);

		exit(-1);
	}
#endif


#if 0
	{
		// Verify the reading-in of decision trees for Cuda DTs.

		svlDecisionTree dt;
		dt.svlClassifier::load("randTree.xml");
		printf("Read\n");
		dt.svlClassifier::save("randOut.xml");

		/*XMLNode root = XMLNode::parseFile("randTree.xml");
		if ( root.isEmpty() ) {
			printf("empty!\n");
			exit(-1);
		}
		char c[32];
		for ( int i=0; i<root.nChildNode(); ++i ) {
			sprintf(c,"randOut_%d.xml",i);
			svlDecisionTree dt;
			dt.load(root.getChildNode(i));
			dt.save(c);
		}*/

		const unsigned feature_len = 45, n = 1000;
		MatrixXd X( (int)n, (int)feature_len );
		// Create random features.
		float *f = (float*)malloc(sizeof(float)*feature_len*n);
		for ( unsigned i=0; i<feature_len*n; ++i ) {
			X( i/feature_len, i%feature_len ) = f[i] = 2.0f*( float(rand())/float(RAND_MAX) ) - 1.0f;
		}
		float *ref = (float*)malloc(sizeof(float)*n);

		// Declare decision tree and special feature line.
		svlCudaDtree cdt;
		cdt.load("randTree.xml");

		cdt.hostEvaluate(f, NULL, ref, feature_len, feature_len, n);

		FILE *fo = fopen("f.out","wb");
		fwrite(f,sizeof(float),feature_len*n,fo);
		fclose(fo);
		fo = fopen("ref.out","wb");
		fwrite(ref,sizeof(float),n,fo);
		fclose(fo);

		free(f);
		free(ref);


		// Have the cuda Dtree read that in.

	}
#endif

#if 0
	{
		// Verify bitmap.
		const int sz = 100;
		svlBitmap map(sz);
		printf("%d> ", map.count()); map.print();
		for ( int i=0; i<sz; ++i ) {
			map.set(i);
			printf("%d> ", map.count()); map.print();
		}
		for ( int i=0; i<sz; ++i ) {
			map.clear(i);
			printf("%d> ", map.count()); map.print();
		}
		for ( int i=0; i<sz; ++i ) {
			map.flip( rand()%sz );
			printf("%d> ", map.count()); map.print();
		}
		// Check that things are the same with get and [] as above.
		map.print();
		for ( int i=0; i<sz; ++i )
			printf("%d", map.get(i)?1:0);
		printf("\n");
		for ( int i=0; i<sz; ++i )
			printf("%d", map[i]?1:0);
		printf("\n");

		printf("\nMap:  "); map.print();
		svlBitmap map1(map);
		printf("Copy: "); map1.print();
		map1.flipAll();
		printf("Flip: "); map1.print();
		map1 = map;
		printf("Opr=: "); map1.print();

		svlBitmap map2;
		map2 = map;
		printf("Map2: "); map2.print();
	}
#endif


#if 0
	{
		// Advanced bitmap tests.
		const int sz = 100;
		svlBitmap m0(sz), m1(sz), mr(sz);
		m1.setAll();
		for ( int i=0; i<sz; ++i ) mr.flip(rand()%sz);
		/*printf("m0: "); m0.print();
		printf("m1: "); m1.print();
		printf("mr: "); mr.print();*/
		mr.print(cout, 32);
		
		svlBitmap msub(mr, 32);
		msub.print(cout, 32);
	}
#endif

#if 0
	{
		// Benchmark bitset and svlBitmap.
		vector<float> times1, times2;
		const int sz = 2048, n_access = 50000000;
		bitset<sz> bs;
		svlBitmap bm(sz);
		float gf;
		clock_t tic;
		vector<int> ix;
		ix.reserve(n_access);
		for ( int i=0; i<n_access; ++i )
			ix.push_back( rand()%sz );
		vector<float> times;

		printf("Set:\n");
		tic = clock();
		for ( vector<int>::const_iterator it=ix.begin(); it!=ix.end(); ++it )
			bs[*it] = 1;
		printf("  bitset: %0.3f\n", gf = float(clock()-tic)/float(CLOCKS_PER_SEC)*1000.0f);
		times.push_back(gf);

		tic = clock();
		for ( vector<int>::const_iterator it=ix.begin(); it!=ix.end(); ++it )
			bm.set(*it);
		printf("  bitmap: %0.3f\n", gf = float(clock()-tic)/float(CLOCKS_PER_SEC)*1000.0f);
		times.push_back(gf);

		printf("\nClear:\n");
		tic = clock();
		for ( vector<int>::const_iterator it=ix.begin(); it!=ix.end(); ++it )
			bs[*it] = 0;
		printf("  bitset: %0.3f\n", gf = float(clock()-tic)/float(CLOCKS_PER_SEC)*1000.0f);
		times.push_back(gf);

		tic = clock();
		for ( vector<int>::const_iterator it=ix.begin(); it!=ix.end(); ++it )
			bm.clear(*it);
		printf("  bitmap: %0.3f\n", gf = float(clock()-tic)/float(CLOCKS_PER_SEC)*1000.0f);
		times.push_back(gf);

		printf("\nFlip:\n");
		tic = clock();
		for ( vector<int>::const_iterator it=ix.begin(); it!=ix.end(); ++it )
			bs.flip(*it);
		printf("  bitset: %0.3f\n", gf = float(clock()-tic)/float(CLOCKS_PER_SEC)*1000.0f);
		times.push_back(gf);

		tic = clock();
		for ( vector<int>::const_iterator it=ix.begin(); it!=ix.end(); ++it )
			bm.flip(*it);
		printf("  bitmap: %0.3f\n", gf = float(clock()-tic)/float(CLOCKS_PER_SEC)*1000.0f);
		times.push_back(gf);

		printf("\nCount:\n");
		int n_count = 100000;
		tic = clock();
		vector<int> cc;
		cc.reserve(n_count);
		for ( int i=0; i<n_count; ++i )
			cc.push_back(bs.count());
		printf("  bitset: %0.3f\n", gf = float(clock()-tic)/float(CLOCKS_PER_SEC)*1000.0f);
		times.push_back(gf);

		tic = clock();
		vector<int> cc1;
		cc1.reserve(n_count);
		for ( int i=0; i<n_count; ++i )
			cc1.push_back(bm.count());
		printf("  bitmap: %0.3f\n", gf = float(clock()-tic)/float(CLOCKS_PER_SEC)*1000.0f);
		times.push_back(gf);
	}
#endif


#if 0
	// Verify stupidly large convolution
	IplImage *A = svlBinaryReadIpl("A.dat");
	IplImage *P = svlBinaryReadIpl("P.dat");
	IplImage *R = cvCreateImage(cvSize( A->width - P->width + 1, A->height - P->height + 1 ), IPL_DEPTH_32F, 1);
	svlCudaPitch *AA = cm->alloc(A),
				 *PP = cm->alloc(P),
				 *RR = cm->alloc(R->width, R->height);
	cvMatchTemplate(A, P, R, CV_TM_CCORR);

	cc->cudaPitchConv(AA, PP, RR);
	svlBinaryWriteIpl("R.out", R);
	cm->pitch2file("RR.out", RR);
#endif

#if 0
	{
		// Verify transpose.
		IplImage *A = svlBinaryReadIpl("A.dat");
		svlCudaPitch *AA = cm->alloc(A),
					 *AT = cm->alloc(A->height, A->width);
		svlCudaMath::transpose(AA, AT);
		cm->pitch2file("A.out", AA);
		cm->pitch2file("AT.out", AT);
	}
#endif

#if 0
	{
		// Verify image resizing.
		IplImage *A = svlBinaryReadIpl("A.dat");
		//IplImage *A = svlBinaryReadIpl("rand.dat");
		//float fac = 0.82123f;
		//float fac = 0.5f;
		//float fac = 0.333333f;
		//float fac = 0.25f;
		float fac = 0.1f;

		//int scaled_w = A->width, scaled_h = A->height; // Scale not at all.
		//int scaled_w = int(A->width*fac), scaled_h = A->height; // Scale width only.
		//int scaled_w = A->width, scaled_h = int(fac*A->height); // Scale height only.
		int scaled_w = int(A->width*fac), scaled_h = int(fac*A->height); // Scale both.

		printf("%d,%d -> %d,%d\n", A->width, A->height, scaled_w, scaled_h);
		IplImage *S = cvCreateImage( cvSize(scaled_w, scaled_h), A->depth, A->nChannels);
		cvResize(A, S, CV_INTER_LINEAR);
		svlBinaryWriteIpl("S.out", S);

		svlCudaPitch *AA = cm->alloc(A),
					 *SS = cm->alloc(A->width, A->height),
					 *temp1 = cm->alloc(A->height, A->width),
					 *temp2 = cm->alloc(A->height, A->width);

		svlCudaFilter::resize(AA, SS, scaled_w, scaled_h, temp1, temp2);
		cm->pitch2file("SS.out", SS);
		cm->pitch2file("temp1.out", temp1);
		cm->pitch2file("temp2.out", temp2);
	}
#endif

	return 0;
}


