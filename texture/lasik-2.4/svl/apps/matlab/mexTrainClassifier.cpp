/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    mexTrainClassifier.cpp
** AUTHOR(S):   Ian Quek <ianquek@stanford.edu>
**              Stephen Gould <sgould@stanford.edu>
** DESCRIPTION:
**  Application for evaluating a logistic or boosted classifier.
**
*****************************************************************************/

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <vector>

#include "cv.h"
#include "cxcore.h"
#include "ml.h"

// Matlab
#include "mex.h"
#include "matrix.h"

#include "svlBase.h"
#include "svlML.h"

#include "matlabUtils.h"

using namespace std;

void usage()
{
    //mexPrintf(SVL_USAGE_HEADER);
    mexPrintf("\n");
    mexPrintf("USAGE: model = mexTrainClassifier(data, labels, [weights, [options]]);\n");
    mexPrintf("OPTIONS:\n");
    mexPrintf("  options.type          :: classifier type: LOGISTIC (default), BOOSTING\n");
    mexPrintf("  options.maxIterations :: maximum training iterations for LOGISTIC (default = 1000)\n");
    mexPrintf("  options.nClasses      :: number of classes for LOGISTIC (default = auto)\n");
    mexPrintf("  options.lambda        :: regularization for LOGISTIC (default = 1e-6)\n");
    mexPrintf("  options.boostFilename :: output filename for BOOSTING\n");
    mexPrintf("  options.boostRounds   :: number of rounds for BOOSTING (default: 100)\n");
    mexPrintf("  options.boostSplits   :: number of splits for BOOSTING (default: 2)\n");
    mexPrintf("  options.verbose       :: verbose output (default: 0)\n");
    mexPrintf("\n");
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    // reset these since statics persist between Matlab calls
    svlLogger::setLogLevel(SVL_LOG_MESSAGE);

    if (nrhs == 0) {
        usage();
        return;
    }
    if ((nrhs != 2) && (nrhs != 3) && (nrhs != 4)) {
        usage();
        mexErrMsgTxt("incorrect number of input arguments");
    }
    if ((nrhs == 4) && (!mxIsStruct(prhs[3]))) { 
        mexErrMsgTxt("incorrect input argument type for OPTION");
    }
	
    // set svlLogger callbacks
    svlLogger::showFatalCallback = mexErrMsgTxt;
    svlLogger::showErrorCallback = mexErrMsgTxt;
    svlLogger::showWarningCallback = mexWarnMsgTxt;
    svlLogger::showMessageCallback = mexMsgTxt;

    // parse options
    map<string, string> options;
    options[string("type")] = string("LOGISTIC");
    options[string("maxIterations")] = string("1000");
    options[string("nClasses")] = string("-1");
    options[string("lambda")] = string("1.0e-6");
    options[string("boostFilename")] = string("");
    options[string("boostRounds")] = string("100");
    options[string("boostSplits")] = string("2");
    options[string("verbose")] = string("0");
    if (nrhs == 4) {
        parseOptions(prhs[3], options);
    }

    int maxIterations = atoi(options[string("maxIterations")].c_str());
    int nClasses = atoi(options[string("nClasses")].c_str());
    int nFeatures = -1;

    if (atoi(options[string("verbose")].c_str()) != 0) {
        svlLogger::setLogLevel(SVL_LOG_VERBOSE);
    }

    // logistic options
    double lambda = atof(options[string("lambda")].c_str());

    // boosting options
    int nBoostRounds = atoi(options[string("boostRounds")].c_str());
    int nBoostSplits = atoi(options[string("boostSplits")].c_str());
    string boostFilename = options[string("boostFilename")];

    // error checking: check that rows(data) = rows(labels) and cols(labels) = 1
    if (mxGetM(prhs[0]) != mxGetM(prhs[1])){
        usage();
        mexErrMsgTxt("data and/or labels matrices are of the wrong size.");
    }

    // read features data from input mxArray	
    int numElementsData = mxGetNumberOfElements(prhs[0]);
    int numElementsLabels = mxGetNumberOfElements(prhs[1]);

    vector<vector<double> > features;
    int nInstances = mxGetM(prhs[0]); // i.e., number of rows
    if(nFeatures < 0) {
        nFeatures = mxGetN(prhs[0]);
    }

    mxArrayToVector(prhs[0], features);

    // read labels data from input mxArray
    vector<int> labels(nInstances, -1);
    if (nClasses < 0) {
        for (int k=0; k<nInstances; k++) {
            labels[k] = (int)mxGetPr(prhs[1])[k];
            if (labels[k] >= nClasses) nClasses = labels[k] + 1;
        }
    } else {
        for (int k=0; k<nInstances; k++) {
            labels[k] = (int)mxGetPr(prhs[1])[k];        
        }
    }

    // load training weights (optional)
    vector<double> weights(features.size(), 1.0);
    if (nrhs >= 3) {
        if (mxGetNumberOfElements(prhs[2]) != 0) {
            if (mxGetNumberOfElements(prhs[2]) == features.size()) {
                for (int i = 0; i < features.size(); i++) {
                    weights[i] = mxGetPr(prhs[2])[i];
                }
            } else {
                mexErrMsgTxt("weights vector must be of the same size as the number of features");
            }
        } 
    }
	
    // train model
    SVL_LOG(SVL_LOG_VERBOSE, "Training " << nClasses << "-class model on " 
        << nInstances << " examples of length " << nFeatures << "...");
    if (!strcasecmp(options[string("type")].c_str(), "LOGISTIC")) {
        svlMultiClassLogistic classifier(nFeatures, nClasses, lambda);
        classifier.setOption("maxIterations", maxIterations);
        classifier.train(features, labels, weights);
        
        vector<vector<double> > parameters = classifier.getTheta();         
        int rows = classifier.numClasses() - 1;
        int cols = classifier.numFeatures();
        mxArray *output = mxCreateDoubleMatrix(rows, cols, mxREAL);
        double *outputPtr = mxGetPr(output);
        for (int n = 0; n < cols; n++) {
            for (int m = 0; m < rows; m++) {
                outputPtr[n*rows + m] = parameters[m][n]; 
            }
        }
        plhs[0] = output;
        
    } else if (!strcasecmp(options[string("type")].c_str(), "BOOSTING")) {
       
        // assemble design matrix for training
        CvMat *data = cvCreateMat((int)features.size(), nFeatures, CV_32FC1);
        CvMat *dataLabels = cvCreateMat((int)labels.size(), 1, CV_32SC1);
        SVL_ASSERT((data != NULL) && (dataLabels != NULL));

        for (unsigned i = 0; i < features.size(); i++) {
	    for (int j = 0; j < nFeatures; j++) {
	        CV_MAT_ELEM(*data, float, i, j) = features[i][j];
	    }
            CV_MAT_ELEM(*dataLabels, int, i, 0) = (labels[i] > 0 ? 1 : -1);
        }

        // train the classifier
        CvBoostParams parameters(CvBoost::GENTLE, nBoostRounds, 0.9,
            nBoostSplits, false, NULL);
        CvMat *varType = cvCreateMat(nFeatures + 1, 1, CV_8UC1);
        for (int i = 0; i < nFeatures; i++) {
            CV_MAT_ELEM(*varType, unsigned char, i, 0) = CV_VAR_NUMERICAL;
        }
        CV_MAT_ELEM(*varType, unsigned char, nFeatures, 0) = CV_VAR_CATEGORICAL;
    
        CvBoost *classifier = new CvBoost(data, CV_ROW_SAMPLE, dataLabels,
            NULL, NULL, varType, NULL, parameters);
        cvReleaseMat(&varType);
        cvReleaseMat(&dataLabels);
        cvReleaseMat(&data);

        // write model
        if (!boostFilename.empty()) {
            SVL_LOG(SVL_LOG_VERBOSE, "...writing parameters to " << boostFilename);
            classifier->save(boostFilename.c_str());
        }
        delete classifier;

        // return an empty matrix
        plhs[0] = mxCreateDoubleMatrix(0, 0, mxREAL);

    } else {
        SVL_LOG(SVL_LOG_FATAL, "unrecognized classifier type " << options[string("type")]);
    }

    SVL_LOG(SVL_LOG_VERBOSE, "...done");

    // print profile information
    svlCodeProfiler::print(cerr); 
}
