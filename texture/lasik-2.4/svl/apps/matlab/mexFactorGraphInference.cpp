/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    mexFactorGraphInference.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
** DESCRIPTION:
**  Matlab interface code for running inference on factor/cluster graphs.
**
*****************************************************************************/

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>

// Matlab
#include "mex.h"
#include "matrix.h"

// xml
#include "xmlParser/xmlParser.h"

// STAIR Vision Library
#include "svlBase.h"
#include "svlPGM.h"

#include "matlabUtils.h"

using namespace std;

void usage()
{
    mexPrintf(SVL_USAGE_HEADER);
    mexPrintf("\n");
    mexPrintf("USAGE: output = mexFactorGraphInference(factors, [options]);\n");
    mexPrintf("ARGUMENTS:\n");
    mexPrintf("  factors(:).vars       :: vector of (0-index) variables\n");
    mexPrintf("  factors(:).cards      :: vector of variable cardinalities\n");
    mexPrintf("  factors(:).data       :: vector of assignment values (size is prod(.cards))\n");
    mexPrintf("OPTIONS:\n");
    mexPrintf("  options.infAlgorithm  :: inference algorithm (SUMPROD (default), SUMPRODDIV,\n");
    mexPrintf("                           MAXPROD, MAXPRODDIV, LOGMAXPROD, LOGMAXPRODDIV,\n");
    mexPrintf("                           ASYNCSUMPROD, ASYNCSUMPRODDIV, ASYNCMAXPROD, ASYNCMAXPRODDIV,\n");
    mexPrintf("                           ASYNCLOGMAXPROD, ASYNCLOGMAXPRODDIV, RBP, GEMPLP, SONTAG08)\n");
    mexPrintf("  options.maxIterations :: maximum number of messages (default: 100)\n");
    mexPrintf("  options.noIndexCache  :: don't cache indices (slower, but used less memory)\n");
    mexPrintf("  options.singletonOnly :: only compute final marginals on singletons (default: true)\n");
    mexPrintf("  options.verbose       :: verbose output\n");
    mexPrintf("  options.debug         :: turn on debugging\n");
    mexPrintf("\n");
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    // reset these since statics persist between Matlab calls
    svlLogger::setLogLevel(SVL_LOG_MESSAGE);
    svlFactorOperation::CACHE_INDEX_MAPPING = true;

    if (nrhs == 0) {
        usage();
        return;
    }

    if ((nrhs != 1) && (nrhs != 2)) {
        usage();
        mexErrMsgTxt("incorrect number of input arguments");
    }

    if ((!mxIsStruct(prhs[0])) || ((nrhs == 2) && (!mxIsStruct(prhs[1])))) {
        mexErrMsgTxt("incorrect input argument types");
    }

    if (nlhs != 1) {
        usage();
        mexErrMsgTxt("incorrect number of output arguments");
    }

    // set svlLogger callbacks
    svlLogger::showFatalCallback = mexErrMsgTxt;
    svlLogger::showErrorCallback = mexErrMsgTxt;
    svlLogger::showWarningCallback = mexWarnMsgTxt;
    svlLogger::showMessageCallback = mexMsgTxt;

    // set default options and parse
    map<string, string> options;
    options[string("infAlgorithm")] = string("SUMPROD");
    options[string("maxIterations")] = string("100");
    options[string("noIndexCache")] = string("0");
    options[string("singletonOnly")] = string("1");
    options[string("verbose")] = string("0");
    options[string("debug")] = string("0");
    if (nrhs == 2) {
        parseOptions(prhs[1], options);
    }
    if (atoi(options[string("verbose")].c_str()) != 0)
        svlLogger::setLogLevel(SVL_LOG_VERBOSE);
    if (atoi(options[string("debug")].c_str()) != 0)
        svlLogger::setLogLevel(SVL_LOG_DEBUG);

    int maxIterations = atoi(options[string("maxIterations")].c_str());
    if (atoi(options[string("noIndexCache")].c_str()))
        svlFactorOperation::CACHE_INDEX_MAPPING = false;
    if (atoi(options[string("singletonOnly")].c_str()) == 0)
        svlMessagePassingInference::SINGLETON_MARGINALS_ONLY = false;

    svlMessagePassingAlgorithms infAlgorithm = SVL_MP_NONE;
    string infAlgoStr = options[string("infAlgorithm")];
    infAlgorithm = decodeMessagePassingAlgorithm(infAlgoStr.c_str());
    SVL_ASSERT_MSG(infAlgorithm != SVL_MP_NONE, "unknown inference algorithm");
    SVL_LOG(SVL_LOG_VERBOSE, "using " << infAlgoStr << " inference");

    // parse factors
    vector<svlFactor> factors;
    for (int i = 0; i < mxGetNumberOfElements(prhs[0]); i++) {
        factors.push_back(parseFactor(prhs[0], i));
        if (svlLogger::getLogLevel() >= SVL_LOG_DEBUG) {
            factors.back().write(cout);
        }
    }    

    // extract and check cardinalities
    vector<int> cards;
    for (int i = 0; i < (int)factors.size(); i++) {
        for (int j = 0; j < factors[i].numVars(); j++) {
            int v = factors[i].vars()[j];
            int c = factors[i].cards()[j];
            SVL_ASSERT((v >= 0) && (c >= 1));
            if (cards.size() <= (unsigned)v) {
                cards.resize(v + 1, -1);
            }
            SVL_ASSERT((cards[v] == -1) || (cards[v] == c));
            cards[v] = c;
        }
    }

    // construct cluster graph
    svlClusterGraph graph((int)cards.size(), cards);
    for (int i = 0; i < (int)factors.size(); i++) {
        vector<int> v = factors[i].vars();
        svlClique c;
        c.insert(v.begin(), v.end());
        graph.addClique(c);
        // don't add empty factors
        // (TODO: fix this in parseFactor code by returning separate cliques?)
        if ((c.size() == 1) || (factors[i].indexOfMin() != factors[i].indexOfMax())) {
            graph.setCliquePotential(graph.numCliques() - 1, factors[i]);
        }
    }
    graph.connectGraph();
    if (svlLogger::getLogLevel() >= SVL_LOG_DEBUG) {
        graph.write(cout);
    }


    // run inference
    svlMessagePassingInference infObject(graph);
    infObject.inference(infAlgorithm, maxIterations);

    // copy marginals to output
    const char *outputFields[2] = {"marginal", "value"};
    int numMarginals = graph.numCliques();
    plhs[0] = mxCreateStructArray(1, &numMarginals, 2, outputFields);
    
    for (int i = 0; i < numMarginals; i++) {
        svlFactor phi = infObject[i];
        if ((!svlMessagePassingInference::SINGLETON_MARGINALS_ONLY) || (phi.numVars() == 1)) {
            mxArray *outputMarginal = mxCreateDoubleMatrix(1, phi.size(), mxREAL);
            mxArray *outputValue = mxCreateDoubleMatrix(1, 1, mxREAL);
            double *outputMarginalPtr = mxGetPr(outputMarginal);
            int mapClass = 0;
            for (int j = 0; j < phi.size(); j++) {
                outputMarginalPtr[j] = phi[j];
                if (phi[j] > phi[mapClass])
                    mapClass = j;
            }        
            mxGetPr(outputValue)[0] = (double)mapClass;
            mxSetField(plhs[0], i, "marginal", outputMarginal);
            mxSetField(plhs[0], i, "value", outputValue);
        }
    }
}

