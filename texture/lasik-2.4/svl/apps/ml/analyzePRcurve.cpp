/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    analyzePRcurve.cpp
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
** DESCRIPTION:
**   Quick application for finding the area and the highest F-score of a 
**   PR curve
*****************************************************************************/

#include <cstdlib>
#include <string>
#include <limits>
#include <iomanip>
#include <fstream>
#include <map>
#include <vector>

#include "svlBase.h"
#include "svlML.h"

using namespace std;


void usage()
{
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./analyzePRcurve [OPTIONS] <filename>" << endl;
    cerr << "OPTIONS:" << endl
	 << "  -atbest               :: prints the Thr/Pr/Recall of point" << endl
	 << "                           with best F-score" << endl
	 << "  -at <p or r> <value>  :: prints the stats of the point" << endl
	 << "                           with that pr or recall value" << endl
	 << "  -x                    :: output all values with just \t in between" << endl
	 << SVL_STANDARD_OPTIONS_USAGE 
	 << endl;
}

int main(int argc, char **argv) {

  bool atbest = false;
  const char *ptr = NULL;

  vector<bool> by_recall;
  vector<float> values;

  bool simpleOutput = false;

  SVL_BEGIN_CMDLINE_PROCESSING(argc, argv)
    SVL_CMDLINE_BOOL_OPTION("-atbest", atbest)
    SVL_CMDLINE_OPTION_BEGIN("-at", ptr)
      switch(ptr[0][0]) {
      case 'r': by_recall.push_back(true); break;
      case 'p': by_recall.push_back(false); break;
      default: usage(); return -1;
      }
    values.push_back(atof(ptr[1]));
    SVL_CMDLINE_OPTION_END(2)
    SVL_CMDLINE_BOOL_OPTION("-x", simpleOutput)
  SVL_END_CMDLINE_PROCESSING(usage())
   
  if (SVL_CMDLINE_ARGC != 1) {
    usage();
    return -1;
  }

  char *filename = SVL_CMDLINE_ARGV[0];

  svlPRCurve d;
  if (!d.readCurve(filename)) {
      usage();
      return -1;
  }

  float result = d.bestF1Score();
  if (simpleOutput)
    cout << result << "\t" << d.averagePrecision();
  else
    cout << "Fscore: " << result
	 << ", Area: " << d.averagePrecision() << endl;

  if (atbest) {
    float thr = d.thresholdForBestF1Score();
    float pr = d.precisionAt(thr);
    float r = d.recallAt(thr);
    if (simpleOutput)
      cout << "\t" << thr << "\t" << pr << "\t" <<  r;
    else
      cout << "Best: threshold " << thr << ", precision " << pr << ", recall " << r << endl;
  }

  for (unsigned i = 0; i < values.size(); i++) {
    float thr = (by_recall[i]) ? d.thresholdForRecall(values[i]): d.thresholdForPrecision(values[i]);
    float pr = d.precisionAt(thr);
    float r = d.recallAt(thr);
    if (simpleOutput)
      cout << "\t" << pr << "\t" <<  r;
    else
      cout << "Threshold " << thr << ", precision " << pr << ", recall " << r << endl;    
  }

  if (simpleOutput)
    cout << endl;

  return 0;
}

