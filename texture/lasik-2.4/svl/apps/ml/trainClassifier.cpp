/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    trainClassifier.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
** DESCRIPTION:
**  Application for training a multi-class logistic or 1-vs-all boosted classifier.
**
*****************************************************************************/

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <vector>

#include "cv.h"
#include "cxcore.h"
#include "ml.h"

#include "svlBase.h"
#include "svlML.h"

using namespace std;

void usage()
{
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./trainClassifier [OPTIONS] <data> <labels>\n";
    cerr << "OPTIONS:\n"
         << "  -c <type>         :: classifier type: LOGISTIC (default), BOOSTING, SVM\n"
         << "  -k <classes>      :: number of classes (default: auto)\n"
         << "  -n <n>            :: number of features (default: auto)\n"
         << "  -m <n>            :: maximum iterations/boosting rounds (default: 1000)\n"
         << "  -o <filename>     :: output model filename (default: none)\n"
         << "  -w <filename>     :: weights for each training instance\n"
         << SVL_STANDARD_OPTIONS_USAGE
         << "OPTIONS (LOGISTIC):\n"
         << "  -r <n>            :: regularization (default: 1.0e-6)\n"
         << "OPTIONS (BOOSTING):\n"
         << "  -method <str>     :: boosting method: GENTLE (default), DISCRETE, LOGIT\n"
         << "  -splits <num>     :: number of splits in weak learners (default: 2)\n"
         << "  -pseudo <num>     :: class pseudo-counts (default: 1)\n"
         << endl;
}

int main(int argc, char *argv[])
{
    svlLogger::setLogLevel(SVL_LOG_MESSAGE);

    // common command line options
    const char *classifierType = "LOGISTIC";    
    int maxIterations = 1000;
    int nClasses = -1;
    int nFeatures = -1;
    const char *modelFilename = NULL;
    const char *weightsFilename = NULL;
    
    // logistic command line options
    double lambda = 1.0e-6;

    // boosting command line options
    const char *boostingMethod = "GENTLE";
    int weakLearnerSplits = 2;
    int pseudoCounts = 1;

    SVL_BEGIN_CMDLINE_PROCESSING(argc, argv)
        SVL_CMDLINE_STR_OPTION("-c", classifierType)
        SVL_CMDLINE_INT_OPTION("-k", nClasses)
        SVL_CMDLINE_INT_OPTION("-n", nFeatures)
        SVL_CMDLINE_INT_OPTION("-m", maxIterations)
        SVL_CMDLINE_STR_OPTION("-o", modelFilename)
        SVL_CMDLINE_REAL_OPTION("-r", lambda)
        SVL_CMDLINE_STR_OPTION("-w", weightsFilename)
        SVL_CMDLINE_STR_OPTION("-method", boostingMethod)
        SVL_CMDLINE_INT_OPTION("-splits", weakLearnerSplits)
        SVL_CMDLINE_INT_OPTION("-pseudo", pseudoCounts)
    SVL_END_CMDLINE_PROCESSING(usage());
    
    if (SVL_CMDLINE_ARGC != 2) {
        usage();
        return -1;
    }

    svlCodeProfiler::tic(svlCodeProfiler::getHandle("main"));

    const char *DATAFILE = SVL_CMDLINE_ARGV[0];
    const char *LABELFILE = SVL_CMDLINE_ARGV[1];

    // load features
    SVL_LOG(SVL_LOG_VERBOSE, "Reading features from " << DATAFILE << "...");
    vector<vector<double> > features;
    ifstream ifs(DATAFILE);
    SVL_ASSERT(!ifs.fail());
    // determine number of features
    if (nFeatures < 0) {
        nFeatures = svlCountFields(&ifs);
    }
    // read feature vectors
    while (1) {
        vector<double> v(nFeatures);
        for (int i = 0; i < nFeatures; i++) {
            ifs >> v[i];
        }
        if (ifs.fail()) break;
        features.push_back(v);
    }
    ifs.close();
    SVL_LOG(SVL_LOG_VERBOSE, "..." << features.size() 
        << " instances of size " << nFeatures << " read");

    // load training labels
    SVL_LOG(SVL_LOG_VERBOSE, "Reading training labels " << LABELFILE << "...");
    vector<int> labels(features.size(), -1);
    ifs.open(LABELFILE);
    SVL_ASSERT(!ifs.fail());
    for (int i = 0; i < (int)labels.size(); i++) {
        ifs >> labels[i];
    }
    ifs.close();
    SVL_LOG(SVL_LOG_VERBOSE, "...done");
    
    // determine number of classes
    if (nClasses < 0) {
        nClasses = *max_element(labels.begin(), labels.end()) + 1;
        SVL_ASSERT(nClasses > 1);
        SVL_LOG(SVL_LOG_VERBOSE, "...number of labels is " << nClasses);
    }

    // load training weights
    vector<double> weights(features.size(), 1.0);
    if (weightsFilename != NULL) {
        SVL_LOG(SVL_LOG_VERBOSE, "Reading training weights " << weightsFilename << "...");
        ifstream ifs(weightsFilename);
        SVL_ASSERT(!ifs.fail());
        for (int i = 0; i < (int)weights.size(); i++) {
            ifs >> weights[i];
        }
        ifs.close();
        SVL_LOG(SVL_LOG_VERBOSE, "...done");
    }

    // train model
    SVL_LOG(SVL_LOG_VERBOSE, "Training model...");
    if (!strcasecmp(classifierType, "LOGISTIC")) {
        svlMultiClassLogistic classifier(nFeatures, nClasses, lambda);

	classifier.setOption("eps", 1.0e-6);
	classifier.setOption("maxIterations", maxIterations);
        classifier.train(features, labels, weights);

        // write model
        if (modelFilename != NULL) {
            SVL_LOG(SVL_LOG_VERBOSE, "...writing parameters to " << modelFilename);
            classifier.save(modelFilename);
        }

    } else if (!strcasecmp(classifierType, "BOOSTING")) {
        svlBoostedClassifierSet classifier(nClasses == 2 ? 1 : nClasses);
        classifier.setOption("boostMethod", boostingMethod);
        classifier.setOption("numSplits", weakLearnerSplits);
        classifier.setOption("boostingRounds", maxIterations);
        classifier.setOption("pseudoCounts", pseudoCounts);
        classifier.train(features, labels);

        // write model
        if (modelFilename != NULL) {
            SVL_LOG(SVL_LOG_VERBOSE, "...writing parameters to " << modelFilename);
            classifier.save(modelFilename);
        }

    } else if (!strcasecmp(classifierType, "SVM")) {
        SVL_ASSERT(nClasses == 2);
        svlSVM classifier;
        classifier.train(features, labels);

        // write model
        if (modelFilename != NULL) {
            SVL_LOG(SVL_LOG_VERBOSE, "...writing parameters to " << modelFilename);
            classifier.save(modelFilename);
        }

    } else {
        SVL_LOG(SVL_LOG_FATAL, "unrecognized classifier type " << classifierType);
    }
    SVL_LOG(SVL_LOG_VERBOSE, "...done");

    // print profile information
    svlCodeProfiler::toc(svlCodeProfiler::getHandle("main"));
    svlCodeProfiler::print(cerr);
    return 0;
}


