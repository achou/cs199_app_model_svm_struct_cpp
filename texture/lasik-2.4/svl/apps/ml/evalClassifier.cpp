/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    evalClassifier.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
** DESCRIPTION:
**  Application for evaluating a logistic or boosted classifier.
**
*****************************************************************************/

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <vector>

#include "svlBase.h"
#include "svlML.h"

using namespace std;

void usage()
{
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./evalClassifier [OPTIONS] <parameters> <data>\n";
    cerr << "OPTIONS:\n"
	 << "  -c <type>         :: classifier type: LOGISTIC (default), BOOSTING, SVM\n"
         << "  -l <labels>       :: groundtruth label file for confusion matrix\n"
         << "  -n <n>            :: number of features (default: auto)\n"
	 << "  -o <filename>     :: output scores/marginal (default: STDOUT (with -verbose))" << endl
         << SVL_STANDARD_OPTIONS_USAGE
	 << endl;
}

int main(int argc, char *argv[])
{
    svlLogger::setLogLevel(SVL_LOG_MESSAGE);

    // common options
    const char *classifierType = "LOGISTIC";
    const char *outputFilename = NULL;
    const char *labelsFilename = NULL;
    int nClasses = -1;
    int nFeatures = -1;

    SVL_BEGIN_CMDLINE_PROCESSING(argc, argv)
        SVL_CMDLINE_STR_OPTION("-c", classifierType)
        SVL_CMDLINE_STR_OPTION("-l", labelsFilename)
        SVL_CMDLINE_INT_OPTION("-k", nClasses)
        SVL_CMDLINE_INT_OPTION("-n", nFeatures)
        SVL_CMDLINE_STR_OPTION("-o", outputFilename)
    SVL_END_CMDLINE_PROCESSING(usage());
    
    if (SVL_CMDLINE_ARGC != 2) {
        usage();
        return -1;
    }

    const char *PARAMFILE = SVL_CMDLINE_ARGV[0];
    const char *DATAFILE = SVL_CMDLINE_ARGV[1];

    // load features
    SVL_LOG(SVL_LOG_VERBOSE, "Reading features from " << DATAFILE << "...");
    vector<vector<double> > features;
    ifstream ifs(DATAFILE);
    SVL_ASSERT(!ifs.fail());
    // determine number of features
    if (nFeatures < 0) {
        nFeatures = svlCountFields(&ifs);
    }

    // read feature vector
    while (1) {
        vector<double> v(nFeatures);
        for (int i = 0; i < nFeatures; i++) {
            ifs >> v[i];
        }
        if (ifs.fail()) break;
        features.push_back(v);
    }
    ifs.close();
    SVL_LOG(SVL_LOG_VERBOSE, "..." << features.size() << " of size " 
        << nFeatures << " instances read");
    
    // evaluate model
    vector<int> predictedLabels(features.size(), -1);
    int nPredicted = 2;
    SVL_LOG(SVL_LOG_VERBOSE, "Evaluating model...");
    if (!strcasecmp(classifierType, "LOGISTIC")) {
        svlMultiClassLogistic classifier;
        if (!classifier.load(PARAMFILE)) {
            SVL_LOG(SVL_LOG_FATAL, "could not load parameters from " << PARAMFILE);
        }

        vector<vector<double> > marginals;
        classifier.getMarginals(features, marginals);
        predictedLabels = argmaxs(marginals);
        nPredicted = classifier.numClasses();

        // write output
        if (outputFilename != NULL) {
            ofstream ofs(outputFilename);
            SVL_ASSERT(!ofs.fail());
            for (unsigned i = 0; i < marginals.size(); i++) {
                ofs << toString(marginals[i]) << "\n";
            }
            ofs.close();
        } else {
            for (unsigned i = 0; i < marginals.size(); i++) {
                SVL_LOG(SVL_LOG_VERBOSE, toString(marginals[i]));
            }
        }

    } else if (!strcasecmp(classifierType, "BOOSTING")) {
        svlBoostedClassifierSet classifier(nClasses > 2 ? nClasses : 1);
        if (!classifier.load(PARAMFILE)) {
            SVL_LOG(SVL_LOG_FATAL, "could not load parameters from " << PARAMFILE);
        }

        vector<vector<double> > scores;
        classifier.getClassScores(features, scores);
        if (classifier.size() == 1) {
            for (unsigned i = 0; i < scores.size(); i++) {
                predictedLabels[i] = (scores[i][0] >= 0) ? 0 : 1;
            }
        } else {
            predictedLabels = argmaxs(scores);
            nPredicted = classifier.size();
        }
        
        // write output
        if (outputFilename != NULL) {
            ofstream ofs(outputFilename);
            SVL_ASSERT(!ofs.fail());
            for (unsigned i = 0; i < scores.size(); i++) {
                ofs << toString(scores[i]) << "\n";
            }
            ofs.close();
        } else {
            for (unsigned i = 0; i < scores.size(); i++) {
                SVL_LOG(SVL_LOG_VERBOSE, toString(scores[i]));
            }
        }

    } else if (!strcasecmp(classifierType, "SVM")) {
        svlSVM classifier;
        if (!classifier.load(PARAMFILE)) {
            SVL_LOG(SVL_LOG_FATAL, "could not load parameters from " << PARAMFILE);
        }

        vector<double> scores;
        classifier.getScores(features, scores);
        for (unsigned i = 0; i < scores.size(); i++) {
            predictedLabels[i] = (scores[i] >= 0) ? 0 : 1;
        }
        
        // write output
        if (outputFilename != NULL) {
            ofstream ofs(outputFilename);
            SVL_ASSERT(!ofs.fail());
            for (unsigned i = 0; i < scores.size(); i++) {
                ofs << scores[i] << "\n";
            }
            ofs.close();
        } else {
            for (unsigned i = 0; i < scores.size(); i++) {
                SVL_LOG(SVL_LOG_VERBOSE, scores[i]);
            }
        }

    } else {
        SVL_LOG(SVL_LOG_FATAL, "unrecognized classifier type " << classifierType);
    }
    SVL_LOG(SVL_LOG_VERBOSE, "...done");

    // load groundtruth labels (for scoring)
    if (labelsFilename) {
        SVL_LOG(SVL_LOG_VERBOSE, "Reading groundtruth labels " << labelsFilename << "...");
        vector<int> actualLabels(features.size(), -1);
        ifs.open(labelsFilename);
        SVL_ASSERT(!ifs.fail());
        for (int i = 0; i < (int)actualLabels.size(); i++) {
            ifs >> actualLabels[i];
        }
        ifs.close();
        SVL_LOG(SVL_LOG_VERBOSE, "...done");

        SVL_ASSERT(predictedLabels.size() == actualLabels.size());
        int nActual = *max_element(actualLabels.begin(), actualLabels.end()) + 1;

        // build confusion matrix
        svlConfusionMatrix confusionMatrix(nActual, nPredicted);
        confusionMatrix.accumulate(actualLabels, predictedLabels);
        
        confusionMatrix.printRowNormalized();
        SVL_LOG(SVL_LOG_MESSAGE, "Classifier accuracy: " 
            << confusionMatrix.accuracy());
    }


    // print profile information
    svlCodeProfiler::print(cerr);
    return 0;
}


