/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    filterPatchDictionary.cpp
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
**                based on trainObjectDetector.cpp by Steve Gould
** DESCRIPTION:
**   Trims the dictionary down to a more manageable size; eliminates
**   features that are not very informative on the training set
**
*****************************************************************************/

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sys/types.h>
#include <vector>

#if defined(_WIN32)||defined(WIN32)||defined(__WIN32__)
#include "win32/dirent.h"
#else
#include <dirent.h>
#endif

#include "cv.h"
#include "cxcore.h"
#include "highgui.h"

#include "svlBase.h"
#include "svlML.h"
#include "svlVision.h"

using namespace std;

// Main -----------------------------------------------------------------------

void usage()
{
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./filterPatchDictionary <dictionary> <+ cache dir> <- cache dir>" << endl;
    cerr << "OPTIONS:" << endl
	 << "  -o <filename>       :: output filename for the new dictionary" << endl
      // << "  -cc <threshold>     :: threshold for cross-correlation of patches (default: 0.9)" << endl
      //<< "  -F <threshold>      :: threshold for F-score of patches (default: 0.75)" << endl
      // << "  -MI <threshold>     :: instead of the above, use the mutual information of the " << endl
      // << "                         features to make a decision; threshold is the lowest acceptable " << endl
      // << "                         increase in MI for a patch to be added (default: 0)" << endl
      // << "  -minSize <n>        :: minimum number of features expected (overrides -F and -MI, not -cc)" << endl
      // << "  -maxSize <n>        :: maximum number of features expected" << endl
	 << "  -maxInstances <n>   :: maximum training images for each class (default: 1000)" << endl
	 << "  -binary             :: the cache dirs are really names of binary files" << endl
	 << "  -update             :: updates the cache to only contain the chosen features" << endl
	 << "  -useMI              :: use mutual information to select features (automatic if -MI is provided)" << endl
	 << SVL_STANDARD_OPTIONS_USAGE
	 << "Abreviations (use instead of -set svlML.svlFeatureSelector ...):" << endl
	 << " *** do not use when providing a config file ***" << endl
	 << "  -minSize <n>       <=> minSize <n> " << endl
	 << "  -maxSize <n>       <=> maxSize <n> " << endl
	 << "             (use instead of -set svlML.svlMIFeatureSelector ...):" << endl
	 << "  -MI <t>            <=> threshold <t> " << endl
	 << "             (use instead of -set svlML.svlPatchFeatureSelector ...):" << endl
	 << "  -F <t>             <=> fThreshold <t> " << endl
	 << "  -cc <t>            <=> ccThreshold <t> " << endl
	 << endl;
}

int main(int argc, char *argv[])
{
  // read commandline parameters
  const int NUM_REQUIRED_PARAMETERS = 3;
  
  const char *posCacheDir = NULL;
  const char *negCacheDir = NULL;
  
  const char *dictionaryFilename = NULL;
  const char *outputFilename = NULL;

  int maxTrainingInstances = 1000;
  
  bool binary = false;
  bool useMI = false;
  bool update = false;
  
  SVL_BEGIN_CMDLINE_PROCESSING(argc, argv)
    SVL_CMDLINE_INT_OPTION("-maxInstances", maxTrainingInstances)
    SVL_CMDLINE_STR_OPTION("-o", outputFilename)
    SVL_CMDLINE_BOOL_OPTION("-binary", binary)
    SVL_CMDLINE_BOOL_OPTION("-update", update)
    SVL_CMDLINE_BOOL_OPTION("-useMI", useMI)
    SVL_CMDLINE_REAL_OPTION("-cc", svlPatchFeatureSelector::CCORR_THRESHOLD)
    SVL_CMDLINE_REAL_OPTION("-F", svlPatchFeatureSelector::F_SCORE_THRESHOLD)
    SVL_CMDLINE_OPTION_BEGIN("-MI", ptr)
      svlMIFeatureSelector::MI_THRESHOLD = atof(ptr[0]);
      useMI = true;
    SVL_CMDLINE_OPTION_END(1)
    SVL_CMDLINE_INT_OPTION("-minSize", svlFeatureSelector::MIN_SIZE)
    SVL_CMDLINE_INT_OPTION("-maxSize", svlFeatureSelector::MAX_SIZE)
  SVL_END_CMDLINE_PROCESSING(usage())

  if (SVL_CMDLINE_ARGC != NUM_REQUIRED_PARAMETERS) {
    usage();
    return -1;
  }

  dictionaryFilename = SVL_CMDLINE_ARGV[0];
  posCacheDir = SVL_CMDLINE_ARGV[1];
  negCacheDir = SVL_CMDLINE_ARGV[2];
  
  // read the dictionary
  svlPatchDictionary dict;
  dict.load(dictionaryFilename);
  
  // load positive and negative training samples from cache
  SVL_LOG(SVL_LOG_VERBOSE, "Reading positive samples...");
  vector<vector<double> > posSamples = svlReadExamplesFromCache(posCacheDir, maxTrainingInstances, binary);
  SVL_LOG(SVL_LOG_VERBOSE, "..." << posSamples.size() << " samples read");
  SVL_LOG(SVL_LOG_VERBOSE, "Reading negative samples...");
  vector<vector<double> > negSamples = svlReadExamplesFromCache(negCacheDir, maxTrainingInstances, binary);
  SVL_LOG(SVL_LOG_VERBOSE, "..." << negSamples.size() << " samples read");
  
  svlFeatureSelector *selector = NULL;
  if (useMI) {
    selector = new svlMIFeatureSelector();
  } else {
    selector = new svlPatchFeatureSelector(&dict);
  }
  vector<bool> selected;
  selector->filter(posSamples, negSamples, selected, dict.numFeatures());
  dict.filterEntries(selected);
  
  SVL_LOG(SVL_LOG_MESSAGE, "Trimmed dictionary to " << dict.numFeatures() << " patches");
  
  dict.write(outputFilename);

  if (update) {
    vector<double> sample;

    ofstream outFile;
    if (binary)
      outFile.open(posCacheDir, std::ios::out|std::ios::binary);
    for (unsigned i = 0; i < posSamples.size(); i++) {
      sample.clear();
      for (unsigned j = 0; j < selected.size(); j++) {
	if (selected[j])
	  sample.push_back(posSamples[i][j]);
      }
      if (! binary)
	svlWriteExampleToCache(posCacheDir, i, sample);
      else
	svlWriteExampleToBinaryCache(outFile, sample);
    }
    if (binary)
      outFile.close();

    if (binary)
      outFile.open(negCacheDir, std::ios::out|std::ios::binary);
    for (unsigned i = 0; i < negSamples.size(); i++) {
      sample.clear();
      for (unsigned j = 0; j < selected.size(); j++) {
	if (selected[j])
	  sample.push_back(negSamples[i][j]);
      }
      if (! binary)
	svlWriteExampleToCache(negCacheDir, i, sample);
      else
	svlWriteExampleToBinaryCache(outFile, sample);
    }
    if (binary)
      outFile.close();

  }
  
  return 0;
}







