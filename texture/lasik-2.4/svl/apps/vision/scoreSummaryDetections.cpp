/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    scoreDetections.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**              Olga Russakovsky <olga@cs.stanford.edu>
** DESCRIPTION:
**   Given detection summary files, computes the precision and recall.
*****************************************************************************/

#include <cstdlib>
#include <string>
#include <limits>
#include <iomanip>
#include <fstream>
#include <map>
#include <vector>

#include "svlBase.h"
#include "svlVision.h"

using namespace std;

// type definitions and function prototypes ----------------------------------



// main ----------------------------------------------------------------------

void usage()
{
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./scoreSummaryDetections [OPTIONS] <summary file>+\n";
    cerr << "OPTIONS:\n"
	 << "  -pr <filestem>    :: generate precision-recall curves\n"
	 << "  -object <name>    :: object name (for pr curve files)\n"
         << SVL_STANDARD_OPTIONS_USAGE
	 << endl;
}

int main(int argc, char *argv[])
{
    // read commandline parameters
  const int NUM_REQUIRED_PARAMETERS = 1;

  const char *prFilename = NULL;
  const char *name = "[unknown]";

  SVL_BEGIN_CMDLINE_PROCESSING(argc, argv)
    SVL_CMDLINE_STR_OPTION("-pr", prFilename)
    SVL_CMDLINE_STR_OPTION("-object", name)
  SVL_END_CMDLINE_PROCESSING(usage())
    
  if (SVL_CMDLINE_ARGC < NUM_REQUIRED_PARAMETERS) {
    usage();
    return -1;
  }

  svlObjectDetectionAnalyzer *d = new svlObjectDetectionAnalyzer(string(name));

  for (unsigned i = 0; i < (unsigned)SVL_CMDLINE_ARGC; i++) {
    bool success = d->addScores(SVL_CMDLINE_ARGV[i]);
    SVL_ASSERT_MSG(success, "Failed to load " << SVL_CMDLINE_ARGV[i]);
  }

  if (prFilename)
    d->writePRCurves(prFilename);
  return 0;
}


