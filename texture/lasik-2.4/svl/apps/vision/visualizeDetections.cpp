/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    visualizeDetections.cpp
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
** DESCRIPTION:
**   Given an XML file and a sequence of images, displays the images with the 
**   detected objects outlined.
*****************************************************************************/

//todo-- switch to using svlLabelVisualizer and then there's a standard way
//of rendering everything

#include <cstdlib>
#include <string>
#include <limits>
#include <iomanip>
#include <fstream>
#include <map>
#include <vector>

#include "svlBase.h"
#include "svlVision.h"

using namespace std;

#define WINDOW_NAME "detections"
#define MAX_CHAR 255

void usage()
{
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./visualizeDetections [OPTIONS] <objects xml> <imageSequence>" << endl;
    cerr << "OPTIONS:" << endl
	 << " -t <num>     :: threshold for displaying objects (default: 0.5)" << endl
	 << " -o <dirname> :: instead of displaying images, dump them to a directory and make an html file to display them" << endl
	 << " -n <dirname> :: number to visualize"  << endl
	 << " -nm          :: use non-maxima suppression"  << endl
	 << " -xmin <x>    :: the xmin of the lefthand corner of the objects to show" << endl
	 << " -xmax <x>    :: the xmax of the lefthand corner of the objects to show" << endl
	 << " -ymin <y>    :: the ymin of the lefthand corner of the objects to show" << endl
	 << " -ymax <y>    :: the ymax of the lefthand corner of the objects to show" << endl
	 << " -tspecial <num>    :: threshold for highlighting objects" << endl
	 << " -white       :: draws the detections using thick white lines" << endl
	 << " -best        :: highlights the best detection" << endl
	 << " -text        :: writes the score of the detection next to it" << endl
	 << endl;
}

static void renderImage(IplImage * img, svlObject2dFrame & objects, double threshold, int maxToVisualize,
			int xmin, int xmax, int ymin, int ymax, float tspecial, bool white, bool best,
			bool text)
{
  if (best) {
    float best_threshold = INT_MIN;
    int best_index = -1;
    for (unsigned j = 0; j < objects.size(); j++) {
      if (objects[j].pr > best_threshold) {
	best_threshold = objects[j].pr;
	best_index = j;
      }
    }
    cerr << "\tBest threshold: " << best_threshold << endl;
    for(unsigned j = 0; j < objects.size(); j++) {
      objects[j].index = ((int)j == best_index) ? 1 : 0;
    }
  }

  int numShown = 0;
  for (unsigned j = 0; j < objects.size(); j++) {
    const svlObject2d &obj = objects[j];
    
    int x1 = (int)(obj.x);
    int y1 = (int)(obj.y);
    int x2 = (int)(obj.x + obj.w - 1);
    int y2 = (int)(obj.y + obj.h - 1);

    if (obj.pr < threshold)
      continue;
    
    if (x1 < xmin || x1 > xmax || y1 < ymin || y1 > ymax)
      continue;

    CvScalar color = CV_RGB(0, 255, 0);
    int line_width = 2;

    if (obj.index == 1) {
      color = CV_RGB(0, 0, 255);
      line_width = 2;
    } else if (white) {
      color = CV_RGB(255, 255, 255);
      line_width = 2;
    } else if (obj.pr > tspecial) {
      color = CV_RGB(255, 0, 0);
      line_width = 2;
    }

    cvRectangle(img, cvPoint(x1, y1), cvPoint(x2, y2),
		color, line_width);
    if (text) {
      CvFont font;
      cvInitFont(&font, CV_FONT_HERSHEY_SIMPLEX, 1, 1, 0, 1);
      char buffer[256];
      //sprintf(buffer, "%.2f", obj.pr);
      sprintf(buffer, "%s", obj.name.c_str());
      cvPutText(img, buffer, cvPoint(x1, y1), &font, color); 
    }

    numShown++;
    if (numShown > maxToVisualize) break;
  }

  cerr << "\tShowing " << numShown << " object(s)";
  if (numShown > maxToVisualize)
    cerr << " (out of " << objects.size() << " originally)";
  cerr << endl;

}


static int showImage(IplImage *&img) {

  
  cvShowImage(WINDOW_NAME, img);
  return cvWaitKey(0) & MAX_CHAR;
}



#define NUM_REQUIRED_PARAMETERS 2

int main(int argc, char **argv)
{
  char *inputFilename;
  char *imageSeq;
  char * outdir = NULL;
  string outdirStr;
  double threshold = 0.5;
  bool bUseNonmax = false;
  int maxToVisualize = INT_MAX;

  int xmin = 0, xmax = INT_MAX, ymin = 0, ymax = INT_MAX;
  float tspecial = INT_MAX; // something > 1
  bool white = false;
  bool best = false;
  bool text = false;

  cvNamedWindow(WINDOW_NAME, 0);
  
  char **args = argv + 1;
  while (--argc > NUM_REQUIRED_PARAMETERS) {
    if (!strcmp(*args, "-t")) {
      threshold = atof(*(++args)); argc--;
    } 
    else if (!strcmp(*args, "-n")) {
      maxToVisualize = atoi(*(++args)); argc--;
    } 
    else if (!strcmp(*args, "-o"))
      {
	args++;
	argc--;

	outdir = *args;
      }
    else if (!strcmp(*args, "-nm"))
      {
	bUseNonmax = true;
      }
    else if (!strcmp(*args, "-white")) {
      white = true;
    }
    else if (!strcmp(*args, "-xmin")) {
      xmin = atoi(*(++args)); argc--;
    } else if (!strcmp(*args, "-xmax")) {
      xmax = atoi(*(++args)); argc--;
    } else if (!strcmp(*args, "-ymin")) {
      ymin = atoi(*(++args)); argc--;
    } else if (!strcmp(*args, "-ymax")) {
      ymax = atoi(*(++args)); argc--;
    } else if (!strcmp(*args, "-tspecial")) {
      tspecial = atof(*(++args)); argc--;
    } else if (!strcmp(*args, "-text")) {
      text = true;
    } else if (!strcmp(*args, "-best")) {
      best = true;
    } else if (**args == '-') {
      SVL_LOG(SVL_LOG_FATAL, "unrecognized option " << *args);
    } else {
      break;
    }
    args++;
  }

  if (argc < NUM_REQUIRED_PARAMETERS) {
    usage();
    return -1;
  }

  inputFilename = args[0];
  imageSeq = args[1];

  ofstream ofs;

  if (outdir)
    {
      outdirStr = string(outdir);

      svlCreateDirectory(outdir);

      ofs.open((outdirStr+"/index.html").c_str());

      ofs << "<html> \n"
	  << "<body> \n";

      assert(!ofs.fail());
    }


  svlObject2dSequence objects;
  SVL_LOG(SVL_LOG_VERBOSE, "Reading from " << inputFilename);
	
  if (!objects.read(inputFilename)) {
    SVL_LOG(SVL_LOG_ERROR, "...failed to read " << inputFilename);
  }

  if (bUseNonmax)
    {
      svlObjectDetectionAnalyzer d;
      d.setNonmax(true);
      d.loadDetections(objects); 
      objects = d.getDetections();
    }

  svlImageSequence sequence;
  SVL_ASSERT_MSG(strExtension(imageSeq).compare("xml") == 0, imageSeq << " is not an image sequence");
  sequence.load(imageSeq);
 
  IplImage *img = NULL;
  int level = 0;
  int numLevels = sequence.size();
  svlObject2dFrame tmp;
  while (true) {
    cerr << "Showing level " << level+1 << " of " << numLevels
	 << " (" << strBaseName(sequence[level]) << ")" << endl;
    // get the correct image at the correct level (and make it non-const)
    img = cvCloneImage(sequence.image(level));
    // find the corresponding list of objects

    map<string, svlObject2dFrame>::iterator iter = objects.find(strBaseName(sequence[level]));

    int key = -1;
    if (iter == objects.end())
      {
	if (!outdir)
	  key = showImage(img);
      }
    else 
      {
	renderImage(img, iter->second, threshold, maxToVisualize,
		    xmin, xmax, ymin, ymax, tspecial, white, best, text);
	if (!outdir)
	  key = showImage(img );
      }


    
    if (outdir)
      {

	string filename = strBaseName(sequence[level])+".jpg";

	stringstream s;
	s << outdirStr << "/" << filename;

	cvSaveImage(s.str().c_str(), img);

	ofs << "<img src=\" "
	    << filename
	    << "\" > \n";


	if (level < numLevels - 1)
	  level++;
	else
	  break;
      }
    else if (key == (int)'n') {
      if (level < numLevels-1) level++;
    } else if (key == (int)'+') {
      threshold += 0.01;
      if (threshold > 1) threshold = 1.0;
      cerr << "New threshold: "  << threshold << endl;
    } else if (key == (int)'-') {
      threshold -= 0.01;
      if (threshold < 0) threshold = 0.0;
      cerr << "New threshold: "  << threshold << endl;
    } else if (key == (int)'b') {
      if (level > 0) level--;
    } else if (key == (int)'s') {
      string name = sequence.directory() + string("/visualize_") + sequence[level];
      cvSaveImage(name.c_str(), img);
    } else if (key == (int)'q') {
      break;
    }
  }


  if (outdir)
    {
      ofs << "</body> \n"
	  << "</html> \n";
      ofs.close();
    }

  return 0;
}

