/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    scoreDetections.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**              Olga Russakovsky <olga@cs.stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
** DESCRIPTION:
**   Given two XML object files, computes the precision and recall.
*****************************************************************************/

#include <cstdlib>
#include <string>
#include <limits>
#include <iomanip>
#include <fstream>
#include <map>
#include <vector>

#include "svlBase.h"
#include "svlVision.h"

using namespace std;

// type definitions and function prototypes ----------------------------------

void usage()
{
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./scoreDetections [OPTIONS] (<groundtruth xml> <results xml>)+\n";
    cerr << "OPTIONS:\n"
	 << "  -threshold <num>  :: threshold for positive classification (default 0.5)\n"
	 << "  -pr <filestem>    :: generate precision-recall curves\n"
	 << "  -prd <filestem>   :: generate discretized (less accurate) precision-recall curves\n"
	 << "  -o <filestem>     :: output detections (true and false)\n"
	 << "  -object <name>    :: specifies one object of interest\n"
         << "  -n                :: generate recall v.s. false positive per window curve (broken)\n"
         << "  -a <filename>     :: print all info (TP, FP, FN, thresh) to a file.\n"
         << "                    :: You can then use this to make an FPPI or FPPW curve\n"
         << SVL_STANDARD_OPTIONS_USAGE
	 << endl;
}

// main ----------------------------------------------------------------------

int main(int argc, char *argv[])
{
    // read commandline parameters
    const int NUM_REQUIRED_PARAMETERS = 2;

    const char *gtFilename = NULL;
    const char *xmlFilename = NULL;

    double threshold = 0.5;
    const char *prFilestem = NULL;
    const char *outputFilestem = NULL;
    const char *outputAllFile = NULL;
    const char *object = NULL;
    bool discreteCurves = false;
    bool bRecallFfpw = false;

    SVL_BEGIN_CMDLINE_PROCESSING(argc, argv)
      SVL_CMDLINE_REAL_OPTION("-threshold", threshold)
      SVL_CMDLINE_STR_OPTION("-pr", prFilestem)
      SVL_CMDLINE_STR_OPTION("-a",outputAllFile)
      SVL_CMDLINE_OPTION_BEGIN("-prd", ptr)
         prFilestem = ptr[0];
         discreteCurves = true;
      SVL_CMDLINE_OPTION_END(1)
      SVL_CMDLINE_STR_OPTION("-o", outputFilestem)
      SVL_CMDLINE_STR_OPTION("-object", object)
      SVL_CMDLINE_BOOL_OPTION("-n", bRecallFfpw)
    SVL_END_CMDLINE_PROCESSING(usage())
    
    if (SVL_CMDLINE_ARGC < NUM_REQUIRED_PARAMETERS) {
	usage();
	return -1;
    }

      svlCodeProfiler::tic(svlCodeProfiler::getHandle("main"));

      // Accumulate counts per object class, e.g. objectScores["mug"]
      // holds the score of every mug detection and whether it is a
      // true positive (1) or not (0).

      svlSmartPointer<svlObjectDetectionAnalyzer> da;
      if (object == NULL) {
          da = new svlMultObjectsDetectionAnalyzer();
      } else {
          da = new svlObjectDetectionAnalyzer(string(object));
      }

    for (unsigned i = 0; i < (unsigned)(SVL_CMDLINE_ARGC / 2); i++) {
	gtFilename = SVL_CMDLINE_ARGV[2 * i];
	xmlFilename = SVL_CMDLINE_ARGV[2 * i + 1];
        
	SVL_LOG(SVL_LOG_VERBOSE, "Reading from " << gtFilename << " and " << xmlFilename);
	
	if (!da->loadGt(gtFilename)) continue;
        
	SVL_LOG(SVL_LOG_VERBOSE, "Loading detections...");
	if (!da->loadDetections(xmlFilename)) continue;
	SVL_LOG(SVL_LOG_VERBOSE, "... done");

 	// output detections
	if (outputFilestem) {
            string filename = string(outputFilestem) + string(".") + toString(i) + string(".xml");
            da->outputDetections(filename.c_str(), threshold);
	}
        
        // accumulate counts
	da->accumulateScores();
    }
   
    // show results
    da->printResults((float)threshold);
    cout << endl;

    // construct PR curve
    if (prFilestem != NULL) {
	SVL_LOG(SVL_LOG_VERBOSE, "Generating precision-recall curves...");
	if (discreteCurves)
            da->quantizeScores();

        if (bRecallFfpw) {
            da->writeNormalizedPRCurves(prFilestem);
        } else {
            da->writePRCurves(prFilestem);
        }
	SVL_LOG(SVL_LOG_VERBOSE, "...done");
    }

    if (outputAllFile) {
	SVL_LOG(SVL_LOG_VERBOSE, "Writing all info to '"<<outputAllFile<<"'");
	da->writeAllInfo(outputAllFile);
    }

    svlCodeProfiler::toc(svlCodeProfiler::getHandle("main"));
    svlCodeProfiler::print(cerr);
    return 0;
}


