/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    segImageEvalModel.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
** DESCRIPTION:
**  Evaluation code for multi-class image segmentation.
**
*****************************************************************************/

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>

#include <cxcore.h>
#include <cv.h>
#include <ml.h>

#include "svlBase.h"
#include "svlML.h"
#include "svlPGM.h"
#include "svlVision.h"

using namespace std;

#define WINDOW_NAME "segImageEvalModel"

typedef struct _TRegionDef {
    int id;
    std::string name;
    unsigned char red;
    unsigned char green;
    unsigned char blue;
} TRegionDef;

// function prototypes -----------------------------------------------------

double scoreImagePixelwise(svlSegImage* testImage,
    const vector<int>& predictedLabels,
    svlConfusionMatrix& confusion);

double scoreImageSegmentwise(const vector<int>& actualLabels,
    const vector<int>& predictedLabels,
    svlConfusionMatrix& confusion);

// main --------------------------------------------------------------------

void usage()
{
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./segImageEvalModel [OPTIONS] <modelFile> (<evaluationList> | <baseName>)" << endl;
    cerr << "OPTIONS:" << endl
         << "  -imgExt <ext>     :: features file extension (default: .jpg)\n"
         << "  -imgDir <dir>     :: features file directory (default: .)\n"
         << "  -segExt <ext>     :: over-segmentation file extension (default: .seg)\n"
         << "  -segDir <dir>     :: over-segmentation file directory (default: <imgDir>)\n"
         << "  -lblExt <ext>     :: pixel labels file extension (default: .txt)\n"
         << "  -lblDir <dir>     :: pixel labels file directory (default: <imgDir>)\n"
         << "  -featuresExt <ext>:: features file extension (default: .boosted.txt)\n"
         << "  -edgeFeaturesExt <ext> :: edge features file extension (default: none)\n"
         << "  -featuresDir <dir>:: features file directory (default: <imgDir>)\n"
	 << "  -model <type>     :: LOGISTIC or CRF (default)\n"
	 << "  -regions <file>   :: region definition file\n"
	 << "  -outputDir <dir>  :: output directory for images/marginals\n"
	 << "  -outputImages     :: output images\n"
         << "  -outputScores     :: output image scores\n"
	 << "  -outputMarginals  :: output marginals\n"
	 << "  -outputConfusion  :: output confusion matrix (<directory>/confusion.txt)\n"
         << "  -outputModel      :: output CRF model (i.e., cluster graph)\n"
         << "  -segwiseScore     :: produces scores on segments rather than pixels\n"
	 << "  -x                :: visualize evaluation\n"
         << SVL_STANDARD_OPTIONS_USAGE
	 << endl;
}

int main(int argc, char *argv[])
{
    // read commandline parameters
    const char *imgExt = ".jpg";
    const char *imgDir = ".";
    const char *segExt = ".seg";
    const char *segDir = NULL;
    const char *lblExt = ".txt";
    const char *lblDir = NULL;
    const char *featuresExt = ".boosted.txt";
    const char *edgeFeaturesExt = NULL;
    const char *featuresDir = NULL;

    const char *modelType = "CRF";
    const char *regionsFilename = NULL;

    const char *outputDir = "./";
    bool bOutputImages = false;
    bool bOutputScores = false;
    bool bOutputMarginals = false;
    bool bOutputConfusion = false;
    bool bOutputCRFModel = false;
    bool bPixelwiseScore = true;
    bool bVisualize = false;

    SVL_BEGIN_CMDLINE_PROCESSING(argc, argv)
        SVL_CMDLINE_STR_OPTION("-imgExt", imgExt)
        SVL_CMDLINE_STR_OPTION("-imgDir", imgDir)
        SVL_CMDLINE_STR_OPTION("-segExt", segExt)
        SVL_CMDLINE_STR_OPTION("-segDir", segDir)
        SVL_CMDLINE_STR_OPTION("-lblExt", lblExt)
        SVL_CMDLINE_STR_OPTION("-lblDir", lblDir)
        SVL_CMDLINE_STR_OPTION("-featuresExt", featuresExt)
        SVL_CMDLINE_STR_OPTION("-edgeFeaturesExt", edgeFeaturesExt)
        SVL_CMDLINE_STR_OPTION("-featuresDir", featuresDir)
        SVL_CMDLINE_STR_OPTION("-model", modelType)
        SVL_CMDLINE_STR_OPTION("-regions", regionsFilename)
        SVL_CMDLINE_STR_OPTION("-outputDir", outputDir)
        SVL_CMDLINE_BOOL_OPTION("-outputImages", bOutputImages)
        SVL_CMDLINE_BOOL_OPTION("-outputScores", bOutputScores)
        SVL_CMDLINE_BOOL_OPTION("-outputMarginals", bOutputMarginals)
        SVL_CMDLINE_BOOL_OPTION("-outputConfusion", bOutputConfusion)
        SVL_CMDLINE_BOOL_OPTION("-outputModel", bOutputCRFModel)
        SVL_CMDLINE_BOOL_TOGGLE_OPTION("-segwiseScore", bPixelwiseScore)
        SVL_CMDLINE_BOOL_OPTION("-x", bVisualize)
    SVL_END_CMDLINE_PROCESSING(usage());

    if (SVL_CMDLINE_ARGC != 2) {
	usage();
	return -1;
    }

    svlCodeProfiler::tic(svlCodeProfiler::getHandle("main"));

    const char *modelFilename = SVL_CMDLINE_ARGV[0];
    const char *evaluationList = SVL_CMDLINE_ARGV[1];
    if (segDir == NULL) segDir = imgDir;
    if (lblDir == NULL) lblDir = imgDir;
    if (featuresDir == NULL) featuresDir = imgDir;

    if (bVisualize) {
	cvNamedWindow(WINDOW_NAME, 1);
    }

    // load region definitions
    vector<TRegionDef> regionDefinitions;
    TRegionDef def;
    if (regionsFilename == NULL) {
	def.id = 0; def.name = "background";
	def.red = def.green = def.blue = 0x00;
	regionDefinitions.push_back(def);
	def.id = 1; def.name = "foreground";
	def.red = def.green = def.blue = 0xff;
	regionDefinitions.push_back(def);
    } else {
	XMLNode root = XMLNode::parseFile(regionsFilename, "regionDefinitions");
	SVL_ASSERT_MSG(!root.isEmpty(), "could not read region definitions from " << regionsFilename);
	for (int i = 0; i < root.nChildNode("region"); i++) {
	    XMLNode node = root.getChildNode("region", i);
	    def.id = atoi(node.getAttribute("id"));
	    def.name = string(node.getAttribute("name"));
	    if (sscanf(node.getAttribute("color"), "%hhd %hhd %hhd",
		    &def.red, &def.green, &def.blue) != 3) {
		SVL_LOG(SVL_LOG_FATAL, "could not parse color for \"" << def.name << "\" \"");
		return -1;
	    }
	    regionDefinitions.push_back(def);
	}
	SVL_LOG(SVL_LOG_VERBOSE, "... " << regionDefinitions.size() << " regions defined");
    }

    // initialize models
    svlMultiClassLogistic logisticModel;
    svlPairwiseCRFModel crfModel;
    int numFeatures = 0;
    int numEdgeFeatures = 0;
    int numClasses = 0;
    if (!strcasecmp(modelType, "LOGISTIC")) {
	logisticModel.load(modelFilename);
	numFeatures = logisticModel.numFeatures();
	numClasses = logisticModel.numClasses();
    } else if (!strcasecmp(modelType, "CRF")) {
	crfModel.read(modelFilename);
	numFeatures = crfModel.dimSingleton();
        numEdgeFeatures = crfModel.dimPairwise();
	numClasses = crfModel.numClasses();
    } else {
	usage();
	return -1;
    }

    // load evaluation images
    vector<string> baseNames;
    if (svlFileExists(evaluationList)) {
        SVL_LOG(SVL_LOG_MESSAGE, "Reading evaluation list from " << evaluationList << "...");
        baseNames = svlReadFile(evaluationList);
        SVL_LOG(SVL_LOG_MESSAGE, "...read " << baseNames.size() << " images");
    } else {
        baseNames.push_back(string(evaluationList));
    }

    int hImageLoad = svlCodeProfiler::getHandle("imageLoad");
    int hFeaturesLoad = svlCodeProfiler::getHandle("featuresLoad");
    int hEdgeFeaturesLoad = svlCodeProfiler::getHandle("edgeFeaturesLoad");
    int hLogisticInference = svlCodeProfiler::getHandle("inference (logistic)");
    int hCRFInference = svlCodeProfiler::getHandle("inference (crf)");

    svlConfusionMatrix confusion(numClasses);

    // process instances
    for (int n = 0; n < (int)baseNames.size(); n++) {
        // instantiate svlSegImage
        SVL_LOG(SVL_LOG_MESSAGE, "Processing image " << baseNames[n]);
        string imgName = string(imgDir) + string("/") + baseNames[n] + string(imgExt);
        string segName = string(segDir) + string("/") + baseNames[n] + string(segExt);
        string lblName = string(lblDir) + string("/") + baseNames[n] + string(lblExt);

        svlCodeProfiler::tic(hImageLoad);
        svlSegImage evalImage(imgName.c_str(), segName.c_str(), lblName.c_str());
        svlCodeProfiler::toc(hImageLoad);
        SVL_LOG(SVL_LOG_VERBOSE, "..." << toString(*evalImage.image()));

	// show segmentation
	if (bVisualize) {
	    IplImage *debugImage = evalImage.visualize();
	    cvShowImage(WINDOW_NAME, debugImage);
	    if (cvWaitKey(-1) == 27) {
		bVisualize = false;
		cvReleaseImage(&debugImage);
		break;
	    }

	    cvZero(debugImage);
	    for (int j = 0; j < evalImage.numSegments(); j++) {
		int indx = evalImage.getLabel(j);
		if (indx < 0) continue;
		for (int k = 0; k < (int)regionDefinitions.size(); k++) {
		    if (regionDefinitions[k].id == indx) {
			evalImage.colorSegment(debugImage, j, regionDefinitions[k].red,
			    regionDefinitions[k].green, regionDefinitions[k].blue);
			break;
		    }
		}
	    }
	    evalImage.colorBoundaries(debugImage);
	    cvShowImage(WINDOW_NAME, debugImage);
	    if (cvWaitKey(-1) == 27) {
		bVisualize = false;
	    }

	    cvReleaseImage(&debugImage);
	}

	// load features and add to training instances
	svlPairwiseCRFInstance instance;
	svlCodeProfiler::tic(hFeaturesLoad);
        string featuresName = string(featuresDir) + string("/") + baseNames[n] + string(featuresExt);
	SVL_LOG(SVL_LOG_VERBOSE, "...computing features from " << featuresName);
        SVL_LOG(SVL_LOG_VERBOSE, "...expecting " << evalImage.numSegments()
            << " features of length " << numFeatures);
        ifstream ifsData(featuresName.c_str());
        assert(!ifsData.fail());
        while (1) {
            // read feature vectors
            vector<double> v(numFeatures);
            for (int j = 0; j < numFeatures; j++) {
                ifsData >> v[j];
            }
            if (ifsData.fail()) break;

            instance.Xn.push_back(v);
        }
        ifsData.close();
	SVL_ASSERT_MSG((int)instance.Xn.size() == evalImage.numSegments(),
            (int)instance.Xn.size() << " != " << evalImage.numSegments());
	svlCodeProfiler::toc(hFeaturesLoad);

	SVL_LOG(SVL_LOG_VERBOSE, "...assigning groundtruth labels");
	instance.Yn.resize(evalImage.numSegments(), -1);
	for (int j = 0; j < evalImage.numSegments(); j++) {
	    instance.Yn[j] = evalImage.getLabel(j);
	}

	SVL_LOG(SVL_LOG_VERBOSE, "...computing graph structure");
	instance.edges = evalImage.getAdjacencyList();
	instance.Xnm.resize(instance.numEdges());
	for (int j = 0; j < (int)instance.Xnm.size(); j++) {
	    instance.Xnm[j].resize(1, 1.0);
	}

        // add edge potentials
        if (edgeFeaturesExt == NULL) {
            // use edge location
            for (int j = 0; j < (int)instance.edges.size(); j++) {
                pair<int, int> edge = instance.edges[j];
                double y = (double)(evalImage.getCentroid(edge.first).y +
                    evalImage.getCentroid(edge.second).y) / (2.0 * evalImage.height());
                double x = (double)(evalImage.getCentroid(edge.first).x +
                    evalImage.getCentroid(edge.second).x) / (2.0 * evalImage.width());
                instance.Xnm[j].push_back(y);
                instance.Xnm[j].push_back(x);
            }
        } else {
            // use feature edge potentials
            svlCodeProfiler::tic(hEdgeFeaturesLoad);
            string edgeFeaturesName = string(featuresDir) + string("/") +
                baseNames[n] + string(edgeFeaturesExt);
            ifstream ifsData(edgeFeaturesName.c_str());
            assert(!ifsData.fail());
            for (int j = 0; j < (int)instance.edges.size(); j++) {
                // read feature vectors
                instance.Xnm[j].resize(numEdgeFeatures);
                for (int k = 0; k < numEdgeFeatures; k++) {
                    ifsData >> instance.Xnm[j][k];
                }
                assert(!ifsData.fail());
            }
            ifsData.close();
            svlCodeProfiler::toc(hEdgeFeaturesLoad);
        }

        // evaluate model on instance
	vector<vector<double> > marginals;

	// run logistic model
	if (!strcasecmp(modelType, "LOGISTIC")) {
            svlCodeProfiler::tic(hLogisticInference);
            vector<double> m;
            for (int j = 0; j < (int)instance.Xn.size(); j++) {
                logisticModel.getMarginals(instance.Xn[j], m);
                marginals.push_back(m);
            }
            svlCodeProfiler::toc(hLogisticInference);
        }

	// run max-product inference
	if (!strcasecmp(modelType, "CRF")) {
	    svlCodeProfiler::tic(hCRFInference);
	    crfModel.inference(instance, marginals, 500, SVL_MP_ASYNCMAXPRODDIV);
	    svlCodeProfiler::toc(hCRFInference);
	}

        // compute max assignment
        vector<int> predictedLabels = argmaxs(marginals);

        // score image
        double imageScore = 0.0;
        if (bPixelwiseScore) {
            imageScore = scoreImagePixelwise(&evalImage, predictedLabels, confusion);
        } else {
            imageScore = scoreImageSegmentwise(instance.Yn, predictedLabels, confusion);
        }
        SVL_LOG(SVL_LOG_MESSAGE, "..." << imageScore);

        // show results
        if ((bVisualize) && (regionsFilename != NULL)) {
            IplImage *debugImage = evalImage.visualize();
            for (int i = 0; i < evalImage.numSegments(); i++) {
                for (int k = 0; k < (int)regionDefinitions.size(); k++) {
                    if (predictedLabels[i] == regionDefinitions[k].id) {
                        evalImage.colorSegment(debugImage, i, regionDefinitions[k].red,
                            regionDefinitions[k].green, regionDefinitions[k].blue);
                        break;
                    }
                }
            }
            evalImage.colorBoundaries(debugImage);
            cvShowImage(WINDOW_NAME, debugImage);
            cvWaitKey(0);
            cvReleaseImage(&debugImage);
        }

        // output results
        if (bOutputImages) {
            IplImage *outputImage = evalImage.visualize(); //cvCloneImage(evalImage.image());
            // color segments
            for (int i = 0; i < evalImage.numSegments(); i++) {
                for (int k = 0; k < (int)regionDefinitions.size(); k++) {
                    if (predictedLabels[i] == regionDefinitions[k].id) {
                        evalImage.colorSegment(outputImage, i, regionDefinitions[k].red,
                            regionDefinitions[k].green, regionDefinitions[k].blue, 0.6);
                        break;
                    }
                }
            }

            string outputFilename = string(outputDir) + string("/") + string(baseNames[n]);
            outputFilename += string(".results.png");
            cvSaveImage(outputFilename.c_str(), outputImage);
            cvReleaseImage(&outputImage);
        }

        // output scores
        if (bOutputScores) {
            string outputFilename = string(outputDir) + string("/") + string(baseNames[n]);
            outputFilename += string(".score.txt");
            ofstream ofs(outputFilename.c_str());
            assert(!ofs.fail());
            ofs << imageScore << endl;
            ofs.close();
        }

        // output marginals
        if (bOutputMarginals) {
            string outputFilename = string(outputDir) + string("/") + string(baseNames[n]);
            outputFilename += string(".marginal.txt");
            ofstream ofs(outputFilename.c_str());
            assert(!ofs.fail());
            for (int m = 0; m < (int)marginals.size(); m++) {
                for (int k = 0; k < numClasses; k++) {
                    ofs << " " << marginals[m][k];
                }
                ofs << "\n";
            }
            ofs.close();
        }

        // output models
        if (bOutputCRFModel && (!strcasecmp(modelType, "CRF"))) {
            string outputFilename = string(outputDir) + string("/") + string(baseNames[n]);
            outputFilename += string(".svlClusterGraph.xml");
            ofstream ofs(outputFilename.c_str());
            assert(!ofs.fail());
            crfModel.dumpClusterGraph(ofs, instance);
            ofs.close();
	}
    }
    SVL_LOG(SVL_LOG_VERBOSE, "...done");

    // show confusion matrix
    confusion.printCounts();
    confusion.printRowNormalized();

    cout << "Class counts:" << endl;
    for (int i = 0; i < numClasses; i++)
	cout << "\t" << confusion.rowSum(i);
    cout << endl << endl << "Accuracy: "  << confusion.accuracy() << endl << endl;

    if (bOutputConfusion) {
	string outputFilename = string(outputDir) + string("/confusion.txt");
	ofstream ofs(outputFilename.c_str());
	assert(!ofs.fail());
	confusion.printCounts(ofs);
	ofs.close();
    }

    // free memory
    cvDestroyAllWindows();
    svlCodeProfiler::toc(svlCodeProfiler::getHandle("main"));
    svlCodeProfiler::print(cerr);
    return 0;
}

// helper functions ---------------------------------------------------------

double scoreImagePixelwise(svlSegImage* testImage,
    const vector<int>& predictedLabels,
    svlConfusionMatrix& confusion)
{
        // score image
        double imageScore = 0.0;
        double totalPixels = 0.0;

        // pixel-wise accuracy
        assert(testImage != NULL);
        for (int i = 0; i < testImage->numSegments(); i++) {
            for (int y = 0; y < testImage->height(); y++) {
                for (int x = 0; x < testImage->width(); x++) {
                    int actual = testImage->getLabel(x, y);
                    if (actual < 0) continue;
                    int predicted = predictedLabels[testImage->getSegment(x, y)];
                    // accumulate confusion matrix
                    confusion.accumulate(actual, predicted);
                    imageScore += (actual == predicted ? 1.0 : 0.0);
                    totalPixels += 1.0;
                }
            }
        }

        if (totalPixels > 0.0)
            imageScore /= totalPixels;
        else imageScore = 1.0;

        return imageScore;
}

double scoreImageSegmentwise(const vector<int>& actualLabels,
    const vector<int>& predictedLabels,
    svlConfusionMatrix& confusion)
{
        // score image
        double imageScore = 0.0;
        double totalPixels = 0.0;

        // segment-wise accuracy
        confusion.accumulate(actualLabels, predictedLabels);
        for (int i = 0; i < (int)actualLabels.size(); i++) {
            int actual = actualLabels[i];
            if (actual < 0) continue;
            imageScore += (actual == predictedLabels[i] ? 1.0 : 0.0);
            totalPixels += 1.0;
        }

        if (totalPixels > 0.0)
            imageScore /= totalPixels;
        else imageScore = 1.0;

        return imageScore;
}
