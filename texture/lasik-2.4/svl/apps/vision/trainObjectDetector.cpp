/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    trainObjectDetector.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**              Olga Russakovsky <olga@cs.stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
** DESCRIPTION:
**   Trains an object classifier using OpenCV's machine learning library.
**
*****************************************************************************/

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sys/types.h>
#include <vector>

#if defined(_WIN32)||defined(WIN32)||defined(__WIN32__)
#include "win32/dirent.h"
#else
#include <dirent.h>
#endif

#include "cv.h"
#include "cxcore.h"
#include "highgui.h"

#include "svlBase.h"
#include "svlML.h"
#include "svlVision.h"

using namespace std;

// Main -----------------------------------------------------------------------

void usage()
{
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./trainObjectDetector [OPTIONS] <+ cache dir> <- cache dir>" << endl;
    cerr << "OPTIONS:" << endl
         << "  -i  <model>         :: evaluate input classifier model (for debugging)" << endl
	 << "  -pr <filename>      :: if already using -i flag, also output a pr curve" << endl
	 << "  -o <filename>       :: output classifier model" << endl
	 << "  -maxInstances <n>   :: maximum training images for each class (default: no limit)" << endl
	 << "  -shuffle            :: shuffles the training set (useful to make sure -cvn is a random subset)" << endl
	 << "  -binary             :: instead of the +/- cache directories just takes the" << endl
	 << "                         names of the binary files" << endl
         << "  -f                  :: uses F-score, rather than normed F-score" << endl 
	 << "  -c                  :: classifier types: BOOST (default), SVM, LOGISTIC" << endl
	 << SVL_STANDARD_OPTIONS_USAGE
	 << "OPTIONS FOR BOOSTED CLASSIFIER ONLY: " << endl
	 << "  -cv <+ cache dir> <- cache dir>  :: cross-validation examples" << endl
	 << "  -noretrain          :: doesn't re-train on full train+cv set" << endl
	 << "  -cvn <n>            :: performs n-fold cv to find optimal number of boosting rounds" << endl
	 << endl;
}

int main(int argc, char *argv[])
{


  // commented out to get reproduceable results for now
  //srand(time(NULL));

  captureOpenCVerrors();

    // read commandline parameters
    const int NUM_REQUIRED_PARAMETERS = 2;

    const char *inputFilename = NULL;
    const char * prFilename = NULL;
    const char *outputFilename = NULL;
    long int maxTrainingInstances = -1;
    const char *posCVcacheDir = NULL;
    const char *negCVcacheDir = NULL;
    bool noretrain = false;
    int kFold = -1;
    bool shuffle = false;
    bool binary = false;
    bool useRegularFScore = false;
    const char *classifierType = "BOOST";
    
    SVL_BEGIN_CMDLINE_PROCESSING(argc, argv)
      SVL_CMDLINE_STR_OPTION("-i", inputFilename)
      SVL_CMDLINE_STR_OPTION("-pr", prFilename)
      SVL_CMDLINE_STR_OPTION("-o", outputFilename)
      SVL_CMDLINE_INT_OPTION("-maxInstances", maxTrainingInstances)
      SVL_CMDLINE_OPTION_BEGIN("-cv", ptr)
        posCVcacheDir = ptr[0];
        negCVcacheDir = ptr[1]; 
      SVL_CMDLINE_OPTION_END(2)
      SVL_CMDLINE_BOOL_OPTION("-noretrain", noretrain)
      SVL_CMDLINE_INT_OPTION("-cvn", kFold)
      SVL_CMDLINE_BOOL_OPTION("-shuffle", shuffle)
      SVL_CMDLINE_BOOL_OPTION("-binary", binary)
      SVL_CMDLINE_BOOL_OPTION("-f", useRegularFScore)
      SVL_CMDLINE_STR_OPTION("-c", classifierType)
    SVL_END_CMDLINE_PROCESSING(usage());

    if (SVL_CMDLINE_ARGC != NUM_REQUIRED_PARAMETERS) {
        usage();
        return -1;
    }

    svlCodeProfiler::tic(svlCodeProfiler::getHandle("main"));

    bool retrain = !noretrain;
    bool useNormedFScore = !useRegularFScore;

    const char *posCacheDir = SVL_CMDLINE_ARGV[0];
    const char *negCacheDir = SVL_CMDLINE_ARGV[1];

    svlBinaryClassifier *classifier = NULL;


    if (!strcmp(classifierType, "BOOST"))
      {
	classifier = new svlBoostedClassifier();
      }
    else if (!strcmp(classifierType, "SVM"))
      {
	classifier = new svlSVM();
      }
    else if (!strcmp(classifierType, "LOGISTIC"))
      {
	classifier = new svlLogistic(); 
      }
    else
      SVL_ASSERT_MSG(false, "Unknown classifier type");

    
    SVL_ASSERT(classifier);

    // if the classifier is not a boosted classifier
    if (strcmp(classifierType, "BOOST")) {
      if (posCVcacheDir || negCVcacheDir || kFold != -1) {
	usage();
	SVL_LOG(SVL_LOG_FATAL, "Options incompatible with classifier type");
      }
    }

    if (maxTrainingInstances == -1)
      {
	SVL_LOG(SVL_LOG_WARNING, "trainObjectDetector: in v2.2beta, -maxInstances 1000 is no longer set by default. If you depended on this default, you must now set the flag manually.");
      }


    // load positive and negative training samples from cache
    SVL_LOG(SVL_LOG_VERBOSE, "Reading positive samples...");
    vector<vector<double> > posSamples = svlReadExamplesFromCache(posCacheDir, maxTrainingInstances, binary);
    SVL_LOG(SVL_LOG_VERBOSE, "..." << posSamples.size() << " samples read");
    SVL_LOG(SVL_LOG_VERBOSE, "Reading negative samples...");
    vector<vector<double> > negSamples = svlReadExamplesFromCache(negCacheDir, maxTrainingInstances, binary);
    SVL_LOG(SVL_LOG_VERBOSE, "..." << negSamples.size() << " samples read");

    // load positive and negative cv samples from cache (unlimited in number)
    vector<vector<double> > posCVsamples;
    vector<vector<double> > negCVsamples;
    if (posCVcacheDir) {
      SVL_LOG(SVL_LOG_VERBOSE, "Reading positive cv samples...");
      posCVsamples = svlReadExamplesFromCache(posCVcacheDir, INT_MAX, binary);
      SVL_LOG(SVL_LOG_VERBOSE, "..." << posCVsamples.size() << " samples read");
    }
    if (negCVcacheDir) {
      SVL_LOG(SVL_LOG_VERBOSE, "Reading negative cv samples...");
      negCVsamples = svlReadExamplesFromCache(negCVcacheDir, INT_MAX, binary);
      SVL_LOG(SVL_LOG_VERBOSE, "..." << negCVsamples.size() << " samples read");
    }

    if (shuffle) {
      random_shuffle(negSamples.begin(), negSamples.end());
      random_shuffle(posSamples.begin(), posSamples.end());
      random_shuffle(negCVsamples.begin(), negCVsamples.end());
      random_shuffle(posCVsamples.begin(), posCVsamples.end());
    }

    if (prFilename != NULL)
      assert(inputFilename != NULL);

    // evaluate model
    if (inputFilename != NULL)
    {
      delete classifier;
      classifier = dynamic_cast<svlBinaryClassifier *>(svlClassifierFactory().load(inputFilename));
      SVL_ASSERT_MSG(classifier, "Supplied classifier from " << inputFilename << " is not a binary classifier");

      if (prFilename) {
	SVL_LOG(SVL_LOG_FATAL, "Not implemented yet!");
	//writePRcurve(prFilename, classifier->getPRcurve(posSamples, negSamples));
      }
      else
	classifier->validate(posSamples, negSamples, "", false);

      if (outputFilename == NULL) {
	delete classifier;
	return 0;
      }
    }

    SVL_LOG(SVL_LOG_VERBOSE, "Training classifier...");

    if (!useNormedFScore)
      classifier->useRealFScoreByDefault();

    if (posCVsamples.size() > 0 && negCVsamples.size() > 0) {
      (dynamic_cast<svlBoostedClassifier *>(classifier))->trainCV(posSamples, negSamples, posCVsamples, negCVsamples, retrain);
    } else if (kFold > 0) {
      (dynamic_cast<svlBoostedClassifier *>(classifier))->trainFullCV(posSamples, negSamples, kFold);
    } else {
      if (!strcmp(classifierType, "LOGISTIC"))
	//(dynamic_cast<svlLogistic *>(classifier))->initialize(posSamples[0].size());
      (dynamic_cast<svlLogistic *>(classifier))->initialize(posSamples[0].size(), 1.0, 1.0e-6, true);
      classifier->train(posSamples, negSamples);
    }

    if (outputFilename != NULL) {
        SVL_LOG(SVL_LOG_VERBOSE, "Writing model to file " << outputFilename);
        classifier->save(outputFilename);
    }


    // free memory
    delete classifier;

    svlCodeProfiler::toc(svlCodeProfiler::getHandle("main"));
    svlCodeProfiler::print(cerr);
    return 0;
}

