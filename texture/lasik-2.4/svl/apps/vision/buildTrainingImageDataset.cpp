/****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    buildTrainingImageDataset.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
**              Olga Russakovsky <olga@cs.stanford.edu>
** DESCRIPTION:
**  Constructs a database of positive and negative training images for
**  training an object detector.
**
*****************************************************************************/

#include <cstdlib>
#include <string>
#include <limits>
#include <iomanip>
#include <map>
#include <set>

// OpenCV library
#include "cv.h"
#include "cxcore.h"
#include "highgui.h"

// stair vision library
#include "svlBase.h"
#include "svlML.h"
#include "svlVision.h"

using namespace std;

void usage()
{
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./buildTrainingImageDataset [OPTIONS] <imageSequence> <objectLabels>?" << endl;
    cerr << "OPTIONS:" << endl
	 << "  -parallel <id> <ct>  :: used for distributed jobs on the cluster." << endl
	 << SVL_STANDARD_OPTIONS_USAGE
	 << "Abreviations (use instead of-set svlVision.svlTrainingDatasetBuilder ...):" << endl
	 << " *** do not use when providing a config file ***" << endl
	 << "  -o <name>          <=> objects <name> " << endl
	 << "  -ovThr <n>         <=> overlapThreshold <n> " << endl
	 << "  -texture           <=> texture true" << endl
	 << "  -allWindows        <=> allWindows true" << endl
	 << "  -baseScaleOnly     <=> baseScaleOnly true" << endl
	 << "  -skipPos           <=> skipPos true" << endl
	 << "  -posBest           <=> posBestWindow true" << endl
	 << "  -skipNeg           <=> skipNeg true" << endl
	 << "  -fpOnly            <=> falsePositivesOnly true" << endl
	 << "  -detections <file> <=> detectionsFilename <filename>" << endl
	 << "  -t <n>             <=> threshold <n>" << endl
	 << "  -includeOther      <=> includeOtherObjects true" << endl
	 << "  -n <n>             <=> numNegs <n>" << endl
	 << "  -negHeight <n>     <=> negHeight <n>" << endl
	 << "  -negRatio <n>      <=> negAspectRatio <n>" << endl
	 << "  -baseDir <name>    <=> baseDir <dir>" << endl
	 << "  -useWinRefs        <=> useWinRefs true" << endl
	 << "  -createDirs        <=> createDirs true" << endl
	 << "  -negName <name>    <=> negSubdirName <name>" << endl
	 << "  -posPrefix <name>  <=> posPrefix <name>" << endl
	 << "  -negPrefix <name>  <=> negPrefix <name>" << endl
	 << "  -includeFlipped    <=> includeFlipped true" << endl
	 << "  -resize <w> <h>    <=> resizeWidth <w> ... resizeHeight <h>" << endl
	 << endl;
}


// baseExt is deprecated; will just strip off the extension of every file in the image sequence
// and replace it with the default channel extension
// will crop out color images by default; 

// Main ----------------------------------------------------------------------

int main(int argc, char *argv[])
{
  captureOpenCVerrors();

  // read command line parameters
  int parallelId = -1;
  int parallelCount = -1;

  const int NUM_REQUIRED_PARAMETERS = 1;

  string newNameForDepr;

  SVL_BEGIN_CMDLINE_PROCESSING(argc, argv)
    SVL_CMDLINE_OPTION_BEGIN("-parallel", ptr)
      parallelId = atoi(ptr[0]);
      parallelCount = atoi(ptr[1]);
      if (parallelId < 0 || parallelId >= parallelCount) {
        SVL_LOG(SVL_LOG_FATAL, "illegal id number " << parallelId);
      } else if (parallelCount < 0) {
        SVL_LOG(SVL_LOG_FATAL, "illegal job count " << parallelCount);
      }
    SVL_CMDLINE_OPTION_END(2)
    SVL_CMDLINE_OPTION_BEGIN("-o", ptr)
      svlTrainingDatasetBuilder::OBJECTS.insert(ptr[0]);
    SVL_CMDLINE_OPTION_END(1)
    SVL_CMDLINE_REAL_OPTION("-ovThr", svlTrainingDatasetBuilder::OVERLAP_THRESHOLD)
    SVL_CMDLINE_BOOL_OPTION("-texture", svlTrainingDatasetBuilder::TEXTURE)
    SVL_CMDLINE_BOOL_OPTION("-allWindows", svlTrainingDatasetBuilder::ALL_WINDOWS)
    SVL_CMDLINE_BOOL_OPTION("-baseScaleOnly", svlTrainingDatasetBuilder::BASE_SCALE_ONLY)
    SVL_CMDLINE_BOOL_OPTION("-skipPos", svlTrainingDatasetBuilder::SKIP_POS)
    SVL_CMDLINE_BOOL_OPTION("-posBest", svlTrainingDatasetBuilder::POS_BEST_WINDOW)
    SVL_CMDLINE_BOOL_OPTION("-skipNeg", svlTrainingDatasetBuilder::SKIP_NEG)
    SVL_CMDLINE_BOOL_OPTION("-fpOnly", svlTrainingDatasetBuilder::FPOS_ONLY)
    SVL_CMDLINE_STR_OPTION("-detections", svlTrainingDatasetBuilder::DETECTIONS_FILE)
    SVL_CMDLINE_REAL_OPTION("-t", svlTrainingDatasetBuilder::THRESHOLD)
    SVL_CMDLINE_BOOL_OPTION("-includeOther", svlTrainingDatasetBuilder::INCLUDE_OTHER_OBJS)
    SVL_CMDLINE_INT_OPTION("-n", svlTrainingDatasetBuilder::NUM_NEG)
    SVL_CMDLINE_INT_OPTION("-negHeight", svlTrainingDatasetBuilder::NEG_HEIGHT)
    SVL_CMDLINE_REAL_OPTION("-negRatio", svlTrainingDatasetBuilder::NEG_ASPECT_RATIO)
    SVL_CMDLINE_STR_OPTION("-baseDir", svlTrainingDatasetBuilder::BASE_DIR)
    SVL_CMDLINE_BOOL_OPTION("-useWinRefs", svlTrainingDatasetBuilder::USE_WIN_REFS)
    SVL_CMDLINE_BOOL_OPTION("-createDirs", svlTrainingDatasetBuilder::CREATE_DIRS)
    SVL_CMDLINE_STR_OPTION("-negName", svlTrainingDatasetBuilder::NEG_SUBDIR_NAME)
    SVL_CMDLINE_STR_OPTION("-posPrefix", svlTrainingDatasetBuilder::POS_PREFIX)
    SVL_CMDLINE_STR_OPTION("-negPrefix", svlTrainingDatasetBuilder::NEG_PREFIX)
    SVL_CMDLINE_BOOL_OPTION("-includeFlipped", svlTrainingDatasetBuilder::INCLUDE_FLIPPED)
    SVL_CMDLINE_OPTION_BEGIN("-resize", ptr)
      svlTrainingDatasetBuilder::RESIZE_WIDTH = atoi(ptr[0]);
      svlTrainingDatasetBuilder::RESIZE_HEIGHT = atoi(ptr[1]);
    SVL_CMDLINE_OPTION_END(2)
  SVL_END_CMDLINE_PROCESSING(usage())
      
  if (SVL_CMDLINE_ARGC < NUM_REQUIRED_PARAMETERS) {
      usage();
      return -1;
    }

  svlCodeProfiler::tic(svlCodeProfiler::getHandle("buildTrainingImageDataset"));

  const char *imageSequenceFile = SVL_CMDLINE_ARGV[0];
  const char *objectLabelsFile = NULL;
  if (SVL_CMDLINE_ARGC > NUM_REQUIRED_PARAMETERS) 
    objectLabelsFile = SVL_CMDLINE_ARGV[1];

  // encapsulates all the different parameters in a cleaner way
  svlTrainingDatasetBuilder builder(imageSequenceFile, objectLabelsFile);
  builder.writeDataset(parallelCount, parallelId);
  
  svlCodeProfiler::toc(svlCodeProfiler::getHandle("buildTrainingImageDataset"));
  svlCodeProfiler::print(cerr);

  return 0;
}
