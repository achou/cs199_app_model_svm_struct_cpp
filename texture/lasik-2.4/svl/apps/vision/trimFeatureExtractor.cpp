/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    trimFeatureExtractor.cpp
** AUTHOR(S):  Ian Goodfellow <ia3n@cs.stanford.edu>
**                
**            
** DESCRIPTION:
**   Removes features that are not used by the boosted detector.
**
*****************************************************************************/

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sys/types.h>
#include <vector>

#if defined(_WIN32)||defined(WIN32)||defined(__WIN32__)
#include "win32/dirent.h"
#else
#include <dirent.h>
#endif

#include "cv.h"
#include "cxcore.h"
#include "highgui.h"

#include "svlBase.h"
#include "svlML.h"
#include "svlVision.h"

using namespace std;

// Prototypes -----------------------------------------------------------------

vector<vector<double> > readFromCache(const char* dirName, int maxImages);

// Main -----------------------------------------------------------------------

void usage()
{
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./trimFeatureExtractor [required options]" << endl;
    cerr << "OPTIONS:" << endl
	 << "  -ie <filename>      :: input filename for the existing extractor" << endl
	 << "  -oe <filename>      :: output filename for the new, trimmed extractor" << endl
	 << "  -m <filename>      :: input filename for the existing model" << endl
	 << endl;
}

int main(int argc, char *argv[])
{
    // read commandline parameters


    const char * outputExtractor = NULL;
    const char * inputExtractor = NULL;

    const char * inputModel = NULL;


    char **args = argv + 1;
    while (--argc) {
      if (!strcmp(*args, "-oe"))
	{
	  outputExtractor = *(++args);
	  argc--;
	} 
      else if (!strcmp(*args, "-ie"))
	{
	  inputExtractor = *(++args);
	  argc--;
	} 
      else if (!strcmp(*args, "-m"))
	{
	  inputModel = *(++args);
	  argc--;
	} 
      else {
	usage();
	SVL_LOG(SVL_LOG_FATAL, "unrecognized option " << *args);
      }
      args++;
    }

    if (!inputModel ||  !inputExtractor || !outputExtractor)
      {
	usage();
      SVL_LOG(SVL_LOG_FATAL, "Missing a required argument.");
      }

    svlBoostedClassifier *c = new svlBoostedClassifier(inputModel);

    const CvMat * active = c->getActiveVars();
    if (!active) return 0;

    svlFeatureExtractor *e = svlFeatureExtractorFactory().load(inputExtractor);
    assert(e);

    vector<bool> used(e->numFeatures());

    for (int i = 0; i < active->cols; i++)
      used[CV_MAT_ELEM(*active,int,0,i)] = true;

    cout << "Pruned extractor from "<<e->numFeatures()<< "features to ";

    //trim extractor
    svlFeatureExtractor * trimmed = e->getPrunedExtractor(used);
    delete e;

    cout << trimmed->numFeatures() << " features." << endl;

    trimmed->write(outputExtractor);

    delete trimmed;
    return 0;
}








