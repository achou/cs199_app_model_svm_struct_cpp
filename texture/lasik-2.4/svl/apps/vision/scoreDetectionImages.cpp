/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2009, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    scoreDetectionImages.cpp
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
** DESCRIPTION:
**   Given a groundtruth XML file and images corresponding to sliding windows
** classifier output, computes the precision and recall.
*****************************************************************************/

#include <cstdlib>
#include <string>
#include <limits>
#include <iomanip>
#include <fstream>
#include <map>
#include <vector>

#include "svlBase.h"
#include "svlVision.h"

using namespace std;

// type definitions and function prototypes ----------------------------------
void usage()
{
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./scoreDetectionImages [OPTIONS] <groundtruth xml> <image seq> <folder of detections>\n";
    cerr << "OPTIONS:\n"
	 << "  -threshold <num>  :: threshold for positive classification (default 0.5)\n"
	 << "  -pr <filestem>    :: generate precision-recall curves\n"
	 << "  -o <filestem>     :: output detections (true and false)\n"
	 << "  -object <name>    :: specifies one object of interest\n"
	 << "  -w <n>            :: if old caching type, w of det. window\n"
	 << "  -h <n>            :: if old caching type, h of det. window\n"
      //	 << "  -shift <n>        :: sliding windows shift for evaluation (default: 4)\n"
         << SVL_STANDARD_OPTIONS_USAGE
	 << endl;
}

#define MAX_LEVELS 50

// main ----------------------------------------------------------------------

int main(int argc, char *argv[])
{
    // read commandline parameters
    const int NUM_REQUIRED_PARAMETERS = 3;

    double threshold = 0.5;
    const char *prFilestem = NULL;
    const char *outputFilestem = NULL;
    const char *object = NULL;
    int input_w = -1;
    int input_h = -1;

    SVL_BEGIN_CMDLINE_PROCESSING(argc, argv)
      SVL_CMDLINE_REAL_OPTION("-threshold", threshold)
      SVL_CMDLINE_STR_OPTION("-pr", prFilestem)
      SVL_CMDLINE_STR_OPTION("-o", outputFilestem)
      SVL_CMDLINE_STR_OPTION("-object", object)
      SVL_CMDLINE_INT_OPTION("-w", input_w)
      SVL_CMDLINE_INT_OPTION("-h", input_h)
    SVL_END_CMDLINE_PROCESSING(usage())
    
    if (SVL_CMDLINE_ARGC < NUM_REQUIRED_PARAMETERS) {
	usage();
	return -1;
    }

    // Accumulate counts per object class, e.g. objectScores["mug"]
    // holds the score of every mug detection and whether it is a
    // true positive (1) or not (0).

      svlObjectDetectionAnalyzer *d;
      if (object == NULL) {
	d = new svlObjectDetectionAnalyzer();
      } else {
	d = new svlObjectDetectionAnalyzer(string(object));
      }

      const char *gtFilename = SVL_CMDLINE_ARGV[0];
      const char *seqFilename = SVL_CMDLINE_ARGV[1];
      const char *detectionsFolder = SVL_CMDLINE_ARGV[2];

      SVL_LOG(SVL_LOG_VERBOSE, "Reading from " << gtFilename << " and " << seqFilename);
      
      if (!d->loadGt(gtFilename)) return -1;

      svlObject2dSequence allObjects;
      
      SVL_LOG(SVL_LOG_VERBOSE, "Loading detections...");
      svlImageSequence seq;
      seq.load(seqFilename);

      for (unsigned i = 0; i < seq.numImages(); i++) {
	SVL_LOG(SVL_LOG_MESSAGE, "Processing image " << i);
	string baseName = strBaseName(seq[i]);

	vector<IplImage *> images;
	for (unsigned j = 0; j < MAX_LEVELS; j++) {
	  stringstream filename;
	  filename << string(detectionsFolder) << "/" << baseName << "." << j << ".ppm";
	  IplImage *img = cvLoadImage(filename.str().c_str(), CV_LOAD_IMAGE_GRAYSCALE);
	  if (img == NULL) break;
	  images.push_back(img);
	}

	SWcacheInfo info(string(detectionsFolder), baseName);
	int baseWidth = info.img_w;
	int baseHeight = info.img_h;
	int shift_x = info.delta_x;
	int shift_y = info.delta_y;
	float delta_scale = info.delta_scale;
	int w = info.w;
	int h = info.h;
	bool center = info.center;

	// old way of getting it out
	if (baseWidth < 0) {
	  for (unsigned j = 0; j < images.size(); j++) {
	    baseWidth = max(baseWidth, images[j]->width);
	    baseHeight = max(baseHeight, images[j]->height);
	  }
	}
	if (w < 0) {
	  w = input_w;
	  h = input_h;
	}

	svlObject2dFrame objects;
	objects.reserve(baseWidth*baseHeight*images.size()/(shift_x*shift_y));
	float scale = 1.0;
	for (unsigned j = 0; j < images.size(); j++) {
	  IplImage *img = images[j];
	  
	  // if not provided, do the old way to approximate
	  if (delta_scale <= 0) {
	    unsigned curWidth = img->width*shift_x;
	    scale = (float)baseWidth / (float)curWidth;
	  }

	  for (int y = 0; y < img->height; y ++) {
	    for (int x = 0; x < img->width; x ++) {
	      float pr =(float)(CV_IMAGE_ELEM(img, uchar, y, x)) / 255.0; 

	      if (pr > threshold) {
		svlObject2d obj;
		if (center) {
		  obj = svlObject2d(max(x*shift_x - w/2, 0),
				    max(y*shift_y - h/2, 0), 
				    w, h, pr);
		} else {
		  obj = svlObject2d(x*shift_x, y*shift_y, w, h, pr);
		}
		obj.scale(scale, scale);
		obj.name = object ? string(object) : "";
		objects.push_back(obj);
	      }
	    }
	  }
	  cvReleaseImage(&img);
	  // if delta_scale is provided, get an accurate scale
	  if (delta_scale > 0) {
	    scale *= delta_scale;
	  }
	}

	if (outputFilestem)
	  allObjects[baseName] = objects;

	d->accumulateScoresForFrame(baseName, objects);
	//d->quantizeScores(); // no need, since already quantized to 255 bins essentially

      }

      if (outputFilestem) {
	allObjects.write(outputFilestem);
      }

    // show results
    d->printResults((float)threshold);

    // construct PR curve
    if (prFilestem != NULL) {
	SVL_LOG(SVL_LOG_VERBOSE, "Generating precision-recall curves...");
	d->writePRCurves(prFilestem);
	SVL_LOG(SVL_LOG_VERBOSE, "...done");
    }

    delete d;

    return 0;
}


