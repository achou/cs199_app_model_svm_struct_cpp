/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    trainClassifierPipeline.cpp
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
** DESCRIPTION:
**   Trains an object classifier using OpenCV's machine learning library.
**   Reads in all the positive and negative images, then for each trial
**     (1) Shuffles them and takes the first numPos and numNeg for training
**     (2) Builds a dictionary of features
**     (3) Trains a boosted detector for the desired number of rounds
**     (4) Evaluates on the remaining examples
**   Outputs the evaluation results averaged over all the testing examples
**   Potentially outputs the last model it trained   
**
*****************************************************************************/

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sys/types.h>
#include <vector>

#if defined(_WIN32)||defined(WIN32)||defined(__WIN32__)
#include "win32/dirent.h"
#else
#include <dirent.h>
#endif

#include "cv.h"
#include "cxcore.h"
#include "highgui.h"

#include "svlBase.h"
#include "svlML.h"
#include "svlVision.h"

using namespace std;

#define NUM_THREADS 8

// Thread args
struct ComputeFeaturesArgs {
  ComputeFeaturesArgs(const svlFeatureExtractor * dict, 
		      const vector<IplImage *> *im,
		      vector<double> *r, vector<bool> *uf = NULL) :
    dictionary(dict), images(im), results(r), usedFeatures(uf) {}

  const svlFeatureExtractor * dictionary;
  const vector<IplImage *> *images;
  vector<double> *results;
  const vector<bool> *usedFeatures;
};

// Thread function
void *computeFeatures(void *voidArgs, unsigned tid) {
  //Copy arguments out of the helper struct
  ComputeFeaturesArgs *args = (ComputeFeaturesArgs*) voidArgs;
              
  // process image
  args->dictionary->windowResponse(*(args->images),
				   *(args->results),
				   0,
				   args->usedFeatures);
 
  delete args;
  return NULL;
}



// Main -----------------------------------------------------------------------

void usage()
{
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./trainClassifierPipeline [OPTIONS] <width> <height> <+ image dir> <- image dir>+" << endl;
    cerr << "OPTIONS:" << endl
	 << "  -n <num features>   :: total num features (default: 10 per pos. image)" << endl
    	 << "  -numPos <n>         :: num positive training images (default: 100)" << endl
	 << "  -numNeg <n>         :: num negative training images (default: 100)" << endl
	 << "  -maxPos <n>         :: maximum positive images to read for both train and test (default: 1000)" << endl
	 << "  -maxNeg <n>         :: maximum negative images to read for both train and test (default: 1000)" << endl
	 << "  -numTrials <n>      :: number of random runs of training to average over (default: 1)" << endl
	 << "  -o <filename>       :: outputs the last trained model" << endl
	 << SVL_STANDARD_OPTIONS_USAGE 
	 << endl;
}

int main(int argc, char *argv[])
{

  captureOpenCVerrors();

    srand(time(NULL));

    // read commandline parameters
    const int NUM_REQUIRED_PARAMETERS = 4;

    int numFeatures = -1;
    int numPos = 100;
    int numNeg = 100;
    int maxPos = 1000;
    int maxNeg = 1000;
    int numTrials = 1;

    const char *outputFilename = NULL;    

    SVL_BEGIN_CMDLINE_PROCESSING(argc, argv)
      SVL_CMDLINE_INT_OPTION("-n", numFeatures)
      SVL_CMDLINE_INT_OPTION("-numPos", numPos)
      SVL_CMDLINE_INT_OPTION("-numNeg", numNeg)
      SVL_CMDLINE_INT_OPTION("-maxPos", maxPos)
      SVL_CMDLINE_INT_OPTION("-maxNeg", maxNeg)
      SVL_CMDLINE_INT_OPTION("-numTrials", numTrials)
      SVL_CMDLINE_STR_OPTION("-o", outputFilename)
    SVL_END_CMDLINE_PROCESSING(usage());
    
    if (SVL_CMDLINE_ARGC < NUM_REQUIRED_PARAMETERS) {
      usage();
      return -1;
    }

    svlCodeProfiler::tic(svlCodeProfiler::getHandle("main"));

    int width = atoi(SVL_CMDLINE_ARGV[0]);
    int height = atoi(SVL_CMDLINE_ARGV[1]);

    // read and resize images
    svlImageLoader loader;
    loader.setResizeSize(width, height);

    vector<vector<IplImage *> > posImages;
    loader.readDir(SVL_CMDLINE_ARGV[2]);
    loader.getAllFrames(posImages, maxPos);

    vector<vector<IplImage *> > negImages;
    vector<vector<IplImage *> > tmp;
    for (int i = 3; i < argc; i++) {
      loader.readDir(SVL_CMDLINE_ARGV[i]);
      loader.getAllFrames(tmp, maxNeg - negImages.size());
      negImages.insert(negImages.end(), tmp.begin(), tmp.end());
    }

    // numPos and numNeg are the actual numbers of training examples that will be used
    if ((int)posImages.size() < numPos)
      numPos = posImages.size();
    if ((int)negImages.size() < numNeg)
      numNeg = negImages.size();

    int numFeaturesPerImage = 10;
    if (numFeatures > 0) {
      numFeaturesPerImage = (int)ceil(numFeatures / (numPos * loader.numChannels()));
    }

    // define classifier type
    svlBoostedClassifier *classifier = new svlBoostedClassifier();

    // for evaluation of average results
    int tp = 0;
    int fp = 0;

    // run through randomized trials
    for (int i = 0; i < numTrials; i++) {
      
      // (1) Choose the examples to use (first numPos and numNeg will be the training set)
      random_shuffle(posImages.begin(), posImages.end());
      random_shuffle(negImages.begin(), negImages.end());

      // (2) Build dictionary

      SVL_LOG(SVL_LOG_MESSAGE, "Building dictionary...");

      svlPatchDictionary dictionary(width, height);
      
      vector<vector<IplImage *> > posTrainImages;
      posTrainImages.insert(posTrainImages.end(),
			    posImages.begin(), 
			    posImages.begin() + numPos);

      vector<svlPatchDefinitionType> patchTypes;
      
      //Store svl enumerations for each of the channels
      for( unsigned i=0 ; i < loader.numChannels(); i++) {
        //Map to enumeration
	svlPatchDefinitionType patchEnumType = loader.patchTypes(i);
	patchTypes.push_back(patchEnumType);
      }
      assert(patchTypes.size() == posTrainImages[0].size());
      
      dictionary.buildDictionary(posTrainImages, patchTypes,
				 numFeaturesPerImage);
	
      if (numFeatures > 0 && (int)dictionary.numFeatures() > numFeatures) {
	dictionary.truncate(numFeatures);
      }
      SVL_LOG(SVL_LOG_MESSAGE, "... with " << dictionary.numFeatures() << " entries");


      // (3) Convert the images to feature vectors
      
      // TODO: compute feature vectors only on the positive examples, and then 
      // only compute the features chosen by the boosting model for the negatives

      SVL_LOG(SVL_LOG_MESSAGE, "Computing feature vectors...");

      vector<vector<double> > posTrain(numPos);
      vector<vector<double> > negTrain(numNeg);
      
      svlThreadPool threadPool(NUM_THREADS);
      threadPool.start();

      for (unsigned i = 0; i < posTrain.size(); i++)
	threadPool.addJob(computeFeatures, 
			  new ComputeFeaturesArgs(&dictionary, &(posImages[i]), &(posTrain[i])));
      for (unsigned i = 0; i < negTrain.size(); i++)
	threadPool.addJob(computeFeatures, 
			  new ComputeFeaturesArgs(&dictionary, &(negImages[i]), &(negTrain[i])));

      threadPool.finish();

      SVL_LOG(SVL_LOG_MESSAGE, "...done");

      // (3) Train boosted detector

      SVL_LOG(SVL_LOG_MESSAGE, "Training classifier...");
      classifier->train(posTrain, negTrain);

      // (4) Evaluate the results

      vector<vector<double> > posTest(posImages.size() - numPos);
      vector<vector<double> > negTest(negImages.size() - numNeg);

      // [[TODO: revive the fast way]]

      SVL_LOG(SVL_LOG_MESSAGE, "Evaluating the slow way...");

      threadPool.start();

      for (unsigned i = 0; i < posTest.size(); i++)
	threadPool.addJob(computeFeatures, 
			  new ComputeFeaturesArgs(&dictionary, &(posImages[numPos + i]), &(posTest[i])));
      
      for (unsigned i = 0; i < negTest.size(); i++)
	threadPool.addJob(computeFeatures, 
			  new ComputeFeaturesArgs(&dictionary, &(negImages[numNeg + i]), &(negTest[i])));

      threadPool.finish();

      classifier->validate(posTest, negTest, "on test", true);

      /*
      SVL_LOG(SVL_LOG_MESSAGE, "Evaluating the fast way...");
      

      SVL_LOG(SVL_LOG_MESSAGE, "Evaluating...");

      vector<bool> usedFeatures = classifier->getUsedFeaturesList(dictionary.numFeatures());

      threadPool.start();

      for (unsigned i = 0; i < posTest.size(); i++)
	threadPool.addJob(computeFeatures, 
			  new ComputeFeaturesArgs(&dictionary, &(posImages[numPos + i]), &(posTest[i]), &usedFeatures));
      
      for (unsigned i = 0; i < negTest.size(); i++)
	threadPool.addJob(computeFeatures, 
			  new ComputeFeaturesArgs(&dictionary, &(negImages[numNeg + i]), &(negTest[i]), &usedFeatures));

      threadPool.finish();
      
      classifier->modelPerformance(posTest, negTest, "on test");
      */

      tp += classifier->numClassifiedAsPositive(posTest);
      fp += classifier->numClassifiedAsPositive(negTest);
    }

    int numPosTest = (posImages.size() - numPos) * numTrials;
    int numNegTest = (negImages.size() - numNeg) * numTrials;

    svlBinaryClassifierStats stats;
    stats.tp = tp;
    stats.fn = numPosTest - tp;
    stats.fp = fp;
    stats.tn = numNegTest - fp;

    float FscoreNormed = stats.computeFscore(true);
    float Fscore = stats.computeFscore(false);
    
    stats.tp /= numTrials;
    stats.fn /= numTrials;
    stats.fp /= numTrials;
    stats.tn /= numTrials;

    SVL_LOG(SVL_LOG_MESSAGE, "After " << numTrials << " trial(s), average "
	    << "(TP, FN, TN, FP, \"F\" score, normed \"F\" score): "
	    << stats.tp << ", " << stats.fn << ", "
	    << stats.tn << ", " << stats.fp << ", "
	    << Fscore << ", " << FscoreNormed);

    //if (outputFilename != NULL) {
    //  SVL_LOG(SVL_LOG_VERBOSE, "Writing model to file " << outputFilename);
    //  classifier->writeModel(outputFilename);
    //}

    // free memory
    delete classifier;

    for (unsigned i = 0; i < posImages.size(); i++) {
      for (unsigned j = 0; j < posImages[i].size(); j++) {
	cvReleaseImage(&posImages[i][j]);
      }
    }

    for (unsigned i = 0; i < negImages.size(); i++) {
      for (unsigned j = 0; j < negImages[i].size(); j++) {
	cvReleaseImage(&negImages[i][j]);
      }
    }

    return 0;
}



