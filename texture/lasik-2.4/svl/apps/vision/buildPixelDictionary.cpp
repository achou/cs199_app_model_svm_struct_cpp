/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    buildPixelDictionary
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
** DESCRIPTION:
**  Build dictionary for object recognition by using vector-quantized
** pixel descriptors.
**
*****************************************************************************/

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <limits>
#include <string>

#include <sys/types.h>
#if defined(_WIN32)||defined(WIN32)||defined(__WIN32__)
#include "win32/dirent.h"
#else
#include <dirent.h>
#endif

#include "cv.h"
#include "cxcore.h"
#include "highgui.h"

#include "svlBase.h"
#include "svlVision.h"

using namespace std;

#define TOTAL_THREADS 8

void *CreateDictionary(void *argsP, unsigned tid);

struct CreateDictionaryArgs {
  CreateDictionaryArgs(const char *isf, int k, svlPixelDictionary *d) :
    imageSeqFile(isf), num_k(k), dict(d) {}

  const char* imageSeqFile;
  int num_k;
  svlPixelDictionary *dict;
};

void usage()
{
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./buildPixelDictionary [OPTIONS] (<imageSeq> k)*" << endl;
    cerr << "OPTIONS:" << endl
	 << "  -o <filename>      :: output dictionary file" << endl
	 << "  -n <num>           :: number of pixels per image (default: 10)" << endl
	 << "  -t <threshold>     :: Harris Laplace threshold (overrides -n)" << endl
	 << "  -w <width>         :: width of dictionary window (default: 1st image)" << endl
	 << "  -h <height>        :: height of dictionary window (default: 1st image)" << endl
	 << "  -rift              :: include rift features" << endl
	 << "  -spin              :: include spin image features" << endl
	 << "  -texton            :: include texton features" << endl
	 << "  -all               :: include all features" << endl
	 << "  -noclustering      :: dictionary is based on raw features" << endl
	 << SVL_STANDARD_OPTIONS_USAGE
	 << " Note: the training images are not resized by default" << endl
	 << endl;
}

int main(int argc, char *argv[])
{  
  captureOpenCVerrors();

  // read commandline parameters
  const int NUM_REQUIRED_PARAMETERS = 2;

  const char *outputFilename = NULL;
  int numPatchesPerImage = 10;
  float cornernessThreshold = INT_MIN;
  int width = -1;
  int height = -1;
  bool rift = false;
  bool spin = false;
  bool texton = false;
  bool noclustering = false;
 
  SVL_BEGIN_CMDLINE_PROCESSING(argc, argv)
    SVL_CMDLINE_STR_OPTION("-o", outputFilename)
    SVL_CMDLINE_INT_OPTION("-n", numPatchesPerImage)
    SVL_CMDLINE_REAL_OPTION("-t", cornernessThreshold)
    SVL_CMDLINE_INT_OPTION("-w", width)
    SVL_CMDLINE_INT_OPTION("-h", height)
    SVL_CMDLINE_BOOL_OPTION("-rift", rift)
    SVL_CMDLINE_BOOL_OPTION("-spin", spin)
    SVL_CMDLINE_BOOL_OPTION("-texton", texton)
    SVL_CMDLINE_OPTION_BEGIN("-all", p)
      rift = true;
      spin = true;
      texton = true;
    SVL_CMDLINE_OPTION_END(0)
    SVL_CMDLINE_BOOL_OPTION("-noclustering", noclustering)
  SVL_END_CMDLINE_PROCESSING(usage());

  svlPixelDictionary dict(width, height);
  if (cornernessThreshold > INT_MIN) {
    svlHarrisLaplaceDetector det(cornernessThreshold);
    dict.setIPdetector(&det);      
  } else {
    dict.setNumPointsPerImage(numPatchesPerImage);
  }
  if (rift) {
    svlRIFTdescriptor r;
    dict.addDescriptor(&r);
  } 
  if (spin) {
    svlSpinDescriptor s;
    dict.addDescriptor(&s);
  } 
  if (rift) {
    svlTextonExtractor t;
    dict.addDescriptor(&t);
  } 
  
  // no training data provided;
  if (noclustering) {
    if (SVL_CMDLINE_ARGC > 0) {
      SVL_LOG(SVL_LOG_WARNING, "Training data is ignored since no clustering is being performed");
    }

    if (outputFilename)
      dict.write(outputFilename);
    return 0;
  }
  
  // invariant: clustering is requested
  if (SVL_CMDLINE_ARGC < NUM_REQUIRED_PARAMETERS || SVL_CMDLINE_ARGC%2 != 0) {
    usage();
    return -1;
  }

  vector<svlPixelDictionary *> dictionaries;
  svlThreadPool vThreadPool(TOTAL_THREADS);
  vThreadPool.start();
  for (int i = 0; i < SVL_CMDLINE_ARGC; i+=2) {
    // set up the dictionary
    svlPixelDictionary *d = new svlPixelDictionary(dict);
    dictionaries.push_back(d);
    CreateDictionaryArgs *args = new CreateDictionaryArgs(SVL_CMDLINE_ARGV[i],
							  atoi(SVL_CMDLINE_ARGV[i+1]),
							  d);
    vThreadPool.addJob(CreateDictionary, args);
  }
  vThreadPool.finish();

  if (outputFilename) {
    for (unsigned i = 1; i < dictionaries.size(); i++)
      dictionaries[0]->appendToCodebook(dictionaries[i]);
    dictionaries[0]->write(outputFilename);
  }

  svlCodeProfiler::print(cerr);

  return 0;

}


void *CreateDictionary(void *argsP, unsigned tid) 
{
  CreateDictionaryArgs *args = (CreateDictionaryArgs *)argsP;

  // iterate over all examples and save them to build the dictionary
  // later
  svlImageLoader loader(false, true); // don't use edge channel but do use color
  loader.readImageSeq(args->imageSeqFile);  
  vector<IplImage *> examples;

  for (unsigned index = 0; index < loader.numFrames(); index++) {
    vector<IplImage *> images;
    bool success = loader.getFrame(index, images);
    if (!success || images.size() == 0) {
      SVL_LOG(SVL_LOG_WARNING, "Failed to load image "
	      << loader.getFrameBasename(index));
      continue;
    }
    
    if (images.size() > 1) {
      SVL_LOG(SVL_LOG_WARNING, "Just considering the first of "
	      << images.size() << " channels of image "
	      << loader.getFrameBasename(index));
    }
    
    examples.push_back(images[0]);
  }

  // build the dictionary
  svlPixelDictionary *dict = args->dict;
  dict->buildDictionary(examples, args->num_k);
  
  SVL_LOG(SVL_LOG_MESSAGE, "Num examples: " << examples.size());
  return NULL;
}
