/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    buildPatchDictionary.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**              Sid Batra <sidbatra@cs.stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
** DESCRIPTION:
**  Build patch dictionary for object recognition by taking random snips from
**  all images in a given directory.
**
*****************************************************************************/

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <limits>
#include <string>

#include <sys/types.h>
#if defined(_WIN32)||defined(WIN32)||defined(__WIN32__)
#include "win32/dirent.h"
#else
#include <dirent.h>
#endif

#include "cv.h"
#include "cxcore.h"
#include "highgui.h"

#include "svlBase.h"
#include "svlVision.h"

using namespace std;

#define WINDOW_NAME "buildPatchDictionary"

void usage()
{
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./buildPatchDictionary [OPTIONS] (<dir>|<imageSeq>) <width> <height>" << endl;
    cerr << "OPTIONS:" << endl
	 << "  -i <filename>      :: input dictionary file" << endl
	 << "  -o <filename>      :: output dictionary file" << endl
	 << "  -n <num>           :: number of patches per image (default: 10)" << endl
         << "  -max_size <num>    :: maximum size of dictionary" << endl
         << "  -useOldVersion <n> :: use a deprecated rather than the new version (not recommended)" << endl
	 << SVL_STANDARD_OPTIONS_USAGE
	 << " Note: -debug also visualizes the dictionary" << endl
	 << endl;
}

int main(int argc, char *argv[])
{  
  captureOpenCVerrors();

  // read commandline parameters
  const int NUM_REQUIRED_PARAMETERS = 3;

  const char *inputFilename = NULL;
  const char *outputFilename = NULL;
  int numPatchesPerImage = 10;
  unsigned maxSize = numeric_limits<unsigned>::max();
  int versionNum = svlPatchDefinition::CURR_VERSION;

  SVL_BEGIN_CMDLINE_PROCESSING(argc, argv)
    SVL_CMDLINE_STR_OPTION("-i", inputFilename)
    SVL_CMDLINE_STR_OPTION("-o", outputFilename)
    SVL_CMDLINE_INT_OPTION("-n", numPatchesPerImage)
    SVL_CMDLINE_INT_OPTION("-max_size", maxSize)
    SVL_CMDLINE_INT_OPTION("-useOldVersion", versionNum)
  SVL_END_CMDLINE_PROCESSING(usage());
    
  if (SVL_CMDLINE_ARGC != NUM_REQUIRED_PARAMETERS) {
    usage();
    return -1;
  }
    
  const char *imageDir = SVL_CMDLINE_ARGV[0];
  unsigned width = atoi(SVL_CMDLINE_ARGV[1]);
  unsigned height = atoi(SVL_CMDLINE_ARGV[2]);
  
  svlImageLoader loader;  
  int nChannels = loader.numChannels();

  // read and resize images
  vector<svlMaskedFrame> images;

  if (strExtension(imageDir).compare("xml") == 0) {  
    loader.readImageSeq(imageDir);
  } else {
    loader.readDir(imageDir);
  }

  loader.setResizeSize(width,height);
  loader.getAllFrames(images);
  
  // this should be more user-friendly
  assert(images.size() > 0);

  for (unsigned i = 0; i < images.size(); i++) {
    assert(images[i].channels.size() == images[0].channels.size());
    for (unsigned j = 0; j < images[i].channels.size(); j++) {
      assert(images[i].channels[j]->width == (int) width);
      assert(images[i].channels[j]->height == (int) height);
    }
  }

  SVL_LOG(SVL_LOG_MESSAGE, "...read " << images.size() << " images from " << imageDir);

  // build dictionary
  svlPatchDictionary dictionary(width, height, versionNum);

  if (inputFilename != NULL) {
    SVL_LOG(SVL_LOG_MESSAGE, "Reading dictionary from " << inputFilename);
    dictionary.load(inputFilename);
    SVL_LOG(SVL_LOG_MESSAGE, "..." << dictionary.numFeatures() << " entries read");
    assert((dictionary.windowWidth() == width) && (dictionary.windowHeight() == height));
    
    if (dictionary.numFeatures() > maxSize) {
      dictionary.truncate(maxSize, true);
    }
  }
  
  SVL_LOG(SVL_LOG_MESSAGE, "Building dictionary...");
  
  vector<svlPatchDefinitionType> patchTypes;
  
  //Store svl enumerations for each of the channels
  for( int i=0 ; i<nChannels ; i++) {
    //Map to enumeration
    svlPatchDefinitionType patchEnumType = loader.patchTypes(i);
    
    patchTypes.push_back(patchEnumType);      
  }
    
  assert(images.size());
  assert(patchTypes.size() == images[0].channels.size());


  {
    vector<vector< IplImage *> > unpackedImages;
    vector<CvMat *> unpackedMasks;
    
    for (unsigned i = 0; i < images.size(); i++)
      {
	unpackedImages.push_back(images[i].channels);
	unpackedMasks.push_back(images[i].mask);
      }
    
    dictionary.buildDictionary(unpackedImages, patchTypes, numPatchesPerImage, false, &unpackedMasks);
  }
  
  SVL_LOG(SVL_LOG_MESSAGE, "..." << dictionary.summary() );
  
  
  if (dictionary.numFeatures() > maxSize) {
    dictionary.truncate(maxSize, false);
    SVL_LOG(SVL_LOG_MESSAGE, "... truncated to " << dictionary.numFeatures() << " entries");
  }
  
  SVL_LOG(SVL_LOG_MESSAGE, "...done");
  
  if (outputFilename != NULL) {
    SVL_LOG(SVL_LOG_MESSAGE, "Writing dictionary to " << outputFilename);
    dictionary.write(outputFilename);
  }
  
  if (svlLogger::getLogLevel() >= SVL_LOG_DEBUG) {
    cvNamedWindow(WINDOW_NAME, 1);
    IplImage *dicImages = dictionary.visualizeDictionary();
    cvShowImage(WINDOW_NAME, dicImages);
    cvWaitKey(0);
    cvReleaseImage(&dicImages);
    cvDestroyWindow(WINDOW_NAME);
  }
  
  // free memory
  for (unsigned i = 0; i < images.size(); i++) {
    for (unsigned j = 0; j < images[i].channels.size(); j++) {
      cvReleaseImage(&images[i].channels[j]);
    }
    if (images[i].mask)
      cvReleaseMat(&images[i].mask);
  }
  
  return 0;
}


