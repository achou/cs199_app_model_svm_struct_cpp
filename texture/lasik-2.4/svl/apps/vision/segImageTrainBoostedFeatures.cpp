/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    segImageTrainBoostedFeatures.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
** DESCRIPTION:
**  Training code for multi-class image segmentation features.
**
** TODO: use svlClassifier
*****************************************************************************/

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>

#include <cxcore.h>
#include <cv.h>
#include <ml.h>

#include "svlBase.h"
#include "svlML.h"
#include "svlPGM.h"
#include "svlVision.h"

using namespace std;

void usage()
{
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./segImageTrainBoostedFeatures [OPTIONS] <trainingList>\n";
    cerr << "OPTIONS:\n"
         << "  -featuresExt <ext>:: features file extension (default: .features.txt)\n"
         << "  -segLblExt <ext>  :: segment labels file extension (default: .labels.txt)\n"
         << "  -featuresDir <dir>:: features file directory (default: .)\n"
         << "  -segLblDir <dir>  :: segment labels file directory (default: <featuresDir>)\n"
         << "  -o <filestem>     :: output model files (no training if missing)\n"
         << "  -outputExt <ext>  :: output model extension (default: .model)\n"
         << "  -rounds <num>     :: number of rounds of boosting (default: 100)\n"
         << "  -splits <num>     :: number of splits in weak learners (default: 2)\n"
         << "  -subSample <n>    :: subsample data by 1/<n>\n"
         << "  -balanced         :: balance class sampling\n"
         << "  -prior <alpha>    :: dirchelet prior on +ve and -ve training sets\n"
         << "  -classId <k>      :: (re)train class <k> only (default: -1 (all classes))\n"
         << SVL_STANDARD_OPTIONS_USAGE
         << endl;
}

int main(int argc, char *argv[])
{
    // read commandline parameters
    const char *featuresExt = ".features.txt";
    const char *segLblExt = ".labels.txt";
    const char *featuresDir = ".";
    const char *segLblDir = NULL;

    const char *outputModelStem = NULL;
    const char *outputExt = ".model";
    int numRounds = 100;
    int numSplits = 2;    
    int subSample = 0;
    bool bBalanced = false;
    int pseudoCounts = 0;
    int trainClassId = -1;

    int numLabels = -1;

    SVL_BEGIN_CMDLINE_PROCESSING(argc, argv)
        SVL_CMDLINE_STR_OPTION("-featuresExt", featuresExt)
        SVL_CMDLINE_STR_OPTION("-featuresDir", featuresDir)
        SVL_CMDLINE_STR_OPTION("-segLblExt", segLblExt)
        SVL_CMDLINE_STR_OPTION("-segLblDir", segLblDir)
        SVL_CMDLINE_STR_OPTION("-o", outputModelStem)
        SVL_CMDLINE_STR_OPTION("-outputExt", outputExt)
        SVL_CMDLINE_INT_OPTION("-rounds", numRounds)
        SVL_CMDLINE_INT_OPTION("-splits", numSplits)
        SVL_CMDLINE_INT_OPTION("-subSample", subSample)
        SVL_CMDLINE_BOOL_OPTION("-balance", bBalanced)
        SVL_CMDLINE_INT_OPTION("-prior", pseudoCounts)
        SVL_CMDLINE_INT_OPTION("-classId", trainClassId)
    SVL_END_CMDLINE_PROCESSING(usage());

    if (SVL_CMDLINE_ARGC != 1) {
        usage();
        return -1;
    }

    svlCodeProfiler::tic(svlCodeProfiler::getHandle("main"));

    const char *trainingList = SVL_CMDLINE_ARGV[0];
    if (segLblDir == NULL) segLblDir = featuresDir;

    // load images
    SVL_LOG(SVL_LOG_MESSAGE, "Reading training list from " << trainingList << "...");
    vector<string> baseNames = svlReadFile(trainingList);
    SVL_LOG(SVL_LOG_MESSAGE, "...read " << baseNames.size() << " images");

    int nFeatures = -1;
    vector<int> classCounts;
    int maxClassCount = 0;

    // train boosted detectors
    if (outputModelStem != NULL) {
        
        vector<vector<double> > featureVectors;
        vector<int> featureLabels;
        
        SVL_LOG(SVL_LOG_MESSAGE, "Processing image features...");
        for (int i = 0; i < (int)baseNames.size(); i++) {
            string featuresName = string(featuresDir) + string("/") + baseNames[i] + string(featuresExt);
            string labelsName = string(segLblDir) + string("/") + baseNames[i] + string(segLblExt);
            
            ifstream ifsLabels(labelsName.c_str());
            if (ifsLabels.fail()) {
                SVL_LOG(SVL_LOG_ERROR, "labels file missing for " << baseNames[i]);
                continue;
            }

            ifstream ifsData(featuresName.c_str());
            assert(!ifsData.fail());            

            while (1) {
                // determine number of features
                if (nFeatures < 0) {
                    nFeatures = svlCountFields(&ifsData, ' ');
                }      

                // read feature vector and label
                vector<double> v(nFeatures);
                for (int j = 0; j < nFeatures; j++) {
                    ifsData >> v[j];
                }
                if (ifsData.fail()) break;
                
                int k;
                ifsLabels >> k;
                assert(!ifsLabels.fail());

                // accumulate non-void labels
                if (k < 0) continue;
                if (classCounts.size() < (unsigned)(k + 1))
                    classCounts.resize(k + 1, 0);

                // random skip
                if ((subSample > 1) && (rand() % subSample != 0) &&
                    (!bBalanced || (classCounts[k] > maxClassCount - 500)))
                    continue;

                featureVectors.push_back(v);
                featureLabels.push_back(k); 
                classCounts[k] += 1;
                if (classCounts[k] > maxClassCount)
                    maxClassCount = classCounts[k];
            }

            ifsData.close();
            ifsLabels.close();

            SVL_LOG(SVL_LOG_VERBOSE, "..." << featureVectors.size() << " ("
                << toString(classCounts) << ") feature vectors accumulated");
        }

        // train boosted superpixel classifiers
        numLabels = *max_element(featureLabels.begin(), featureLabels.end()) + 1;
        SVL_LOG(SVL_LOG_MESSAGE, featureVectors.size() << " feature vectors; " 
                << featureVectors[0].size() << " features; "
                << numLabels << " labels");
            
        CvMat *data = cvCreateMat((int)featureVectors.size(),
            (int)featureVectors[0].size(), CV_32FC1);
        CvMat *labels = cvCreateMat((int)featureVectors.size(), 1, CV_32SC1);
        assert((data != NULL) && (labels != NULL));
        double maxValue = -numeric_limits<double>::max();
        double minValue = numeric_limits<double>::max();
        for (int j = 0; j < (int)featureVectors.size(); j++) {
            for (int k = 0; k < data->width; k++) {
                cvmSet(data, j, k, featureVectors[j][k]);
                assert(!isnan(featureVectors[j][k]));
                if (featureVectors[j][k] > maxValue)
                    maxValue = featureVectors[j][k];
                if (featureVectors[j][k] < minValue)
                    minValue = featureVectors[j][k];
            }
        }
        SVL_LOG(SVL_LOG_MESSAGE, "Feature range: " << minValue << " to " << maxValue);
        
        for (int i = 0; i < numLabels; i++) {
            if ((trainClassId >= 0) && (i != trainClassId)) {
                SVL_LOG(SVL_LOG_MESSAGE, "Training boosted classifier for class " << i << "...");
                continue;
            }
            SVL_LOG(SVL_LOG_MESSAGE, "Training boosted classifier for class " << i << "...");

            // set up data vectors
            int posCount = 0;
            for (int j = 0; j < (int)featureVectors.size(); j++) {
                CV_MAT_ELEM(*labels, int, j, 0) = (featureLabels[j] == i) ? 1 : -1;
                posCount += (featureLabels[j] == i);
            }
            int negCount = featureVectors.size() - posCount;
            SVL_LOG(SVL_LOG_MESSAGE, "..." << posCount << " positive samples, " 
                << negCount << " negative samples");
            
            if (posCount == 0) continue;
            
            // train the classifier
            float *classPriors = NULL;
            if (pseudoCounts > 0) {
                classPriors = new float[2];
                classPriors[0] = (float)(posCount + pseudoCounts) /
                    (float)(negCount + posCount + 2 * pseudoCounts);
                classPriors[1] = (float)(negCount + pseudoCounts) / 
                    (float)(negCount + posCount + 2 * pseudoCounts);
            }
            CvBoostParams parameters(CvBoost::GENTLE, numRounds, 0.90, numSplits,
                false, classPriors);
            parameters.split_criteria = CvBoost::DEFAULT;
            CvMat *varType = cvCreateMat(data->width + 1, 1, CV_8UC1);
            for (int j = 0; j < data->width; j++) {
                CV_MAT_ELEM(*varType, unsigned char, j, 0) = CV_VAR_NUMERICAL;
            }
            CV_MAT_ELEM(*varType, unsigned char, data->width, 0) = CV_VAR_CATEGORICAL;
            svlCodeProfiler::tic(svlCodeProfiler::getHandle("boosting"));

            CvBoost *model = new CvBoost();
	    assert(model);
	    
	    model->train(data, CV_ROW_SAMPLE, labels,
                NULL, NULL, varType, NULL, parameters);

            svlCodeProfiler::toc(svlCodeProfiler::getHandle("boosting"));
            cvReleaseMat(&varType);
            if (classPriors != NULL) {
                delete[] classPriors;
            }
            
            // save the model
            string modelFilename = string(outputModelStem) + string(".class.") +
                toString(i) + string(outputExt);
            SVL_LOG(SVL_LOG_VERBOSE, "...saving to " << modelFilename);

            model->save(modelFilename.c_str());
#ifdef OPENCV_BUGHUNT
	    delete model;
	    model = new CvBoost();
	    assert(model);
	    model->load(modelFilename.c_str());
	    model->sanityCheck();
#endif            

            // evaluate model (on training data)
            int tp = 0; int tn = 0; int fp = 0; int fn = 0;
            
            CvMat *fv = cvCreateMat(1, (int)featureVectors[0].size(), CV_32FC1);
            for (int j = 0; j < (int)featureVectors.size(); j++) {
                for (int k = 0; k < data->width; k++) {
                    cvmSet(fv, 0, k, featureVectors[j][k]);
                }
                float score = model->predict(fv);
                if (CV_MAT_ELEM(*labels, int, j, 0) > 0) {
                    if (score > 0.0) {
                        tp += 1;
                    } else {
                        fn += 1;
                    }
                } else if (score > 0.0) {
                    fp += 1;
                } else {
                    tn += 1;
                }
            }
            cvReleaseMat(&fv);
            
            SVL_LOG(SVL_LOG_MESSAGE, "...done (TP, FN, TN, FP): " << tp << ", " << fn << ", " << tn << ", " << fp);
            
            delete model;            
        }

        // free memory
        cvReleaseMat(&labels);
        cvReleaseMat(&data);
    }

    // print profile information
    svlCodeProfiler::toc(svlCodeProfiler::getHandle("main"));
    svlCodeProfiler::print(cerr);
    return 0;
}

