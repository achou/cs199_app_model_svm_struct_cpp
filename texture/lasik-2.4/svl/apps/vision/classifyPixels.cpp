/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    classifyPixels.cpp
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
** DESCRIPTION:
**  Application for classifying pixels in an image and outputting a binary mask.
**
*****************************************************************************/

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <string.h>

#include "cv.h"
#include "cxcore.h"
#include "highgui.h"

#include "svlBase.h"
#include "svlML.h"
#include "svlVision.h"

using namespace std;

// main -----------------------------------------------------------------------

void usage()
{
	cerr << SVL_USAGE_HEADER << endl;
	cerr << "USAGE: ./classifyPixels [OPTIONS] <extractor> <model> (<image>* | <imageSequence>)" << endl;
	cerr << "OPTIONS:" << endl
	  //<< "  -allScales         :: analyze all scales of the image" << endl
	     << "  -forceAllPixels   :: classify all pixels even if IP detector stored in dict" << endl
	     << "  -maxImages <n>    :: maximum number of images to analyze" <<endl
	     << "  -r <x> <y> <w> <h> :: scan subregion of image" << endl
	     << "  -o <folder>       :: output folder" << endl
	     << "  -heat             :: output a heat map of results (red=pos, blue=neg)" << endl
	     << "  -text             :: output text file with probabilities (-1 = unkown)" << endl
	     << "  -mask           :: output a binary mask text file (-1 = unknown)" << endl
	     << "  -all              :: output everything" << endl
	     << SVL_STANDARD_OPTIONS_USAGE 
	     << endl;
}



int main(int argc, char *argv[])
{		
  captureOpenCVerrors();
  
  // read commandline parameters  
  //bool bBaseScaleOnly = false;
  const char *outputFolder = NULL;
  CvRect subRegion = cvRect(0, 0, 0, 0);
  unsigned maxImages = INT_MAX;
  bool bMask = false;
  bool bText = false;
  bool bHeat = false;
  bool forceAllPixels = false;
  
  SVL_BEGIN_CMDLINE_PROCESSING(argc, argv)
    SVL_CMDLINE_BOOL_OPTION("-forceAllPixels", forceAllPixels)
    SVL_CMDLINE_INT_OPTION("-maxImages", maxImages)
    SVL_CMDLINE_STR_OPTION("-o", outputFolder)
    SVL_CMDLINE_BOOL_OPTION("-heat", bHeat)
    SVL_CMDLINE_BOOL_OPTION("-mask", bMask)
    SVL_CMDLINE_BOOL_OPTION("-text", bText)
    SVL_CMDLINE_OPTION_BEGIN("-all", ptr)
      bHeat = bMask = bText = true;
    SVL_CMDLINE_OPTION_END(0)
    SVL_CMDLINE_OPTION_BEGIN("-r", ptr)
      subRegion.x = atoi(ptr[0]);
      subRegion.y = atoi(ptr[1]);
      subRegion.width = atoi(ptr[2]);
      subRegion.height = atoi(ptr[3]);
    SVL_CMDLINE_OPTION_END(4)
  SVL_END_CMDLINE_PROCESSING(usage());
    
  if (SVL_CMDLINE_ARGC < 3) {
    usage();
    return -1;
  }

  if (!outputFolder || (!bMask && !bText && !bHeat)) {
    SVL_LOG(SVL_LOG_WARNING, "No output specified, returning");
    return 0;
  }

  svlCreateDirectory(outputFolder);

  const char *extractorFilename = *SVL_CMDLINE_ARGV++; SVL_CMDLINE_ARGC--;
  const char *classifierFilename = *SVL_CMDLINE_ARGV++; SVL_CMDLINE_ARGC--;

  svlImageLoader loader(false, true);
  loader.readCommandLine(SVL_CMDLINE_ARGV, SVL_CMDLINE_ARGC);

  svlBinaryClassifier *classifier =
    dynamic_cast<svlBinaryClassifier *>(svlClassifierFactory().load(classifierFilename));
  SVL_ASSERT_MSG(classifier, "Please provide a binary classifier");

  svlPixelDictionary *dictionary =
    dynamic_cast<svlPixelDictionary *>(svlFeatureExtractorFactory().load(extractorFilename));
  SVL_ASSERT_MSG(dictionary, "Please provide a pixel dictionary");
  
  if (forceAllPixels)
    dictionary->clearDetector();

  // run classifier on all supplied images
  unsigned numImages = loader.numFrames();
  if (maxImages < numImages) numImages =  maxImages;

  SVL_LOG(SVL_LOG_MESSAGE, "Running classifier on " << numImages << " images/sequences...");

  for (unsigned index = 0; index < numImages; index++) {
    SVL_LOG(SVL_LOG_VERBOSE, "Processing " << loader.getFrameBasename(index) << "...");
    
    vector<IplImage *> images;
    bool success = loader.getFrame(index, images);
    if (!success || images.size() == 0) {
      SVL_LOG(SVL_LOG_WARNING, "Failed to load image "
	      << loader.getFrameBasename(index));
      continue;
    }
    
    if (images.size() > 1) {
      SVL_LOG(SVL_LOG_WARNING, "Just considering the first of "
	      << images.size() << " channels of image "
	      << loader.getFrameBasename(index));
    }
    
    IplImage *img = images[0];
    string name = strFilename(loader.getFrameBasename(index));
    
    if (subRegion.width > 0 && subRegion.height > 0)
      cropInPlace(&img, subRegion);
    
    vector<CvPoint> locations;
    vector<vector<double> > descriptors;
    // will ensure all points are extracted unless IP detector is present
    int numPoints = -1; 
    SVL_LOG(SVL_LOG_VERBOSE, "Extracting descriptors...");
    dictionary->extractDescriptors(img, locations, descriptors, &numPoints);
    
    SVL_LOG(SVL_LOG_VERBOSE, "Computing probabilities...");
    map<CvPoint, double> probs;
    for (unsigned i = 0; i < locations.size(); i++)
      probs[locations[i]] = classifier->getScore(descriptors[i]);
    
    SVL_LOG(SVL_LOG_VERBOSE, "Generating output...");
    if (bHeat) { 
      IplImage *heat = cvCreateImage(cvGetSize(img), IPL_DEPTH_32F, 3);
      cvZero(heat);
      for (unsigned i = 0; i < locations.size(); i++) {
	CvPoint loc = locations[i];
	CV_IMAGE_ELEM(heat, float, loc.y, loc.x * 3 + 2) = probs[loc]; // R
	CV_IMAGE_ELEM(heat, float, loc.y, loc.x * 3) = 1 - probs[loc]; // B
      }
      
      string heatName = string(outputFolder) + "/" + name + ".heat.jpg";
      cvSaveImage(heatName.c_str(), heat);
      cvReleaseImage(&heat);
    }
    if (bText) {
      string probTextName = string(outputFolder) + "/" + name + ".probs.txt";
      ofstream ofs(probTextName.c_str());
      for (int y = 0; y < img->height; y++) {
	for (int x = 0; x < img->width; x++) {
	  if (probs.find(cvPoint(x, y)) != probs.end())
	    ofs << probs[cvPoint(x, y)];
	  else
	    ofs << -1;
	  ofs << endl;
	}
	ofs.close();
      }
    }
    if (bMask) {
      string maskTextName = string(outputFolder) + "/" + name + ".mask.txt";
      ofstream ofs(maskTextName.c_str());
      for (int y = 0; y < img->height; y++) {
	for (int x = 0; x < img->width; x++) {
	  if (probs.find(cvPoint(x, y)) != probs.end())
	    ofs << (int)round(probs[cvPoint(x, y)]);
	  else
	    ofs << -1;
	  ofs << endl;
	}
	ofs.close();
      }
    }

    // free image
    for (unsigned i = 0; i < images.size(); i++) {
      cvReleaseImage(&images[i]);
    }  
  }//image names in image sequence loop

  svlCodeProfiler::toc(svlCodeProfiler::getHandle("main"));
  svlCodeProfiler::print(cerr);
 
  return 0;
}
