/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    buildPixelFeatureCache.cpp
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
** DESCRIPTION:
**  Build a cache of feature values computed pixel-level
**
*****************************************************************************/

#define NUM_THREADS 8

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sys/types.h>
#include <sstream>

#if defined(_WIN32)||defined(WIN32)||defined(__WIN32__)
#include "win32/dirent.h"
#else
#include <dirent.h>
#endif

#include "cv.h"
#include "cxcore.h"
#include "highgui.h"

#include "svlBase.h"
#include "svlVision.h"

using namespace std;


void usage()
{
    cerr << SVL_USAGE_HEADER << endl;
    cerr << "USAGE: ./buildPixelFeatureCache [OPTIONS] (<image dir> | <image sequence>) <output dir> <feature extractor>" << endl;
    cerr << "OPTIONS:" << endl
	 << "  -maxImages <num>   :: maximum training images to cache" << endl
 	 << "  -binary            :: creates binary cache file named <output dir>.bin" << endl
	 << SVL_STANDARD_OPTIONS_USAGE << endl
	 << endl;
}


int main(int argc, char *argv[])
{
  captureOpenCVerrors();
  svlCodeProfiler::tic(svlCodeProfiler::getHandle("buildPixelFeatureCache"));

  // read commandline parameters
  const int NUM_REQUIRED_PARAMETERS = 3;
  
  int maxImages = INT_MAX;
  bool binary = false;

  SVL_BEGIN_CMDLINE_PROCESSING(argc, argv)
    SVL_CMDLINE_INT_OPTION("-maxImages", maxImages)
    SVL_CMDLINE_BOOL_OPTION("-binary", binary)
  SVL_END_CMDLINE_PROCESSING(usage());
    
  if (SVL_CMDLINE_ARGC != NUM_REQUIRED_PARAMETERS) {
    usage();
    return -1;
  }

  const char *imageDir = SVL_CMDLINE_ARGV[0];
  const char *outputDir = SVL_CMDLINE_ARGV[1];
  const char *extractorFilename = SVL_CMDLINE_ARGV[2];

  svlImageLoader loader(false, true); // by default load without edge maps but with color

  // read dictionary
  svlFeatureExtractor *extractorTmp = svlFeatureExtractorFactory().load(extractorFilename);
    
  if (!extractorTmp)
    SVL_LOG(SVL_LOG_FATAL, "Could not load "<<extractorFilename);

  svlPixelDictionary *extractor = dynamic_cast<svlPixelDictionary *>(extractorTmp);
  if (!extractor)
    SVL_LOG(SVL_LOG_FATAL, "The provided extractor should be a PixelDictionary");

  SVL_LOG(SVL_LOG_MESSAGE, extractor->numFeatures() << " features in the feature extractor");

  if (strExtension(imageDir).compare("xml") == 0) {  
    loader.readImageSeq(imageDir);
  } else {
    loader.readDir(imageDir);
  }

  // creates an output binary file
  string outputDirStr = string(outputDir);
  int pos = outputDirStr.find_last_not_of("/");
  outputDirStr = outputDirStr.substr(0, pos+1);
  string filename = outputDirStr + ".bin";
  ofstream ofs;
  if (binary)
    ofs.open(filename.c_str(), std::ios::out|std::ios::binary);

  unsigned descriptorNum = 0;
  for (unsigned index = 0; index < loader.numFrames(); index++) {
    vector<IplImage *> images;
    bool success = loader.getFrame(index, images);
    if (!success || images.size() == 0) {
      SVL_LOG(SVL_LOG_WARNING, "Failed to load image "
	      << loader.getFrameBasename(index));
      continue;
    }
    
    if (images.size() > 1) {
      SVL_LOG(SVL_LOG_WARNING, "Just considering the first of "
	      << images.size() << " channels of image "
	      << loader.getFrameBasename(index));
    }

    vector<CvPoint> locations;
    vector<vector<double> > descriptors;
    extractor->extractDescriptors(images[0], locations, descriptors);

    for (unsigned i = 0; i < descriptors.size(); i++, descriptorNum++) {
      if (binary)
	svlWriteExampleToBinaryCache(ofs, descriptors[i]);
      else
	svlWriteExampleToCache(outputDir, descriptorNum, descriptors[i]);
    }

    for (unsigned i = 0; i < images.size(); i++)
      cvReleaseImage(&images[0]);
  }

  delete extractor;
 
  svlCodeProfiler::toc(svlCodeProfiler::getHandle("buildPixelFeatureCache"));
  svlCodeProfiler::print(cerr);
  
  return 0;
}
