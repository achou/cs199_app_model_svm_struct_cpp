function writeXML(fname,root,varargin)
%Write XML tree to a file.
%  ROOT = WRITEXML(FNAME,ROOT) writres the tree to FNAME.
%
%  ROOT = WRITEXML(FNAME,ROOT,...) uses the options:
%    'Pretty' - Pretty prints the XML by putting each attribute on its own
%               space.
%
%
%  STAIR VISION LIBRARY
%  Copyright (c) 2009, Stanford University
%
%  AUTHOR(S): Paul Baumstarck <pbaumstarck@stanford.edu>

global xml_pretty;
% global xml_expand_text;
global xml_tab_size;
xml_pretty = false;
% xml_expand_text = false;
xml_tab_size = 4;
i = 1;
while i <= length(varargin)
    if strcmp(varargin{i},'Pretty')
        xml_pretty = true;
%     elseif strcmp(varargin{i},'ExpandText')
%         xml_expand_text = true;
    elseif strcmp(varargin{i},'TabSize')
        xml_tab_size = varargin{i+1};
        i=i+1;
    end
    i=i+1;
end

fp = fopen(fname,'w');
fprintf(fp,'<?xml version="1.0" encoding="utf-8"?>\n');
if iscell(root)
    for i=1:length(root)
        writeNode(fp,root{i});
    end
else
    writeNode(fp,root);
end
fclose(fp);


function writeNode(fp,node,ind)
%Write a node given the file pointer.
%  WRITENODE(FP,NODE,INDENT='').

global xml_pretty;
% global xml_expand_text;
global xml_tab_size;

if nargin < 3 || isempty(ind)
    ind = '';
end

fields = fieldnames(node);
gs = [ind '<' node.Name];
noprint = {'Name','Inline','ch'};
ix = ~ismember(fields,noprint);
fix = find(ix);
if ~xml_pretty || length(fix) <= 1
    for i=1:length(fix)
        gs = sprintf('%s %s="%s"', gs, fields{fix(i)}, getfield(node,fields{fix(i)}));
    end
else
    osz = ceil( (length([ind '<' node.Name])+1) / xml_tab_size );
    gt = [];
    for i=1:osz
        gt = [gt '\t'];
    end
    gs = sprintf('%s %s="%s"\n', gs, fields{fix(1)}, getfield(node,fields{fix(1)}));
    for i=2:length(fix)-1
        gs = sprintf('%s%s %s="%s"\n', gs, gt, fields{fix(i)}, getfield(node,fields{fix(i)}));
    end
    gs = sprintf('%s%s %s="%s"', gs, gt, fields{fix(end)}, getfield(node,fields{fix(end)}));
end

if any(ismember(fields,'Inline'));
    % Close opening tag, inline text, print closing tag.
    fprintf(fp,[gs '>' node.Inline '</' node.Name '>\n']);
elseif ~isfield(node,'ch');
    % If no children, close node and print to file.
    fprintf(fp,[gs '/>\n']);
else
    % Close opening tag.
    fprintf(fp,[gs '>\n']);
    % Print children.
    ind1 = [ind '\t'];
    for i=1:length(node.ch)
        writeNode(fp,node.ch{i},ind1);
    end
    % Print close tag to file.
    fprintf(fp,[ind '</' node.Name '>\n']);
end

