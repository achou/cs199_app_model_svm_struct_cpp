function K = readipl(file,prec,packed)
%Read an IPL image.
%  K = READIPL(FILE,PREC='float',PACKED=1) reads binary data stored as
%  (width, height, data in row-major order).
%
%  STAIR VISION LIBRARY
%  Copyright (c) 2009, Stanford University
%
%  AUTHOR(S): Paul Baumstarck <pbaumstarck@stanford.edu>

if nargin < 2 || isempty(prec)
    prec = 'float32';
end
if nargin < 3 || isempty(packed)
    packed = 1;
end
% if nargin < 2 || isempty(correct)
%     correct = true;
% end
fp = fopen(file,'rb');
w = fread(fp,1,'int32');
h = fread(fp,1,'int32');
K = reshape(fread(fp,w*h*packed,prec),[packed*w h])';
% if correct
%     K(K<0) = 256 + K(K<0);
% end
fclose(fp);
