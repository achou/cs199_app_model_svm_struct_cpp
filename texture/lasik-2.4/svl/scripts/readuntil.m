function [gs,ix,rets] = readuntil(fp,str,past)
%Read file until str is found
%  [GS,IX,RETS] = READUNTIL(FP,STR,PAST = 0) calls FGETL on FP until STR if
%  found, and then goes PAST number of FGETLs past. GS is the last line
%  read, and RETS is all lines read before in the call. If STR is a cell
%  array of strings, the function returns at the first match. Then IX is
%  the index of which string was matched in GS.
%
%  STAIR VISION LIBRARY
%  Copyright (c) 2009, Stanford University
%
%  AUTHOR(S): Paul Baumstarck <pbaumstarck@stanford.edu>

rets = {};
if ~iscell(str)
    gs = fgetl(fp);
    while isempty(regexp(gs,str))
        rets{end+1} = gs; %#ok<*AGROW>
        gs = fgetl(fp);
    end
    ix = 1;
else
    while ~feof(fp)
        gs = fgetl(fp);
        gar = regexp(gs,str);
        % Find if match.
        ix = -1;
        for i=1:length(gar)
            if ~isempty(gar{i})
                ix = i;
                break;
            end
        end
        if ix ~= -1
            break;
        end
        rets{end+1} = gs;
    end
end

if nargin < 3 || isempty(past)
    past = 0;
end
for i=1:past
    rets{end+1} = gs;
    gs = fgetl(fp);
end

