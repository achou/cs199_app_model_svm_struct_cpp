#!/usr/bin/perl -I ../../external/perl5


use strict;
use Getopt::Std;
use File::Path;
use Cwd 'realpath';

my $HOME = "/afs/cs/u/olga/";
my $STAIR = "/afs/cs/group/stair/";

my $dir = "${HOME}lasik/experiments/";

my $script="${HOME}lasik/svl/scripts/fullyTrainObjectDetector.2.pl";

my $dataDir="${STAIR}data/visionDataset/";
my $objectsDir="${STAIR}data/objects/";
my $outputDir = "${HOME}scratch/experiments/cvpr/";
my $modelDir = "${HOME}scratch/models/cvpr/";

my @objects = qw(bottle can clock cup door_handle keyboard monitor mug ski_boot wastebasket);
my @widths =  qw(32 32 32 32 40 96 64 32 64 32);
my @heights = qw(64 64 32 40 40 32 64 32 64 40);

my $params = "-r ${dataDir}olga/image_seq_neg.xml -t ${dataDir}olga/image_seq_rest.xml -G ${dataDir}video_objects.xml -c 2 -b 500";

#my @labels = qw(intensity noedge coloredge);

#my @labels = qw(PbothNwith PtrainNwith PgoogleNwith PbothNwithout PgoogleNwithout PtrainNwithout );

my @labels = qw(new_dict);

#my $fs = "0";
my @fs_labels = qw(Boosting Filter MI);


my $FILE;

for (my $j = 0; $j < @objects; $j++) {
    my $object = @objects[$j];

    for (my $i = 0; $i < @labels; $i++) {
	my $label = @labels[$i];

	for (my $f = 0; $f < @fs_labels; $f++) {

	    my $filename = "${dir}train.cvpr.${label}.${f}.${object}.sh";
	
	    open(FILE, ">$filename");
	    print FILE "#!/bin/sh\n";

	    my $z = $f + 10;

	    my $cmd = "$script $params -o ${outputDir}${label}/".@fs_labels[$f]."/ -m ${modelDir}${label}/".@fs_labels[$f]."/ -f $f"; 
	    $cmd = "$cmd -w @widths[$j] -h @heights[$j] -z $z";

	if ($object eq "clock" || $object eq "door_handle" || $object eq "cup" || 
	    $object eq "wastebasket" || $object eq "ski_boot") {
	    $cmd = "$cmd -p ${objectsDir}${object}"; 
	}
	print FILE "$cmd $object >& ${outputDir}${label}/".@fs_labels[$f]."/${object}.commands\n";
    }
    }

    close(FILE);
}

