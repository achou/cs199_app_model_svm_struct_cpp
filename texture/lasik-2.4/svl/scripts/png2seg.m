% PNG2SEG   Convert a png segmentation file to text
% Stephen Gould <sgould@stanford.edu>
%

function png2seg(directory, extension);

files = dir([directory, '/*.png']);
for i = 1:length(files),
    baseName = files(i).name(1:end - 4);
    disp(['Processing ', baseName, '...']);
    
    seg = int32(imread([directory, '/', files(i).name]));
    dlmwrite([directory, '/', baseName, extension], seg, 'delimiter', ' ');
end;
