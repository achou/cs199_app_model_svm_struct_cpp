#!/usr/bin/perl -I ../../external/perl5
#
# STAIR VISION LIBRARY
# Copyright (c) 2008, Stanford University
#
# FILENAME:    splitDetectionsByFrame.pl
# AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
# DESCRIPTION:
#  Given an object2dsequence file


use strict;
use Getopt::Std;
use File::Path;
use Cwd 'realpath';
use IO::Handle;

my $cmd;
my $cmdline;

#my %opts = ();
#getopts("", \%opts);
if ($#ARGV < 0) {
    print STDERR "USAGE: ./splitDetectionsByFrame.pl <inputFile> <outputFileBase>?\n";
    exit(-1);
}

my $filename = $ARGV[0];
my $outBase = $ARGV[1];

my $FILE;
open(FILE, "<$filename");
my @lines = <FILE>;
close(FILE);

my $line;
my $firstLine;
my $outputFilename;
foreach $line (@lines) {
    if ($line =~ m/Object2dSequence/) {
	$firstLine = $line;
    } elsif ($line =~ m/Object2dFrame/) {
	if ($line =~ /.*id=\"(.*)\"/) {
	    $outputFilename = ${outBase}.${1}.".xml";
	    open(FILE, ">$outputFilename");
	    print FILE $firstLine;
	    print FILE $line;
	} else {
    	    print FILE "\\t</Object2dFrame\>\n";
	    print FILE "\</Object2dSequence\>\n";
	    close(FILE);
	}
    } else {#if ($FILE->opened()) {
	print FILE $line;
    }
}

exit(0);




