function plotclassified(A,c,cur,w,subdims,point_sizes)
%Plot classified results versus ground truth.
%  PLOTCLASSIFIED(A,C,CUR=C,W=[],SUBDIMS=[1:2]) plots the N-by-K matrix A,
%  using groundtruth labels C, and current labels CUR (properly classified
%  samples show up as dots, improperly classified as x's). If CUR is
%  unspecified, dots are used for plotting. An optional weight vector will
%  scales the symbols. If A has more than 3 dimensions, SUBDIMS will
%  specify the dimensions to use in plotting (can be 2 or 3).
%
%  PLOTCLASSIFIED(...,POINT_SIZES) uses the marker sizes specified for
%  displaying weighted results.
%  
%  STAIR VISION LIBRARY
%  Copyright (c) 2009, Stanford University
%
%  AUTHOR(S): Paul Baumstarck <pbaumstarck@stanford.edu>


if nargin < 3 || isempty(cur)
    cur = c;
end
if nargin < 4
    w = [];
end
if nargin < 5 || isempty(subdims)
    subdims = 1:min(3,size(A,1));
elseif numel(subdims) > 3
    subdims = subdims(1:3);
end
if nargin < 6 || isempty(point_sizes)
    point_sizes = [5 14 18 23 27 32 36];
end

% Plot correct/incorrect labels with ./x
if ~islogical(cur)
    cur = round(cur);
end
u = unique(c);
if length(u) == 2
    colours = [0 0 1; 1 0 0];
elseif length(u) == 3
    colours = [0 0 1; 0 1 0; 1 0 0];
else
    colours = jet(length(u));
end

if ~isempty(w)
    w = round( length(point_sizes) * (w-min(w))/(max(w)-min(w)) );
    w = min(length(point_sizes),max(1,w));
    w = point_sizes(w);
end
w_unq = unique(w);
w = w(:);
c = c(:);
cur = cur(:);

for i=1:length(u)
    if isempty(w)
        if numel(subdims) == 2
            plot(A(subdims(1),c==u(i) & cur==u(i)), ...
                 A(subdims(2),c==u(i) & cur==u(i)), ...
                 '.', 'Color',colours(i,:));
            plot(A(subdims(1),c==u(i) & cur~=u(i)), ...
                 A(subdims(2),c==u(i) & cur~=u(i)), ...
                 'x', 'Color',colours(i,:));
        elseif numel(subdims) == 3
            plot3(A(subdims(1),c==u(i) & cur==u(i)), ...
                  A(subdims(2),c==u(i) & cur==u(i)), ...
                  A(subdims(3),c==u(i) & cur==u(i)), ...
                  '.', 'Color',colours(i,:));
            plot3(A(subdims(1),c==u(i) & cur~=u(i)), ...
                  A(subdims(2),c==u(i) & cur~=u(i)), ...
                  A(subdims(3),c==u(i) & cur~=u(i)), ...
                  'x', 'Color',colours(i,:));
        end
    else 
        for j=1:length(w_unq)
            if numel(subdims) == 2
                cond = c==u(i) & cur==u(i) & w==w_unq(j);
                plot(A(subdims(1),cond),A(subdims(2),cond),...
                     '.','Color',colours(i,:),'MarkerSize',w_unq(j));
                 cond = c==u(i) & cur~=u(i) & w==w_unq(j);
                plot(A(subdims(1),cond),A(subdims(2),cond),...
                     'x','Color',colours(i,:),'MarkerSize',w_unq(j));
            elseif numel(subdims) == 3
%                 plot3(A(subdims(1),c==u(i) & cur==u(i)), ...
%                       A(subdims(2),c==u(i) & cur==u(i)), ...
%                       A(subdims(3),c==u(i) & cur==u(i)), ...
%                       '.', 'Color',colours(i,:));
%                 plot3(A(subdims(1),c==u(i) & cur~=u(i)), ...
%                       A(subdims(2),c==u(i) & cur~=u(i)), ...
%                       A(subdims(3),c==u(i) & cur~=u(i)), ...
%                       'x', 'Color',colours(i,:));
            end
        end
    end
%         plot(A(subdims(1),c&cur),A(subdims(2),c&cur),'b.');
%         plot(A(subdims(1),~c&~cur),A(subdims(2),~c&~cur),'r.');
%         plot(A(subdims(1),c&~cur),A(subdims(2),c&~cur),'bx');
%         plot(A(subdims(1),~c&cur),A(subdims(2),~c&cur),'rx');
end


