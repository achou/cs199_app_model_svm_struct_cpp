% SEG2PNG   Convert a text segmentation file to png
% Stephen Gould <sgould@stanford.edu>
%

function seg2png(directory, extension);

files = dir([directory, '/*', extension]);
for i = 1:length(files),
    baseName = files(i).name(1:end - length(extension));
    disp(['Processing ', baseName, '...']);
    
    seg = uint16(load([directory, '/', files(i).name]));
    imwrite(seg, [directory, '/', baseName, '.png'], 'BitDepth', 16);    
end;