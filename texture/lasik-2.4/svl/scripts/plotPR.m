% PLOTPR    Plot PR Curve Generated from SVL Object Detectors
%           Assumes data input files have a single header line
%           followed by comma delimited threshold, recall and
%           precision. If input argument is given as a cell
%           array, then the PR curves are averaged.
%
% STAIR VISION PROJECT
% Copyright (c) 2007-2010, Stanford University
%
% Stephen Gould <sgould@stanford.edu>

function [pr] = plotPR(varargin);

LINECOLORS = {'b-', 'r-.', 'g--', 'm--', 'c--', 'k--', 'y--'};

legendText = {};
for i = 1:length(varargin),
    if (isempty(varargin{i})), % space holder
        vpr = [0, 0, 0];
        %if (i == 1), hold on; end;
    elseif (iscell(varargin{i})),        
        legendText{end + 1} = 'average';
        vpr = zeros(10000, 3);
        vpr(:, 1) = (size(vpr, 1):-1:1) / size(vpr, 1);
        vpr(:, 2) = (0:size(vpr, 1) - 1) / (size(vpr, 1) - 1);
        for j = 1:length(varargin{i}),
            tmp = dlmread(varargin{i}{j}, ',', 1, 0);
            if (isempty(tmp)), continue; end;
            x = unique(tmp(:, 2));
            y = zeros(size(x));
            for k = 1:length(x),
                y(k) = max(tmp(find(tmp(:, 2) == x(k)), 3));
            end;
            if (x(1) ~= 0), x = [0; x]; y = [y(1); y]; end;
            if (x(end) ~= 1), x = [x; x(end)+1.0e-6; 1]; y = [y; 0; 0]; end;
            vpr(:, 3) = vpr(:, 3) + interp1(x, y, vpr(:, 2), 'linear');
        end;
        vpr(:, 3) = vpr(:, 3) / length(varargin{i});
    else
        legendText{end + 1} = varargin{i};
        vpr = dlmread(varargin{i}, ',', 1, 0);
        if (isempty(vpr)),
            disp(['WARNING: ', varargin{i}, ' is empty']);
            vpr = [0, 0, 0];
        end;
    end;
    vpr = sortrows(vpr, 2);
    plot(vpr(:, 2), vpr(:, 3), LINECOLORS{mod(i - 1, length(LINECOLORS)) + 1}, 'LineWidth', 2);
    [maxf1, argf1] = max(2.0 * vpr(:, 2) .* vpr(:, 3) ./ (vpr(:, 2) + vpr(:, 3)));
    ap = averagePrecision(vpr(:, 3), vpr(:, 2));
    if (nargout == 0),
        disp([int2str(i), '. ', legendText{end}]);
        disp(['   Max. F1-score: ', num2str(maxf1), ' at t=', num2str(vpr(argf1, 1))]);
        disp(['   Avg. Precision: ', num2str(ap)]);
    end;
    
    if (i == 1), hold on; end;
    
    pr(i).pr = vpr;
    pr(i).ap = ap;
    pr(i).maxf1 = maxf1;
    pr(i).argf1 = argf1;
end;
            
hold off;
axis([0, 1, 0, 1]); axis square;
xlabel('recall'); ylabel('precision'); grid on;
legend(legendText);

% averagePrecision -------------------------------------------------------

function [ap] = averagePrecision(precision, recall);

% compute 11-point interpoloated average precision
ap = 0.0;
for t = 0.0:0.1:1.0
    p = max(precision(recall >= t));
    if isempty(p), p = 0; end;
    ap = ap + p / 11.0;
end
