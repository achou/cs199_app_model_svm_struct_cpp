% ADDSVLPATHS Add STAIR Vision Library paths to Matlab.
% Stephen Gould <sgould@stanford.edu>
% 
% Execute as: run('svl/scripts/addSVLPaths');
%

baseDir = [pwd, '/../..'];

warning('off', 'MATLAB:dispatcher:pathWarning');
path(path, [baseDir, '/svl/scripts']);
path(path, [baseDir, '/bin']);
projectDirs = dir([baseDir, '/projects']);
for i = 1:length(projectDirs),
    if ((projectDirs(i).isdir == 1) && (projectDirs(i).name(1) ~= '.')),
        path(path, [baseDir, '/projects/', projectDirs(i).name]);
    end;
end;
warning('on', 'MATLAB:dispatcher:pathWarning');
