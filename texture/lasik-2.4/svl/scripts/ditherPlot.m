% DITHERPLOT    Produce a dither plot of features
% Stephen Gould <sgould@stanford.edu>
%
% Produces a dither plot of features split by class label. This is very
% useful for visualizing which features separate classes well.
%

function [h] = ditherPlot(x, y);

% TODO: check x,y size

MARKERS = {'b.', 'r.', 'g.', 'c.', 'm.', 'k.', 'y.', 'bx'};

% find classes
K = max(y) + 1;
for k = 1:K,
    indx{k} = find(y == k - 1);
end;

% normalize data
x = x - repmat(mean(x, 1), size(x, 1), 1);
x = x ./ repmat(std(x, 1), size(x, 1), 1);

% plot features
h = figure; hold on;
for n = 1:size(x, 2),
    for k = 1:K,
        marker = MARKERS{mod(k - 1, length(MARKERS)) + 1};
        dither = ((k - 1) + rand(length(indx{k}), 1)) / (K + 1);
        plot(n + dither, x(indx{k}, n), marker);
    end;
end;
hold off; grid on;
