% CONFUSIONMATRIX   Generate a confusion matrix given class predictions.
%
% STAIR VISION PROJECT
% Copyright (c) 2007-2010, Stanford University
%
% Stephen Gould <sgould@stanford.edu>

function [c, accuracy] = confusionMatrix(actual, predicted);

if (any(size(actual) ~= size(predicted))),
    error('actual and predicted vectors must have equal size');
end;

nClasses = ceil(max(actual(:)) + 1);
c = zeros(nClasses, nClasses);

for i = 1:nClasses,
    indx = find(actual == i - 1);
    for j = 1:nClasses,
        c(i, j) = sum(predicted(indx) == j - 1);
    end;
end;

accuracy = sum(diag(c)) / sum(c(:));

