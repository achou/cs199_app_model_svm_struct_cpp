#!/usr/bin/perl -I ../../external/perl5 -I ./external/perl5
#
# STAIR VISION LIBRARY
# Copyright (c) 2007-2010, Stephen Gould
#
# FILENAME:    viewClusterGraph.pl
# AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
# DESCRIPTION:
#   Generates a graphical visualization of an SVL cluster graph from
#   XML file. Can highlight distinguished nodes. Requires dot to be
#   installed on the system (see http://www.graphviz.org/).
#
#   USAGE:
#     ./viewClusterGraph.pl [OPTIONS] <XML file> [[<colour>] <clique>]*
#
#   OPTIONS:
#     -c :: show as cliques (not factor graph)
#     -i :: output image filename
#     -o :: output dotty filename
#     -v :: verbose
#     -x :: don't show output
#
#   <clique> can be a regular expression (don't use .*, use \w* instead)
#   <colour> can be one of "red", "green", "blue"
#

use Getopt::Std;
use XML::Simple;

my %opts = ();
getopts("ci:o:vx", \%opts);

if ($#ARGV < 0) {
    print STDERR "USAGE: ./viewClusterGraph.pl [OPTIONS] <XML file> [[<colour>] <clique>]*\n";
    exit;
}

my $INFILE = $ARGV[0];
my $BASENAME = ($INFILE =~ m/.*[\\\/](.+)/) ? $1 : $INFILE;
my $OUTFILE = (defined($opts{'i'}) ? $opts{'i'} : "/tmp/${BASENAME}.png");
my $TMPFILE = (defined($opts{'o'}) ? $opts{'o'} : "/tmp/${BASENAME}.dot");

# read the XML cluster graph
die "ERROR: could not open file $INFILE" unless (-e $INFILE);
print "Processing $INFILE...\n" if (defined($opts{'v'}));
my $graph = XMLin($INFILE, KeyAttr => 1,
                  ForceArray => ['VarCards', 'Clique', 'Edges', 'Potentials']);

# determine variable cardinalities and cliques
$graph->{VarCards}->[0] =~ s/^\s+//;
$graph->{VarCards}->[0] =~ s/\s+$//;
my @cards = split(/\s+/, $graph->{VarCards}->[0]);
my @cliques = ();
for (my $i = 0; $i <= $#{$graph->{Clique}}; $i++) {
    my $c = $graph->{Clique}->[$i]->{content};
    $c =~ s/\s+/ /mg; $c =~ s/^\s+//g; $c =~ s/\s+$//g;
    push @cliques, $c;
}

# generate dot file
print "Generating dot input...\n" if (defined($opts{'v'}));
open FOUT, ">$TMPFILE" or die "ERROR: could not open $TMPFILE\n";
print FOUT "graph svlClusterGraph {\n";

if (!defined($opts{'c'})) {
    # factor graph
    for (my $i = 0; $i <= $#{$graph->{Clique}}; $i++) {
        print FOUT "  \"$cliques[$i]\"";

        my $attributes =  ($graph->{Clique}->[$i]->{size} > 1) ? "shape=box" : "shape=circle";
        my $COLOUR = "red";
        for (my $j = 1; $j <= $#ARGV; $j++) {
            if (($ARGV[$j] eq "red") || ($ARGV[$j] eq "green") || ($ARGV[$j] eq "blue")) {
                $COLOUR = $ARGV[$j];
                next;
            }
            if ($cliques[$i] =~ m/^\s*\"?\s*($ARGV[$j])\s*\"?/) {
                $attributes = "$attributes, style=bold, fontcolor=$COLOUR";
                last;
            }
        }

        print FOUT " [$attributes];\n";
    }

    my $edges = join(" ", @{$graph->{Edges}});
    while ($edges =~ m/(\d+)\s+(\d+)/mg) {
        my $src = $cliques[$1];
        my $dst = $cliques[$2];
        print FOUT "  \"$src\" -- \"$dst\";\n";
    }

} else {
    # nodes
    for (my $i = 0; $i <= $#cards; $i++) {
        print FOUT "  \"$i\"";

        my $label = "\"$i ($cards[$i])\"";
        my $attributes = "label=$label, shape=circle";
        my $COLOUR = "red";
        for (my $j = 1; $j <= $#ARGV; $j++) {
            print "$ARGV[$j]\n";
            if (($ARGV[$j] eq "red") || ($ARGV[$j] eq "green") || ($ARGV[$j] eq "blue")) {
                $COLOUR = $ARGV[$j];
                next;
            }
            if ($label =~ m/^\s*\"?\s*($ARGV[$j])\s*\"?/) {
                $attributes = "$attributes, style=bold, fontcolor=$COLOUR";
                last;
            }
        }

        print FOUT " [$attributes];\n";
    }

    # cliques
    for (my $i = 0; $i <= $#{$graph->{Clique}}; $i++) {
        my @c = split(/\s+/, $cliques[$i]);
        for (my $j = 0; $j < $#c; $j++) {
            for (my $k = $j + 1; $k <= $#c; $k++) {
                print FOUT "  \"$c[$j]\" -- \"$c[$k]\";\n";
            }
        }
    }
}

print FOUT "}\n";
close FOUT;

# create image
print "Creating image...\n" if (defined($opts{'v'}));
`dot -Tpng $TMPFILE > $OUTFILE`;
unlink($TMPFILE) unless (defined($opts{'o'}));
if (!defined($opts{'x'})) {
    `eog $OUTFILE &`;
};

