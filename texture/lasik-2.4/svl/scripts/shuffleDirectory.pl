#!/usr/bin/perl -I ../../external/perl5

# useful for shuffling the directory with negative training images

my $DIRNAME = $ARGV[0];

my $DIR;
opendir(DIR, "$DIRNAME") or die $!;
my @files = grep(!/^\.\.?/, readdir(DIR));

my $num_files = @files;
#print "$num_files\n";

my @indices = 1..@files;
my @new_indices;

push(@new_indices, splice(@indices, rand(@indices), 1))
    while @indices;

#print "@new_indices\n";

for (my $i = 0; $i < @files; $i++) {
    my $new_name = $DIRNAME."n".$new_indices[$i].$files[$i];
    my $cmd = "mv ${DIRNAME}$files[$i] $new_name";
    #print "$cmd\n";
    `$cmd`;
}


