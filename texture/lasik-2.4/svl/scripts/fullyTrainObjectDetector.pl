#!/usr/bin/perl -I ../../external/perl5
#
# STAIR VISION LIBRARY
# Copyright (c) 2008, Stanford University
#
# FILENAME:    fullyTrainObjectDetector.pl
# AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
#              based on a script by Stephen Gould <sgould@stanford.edu>
# DESCRIPTION:
#  Generates the data for and trains a patch-based object detector for a given class 
#  from scratch, using the following steps:
#      1. Creates negative image patches:
#           Either (a) samples negative patches from the training set, or
#                  (b) copies over a given directory of patchess to use as negatives, or
#                  (c) takes a given directory of negatives and just uses that
#                      after cleaning out all the FP* images in there,
#                      which might've been the false positives added by previous
#                      runs of the script
#      2. Creates positive image patches, using one of the techniques above
#      3. Calls trainObjectDetector with the -x option to make sure the
#           dictionary doesn't get trimmed after the second training stage
#      4. Saves the dictionary and trims it temporarily for faster detection
#      5. Evaluates the classifier on the training and testing sets
#      6. Generates false positive examples using the buildTrainingImageDataset
#         program
#                   - at most 50 patches per image (by default) are added, so
#                     at most half of the new training set is false positives
#                   - only the false positives that are above a specified
#                     probability threshold are added
#      7. Restores the dictionary and runs the trainObjectDetector script again
#         (so performs only the last stage of training again)
#      8. Evaluates the performance on the training and test sets
#


use strict;
use Getopt::Std;
use File::Path;
use Cwd 'realpath';

my $cmd;
my $cmdline;

my $PATH = "$ENV{HOME}/lasik/"; # change if lasik is not in your home directory
#print "$PATH\n";

my $EXEBASE = "${PATH}bin/";
my $MODELDIR = "${PATH}../scratch/models/";
my $SCRIPTDIR = "${PATH}svl/scripts/";

my $OUTPUTDIR = "${PATH}experiments/";
my $BASE_WIDTH = 32;
my $BASE_HEIGHT = 32;
#my $NUM_NEG = 100;

my $MAX_IMAGES_A = 2000;
my $MAX_IMAGES_B = 20000;
my $MAX_IMAGES_C = 8000;

my $MAX_DICT_SIZE = 2000;
my $MIN_FINAL_DICT_SIZE = 100;
my $MAX_FINAL_DICT_SIZE = 500;

my $WEAK_LEARNER_SPLITS = 2;

my $FP_THRESHOLD = 0.25;
my $BOOSTING_ROUNDS = 500;
my $KFOLD = -1;
my $MI_THRESHOLD = 0.001;

my $CLASSIFIER_TYPE = 0;
my $FEATURE_SELECTION_TYPE = 1;
my $ID = "";

my %opts = ();
getopts("i:r:t:G:o:p:n:N:k:w:h:c:b:SxXm:f:Rdz:sDF:", \%opts);
if ($#ARGV < 0 || !exists($opts{'r'}) || !exists($opts{'t'}) || !exists($opts{'G'})) {
    print STDERR "USAGE: ./fullyTrainObjectDetector.pl [options] <object>\n";
    print STDERR "REQUIRED:\n";
    print STDERR "   -r <image_seq> :: image sequence file for training\n";
    print STDERR "   -t <image_seq> :: image sequence file for testing\n";
    print STDERR "   -G <ground_truth> :: ground truth file for both\n";
    print STDERR "OPTIONAL:\n";
    #print STDERR "   -i <type> :: determines the patch classifier type (default: $CLASSIFIER_TYPE)\n";
    #print STDERR "                (0 = INT + EDGE, 1 = INT + EDGE_BGR, 2 = INT only)\n";
    print STDERR "   -o <dir>  :: output results directory (default: $OUTPUTDIR)\n";
    print STDERR "                (note: should be different for different training methods, e.g. abs/intensity)\n";
    print STDERR "   -m <dir>  :: output model directory (default $MODELDIR)\n";
    print STDERR "   -w <n>    :: base image width (default: $BASE_WIDTH)\n";
    print STDERR "   -h <n>    :: base image height (default: $BASE_HEIGHT)\n";
    print STDERR "   -c <n>    :: cross-validation option (cvn) for trainObjectDetector (default: $KFOLD)\n";
    print STDERR "   -b <n>    :: number of boosting rounds total (default: $BOOSTING_ROUNDS)\n";
    print STDERR "   -f <n>    :: feature selection options: (default: $FEATURE_SELECTION_TYPE)\n";
    print STDERR "                (0 = boosting on 2000 examples, 1 = filterPatchDictionary, 2 = mutual info)\n";
    print STDERR "   -R        :: redo feature selection after the initial training\n";
    print STDERR "   -S        :: stop before retraining on FPs\n";    
    #print STDERR "   -s        :: start at retraining on FPs\n";    
    print STDERR "   -D        :: build the dictionary to include features from the supplied negatives\n";
    print STDERR "   -d        :: rebuild the dictionary to include features from FPs\n";
    print STDERR "   -z <n>    :: provide an id to use in the /tmp directory\n";
    print STDERR "   -F <n>    :: false positive threshold (default: $FP_THRESHOLD)\n";
    print STDERR "-------------------\n";

    print STDERR "   -p <dir>  :: positive examples directory \n";
    print STDERR "   -X        :: don't include the positives from the training images\n";
    print STDERR "   -n <dir>  :: negative examples directory (default: extract from training data)\n";
    print STDERR "   -N <dir>  :: extra folder containing subfolders of random objects\n";
    print STDERR "   -x        :: -includeOtherObjects parameter for buildTrainingImageDataset\n\n";

    print STDERR "   Algorithm for building the positive dataset (bounded at 20,000 examples):\n";
    print STDERR "      1. If -p is present, use the .jpg images from there\n";
    print STDERR "      2. Extract all positive examples from the training images\n";

    print STDERR "   Algorithm for building the negative dataset (bounded at 20,000 examples):\n";
    print STDERR "      1. If -N is present, use all the subfolders except the one that matches <object>\n";
    print STDERR "      2. If -n is present, augment with all images from there\n";
    print STDERR "      3. If insufficient data, extract random non-objects from the training images\n";
    print STDERR "          3.5 If -x is present, include first other labeled objects as negatives\n";
    print STDERR "      4. If -S not present, after training place all the FP examples at the beginning \n";
    print STDERR "\n";
    exit(-1);
}

###############################################
# Parameter processing
###############################################

my $object = $ARGV[0];

my $imageSeq = $opts{'r'} if (exists($opts{'r'}));
my $groundTruth = $opts{'G'} if (exists($opts{'G'})); 
my $testImageSeq = $opts{'t'} if (exists($opts{'t'}));
my $testGroundTruth = $groundTruth;

$OUTPUTDIR = $opts{'o'} if (exists($opts{'o'}));
$MODELDIR = $opts{'m'} if (exists($opts{'m'}));

my $POSDIR = $opts{'p'} if (exists($opts{'p'}));
my $NEGDIR = $opts{'n'} if (exists($opts{'n'}));
my $EXTRANEGDIR = $opts{'N'} if (exists($opts{'N'}));

#$NUM_NEG = $opts{'k'} if (exists($opts{'k'}));
$BASE_WIDTH = $opts{'w'} if (exists($opts{'w'}));
$BASE_HEIGHT = $opts{'h'} if (exists($opts{'h'}));
$KFOLD = $opts{'c'} if (exists($opts{'c'}));
$BOOSTING_ROUNDS = $opts{'b'} if (exists($opts{'b'}));
$CLASSIFIER_TYPE = $opts{'i'} if (exists($opts{'i'}));
$FEATURE_SELECTION_TYPE = $opts{'f'} if (exists($opts{'f'}));
$FP_THRESHOLD = $opts{'F'} if (exists($opts{'F'}));

$ID = "_$opts{'z'}" if (exists($opts{'z'}));

# setting up the temp data directories
my $TMPDATADIR = "/tmp/data${ID}/";
my $CACHEDIR = "/tmp/response-cache${ID}/";
my $NUM_SAMPLES_PER_IMAGE = 10;
my $dictFilename = "${MODELDIR}/${object}.dictionary.xml";
my $modelFilename = "${MODELDIR}/${object}.model";
my $outputFile = "${OUTPUTDIR}/${object}.output";
my $channelCommand = "";

#if ($CLASSIFIER_TYPE == 1) {
#    $TMPDATADIR = "/tmp/data_noedges/";
#    $CACHEDIR = "/tmp/response-cache-noedges/";
#    $NUM_SAMPLES_PER_IMAGE = 20;
#    $dictFilename = "${MODELDIR}/${object}.dictionary_noedges.xml";
#    $modelFilename = "${MODELDIR}/${object}.model_noedges";
#    $outputFile = "${OUTPUTDIR}/${object}.output_noedges";
#    $channelCommand = "-set svlVision.svlImageLoader channels \"INTENSITY .jpg\"";
#} elsif ($CLASSIFIER_TYPE == 2) {
#    $TMPDATADIR = "/tmp/data_coloredges/";
#    $CACHEDIR = "/tmp/response-cache-coloredges/";
#    $NUM_SAMPLES_PER_IMAGE = 10;
#    $dictFilename = "${MODELDIR}/${object}.dictionary_coloredges.xml";
#    $modelFilename = "${MODELDIR}/${object}.model_coloredges";
#    $outputFile = "${OUTPUTDIR}/${object}.output_coloredges";
#    $channelCommand = "-set svlVision.svlImageLoader channels \"INTENSITY .jpg EDGE_RGB .jpg\"";
#}

#} elsif ($CLASSIFIER_TYPE == 2) {
#    $TMPDATADIR = "/tmp/data_abs/";
#    $CACHEDIR = "/tmp/response-cache-abs/";
#    $NUM_SAMPLES_PER_IMAGE = 40;
#    $dictFilename = "${MODELDIR}/${object}.dictionary_abs.xml";
#    $modelFilename = "${MODELDIR}/${object}.model_abs";
#    $outputFile = "${OUTPUTDIR}/${object}.output_abs";
#    $channelCommand = "-set svlVision.svlImageLoader channels \"ABS .jpg\"";
#} elsif ($CLASSIFIER_TYPE == 3) {
#    $TMPDATADIR = "/tmp/data_absg/";
#    $CACHEDIR = "/tmp/response-cache-absg/";
#    $NUM_SAMPLES_PER_IMAGE = 40;
#    $dictFilename = "${MODELDIR}/${object}.dictionary_absg.xml";
#    $modelFilename = "${MODELDIR}/${object}.model_absg";
#    $outputFile = "${OUTPUTDIR}/${object}.output_absg";
#    $channelCommand = "-set svlVision.svlImageLoader channels \"ABSG .jpg\"";
#} elsif ($CLASSIFIER_TYPE == 4) {
#    $TMPDATADIR = "/tmp/data_abs_hsv/";
#    $CACHEDIR = "/tmp/response-cache-abs-hsv/";
#    $NUM_SAMPLES_PER_IMAGE = 40;
#    $dictFilename = "${MODELDIR}/${object}.dictionary_abs_hsv.xml";
#    $modelFilename = "${MODELDIR}/${object}.model_abs_hsv";
#    $outputFile = "${OUTPUTDIR}/${object}.output_abs_hsv";
#    $channelCommand = "-set svlVision.svlImageLoader channels \"ABS_HSV .jpg\"";
#}

my $FPDIRNAME = "${object}_FP/";
my $FPDIR = "${TMPDATADIR}/${FPDIRNAME}";
my $TRAINNEGDIRNAME = "${object}_neg/";
my $TRAINNEGDIR = "${TMPDATADIR}/${TRAINNEGDIRNAME}";
my $TRAINPOSDIRNAME = "${object}/";
my $TRAINPOSDIR = "${TMPDATADIR}/${TRAINPOSDIRNAME}";

my $CACHEPOS = "${CACHEDIR}${object}-pos";
my $CACHENEG = "${CACHEDIR}${object}-neg";

if (-e $outputFile) {
    unlink($outputFile);
}

if (!-d $OUTPUTDIR) {
    mkdir($OUTPUTDIR);
}
if (!-d $MODELDIR) {
    mkdir($MODELDIR);
}
if (!-d $TMPDATADIR) {
    mkdir($TMPDATADIR);
}
if (!-d $CACHEDIR) {
    mkdir($CACHEDIR);
}

if (-d $FPDIR) {
    rmtree($FPDIR);
}
mkdir($FPDIR);



#########################################################
# generate the extra data
###########################################################

my $max_images = $MAX_IMAGES_B;
my $kfold = $KFOLD;

my $num_training_images = 0;
if (exists($opts{'r'})) {
    my $FILE;
    open(FILE, "$imageSeq");
    my @lines = <FILE>;
    $num_training_images = @lines - 4;
    close(FILE);
}

my $num_provided_negatives = &count_provided_negatives();
my $num_provided_positives = &count_jpg_files_in_dir($POSDIR);

if (exists($opts{'r'})) {
    &generate_extra_samples;
}

my $num_generated_negatives = &count_jpg_files_in_dir($TRAINNEGDIR);
my $num_generated_positives = &count_jpg_files_in_dir($TRAINPOSDIR);

my $num_pos = $num_provided_positives + $num_generated_positives;
my $num_neg = $num_provided_negatives + $num_generated_negatives;

if ($num_pos <= 0) {
    print STDERR "No positive examples provided!";
    exit(-1);
}

# copy all the positive examples to the (already created) TRAINPOSDIR
if ($num_provided_positives > 0) {
    #`cp ${POSDIR}/* ${TRAINPOSDIR}`;
    &copy_dir($POSDIR, $TRAINPOSDIR);
}


###########################################################
# train the object detector on the generated data
###########################################################

### 1. Build dictionary

unlink($dictFilename) if (-e $dictFilename);

if (exists($opts{'D'})) {
    # build the dictionary using both the positive samples and the provided negatives
    my $n = &count_jpg_files_in_dir($NEGDIR) + $num_pos;
    my $file1 = "${dictFilename}.init.pos.xml";
    my $file2 = "${dictFilename}.init.neg.xml";
    $MAX_DICT_SIZE = $MAX_DICT_SIZE / 2;
    &build_dictionary($n, $file1, $TRAINPOSDIR);
    &build_dictionary($n, $file2, $NEGDIR);
    $MAX_DICT_SIZE = $MAX_DICT_SIZE * 2;
    $cmd = "${EXEBASE}combineDictionaries $dictFilename $file1 -1 $file2 -1";
    print "$cmd\n";
    `$cmd`;
} else {
   &build_dictionary($num_pos, $dictFilename, $TRAINPOSDIR);
}

if (exists($opts{'R'})) {
    $cmd = "cp $dictFilename ${dictFilename}.init";
    print "$cmd\n";
    `$cmd`;
}



### 2. Train the initial detector (with pre-caching)

&feature_select_with_caching();


### 4. Train the second (reasonable) detector

&cache_pos();
&cache_neg();
&train();


### 5. Save the dictionary and model files

$cmd = "cp $dictFilename ${dictFilename}.orig";
print "$cmd\n";
`$cmd`;

$cmd = "cp $modelFilename ${modelFilename}.orig";
print "$cmd\n";
`$cmd`;





##########################################################
# evaluate the current detector
##########################################################


### 1. Trim dictionary temporarily for faster detection

$cmd = "${SCRIPTDIR}trimDictionary.pl $dictFilename $modelFilename";
print "$cmd\n";
`$cmd  >> $outputFile`;



### 2. Evaluate the detector on the test set

if (exists($opts{'t'})) {
#    &evaluate("${OUTPUTDIR}/test", "orig", "$testImageSeq", "$testGroundTruth");
}


### 3. Evaluate the detector on the training set to obtain the FP

my $detectionsFilename;

if (exists($opts{'r'})) {
  $detectionsFilename= &evaluate("${OUTPUTDIR}/train", "orig", "$imageSeq", "$groundTruth");
}

if (exists($opts{'S'}) || !exists($opts{'r'})) {
    exit 0;
}



##############################################################################
# augment the training set, put the FPs into the FPDIR 
##############################################################################
&add_fp();


### 4. Restore the original dictionary, or re-do feature selection, or
### rebuild the whole dictionary from scratch using the false
### positives as well

if (exists($opts{'d'})) {
    my $num_fp = count_jpg_files_in_dir($FPDIR);
    my $n = $num_fp + $num_pos;
    my $file1 = "${dictFilename}.init2.pos.xml";
    my $file2 = "${dictFilename}.init2.FP.xml";
    $MAX_DICT_SIZE = $MAX_DICT_SIZE / 2;
    &build_dictionary($n, $file1, $TRAINPOSDIR);
    &build_dictionary($n, $file2, $FPDIR);
    $MAX_DICT_SIZE = $MAX_DICT_SIZE * 2;
    $cmd = "${EXEBASE}combineDictionaries ${dictFilename}.init2 $file1 -1 $file2 -1";
    print "$cmd\n";
    `$cmd`;

    $cmd = "cp ${dictFilename}.init2 $dictFilename";
    print "$cmd\n";
    `$cmd`;

    # re-cache everything
    &feature_select_with_caching();
    &cache_pos();
    &cache_neg();
    &cache_fp();
} elsif (exists($opts{'R'})) {
    $cmd = "cp ${dictFilename}.init $dictFilename";
    print "$cmd\n";
    `$cmd`;

    # re-cache everything, with the initial dictionary, and use it for
    # feature selection again (TODO: save files rather than recompute everything)
    &feature_select_with_caching();

    # we have now changed the dictionary, so have to re-cache everything for training
    &cache_pos();
    &cache_neg();
    &cache_fp();
} else {
    $cmd = "cp ${dictFilename}.orig $dictFilename";
    print "$cmd\n";
    `$cmd`;

    &cache_fp();
}


###########################################################
# re-train the object detector on the new data and trim it
###########################################################

&train();

$cmd = "${SCRIPTDIR}trimDictionary.pl $dictFilename $modelFilename";
print "$cmd\n";
`$cmd  >> $outputFile`;





###########################################################
# evaluate the new detector
###########################################################

if (exists($opts{'t'})) {
    &evaluate("${OUTPUTDIR}/test", "final", "$testImageSeq", "$testGroundTruth");
}

if (exists($opts{'r'})) {
    &evaluate("${OUTPUTDIR}/train", "final", "$imageSeq", "$groundTruth");
}

# clean-up?
#unlink($TMPDATADIR);
#unlink($CACHEDIR);










#######################################################################
# Functions
#######################################################################

############################
# Counts the provided files
############################

sub count_jpg_files_in_dir {
    my $DIR;
    my @files;
    my $dirname = $_[0];

    if (! -d $dirname) { 
	return 0;
    }

    opendir(DIR, "$dirname") or die $!;
    @files = grep(!/^\.\.?/, readdir(DIR));
    @files = grep(/.jpg/, @files);
    closedir(DIR);

    return @files;
}

sub copy_dir {
    my $from = $_[0];
    my $to = $_[1];
    if (!-d $from) {
	return 0;
    }
    if (!-d $to) {
	return 0;
    }
    opendir(DIR, "$from") or die $!;
    my @files = grep(!/^\.\.?/, readdir(DIR));
    @files = grep(/.jpg/, @files);
    my $f;
    foreach $f (@files) {
	`cp ${from}/${f} ${to}`;
    }
}

sub count_provided_negatives {
    my $DIR;
    my $num_other_samples = 0;
    my @files;

    if (exists($opts{'N'})) {
	opendir(DIR, "$EXTRANEGDIR") or die $!;

	# extract all files except the ".", "..", and $object
	@files = grep(!/^\.\.?/, readdir(DIR));
	@files = grep(!/${object}/, @files);

	my $name;
	foreach $name (@files) {
	    my $full_name = ${EXTRANEGDIR}.$name;
            # if it's a directory, count the files within it
	    if (-d $full_name) {
		$num_other_samples += &count_jpg_files_in_dir($full_name);
	    }
	}

	closedir(DIR);
    }

    if (exists($opts{'n'})) {
	$num_other_samples += &count_jpg_files_in_dir($NEGDIR);
    }

    return $num_other_samples;
}

###################
# dataset building
###################

sub generate_extra_samples {

    if ($num_training_images <= 0) {
	return;
    }

    if (-d "$TRAINPOSDIR") {
	rmtree("$TRAINPOSDIR");
    }
    mkdir("$TRAINPOSDIR");

    if (-d "$TRAINNEGDIR") {
	rmtree("$TRAINNEGDIR");
    }
    mkdir("$TRAINNEGDIR");

    
    $cmd = "${EXEBASE}buildTrainingImageDataset";
    $cmd = "$cmd -set svlVision.svlTrainingDatasetBuilder objects $object";

    if (exists($opts{'x'})) {
	$cmd = "$cmd -set svlVision.svlTrainingDatasetBuilder includeOtherObjects true";
    }
    if (exists($opts{'X'})) {
	$cmd = "$cmd -set svlVision.svlTrainingDatasetBuilder skipPos true"; 
    }

    # num samples is the number of negative images still left to add    
    my $num_samples = $max_images - $num_provided_negatives;
    if ($num_samples  > 0) {
	my $NUM_NEG = int($num_samples / $num_training_images + 1);
	my $ratio = ${BASE_WIDTH} / ${BASE_HEIGHT};

	$cmd = "$cmd -set svlVision.svlTrainingDatasetBuilder negSubdirName $TRAINNEGDIRNAME";
	$cmd = "$cmd -set svlVision.svlTrainingDatasetBuilder negHeight $BASE_HEIGHT";
	$cmd = "$cmd -set svlVision.svlTrainingDatasetBuilder negAspectRatio $ratio";
	$cmd = "$cmd -set svlVision.svlTrainingDatasetBuilder numNegs $NUM_NEG";
    } else {
	$cmd = "$cmd -set svlVision.svlTrainingDatasetBuilder skipNeg true"; 
    }

    $cmd = "$cmd -set svlVision.svlTrainingDatasetBuilder baseDir $TMPDATADIR";
    $cmd = "$cmd $channelCommand $imageSeq $groundTruth";
    print "$cmd\n";
    `$cmd >> $outputFile`;  

    #$cmd = "${SCRIPTDIR}shuffleDirectory.pl  ${TRAINNEGDIR}";
    #print "$cmd\n";
    #`$cmd >> $outputFile`;  
}

sub add_fp {
    # the FPs will account to up to half of the training set
    my $max_fp_images = $max_images / 2;
    my $NUM_NEG = int($max_fp_images / $num_training_images + 1);

    $cmd = "${EXEBASE}buildTrainingImageDataset";
    $cmd = "$cmd -set svlVision.svlTrainingDatasetBuilder objects $object";
    $cmd = "$cmd -set svlVision.svlTrainingDatasetBuilder skipPos true";
    $cmd = "$cmd -set svlVision.svlTrainingDatasetBuilder negSubdirName $FPDIRNAME";
    $cmd = "$cmd -set svlVision.svlTrainingDatasetBuilder numNegs $NUM_NEG";
    $cmd = "$cmd -set svlVision.svlTrainingDatasetBuilder falsePositivesOnly true";
    $cmd = "$cmd -set svlVision.svlTrainingDatasetBuilder detectionsFilename $detectionsFilename";
    $cmd = "$cmd -set svlVision.svlTrainingDatasetBuilder threshold $FP_THRESHOLD";
    $cmd = "$cmd -set svlVision.svlTrainingDatasetBuilder baseDir $TMPDATADIR";
    $cmd = "$cmd $channelCommand $imageSeq $groundTruth";
    print "$cmd\n";
    `$cmd >> $outputFile`;
}


#######################
# dictionary building
######################

sub build_dictionary {
    my ($num_images, $filename, $dir) = @_;

    # want to consider all of the given examples, so if possible, extract
    # less than the NUM_SAMPLES_PER_IMAGE patches from each example;
    # however, if not enough examples given, extract the maximum allowed

    my $num_samples_per_image = int ($MAX_DICT_SIZE / $num_images + 1);
    # hack; this is the only classifier type that uses two channels of input, and 
    # thus will produce double the expected number of features in the dictionary
    if ($CLASSIFIER_TYPE == 0 || $CLASSIFIER_TYPE == 2) {
	$num_samples_per_image = int ($num_samples_per_image / 2);
    }
    $NUM_SAMPLES_PER_IMAGE = min($NUM_SAMPLES_PER_IMAGE, $num_samples_per_image);
    $NUM_SAMPLES_PER_IMAGE = max($NUM_SAMPLES_PER_IMAGE, 1);

    $cmd = "${EXEBASE}buildPatchDictionary -useOldVersion -max_size $MAX_DICT_SIZE -o $filename -n $NUM_SAMPLES_PER_IMAGE $channelCommand";
    $cmd = "$cmd $dir $BASE_WIDTH $BASE_HEIGHT";
    print "$cmd\n";
    print `$cmd`;
}

##########
# caching
##########

# Input: inputDir
#        outputDir
#        startIndex
#        maxNumImages
# Output: $outputDir/$startIndex.bin files

sub cache_indiv {
    my ($inputDir, $outputDir, $startIndex, $maxNumImages) = @_;

    my $label = sprintf("%04d", $startIndex);
    $outputDir = $outputDir."/".$label;

    $cmd = "${EXEBASE}buildWindowFeatureCache -set svlBase.svlThreadPool threads 0 -maxImages $maxNumImages $channelCommand";
    $cmd = "$cmd -binary $inputDir $outputDir $dictFilename";
    print "$cmd\n";
    print `$cmd`;
}

sub cache_pos {
    if (-d "$CACHEPOS") {
	rmtree("$CACHEPOS");
    }
    mkdir("$CACHEPOS");
	
    &cache_indiv($TRAINPOSDIR, $CACHEPOS, 1, $max_images);
}

sub cache_fp {
    &cache_indiv($FPDIR, $CACHENEG, 0, $max_images);
}

sub cache_neg {
    my $DIR;
    my @files;

    # 1. If -N is present, use all the subfolders except the one that matches <object>

    if (-d "$CACHENEG") {
	rmtree("$CACHENEG");
    }
    mkdir("$CACHENEG");

    # want to start at incr to make sure FPs can later be added to the front of the list
    my $start = 1;

    if (exists($opts{'N'})) {
	opendir(DIR, "$EXTRANEGDIR") or die $!;

	# extract all files except the ".", "..", and $object
	@files = grep(!/^\.\.?/, readdir(DIR));
	@files = grep(!/${object}/, @files);

	my $name;
	foreach $name (@files) {
	    my $full_name = ${EXTRANEGDIR}.$name;
            # if it's a directory, proceed with caching
	    if (-d $full_name) {
		&cache_indiv($full_name, $CACHENEG, $start, $max_images);
		$start++;
	    }
	}

	closedir(DIR);
    }

    # 2. If -n is present, augment with all images from there

    if (exists($opts{'n'})) {
	&cache_indiv($NEGDIR, $CACHENEG, $start, $max_images);
	$start++;
    }

    # 3. If insufficient data and extracted some from the training directory
    
    if ($num_generated_negatives > 0) {
	&cache_indiv($TRAINNEGDIR, $CACHENEG, $start, $max_images);
    }
}

####################
# Feature selection
####################

sub feature_select {
    my $cachepos_file = "${CACHEPOS}/cached.bin";
    my $cacheneg_file = "${CACHENEG}/cached.bin";

    # to make sure we don't get itself as one of the files
    `cat ${CACHEPOS}/0* >& $cachepos_file`;
    `cat ${CACHENEG}/0* >& $cacheneg_file`;

    $cmd = "${EXEBASE}filterPatchDictionary  -set svlBase.svlThreadPool threads 0 -v -o $dictFilename -binary";
    $cmd = "$cmd -minSize $MIN_FINAL_DICT_SIZE -maxSize $MAX_FINAL_DICT_SIZE -maxInstances $max_images -binary";

    if ($FEATURE_SELECTION_TYPE == 2) {
	$cmd = "$cmd -MI $MI_THRESHOLD";
    }

    $cmd = "$cmd $dictFilename $cachepos_file $cacheneg_file";
    print "$cmd\n";
    `$cmd >> $outputFile`;

    unlink($cachepos_file);
    unlink($cacheneg_file);
}

sub feature_select_with_caching {
    if ($FEATURE_SELECTION_TYPE == 0) {
	$max_images = $MAX_IMAGES_A;
    } else {
	$max_images = $MAX_IMAGES_C;
    }

    &cache_pos();
    &cache_neg();
    &cache_fp();
    
    if ($FEATURE_SELECTION_TYPE == 0) {
	$kfold = -1;
	&train();
	$cmd = "${EXEBASE}../svl/scripts/trimDictionary.pl $dictFilename $modelFilename";
	print "$cmd\n";
	`$cmd >> $outputFile`;
	$kfold = $KFOLD;
    } else {
	&feature_select();
    }
    $max_images = $MAX_IMAGES_B;
}


###########
# Training
###########

sub train {

### 3. Train the detector
    my $cachepos_file = "${CACHEPOS}/cached.bin";
    my $cacheneg_file = "${CACHENEG}/cached.bin";

    # to make sure we don't get itself as one of the files
    `cat ${CACHEPOS}/0* >& $cachepos_file`;
    `cat ${CACHENEG}/0* >& $cacheneg_file`;

    unlink($modelFilename) if (-e $modelFilename);
    
    $cmd = "${EXEBASE}trainObjectDetector  -set svlBase.svlThreadPool threads 0 -o $modelFilename -maxInstances $max_images";
    $cmd = "$cmd -set svlML.svlBoostedClassifier numSplits $WEAK_LEARNER_SPLITS";
    $cmd = "$cmd -set svlML.svlBoostedClassifier boostingRounds $BOOSTING_ROUNDS";
    $cmd = "$cmd -cvn $kfold";
    $cmd = "$cmd -shuffle";
    $cmd = "$cmd -binary $cachepos_file $cacheneg_file";
    print "$cmd\n";
    `$cmd >> $outputFile`;

    unlink($cachepos_file);
    unlink($cacheneg_file);
}

#############
# evaluation
#############

# Arguments: output directory (should be different for train and testing evaluation),
#            prefix for output files (used to distinguish between original and final evaluation, and abs/intensity, etc.)
#            imageSeq
#            groundTruth
# Return: full name of the detection filename
sub evaluate {
    my ($outputDir,$prefix,$imageSeq,$groundTruth) = @_;

    if (!-d "${outputDir}") {
	mkdir("${outputDir}");
    }

    my $baseName = "${outputDir}/${prefix}";

    my $detectionsFilename = "${baseName}.detections.${object}.xml";
    my $prFilename = "${baseName}";
    my $resultFilename = "${baseName}.${object}.result";

    $cmd = "${EXEBASE}classifyImages  -set svlBase.svlThreadPool threads 0 -n $object -o $detectionsFilename -set svlVision.svlSlidingWindowDetector threshold 0.01 -t $FP_THRESHOLD -g $groundTruth -pr $prFilename ";
    $cmd = "$cmd $channelCommand $dictFilename $modelFilename $imageSeq";
    print "$cmd\n";
    print `$cmd`;

    #$cmd = "${EXEBASE}scoreDetections -object $object -pr $prFilename $groundTruth $detectionsFilename >& $resultFilename";
    #print "$cmd\n";
    #print `$cmd`;

    return $detectionsFilename;
}


# subroutine name: min
# Input: number1, number2
# returns less of 2 numbers
sub min {
    if ($_[0]>$_[1]) {return $_[1]} else {return $_[0]};
} 

sub max {
    if ($_[1]>$_[0]) {return $_[1]} else {return $_[0]};
} 

exit(0);



