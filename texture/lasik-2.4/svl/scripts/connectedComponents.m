% CONNECTEDCOMPONENTS  Finds the 4-connected or 8-connected components in
%                      an integer matrix.
%
% STAIR VISION PROJECT
% Copyright (c) 2007-2010, Stephen Gould
%
% Stephen Gould <sgould@stanford.edu>
%

function [cc, numComponents] = connectedComponents(seg, connectedness);

if (nargin < 2), connectedness = 4; end;
if ((connectedness ~= 4) && (connectedness ~= 8)),
    error('connectedness must be 4 or 8 for 4-connected or 8-connected');
end;

% number segments uniquely from 0 to numSegments - 1
seg = seg - min(seg(:));
segIds = sort(unique(seg(:)));
numSegments = length(segIds);
for i = 1:numSegments,
    seg(seg == segIds(i)) = i - 1;
end;

% find connected components
cc = zeros(size(seg));
numComponents = 0;
for k = 1:numSegments,
    [L, N] = bwlabel(seg == k - 1, connectedness);
    for n = 1:N,
        cc(L == n) = numComponents;
        numComponents = numComponents + 1;
    end;
end;
