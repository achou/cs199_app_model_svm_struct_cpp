function writeipl(file,ipl,prec)
%Write an IPL image.
%  WRITEIPL(FILE,IPL,PREC='float') writes bionary data as (width, height,
%  data in row-major order).
%
%  STAIR VISION LIBRARY
%  Copyright (c) 2009, Stanford University
%
%  AUTHOR(S): Paul Baumstarck <pbaumstarck@stanford.edu>

if nargin < 3 || isempty(prec)
    prec = 'float';
end
fp = fopen(file,'wb');
fwrite(fp,size(ipl,2),'int32'); % w
fwrite(fp,size(ipl,1),'int32'); % h
if ndims(ipl) == 2
    ipl = ipl';
end
fwrite(fp,ipl,prec);
fclose(fp);

