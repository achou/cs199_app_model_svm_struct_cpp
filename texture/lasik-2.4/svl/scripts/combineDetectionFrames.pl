#!/usr/bin/perl -I ../../external/perl5
#
# STAIR VISION LIBRARY
# Copyright (c) 2008, Stanford University
#
# FILENAME:    splitDetectionsByFrame.pl
# AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
# DESCRIPTION:
#  Given an object2dsequence file


use strict;
use Getopt::Std;
use File::Path;
use Cwd 'realpath';
use IO::Handle;

my $cmd;
my $cmdline;

#my %opts = ();
#getopts("", \%opts);
if ($#ARGV < 1) {
    print STDERR "USAGE: ./combineDetectionFrames.pl <outputFileName> <inputFile>+\n";
    exit(-1);
}

my $outFilename = $ARGV[0];
my $OUTFILE;
open(OUTFILE, ">$outFilename");
print OUTFILE "\<Object2dSequence version=\"1.0\"\>\n";

my $FILE;
my $line;
for (my $i = 1; $i <= $#ARGV; $i++) {
    my $filename = $ARGV[$i];
    print "$filename\n";
    open (FILE, "<${filename}");
    foreach $line (<FILE>) {
	if ($line !~ m/Object2dSequence/) {
	    print OUTFILE $line;
	} 
    }
    close(FILE);
}

print OUTFILE "\</Object2dSequence\>\n";

exit(0);




