/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlStructuredCloud.cpp
** AUTHOR(S):   Ian Goodfellow    <ia3n@cs.stanford.edu>
** DESCRIPTION: A renderable point cloud that exploits the fact that the 
**              points are sampled from a (possibly quite distorted) grid
**
*****************************************************************************/


#include "svlStructuredCloud.h"
#include <GL/gl.h>

void svlStructuredCloud::copy(const svlStructuredCloud & o)
{
  _empty = o._empty;				 
  if (!_empty)  
    {  
      SVL_ASSERT(o._x);				 
      SVL_ASSERT(o._y);				 
      SVL_ASSERT(o._z);				 
      SVL_ASSERT(o._x->rows == o._y->rows);		 
      SVL_ASSERT(o._x->cols == o._y->cols);		 
      SVL_ASSERT(o._x->rows == o._z->rows);		 
      SVL_ASSERT(o._x->cols == o._z->cols);		 
      _x = cvCloneMat(o._x);			 
      SVL_ASSERT(_x);				 
      _y = cvCloneMat(o._y);			 
      SVL_ASSERT(_y);				 
      _z = cvCloneMat(o._z);			 
      SVL_ASSERT(_z);				 
      if (o._nx)				 
	{					 
	  SVL_ASSERT(o._ny);			 
	  SVL_ASSERT(o._nz);			 
	  SVL_ASSERT(_x->rows == o._nx->rows);	 
	  SVL_ASSERT(_x->cols == o._nx->cols);	 
	  SVL_ASSERT(_x->rows == o._ny->rows);	 
	  SVL_ASSERT(_x->cols == o._ny->cols);	 
	  SVL_ASSERT(_x->rows == o._nz->rows);	 
	  SVL_ASSERT(_x->cols == o._nz->cols);	 
	  _nx = cvCloneMat(o._nx);		 
	  SVL_ASSERT(_nx);				 
	  _ny = cvCloneMat(o._ny);		 
	  SVL_ASSERT(_ny);				 
	  _nz = cvCloneMat(o._nz);		 
	  SVL_ASSERT(_nz);				 
	}					 
      else					 
	{					 
	  SVL_ASSERT(!o._ny);			 
	  SVL_ASSERT(!o._nz);			 
	  _nx = _ny = _nz = NULL;		 
	}					 
      if (o._bad)				 
	{					 
	  _bad = cvCloneMat(o._bad);		 
	  SVL_ASSERT(_bad);				 
	}					 
      else					 
	_bad = NULL;				 
    }						 
  else						 
    {						 
      _x = _y = _z = _nx = _ny = _nz = NULL;	 
    } 
}

svlStructuredCloud::svlStructuredCloud() : _empty(true), _x(NULL), _y(NULL),
					   _z( NULL), _nx(NULL), _ny(NULL),
					   _nz(NULL), _bad(NULL)
{

}


svlStructuredCloud::svlStructuredCloud(const CvMat * x, const CvMat *y, const CvMat * z,  
		     const CvMat * nx, const CvMat * ny,  
				       const CvMat * nz) : _empty(false), _bad(NULL)
{
  SVL_ASSERT(x);
  SVL_ASSERT(y);
  SVL_ASSERT(z);
  SVL_ASSERT(x->rows == y->rows);
  SVL_ASSERT(x->cols == y->cols);
  SVL_ASSERT(x->rows == z->rows);
  SVL_ASSERT(x->cols == z->cols);

  _x = cvCloneMat(x);
  SVL_ASSERT(_x);

  _y = cvCloneMat(y);
  SVL_ASSERT(_y);

  _z = cvCloneMat(z);
  SVL_ASSERT(_z);

  if (nx)
    {
      SVL_ASSERT(ny);
      SVL_ASSERT(nz);
      SVL_ASSERT(x->rows == nx->rows);
      SVL_ASSERT(x->cols == nx->cols);
      SVL_ASSERT(x->rows == ny->rows);
      SVL_ASSERT(x->cols == ny->cols);
      SVL_ASSERT(x->rows == nz->rows);
      SVL_ASSERT(x->cols == nz->cols);
      
      _nx = cvCloneMat(nx);
      SVL_ASSERT(_nx);

      _ny = cvCloneMat(ny);
      SVL_ASSERT(_ny);

      _nz = cvCloneMat(nz);
      SVL_ASSERT(_nz);
    }
  else
    {
      SVL_ASSERT(!ny);
      SVL_ASSERT(!nz);

      _nx = _ny = _nz = NULL;
    }
}


svlStructuredCloud::svlStructuredCloud(const svlStructuredCloud & o) : svlAbstractCloud(o)
{
  copy(o);
}


const svlStructuredCloud & svlStructuredCloud::operator=(const svlStructuredCloud & o)
{
  svlAbstractCloud::operator    =(o);   //not an emoticon!

  if (this != &o)
    {
      destroy();

      copy(o);
    }

  return *this;
}


svlStructuredCloud::~svlStructuredCloud()
{
  destroy();
}


void svlStructuredCloud::destroy()
{
  if (!_empty)
    {
      SVL_ASSERT(_x);
      cvReleaseMat(&_x);
      
      SVL_ASSERT(_y);
      cvReleaseMat(&_y);
      
      SVL_ASSERT(_z);
      cvReleaseMat(&_z);
      
       if (_nx)
	 {
	   cvReleaseMat(&_nx);
	   
	   SVL_ASSERT(_ny);
	   cvReleaseMat(&_ny);
	   
	   SVL_ASSERT(_nz);
	   cvReleaseMat(&_nz);
	 }
       else
	 {
	   SVL_ASSERT(!_ny);
	   SVL_ASSERT(!_nz);
	 }
       
       if (_bad)
	 {
	   cvReleaseMat(&_bad);
	 }
    }
  else
    {
      SVL_ASSERT( ! _x );
      SVL_ASSERT( ! _y );
      SVL_ASSERT( ! _z );

      SVL_ASSERT( ! _nx );
      SVL_ASSERT( ! _ny );
      SVL_ASSERT( ! _nz );

      SVL_ASSERT( ! _bad );
    }
}


void svlStructuredCloud::render(svlRenderType renderType, double option ) const
{
  preprocess(renderType);

  if (_empty)
    return;


  int c = _x->cols;
  int r = _x->rows;
  

  bool pretty = true; //if true we try to put gaps in our mesh where there are large discontinuities in the point cloud. if false we just render a fully connected mesh (which is what you want if you are trying to render a dense depth map)

  switch (renderType)
    {
    case SVL_RENDER_STRUCTURED_SURFACE:
      pretty = false;
    case SVL_RENDER_STRUCTURED_SURFACE_PRETTY:
      {
	//assume lighting is already configured
	
	
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	glEnable(GL_COLOR_MATERIAL);
	
	glBegin(GL_QUADS);
	
	
	if (pretty && (cloudUpdated || assumeCloudAlwaysUpdated))
	  {

	    if (!_bad)
	      _bad = cvCreateMat(r,c,CV_32FC1);
	    SVL_ASSERT(_bad);

	    cvZero(_bad);

	    for (int i = 0; i < r; i++)
	      {
		double curr_x = cvmGet(_x,i,0);
		double curr_y = cvmGet(_y,i,0);
		double curr_z = cvmGet(_z,i,0);

		for (int j = 1; j < c; j++)
		  {
		    double prev_x = curr_x;
		    double prev_y = curr_y;
		    double prev_z = curr_z;

		    curr_x = cvmGet(_x,i,j);
		    curr_y = cvmGet(_y,i,j);
		    curr_z = cvmGet(_z,i,j);

		    double dist2 = 0;



		    double diff = curr_x - prev_x;
		    dist2 += diff * diff;
		    
		    diff = curr_y - prev_y;
		    dist2 += diff * diff;

		    diff = curr_z - prev_z;
		    dist2 += diff * diff;

		    if (diff > option)
		      {
			cvmSet(_bad,i,j-1,1);
			cvmSet(_bad,i,j,1);
		      }
		  }
	      }

	    for (int j = 0; j < c; j++)
	      {
		double curr_x = cvmGet(_x,0,j);
		double curr_y = cvmGet(_y,0,j);
		double curr_z = cvmGet(_z,0,j);

		for (int i = 1; i < r; i++)
		  {
  double prev_x = curr_x;
		    double prev_y = curr_y;
		    double prev_z = curr_z;

		    curr_x = cvmGet(_x,i,j);
		    curr_y = cvmGet(_y,i,j);
		    curr_z = cvmGet(_z,i,j);

		    double dist2 = 0;



		    double diff = curr_x - prev_x;
		    dist2 += diff * diff;
		    
		    diff = curr_y - prev_y;
		    dist2 += diff * diff;
		    
		    diff = curr_z - prev_z;
		    dist2 += diff * diff;
		    
		    if (diff > option)
		      {
			cvmSet(_bad,i,j-1,1);
			cvmSet(_bad,i,j,1);
		      }
		  }
	      }
	    
	  }




	for (int i = 0; i < r-1; i++)
	  {
	    for (int j = 0; j < c-1; j++)
	      {
		//arrPos starts out as current point
		//order of inds is current point, j+1, (j and i)+1, j+1
		
		bool bads[4];
		int numBad = 0;

		int js [] = {j, j+1,j+1,j};
		int is [] = {i, i, i+1, i+1};

		if (pretty)
		  {
		    for (int nbi = 0; nbi < 4; nbi++)
		      {
			//int next = (nbi+1)%4;
			
			bads[nbi] = (cvmGet(_bad, is[nbi], js[nbi]) != 0.0);
			
			numBad += bads[nbi];
		      }
		  }

		if ( (!pretty) || (numBad == 0))
		  {
		    for (int qi = 0; qi < 4; qi++)
		      {
			svlPoint3d pt( cvmGet(_x, is[qi], js[qi]), \
				       cvmGet(_y, is[qi], js[qi]), \
				       cvmGet(_z, is[qi], js[qi]) );
			
			if (_nx)
			  _curNormal = svlPoint3d( cvmGet(_nx, is[qi], js[qi]), \
						 cvmGet(_ny, is[qi], js[qi]), \
						 cvmGet(_nz, is[qi], js[qi]));

			process(pt,false,true);
			glVertex3f(pt.x, pt.y, pt.z);
		      }
		  }
		else if (numBad ==1)
		  {
		    SVL_ASSERT(!pretty);

		    //Only one point is bad->draw the three good points as a triangle
		    glEnd();
		    glBegin(GL_TRIANGLES);


		    for (int ti = 0; ti < 4; ti++)
		      {
			if (bads[ti])
			  {
			    continue;
			  }

			svlPoint3d pt( cvmGet(_x, is[ti], js[ti]), \
				       cvmGet(_y, is[ti], js[ti]),	\
				       cvmGet(_z, is[ti], js[ti]) );
		
			if (_nx)
			  _curNormal = svlPoint3d( cvmGet(_nx, is[ti], js[ti]), \
						 cvmGet(_ny, is[ti], js[ti]), \
						 cvmGet(_nz, is[ti], js[ti]));
	
			process(pt,false,true);
			glVertex3f(pt.x, pt.y, pt.z);
		      }
		    
		    glEnd();
		    
		    
		    glBegin(GL_QUADS);
		  }//closes else if numBad == 1	    
		    
	      }//close j loop
	  }//close i loop

	glEnd();

	if (pretty)
	  {
	    glBegin(GL_POINTS);

	    for (int i = 0; i < r; i++)
	      {
		for (int j = 0; j < r; j++)
		  {
		    if (cvmGet(_bad, i,j))
		      {
			svlPoint3d pt( cvmGet(_x, i, j),   \
				       cvmGet(_y, i, j),   \
				       cvmGet(_z, i, j) );
			
			if (_nx)
			  _curNormal = svlPoint3d( cvmGet(_nx, i, j),	\
						 cvmGet(_ny, i, j), \
						 cvmGet(_nz, i, j));

			process(pt,true,false);
			glVertex3f(pt.x, pt.y, pt.z);
		      }
		  }
	      }
	    
	    glEnd();
	  }
      }//close this switch case's variable scop



      break;



    case SVL_RENDER_POINTS:
      
      glBegin(GL_POINTS);
      
      for (int i = 0; i < r; i++)
	{
	  for (int j = 0; j < c; j++)
	    {
	      svlPoint3d pt( cvmGet(_x, i, j),		   \
			     cvmGet(_y, i, j),		   \
			     cvmGet(_z, i, j) );
	      
	      if (_nx)
		_curNormal = svlPoint3d( cvmGet(_nx, i, j),	\
					 cvmGet(_ny, i, j),	\
					 cvmGet(_nz, i, j));
	      
	      process(pt,true,false);
	      glVertex3f(pt.x, pt.y, pt.z);	   
	    }
	}
      
      glEnd();

      break;

    default:
      SVL_LOG(SVL_LOG_FATAL, "svlStructuredCloud::render was passed unrecognized svlRenderType");
    }
  
  cloudUpdated = false;
}

unsigned svlStructuredCloud::size() const
{
  if (_empty)
    return 0;

  return _x->rows * _x->cols;
}

svlPoint3d svlStructuredCloud::getPoint(unsigned idx) const
{
  if (_empty)
    SVL_ASSERT(false);

  SVL_ASSERT(idx < size());

  unsigned i = idx / _x->cols;
  unsigned j = idx % _x->cols;

  svlPoint3d rval( cvmGet(_x, i , j), cvmGet(_y,i,j), cvmGet(_z,i,j));

  return rval;
}

svlObject3d svlStructuredCloud::boundingBox() const
{
  if (_empty)
    return svlObject3d(
		       svlPoint3d(0,0,0),
		       0,0,0);

  double minX = svlMin(_x);
  double maxX = svlMax(_x);
  double minY = svlMin(_y);
  double maxY = svlMax(_y);
  double minZ = svlMin(_z);
  double maxZ = svlMax(_z);

  svlPoint3d centroid( (minX + maxX) / 2.0, (minY+maxY)/2.0, (minZ+maxZ)/2.0 );
  

  return svlObject3d(
		     centroid,
		     (maxX - minX) / 2.0,
		     (maxY - minY) / 2.0,
		     (maxZ - minZ) /2.0

		     );
}
