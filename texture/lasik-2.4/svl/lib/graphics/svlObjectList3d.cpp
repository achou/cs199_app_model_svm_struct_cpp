/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2008, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlObjectList.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**
*****************************************************************************/

#if defined(_WIN32)||defined(WIN32)||defined(__WIN32__)
#define _CRT_SECURE_NO_DEPRECATE
#include <windows.h>
#undef max
#undef far
#undef near
#endif

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iterator>
#include <vector>
#include <cassert>
#include <string>

#include <GL/glu.h>

#include "svlBase.h"
#include "svlVision.h"
#include "svlObjectList3d.h"

using namespace std;


// svlObject3d Class --------------------------------------------------------

svlObject3d::svlObject3d() :
  name("[unknown]"), centroid(0.0, 0.0, 0.0), delta(0.1, 0.1, 0.1), theta(0.0), _hasColor(false)
{
    // do nothing
}

svlObject3d::svlObject3d(const svlObject3d& o) :
  name(o.name), centroid(o.centroid), delta(o.delta), theta(o.theta), _hasColor(o._hasColor), _r(o._r), _g(o._g), _b(o._b)
{
    // do nothing
}

svlObject3d::svlObject3d(double x, double y, double z, double d) :
  name("[unknown]"), centroid(x, y, z), delta(d, d, d), theta(0.0), _hasColor(false)
{
    // do nothing
}

svlObject3d::svlObject3d(const svlPoint3d& c, double d) :
  name("[unknown]"), centroid(c), delta(d, d, d), theta(0.0), _hasColor(false)
{
    // do nothing
}

svlObject3d::svlObject3d(const svlPoint3d& c, double dx, double dy, double dz) :
    name("[unknown]"), centroid(c), delta(dx, dy, dz), theta(0.0)
{
    // do nothing
}

svlObject3d::~svlObject3d()
{ 
    // do nothing
}

float svlObject3d::proportionContained(const vector<svlPoint3d> & v, unsigned int start, unsigned int stop, unsigned int inc) const
{
  SVL_ASSERT (start <= stop);
  SVL_ASSERT (stop < v.size());

  int sum = 0;
  for (unsigned int i = start; i <= stop; i+= inc)
    {
      sum += isContained(v[i]);
      //printf("\tpoint %d->%d\n",i,isContained(v[i]));
    }
  float rval =  float(sum)/float((stop-start)/inc+1);
  // printf("Resulting proportion: %f\n",rval);
  return rval;
}

float svlObject3d::proportionContained(const svlObject3d & other) const
{
  //cout<<"Overlap: ("<<centroid.x<<"+/-"<<delta.x<<","<<centroid.y<<"+/-"<<delta.y<<","<<centroid.z<<"+/-"<<delta.z<<") against  ("<<other.centroid.x<<"+/-"<<other.delta.x<<","<<other.centroid.y<<"+/-"<<other.delta.y<<","<<other.centroid.z<<"+/-"<<other.delta.z<<")"<<endl;

  //Find the range of the overlap in each dimension

  float left = (float)max(other.centroid.x - other.delta.x, centroid.x-delta.x);
  float right = (float)min(other.centroid.x + other.delta.x, centroid.x+delta.x);

  if (right < left)
    return 0;

  float near = (float)max(other.centroid.z - other.delta.z, centroid.z - delta.z);
  float far = (float)min(other.centroid.z + other.delta.z, centroid.z + delta.z);

  if (far < near)
    return 0;

  float bottom = (float)max(other.centroid.y - other.delta.y, centroid.y - delta.y);
  float top = (float)min(other.centroid.y + other.delta.y, centroid.y + delta.y);

  if (top < bottom)
    return 0;

  float intersectVolume = (right - left)*(far-near)*(top-bottom);

  return intersectVolume / other.volume();
}

float svlObject3d::volume() const
{
  return (float)(8.0 * delta.x * delta.y * delta.z);
}

bool svlObject3d::isContained(const svlPoint3d & p) const
{
  svlPoint3d diff = p - centroid;
  if ((fabs(diff.x) < delta.x) && (fabs(diff.y) < delta.y) && (fabs(diff.z) < delta.z)) {
    return true;
  }
  return false;
}


void svlObject3d::setColor(double r, double g, double b)
{
  _hasColor = true;
  _r = r;
  _g = g;
  _b = b;
}

void svlObject3d::setNoColor()
{
  _hasColor = false;
}

void svlObject3d::draw() const {
    // get oriented vertices
    vector<svlPoint3d> v = vertices();

    if (_hasColor)
      {
	glColor3f(_r,_g,_b);
      }


    // wireframe box
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glBegin(GL_QUAD_STRIP);
    for (unsigned i = 0; i < v.size() + 2; i++) {
	glVertex3f((GLfloat)v[i % v.size()].x, (GLfloat)v[i % v.size()].y, (GLfloat)v[i % v.size()].z);
    } 
    glEnd();
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

bool svlObject3d::hit(const svlPoint3d& u, const svlPoint3d& v) const {
    // TO DO [SG]: take into account orientation
    if ((u.x < (centroid.x - delta.x)) && (v.x < (centroid.x - delta.x))) return false;
    if ((u.x > (centroid.x + delta.x)) && (v.x > (centroid.x + delta.x))) return false;
    if ((u.y < (centroid.y - delta.y)) && (v.y < (centroid.y - delta.y))) return false;
    if ((u.y > (centroid.y + delta.y)) && (v.y > (centroid.y + delta.y))) return false;
    if ((u.z < (centroid.z - delta.z)) && (v.z < (centroid.z - delta.z))) return false;
    if ((u.z > (centroid.z + delta.z)) && (v.z > (centroid.z + delta.z))) return false;
    if ((u.x > (centroid.x - delta.x)) && (u.x < (centroid.x + delta.x)) &&
        (u.y > (centroid.y - delta.y)) && (u.y < (centroid.y + delta.y)) &&
        (u.z > (centroid.z - delta.z)) && (u.z < (centroid.z + delta.z))) {
            return true;
    }
    if ((v.x > (centroid.x - delta.x)) && (v.x < (centroid.x + delta.x)) &&
        (v.y > (centroid.y - delta.y)) && (v.y < (centroid.y + delta.y)) &&
        (v.z > (centroid.z - delta.z)) && (v.z < (centroid.z + delta.z))) {
            return true;
    }

    svlPoint3d hitPoint;
    if ((intersection(u.x - centroid.x + delta.x, v.x - centroid.x + delta.x, u, v, hitPoint) && insideBox(hitPoint, 0)) ||
        (intersection(u.y - centroid.y + delta.y, v.y - centroid.y + delta.y, u, v, hitPoint) && insideBox(hitPoint, 1)) ||
        (intersection(u.z - centroid.z + delta.z, v.z - centroid.z + delta.z, u, v, hitPoint) && insideBox(hitPoint, 2)) ||
        (intersection(u.x - centroid.x - delta.x, v.x - centroid.x - delta.x, u, v, hitPoint) && insideBox(hitPoint, 0)) ||
        (intersection(u.y - centroid.y - delta.y, v.y - centroid.y - delta.y, u, v, hitPoint) && insideBox(hitPoint, 1)) ||
        (intersection(u.z - centroid.z - delta.z, v.z - centroid.z - delta.z, u, v, hitPoint) && insideBox(hitPoint, 2))) {
            return true;
    }

    return false;
}

void svlObject3d::enableClipping() const
{
  GLdouble topEqn[] = { 0, -1, 0 , 0} 
  , bottomEqn[] = {0, 1, 0, 0}
  , temp[4];
  
  //Equation for the plane is the normal to the plane (+ n preserves points)
  //followed by the D term, which is -p dot n, where p is any point on the plane

  //Top equation is a downward normal and ymax
  topEqn[3] = centroid.y + delta.y;
  //Bottom equation is an upward normal and -ymin
  bottomEqn[3] = - (centroid.y - delta.y);

  glEnable(GL_CLIP_PLANE0);
  glEnable(GL_CLIP_PLANE1);
  
  glClipPlane(GL_CLIP_PLANE0, topEqn);
  glClipPlane(GL_CLIP_PLANE1, bottomEqn);
    

  svlPoint3d d1(delta.x,delta.y,delta.z);
  svlPoint3d d2(delta.x,delta.y,-delta.z);
  svlPoint3d d3(-delta.x,delta.y,-delta.z);

  d1.pan(theta);
  d2.pan(theta);
  d3.pan(theta);

  svlPoint3d brCorner = centroid + d1;
  svlPoint3d frCorner = centroid + d2;
  svlPoint3d flCorner = centroid + d3;

  //Back plane
  svlPoint3d normal = frCorner - brCorner;
  temp[0] = normal.x;
  temp[1] = normal.y;
  temp[2] = normal.z;
  temp[3] = -normal.dot(brCorner);
  glEnable(GL_CLIP_PLANE2);
  glClipPlane(GL_CLIP_PLANE2, temp);

//Front plane
  temp[0] = -normal.x;
  temp[1] = -normal.y;
  temp[2] = -normal.z;
  temp[3] = normal.dot(frCorner);
  glEnable(GL_CLIP_PLANE3);
  glClipPlane(GL_CLIP_PLANE3, temp);

//Right plane
  normal = flCorner - frCorner;
  temp[0] = normal.x;
  temp[1] = normal.y;
  temp[2] = normal.z;
  temp[3] = -normal.dot(frCorner);
  glEnable(GL_CLIP_PLANE4);
  glClipPlane(GL_CLIP_PLANE4, temp);

//Left plane
  temp[0] = -normal.x;
  temp[1] = -normal.y;
  temp[2] = -normal.z;
  temp[3] = normal.dot(flCorner);
  glEnable(GL_CLIP_PLANE5);
  glClipPlane(GL_CLIP_PLANE5, temp);
}

void svlObject3d::disableClipping() const
{
  glDisable(GL_CLIP_PLANE0);
  glDisable(GL_CLIP_PLANE1);
  glDisable(GL_CLIP_PLANE2);
  glDisable(GL_CLIP_PLANE3);
  glDisable(GL_CLIP_PLANE4);
  glDisable(GL_CLIP_PLANE5);
}

vector<svlPoint3d> svlObject3d::vertices() const
{
    svlPoint3d d1(delta.x, delta.y, delta.z);
    svlPoint3d d2(delta.x, delta.y, -delta.z);
    d1.pan(theta); d2.pan(theta);

    vector<svlPoint3d> v(8, centroid);
    v[0] += d1;
    v[1] += svlPoint3d(d1.x, -d1.y, d1.z);
    v[2] += d2;
    v[3] += svlPoint3d(d2.x, -d2.y, d2.z);
    v[4] -= svlPoint3d(d1.x, -d1.y, d1.z);
    v[5] -= d1;
    v[6] -= svlPoint3d(d2.x, -d2.y, d2.z);
    v[7] -= d2;

    return v;
}

inline bool svlObject3d::intersection(double d1, double d2, const svlPoint3d& p1, const svlPoint3d& p2, svlPoint3d& hitPoint) const {
    if ((d1 * d2 >= 0.0) || (d1 == d2)) return false;
    hitPoint = p1 - (d1/(d2 - d1)) * (p2 - p1);
    return true;
}

inline bool svlObject3d::insideBox(const svlPoint3d& hitPoint, const int axis) const {
  //IG-- this does not appear to take object orientation into account
    switch (axis) {
    case 0 :
	return ((hitPoint.z > (centroid.z - delta.z)) && 
		(hitPoint.z < (centroid.z + delta.z)) &&
		(hitPoint.y > (centroid.y - delta.y)) &&
		(hitPoint.y < (centroid.y + delta.y)));
    case 1 :
	return ((hitPoint.z > (centroid.z - delta.z)) && 
		(hitPoint.z < (centroid.z + delta.z)) &&
		(hitPoint.x > (centroid.x - delta.x)) &&
		(hitPoint.x < (centroid.x + delta.x)));
    case 2 :
	return ((hitPoint.x > (centroid.x - delta.x)) && 
		(hitPoint.x < (centroid.x + delta.x)) &&
		(hitPoint.y > (centroid.y - delta.y)) &&
		(hitPoint.y < (centroid.y + delta.y)));
    default : SVL_ASSERT(false);
    }

    return false;
}

// svlObject3dList Class -------------------------------------------------------

bool writeObject3dList(const char *filename, const svlObject3dList& list)
{
    ofstream ofs(filename);
    if (ofs.fail()) return false;

    ofs << "<Object3dList version=\"1.0\">" << endl;
    for (unsigned i = 0; i < list.size(); i++) {
        ofs << "    <Object name=\"" << list[i].name.c_str() << "\""
            << " centroid=\"" << list[i].centroid << "\""
            << " delta=\"" << list[i].delta << "\""
            << " theta=\"" << list[i].theta << "\" />" << endl;
    }
    ofs << "</Object3dList>" << endl;

    ofs.close();
    return true;
}

bool readObject3dList(const char *filename, svlObject3dList& list)
{
    list.clear();

#ifdef USE_PUG_XML
    xml_parser *xml = new xml_parser();
    if (!xml->parse_file(filename)) {
        delete xml;
        return false;
    }

    svlObject3d object;
    xml_node node = xml->document().child(0);
    for (xml_node::xml_node_iterator it = node.begin(); it != node.end(); ++it) {
        object.name = string((const char *)it->attribute("name"));
        if (sscanf((const char *)it->attribute("centroid"), "%lf %lf %lf", &object.centroid.x, &object.centroid.y, &object.centroid.z) != 3) {
            cerr << "ERROR: could not parse centroid \"" << (const char *)it->attribute("centroid") << "\"" << endl;
            return false;
        }
        if (sscanf((const char *)it->attribute("delta"), "%lf %lf %lf", &object.delta.x, &object.delta.y, &object.delta.z) != 3) {
            cerr << "ERROR: could not parse delta \"" << (const char *)it->attribute("delta") << "\"" << endl;
            return false;
        }
        object.theta = (double)it->attribute("theta");
        list.push_back(object);
    }

    delete xml;
#else
    XMLNode root = XMLNode::parseFile(filename, "Object3dList");
    if (root.isEmpty()) {
	return false;
    }

    svlObject3d object;
    for (int i = 0; i < root.nChildNode("Object"); i++) {
	XMLNode node = root.getChildNode("Object", i);
	object.name = node.getAttribute("name");
        if (sscanf(node.getAttribute("centroid"), "%lf %lf %lf", &object.centroid.x, &object.centroid.y, &object.centroid.z) != 3) {
            cerr << "ERROR: could not parse centroid \"" << node.getAttribute("centroid") << "\"" << endl;
            return false;
        }
        if (sscanf(node.getAttribute("delta"), "%lf %lf %lf", &object.delta.x, &object.delta.y, &object.delta.z) != 3) {
            cerr << "ERROR: could not parse delta \"" << node.getAttribute("delta") << "\"" << endl;
            return false;
        }
        object.theta = atof(node.getAttribute("theta"));
        list.push_back(object);	
    }
#endif

    return true;
}



svlObject3d svlObject3d::boundsOfContents(const vector<svlPoint3d> & contents) const
{
  vector<double> xs;
  vector<double> ys;
  vector<double> zs;

  for (int i = 0; i < (int)contents.size(); i++)
    {
      if (isContained(contents[i]))
	{
	  xs.push_back(contents[i].x);
	  ys.push_back(contents[i].y);
	  zs.push_back(contents[i].z);
	}
    }

  svlObject3d rval;


  svlPoint3d minP;
  svlPoint3d maxP;
  
  minP.x = minElem(xs);
  maxP.x = maxElem(xs);
 
  minP.y = minElem(ys);
  maxP.y = maxElem(ys);

  minP.z = minElem(zs);
  maxP.z = maxElem(zs);

  rval.centroid = (minP+maxP)/2.0;
  
  rval.delta = (maxP-minP)/2.0;

  return rval;
}

vector<svlPoint3d> svlObject3d::subsetContained(const vector<svlPoint3d> & v) const
{
    vector<svlPoint3d> rval;
    for (int i = 0; i < (int)v.size(); i++) {
	if (isContained(v[i]))
	    rval.push_back(v[i]);
    }

    return rval;
}


std::ostream & operator<<(std::ostream & os, const svlObject3d & obj)
{
    string str = obj.toString();
    os << str;
    return os;
}


string svlObject3d::toString() const
{
    stringstream s;
    s << "(" << name << "," << centroid << "," << delta << "," << theta << ")";

    return s.str();
}
