/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlAbstractCloud.cpp
** AUTHOR(S):   Ian Goodfellow <ia3n@cs.stanford.edu>
** DESCRIPTION:
**   A pure virtual class that other point cloud classes should subclass
**
*****************************************************************************/

#include "svlBase.h"
#include "svlAbstractCloud.h"
#include "svlPointColourer.h"


void svlAbstractCloud::preprocess(svlRenderType renderType) const
{
  bool point = false;
  bool surface = false;

  switch (renderType)
    {
    case SVL_RENDER_POINTS:
      point = true;
      break;

    case SVL_RENDER_STRUCTURED_SURFACE:
      surface = true;
      break;

    case SVL_RENDER_STRUCTURED_SURFACE_PRETTY:
      point = surface = true;
      break;

    default:
      SVL_LOG(SVL_LOG_FATAL, "svlAbstractCloud::preprocess was passed an unrecognized svlRenderType");
    }

  for (vector<const svlPointColourer *>::const_iterator i = universalCallbacks.begin(), last = universalCallbacks.end(); i != last; ++i)
    (*i)->preprocess(this);

  if (point)
    {
      for (vector<const svlPointColourer *>::const_iterator i = pointCallbacks.begin(), last = pointCallbacks.end(); i != last; ++i)
	(*i)->preprocess(this);
    }
  
  if (surface)
    {
      for (vector<const svlPointColourer *>::const_iterator i = surfaceCallbacks.begin(), last = surfaceCallbacks.end(); i != last; ++i)
    (*i)->preprocess(this);
    }
}

void svlAbstractCloud::process(const svlPoint3d & pt, bool usePointCallbacks, bool useSurfaceCallbacks) const
{
  //cout << "PROCESS CALLED" << endl;

  for (vector<const svlPointColourer *>::const_iterator i = universalCallbacks.begin(), last = universalCallbacks.end(); i != last; ++i)
    (*i)->colour(pt, this);

  if (usePointCallbacks)
    {
      for (vector<const svlPointColourer *>::const_iterator i = pointCallbacks.begin(), last = pointCallbacks.end(); i != last; ++i)
	(*i)->colour(pt, this);
    }
  
  if (useSurfaceCallbacks)
    {
      for (vector<const svlPointColourer *>::const_iterator i = surfaceCallbacks.begin(), last = surfaceCallbacks.end(); i != last; ++i)
	(*i)->colour(pt, this);
    }
}

svlAbstractCloud::~svlAbstractCloud()
{
}

svlAbstractCloud::svlAbstractCloud()
{
  cloudUpdated = true;
  assumeCloudAlwaysUpdated = false;
}
