/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlPointColourer.cpp
** AUTHOR(S):   Ian Goodfellow <ia3n@cs.stanford.edu>
** DESCRIPTION: a class used for callbacks for custom colorings of renderable meshes
**
*****************************************************************************/

#if defined(_WIN32)||defined(WIN32)||defined(__WIN32__)
#include <windows.h>
#endif

#include <GL/gl.h>
#include "svlPointColourer.h"

void svlPointColourer::preprocess(const svlAbstractCloud * cloud) const
{
}

svlDepthColourer::svlDepthColourer(double orig_x, double orig_y, double orig_z)
{
  _origin = svlPoint3d(orig_x, orig_y, orig_z);
  _nearColor = svlPoint3d(0.0, 0.0, 0.0);
  _farColor = svlPoint3d(1.0,1.0,1.0);

  _nearDist = 0;
  _farDist = 10;
}

void svlDepthColourer::colour(const svlPoint3d & pt, const svlAbstractCloud * cloud) const
{
  double dist = (pt-_origin).norm();

  if ( ! (dist >= 0) )
    {
      cout << "pt: " << pt << endl;
      cout << "origin: " << _origin << endl;
      cout << "Dist = " << dist << endl;
    }

  SVL_ASSERT(dist >= 0);

  double normDist = (dist-_nearDist) / _farDist;

  if (normDist > 1.0)
    normDist = 1.0;

  if (normDist < 0.0)
    normDist = 0.0;

  svlPoint3d colour = (1.0-normDist) * _nearColor + normDist * _farColor;
 
  /* 
  cout << "dist: "<<dist<<endl;
  cout << "_nearDist: "<<_nearDist<<endl;
  cout << "_farDist: "<<_farDist<<endl;
  cout << "normDist: "<<normDist<<endl;
  cout << "color.x: "<<colour.x<<endl;
  cout << "_nearColor: "<<_nearColor<<endl;
  cout << "_farColor: "<<_farColor<<endl;
  cout << "(1.0-normDist) * _nearColor: "<<(1.0-normDist) * _nearColor << endl;
  cout << "normDist * _farColor: "<< normDist * _farColor << endl;
  cout << "color: "<<colour<<endl;
  */


  glColor3f(colour.x, colour.y, colour.z);
}

svlNormalColourer::svlNormalColourer(double ref_x, double ref_y, double ref_z) : _ref(ref_x, ref_y, ref_z)
{
}

void svlNormalColourer::colour(const svlPoint3d & point, const svlAbstractCloud * cloud) const
{
  svlPoint3d normal = cloud->curNormal();

  normal = normal / sqrt( normal.x*normal.x + normal.y*normal.y + normal.z*normal.z);

  double cp = normal.x * _ref.x + normal.y * _ref.y + normal.z * _ref.z;

  cp = 0.5 + 0.5 * cp;

  if (cp < 0.0)
    {
      SVL_LOG(SVL_LOG_WARNING, "dot product between two supposed normal vectors came out as "<<cp);
      cp = 0.0;
    }
  if (isnan(cp) || isinf(cp))
    {
      SVL_LOG(SVL_LOG_WARNING, "normal colourer had to fix NaN or inf");
      cp = 0.5;
    }
  SVL_ASSERT(cp <= 1.0);

  glColor3f(cp,cp,cp);
}
