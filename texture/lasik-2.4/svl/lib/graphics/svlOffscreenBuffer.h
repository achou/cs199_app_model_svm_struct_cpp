/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlOffscreenBuffer.h
** AUTHOR(S):   Ian Goodfellow    <ia3n@cs.stanford.edu>
** DESCRIPTION: A class for rendering things in opengl to an image rather 
**              than to the screen
**
*****************************************************************************/

#ifdef SVL_OFFSCREEN_BUFFER

#pragma once

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glext.h>
#include "svlVision.h"


class svlOffscreenBuffer
{
 public:

  svlOffscreenBuffer(unsigned w, unsigned h);
  ~svlOffscreenBuffer();

  void setActive(bool active);

  IplImage * getImage() const; //caller becomes the owner of the returned image-- you can do anything you like to it / are responsible for freeing it

  static bool statusOK(GLenum status);
  static string statusToStr(GLenum status);

  protected:

  unsigned _w;
  unsigned _h;
  bool _active;

  GLuint _frameBufferObject;
  GLuint _depthBuffer;
  GLuint _texture;
  GLint _viewport[4];
  

};

#endif
