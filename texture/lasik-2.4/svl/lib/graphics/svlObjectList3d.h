/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2008, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlObjectList.h
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**              Olga Russakovsky (olga@cs.stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
** DESCRIPTION:
**  Classes for defining object lists in 2d and 3d.
**
*****************************************************************************/

#pragma once

#include <vector>
#include <iostream>

#include "svlBase.h"
#include "svlVision.h"

using namespace std;

// svlObject3d Class --------------------------------------------------------
// This class holds the definition of a single object in 3d space. The object
// bounding box can rotate about the y-axis, but is always aligned to the
// xz-plane. The class supports drawing using OpenGL.
//
class svlObject3d {
public:
    svlObject3d();
    svlObject3d(const svlObject3d& o);
    svlObject3d(double x, double y, double z, double d = 0.1);
    svlObject3d(const svlPoint3d& c, double d = 0.1);
    svlObject3d(const svlPoint3d& c, double dx, double dy, double dz);
    ~svlObject3d();

    //Sets OpenGL to clip polygons to fit inside the object
    void enableClipping() const;
    //Turns off the above
    void disableClipping() const;

    //Renders the object as a wireframe box
    void draw() const;
    bool hit(const svlPoint3d& u, const svlPoint3d& v) const;
    std::vector<svlPoint3d> vertices() const;

    //Returns the proportion of points in v with start <= index <= stop,
    // (index-start)%inc = 0
    //that are contained in the object. Does not take rotation into account yet.
    float proportionContained(const vector<svlPoint3d> & v, unsigned int start, unsigned int stop, unsigned int increment = 1) const;
    //Returns the proportion of the other box that is contained in this box. Does not
    //take orientation into account.
    float proportionContained(const svlObject3d & other) const;

    vector<svlPoint3d> subsetContained(const vector<svlPoint3d> & v) const;


    float volume() const;

    //Returns true if p is contained in the object. Does not take rotation into account yet.
    bool isContained(const svlPoint3d & p) const;

    //Returns a new object 3D that exactly matches the x, y, z extents
    //of the points that fall inside this object in the given point cloud
    //Note-- assumes box is axis-aligned (ignores theta)
    svlObject3d boundsOfContents(const vector<svlPoint3d> & contents) const;

    void setColor(double r, double g, double b);
    void setNoColor();


    friend std::ostream & operator<<(std::ostream & os, const svlObject3d & obj);

    string toString() const;


public:
    std::string name;   // name of object
    svlPoint3d centroid;   // object centroid
    svlPoint3d delta;      // half object size (extent is (c +/- d))
    double theta;       // orientation (in x-z plane)




protected:
    inline bool intersection(double d1, double d2, const svlPoint3d& p1, const svlPoint3d& p2, svlPoint3d& hitPoint) const;
    inline bool insideBox(const svlPoint3d& hitPoint, const int axis) const;
    bool _hasColor;      //If false, keeps last call to glColor when rendering
    double _r,_g,_b;       //Color of the wireframe box
};

// svlObject3dList Class ----------------------------------------------------

typedef std::vector<svlObject3d> svlObject3dList;

bool writeObject3dList(const char *filename, const svlObject3dList& list);
bool readObject3dList(const char *filename, svlObject3dList& list);


