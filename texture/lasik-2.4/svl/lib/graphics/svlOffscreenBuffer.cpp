/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlOffscreenBuffer.cpp
** AUTHOR(S):   Ian Goodfellow <ia3n@cs.stanford.edu>
** DESCRIPTION: A class for rendering things in opengl to an image rather 
**              than to the screen
**
*****************************************************************************/

#ifdef SVL_OFFSCREEN_BUFFER

#include "svlOffscreenBuffer.h"


svlOffscreenBuffer::svlOffscreenBuffer(unsigned w, unsigned h) :_w(w), _h(h),  _active(false)
{
  const GLubyte * strExt = glGetString(GL_EXTENSIONS);

  if (! gluCheckExtension((const GLubyte *)"GL_EXT_framebuffer_object",strExt))
    SVL_LOG(SVL_LOG_FATAL, "Your system does not support the OpenGL frame buffer object extension, so you can't use offscreen rendering.");

  _frameBufferObject = _depthBuffer = 0;

  //cout << "A: "<<statusToStr(glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT))<<endl;

  //make the FBO
  glGenFramebuffersEXT(1, &_frameBufferObject);
 
  //cout << "B: "<<statusToStr(glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT))<<endl;

  if (!_frameBufferObject) //not sure if this is a necessary condition for failure, but it sure seems like a sufficient condition
    SVL_LOG(SVL_LOG_FATAL, "Failed to allocate _frameBufferObject"); 

  //bind to the FBO
  setActive(true);

  //cout << "C: "<<statusToStr(glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT))<<endl;

  //Make a renderbuffer for our depth map, give it memory, and attach it to our FBO
  glGenRenderbuffersEXT(1, &_depthBuffer);

  //cout << "D: "<<statusToStr(glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT))<<endl;

  if (!_depthBuffer)
    SVL_LOG(SVL_LOG_FATAL, "Failed to allocate _depthBuffer");

  glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, _depthBuffer);
  //todo-- add check that this worked

  //cout << "E: "<<statusToStr(glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT))<<endl;

  glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT,GL_DEPTH_COMPONENT,_w,_h);
  //todo-- add check that this worked;

  //cout << "F: "<<statusToStr(glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT))<<endl;

  glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, _depthBuffer);
  //todo-- add check that this worked;

  //cout << "G: "<<statusToStr(glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT))<<endl;


  //Make a texture as the color buffer for our FBO, and attach it to the FBO
  glGenTextures(1, &_texture);
  

  //cout << "H: "<<statusToStr(glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT))<<endl;

  if (!_texture)
    SVL_LOG(SVL_LOG_FATAL, "Failed to allocate _texture");

  glBindTexture(GL_TEXTURE_RECTANGLE_NV, _texture);
  //todo-- add check that this worked;

  //cout << "I: "<<statusToStr(glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT))<<endl;

  glTexImage2D(GL_TEXTURE_RECTANGLE_NV, 0, GL_RGBA, _w, _h, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
  //todo-- add check that this worked;

  //cout << "J: "<<statusToStr(glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT))<<endl;

  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_RECTANGLE_NV, _texture, 0);
  //todo-- add check that this worked;

  GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);

  if (!statusOK(status))
    {
      SVL_LOG(SVL_LOG_FATAL, "Failed to make offscreen buffer of size "<<_w<<"x"<<_h<<". Failed with status "<<statusToStr(status));
    }
  

  setActive(false);
}

void svlOffscreenBuffer::setActive(bool active)
{
  if (_active == active)
    return;
  else
    {
      if (_active)
	{
	  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
	  glViewport(_viewport[0],_viewport[1],_viewport[2],_viewport[3]);
	  _active = false;
	}
      else
	{
	  glGetIntegerv(GL_VIEWPORT, _viewport);
	  glViewport(0,0,_w,_h);
	  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, _frameBufferObject);
	}
    }
}


svlOffscreenBuffer::~svlOffscreenBuffer()
{
  glDeleteFramebuffersEXT(1, &_frameBufferObject);
  glDeleteRenderbuffersEXT(1, &_depthBuffer);
}


IplImage * svlOffscreenBuffer::getImage() const
{
  IplImage * rval = cvCreateImage(cvSize(_w,_h), IPL_DEPTH_8U, 4);

  glGetTexImage(GL_TEXTURE_RECTANGLE_NV, 0, GL_RGBA, GL_UNSIGNED_BYTE, (GLvoid *) rval->imageData);

  return rval;
}


bool svlOffscreenBuffer::statusOK(GLenum status)
{
  return status == GL_FRAMEBUFFER_COMPLETE_EXT;
}

string svlOffscreenBuffer::statusToStr(GLenum status)
{
  switch(status)
    {
    case GL_FRAMEBUFFER_COMPLETE_EXT:
      
      return "GL_FRAMEBUFFER_COMPLETE_EXT";
      break;

    case GL_FRAMEBUFFER_UNSUPPORTED_EXT:
      /* this might just mean you need different formats ???  */ 
      return "GL_FRAMEBUFFER_UNSUPPORTED_EXT";
      break;
    case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT_EXT:
      return "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT_EXT";
      break;
    case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_EXT:
      return "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_EXT";
      break;
    case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT:
      return "GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT";
      break;
#if GL_GLEXT_VERSION < 39 // this define was removed in glext.h versions 39 and greater
    case GL_FRAMEBUFFER_INCOMPLETE_DUPLICATE_ATTACHMENT_EXT:
      return "GL_FRAMEBUFFER_INCOMPLETE_DUPLICATE_ATTACHMENT_EXT";
      break;
#endif
    case GL_FRAMEBUFFER_INCOMPLETE_FORMATS_EXT:
      return "GL_FRAMEBUFFER_INCOMPLETE_FORMATS_EXT";
      break;
    case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER_EXT:
      return "GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER_EXT";
      break; 
    case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER_EXT: 
      return "GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER_EXT";
      break; 
    case GL_FRAMEBUFFER_BINDING_EXT: 
      return "GL_FRAMEBUFFER_BINDING_EXT";

      break; 
      /* 
	 case GL_FRAMEBUFFER_STATUS_ERROR_EXT: 
	 fprintf(stderr,"framebuffer STATUS_ERRORn"); break; 
      */ 
    default: 
      SVL_LOG(SVL_LOG_FATAL, "svlOffscreenBuffer encountered unknown OpenGL error");
    }

  SVL_ASSERT(false);
  return "";
}

#endif
