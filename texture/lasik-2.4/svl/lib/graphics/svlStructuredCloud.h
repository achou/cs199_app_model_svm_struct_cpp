/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlStructuredCloud.h
** AUTHOR(S):   Ian Goodfellow    <ia3n@cs.stanford.edu>
** DESCRIPTION: A renderable point cloud that exploits the fact that the 
**              points are sampled from a (possibly quite distorted) grid
**
*****************************************************************************/

#pragma once

#include "svlAbstractCloud.h"
#include "svlObjectList3d.h"

class svlStructuredCloud : public svlAbstractCloud
{

public:

  //Makes an empty cloud
  svlStructuredCloud();

  //Makes a deep copy of all matrices-- client is responsible for freeing them
  svlStructuredCloud(const CvMat * x, const CvMat *y, const CvMat * z, \
		     const CvMat * nx = NULL, const CvMat * ny = NULL, \
		     const CvMat * nz = NULL);

  //Makes a deep copy
  svlStructuredCloud(const svlStructuredCloud & o);
  
  //Makes a deep copy
  const svlStructuredCloud & operator=(const svlStructuredCloud & o);


  virtual ~svlStructuredCloud();

  virtual void render(
		      svlRenderType renderType,		      
		      double option = 0.1) const; //option can be, i.e., the max depth change per polygon for RENDER_STRUCTURED_SURFACE_PRETTY

  virtual unsigned size() const;
  virtual svlPoint3d getPoint(unsigned idx) const;

  svlObject3d boundingBox() const;

 protected:

  void copy(const svlStructuredCloud & o);
  void destroy();

 protected:

  bool _empty;

  CvMat * _x;
  CvMat * _y;
  CvMat * _z;
  CvMat * _nx;
  CvMat * _ny;
  CvMat * _nz;

  mutable CvMat * _bad;

};
