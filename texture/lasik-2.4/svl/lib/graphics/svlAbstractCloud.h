/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlAbstractCloud.h
** AUTHOR(S):   Ian Goodfellow <ia3n@cs.stanford.edu>
** DESCRIPTION:
**   A pure virtual class that other point cloud classes should subclass
**
*****************************************************************************/

#pragma once

#include "svlVision.h"
#include <vector>
using namespace std;

class svlPointColourer;

enum svlRenderType
  {
    SVL_RENDER_POINTS, //just renders points in isolation
    SVL_RENDER_STRUCTURED_SURFACE, //uses the structure of the data to fit a surface, renders that
    SVL_RENDER_STRUCTURED_SURFACE_PRETTY, //like above, but attempts to automatically discard some polygons where it looks like there should be gaps in the mesh
  };



class svlAbstractCloud
{
 public:

  svlAbstractCloud();
  virtual ~svlAbstractCloud();

  virtual unsigned size() const = 0;
  virtual svlPoint3d getPoint(unsigned idx) const = 0;

  virtual void render(svlRenderType renderType, double option = 0.1) const = 0; //option can be, i.e., the max depth change per polygon for RENDER_STRUCTURED_SURFACE_PRETTY

  svlPoint3d curNormal() const { return _curNormal; }

 public:
  //Callbacks to be called per point regardless of renderType
  vector<const svlPointColourer *> universalCallbacks;
  //Callbacks to be called per point only for point rendering
  vector<const svlPointColourer *> pointCallbacks;
  //Callbacks to be called per point only for surface rendering
  vector<const svlPointColourer *> surfaceCallbacks;

  //Determine whether "bad" vertices will be recalculated on next
  //rendering pass of type SVL_RENDER_STRUCTURED_SURFACE_PRETTY
  mutable bool cloudUpdated; //defaults to true
  bool assumeCloudAlwaysUpdated; //defaults to false

 protected:

  void preprocess(svlRenderType renderType) const;
  void process(const svlPoint3d &, bool pointCallbacks, bool surfaceCallbacks) const;

 protected:
  
  mutable svlPoint3d _curNormal;

};
