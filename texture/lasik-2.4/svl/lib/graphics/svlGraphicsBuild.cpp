/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlGraphicsBuild.cpp
** AUTHOR(S):   Ian Goodfellow    <ia3n@cs.stanford.edu>
** DESCRIPTION: Dummy implementations of OpenGL functions that enable the
**              library to be built without OpenGL             
**
*****************************************************************************/

#if !USE_OPENGL

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glext.h>
#include "svlBase.h"

void svlOpenGLunimpMsg()
{
  SVL_LOG(SVL_LOG_FATAL, "Dying on attempt to use OpenGL function in library despite not building library with OpenGL enabled. To avoid this error, please set USE_OPENGL = 1 in your make.local file.");
}

#define SVL_OPENGL_UNIMPLEMENTED_VOID(x) void x {svlOpenGLunimpMsg();  }

#define SVL_OPENGL_UNIMPLEMENTED(t, x) t x { svlOpenGLunimpMsg(); t temp; return temp; }


SVL_OPENGL_UNIMPLEMENTED_VOID( glBegin(GLenum mode));
SVL_OPENGL_UNIMPLEMENTED_VOID( glDisable(GLenum cap));
SVL_OPENGL_UNIMPLEMENTED_VOID( glViewport(GLint x, GLint y, GLsizei width, GLsizei height));
SVL_OPENGL_UNIMPLEMENTED_VOID( glVertex3f (GLfloat x, GLfloat y, GLfloat z));
SVL_OPENGL_UNIMPLEMENTED_VOID( glColor3f (GLfloat red, GLfloat green, GLfloat blue));
SVL_OPENGL_UNIMPLEMENTED_VOID( glEnd (void));
SVL_OPENGL_UNIMPLEMENTED_VOID( glEnable (GLenum cap));
SVL_OPENGL_UNIMPLEMENTED_VOID( glPolygonMode (GLenum face, GLenum mode));
SVL_OPENGL_UNIMPLEMENTED_VOID( glGetIntegerv (GLenum pname, GLint *params));
SVL_OPENGL_UNIMPLEMENTED_VOID( glTexImage2D (GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const GLvoid *pixels));
SVL_OPENGL_UNIMPLEMENTED_VOID( glGenTextures (GLsizei n, GLuint *textures));
SVL_OPENGL_UNIMPLEMENTED_VOID( glBindTexture (GLenum target, GLuint texture));
SVL_OPENGL_UNIMPLEMENTED_VOID( glClipPlane (GLenum plane, const GLdouble *equation));
SVL_OPENGL_UNIMPLEMENTED_VOID( glGetTexImage (GLenum target, GLint level, GLenum format, GLenum type, GLvoid *pixels));
SVL_OPENGL_UNIMPLEMENTED(const GLubyte * , glGetString (GLenum name));
SVL_OPENGL_UNIMPLEMENTED_VOID( glColorMaterial (GLenum face, GLenum mode));
SVL_OPENGL_UNIMPLEMENTED(GLboolean , gluCheckExtension (const GLubyte *extName, const GLubyte *extString));
SVL_OPENGL_UNIMPLEMENTED_VOID( glDeleteFramebuffersEXT (GLsizei, const GLuint *));
SVL_OPENGL_UNIMPLEMENTED_VOID( glBindRenderbufferEXT (GLenum, GLuint));
SVL_OPENGL_UNIMPLEMENTED_VOID( glFramebufferTexture2DEXT (GLenum, GLenum, GLenum, GLuint, GLint));
SVL_OPENGL_UNIMPLEMENTED_VOID( glGenFramebuffersEXT (GLsizei, GLuint *));
SVL_OPENGL_UNIMPLEMENTED_VOID( glRenderbufferStorageEXT (GLenum, GLenum, GLsizei, GLsizei));
SVL_OPENGL_UNIMPLEMENTED_VOID( glDeleteRenderbuffersEXT (GLsizei, const GLuint *));
SVL_OPENGL_UNIMPLEMENTED_VOID( glGenRenderbuffersEXT (GLsizei, GLuint *));
SVL_OPENGL_UNIMPLEMENTED(GLenum , glCheckFramebufferStatusEXT(GLenum) );
SVL_OPENGL_UNIMPLEMENTED_VOID( glBindFramebufferEXT(GLenum, GLuint));
SVL_OPENGL_UNIMPLEMENTED_VOID( glFramebufferRenderbufferEXT (GLenum, GLenum, GLenum, GLuint));

#endif
