/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlPointColourer.h
** AUTHOR(S):   Ian Goodfellow <ia3n@cs.stanford.edu>
** DESCRIPTION: a class used for callbacks for custom colorings of renderable meshes
**
*****************************************************************************/

#pragma once

#include "svlAbstractCloud.h"

class svlPointColourer
{
 public:
    virtual ~svlPointColourer() {}

  //default is no-op
  virtual void preprocess(const svlAbstractCloud * cloud) const ;


  virtual void colour(const svlPoint3d & pt, const svlAbstractCloud * cloud) const = 0;
};

class svlDepthColourer : public svlPointColourer
{
 public:
  
  svlDepthColourer(double orig_x, double orig_y, double orig_z);
  void colour(const svlPoint3d & pt, const svlAbstractCloud * cloud) const;


 protected:

  svlPoint3d _origin;
  svlPoint3d _nearColor;
  svlPoint3d _farColor;
  double _nearDist;
  double _farDist;
};

class svlNormalColourer : public svlPointColourer
{
 public:

  svlNormalColourer(double ref_x, double ref_y, double ref_z);
  void colour(const svlPoint3d & pt, const svlAbstractCloud * cloud) const;

 protected:

  svlPoint3d _ref;
};
