/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlSlidingWindowDetector.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**              Sid Batra <sidbatra@stanford.edu>
**              Olga Russakovsky <olga@cs.stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
**
*****************************************************************************/

#include "svlFeatureExtractor.h"
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <sys/types.h>
#include <limits>

#include "cxcore.h"
#include "ml.h"

#include "svlBase.h"
#include "svlVision.h"

#if defined(_WIN32)||defined(WIN32)||defined(__WIN32__)
#define _CRT_SECURE_NO_DEPRECATE
#include "win32/dirent.h"
#else
#include <dirent.h>
#endif

#define VERBOSE
#undef DEBUG
//#define DEBUG

using namespace std;

// Statics and Globals -------------------------------------------------------
int svlSlidingWindowDetector::DELTA_X = 4;
int svlSlidingWindowDetector::DELTA_Y = 4;
double svlSlidingWindowDetector::DELTA_SCALE = 1.2;
double svlSlidingWindowDetector::THRESHOLD = 0.5;
bool svlSlidingWindowDetector::OUTPUT_RAW_SCORES = false;

// Constructors/Destructors --------------------------------------------------

svlSlidingWindowDetector::svlSlidingWindowDetector(const char *name, int w, int h)
{
  init(name, w, h);
}

svlSlidingWindowDetector::svlSlidingWindowDetector(const svlSlidingWindowDetector& c) :
  _classifier(c._classifier), _owns_classifier(false), _discarders(c._discarders), _owns_discarders(c._owns_discarders),
  _windowWidth(c._windowWidth), _windowHeight(c._windowHeight), _objectName(c._objectName),
  _threshold(c._threshold), _delta_x(c._delta_x), _delta_y(c._delta_y), _delta_scale(c._delta_scale)
{
  _extractor = c._extractor->clone();
  _owns_extractor = true;
}

svlSlidingWindowDetector::svlSlidingWindowDetector(const svlBinaryClassifier *c,
						   const svlFeatureExtractor *e)
{
  init();
  setModel(c);
  setFeatureExtractor(e);
}

svlSlidingWindowDetector::~svlSlidingWindowDetector()
{
  if (_owns_classifier && _classifier)
    delete _classifier;

  if (_owns_extractor && _extractor)
    delete _extractor;

  SVL_ASSERT(_discarders.size() == _owns_discarders.size());
  for ( unsigned i=0; i<_discarders.size(); ++i )
      if ( _owns_discarders[i] )
          delete _discarders[i];
}

void svlSlidingWindowDetector::init(const char *name, int w, int h)
{
  _classifier = NULL;
  _extractor = NULL;
  _owns_classifier = _owns_extractor = false;
  _discarders.push_back(new svlWindowDiscarder(w, h));
  _owns_discarders.push_back(true);

  _windowWidth = w;
  _windowHeight = h;
  
  if (name == NULL) {
    _objectName = "[unknown]";
  } else {
    _objectName = string(name);
  }
  
  _threshold = THRESHOLD;
  
  // set the default deltas
  resetDeltas();
}


// Public Member Functions --------------------------------------------------

void svlSlidingWindowDetector::setModel(const svlBinaryClassifier *c,
					bool owns_classifier)
{
  SVL_ASSERT(c);

  if (_owns_classifier && _classifier)
    delete _classifier;
  
  _classifier = c;
  _owns_classifier = owns_classifier;
}

void svlSlidingWindowDetector::setFeatureExtractor(const svlFeatureExtractor *e,
						   bool owns_extractor) 
{
  SVL_ASSERT(e);
  
  if (_owns_extractor && _extractor)
    delete _extractor;
  
  _extractor = e;
  _owns_extractor = owns_extractor;
  _windowWidth = e->windowWidth();
  _windowHeight = e->windowHeight();
  for ( vector<svlWindowDiscarder*>::iterator it=_discarders.begin(); it!=_discarders.end(); ++it )
    (*it)->setSize(_windowWidth, _windowHeight);
}

bool svlSlidingWindowDetector::load(const char *extractorFilename, const char *modelFilename)
{
  svlFeatureExtractor *e = svlFeatureExtractorFactory().load(extractorFilename);
  if (!e) return false;
  setFeatureExtractor(e, true);

  if ( modelFilename ) {
    svlBinaryClassifier *c = dynamic_cast<svlBinaryClassifier *>(svlClassifierFactory().load(modelFilename));
    if (!c) return false;
    setModel(c, true);
  }
  return true;
}

// Classifies a multi-channel image, optionally returning objects or features.
void svlSlidingWindowDetector::classifyImage(const vector<IplImage*> &images,
					     svlObject2dFrame *objects,
					     svlFeatureVectors *vecs)
{
  checkImageSizes(images);

  double scale = 1.0;
  int numChannels = (int)images.size();

  SVL_LOG(SVL_LOG_VERBOSE, "Running classifier on " << numChannels << "-channel image");

  // copy of original images for rescaling
  vector <IplImage *> scaledImages(numChannels);
  for (int i = 0; i < numChannels; i++) {
    scaledImages[i] = cvCloneImage(images[i]);
  }

  // Iterate over all scales
  while ((scaledImages[0]->width >= _windowWidth) &&
	 (scaledImages[0]->height >= _windowHeight)) 
    {
      // iterate over all locations
      vector<CvPoint> locations;
      createWindowLocations(scaledImages[0]->width, scaledImages[0]->height,
			    locations);
	
      unsigned numOrig = objects->size();
      classifySingleScale(scaledImages, locations, objects, vecs, false);
      for (unsigned i = numOrig; i < objects->size(); i++)
	objects->at(i).scale(scale);
      
      // scale down each image channel
      scale *= _delta_scale;
      
      for (int i = 0; i < numChannels; i++) {
	cvReleaseImage(&(scaledImages[i]));
	scaledImages[i] = cvCreateImage(cvSize((int)(images[i]->width / scale),
					       (int)(images[i]->height / scale)),
					images[i]->depth, images[i]->nChannels);
	cvResize(images[i], scaledImages[i]);
      }
    }
  
  // free memory
  for (int i = 0; i < numChannels ; i++) {
    cvReleaseImage(&(scaledImages[i]));
  }
}

void svlSlidingWindowDetector::classifyImageF(const vector<IplImage*>& imagesin,
					      svlObject2dFrame *objects,
					      svlFeatureVectorsF *vecs)
{
  if ( !vecs ) {
    classifyImage(imagesin, objects);
  } else {
    // Compute double features, then covert back to float.
    SVL_LOG(SVL_LOG_WARNING, "Called a double extractor asking for float features");
    svlFeatureVectors D;
    classifyImage(imagesin, objects, &D);
    D.convertTo(*vecs);
  }
}

// Classifies a multi-channel image, returning the results as a vector of images
void svlSlidingWindowDetector::classifyImage(const vector<IplImage*> &images,
					     vector<IplImage *> &output)
{
  checkImageSizes(images);

  double scale = 1.0;
  int numChannels = (int)images.size();

  SVL_LOG(SVL_LOG_VERBOSE, "Running classifier on " << numChannels << "-channel image");

  // copy of original images for rescaling
  vector <IplImage *> scaledImages(numChannels);
  for (int i = 0; i < numChannels; i++) {
    scaledImages[i] = cvCloneImage(images[i]);
  }

  output.clear();

  // Iterate over all scales
  while ((scaledImages[0]->width >= _windowWidth) &&
	 (scaledImages[0]->height >= _windowHeight)) 
    {
      IplImage *result = classifySingleScaleDense(scaledImages);
      output.push_back(result);
      
      // scale down each image channel
      scale *= _delta_scale;
      
      for (int i = 0; i < numChannels; i++) {
	cvReleaseImage(&(scaledImages[i]));
	scaledImages[i] = cvCreateImage(cvSize((int)(images[i]->width / scale),
					       (int)(images[i]->height / scale)),
					images[i]->depth, images[i]->nChannels);
	cvResize(images[i], scaledImages[i]);
      }
    }
  
  // free memory
  for (int i = 0; i < numChannels ; i++) {
    cvReleaseImage(&(scaledImages[i]));
  }
}


// Classifies a multi-channel image.
void svlSlidingWindowDetector::classifyImage(const vector<IplImage *>& images,
					     const map<double, vector<CvPoint> > &locationsWithScales,
					     map<double, svlObject2dFrame> &objects, bool sparse)
{
  checkImageSizes(images);

  int numChannels = (int)images.size();
  
  SVL_LOG(SVL_LOG_MESSAGE, "Running classifier on " << numChannels << "-channel image");
  
  vector <IplImage *> scaledImages(numChannels);
  for (int i = 0; i < numChannels; i++)
    scaledImages[i] = NULL;
  
  for (map<double, vector<CvPoint> >::const_iterator it = locationsWithScales.begin();
       it != locationsWithScales.end();
       it++) {
    double scale = it->first;
    vector<CvPoint> locations = it->second;

    for (int i = 0; i < numChannels; i++) {
      if (scaledImages[i])
	cvReleaseImage(&(scaledImages[i]));
      scaledImages[i] = cvCreateImage(cvSize((int)(images[i]->width / scale),
					     (int)(images[i]->height / scale)), images[i]->depth, images[i]->nChannels);
      cvResize(images[i], scaledImages[i]);
    }

    if ((scaledImages[0]->width < _windowWidth) || (scaledImages[0]->height < _windowHeight)) 
      continue;

    objects[scale] = svlObject2dFrame();
    classifySingleScale(scaledImages, locations, &(objects[scale]), NULL, sparse);
  }

  // free memory
  for (int i = 0; i < numChannels ; i++) {
    if (scaledImages[i])
      cvReleaseImage(&(scaledImages[i]));
  }
}


void svlSlidingWindowDetector::getAllWindows(int width, int height, svlObject2dFrame & windows) const
{
  windows.clear();
  int scaledWidth = width;
  int scaledHeight = height;

  double scale = 1.0;

  //iterate over all scales
  while ((scaledWidth >= _windowWidth) && (scaledHeight >= _windowHeight))
    {
      //iterate over all locations
      vector<CvPoint> locations;
      createWindowLocations(scaledWidth, scaledHeight, locations);

      windows.reserve(windows.size() + locations.size());
      for (unsigned i = 0; i < locations.size(); i++)
	windows.push_back( svlObject2d(locations[i].x * scale,
				       locations[i].y * scale,
				       _windowWidth * scale,
				       _windowHeight * scale));

      scale *= this->_delta_scale;

      scaledWidth = (int)(width / scale);
      scaledHeight = (int)(height / scale);
      
    }
}

void svlSlidingWindowDetector::getWindowsAtBaseScale(int width, int height, svlObject2dFrame & windows) const
{
  windows.clear();
  //iterate over all locations
  vector<CvPoint> locations;
  createWindowLocations(width, height, locations);

  windows.reserve(locations.size());
  for (unsigned i = 0; i < locations.size(); i++)
    windows.push_back( svlObject2d(locations[i].x, locations[i].y, _windowWidth, _windowHeight));
}


void svlSlidingWindowDetector::classifyImage(const vector<IplImage *>& images,
					     const vector<CvPoint>& locationsOrig,
					     svlObject2dFrame *objects,
					     svlFeatureVectors *vecs,
					     bool sparse)
{
  vector<CvPoint> locations = locationsOrig;
  classifySingleScale(images, locations, objects, vecs, sparse);
}

// float version of above.
void svlSlidingWindowDetector::classifyImageF(const vector<IplImage *>& images,
					      const vector<CvPoint>& locations,
					      svlObject2dFrame *objects,
					      svlFeatureVectorsF *vecs,
					      bool sparse)
{
  if ( !vecs ) {
    classifyImage(images, locations, objects, NULL, sparse);
  } else {
    // Compute double features, then covert back to float.
    SVL_LOG(SVL_LOG_WARNING, "Called a double extractor asking for float features");
    svlFeatureVectors D;
    classifyImage(images, locations, objects, &D, sparse);
    D.convertTo(*vecs);
  }
}


// evaluate classifier on given region (and subwindows)
void svlSlidingWindowDetector::classifySubImage(const vector<IplImage *>& images,
						CvRect region,
						svlObject2dFrame *objects,
						svlFeatureVectors *vecs)
{
  checkImageSizes(images);
  
  int numChannels = (int)images.size();
  
  // create subimages
  vector<IplImage *> subImages = cropAllImages(images, region);
  
  // run full detector on sub-images
  classifyImage(subImages, objects, vecs);
  
  for (unsigned i = 0; i < objects->size(); i++) {
    (*objects)[i].x += region.x;
    (*objects)[i].y += region.y;
  }

  // free memory
  for (int i = 0; i < numChannels; i++) {
    cvReleaseImage(&subImages[i]);
  }
}

// float version
void svlSlidingWindowDetector::classifySubImageF(const vector<IplImage *>& images,
    CvRect region,
    svlObject2dFrame *objects,
    svlFeatureVectorsF *vecs)
{
  if ( !vecs ) {
    classifySubImage(images, region, objects);
  } else {
    // Compute double features, then covert back to float.
    SVL_LOG(SVL_LOG_WARNING, "Called a double extractor asking for float features");
    svlFeatureVectors D;
    classifySubImage(images, region, objects, &D);
    D.convertTo(*vecs);
  }
}

// create sliding window locations for given scale ----------------------------

void svlSlidingWindowDetector::createWindowLocations(int width, int height,
    vector<CvPoint>& locations) const
{
    int maxX = width - _windowWidth;
    int maxY = height - _windowHeight;

    if ((maxX < 0) || (maxY < 0))
	return;

    locations.reserve((maxX + _delta_x) * (maxY + _delta_y) / (_delta_x * _delta_y));
    for (int x = 0; x <= maxX; x += _delta_x) {
	for (int y = 0; y <= maxY; y += _delta_y) {
	    locations.push_back(cvPoint(x, y));
	}
    }
}

void svlSlidingWindowDetector::createAllWindowLocations(int width, int height,
    map<double, vector<CvPoint> >& locations) const
{
    double scale = 1.0;
    int w = width;
    int h = height;
    locations.clear();
    while ((w >= _windowWidth) && (h >= _windowHeight)) {
	createWindowLocations(w, h, locations[scale]);
	scale *= _delta_scale;
	w = (int)(width / scale);
	h = (int)(height / scale);
    }    
}

// learning functions -------------------------------------------------------

double svlSlidingWindowDetector::evaluateModel(const vector<IplImage *>& images, CvRect region)
{
  checkImageSizes(images);
  
  int numChannels = (int)images.size();
  
  // create subimages
  vector<IplImage *> subImages = cropAllImages(images, region);

  for (unsigned i = 0; i < subImages.size(); i++)
    if (subImages[i]->width != _windowWidth || subImages[i]->height != _windowHeight)
      resizeInPlace(&(subImages[i]), _windowWidth, _windowHeight);
  
  // compute feature vector
  vector<double> fv;
   _extractor->windowResponse(subImages, fv);

#ifdef DEBUG
  for (unsigned i =0; i < fv.size(); i++)
    cerr << fv[i] << " ";
  cerr << endl;
  
#endif
  
  // free memory
  for (int i = 0; i < numChannels; i++) {
    cvReleaseImage(&subImages[i]);
  }
  
  float score = _classifier->getScore(fv);
    
  if (OUTPUT_RAW_SCORES) {
    return score;
  } else {
    return _classifier->scoreToProbability(score);
  }
}

void svlSlidingWindowDetector::checkImageSizes(const vector<IplImage *> &images)
{
  SVL_ASSERT(!images.empty());
  // each image in the vector must not be NULL and all images must be the same size
  for (unsigned i = 0; i < images.size(); i++) {
    SVL_ASSERT(images[i] != NULL);
    SVL_ASSERT(images[i]->width == images[0]->width);
    SVL_ASSERT(images[i]->height == images[0]->height);
  }
}

///////////////////////////////////////////////////////////////////////////////////////
//
// Main function in this class; computes features and optionally runs
// the classifier over a given set of locations at the base scale
//
///////////////////////////////////////////////////////////////////////////////////////

void svlSlidingWindowDetector::classifySingleScale(const vector<IplImage *>& images,
						   vector<CvPoint>& locations,
						   svlObject2dFrame *objects,
						   svlFeatureVectors *vecs,
						   bool sparse)
{
  SVL_ASSERT_MSG(objects || vecs, "classifyImage not asked to return anything");
  SVL_ASSERT_MSG(_extractor, "Need to provide an extractor first");
  SVL_ASSERT_MSG(!objects || _classifier, "Need to provide a classifier first");
  
  checkImageSizes(images);
  
  float scoreThreshold = _classifier->probabilityToScore((float)_threshold);
  
  discardInvalid(images[0], locations);
  discardUninteresting(images, 1.0, locations);
  
  // run classifier
  SVL_LOG(SVL_LOG_VERBOSE, "Computing features over " << locations.size()
	  << " windows for image size " << images[0]->width << " x " 
	  << images[0]->height << "...");
  
  vector<vector<double> > features;
  features.reserve(locations.size());
  if (objects)
    objects->reserve(objects->size() + locations.size());
  
  _extractor->extract(locations, images, features, sparse);

#ifdef DEBUG
  string filename = string("svlSlidingWindowDetector.classifyImage.")
    + toString(images[0]->width) + string(".") 
    + toString(images[0]->height) + string(".txt");

  ofstream ofs(filename.c_str());
  for (unsigned i = 0; i < features.size(); i++) {
    ofs << locations[i].x << " " << locations[i].y << " "
	<<  _windowWidth << " " << _windowHeight;
    for (unsigned j = 0; j < features[i].size(); j++) {
      ofs << " " << features[i][j];
    }
    ofs << "\n";
  }
  ofs.close();

  string filename2 = string("svlSlidingWindowDetector.classifyImage.")
    + toString(images[0]->width) + string(".") 
    + toString(images[0]->height) + string(".image.txt");
  
  ofstream ofs2(filename2.c_str());
  for (unsigned y = locations[0].y; y < _windowHeight; y++) {
    for (unsigned x = locations[0].x; x < _windowWidth; x++) {
      ofs2 << (int)(CV_IMAGE_ELEM(images[0], uchar, y, x)) << " ";
    }
    ofs2 << endl;
  }
  
  ofs2.close();

#endif

  SVL_LOG(SVL_LOG_VERBOSE, "...done");
  
  if ( vecs ) {
    vecs->push(images[0], locations, features);
  }
  if ( objects ) {
    SVL_LOG(SVL_LOG_VERBOSE, "Running multi-channel classifier on " << locations.size() << " windows...");
    for (unsigned i = 0; i < locations.size(); i++) {
      if(containsInvalidEntries(features[i])) continue;
      float score = _classifier->getScore(features[i]);
      if (score > scoreThreshold) {
	objects->push_back(svlObject2d(locations[i].x, locations[i].y,
				       _windowWidth, _windowHeight));
	objects->back().name = _objectName;
	if (OUTPUT_RAW_SCORES) {
	  objects->back().pr = score;
	} else {
	  objects->back().pr = _classifier->scoreToProbability(score);
	}
      }
    }
    SVL_LOG(SVL_LOG_VERBOSE, "...done");
  }
}

IplImage *svlSlidingWindowDetector::classifySingleScaleDense(const vector<IplImage *>& images)
{
  SVL_ASSERT_MSG(_extractor, "Need to provide an extractor first");
  SVL_ASSERT_MSG( _classifier, "Need to provide a classifier first");
  
  checkImageSizes(images);
  int w = images[0]->width;
  int h = images[0]->height;

  //float scoreThreshold = _classifier->probabilityToScore((float)_threshold);

  vector<CvPoint> locations;
  createWindowLocations(w, h, locations);

  discardInvalid(images[0], locations);
  discardUninteresting(images, 1.0, locations);
  
  // run classifier
  SVL_LOG(SVL_LOG_VERBOSE, "Computing features over " << locations.size()
	  << " windows for image size " << w << " x " << h << "...");
  
  // space-saving trick
  IplImage *output = cvCreateImage(cvSize(w/_delta_x, h/_delta_y), IPL_DEPTH_8U, 1);
  SVL_ASSERT(output);
  cvZero(output);

  if (OUTPUT_RAW_SCORES) {
    SVL_LOG(SVL_LOG_WARNING, 
	    "svlSlidingWindows::classifySingleScaleDense: All " <<
	    "uninteresting/discarded locations will have score " <<
	    "of 0; make sure this is the desired behavior.");
    SVL_LOG(SVL_LOG_WARNING, 
	    "svlSlidingWindows::classifySingleScaleDense: All " <<
	    "scores are assumed to be integers between 0 and 255.");
  }

  vector<vector<double> > features;
  _extractor->extract(locations, images, features, false);

  SVL_LOG(SVL_LOG_VERBOSE, "...done");
  
  SVL_LOG(SVL_LOG_VERBOSE, "Running multi-channel classifier on " << locations.size() << " windows...");
  for (unsigned i = 0; i < locations.size(); i++) {
    if(containsInvalidEntries(features[i])) continue;
    float score = _classifier->getScore(features[i]);

    int output_score = (int)score;
    if (!OUTPUT_RAW_SCORES) {
      float pr_score = _classifier->scoreToProbability(score);
      output_score = (int)(pr_score*255);
    } 

    CV_IMAGE_ELEM(output, uchar, locations[i].y/_delta_y, 
		  locations[i].x/_delta_x) = (uchar)output_score;
  }
  SVL_LOG(SVL_LOG_VERBOSE, "...done");
  return output;
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////


// svlSingleChannelSlidingWindowDetector -------------------------------------

svlSingleChannelSlidingWindowDetector::svlSingleChannelSlidingWindowDetector() :
    svlSlidingWindowDetector()
{
    // do nothing
}


svlSingleChannelSlidingWindowDetector::~svlSingleChannelSlidingWindowDetector()
{
    // do nothing
}

// evaluate classifer over all shifts and scales
void svlSingleChannelSlidingWindowDetector::classifyImage(IplImage *image,
							  svlObject2dFrame *objects,
							  svlFeatureVectors *vecs)
{
  vector<IplImage*> v(1, image);
  svlSlidingWindowDetector::classifyImage(v, objects, vecs);
}

// evaluate classifier at base scale and given shifts
void svlSingleChannelSlidingWindowDetector::classifyImage(IplImage *image,
							  const std::vector<CvPoint>& locations,
							  svlObject2dFrame *objects,
							  svlFeatureVectors *vecs,
							  bool sparse)
{
  vector<IplImage*> v(1, image);
  svlSlidingWindowDetector::classifyImage(v, locations, objects, vecs, sparse);
}

// evaluate classifier on given region (and subwindows)
void svlSingleChannelSlidingWindowDetector::classifySubImage(IplImage *image,
							     CvRect region,
							     svlObject2dFrame *objects,
							     svlFeatureVectors *vecs)
{
  vector<IplImage*> v(1, image);
  svlSlidingWindowDetector::classifySubImage(v, region, objects, vecs);
}

// evaluate the model on a given feature vector/patch
float svlSingleChannelSlidingWindowDetector::evaluateModel(IplImage * image,
							   CvRect region)
{
  vector<IplImage*> v(1, image);
  return svlSlidingWindowDetector::evaluateModel(v, region);
}

class svlSlidingWindowDetectorConfig : public svlConfigurableModule {
public:
  svlSlidingWindowDetectorConfig() : svlConfigurableModule("svlVision.svlSlidingWindowDetector") {}

  void usage(ostream &os) const {
    os << "      deltaX        :: shift in pixels in x-direction (default: 4)\n"
       << "      deltaY        :: shift in pixels in y-direction (default: 4)\n"
       << "      deltaScale    :: change in scales between successive levels (default: 1.2)\n"
       << "      threshold     :: minimum probability for detection to be returned (default: 0.5)\n"
       << "      rawScores     :: output raw scores instead of probabilities (default: false)\n";
  }

  void setConfiguration(const char *name, const char *value) {
      if (!strcmp(name, "deltaX")) {
          svlSlidingWindowDetector::DELTA_X = atoi(value);
      } else if (!strcmp(name, "deltaY")) {
          svlSlidingWindowDetector::DELTA_Y = atoi(value);
      } else if (!strcmp(name, "deltaScale")) {
          svlSlidingWindowDetector::DELTA_SCALE = atof(value);
      } else if (!strcmp(name, "threshold")) {
          svlSlidingWindowDetector::THRESHOLD = atof(value);
      } else if (!strcmp(name, "rawScores")) {
          svlSlidingWindowDetector::OUTPUT_RAW_SCORES = 
              (!strcasecmp(value, "TRUE") || !strcmp(value, "1"));
      } else {
          SVL_LOG(SVL_LOG_FATAL, "unrecognized configuration option for " << this->name());
      }
  }
};

static svlSlidingWindowDetectorConfig gSlidingWindowDetectorConfig;

