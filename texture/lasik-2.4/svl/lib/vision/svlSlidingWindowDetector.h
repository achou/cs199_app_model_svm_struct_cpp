/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlSlidingWindowDetector.h
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**              Sid Batra <sidbatra@stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
**              Olga Russakovsky <olga@cs.stanford.edu>
**              Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  Defines a multi-channel interface for sliding-window boosted-classifier 
**  object detectors. Derived classes must implement feature extraction 
**  functions and may override other functions for more efficient implementation.
**  Uses svlOptions interface for handling training options and basic feature
**  additions. The derived class svlSingleChannelSlidingWindowDetector implements
**  a single channel sliding-window detector.
**
*****************************************************************************/

#pragma once

#include <cassert>
#include <iostream>
#include <vector>
#include <map>

#include "cv.h"
#include "cxcore.h"
#include "ml.h"
#include "svlBase.h"
#include "svlObjectList.h"
#include "svlFeatureVectors.h"
#include "svlFeatureExtractor.h"
#include "svlML.h"
#include "svlWindowDiscarder.h"

using namespace std;


// svlSlidingWindowDetector ---------------------------------------------------

class svlSlidingWindowDetector {

 public:
    static int DELTA_X;
    static int DELTA_Y;
    static double DELTA_SCALE;
    static double THRESHOLD;
    static bool OUTPUT_RAW_SCORES; // instead of converting to probabilities

 protected:
    const svlBinaryClassifier *_classifier;
    bool _owns_classifier;

    const svlFeatureExtractor *_extractor;
    bool _owns_extractor;

    vector<svlWindowDiscarder*> _discarders;
    vector<bool> _owns_discarders;

    int _windowWidth, _windowHeight;
    string _objectName;
    double _threshold;
    
    // these are the ones that are actually used; can
    // be set by user and reset to the standard ones
    // as needed
    int _delta_x;
    int _delta_y;
    double _delta_scale;

 private:
    void init(const char *name = NULL, int w = 32, int h = 32);

 public:
    // Note: w and h will be overwritten by the extractor once it's passed in
    svlSlidingWindowDetector(const char *name = NULL, int w = 32, int h = 32);
    svlSlidingWindowDetector(const svlSlidingWindowDetector& c);
    // does not make deep copies; does not later free memory
    svlSlidingWindowDetector(const svlBinaryClassifier *c, const svlFeatureExtractor *e);
    virtual ~svlSlidingWindowDetector();

    // configuration and access functions (does not make a deep copy; only frees memory if second argument is 'true')
    void setModel(const svlBinaryClassifier *c, bool owns_classifier = false);
    void setFeatureExtractor(const svlFeatureExtractor *e, bool owns_extractor = false);

    // does free mamory later
    bool load(const char *extractorFilename, const char *modelFilename = NULL);

    int width() const { return _windowWidth; }
    int height() const { return _windowHeight; }
    virtual bool isInitialized() const { return _extractor && _classifier; }

    inline string getObjectName() const { return _objectName; }
    inline void setObjectName(string name) { _objectName = name; }
    inline void setObjectName(const char *name) { _objectName = string(name); }

    inline double getThreshold() const { return _threshold; }
    inline void setThreshold(double t) { _threshold = t; }

    inline double getDeltaScale() const { return _delta_scale; }
    inline int getDeltaX() const { return _delta_x; }
    inline int getDeltaY() const { return _delta_y; }

    inline void setDeltas(int dx, int dy, double dscale) {
      _delta_x = dx; _delta_y = dy; _delta_scale = dscale;
    }
    inline void resetDeltas() {
      _delta_x = DELTA_X; _delta_y = DELTA_Y; _delta_scale = DELTA_SCALE;
    }

    // Call discard methods on all issued discarders.
    inline void discardInvalid(const IplImage *img, vector<CvPoint> &locations) const {
      vector<svlWindowDiscarder*>::const_iterator it;
      for (it =_discarders.begin(); it!=_discarders.end(); ++it )
	(*it)->discardInvalid(img->width, img->height, locations);
    }
    inline void discardUninteresting(const vector<IplImage*> &images, double scale,
				     vector<CvPoint> &locations) const {
      vector<svlWindowDiscarder*>::const_iterator it;
      for (it =_discarders.begin(); it!=_discarders.end(); ++it )
	(*it)->discardUninteresting(images, scale, locations);
    }
    
    // Accessors to discarders.
    inline void pushDiscarder(svlWindowDiscarder *d) {
      _discarders.push_back(d);
      _owns_discarders.push_back(false);
    }
    inline void popDiscarder() {
      _discarders.pop_back();
      _owns_discarders.pop_back();
    }
    inline svlWindowDiscarder* getDiscarder(unsigned i = 0) {
      return _discarders[i];
    }
    inline svlWindowDiscarder* getBackDiscarder(unsigned i = 0) {
      return _discarders.back();
	}

    // Classifies a multi-channel image, optionally returning objects or features.
    // objects can be NULL, in which case classification is not performed.
    virtual void classifyImage(const vector<IplImage*> &images, 
			       svlObject2dFrame *objects,
			       svlFeatureVectors *vecs = NULL);
    // float version of above.
    virtual void classifyImageF(const vector<IplImage*>& imagesin,
				svlObject2dFrame *objects,
				svlFeatureVectorsF *vecs = NULL);

    // same as above, except instead of an object2dFrame returns a
    // vector of images, where each image corresponds to detections at
    // a certain scale, and every pixel value is the classifier's output
    // at that location
    // - threshold is ignored (no reason not to save all the
    //   detections, since the output is dense anyway)
    virtual void classifyImage(const vector<IplImage*> &images,
			       vector<IplImage *> &objects);

    //return list of windows at all shifts and scales
    void getAllWindows(int width, int height, svlObject2dFrame & windows) const ;
    void getWindowsAtBaseScale(int width, int height, svlObject2dFrame &windows) const;

    // Evaluate classifier at base scale and given locations.
    virtual void classifyImage(const vector<IplImage *>& images,
			       const vector<CvPoint>& locations,
			       svlObject2dFrame *objects,
			       svlFeatureVectors *vecs = NULL,
			       bool sparse = false);
    // float version
    virtual void classifyImageF(const vector<IplImage *>& images,
				const vector<CvPoint>& locations,
				svlObject2dFrame *objects,
				svlFeatureVectorsF *vecs = NULL,
				bool sparse = false);

    // Evaluate classifier on given region (and subwindows)
    virtual void classifySubImage(const vector<IplImage *>& images,
				  CvRect region,
				  svlObject2dFrame *objects,
				  svlFeatureVectors *vecs = NULL);
    // float version
    virtual void classifySubImageF(const vector<IplImage *>& images,
				   CvRect region,
				   svlObject2dFrame *objects,
				   svlFeatureVectorsF *vecs = NULL);
    
    // evaluate classifier at different scales and shifts
    void classifyImage(const vector<IplImage *>& images,
		       const map<double, vector<CvPoint> > &locationsWithScales,
		       map<double, svlObject2dFrame> &objects, bool sparse = false);

    // create sliding window locations for given scale
    void createWindowLocations(int width, int height, vector<CvPoint>& locations) const;
    void createAllWindowLocations(int width, int height, map<double, vector<CvPoint> >& locations) const;

    // after learning the model deletes all features that are not used
    // from the extractor
    // moved to trimFeatureExtractor.cpp app
    //void trimExtractor();

    double evaluateModel(const vector<IplImage *>& images, CvRect region);


 protected:
    void removeInvalidLocations(IplImage *image, vector<CvPoint> &locations);
    void removeUninterestingLocations(IplImage *image, vector<CvPoint> &locations);
    void removeUninterestingLocations(vector<IplImage *> images, vector<CvPoint> &locations);

    void checkImageSizes(const vector<IplImage *> &images);

    // all classifyImage functions must rely on one of these
    void classifySingleScale(const vector<IplImage *>& images, vector<CvPoint>& locations,
			     svlObject2dFrame *objects, svlFeatureVectors *vecs, bool sparse);
    IplImage *classifySingleScaleDense(const vector<IplImage *>& images);
};

// svlSingleChannelSlidingWindowDetector ------------------------------------

class svlSingleChannelSlidingWindowDetector : public svlSlidingWindowDetector {
 public:
	 svlSingleChannelSlidingWindowDetector();
	 ~svlSingleChannelSlidingWindowDetector();

	 // evaluate classifer over all shifts and scales
	 void classifyImage(IplImage *image, svlObject2dFrame *objects, svlFeatureVectors *vecs = NULL);
	 // evaluate classifier at base scale and given shifts
	 void classifyImage(IplImage *image,
		 const vector<CvPoint>& locations,
		 svlObject2dFrame *objects,
		 svlFeatureVectors *vecs = NULL,
		 bool sparse = false);
	 // evaluate classifier on given region (and subwindows)
	 void classifySubImage(IplImage *image, 
		 CvRect region,
		 svlObject2dFrame *objects,
		 svlFeatureVectors *vecs = NULL);

	 // evaluate the model on a given feature vector/patch
         float evaluateModel(IplImage * image,
             CvRect region = cvRect(0, 0, 0, 0));	
};


