/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlMIFeatureSelector.cpp
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
**
*****************************************************************************/

#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <sys/types.h>
#include <limits>
#include <cmath>

#include "svlBase.h"
#include "svlMIFeatureSelector.h"

using namespace std;

double svlMIFeatureSelector::MI_THRESHOLD = 0.0;

// static prototypes ------------------------------------------------------------

static double compute_MI_given_counts(int counts[2][2]);
static double compute_joint_MI_given_counts(int counts[2][2][2]);

// svlFeatureResponse -----------------------------------------------------------

void svlFeatureResponse::insert(double score, bool pos)
{
  if (pos)
    accumulatePositives(score);
  else
    accumulateNegatives(score);

  _responses.push_back(make_pair<double, bool>(score, pos));
}

void svlFeatureResponse::findHighestMI(double &bestMI, double &bestThreshold) const
{
  bestMI = -1;
  bestThreshold = -1;

  int posSum = INCLUDE_MISSES ? _numPositiveSamples : _numPositiveSamples - numMisses();
  int negSum = _numNegativeSamples;
  for (pr_counts_t::const_iterator it = _scoredResults.begin();
       it != _scoredResults.end(); it++) {

    // counts[x][c]: x = 1[value >= threshold]
    //             : c = 1[is positive detection]
    int counts[2][2];
    counts[0][0] = _numNegativeSamples - negSum; 
    counts[0][1] = _numPositiveSamples - posSum; // TODO: weighted by _posWeight?
    counts[1][0] = negSum;
    counts[1][1] = posSum;

    posSum -= it->second.posCount;
    negSum -= it->second.negCount;
   
    double MI = compute_MI_given_counts(counts);

    if (MI > bestMI) {
      bestMI = MI;
      bestThreshold = it->first;
    }
  }
}

void svlFeatureResponse::binarizeFeature(double threshold)
{
  for (unsigned i = 0; i < 2; i++)
    for (unsigned j = 0; j < 2; j++)
      _cache[i][j].clear();

  for (unsigned i = 0; i < _responses.size(); i++) {
    int featureFires = (_responses[i].first > threshold) ? 1 : 0;
    int examplePositive = (_responses[i].second) ? 1 : 0;

    _cache[featureFires][examplePositive].push_back(i);
  }
}

double svlFeatureResponse::computeJointMI(const svlFeatureResponse &feature) const
{
  // counts[x][y][c]: x = 1[feature X fires]
  //                  y = 1[feature Y fires]
  //                : c = 1[is positive detection]
  int counts[2][2][2];

  vector<int> result;
  for (int x = 0; x < 2; x++) {
    for (int y = 0; y < 2; y++) {
      for (int c = 0; c < 2; c++) {
	result.clear();
	set_intersection(_cache[x][c].begin(), _cache[x][c].end(),
			 feature._cache[y][c].begin(), feature._cache[y][c].end(),
			 inserter(result, result.begin()));
	counts[x][y][c] = result.size();
      }
    }
  }

  return compute_joint_MI_given_counts(counts);
}


// svlMIFeatureSelector ---------------------------------------------------------------

// threaded function 

#define TOTAL_THREADS 8

struct binarizeFeatureArgs {
  binarizeFeatureArgs(svlFeatureResponse &response,
		      double &MI,
		      const vector<vector<double> > &posSamples,
		      const vector<vector<double> > &negSamples,
		      int featureIndex) :
    _response(&response), _MI(&MI), _posSamples(&posSamples), 
    _negSamples(&negSamples), _featureIndex(featureIndex) {}

  svlFeatureResponse *_response;
  double *_MI;
  const vector<vector<double> > *_posSamples;
  const vector<vector<double> > *_negSamples;
  int _featureIndex;
};

void *binarizeFeature(void *voidArgs, unsigned tid) {
  binarizeFeatureArgs *args = (binarizeFeatureArgs *) voidArgs;
 
  svlFeatureResponse *response = args->_response;
  double &MI = *(args->_MI);
  const vector<vector<double> > &posSamples = *(args->_posSamples);
  const vector<vector<double> > &negSamples = *(args->_negSamples);
  const int i = args->_featureIndex; 

  for (unsigned j = 0; j < posSamples.size(); j++)
    response->insert(posSamples[j][i], true);
  for (unsigned j = 0; j < negSamples.size(); j++)
    response->insert(negSamples[j][i], false);

  double threshold;
  response->findHighestMI(MI, threshold);
  response->binarizeFeature(threshold);

  delete args;
  return NULL;
}

bool svlMIFeatureSelector::cacheExamples(const vector<vector<double> > &posSamples,
					 const vector<vector<double> > &negSamples)
{
  _features.clear();
  _features.resize(_numFeatures);

  _MI.clear();
  _MI.resize(_numFeatures);

  // binarize all the features and compute the mutual information between
  // each feature and the class label
  svlThreadPool threadPool(TOTAL_THREADS);
  threadPool.start();    
  for (unsigned i = 0; i < _numFeatures; i++) {
    threadPool.addJob(binarizeFeature,
		      new binarizeFeatureArgs(_features[i], _MI[i],
					      posSamples, negSamples, i));
					      
  }
  threadPool.finish();

  return true;
}

// threaded function

struct updateMIincreaseArgs {
  updateMIincreaseArgs(double &result, double origMI, // MI of the selected feature
		       const svlFeatureResponse &r1,
		       const svlFeatureResponse &r2) :
    _result(&result), _origMI(origMI), _response1(&r1), _response2(&r2) {}
  
  double *_result;
  double _origMI;
  const svlFeatureResponse *_response1;
  const svlFeatureResponse *_response2;
};
  
void *updateMIincrease(void *voidArgs, unsigned tid) {

  updateMIincreaseArgs *args = (updateMIincreaseArgs *) voidArgs;

  const svlFeatureResponse &response1 = *(args->_response1);
  const svlFeatureResponse &response2 = *(args->_response2);

  double jointMI = response1.computeJointMI(response2);
  double &result = *(args->_result);
  double origMI = args->_origMI;

  result = std::min(result, jointMI - origMI);  

  return NULL;
}

bool svlMIFeatureSelector::chooseFeatures(vector<bool> &selected)
{
  double max_MI = -1.0;
  int max_index = -1;
  for (unsigned i = 0; i < _numFeatures; i++) {
    if (_MI[i] > max_MI) {
      max_MI = _MI[i];
      max_index = i;
    }
  }
    
  // select the features with the highest mutual information
  selected[max_index] = true;
  _numSelectedFeatures++;
  SVL_LOG(SVL_LOG_VERBOSE, "Selected feature " << max_index << ", with MI of " << _MI[max_index]);
 
  // TODO: discard all features that are below a certain threshold in
  // terms of informativeness
 
  // stores the minimum jointMI between each features and any of the
  // features that have been selected so far
  vector<double> minMIincreaseWithSelected(_numFeatures, DBL_MAX);
  
  svlThreadPool threadPool(TOTAL_THREADS);
  while (_numSelectedFeatures < MAX_SIZE) {
    // update the minimum jointMI based on the last patch chosen
    threadPool.start();    
    for (unsigned i = 0; i < _numFeatures; i++) {
      if (selected[i]) continue;
      threadPool.addJob(updateMIincrease,
			new updateMIincreaseArgs(minMIincreaseWithSelected[i],
						 _MI[max_index], _features[i],
						 _features[max_index]));
    }
    threadPool.finish();
    
    // find the next best patch to add
    double MIincrease = -1.0;
    max_index = -1;
    for (unsigned i = 0; i < _numFeatures; i++) {
      if (selected[i]) continue;
      double increase = minMIincreaseWithSelected[i];
      if (increase > MIincrease) {
	MIincrease = increase;
	max_index = i;
      }
    }
  
    if (MIincrease < MI_THRESHOLD && _numSelectedFeatures >= MIN_SIZE)
      break;
    
    selected[max_index] = true;
    _numSelectedFeatures++;
    SVL_LOG(SVL_LOG_VERBOSE, "Selected feature " << max_index << ", with MI increase of " << MIincrease);
  }

  return true;
}



// Mutual information static functions --------------------------------------------

// counts[x][c]: x = 1[value >= threshold]
//             : c = 1[is positive detection]
static double compute_MI_given_counts(int counts[2][2])
{
  int total_count = counts[0][0] + counts[0][1] + counts[1][0] + counts[1][1];

 // computing MI(X; C) = sum_c,x p(c,x)log(p(c,x)/(p(c)*p(x)))
  double MI = 0;
  for (int x = 0; x < 2; x++) {   
    int count_of_x = counts[x][0] + counts[x][1];

    for (int c = 0; c < 2; c++) {
      int cur_count = counts[x][c];
      int count_of_c = counts[0][c] + counts[1][c];

      // 0 * log(0) = 0
      if (cur_count == 0) 
	continue;

      double pcx = (double)cur_count / (double)total_count;    // p(c,x)
      double pc = (double)count_of_c / (double)total_count; // p(c)
      double px = (double)count_of_x / (double)total_count; // p(x)
      
      MI += pcx * log(pcx / (pc * px)) / M_LN2;
    }
  }

  return MI;
}


// counts[x][y][c]: x = 1[feature X fires]
//                  y = 1[feature Y fires]
//                : c = 1[is positive detection]
static double compute_joint_MI_given_counts(int counts[2][2][2]) {
  int total_count = 0;
  for (int x = 0; x < 2; x++) {
    for (int y = 0; y < 2; y++) {
      for (int c = 0; c < 2; c++) {
	total_count += counts[x][y][c];
      }
    }
  }

 // computing MI(X, Y; C) = sum_c,x,y p(c,x,y)log(p(c,x,y)/(p(c)*p(x,y)))
  double MI = 0;
  for (int x = 0; x < 2; x++) {
    for (int y = 0; y < 2; y++) {
      int count_of_xy = counts[x][y][0] + counts[x][y][1];
   
      for (int c = 0; c < 2; c++) {
	int count_of_c = counts[0][0][c] + counts[0][1][c] + counts[1][0][c] + counts[1][1][c];
	int cur_count = counts[x][y][c];

	// 0 * log(0) = 0
	if (cur_count == 0) 
	  continue;

	double pcxy = (double)cur_count / (double)total_count;    // p(c,x,y)
	double pxy = (double)count_of_xy / (double)total_count;   // p(x,y)
	double pc = (double)count_of_c / (double)total_count;     // p(c)

	//if (count_of_xy == 0)
	//SVL_LOG(SVL_LOG_VERBOSE, "\tfor x = " << x << ", y = " << y << ", the count is 0");

	//if (cur_count == 0)
	//SVL_LOG(SVL_LOG_VERBOSE, "\tfor x = " << x << ", y = " << y << ", c = " << c << ", the count is 0");

	MI += pcxy * log(pcxy / (pc * pxy)) / M_LN2;
      }
    }
  }

  return MI;
}


// configuration ------------------------------------

class svlMIFeatureSelectorConfig : public svlConfigurableModule {
public:
    svlMIFeatureSelectorConfig() : svlConfigurableModule("svlML.svlMIFeatureSelector") { }

    void usage(ostream &os) const {
        os << "      threshold    :: minimum required MI increase (default: \"" << svlMIFeatureSelector::MI_THRESHOLD << "\")\n";
    }

    void setConfiguration(const char *name, const char *value) {
        if (!strcmp(name, "threshold")) {
	  svlMIFeatureSelector::MI_THRESHOLD = atoi(value);
        } else {
            SVL_LOG(SVL_LOG_FATAL, "unrecognized configuration option for " << this->name());
        }
    }
};

static svlMIFeatureSelectorConfig gMIFeatureSelectorConfig;
