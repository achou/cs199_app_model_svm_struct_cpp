/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlPRCurve.h
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**              Olga Russakovsky <olga@cs.stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
** DESCRIPTION:
**   Encapsulates a precision-recall curve. Allows results to be arbitrarily
**   scored. Higher score implies more likely. Positive samples can be weighted
**   differently from negative samples.
**
*****************************************************************************/

#pragma once

#include <cassert>
#include <iostream>
#include <vector>
#include <map>

#include "svlBase.h"
#include "svlHistogram.h"

using namespace std;

// svlClassifierSummaryStatistics -------------------------------------------
// TODO: come up with a shorter class name.
//


/* Stores a number of positive examples and a number of negative examples
   that were all given the same score */
struct svlPrCountEntry
{
svlPrCountEntry(int p, int n) : posCount(p), negCount(n) {}

  int posCount;
  int negCount;
};


typedef map<double, svlPrCountEntry > pr_counts_t;

class svlClassifierSummaryStatistics {
 public:
    static bool INCLUDE_MISSES;
    static std::string COL_SEP;
    static std::string ROW_BEGIN;
    static std::string ROW_END;

 protected:
    // number of positives and negatives grouped by score
    pr_counts_t _scoredResults;
    int _numPositiveSamples;  // must be greater than sum(_scoredResults.first)
    int _numNegativeSamples;  // must be must be equal to sum(_scoredResults.second)
    double _posWeight;        // weight of positive-to-negative count

 public:
    svlClassifierSummaryStatistics();
    svlClassifierSummaryStatistics(const svlClassifierSummaryStatistics& c);
    virtual ~svlClassifierSummaryStatistics();

    inline int numPositives() const { return _numPositiveSamples; }
    inline int numNegatives() const { return _numNegativeSamples; }
    inline int numSamples() const { return _numPositiveSamples + _numNegativeSamples; }
    inline int numPoints() const { return (int)_scoredResults.size(); }
    inline int numMisses() const;
    
    inline double getPosWeight() const { return _posWeight; }
    inline void setPosWeight(double w) { SVL_ASSERT(w > 0.0); _posWeight = w; }

    // this will change the weight of the positive examples such that
    // overall positive and negative examples will have the same weight
    inline void normalize() { _posWeight = _numNegativeSamples / _numPositiveSamples; }

    // i/o
    void clear();
    bool write(const char *filename) const;
    bool read(const char *filename);

    // modify the statistics
    void accumulate(const svlClassifierSummaryStatistics& c);
    void accumulatePositives(double score, int count = 1);
    void accumulatePositives(const vector<double>& scores);
    void accumulatePositives(const svlHistogram& counts);
    void accumulateNegatives(double score, int count = 1);
    void accumulateNegatives(const vector<double> &scores);
    void accumulateNegatives(const svlHistogram& counts);

    void accumulateMisses(int count = 1); // unscored missed positives

    // reduce the number of points in the curve aggregating nearby scores
    void quantize(double minThreshold = 0.0, double maxThreshold = 1.0, int numBins = 100);
 };

// svlPRCurve -------------------------------------------------------------------

struct svlRecallPrecision
{
  svlRecallPrecision() : r(-1.0), p(-1.0) {}
  svlRecallPrecision(double r, double p) : r(r), p(p) {}
  double r;
  double p;
};

typedef map<double, svlRecallPrecision > pr_storage_t;

class svlPRCurve : public svlClassifierSummaryStatistics {
 private:
   pr_storage_t _storedCurve;

  public:
    svlPRCurve();
    svlPRCurve(const svlClassifierSummaryStatistics& c);
    svlPRCurve(const svlPRCurve& c);
    virtual ~svlPRCurve();

    void writeCurve(const char *filename) const;
    void writeNormalizedCurve(const char *filename) const;
    bool readCurve(const char *filename);

    void writeAllInfo(const char * filepath);

    // statistics
    double averagePrecision(int numPoints = 11) const;
    double bestF1Score() const;
    double thresholdForBestF1Score() const;
    double F1ScoreAt(double threshold) const;
    double precisionAt(double threshold) const;
    double recallAt(double threshold) const;   
    // returns the threshold for reaching minimum precision and recall
    double thresholdForRecall(double recall) const;
    double thresholdForPrecision(double pr) const;

    string summaryAt(double threshold) const;
};

// svlClassifierSummaryStatistics inline implementation -------------------------

inline int svlClassifierSummaryStatistics::numMisses() const {
    int count = _numPositiveSamples;
    for (pr_counts_t::const_iterator it = _scoredResults.begin();
         it != _scoredResults.end(); it++) {
        count -= it->second.posCount;
    }
    return count;
}




#if 0

//
// OLD STUFF
//

typedef enum _svlPRpointSort_t {
  NONE, BY_RECALL, BY_PRECISION, BY_FSCORE
} svlPRpointSort_t;


#endif
