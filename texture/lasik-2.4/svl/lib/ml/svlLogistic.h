/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlLogistic.h
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
** DESCRIPTION:
**  The svlLogistic class implements a logistic regression classifier,
**      P(Y = 0 | X = x) = 1 / (1 + exp(-w^T x))
**  The classifier is trained using Newton's method.
**
**  The svlMultiClassLogistic class supercedes the svlLogistic class
**  and implements a multiclass logistic regression classifier,
**      P(Y = K | X = x) = 1 / (1 + sum exp(w_i^T x))
**      P(Y = k | X = x) = exp(w_k^T x) / (1 + sum exp(w_i^T x)), for 0 <= k < K
**  The classifier is trained using the svlOptimizer class.
**
*****************************************************************************/

#pragma once

#include <vector>
#include <limits>

#include "Eigen/Core"
#include "Eigen/Array"
#include "Eigen/Cholesky"

#include "svlBase.h"
#include "svlClassifier.h"
#include "svlBinaryClassifier.h"

using namespace std;
USING_PART_OF_NAMESPACE_EIGEN;

// svlLogistic class ---------------------------------------------------------

class svlLogistic : public svlBinaryClassifier {
 public:
    static int defaultMaxIterations;
    static double defaultEpsilon;
    
 private:
    VectorXd _theta;           // parameters
    VectorXd _gradient;        // gradient (during training)
    MatrixXd _hessian;         // hessian (during training)
    double _posClassWeight;    // relative weight of each positive sample
    double _lambda;            // regularization parameter (x 2)
    VectorXd _featureSum;      // feature mean for computing feature variance
    VectorXd _featureSum2;     // feature variance for scaling regularization
    int _numSamples;           // number of samples accumulated
    double _stepSize;          // truncated newton step size
    bool _bUseBiasTerm;        // adds a bias term to the feature vector

 public:
    svlLogistic();
    svlLogistic(unsigned n, double w = 1.0, double r = 1.0e-6, bool b = false);
    svlLogistic(const VectorXd& t);
    svlLogistic(const vector<double>& t);
    svlLogistic(const svlLogistic& model);
    ~svlLogistic();

    // initialize the parameters
    void initialize(unsigned n, double w = 1.0, double r = 1.0e-6, bool b = false);
    void initialize(const VectorXd& t);
    void initialize(const vector<double>& t);
    void initialize(unsigned n, unsigned k);
    void zero();
    void randomize();

    // i/o
    bool save(const char *filename) const;
    bool save(XMLNode &node) const;
    bool load(const char *filename) { return this->svlBinaryClassifier::load(filename); }
    bool load(XMLNode& node);

    // unnormalized score (required by svlBinaryClassifier)
    double getScore(const VectorXd  &sample) const;
    double getScore(const vector<double> &x) const;

    // evaluate model (caller is responsible for providing correct size matrix)
    // each row in x is a feature vector
    void getProbabilitiesFromMatrices(const MatrixXd& x, VectorXd& y) const;
    
    double mse(const VectorXd& y, const VectorXd& p) const;
    double mse(const vector<double>& y, const vector<double>& p) const;

    // accumulate evidence with data x, target y and prediction p
    void accumulate(const MatrixXd& x, const VectorXd& y, const VectorXd& p);
    void accumulate(const vector<vector<double> >& x, const vector<double>& y,
		    const vector<double> &p);

    // perform a single training update with regularization using accumulated data
    void update();

    // perform full training until mse no longer decreases
    double train(const MatrixXd & X, const VectorXi & Y);
    double train(const vector<vector<double> >& posSamples,
        const vector<vector<double> >& negSamples);
    double train(const MatrixXd& x, const VectorXd& y);
    double train(const vector<vector<double> >& x, const vector<double>& y);
 
    // access functions
    const VectorXd& parameters() const { return _theta; }
    int size() const { return _theta.rows(); }

    // standard operators
    double operator[](unsigned index) const { return _theta(index); }
    svlLogistic& operator=(const svlLogistic& model);
};

// svlMultiClassLogistic class -----------------------------------------------

class svlMultiClassLogistic : public svlClassifier {
private:
    vector<vector<double> > _theta;    // parameters (_nClasses - 1) * _nFeatures
    double _lambda;                    // regularization parameter

public:
    svlMultiClassLogistic(unsigned n = 1, unsigned k = 2, double r = 1.0e-9);
    svlMultiClassLogistic(const svlMultiClassLogistic& model);
    ~svlMultiClassLogistic();

    // initialize the parameters
    void initialize(unsigned n, unsigned k = 2, double r = 1.0e-9);
	void initialize(unsigned n, unsigned k);
    void zero();

    // i/o
    bool save(const char *filename) const;
	bool save(XMLNode &node) const;
    bool load(const char *filename) { return this->svlClassifier::load(filename); }
    bool load(XMLNode& node);

    // evaluate model
    void getClassScores(const VectorXd &x, VectorXd &y) const;
    void getClassScores(const vector<double>& x, vector<double>& y) const;

    // perform full training until log-likelihood no longer decreases
    double train(const MatrixXd & samples, const VectorXi & labels);
	double train(const MatrixXd & samples, const VectorXi & labels, const VectorXd &weights);
    double train(const vector<vector<double> >& x, const vector<int>& y);
    double train(const vector<vector<double> >& x, const vector<int>& y,
		 const vector<double>& w);

    // access functions
    vector<vector<double> > getTheta() const { return _theta; }
    void setTheta(const vector<vector<double> > &param);

    // standard operators
    svlMultiClassLogistic& operator=(const svlMultiClassLogistic& model);
};

// svlMultiClassLogisticOptimizer ------------------------------------------
// class to do all the grunt work for optimizing the multi-class logistic
class svlMultiClassLogisticOptimizer : public svlOptimizer
{
 public:
    int nClasses;
    int nFeatures;
    const vector<vector<double> > *pTrainingData;
    const vector<int> *pTrainingLabels;
    const vector<double> *pTrainingWeights;
    double lambda;

    svlMultiClassLogisticOptimizer() : svlOptimizer(), 
	pTrainingData(NULL), pTrainingLabels(NULL), lambda(0.0) { }
    svlMultiClassLogisticOptimizer(unsigned n) : svlOptimizer(n), 
	pTrainingData(NULL), pTrainingLabels(NULL), lambda(0.0) { }

    double objective(const double *x);
    void gradient(const double *x, double *df);
    double objectiveAndGradient(const double *x, double *df);
};



SVL_AUTOREGISTER_H( LOGISTIC , svlClassifier );
SVL_AUTOREGISTER_H( MULTICLASSLOGISTIC, svlClassifier);


