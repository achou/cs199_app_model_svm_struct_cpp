/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlSufficientStats.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**
*****************************************************************************/

#include <cassert>
#include <iostream>
#include <fstream>
#include <limits>
#include <cmath>

// SVL libraries
#include "svlBase.h"
#include "svlSufficientStats.h"

using namespace std;

// svlSufficientStats class --------------------------------------------------

svlSufficientStats::svlSufficientStats(int n, svlPairSuffStatsType pairStats) :
    _n(n), _pairStats(pairStats), _count(0.0)
{
    if (_n > 0) {
        clear(_n);
    }
}

svlSufficientStats::svlSufficientStats(const svlSufficientStats& stats) :
    _n(stats._n), _pairStats(stats._pairStats), _count(stats._count),
    _sum(stats._sum), _sum2(stats._sum2)
{
    // do nothing
}

svlSufficientStats::~svlSufficientStats()
{
    // do nothing
}

void svlSufficientStats::clear()
{
    _count = 0.0;
    _sum.setZero();
    _sum2.setZero();
}

void svlSufficientStats::clear(int n, svlPairSuffStatsType pairStats)
{
    SVL_ASSERT(n > 0);

    _n = n;
    _pairStats = pairStats;
    _count = 0.0;
    _sum = VectorXd::Zero(_n);
    switch (_pairStats) {
    case SVL_PSS_FULL:
        _sum2 = MatrixXd::Zero(_n, _n);
        break;
    case SVL_PSS_DIAG:
        _sum2 = MatrixXd::Zero(_n, 1);
        break;
    case SVL_PSS_NONE:
        _sum2 = MatrixXd::Zero(0, 0);
        break;
    }
}

void svlSufficientStats::diagonalize()
{
    switch (_pairStats) {
    case SVL_PSS_FULL:
        {
            MatrixXd t = _sum2.diagonal();
            _sum2 = t;
        }
        break;
    case SVL_PSS_DIAG:
        // do nothing
        break;
    case SVL_PSS_NONE:
        _sum2 = MatrixXd::Zero(_n, 1);
        break;
    }    
    _pairStats = SVL_PSS_DIAG;
}

void svlSufficientStats::save(const char *filename, bool bBinary) const
{
    SVL_ASSERT(filename != NULL);

    ofstream ofs(filename, bBinary ? ios::out | ios::binary : ios::out);
    SVL_ASSERT(!ofs.fail());
    this->save(ofs, bBinary);
    ofs.close();
}

void svlSufficientStats::save(ostream& os, bool bBinary) const
{
    if (bBinary) {
        SVL_ASSERT_MSG(false, "TODO");
    } else {
        os << _n << "\n\n";
        os << _count << "\n\n";
        os << _sum << "\n\n";
        os << secondMoments() << "\n";
    }
}

void svlSufficientStats::load(const char *filename, bool bBinary)
{
    SVL_ASSERT(filename != NULL);

    ifstream ifs(filename, bBinary ? ios::in | ios::binary : ios::in);
    SVL_ASSERT(!ifs.fail());
    this->load(ifs, bBinary);
    SVL_ASSERT(!ifs.fail());
    ifs.close();
}

void svlSufficientStats::load(istream& is, bool bBinary)
{
    int n;

    if (bBinary) {
        SVL_ASSERT_MSG(false, "TODO");
    } else {
        is >> n;
        this->clear(n);

        is >> _count;
        for (int i = 0; i < _n; i++) {
            is >> _sum(i);
        }
        MatrixXd tmp(_n, _n);
        for (int i = 0; i < _n; i++) {
            for (int j = 0; j < _n; j++) {
                is >> tmp(i, j);
            }
        }

        switch (_pairStats) {
        case SVL_PSS_FULL:
            _sum2 = tmp;
            break;
        case SVL_PSS_DIAG:
            _sum2 = tmp.diagonal();
            break;
        default:
            // do nothing
            break;
        }
    }
}

void svlSufficientStats::accumulate(const vector<double>& x)
{
    SVL_ASSERT_MSG((int)x.size() == _n, x.size() << " != " << _n);

    _sum += Eigen::Map<VectorXd>(&x[0], x.size());
    switch (_pairStats) {
    case SVL_PSS_FULL:
        _sum2 += Eigen::Map<VectorXd>(&x[0], x.size()) *
            Eigen::Map<VectorXd>(&x[0], x.size()).transpose();
        break;
    case SVL_PSS_DIAG:
        _sum2 += Eigen::Map<MatrixXd>(&x[0], x.size(), 1).cwise().square();
        break;
    default:
        // do nothing
        break;
    }
    _count += 1.0;
}

void svlSufficientStats::accumulate(const vector<double>& x, double w)
{
    SVL_ASSERT_MSG((int)x.size() == _n, x.size() << " != " << _n);

    VectorXd t(w * Eigen::Map<VectorXd>(&x[0], x.size()));
    _sum += t;
    switch (_pairStats) {
    case SVL_PSS_FULL:
        _sum2 += Eigen::Map<VectorXd>(&x[0], x.size()) * t.transpose();
        break;
    case SVL_PSS_DIAG:
        _sum2 += w * Eigen::Map<MatrixXd>(&x[0], x.size(), 1).cwise().square();
        break;
    default:
        // do nothing
        break;
    }
    _count += w;
}

void svlSufficientStats::accumulate(const vector<vector<double> >& x, double w)
{
    svlSufficientStats stats(_n, _pairStats);
    for (vector<vector<double> >::const_iterator it = x.begin(); it != x.end(); ++it) {
        stats.accumulate(*it);
    }
    accumulate(stats, w);
}

void svlSufficientStats::accumulate(const svlSufficientStats& stats, double w)
{
    SVL_ASSERT(stats._n == _n);

    _count += w * stats._count;
    _sum += w * stats._sum;
    switch (_pairStats) {
    case SVL_PSS_FULL:
        switch (stats._pairStats) {
        case SVL_PSS_FULL:
            _sum2 += w * stats._sum2;
            break;
        case SVL_PSS_DIAG:
            _sum2 += w * stats._sum2.col(0).asDiagonal();
            break;
        }
        break;
    case SVL_PSS_DIAG:
        switch (stats._pairStats) {
        case SVL_PSS_FULL:
            _sum2 += w * stats._sum2.diagonal();
            break;
        case SVL_PSS_DIAG:
            _sum2 += w * stats._sum2;
            break;
        }
        break;
    default:
        // do nothing
        break;
    }
}

void svlSufficientStats::subtract(const vector<double>& x)
{
    SVL_ASSERT_MSG((int)x.size() == _n, x.size() << " != " << _n);
    SVL_ASSERT(_count >= 1.0);

    _sum -= Eigen::Map<VectorXd>(&x[0], x.size());
    switch (_pairStats) {
    case SVL_PSS_FULL:
        _sum2 -= Eigen::Map<VectorXd>(&x[0], x.size()) *
            Eigen::Map<VectorXd>(&x[0], x.size()).transpose();
        break;
    case SVL_PSS_DIAG:
        _sum2 -= Eigen::Map<VectorXd>(&x[0], x.size()).cwise().square();
        break;
    default:
        // do nothing
        break;
    }
    _count -= 1.0;
}

void svlSufficientStats::subtract(const vector<double>& x, double w)
{
    SVL_ASSERT((int)x.size() == _n);
    SVL_ASSERT(_count >= w);

    VectorXd t(w * Eigen::Map<VectorXd>(&x[0], x.size()));
    _sum -= t;
    switch (_pairStats) {
    case SVL_PSS_FULL:
        _sum2 -= Eigen::Map<VectorXd>(&x[0], x.size()) * t.transpose();
        break;
    case SVL_PSS_DIAG:
        _sum2 -= w * Eigen::Map<VectorXd>(&x[0], x.size()).cwise().square();
        break;
    default:
        // do nothing
        break;
    }
    _count -= w;
}

void svlSufficientStats::subtract(const vector<vector<double> >& x, double w)
{
    SVL_ASSERT(w * x.size() <= _count);

    svlSufficientStats stats(_n, _pairStats);
    for (vector<vector<double> >::const_iterator it = x.begin(); it != x.end(); ++it) {
        stats.accumulate(*it);
    }
    subtract(stats, w);
}

void svlSufficientStats::subtract(const svlSufficientStats& stats, double w)
{
    SVL_ASSERT(stats._n == _n);
    SVL_ASSERT(w * stats._count <= _count);

    _count -= w * stats._count;
    _sum -= w * stats._sum;
    switch (_pairStats) {
    case SVL_PSS_FULL:
        switch (stats._pairStats) {
        case SVL_PSS_FULL:
            _sum2 -= w * stats._sum2;
            break;
        case SVL_PSS_DIAG:
            _sum2 -= w * stats._sum2.col(0).asDiagonal();
            break;
        }
        break;
    case SVL_PSS_DIAG:
        switch (stats._pairStats) {
        case SVL_PSS_FULL:
            _sum2 -= w * stats._sum2.diagonal();
            break;
        case SVL_PSS_DIAG:
            _sum2 -= w * stats._sum2;
            break;
        }
        break;
    default:
        // do nothing
        break;
    }
}

// standard operators
svlSufficientStats& svlSufficientStats::operator=(const svlSufficientStats& stats)
{
    _n = stats._n;
    _pairStats = stats._pairStats;
    _count = stats._count;
    _sum = stats._sum;
    _sum2 = stats._sum2;

    return *this;
}

// svlCondSufficientStats class ----------------------------------------------

svlCondSufficientStats::svlCondSufficientStats(int n, int k) :
    _n(n), _k(k) {
    if (_k > 0) {
        _stats.resize(_k, svlSufficientStats(_n));
    }
}

svlCondSufficientStats::svlCondSufficientStats(const svlCondSufficientStats& condStats) :
    _n(condStats._n), _k(condStats._k), _stats(condStats._stats)
{
    // do nothing
}

svlCondSufficientStats::~svlCondSufficientStats()
{
    // do nothing
}

void svlCondSufficientStats::clear()
{
    for (int y = 0; y < _k; y++) {
        _stats[y].clear();
    }
}

void svlCondSufficientStats::clear(int n, int k)
{
    SVL_ASSERT((n > 0) && (k > 0));

    _n = n;
    _k = k;
    _stats.resize(_k);
    for (int y = 0; y < _k; y++) {
        _stats[y].clear(_n);
    }
}

void svlCondSufficientStats::save(const char *filename) const
{
    SVL_ASSERT(filename != NULL);

    ofstream ofs(filename);
    SVL_ASSERT(!ofs.fail());

    ofs << _n << " " << _k << "\n\n";
    for (int i = 0; i < _k; i++) {
        _stats[i].save(ofs);
    }
    ofs.close();
}

void svlCondSufficientStats::load(const char *filename)
{
    SVL_ASSERT(filename != NULL);

    ifstream ifs(filename);
    SVL_ASSERT(!ifs.fail());

    int n, k;
    ifs >> n >> k;
    clear(n, k);

    for (int i = 0; i < k; i++) {
        _stats[i].load(ifs);
        SVL_ASSERT(!ifs.fail());
    }
    ifs.close();
}

// TODO: move inline
void svlCondSufficientStats::accumulate(const vector<double>& x, int y)
{
    SVL_ASSERT((y >= 0) && (y < _k));
    _stats[y].accumulate(x);
}

void svlCondSufficientStats::accumulate(const vector<vector<double> >& x, int y)
{
    SVL_ASSERT((y >= 0) && (y < _k));
    _stats[y].accumulate(x);
}

void svlCondSufficientStats::accumulate(const vector<vector<double> >& x, const vector<int>& y)
{
    SVL_ASSERT(x.size() == y.size());
    for (int i = 0; i < (int)x.size(); i++) {
        accumulate(x[i], y[i]);
    }
}

void svlCondSufficientStats::accumulate(const svlSufficientStats& stats, int y)
{
    SVL_ASSERT((y >= 0) && (y < _k));
    _stats[y].accumulate(stats);
}

// TODO: move inline
void svlCondSufficientStats::subtract(const vector<double>& x, int y)
{
    SVL_ASSERT((y >= 0) && (y < _k));
    _stats[y].subtract(x);
}

void svlCondSufficientStats::subtract(const vector<vector<double> >& x, int y)
{
    SVL_ASSERT((y >= 0) && (y < _k));
    _stats[y].subtract(x);
}

void svlCondSufficientStats::subtract(const vector<vector<double> >& x, const vector<int>& y)
{
    SVL_ASSERT(x.size() == y.size());
    for (int i = 0; i < (int)x.size(); i++) {
        subtract(x[i], y[i]);
    }
}

void svlCondSufficientStats::subtract(const svlSufficientStats& stats, int y)
{
    SVL_ASSERT((y >= 0) && (y < _k));
    _stats[y].subtract(stats);
}

void svlCondSufficientStats::redistribute(int y, int k)
{
    SVL_ASSERT((y >= 0) && (y < _k))
    SVL_ASSERT((k >= 0) && (k < _k));

    _stats[k].accumulate(_stats[y]);
    _stats[y].clear();
}
