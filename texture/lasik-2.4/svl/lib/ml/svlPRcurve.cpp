/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlPRCurve.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**              Olga Russakovsky <olga@cs.stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
**
*****************************************************************************/

#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <sys/types.h>
#include <limits>

#include "svlBase.h"
#include "svlPRcurve.h"

using namespace std;

// static data members ------------------------------------------------------

bool svlClassifierSummaryStatistics::INCLUDE_MISSES = false;
string svlClassifierSummaryStatistics::COL_SEP(", ");
string svlClassifierSummaryStatistics::ROW_BEGIN("");
string svlClassifierSummaryStatistics::ROW_END("");

// svlClassifierSummaryStatistics -------------------------------------------

svlClassifierSummaryStatistics::svlClassifierSummaryStatistics() :
    _numPositiveSamples(0), _numNegativeSamples(0), _posWeight(1.0)
{
    // do nothing
}

svlClassifierSummaryStatistics::svlClassifierSummaryStatistics(const svlClassifierSummaryStatistics& c) :
    _scoredResults(c._scoredResults),
    _numPositiveSamples(c._numPositiveSamples),
    _numNegativeSamples(c._numNegativeSamples),
    _posWeight(c._posWeight)
{
    // do nothing
}

svlClassifierSummaryStatistics::~svlClassifierSummaryStatistics()
{
    // do nothing
}

// i/o
void svlClassifierSummaryStatistics::clear()
{
    _scoredResults.clear();
    _numPositiveSamples = 0;
    _numNegativeSamples = 0;
    _posWeight = 1.0;
}

bool svlClassifierSummaryStatistics::write(const char *filename) const
{
    SVL_ASSERT(filename != NULL);

    ofstream ofs(filename);
    SVL_ASSERT(!ofs.fail());

    ofs << _scoredResults.size() << " " 
        << _numPositiveSamples << " " 
        << _numNegativeSamples << " "
        << _posWeight << "\n";
    for (pr_counts_t::const_iterator p = _scoredResults.begin();
         p != _scoredResults.end(); p++) {
        ofs << p->first << " " << p->second.posCount << " " << p->second.negCount << "\n";
    }
    ofs.close();
    return true;
}

bool svlClassifierSummaryStatistics::read(const char *filename)
{
    SVL_ASSERT(filename != NULL);

    clear();
    ifstream ifs(filename);
    SVL_ASSERT(!ifs.fail());

    int numPoints;
    ifs >> numPoints >> _numPositiveSamples >> _numNegativeSamples >> _posWeight;
    while (numPoints-- > 0) {
        double threshold;
        int posCount, negCount;

        ifs >> threshold >> posCount >> negCount;
        SVL_ASSERT(!ifs.fail() && (posCount >= 0) && (negCount >= 0));
        _scoredResults.insert(make_pair(threshold, svlPrCountEntry(posCount, negCount)));
    }

    ifs.close();
    return true;
}

// add points to the curve
void svlClassifierSummaryStatistics::accumulate(const svlClassifierSummaryStatistics& c)
{
    for (pr_counts_t::const_iterator p = c._scoredResults.begin();
         p != c._scoredResults.end(); p++) {
        pr_counts_t::iterator it = _scoredResults.find(p->first);
        if (it == _scoredResults.end()) {
            _scoredResults.insert(*p);
        } else {
            it->second.posCount += p->second.posCount;
            it->second.negCount += p->second.negCount;
        }
    }

    _numPositiveSamples += c._numPositiveSamples;
    _numNegativeSamples += c._numNegativeSamples;
}

void svlClassifierSummaryStatistics::accumulatePositives(double score, int count)
{
    SVL_ASSERT(count >= 0);
    _numPositiveSamples += count;

    pr_counts_t::iterator it = _scoredResults.find(score);
    if (it == _scoredResults.end()) {
        _scoredResults.insert(make_pair(score, svlPrCountEntry(count, 0)));
    } else {
        it->second.posCount += count;
    }
}

void svlClassifierSummaryStatistics::accumulatePositives(const vector<double>& scores)
{
    for (vector<double>::const_iterator it = scores.begin(); it != scores.end(); it++) {
        accumulatePositives(*it, 1);
    }
}

void svlClassifierSummaryStatistics::accumulatePositives(const svlHistogram& counts)
{
    unsigned numBins = counts.numBins();
    SVL_ASSERT(numBins > 0);

    double dthreshold = counts.getBinSize();
    for (unsigned i = 0; i < numBins; i++) {
        accumulatePositives(i * dthreshold, (int)counts[i]);
    }
}

void svlClassifierSummaryStatistics::accumulateNegatives(double score, int count)
{
    SVL_ASSERT(count >= 0);

    _numNegativeSamples += count;

    pr_counts_t::iterator it = _scoredResults.find(score);
    if (it == _scoredResults.end()) {
        _scoredResults.insert(make_pair(score, svlPrCountEntry(0, count)));
    } else {
        it->second.negCount += count;
    }
}

void svlClassifierSummaryStatistics::accumulateNegatives(const vector<double>& scores)
{
    for (vector<double>::const_iterator it = scores.begin(); it != scores.end(); it++) {
        accumulateNegatives(*it, 1);
    }
}

void svlClassifierSummaryStatistics::accumulateNegatives(const svlHistogram& counts)
{
    unsigned numBins = counts.numBins();
    SVL_ASSERT(numBins > 0);

    double dthreshold = counts.getBinSize();
    for (unsigned i = 0; i < numBins; i++) {
        accumulateNegatives(i * dthreshold, (int)counts[i]);
    }
}

void svlClassifierSummaryStatistics::accumulateMisses(int count)
{
    SVL_ASSERT(count >= 0);
    _numPositiveSamples += count;
}

void svlClassifierSummaryStatistics::quantize(double minThreshold, double maxThreshold, int numBins)
{
    SVL_ASSERT((minThreshold < maxThreshold) && (numBins > 1));

    // build histogram of scores
    // note: since the histogram from a to b will only allow values in
    // the [a, b) range, the histogram created here must allow for one
    // extra bin to include the entire [a, b] range
    svlHistogram posScores(numBins, minThreshold, maxThreshold, true);
    svlHistogram negScores(numBins, minThreshold, maxThreshold, true);
    for (pr_counts_t::const_iterator it = _scoredResults.begin();
         it != _scoredResults.end(); it++) {
      posScores.insert(it->first, it->second.posCount);
      negScores.insert(it->first, it->second.negCount);
    }

    // create new summary statistics object
    svlClassifierSummaryStatistics q;
    q.setPosWeight(_posWeight);
    q.accumulatePositives(posScores);
    q.accumulateNegatives(negScores);
    q.accumulateMisses(numMisses());

    *this = q;
}

// svlPRCurve ---------------------------------------------------------------

svlPRCurve::svlPRCurve() : svlClassifierSummaryStatistics()
{
    // do nothing
}

svlPRCurve::svlPRCurve(const svlClassifierSummaryStatistics& c) : 
    svlClassifierSummaryStatistics(c)
{
    // do nothing
}

svlPRCurve::svlPRCurve(const svlPRCurve& c) : 
  svlClassifierSummaryStatistics(c), _storedCurve(c._storedCurve)
{
    // do nothing
}

svlPRCurve::~svlPRCurve()
{
    // do nothing
}


//please do not delete this without first contacting Ian Goodfellow
//Willow Garage is using it; at the very least it should remain alive until ECCV 10
void svlPRCurve::writeAllInfo(const char * filepath)
{
  ofstream ofs(filepath);
  SVL_ASSERT(!ofs.fail());

  ofs << "THRESHOLD, TP, FP, FN" << endl;

  // Visit each point on the curve (every possible threshold)
  for (pr_counts_t::const_iterator pt = _scoredResults.begin();
       pt != _scoredResults.end(); ++pt)
    {
      double thresh = pt->first;

      //Compute TP, FP, FN at this point, and report them all
      int posSum = INCLUDE_MISSES ? numMisses() : 0;
      int negSum = 0;
      pr_counts_t::const_reverse_iterator it = _scoredResults.rbegin();
      while ((it != _scoredResults.rend()) && (it->first >= thresh)) {
	posSum += it->second.posCount;
	negSum += it->second.negCount;
	it++;
      }
     
      int tp = posSum;
      int fp = negSum;
      int fn = _numPositiveSamples - posSum;

      ofs << thresh << ", " << tp << ", " << fp << ", " << fn << endl;
 
      
    }

  ofs.close();
}


void svlPRCurve::writeCurve(const char *filename) const
{
    SVL_ASSERT(filename != NULL);

    ofstream ofs(filename);
    SVL_ASSERT(!ofs.fail());
    ofs << ROW_BEGIN << "THRESHOLD" << COL_SEP << "RECALL" << COL_SEP << "PRECISION" << ROW_END << "\n";

    int posSum = INCLUDE_MISSES ? numMisses() : 0;
    int negSum = 0;
    for (pr_counts_t::const_reverse_iterator it = _scoredResults.rbegin();
         it != _scoredResults.rend(); it++) {
        posSum += it->second.posCount;
        negSum += it->second.negCount;
        double recall = (double)posSum / (double)_numPositiveSamples;
        double precision = _posWeight * posSum / (_posWeight * posSum + negSum);
	if (!isnan(recall) && !isnan(precision))
	  ofs << ROW_BEGIN << it->first << COL_SEP << recall << COL_SEP << precision << ROW_END << "\n";
    }
    SVL_ASSERT((posSum <= _numPositiveSamples) && (negSum == _numNegativeSamples));

    ofs.close();
}

// normalized curve: recall v.s. false positive per window
void svlPRCurve::writeNormalizedCurve(const char *filename) const
{

  SVL_LOG(SVL_LOG_FATAL, "svlPRCUrve::writeNormalizedCurve is broken. It reports false positives per scored window rather than false positives per window.");

    SVL_ASSERT(filename != NULL);

    ofstream ofs(filename);
    SVL_ASSERT(!ofs.fail());
    ofs << ROW_BEGIN << "THRESHOLD" << COL_SEP << "RECALL" << COL_SEP << "PRECISION" << ROW_END << "\n";

    int posSum = INCLUDE_MISSES ? numMisses() : 0;
    int negSum = 0;
    for (pr_counts_t::const_reverse_iterator it = _scoredResults.rbegin();
         it != _scoredResults.rend(); it++) {
        posSum += it->second.posCount;
        negSum += it->second.negCount;
        double recall = (double)posSum / (double)_numPositiveSamples;
        double fppw = (double)negSum / (double)(_numNegativeSamples + _numPositiveSamples);
        ofs << ROW_BEGIN << it->first << COL_SEP << recall << COL_SEP << fppw << ROW_END << "\n";
    }
    SVL_ASSERT((posSum <= _numPositiveSamples) && (negSum == _numNegativeSamples));

    ofs.close();
}

bool svlPRCurve::readCurve(const char *filename) 
{
   SVL_ASSERT(filename != NULL);

   ifstream ifs(filename);
   if(ifs.fail()) return false;
   char buffer[256];
   ifs.getline(buffer, 256);

   while (true) {
     if (ROW_BEGIN.size() > 0)
       ifs.get(buffer, ROW_BEGIN.size());
     double thr;
     ifs >> thr;
     ifs.get(buffer, COL_SEP.size());
     double r;
     ifs >> r;
     ifs.get(buffer, COL_SEP.size());
     double pr;
     ifs >> pr;
     ifs.getline(buffer, 256);
     if (ifs.fail()) break;
     _storedCurve[thr] = svlRecallPrecision(r, pr);
   }
   return true;
}


double svlPRCurve::averagePrecision(int numPoints) const
{
    SVL_ASSERT(numPoints > 2);

    double ap = 0.0;

    if (_storedCurve.empty()) {
      int posSum = INCLUDE_MISSES ? numMisses() : 0;
      int negSum = 0;
      pr_counts_t::const_reverse_iterator it = _scoredResults.rbegin();
      double recall = (double)posSum / (double)_numPositiveSamples;
      double precision = 0.0;
      for (int i = 0; i < numPoints; i++) {
	double t = i / (double)(numPoints - 1);
	while ((it != _scoredResults.rend()) && (recall < t)) {
	  posSum += it->second.posCount;
	  negSum += it->second.negCount;
	  recall = (double)posSum / (double)_numPositiveSamples;
	  precision = std::max(precision, _posWeight * posSum / (_posWeight * posSum + negSum));
	  it++;
	}
	if (recall >= t) ap += precision;
	if (it == _scoredResults.rend()) break;
      }
    } else {
      pr_storage_t::const_reverse_iterator it = _storedCurve.rbegin();
      double recall = it->second.r;
      double precision = it->second.p;

      for (int i = 0; i < numPoints; i++) {
	double t = i / (double)(numPoints - 1);
	while ((it != _storedCurve.rend()) && (recall < t)) {
	  recall = it->second.r;
	  precision = it->second.p;
	  it++;
	}
	if (recall >= t) ap += precision;
	if (it == _storedCurve.rend()) break;
      }
    }
    
    return ap / (double)numPoints;
}

double svlPRCurve::bestF1Score() const
{
    double bestScore = 0.0;

    if (_storedCurve.empty()) {
      int posSum = INCLUDE_MISSES ? numMisses() : 0;
      int negSum = 0;
      for (pr_counts_t::const_reverse_iterator it = _scoredResults.rbegin();
	   it != _scoredResults.rend(); it++) {
        posSum += it->second.posCount;
        negSum += it->second.negCount;
        double recall = (double)posSum / (double)_numPositiveSamples;
        double precision = _posWeight * posSum / (_posWeight * posSum + negSum);
        double score = 2.0 * precision * recall / (precision + recall);
        bestScore = std::max(bestScore, score);
      }
      SVL_ASSERT((posSum <= _numPositiveSamples) && (negSum == _numNegativeSamples));
    } else {
      for (pr_storage_t::const_reverse_iterator it = _storedCurve.rbegin();
	   it != _storedCurve.rend(); it++) {
	double recall = it->second.r;
        double precision = it->second.p;
        double score = 2.0 * precision * recall / (precision + recall);
        bestScore = std::max(bestScore, score);
      }
    }
    return bestScore;
}

double svlPRCurve::thresholdForBestF1Score() const
{
   double bestScore = DBL_MIN;
   double bestThreshold = -1;

   if (_storedCurve.empty()) {
     int posSum = INCLUDE_MISSES ? numMisses() : 0;
     int negSum = 0;
     for (pr_counts_t::const_reverse_iterator it = _scoredResults.rbegin();
	  it != _scoredResults.rend(); it++) {
       posSum += it->second.posCount;
       negSum += it->second.negCount;
       double recall = (double)posSum / (double)_numPositiveSamples;
       double precision = _posWeight * posSum / (_posWeight * posSum + negSum);
       double score = 2.0 * precision * recall / (precision + recall);
       if (score > bestScore) {
	 bestScore = score;
	 bestThreshold = it->first;
       }
     }
     SVL_ASSERT((posSum <= _numPositiveSamples) && (negSum == _numNegativeSamples));
   } else {
     for (pr_storage_t::const_reverse_iterator it = _storedCurve.rbegin();
	  it != _storedCurve.rend(); it++) {
       double recall = it->second.r;
       double precision = it->second.p;
       double score = 2.0 * precision * recall / (precision + recall);
       if (score > bestScore) {
	 bestScore = score;
	 bestThreshold = it->first;
       }
     }
   }
   return bestThreshold;
}

double svlPRCurve::F1ScoreAt(double threshold) const
{
  double recall = 0.0;
  double precision = 0.0;

  if (_storedCurve.empty()) {
    int posSum = INCLUDE_MISSES ? numMisses() : 0;
    int negSum = 0;
    pr_counts_t::const_reverse_iterator it = _scoredResults.rbegin();
    while ((it != _scoredResults.rend()) && (it->first >= threshold)) {
      posSum += it->second.posCount;
      negSum += it->second.negCount;
      it++;
    }
    
    recall = (double)posSum / (double)_numPositiveSamples;
    precision = _posWeight * posSum / (_posWeight * posSum + negSum);
  } else {
    pr_storage_t::const_reverse_iterator it = _storedCurve.rbegin();
    while ((it != _storedCurve.rend()) && (it->first >= threshold)) {
      it++;
    }
    recall = it->second.r;
    precision = it->second.p;
  }
  return 2.0 * precision * recall / (precision + recall);
}

double svlPRCurve::precisionAt(double threshold) const
{
  double precision = 0.0;
  if (_storedCurve.empty()) {
    int posSum = INCLUDE_MISSES ? numMisses() : 0;
    int negSum = 0;
    pr_counts_t::const_reverse_iterator it = _scoredResults.rbegin();
    while ((it != _scoredResults.rend()) && (it->first >= threshold)) {
        posSum += it->second.posCount;
        negSum += it->second.negCount;
        it++;
    }
    precision = _posWeight * posSum / (_posWeight * posSum + negSum);
  } else {
    pr_storage_t::const_reverse_iterator it = _storedCurve.rbegin();
    while ((it != _storedCurve.rend()) && (it->first >= threshold)) {
      it++;
    }
    precision = it->second.p;
  }

  return precision;
}

double svlPRCurve::recallAt(double threshold) const
{
  double recall = 0.0;
  if (_storedCurve.empty()) {
    int posSum = INCLUDE_MISSES ? numMisses() : 0;
    int negSum = 0;
    pr_counts_t::const_reverse_iterator it = _scoredResults.rbegin();
    while ((it != _scoredResults.rend()) && (it->first >= threshold)) {
        posSum += it->second.posCount;
        negSum += it->second.negCount;
        it++;
    }
    recall =  (double)posSum / (double)_numPositiveSamples;
  } else {
    pr_storage_t::const_reverse_iterator it = _storedCurve.rbegin();
    while ((it != _storedCurve.rend()) && (it->first >= threshold)) {
      it++;
    }
    recall =  it->second.r;
  }
  return recall;
}

double svlPRCurve::thresholdForRecall(double recall) const
{
  double threshold = 0.0;
  if (_storedCurve.empty()) {
    int posSum = INCLUDE_MISSES ? numMisses() : 0;
    int negSum = 0;
    pr_counts_t::const_reverse_iterator it = _scoredResults.rbegin();
    while (it != _scoredResults.rend()) {
        posSum += it->second.posCount;
        negSum += it->second.negCount;
	double r = (double)posSum / (double)_numPositiveSamples;
	if (r >= recall) break;
        it++;
    }
    threshold = it->first;
  } else {
    pr_storage_t::const_reverse_iterator it = _storedCurve.rbegin();
    while (it != _storedCurve.rend()) {
      double r = it->second.r;
      if (r >= recall) break;
      it++;
    }
    threshold = it->first;
  }
  return threshold;
}

double svlPRCurve::thresholdForPrecision(double pr) const
{
  double threshold = 0.0;
  if (_storedCurve.empty()) {
    int posSum = INCLUDE_MISSES ? numMisses() : 0;
    int negSum = 0;
    pr_counts_t::const_iterator it = _scoredResults.begin();
    while ((it != _scoredResults.end())) {
        posSum += it->second.posCount;
        negSum += it->second.negCount;
	double p = _posWeight * posSum / (_posWeight * posSum + negSum);
	if (p >= pr) break;
        it++;
    }
  } else {
    pr_storage_t::const_iterator it = _storedCurve.begin();
    while (it != _storedCurve.end()) {
      double p = it->second.p;
      if (p >= pr) break;
      it++;
    }
    threshold = it->first;
  }
  return threshold;
}


string svlPRCurve::summaryAt(double threshold) const
{
  stringstream result;
  if (_storedCurve.empty()) {
    int posSum = INCLUDE_MISSES ? numMisses() : 0;
    int negSum = 0;
    pr_counts_t::const_reverse_iterator it = _scoredResults.rbegin();
    while ((it != _scoredResults.rend()) && (it->first >= threshold)) {
      posSum += it->second.posCount;
      negSum += it->second.negCount;
      it++;
    }
    
    double recall = (double)posSum / (double)_numPositiveSamples;
    double precision = _posWeight * posSum / (_posWeight * posSum + negSum);
    double F1 = 2.0 * precision * recall / (precision + recall);
    
    result << _numPositiveSamples << "\t" << posSum << "\t" << negSum << "\t" 
	   << recall << "\t" << precision << "\t" << F1;
  } else {
    pr_storage_t::const_reverse_iterator it = _storedCurve.rbegin();
    while ((it != _storedCurve.rend()) && (it->first >= threshold)) {
      it++;
    }
    
    double recall = it->second.r;
    double precision = it->second.p;
    double F1 = 2.0 * precision * recall / (precision + recall);
    
    result << "?" << "\t" << "?" << "\t" << "?" << "\t" 
	   << recall << "\t" << precision << "\t" << F1;
  }

  return result.str();
}




// configuration --------------------------------------------------------

class svlClassifierSummaryStatisticsConfig : public svlConfigurableModule {
public:
    svlClassifierSummaryStatisticsConfig() : svlConfigurableModule("svlML.svlClassifierSummaryStatistics") { }

    void usage(ostream &os) const {
        os << "      includeMisses:: include missed positives (default: " 
           << (svlClassifierSummaryStatistics::INCLUDE_MISSES ? "true" : "false") << ")\n"
           << "      colSep       :: column separator (default: \"" << svlClassifierSummaryStatistics::COL_SEP << "\")\n"
           << "      rowBegin     :: start of row token (default: \"" << svlClassifierSummaryStatistics::ROW_BEGIN << "\")\n"
           << "      rowEnd       :: end of row token (default: \"" << svlClassifierSummaryStatistics::ROW_END << "\")\n";
    }

    void setConfiguration(const char *name, const char *value) {
        if (!strcmp(name, "includeMisses")) {
            svlClassifierSummaryStatistics::INCLUDE_MISSES =
                (!strcasecmp(value, "TRUE") || !strcmp(value, "1"));
        } else if (!strcmp(name, "colSep")) {
            svlClassifierSummaryStatistics::COL_SEP = string(value);
        } else if (!strcmp(name, "rowBegin")) {
            svlClassifierSummaryStatistics::ROW_BEGIN = string(value);
        } else if (!strcmp(name, "rowEnd")) {
            svlClassifierSummaryStatistics::ROW_END = string(value);
        } else {
            SVL_LOG(SVL_LOG_FATAL, "unrecognized configuration option for " << this->name());
        }
    }
};

static svlClassifierSummaryStatisticsConfig gClassifierSummaryStatisticsConfig;
