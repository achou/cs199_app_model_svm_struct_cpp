/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlBoostedClassifierSet.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**              Olga Russakovsky <olga@cs.stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
**
*****************************************************************************/

#include <cassert>
#include <iostream>
#include <fstream>
#include <limits>
#include <vector>

#include "cv.h"
#include "cxcore.h"
#include "ml.h"

#include "svlBase.h"
#include "svlBoostedClassifierSet.h"

using namespace std;

#define OPENCV_WORKAROUND_RETRIES 10

#define MULTITHREAD_EVAL
#ifdef MULTITHREAD_EVAL
struct svlBoostedClassifierEvalArgs {
    svlBoostedClassifierEvalArgs(const vector<vector<double> > *x,
        const vector<svlBoostedClassifier> *m, vector<vector<double> > *y, int i) :
        _x(x), _models(m), _y(y), _indx(i) {
        // do nothing
    }

    const vector<vector<double> > * const _x;           // feature vectors
    const vector<svlBoostedClassifier> * const _models; // boosted classifiers
    vector<vector<double> > * const _y;                 // return values
    const int _indx;                                    // model index
};

void *fcnEvalBoostedClassifier(void *a, unsigned tid)
{
    svlBoostedClassifierEvalArgs *args = (svlBoostedClassifierEvalArgs *)a;
    int modelNum = args->_indx;

    for (int j = 0; j < (int)args->_x->size(); j++) {
      args->_y->at(j)[modelNum] = args->_models->at(modelNum).getScore(args->_x->at(j));
    }
    
    delete args;
    return NULL;
}
#endif

// svlBoostedClassifierSet class ---------------------------------------------

string svlBoostedClassifierSet::DEFAULT_MODEL_EXT = string(".model");

svlBoostedClassifierSet::svlBoostedClassifierSet(unsigned n) : svlClassifier()
{
    _nClasses = n;
    _models.resize(n, svlBoostedClassifier());

    // default training options (will be the same as in each of the models)
    declareOption("boostMethod", svlBoostedClassifier::defaultBoostMethod);
    declareOption("boostingRounds", svlBoostedClassifier::defaultBoostingRounds);
    declareOption("trimRate", svlBoostedClassifier::defaultTrimRate);
    declareOption("numSplits", svlBoostedClassifier::defaultNumSplits);
    declareOption("pseudoCounts", svlBoostedClassifier::defaultPseudoCounts);
    declareOption("quietTraining", svlBoostedClassifier::defaultQuietTraining);
}

svlBoostedClassifierSet::svlBoostedClassifierSet(const svlBoostedClassifierSet& bcs) :
    _models(bcs._models)
{
    // do nothing
}

svlBoostedClassifierSet::~svlBoostedClassifierSet()
{
    // do nothing
}

bool svlBoostedClassifierSet::trained() const
{
    if (_models.empty()) return false;

    for (unsigned i = 0; i < _models.size(); i++) {
        if (!_models[i].trained()) return false;
    }
    return true;
}

bool svlBoostedClassifierSet::trained(int modelIndx) const 
{
    if (modelIndx >= (int)_models.size()) return false;
    return _models[modelIndx].trained();
}

// i/o
void svlBoostedClassifierSet::initialize(unsigned n)
{
    _models.clear();
    _models.resize(n, svlBoostedClassifier());
    _nClasses = n;
}

bool svlBoostedClassifierSet::save(const char *filename, const char *ext, int modelIndx) const
{
    string filestem = strReplaceExt(string(filename), string(""));

    // save xml wrapper
    ofstream ofs(filename);
    if (ofs.fail()) {
        SVL_LOG(SVL_LOG_ERROR, "could not write svlBoostedClassifierSet to " << filename);
        return false;
    }

    ofs << "<svlClassifier id=\"BOOSTEDCLASSIFIERSET\"\n"
        << "               version=\"1\"\n"
        << "               numFeatures=\"" << _nFeatures << "\"\n"
        << "               numClasses=\"" << _nClasses << "\" >\n";

    ofs << "  <filestem>" << filestem << "</filestem>\n";
    ofs << "  <extension>" << ext << "</extension>\n";
    ofs << "</svlClassifier>\n";
    ofs.close();

    // save individual models
    for (int i = 0; i < (int)_models.size(); i++) {
        string modelFilename;
        if (_models.size() > 1) {
            modelFilename = filestem + string(".") + toString(i) + string(ext);
        } else {
            modelFilename = filestem + string(ext);
        }
        if ((modelIndx < 0) || (i == modelIndx)) {
            if (!_models[i].trained()) {
                SVL_LOG(SVL_LOG_VERBOSE, "...svlBoostedClassifierSet skipping untrained model " << modelFilename);
                continue;
            }
            SVL_LOG(SVL_LOG_VERBOSE, "...svlBoostedClassifierSet saving to " << modelFilename);
            _models[i].save(modelFilename.c_str());
        } else {
            SVL_LOG(SVL_LOG_VERBOSE, "...svlBoostedClassifierSet skipping " << modelFilename);
        }
    }
  
    return true;
}

bool svlBoostedClassifierSet::save(const char *filename) const
{
    return save(filename, DEFAULT_MODEL_EXT.c_str());
}

bool svlBoostedClassifierSet::save(XMLNode &node) const
{
    SVL_LOG(SVL_LOG_FATAL, "save with XMLNode not supported for svlBoostedClassifierSet");
	return false;
}

bool svlBoostedClassifierSet::load(XMLNode& node) 
{
    if (!this->svlClassifier::load(node)) {
        return false;
    }

    initialize(_nClasses);
    string filestem = string(node.getChildNode("filestem").getText());
    string ext = string(node.getChildNode("extension").getText());

    for (int i = 0; i < (int)_models.size(); i++) {
        string modelFilename;
        if (_models.size() > 1) {
            modelFilename = filestem + string(".") + toString(i) + ext;
        } else {
            modelFilename = filestem + ext;
        }
        SVL_LOG(SVL_LOG_VERBOSE, "...svlBoostedClassifierSet loading from " << modelFilename);
        dynamic_cast<svlClassifier *>(& (_models[i]))->load(modelFilename.c_str()); //IG-- Steve checked this in as just models[i].load but that wouldn't compile on my machine... seems like it should though...
    }

    return true;       
}

// set/append pre-trained model
void svlBoostedClassifierSet::addModel(svlBoostedClassifier& model)
{
    _models.push_back(model);
    _nClasses += 1;
}

void svlBoostedClassifierSet::setModel(svlBoostedClassifier& model, int modelIndx)
{
    SVL_ASSERT((modelIndx >= 0) && (modelIndx <= _nClasses));
    _models[modelIndx] = model;
}

// learning
double svlBoostedClassifierSet::train(const MatrixXd &x, const VectorXi &y)
{
    return train(x, y, -1);
}

double svlBoostedClassifierSet::train(const vector<vector<double> >& x, const vector<int>& y)
{
    return train(x, y, -1);
}

double svlBoostedClassifierSet::train(const MatrixXd& x, const VectorXi& y,
		 int modelIndx) // modelIndx = -1 will train all
{
  //TODO-- convert this to eigen native
  vector<vector<double> > xSTL(x.rows(), vector<double> (x.cols()));
  for (int i = 0; i < x.rows(); i++)
    {
      for (int j =0; j < x.cols(); j++)
	{
	  xSTL[i][j] = x(i,j);
	}
    }

  vector<int> ySTL(y.size());
  for (int i = 0; i < y.size(); i++)
    ySTL[i] = y(i);

  return train(xSTL, ySTL, modelIndx);
}

double svlBoostedClassifierSet::train(const vector<vector<double> >& x, const vector<int>& y,
				      int modelIndx)
{
    SVL_ASSERT((x.size() == y.size()) && (!x.empty()));

    bool quiet = getOptionAsBool("quietTraining");

    if (x.size() == 0) {
        SVL_LOG(SVL_LOG_WARNING, "No examples to train on");
        return -1.0;
    }
    _nFeatures = x[0].size();

    CvMat *data, *labels, *varType;
    allocateOpenCVTrainingData(x.size(), x[0].size(), &data, labels, varType);
    fillInOpenCVTrainingData(x, data, quiet);
 
    // train models
    double avgRecall = 0.0;
    int numTrained = 0;
    for (int i = 0; i < (int)_models.size(); i++) {
        if (!quiet) SVL_LOG(SVL_LOG_MESSAGE, "Training boosted classifier for label " << i << "...");
        if ((modelIndx >= 0) && (modelIndx != i)) {
            if (!quiet) SVL_LOG(SVL_LOG_MESSAGE, "...skipping");
            continue;
        }

        // set up data vectors
	int posCount = fillInOpenCVTrainingLabels(y, labels, i);
        if (posCount == 0) {
            SVL_LOG(SVL_LOG_WARNING, "no training samples for label " << i);
            continue;
        }

        int negCount = y.size() - posCount;
	if (!quiet) SVL_LOG(SVL_LOG_MESSAGE, "..." << posCount << " positive samples, " 
            << negCount << " negative samples");

	_models[i].trainUsingOpenCVData(data, labels, varType);
     
        // evaluate classifier (on training data)
	svlBinaryClassifierStats stats = _models[i].getStats(x, y, i);

        // workaround for OpenCV boosting bug
        int trainingAttempts = OPENCV_WORKAROUND_RETRIES;
        while ((trainingAttempts-- > 0) && stats.isBlackStick()) {
            SVL_LOG(SVL_LOG_WARNING, "OpenCV bug detected: re-training boosted classifier " << i);
            _models[i].trainUsingOpenCVData(data, labels, varType);
            stats = _models[i].getStats(x, y, i);
        }

	if (!quiet) SVL_LOG(SVL_LOG_MESSAGE, "...done (TP, FN, TN, FP): " << stats.tp << ", " << stats.fn 
            << ", " << stats.tn << ", " << stats.fp);

        avgRecall += (double)stats.tp / (double)(stats.tp + stats.fn);
        numTrained += 1;
    }

    // free memory
    releaseOpenCVTrainingData(data, labels, varType);

    return (avgRecall / (double)numTrained);
}

// evaluation
void svlBoostedClassifierSet::getClassScores(const VectorXd &sample,
    VectorXd &outputScores) const
{
    outputScores = VectorXd::Zero(_models.size());
  
    // single-threaded evaluation (only)
    for (int j = 0; j < (int)_models.size(); j++) {
        outputScores(j) = _models[j].getScore(sample);
    }
}

void svlBoostedClassifierSet::getClassScores(const vector<double>& x, vector<double>& y) const
{
    y.resize(_models.size());
  
    // single-threaded evaluation (only)
    for (int j = 0; j < (int)_models.size(); j++) {
        y[j] = _models[j].getScore(x);
    }
}

void svlBoostedClassifierSet::getClassScores(const vector<vector<double> >& x,
					     vector<vector<double> >& y) const
{
    if (y.empty()) y.resize(x.size());
    SVL_ASSERT((x.size() == y.size()) && (!x.empty()));
   
#ifdef MULTITHREAD_EVAL
    // multi-threaded version
    for (int j = 0; j < (int)x.size(); j++) {
      y[j].resize(_models.size());
    }
 
    svlThreadPool threadPool(_models.size());
    threadPool.start();
    for (int i = 0; i < (int)_models.size(); i++) {
        svlBoostedClassifierEvalArgs *args = 
	  new svlBoostedClassifierEvalArgs(&x, &_models, &y, i);
        threadPool.addJob(fcnEvalBoostedClassifier, args);
    }
    threadPool.finish();
#else
    // iterate over training examples
    for (int i = 0; i < x.size(); i++) {
        getClassScores(x[i], y[i]);
    }
#endif
}

double svlBoostedClassifierSet::getSingleClassScore(const vector<double>& x, int modelIndx) const
{
    return _models[modelIndx].getScore(x);
}

double svlBoostedClassifierSet::validate(const vector<vector<double> >& x, 
    const vector<int>& y, int modelIndx) const
{
    SVL_ASSERT((x.size() == y.size()) && (!x.empty()));

    double maxValue = -numeric_limits<double>::max();
    double minValue = numeric_limits<double>::max();
    for (int j = 0; j < (int)x.size(); j++) {
        SVL_ASSERT(x[j].size() == x[0].size());
        for (int k = 0; k < (int)x[j].size(); k++) {
            SVL_ASSERT(!isnan(x[j][k]));
            if (x[j][k] > maxValue) maxValue = x[j][k];
            if (x[j][k] < minValue) minValue = x[j][k];
        }
    }
    SVL_LOG(SVL_LOG_MESSAGE, "Validating: " << x[0].size() << " features range from " << minValue << " to " << maxValue);
    
    // validate models
    double avgRecall = 0.0;
    int numValidated = 0;
    for (int i = 0; i < (int)_models.size(); i++) {
        SVL_LOG(SVL_LOG_MESSAGE, "Validating boosted classifier for label " << i << "...");
        if ((modelIndx >= 0) && (modelIndx != i)) {
            SVL_LOG(SVL_LOG_MESSAGE, "...skipping");
            continue;
        }

        svlBinaryClassifierStats stats = _models[i].getStats(x, y, i);

        SVL_LOG(SVL_LOG_MESSAGE, "...done (TP, FN, TN, FP): " << stats.tp << ", " << stats.fn 
            << ", " << stats.tn << ", " << stats.fp);
        avgRecall += (double)stats.tp / (double)(stats.tp + stats.fn);
        numValidated += 1;
    }

    return (avgRecall / (double)numValidated);
}

void svlBoostedClassifierSet::optionChanged(const string& name, const string& value)
{
    // propagate to child classifiers
    for(int i = 0; i < (int)_models.size(); i++) {
        _models[i].setOption(name, value);
    }
}



