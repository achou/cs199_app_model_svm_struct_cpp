/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlHistogram.h
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
** DESCRIPTION:
**  Defines one- and two-dimensional histograms
**
*****************************************************************************/

#pragma once

#include <string>
#include <vector>
#include <fstream>
#include <cv.h>
#include "svlBase.h"
#include "svlHistogramOperators.h"

using namespace std;

// svlHistogram ---------------------------------------------------------
class svlHistogram 
{
 protected:
  svlHistogramOperators _ops;

  unsigned _numValues; // used to resize vectors supplied later
  bool _ownsValues;  // if true, will deallocate _values at the end
  vector<double> *_values;
  // will only concern itself with values between _offset
  // and _offset + _numValues
  unsigned _offset;  


 public:
  // Creating a histogram
  // ===================
  // Range:
  //
  // By default it is assumed that bins have a width of 1, so minValue
  // will be set to 0 and maxValue will be set to numBins. Thus
  //         svlHistogram hist(n);
  // is the simplest constructor call and will allocate enough memory
  // to support a histogram with n bins with values in [0, n).
  //   If 'inclusive' is set to 'true' then the histogram will support
  // values from [0, n] inclusive (by creating numBins+1 bins).
  // 
  // Memory: 
  // 
  // The first two constructors allocate memory internally, while the
  // last two don't and instead takes in the vector of values to
  // populate, taking care of resizing it appropriately (and setting
  // all entries to 0 initially). The passed in vector may be NULL --
  // that means it should later be set via the method
  // set(vector<double> *v). The set method can always be used to
  // reassign which memory the histogram is working with (if memory
  // was allocated in the constructor, it will be deallocated then).
  // It resizes and clears the given vector.
  //
  // Offset:
  //
  // The user can request that the values between offset and
  // offset+numBins are populated in the provided vector instead of
  // the first numBins values. The same is true when calling the set
  // method: the second argument is the integer offset to be used
  // with this new vector.
  //
  //=================== 
  // fyi: the reason why there are no default arguments is because
  // minValue and maxValue really can be arbitrary (as long as min <
  // max)
  svlHistogram(unsigned numBins, bool inclusive = false);
  svlHistogram(unsigned numBins, double minValue, double maxValue, bool inclusive = false);
  svlHistogram(unsigned numBins, vector<double> *v, unsigned offset = 0);
  svlHistogram(unsigned numBins, double minValue, double maxValue,
	       vector<double> *v, unsigned offset = 0);
  svlHistogram(const svlHistogram &h);

  ~svlHistogram();

  // Will resize and clear the feature vector
  void set(vector<double> *v, unsigned offset  = 0);

  // the histogram now supports wrap-around of entries
  void makeCircular() { _ops.makeCircular(); }

  // will insert weight into the closest bin to binVal;
  // returns true on success
  bool insert(double binVal, double weight = 1.0);

  // for each element of binValues will add 1 to the count of the closest bin;
  bool insert(const vector<double> &binValues);

  // will do linear smoothing for insertion, i.e.  the two closest
  // bins will receive a contribution of weight * (1-d) where d is the
  // distance from the bin center measured in _binSize units. In a
  // non-circular histogram, if binValue is < (center of first bin) or
  // > (center of last bin), but still within _binSize of that center,
  // will be added with full weight to the first or last bucket
  // respectively.
  // 
  // returns true on success
  bool insertLinearSmooth(double binVal, double weight = 1.0);

  inline double operator[](unsigned i) const {
    SVL_ASSERT_MSG(_values, "storage for the histogram must be provided");
    SVL_ASSERT(i >= 0 && i < (unsigned)_numValues); // todo: more user friendly
  
    return _values->at(i+_offset);
  }

  float getRange() const { return _ops.getRange(); }
  float getBinSize() const { return _ops.getBinSize(); }
  unsigned numBins() const { return _numValues;}

  void clear();
  void normalize(); // normalizes histogram to unit length
  void truncate(double t); // will truncate all entries > t to t
};

// svl2dHistogram --------------------------------------------------------
class svl2dHistogram : public svlHistogram {
 protected:
  svlHistogramOperators _ops1;
  svlHistogramOperators _ops2;
  unsigned _numBins2;

 public:
  // See svlHistogram for details of the constructors;
  // note that the histogram is still represented
  // as a one-dimensional vector
  svl2dHistogram(unsigned numBins1, unsigned numBins2);
  svl2dHistogram(unsigned numBins1, double minValue1, double maxValue1,
	       unsigned numBins2, double minValue2, double maxValue2);
  svl2dHistogram(unsigned numBins1, unsigned numBins2, 
	       vector<double> *v, unsigned offset = 0);
  svl2dHistogram(unsigned numBins1, double minValue1, double maxValue1,
	       unsigned numBins2, double minValue2, double maxValue2,
	       vector<double> *v, unsigned offset = 0);

  void makeFirstCircular() { _ops.makeCircular(); }
  void makeSecondCircular() { _ops2.makeCircular(); }

  bool insert(double binVal1, double binVal2, double weight = 1.0);
  bool insertLinearSmooth(double binVal1, double binVal2,
			  double weight = 1.0);

  // optimized specifically for the svlRIFTextractor
  bool insertLinearSmooth(svlSoftBinAssignment assn1, double binVal2,
			  double weight = 1.0);

  inline double bin(unsigned i, unsigned j) const { return (*this)[i*_numBins2+j]; }
};

