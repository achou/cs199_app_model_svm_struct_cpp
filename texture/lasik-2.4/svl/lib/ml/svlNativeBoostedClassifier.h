/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlnativeBoostedClassifier.h
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@cs.stanford.edu>
**
** DESCRIPTION:
**  Implements a generic boosted classifier.
**
*****************************************************************************/

#pragma once

#include <vector>

#include "cv.h"
#include "cxcore.h"
#include "ml.h"

#include "xmlParser/xmlParser.h"

#include "svlBase.h"
#include "svlClassifier.h"
#include "svlMLUtils.h"

USING_PART_OF_NAMESPACE_EIGEN;
using namespace std;

#define SVL_ADA_BOOST 0
#define SVL_GENTLE_BOOST 1

// svlClassifier class ------------------------------------------------------------------

class svlNativeBoostedClassifier : public svlClassifier {
protected:
	vector<svlClassifier*> _models;
	vector<double> _coeffs;
	svlLoopTimer *_timer;
	int _method;

public:
	svlNativeBoostedClassifier(unsigned n = 1, unsigned k = 2);
    svlNativeBoostedClassifier(const svlNativeBoostedClassifier &model);
    virtual ~svlNativeBoostedClassifier();
    
    // I/O
	bool save(const char *fname) const;
	bool save(XMLNode& node) const;

	// Accessors
	int getMethod() const { return _method; }
	void setMethod(int method) { _method = method; }
    
	// Training.
	double train(const MatrixXd &samples, const VectorXi &labels) {
		SVL_LOG(SVL_LOG_FATAL, "Must supply a classifier ID to the train function");
		return -1.0;
	}
    double train(const MatrixXd &X, const VectorXi &y, const string &subClassID, int nRounds, const VectorXd *w = NULL);
    
    void getClassScores(const VectorXd &sample, VectorXd &outputScores) const;
	void getMarginals(const VectorXd &sample, VectorXd &outputMarginals) const;
};

SVL_AUTOREGISTER_H( NativeBoosted, svlClassifier );


