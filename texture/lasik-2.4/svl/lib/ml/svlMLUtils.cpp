/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlMLUtils.cpp
** AUTHOR(S):   Ian Goodfellow <ia3n@stanford.edu>
**              Olga Russakovsky <olga@cs.stanford.edu>
** DESCRIPTION:
**
*****************************************************************************/

#include <iostream>
#include <fstream>

#include "../base/svlCompatibility.h"
#include "svlMLUtils.h"

using namespace std;

//TODO-- I think this stl to eigen stuff can be redone with mappings
//that don't require actually duplicating the data. -IG
Eigen::VectorXd stlToEigenV(const vector<double> & v)
{
  Eigen::VectorXd rval(v.size());

  for (unsigned i = 0; i < v.size(); i++)
    rval(i) = v[i];

  return rval;
}

VectorXi stlToEigenV(const vector<int> & v)
{
  VectorXi rval(v.size());

  for (unsigned i = 0; i < v.size(); i++)
    rval(i) = v[i];

  return rval;
}

MatrixXd stlToEigen(const vector<vector<double> > & m)
{
  if (!m.size())
    return MatrixXd::Zero(0,0);
  MatrixXd rval = MatrixXd::Zero(m.size(),m[0].size());

  for (unsigned i = 0; i < m.size(); i++)
    for (unsigned j = 0; j < m[i].size(); j++)
      rval(i,j) = m[i][j];

  return rval;
}

vector<double> eigenToSTL(const VectorXd & v)
{
  vector<double> rval(v.size());
  for (unsigned i = 0; i < rval.size(); i++)
    rval[i] = v(i);
  return rval;
}


bool svlContainsNanOrInf(const VectorXd & v)
{
  unsigned r = v.rows();
  for (unsigned i = 0; i < r; i++)
    {
      double val = v(i);
      if (isinf(val) || isnan(val))
	return true;
    }
  return false;
}


// Eigen I/O

// Templated MatrixX output.
template<typename XT, typename T>
void svlBinaryWriteMatrixX(const char *fname, const XT &X, bool row_major = true)
{
  FILE *fo = fopen(fname, "wb");
	int gi;
	fwrite(&(gi=X.cols()),sizeof(int),1,fo);
	fwrite(&(gi=X.rows()),sizeof(int),1,fo);
	T gd;
	if ( row_major ) {
		for ( int r=0; r<X.rows(); ++r )
			for ( int c=0; c<X.cols(); ++c )
				fwrite(&(gd=X(r,c)),sizeof(T),1,fo);
	} else {	
		for ( int c=0; c<X.cols(); ++c )
			for ( int r=0; r<X.rows(); ++r )
				fwrite(&(gd=X(r,c)),sizeof(T),1,fo);
	}
	fclose(fo);
}
// Specific instantiations.
void svlBinaryWriteMatrixXd(const char *fname, const MatrixXd &X, bool row_major) {
	svlBinaryWriteMatrixX<MatrixXd,double>(fname, X, row_major);
}
void svlBinaryWriteMatrixXf(const char *fname, const MatrixXf &X, bool row_major) {
	svlBinaryWriteMatrixX<MatrixXf,float>(fname, X, row_major);
}
void svlBinaryWriteMatrixXi(const char *fname, const MatrixXi &X, bool row_major) {
	svlBinaryWriteMatrixX<MatrixXi,int>(fname, X, row_major);
}

// Templated VectorX output.
template<typename XT, typename T>
void svlBinaryWriteVectorX(const char *fname, const XT &X)
{
	FILE *fo = fopen(fname,"wb");
	T gd;
	for ( int r=0; r<X.rows(); ++r )
		fwrite(&(gd=X(r)),sizeof(T),1,fo);
	fclose(fo);
}
// Specific instantiations.
void svlBinaryWriteVectorXd(const char *fname, const VectorXd &X) {
	svlBinaryWriteVectorX<VectorXd,double>(fname, X);
}
void svlBinaryWriteVectorXf(const char *fname, const VectorXf &X) {
	svlBinaryWriteVectorX<VectorXf,float>(fname, X);
}
void svlBinaryWriteVectorXi(const char *fname, const VectorXi &X) {
	svlBinaryWriteVectorX<VectorXi,int>(fname, X);
}


