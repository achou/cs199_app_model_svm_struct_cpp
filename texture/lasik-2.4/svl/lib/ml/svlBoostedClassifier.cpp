/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlBoostedClassifier.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**              Olga Russakovsky <olga@cs.stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
**
*****************************************************************************/

#include <cassert>
#include <iostream>
#include <fstream>
#include <limits>
#include <vector>

#include "cv.h"
#include "cxcore.h"
#include "ml.h"

#include "svlBase.h"
#include "svlBoostedClassifier.h"

using namespace std;

#define OPENCV_WORKAROUND_RETRIES 10

// svlBoostedClassifierSet class ---------------------------------------------

string svlBoostedClassifier::defaultBoostMethod = string("GENTLE");
int svlBoostedClassifier::defaultBoostingRounds = 100;
double svlBoostedClassifier::defaultTrimRate = 0.9;
int svlBoostedClassifier::defaultNumSplits = 2;
int svlBoostedClassifier::defaultPseudoCounts = 1;
bool svlBoostedClassifier::defaultQuietTraining = false;
bool svlBoostedClassifier::bDetectOpenCVErrors = false;

svlBoostedClassifier::svlBoostedClassifier() : svlBinaryClassifier()
{
    _numWeakToConsider = INT_MAX;
    _model = NULL;

    // default training options
    declareOption("boostMethod", svlBoostedClassifier::defaultBoostMethod);
    declareOption("boostingRounds", svlBoostedClassifier::defaultBoostingRounds);
    declareOption("trimRate", svlBoostedClassifier::defaultTrimRate);
    declareOption("numSplits", svlBoostedClassifier::defaultNumSplits);
    declareOption("pseudoCounts", svlBoostedClassifier::defaultPseudoCounts);
    declareOption("quietTraining", svlBoostedClassifier::defaultQuietTraining);
}

svlBoostedClassifier::svlBoostedClassifier(const char *filename) : svlBinaryClassifier()
{
    _numWeakToConsider = INT_MAX;

    // default training options
    declareOption("boostMethod", svlBoostedClassifier::defaultBoostMethod);
    declareOption("boostingRounds", svlBoostedClassifier::defaultBoostingRounds);
    declareOption("trimRate", svlBoostedClassifier::defaultTrimRate);
    declareOption("numSplits", svlBoostedClassifier::defaultNumSplits);
    declareOption("pseudoCounts", svlBoostedClassifier::defaultPseudoCounts);
    declareOption("quietTraining", svlBoostedClassifier::defaultQuietTraining);

    _model = NULL;
    load(filename);
}

svlBoostedClassifier::svlBoostedClassifier(const svlBoostedClassifier& bc) :
    svlBinaryClassifier(bc), _numWeakToConsider(bc._numWeakToConsider),
    _model(bc._model)
{
    // do nothing
}

svlBoostedClassifier::~svlBoostedClassifier()
{
    // do nothing
}

// required virtual functions

bool svlBoostedClassifier::save(const char *filename) const
{
  if (_model == NULL || !_trained)
    {
      SVL_LOG(SVL_LOG_WARNING, "svlBoostedClassifier::save: not saving \""<<filename<<"\" because model is NULL or is known not to have been trained.");
      return false;
    }

    // openCV's save function is not const...
    ((svlSmartPointer<CvBoost>)_model)->save(filename);
    return true;
}

// Save to an XMLNode.
bool svlBoostedClassifier::save(XMLNode &node) const
{
    SVL_LOG(SVL_LOG_FATAL, "save with XMLNode not supported for svlBoostedClassifier");
    return false;
}

bool svlBoostedClassifier::load(const char * filename)
{
    _model = svlSmartPointer<CvBoost>(new CvBoost()); //_model is a smart pointer, so that will handle deleting the old one if it exists
    SVL_ASSERT(_model != NULL);
    _model->clear();
    _model->load(filename);
    _trained = true;

    return true;
}

bool svlBoostedClassifier::load(XMLNode& node)
{
    SVL_LOG(SVL_LOG_FATAL, "XML load not implemented for svlBoostedClassifier");
    return false;
}

double svlBoostedClassifier::train(const MatrixXd &X, const VectorXi &Y)
{
    bool quiet = getOptionAsBool("quietTraining");
    SVL_ASSERT(X.rows() == Y.size());

    if (!quiet)
        SVL_LOG(SVL_LOG_MESSAGE, "svlBoostedClassifier::train(const MatrixXd &, constVectorXi &): Training with "
	    << X.rows() << " examples");

    bool containsPos = false;
    bool containsNeg = false;

    for (int i = 0; i < Y.size(); i++) {
        if (Y(i) == 0)
            containsNeg = true;
        else if (Y(i) == 1)
            containsPos = true;
        else {
            SVL_LOG(SVL_LOG_WARNING, "svlBoostedClassifier::train(const MatrixXd &, const VectorXi &): Example "<<i
                << " has label " << Y[i] << ", but only 0/1 labels are valid.");
            return 0;
	}
    }

    _nFeatures = X.cols();

    CvMat *data, *labels, *varType;
    allocateOpenCVTrainingData(X.rows(), X.cols(), NULL, labels, varType);

    data = svlCreateEmptyCvMat(X.cols(),X.rows(),CV_64FC1);
    data->data.db = (double *) X.data();

    fillInOpenCVTrainingLabels(Y, labels);
    trainUsingOpenCVData(data, labels, varType, CV_COL_SAMPLE);

    // workaround for OpenCV boosting bug
    int trainingAttempts = OPENCV_WORKAROUND_RETRIES;
    svlBinaryClassifierStats stats = getStats(X, Y);
    while ((trainingAttempts-- > 0) && stats.isBlackStick()) {
        SVL_LOG(SVL_LOG_WARNING, "OpenCV bug detected: re-training boosted classifier");
        trainUsingOpenCVData(data, labels, varType, CV_COL_SAMPLE);
        stats = getStats(X, Y);
    }

    return validate(stats);
}

double svlBoostedClassifier::train(const MatrixXf &X, const VectorXi &Y)
{
    bool quiet = getOptionAsBool("quietTraining");
    SVL_ASSERT(X.rows() == Y.size());

    if (!quiet)
        SVL_LOG(SVL_LOG_MESSAGE, "svlBoostedClassifier::train(const MatrixXf &, constVectorXi &): Training with "
	    << X.rows() << " examples");

    bool containsPos = false;
    bool containsNeg = false;

    for (int i = 0; i < Y.size(); i++) {
        if (Y(i) == 0)
            containsNeg = true;
        else if (Y(i) == 1)
            containsPos = true;
        else {
            SVL_LOG(SVL_LOG_WARNING, "svlBoostedClassifier::train(const MatrixXf &, const VectorXi &): Example "
                << i << " has label " << Y[i] << ", but only 0/1 labels are valid.");
            return 0;
	}
    }

    _nFeatures = X.cols();

    CvMat *data, *labels, *varType;
    allocateOpenCVTrainingData(X.rows(), X.cols(), NULL, labels, varType);

    data = svlCreateEmptyCvMat(X.cols(), X.rows(), CV_32FC1);
    data->data.db = (double *) X.data();

    fillInOpenCVTrainingLabels(Y, labels);
    trainUsingOpenCVData(data, labels, varType, CV_COL_SAMPLE);

    // workaround for OpenCV boosting bug
    int trainingAttempts = OPENCV_WORKAROUND_RETRIES;
    svlBinaryClassifierStats stats = getStats(X, Y);
    while ((trainingAttempts-- > 0) && stats.isBlackStick()) {
        SVL_LOG(SVL_LOG_WARNING, "OpenCV bug detected: re-training boosted classifier");
        trainUsingOpenCVData(data, labels, varType, CV_COL_SAMPLE);
        stats = getStats(X, Y);
    }

    return validate(stats);
}

double svlBoostedClassifier::train(const vector<vector<double> >& posSamples,
    const vector<vector<double> >& negSamples)
{
    bool quiet = getOptionAsBool("quietTraining");

    if (!quiet)
      SVL_LOG(SVL_LOG_MESSAGE, "..." << posSamples.size() << " positive samples, "
	      << negSamples.size() << " negative samples");

    if (posSamples.size() == 0) {
        SVL_LOG(SVL_LOG_WARNING, "No positive examples to train on");
        return 0.0;
    }
    if (negSamples.size() == 0) {
        SVL_LOG(SVL_LOG_WARNING, "No negative examples to train on");
        return 0.0;
    }

    _nFeatures = posSamples[0].size();

    // workaround for OpenCV boosting bug
    int trainingAttempts = OPENCV_WORKAROUND_RETRIES;
    svlBinaryClassifierStats stats;
    while (trainingAttempts-- > 0) {
        // train classifier
        CvMat *data, *labels, *varType;
        allocateOpenCVTrainingData(posSamples.size() + negSamples.size(),
            posSamples[0].size(), &data, labels, varType);

        fillInOpenCVTrainingData(posSamples, data, quiet);
        fillInOpenCVTrainingData(negSamples, data, quiet, posSamples.size());

        fillInOpenCVTrainingLabels(posSamples.size(), labels);
        trainUsingOpenCVData(data, labels, varType);

        // free memory
        cvReleaseMat(&varType);
        cvReleaseMat(&labels);
        cvReleaseMat(&data);

        // check that training succeeded
        stats = getStats(posSamples, negSamples);
        if (!stats.isBlackStick())
            break;

        SVL_LOG(SVL_LOG_WARNING, "OpenCV bug detected: re-training boosted classifier");
    }

    return validate(stats, "", _useNormedFScore);
}

double svlBoostedClassifier::getScore(const VectorXd& x) const
{
#if 0
    CvMat *fv = createOpenCVFeatureVector(x);
    CvSlice slice = cvSlice(0, _numWeakToConsider);
    double score = _model->predict(fv, NULL, NULL, slice);
    cvReleaseMat(&fv);
    return score;
#else
    // Steve's openCV hack to speed things up
    double result = 0.0;
    // why is get_weak_predictors not const in openCV?
    const CvSeq *weak = ((svlSmartPointer<CvBoost>)_model)->get_weak_predictors();

    CvSeqReader reader;
    cvStartReadSeq(weak, &reader);
    cvSetSeqReaderPos(&reader, 0);

    for (int i = 0; i < min(weak->total, _numWeakToConsider); i++) {
        CvBoostTree* wtree;
        CV_READ_SEQ_ELEM(wtree, reader);

        const CvDTreeNode* node = wtree->get_root();
        while (node->left) {
            CvDTreeSplit* split = node->split;
            int vi = split->var_idx;
            SVL_ASSERT_MSG(vi>=0 && vi < x.size(), "oops " << vi << " size " << x.size());
            int dir = x[vi] <= split->ord.c ? -1 : 1;
            if (split->inversed) {
                node = (dir < 0) ? node->right : node->left;
            } else {
                node = (dir < 0) ? node->left : node->right;
            }
        }

        result += node->value;
    }

    return result;
#endif
}

void svlBoostedClassifier::trainCV(const vector<vector<double> >& posSamples,
				   const vector<vector<double> >& negSamples,
				   const vector<vector<double> >& posCVsamples,
				   const vector<vector<double> >& negCVsamples,
				   bool retrain)
{
  // Step 1: Train the model quietly with the full number of boosting rounds
  bool curQuietTraining = getOptionAsBool("quietTraining");
  int curBoostingRounds = getOptionAsInt("boostingRounds");
  setOption("quietTraining", true);
  train(posSamples, negSamples);

  // Step 2: Determine how many boosting rounds result in optimal cross-validation performance
  SVL_LOG(SVL_LOG_MESSAGE, "Using " << posCVsamples.size() << " positive and "
	  << negCVsamples.size() << " negative cross-validation samples");

  _numWeakToConsider = curBoostingRounds;

  double bestScore = -1;
  int bestNumWeak = -1;

  while (_numWeakToConsider > 0) {
    SVL_LOG(SVL_LOG_MESSAGE, "\tAfter " << _numWeakToConsider << " rounds of training");

    // evaluate model (on training data) and print the results
    double training_performance = validate(posSamples, negSamples, "", _useNormedFScore);
    // evaluate model (on cv data)
    double score = validate(posCVsamples, negCVsamples, "on cv", _useNormedFScore);

    // two things going on here:
    // (1) makes sure that a random spike in cv performance at the beginning of
    //     training doesn't screw it up (has happened before); thus wait either
    //     until 50 rounds have passed, or training performance gets to 1, which
    //     it almost inevitably will (this is hacky, but necessary for now)
    // (2) given the option between two equally-scoring rounds, want to choose
    //     the smaller one, so score >= bestScore, not score > bestScore
    if ((_numWeakToConsider >= 50 || training_performance > 0.99999) &&
	(score >= bestScore)) {
      bestScore = score;
      bestNumWeak = _numWeakToConsider;
    }

    // with fewer weak learners, each one matters much more
    int numReevalRounds = 20;
    if (_numWeakToConsider <= 200)
      numReevalRounds = 10;
    if (_numWeakToConsider <= 100)
      numReevalRounds = 5;

    _numWeakToConsider -= numReevalRounds;
  }

  // Step 3: Re-train the model with either entire train+cv set, or
  // just the cv set. This is important because the number of boosting
  // rounds chosen is different from the last trained model
  setOption("boostingRounds", bestNumWeak);
  setOption("quietTraining", curQuietTraining);
  _numWeakToConsider = INT_MAX;

  if (retrain) {
    SVL_LOG(SVL_LOG_MESSAGE, "\tRetraining");
    unsigned orig_num_pos = posSamples.size();
    unsigned orig_num_neg = negSamples.size();

    vector<vector<double> > fullPosSet(orig_num_pos + posCVsamples.size());
    vector<vector<double> > fullNegSet(orig_num_neg + negCVsamples.size());

    for (unsigned i = 0; i < orig_num_pos; i++)
      fullPosSet[i] = posSamples[i];
    for (unsigned i = 0; i < posCVsamples.size(); i++)
      fullPosSet[orig_num_pos + i] = posCVsamples[i];

    for (unsigned i = 0; i < orig_num_neg; i++)
      fullNegSet[i] = negSamples[i];
    for (unsigned i = 0; i < negCVsamples.size(); i++)
      fullNegSet[orig_num_neg + i] = negCVsamples[i];

    train(fullPosSet, fullNegSet);
  }
  else {
    //train(posSamples, negSamples);
    _model->prune(cvSlice(0, bestNumWeak));
  }

  // restore the standard option
  setOption("boostingRounds", curBoostingRounds);
}


static void createTrainingSamples(const vector<vector<double> > &orig,
				  double num_in_batch, int fold,
				  vector<vector<double> > &newSamples,
				  vector<vector<double> > &newCVsamples) {
  newSamples.clear();
  newCVsamples.clear();

  vector<vector<double> >::const_iterator cutoff1 = orig.begin() + (int)(num_in_batch * fold);
  vector<vector<double> >::const_iterator cutoff2 = orig.begin() + (int)(num_in_batch * (fold+1));

  newSamples.insert(newSamples.end(), orig.begin(), cutoff1);
  newCVsamples.insert(newCVsamples.end(), cutoff1, cutoff2);
  newSamples.insert(newSamples.end(), cutoff2, orig.end());

}

void svlBoostedClassifier::trainFullCV(const vector<vector<double> >& posSamples,
					  const vector<vector<double> >& negSamples,
					  int kfold)
{

  if (kfold <= 1) {
    train(posSamples, negSamples);
    return;
  }

  //bool curQuietTraining = getOptionAsBool("quietTraining");
  //setOption("quietTraining", true);

  int numPos = posSamples.size();
  double numPosInBatch = (double)numPos / kfold;

  int numNeg = negSamples.size();
  double numNegInBatch = (double)numNeg / kfold;

  vector<vector<double> > pos, neg, posCV, negCV;

  int numBoostingRounds = getOptionAsInt("boostingRounds");
  int numReevalRounds = 10;
  int numOptions = (int)ceil((double)numBoostingRounds / (double)numReevalRounds);

  vector<int> tp(numOptions, 0);
  vector<int> fp(numOptions, 0);

  for (int i = 0; i < kfold; i++) {
    createTrainingSamples(posSamples, numPosInBatch, i, pos, posCV);
    createTrainingSamples(negSamples, numNegInBatch, i, neg, negCV);

    train(pos, neg);

    for (int j = 0; j < numOptions; j++) {
      _numWeakToConsider = min((j+1) * numReevalRounds, numBoostingRounds);

      tp[j] += numClassifiedAsPositive(posCV);
      fp[j] += numClassifiedAsPositive(negCV);
    }
  }

  double best_score = -1;
  int best_i = -1;

  string normed = _useNormedFScore ? " (normed)" : "";

  for (int i = 0; i < numOptions; i++) {
    svlBinaryClassifierStats stats;
    stats.tp = tp[i];
    stats.fn = posSamples.size() - tp[i];
    stats.fp = fp[i];
    stats.tn = negSamples.size() - fp[i];

    double score = stats.computeFscore(_useNormedFScore);

    SVL_LOG(SVL_LOG_MESSAGE, "After " << (i+1)*numReevalRounds << " rounds of training, "
	    << "performance" << normed <<" is " << score);

    if (score > best_score) {
      best_score = score;
      best_i = i;
    }
  }

  setOption("boostingRounds", (best_i + 1) * numReevalRounds);
  //setOption("quietTraining", curQuietTraining);
  _numWeakToConsider = INT_MAX;

  SVL_LOG(SVL_LOG_MESSAGE, "Chose " << (best_i + 1) * numReevalRounds << " as the best number of boosting rounds");

  train(posSamples, negSamples);

  // restore original
  setOption("boostingRounds", numBoostingRounds);
}

const CvMat *svlBoostedClassifier::getActiveVars()
{
#if OPENCV_DEVEL
    return _model->get_active_vars();
#else
    SVL_LOG(SVL_LOG_WARNING, "Model trimming is only supported using "
	    << "the SVN copy of OpenCV. Please use the latest copy of "
	    << "OpenCV and define OPENCV_DEVEL to 1 in your copy of svl's "
	    << "make.local file. Otherwise you are limited to using the "
	    << "patchdictionary feature extractor and trimming it with "
	    << "the trimDictionary.pl script");
    return NULL;
#endif
}

// reorder the variables in the model and return the new mapping
vector<int> svlBoostedClassifier::reorderActiveVars()
{
    vector<int> varOrder;
    SVL_ASSERT(_model != NULL);

    // parse tree and re-number variables
    map<int, int> varMapping;
    
    const CvSeq *weak = ((svlSmartPointer<CvBoost>)_model)->get_weak_predictors();
    CvSeqReader reader;
    cvStartReadSeq(weak, &reader);
    cvSetSeqReaderPos(&reader, 0);

    for (int i = 0; i < weak->total; i++) {
        const CvBoostTree* wtree;
        CV_READ_SEQ_ELEM(wtree, reader);

        deque<const CvDTreeNode *> frontier;
        frontier.push_back(wtree->get_root());
        while (!frontier.empty()) {
            const CvDTreeNode *node = frontier.front();
            frontier.pop_front();

            if (node->left) {
                frontier.push_back(node->left);
                frontier.push_back(node->right);
                CvDTreeSplit* split = node->split;
                int vi = split->var_idx;
                map<int, int>::iterator it = varMapping.find(split->var_idx);
                if (it == varMapping.end()) {
                    it = varMapping.insert(it, make_pair(split->var_idx, varMapping.size()));
                    varOrder.push_back(split->var_idx);
                }
                split->var_idx = it->second;
            }
        }
    }

    return varOrder;
}


// protected

void svlBoostedClassifier::trainUsingOpenCVData(CvMat *data, CvMat *labels, CvMat *varType, int format)
{
    int numRounds = getOptionAsInt("boostingRounds");
    double trimRate = getOptionAsDouble("trimRate");
    int numSplits = getOptionAsInt("numSplits");
    int pseudoCounts = getOptionAsInt("pseudoCounts");
    float classPriors[2] = {1.0f, 1.0f};

    int boostMethod = -1;
    if (!strcasecmp(getOption("boostMethod").c_str(), "GENTLE")) {
        boostMethod = CvBoost::GENTLE;
    } else if (!strcasecmp(getOption("boostMethod").c_str(), "DISCRETE")) {
        boostMethod = CvBoost::DISCRETE;
    } else if (!strcasecmp(getOption("boostMethod").c_str(), "LOGIT")) {
        boostMethod = CvBoost::LOGIT;
    } else {
        SVL_LOG(SVL_LOG_FATAL, "unknown boosting method " << getOption("boostMethod")
            << " in " << __PRETTY_FUNCTION__);
    }

    // train the classifier
    if (pseudoCounts > 0) {
        // labels->width = p + n
        // cvSum(labels) = p - n since positive examples contribute 1 and negative -1
        CvScalar sum = cvSum(labels);
        int posCount = (labels->height + (int)sum.val[0]) / 2;
        int negCount = labels->height - posCount;

        classPriors[0] = (float)(posCount + pseudoCounts) /
            (float)(negCount + posCount + 2 * pseudoCounts);
        classPriors[1] = (float)(negCount + pseudoCounts) /
            (float)(negCount + posCount + 2 * pseudoCounts);
        SVL_LOG(SVL_LOG_VERBOSE, "...class priors: " << classPriors[0] << " " << classPriors[1]);
    }

    CvBoostParams parameters(boostMethod, numRounds, trimRate, numSplits,
			     false, &classPriors[0]);
    parameters.split_criteria = CvBoost::DEFAULT;

    SVL_LOG(SVL_LOG_DEBUG, "boosting parameters: " << numRounds << " " << numSplits
        << " " << classPriors[0] << " " << classPriors[1]);

    svlCodeProfiler::tic(svlCodeProfiler::getHandle("svlBoostedClassifier.training"));
     _model = svlSmartPointer<CvBoost>(new CvBoost());
     _model->train(data, format, labels, NULL, NULL, varType, NULL, parameters, false);
    svlCodeProfiler::toc(svlCodeProfiler::getHandle("svlBoostedClassifier.training"));

    _trained = true;
}

// configuration --------------------------------------------------------

class svlBoostedClassifierConfig : public svlConfigurableModule {
public:
    svlBoostedClassifierConfig() : svlConfigurableModule("svlML.svlBoostedClassifier") { }

    void usage(ostream &os) const {
        os << "      boostMethod  :: boosting method: GENTLE (default), DISCRETE, LOGIT\n"
           << "      boostingRounds :: number of boosting rounds (default: "
           << svlBoostedClassifier::defaultBoostingRounds << ")\n"
           << "      trimRate     :: subsampling rate for each round (default: "
           << svlBoostedClassifier::defaultTrimRate << ")\n"
           << "      numSplits    :: number of splits for weak learner (default: "
           << svlBoostedClassifier::defaultNumSplits << ")\n"
           << "      pseudoCounts :: regularization counts (default: "
           << svlBoostedClassifier::defaultPseudoCounts << ")\n"
           << "      quietTraining:: don't print messages during training (default: true)\n";
    }

    void setConfiguration(const char *name, const char *value) {
        // factor operation cache
        if (!strcmp(name, "boostMethod")) {
            svlBoostedClassifier::defaultBoostMethod = string(value);
        } else if (!strcmp(name, "boostingRounds")) {
            svlBoostedClassifier::defaultBoostingRounds = atoi(value);
        } else if (!strcmp(name, "trimRate")) {
            svlBoostedClassifier::defaultTrimRate = atof(value);
        } else if (!strcmp(name, "numSplits")) {
            svlBoostedClassifier::defaultNumSplits = atoi(value);
        } else if (!strcmp(name, "pseudoCounts")) {
            svlBoostedClassifier::defaultPseudoCounts = atoi(value);
        } else if (!strcmp(name, "quietTraining")) {
            svlBoostedClassifier::defaultQuietTraining =
                (!strcasecmp(value, "TRUE") || !strcmp(value, "1"));
        } else {
            SVL_LOG(SVL_LOG_FATAL, "unrecognized configuration option for " << this->name());
        }
    }
};

static svlBoostedClassifierConfig gBoostedClassifierConfig;

string svlCvBoostFileChecker(const char * filepath)
{
    //We don't want to display a lot of error messages here; it could
    //be that a different file checker will be able to parse this file
    //Getting unexpected input just means that this is not a CvBoost,
    //all we have to do is return "", not whine about it.


   if (strExtension(string(filepath)) == "xml")
   {

	XMLNode root = XMLNode::parseFile(filepath);

	if (root.isEmpty())
		return "";


	if (!root.getName())
	{
		//sometiems the root has no name, even though file is valid.
		//in these cases, root has a single child, which is the real root
		if (root.nChildNode() != 1)
			return "";

		root = root.getChildNode(0);
		if (!root.getName())
			return "";
	}


	if (0 != strcmp(root.getName(), "xml"))
	{
		return "";
	}

	root = root.getChildNode(0);

	if (!root.getName())
		return "";


	if (0 != strcmp(root.getName(), "opencv_storage"))
	{
		return "";
	}

	root = root.getChildNode(0);

	if (!root.getName())
		return "";


	if (!strcmp(root.getName(),"my_boost_tree"))
		return "CVBOOST";
   }
   else
   {

    ifstream ifs;
    ifs.open(filepath, ios::binary);

    static const char *unixHeader = "%YAML:1.0\nmy_boost_tree: !!opencv-ml-boost-tree";
    static const char *dosHeader = "%YAML:1.0\r\nmy_boost_tree: !!opencv-ml-boost-tree";

    char buff[256];
    memset((void *)buff, 0, 256 * sizeof(char));
    ifs.read(buff, std::max(strlen(unixHeader), strlen(dosHeader)));
    ifs.close();

    if (!strncmp(unixHeader, buff, strlen(unixHeader)))
        return "CVBOOST";
    if (!strncmp(dosHeader, buff, strlen(dosHeader)))
        return "CVBOOST";
    }
    return "";
}





