/******************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlConfusionMatrix.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
** 
*****************************************************************************/

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>

#include "svlBase.h"
#include "svlConfusionMatrix.h"

using namespace std;

// static data members ------------------------------------------------------

string svlConfusionMatrix::COL_SEP("\t");
string svlConfusionMatrix::ROW_BEGIN("\t");
string svlConfusionMatrix::ROW_END("");

// member functions ---------------------------------------------------------

svlConfusionMatrix::svlConfusionMatrix(int n)
{
    _matrix.resize(n);
    for (int i = 0; i < n; i++) {
	_matrix[i].resize(n, 0);
    }
}
   
svlConfusionMatrix::svlConfusionMatrix(int n, int m)
{
    _matrix.resize(n);
    for (int i = 0; i < n; i++) {
	_matrix[i].resize(m, 0);
    }
}

svlConfusionMatrix::~svlConfusionMatrix()
{
    // do nothing
}

int svlConfusionMatrix::numRows() const
{
    return (int)_matrix.size();
}

int svlConfusionMatrix::numCols() const
{
    if (_matrix.empty())
        return 0;
    return (int)_matrix[0].size();
}

void svlConfusionMatrix::clear()
{
    for (int i = 0; i < (int)_matrix.size(); i++) {
	fill(_matrix[i].begin(), _matrix[i].end(), 0);
    }
}

void svlConfusionMatrix::accumulate(int actual, int predicted)
{
    if ((actual < 0) || (predicted < 0))
        return;
    _matrix[actual][predicted] += 1;
}

void svlConfusionMatrix::accumulate(const vector<int>& actual,
    const vector<int>& predicted)
{
    SVL_ASSERT(actual.size() == predicted.size());
    for (unsigned i = 0; i < actual.size(); i++) {
	// treat < 0 as unknown
	if ((actual[i] < 0) || (predicted[i] < 0))
	    continue;
	SVL_ASSERT(actual[i] < (int)_matrix.size());
	SVL_ASSERT(predicted[i] < (int)_matrix[actual[i]].size());
	
	_matrix[actual[i]][predicted[i]] += 1;
    }
}

void svlConfusionMatrix::printCounts(ostream &os, const char *header) const
{
    if (header == NULL) {
        os << "--- Confusion matrix: (actual, predicted) ---" << endl;
    } else {
        os << header << endl;
    }
    for (int i = 0; i < (int)_matrix.size(); i++) {
        os << ROW_BEGIN;
	for (int j = 0; j < (int)_matrix[i].size(); j++) {
            if (j > 0) os << COL_SEP;
	    os << _matrix[i][j];
	}
	os << ROW_END << "\n";
    }
    os << "\n";
}

void svlConfusionMatrix::printRowNormalized(ostream &os, const char *header) const
{
    if (header == NULL) {
        os << "--- Confusion matrix: (actual, predicted) ---" << endl;
    } else {
        os << header << endl;
    }
    for (int i = 0; i < (int)_matrix.size(); i++) {
	double total = rowSum(i);
        os << ROW_BEGIN;
	for (int j = 0; j < (int)_matrix[i].size(); j++) {
            if (j > 0) os << COL_SEP;
	    os << fixed << setprecision(3) << setw(4) 
		 << ((double)_matrix[i][j] / total);
	}
	os << ROW_END << "\n";
    }
    os << "\n";    
}

void svlConfusionMatrix::printColNormalized(ostream &os, const char *header) const
{
    vector<double> totals;
    for (int i = 0; i < (int)_matrix[0].size(); i++) {
	totals.push_back(colSum(i));
    }

    if (header == NULL) {
        os << "--- Confusion matrix: (actual, predicted) ---" << endl;
    } else {
        os << header << endl;
    }
    for (int i = 0; i < (int)_matrix.size(); i++) {
        os << ROW_BEGIN;
	for (int j = 0; j < (int)_matrix[i].size(); j++) {
            if (j > 0) os << COL_SEP;
	    os << fixed << setprecision(3) << setw(4) 
		 << ((double)_matrix[i][j] / totals[j]);
	}
	os << ROW_END << "\n";
    }
    os << "\n";
}

void svlConfusionMatrix::printNormalized(ostream &os, const char *header) const
{
    double total = totalSum();

    if (header == NULL) {
        os << "--- Confusion matrix: (actual, predicted) ---" << endl;
    } else {
        os << header << endl;
    }
    for (int i = 0; i < (int)_matrix.size(); i++) {
        os << ROW_BEGIN;
	for (int j = 0; j < (int)_matrix[i].size(); j++) {
            if (j > 0) os << COL_SEP;
	    os << fixed << setprecision(3) << setw(4) 
		 << ((double)_matrix[i][j] / total);
	}
	os << ROW_END << "\n";
    }
    os << "\n";
}

void svlConfusionMatrix::printPrecisionRecall(ostream &os, const char *header) const
{
    if (header == NULL) {
        os << "--- Class-specific recall/precision ---" << endl;
    } else {
        os << header << endl;
    }

    // recall
    os << ROW_BEGIN;
    for (unsigned i = 0; i < _matrix.size(); i++) {
        if (i > 0) os << COL_SEP;
        double r = (_matrix[i].size() > i) ? (double)_matrix[i][i] / (double)rowSum(i) : 0.0;
        os << fixed << setprecision(3) << setw(4) << r;
    }
    os << ROW_END << "\n";

    // precision
    os << ROW_BEGIN;
    for (unsigned i = 0; i < _matrix.size(); i++) {
        if (i > 0) os << COL_SEP;
        double p = (_matrix[i].size() > i) ? (double)_matrix[i][i] / (double)colSum(i) : 1.0;
        os << fixed << setprecision(3) << setw(4) << p;
    }
    os << ROW_END << "\n";
    os << "\n";    
}

void svlConfusionMatrix::printF1Score(ostream &os, const char *header) const
{
    if (header == NULL) {
        os << "--- Class-specific F1 score ---" << endl;
    } else {
        os << header << endl;
    }

    os << ROW_BEGIN;
    for (unsigned i = 0; i < _matrix.size(); i++) {
        if (i > 0) os << COL_SEP;
        // recall
        double r = (_matrix[i].size() > i) ? (double)_matrix[i][i] / (double)rowSum(i) : 0.0;
        // precision
        double p = (_matrix[i].size() > i) ? (double)_matrix[i][i] / (double)colSum(i) : 1.0;
        os << fixed << setprecision(3) << setw(4) << ((2.0 * p * r) / (p + r));
    }
    os << ROW_END << "\n";
    os << "\n";    
}


void svlConfusionMatrix::printJaccard(ostream &os, const char *header) const
{
    if (header == NULL) {
        os << "--- Class-specific Jaccard coefficient ---" << endl;
    } else {
        os << header << endl;
    }

    os << ROW_BEGIN;
    for (unsigned i = 0; i < _matrix.size(); i++) {
        if (i > 0) os << COL_SEP;
        double p = (_matrix[i].size() > i) ? (double)_matrix[i][i] / 
            (double)(rowSum(i) + colSum(i) - _matrix[i][i]) : 0.0;
        os << fixed << setprecision(3) << setw(4) << p;
    }
    os << ROW_END << "\n";
    os << "\n";    
}


void svlConfusionMatrix::write(ostream &os) const
{
    printCounts(os);
}

void svlConfusionMatrix::read(istream &is)
{
    for (int i = 0; i < (int)_matrix.size(); i++) {
	for (int j = 0; j < (int)_matrix[i].size(); j++) {
	    is >> _matrix[i][j];
	}
    }    
}

double svlConfusionMatrix::rowSum(int n) const
{
    double v = 0.0;
    for (int i = 0; i < (int)_matrix[n].size(); i++) {
	v += (double)_matrix[n][i];
    }
    return v;
}

double svlConfusionMatrix::colSum(int m) const
{
    double v = 0;
    for (int i = 0; i < (int)_matrix.size(); i++) {
	v += (double)_matrix[i][m];
    }
    return v;
}

double svlConfusionMatrix::diagSum() const
{
    double v = 0;
    for (int i = 0; i < (int)_matrix.size(); i++) {
	if (i >= (int)_matrix[i].size())
	    break;
	v += (double)_matrix[i][i];
    }
    return v;
}

double svlConfusionMatrix::totalSum() const
{
    double v = 0;
    for (int i = 0; i < (int)_matrix.size(); i++) {
	for (int j = 0; j < (int)_matrix[i].size(); j++) {
	    v += (double)_matrix[i][j];
	}
    }
    return v;
}

double svlConfusionMatrix::accuracy() const
{
    return (diagSum() / totalSum());
}

double svlConfusionMatrix::avgPrecision() const
{
    double totalPrecision = 0.0;
    for (unsigned i = 0; i < _matrix.size(); i++) {
        totalPrecision += (_matrix[i].size() > i) ? 
            (double)_matrix[i][i] / (double)colSum(i) : 1.0;
    }
    
    return totalPrecision /= (double)_matrix.size();
}

double svlConfusionMatrix::avgRecall() const
{
    double totalRecall = 0.0;    
    for (unsigned i = 0; i < _matrix.size(); i++) {
        totalRecall += (_matrix[i].size() > i) ? 
            (double)_matrix[i][i] / (double)rowSum(i) : 0.0;
    }

    return totalRecall / (double)_matrix.size();
}

double svlConfusionMatrix::avgJaccard() const
{
    double totalJaccard = 0.0;    
    for (unsigned i = 0; i < _matrix.size(); i++) {
        totalJaccard += (_matrix[i].size() > i) ? 
            (double)_matrix[i][i] / (double)(rowSum(i) + colSum(i) - _matrix[i][i]) : 0.0;
    }

    return totalJaccard / (double)_matrix.size();
}

const unsigned svlConfusionMatrix::operator()(int i, int j) const
{
    return _matrix[i][j];
}

unsigned& svlConfusionMatrix::operator()(int i, int j)
{
    return _matrix[i][j];
}

// configuration --------------------------------------------------------

class svlConfusionMatrixConfig : public svlConfigurableModule {
public:
    svlConfusionMatrixConfig() : svlConfigurableModule("svlML.svlConfusionMatrix") { }

    void usage(ostream &os) const {
        os << "      colSep       :: column separator (default: \"" << svlConfusionMatrix::COL_SEP << "\")\n"
           << "      rowBegin     :: start of row token (default: \"" << svlConfusionMatrix::ROW_BEGIN << "\")\n"
           << "      rowEnd       :: end of row token (default: \"" << svlConfusionMatrix::ROW_END << "\")\n";
    }

    void setConfiguration(const char *name, const char *value) {
        // factor operation cache
        if (!strcmp(name, "colSep")) {
            svlConfusionMatrix::COL_SEP = string(value);
        } else if (!strcmp(name, "rowBegin")) {
            svlConfusionMatrix::ROW_BEGIN = string(value);
        } else if (!strcmp(name, "rowEnd")) {
            svlConfusionMatrix::ROW_END = string(value);
        } else {
            SVL_LOG(SVL_LOG_FATAL, "unrecognized configuration option for " << this->name());
        }
    }
};

static svlConfusionMatrixConfig gConfusionMatrixConfig;
