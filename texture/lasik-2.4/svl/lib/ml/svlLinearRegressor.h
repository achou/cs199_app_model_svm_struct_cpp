/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlLinearRegressor.h
** AUTHOR(S):   Beyang Liu <beyangl@cs.stanford.edu>
**              Stephen Gould <sgould@stanford.edu>
** DESCRIPTION:
**  Implements linear regression optimization with either L2 or Huber penalty.
**
*****************************************************************************/

#pragma once

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <limits>

#include "Eigen/Core"

#include "svlBase.h"

using namespace std;

class svlLinearRegressor : public svlOptimizer {
 protected:
    vector<vector<double> > *_features;
    vector<double> *_labels;
    vector<double> *_weights;

    bool _useHuber;
    double _tau;

 public:
    svlLinearRegressor(bool bHuber = false, double tau = 1.0);
    ~svlLinearRegressor();

    void initialize(unsigned n, vector<vector<double> > &features,
        vector<double> &labels, vector<double> &weights, const double *x = NULL);

    double objective(const double *x);
    void gradient(const double *x, double *df);
    double objectiveAndGradient(const double *x, double *df);

    void predictLabels(vector<double> &labels, vector<vector<double> > &features, const double *x) const;
    void predictLabels(vector<double> &labels, vector<vector<double> > &features, vector<double> &x) const;

    void getSolution(vector<double> &soln) const;
};
