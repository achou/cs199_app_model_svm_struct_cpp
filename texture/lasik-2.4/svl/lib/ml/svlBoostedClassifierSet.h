/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlBoostedClassifierSet.h
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**              Olga Russakovsky <olga@cs.stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
** DESCRIPTION:
**  Implements a set (vector) of boosted classifiers as 1-vs-all binary
**  classifiers. Uses options:
**    boostMethod    : boosting method: GENTLE, DISCRETE, etc.
**    boostingRounds : number of rounds of boosting
**    numSplits      : number of decision tree splits
**    trimRate       : boosting trim rate
**    pseudoCounts   : Dirichlet prior pseudo-counts
**
*****************************************************************************/

#pragma once

#include <vector>

#include "cv.h"
#include "cxcore.h"
#include "ml.h"

#include "svlBase.h"
#include "svlML.h"
#include "svlPRcurve.h"
#include "svlBoostedClassifier.h"

using namespace std;

// svlBoostedClassifierSet class ---------------------------------------------

class svlBinaryClassifier;
class svlBoostedClassifier;

class svlBoostedClassifierSet : public svlClassifier {
 public:
    static string DEFAULT_MODEL_EXT;
    
 protected:
    vector<svlBoostedClassifier> _models;

 public:
    svlBoostedClassifierSet(unsigned n = 0);
    svlBoostedClassifierSet(const svlBoostedClassifierSet& bcs);
    virtual ~svlBoostedClassifierSet();

    inline int size() const { return (int)_models.size(); }
    bool trained() const;
    bool trained(int modelIndx) const;

    // from svlOptions, propagates the options down to each of the _models
    void optionChanged(const string& name, const string& value);

    // i/o
    void initialize(unsigned n);
    bool save(const char *filename, const char *modelExt, int modelIndx = -1) const;    
    bool save(const char *filename) const;  // appends default extension to model files
	bool save(XMLNode &node) const;
    bool load(const char *filename) { return this->svlClassifier::load(filename); }
    bool load(XMLNode& node);

    // set/append pre-trained model
    void addModel(svlBoostedClassifier& model);
    void setModel(svlBoostedClassifier& model, int modelIndx);
  
    // train (returns average recall)
    double train(const MatrixXd& x, const VectorXi& y);
    double train(const vector<vector<double> >& x, const vector<int>& y);
    double train(const MatrixXd& x, const VectorXi& y,
        int modelIndx); // modelIndx = -1 will train all
    double train(const vector<vector<double> >& x, const vector<int>& y,
        int modelIndx); // modelIndx = -1 will train all

    void getClassScores(const VectorXd &x, VectorXd &y) const;
    void getClassScores(const vector<double> &x, vector<double> &y) const;
    void getClassScores(const vector<vector<double> >& x,
        vector<vector<double> >& y) const;
    double getSingleClassScore(const vector<double>& x, int modelIndx) const;

    // validate (returns average recall)
    double validate(const vector<vector<double> >& x, const vector<int>& y,
        int modelIndx) const;

};

SVL_AUTOREGISTER_H( BOOSTEDCLASSIFIERSET, svlClassifier);
