/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlClassifier.cpp
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
**
*****************************************************************************/

#include <cassert>
#include <iostream>
#include <fstream>
#include <limits>
#include <vector>

#include "svlBase.h"
#include "svlClassifier.h"
#include "svlBoostedClassifier.h"
#include "svlLogistic.h"
#include "svlBoostedClassifierSet.h"
#include "svlDecisionTree.h"
#include "svlDecisionTreeSet.h"
#include "svlNativeBoostedClassifier.h"
#include "svlSVM.h"

using namespace std;

svlClassifier::svlClassifier() : svlOptions(), 
    _nFeatures(0), _nClasses(0), _trained(false) 
{
    // do nothing
}

svlClassifier::svlClassifier(unsigned n, unsigned k) : svlOptions(),
    _nFeatures(n), _nClasses(k), _trained(false)
{
    // do nothing
}

svlClassifier::svlClassifier(const svlClassifier &c) : svlOptions(c),
    _nFeatures(c._nFeatures), _nClasses(c._nClasses), _trained(c._trained)
{
    // do nothing
}

void svlClassifier::initialize(unsigned n, unsigned k)
{
    _nFeatures = n;
    _nClasses = k;
    _trained = false;
}

bool svlClassifier::load(const char *filename)
{
    SVL_ASSERT(filename != NULL);

    XMLNode root = XMLNode::parseFile(filename);
    if ( root.isEmpty() ) {
        SVL_LOG(SVL_LOG_ERROR, "invalid svlClassifier file " << filename);
        return false;
    }

    if ( strcmp(root.getName(),"xml") == 0 && root.nChildNode() > 0 ) {
        root = root.getChildNode(0);
    }
    
    return load(root);
}

bool svlClassifier::load(XMLNode& node)
{
    SVL_ASSERT_MSG(!node.isEmpty(), "XML node is empty in svlClassifier::load(XMLNode&)");

    _trained = false;
    if (strcasecmp(node.getName(), "svlClassifier")) {
        SVL_LOG(SVL_LOG_ERROR, "invalid svlClassifier XML node");
        return false;
    }

    if (node.getAttribute("numFeatures") != NULL) {
        _nFeatures = atoi(node.getAttribute("numFeatures"));
    }

    if (node.getAttribute("numClasses") != NULL) {
        _nClasses = atoi(node.getAttribute("numClasses"));
    }
    
    _trained = true;
    return true;
}

// Save to a file by passing the buck to the XMLNode saver.
bool svlClassifier::save(const char *fname) const 
{
	XMLNode root = XMLNode::createXMLTopNode("xml",true);
	save(root);
	root.getChildNode(0).writeToFile(fname);
	return true;
}

// Save to an XMLNode.
bool svlClassifier::save(XMLNode &node) const
{
    // Create root classifier node.
    XMLNode root = node.addChild("svlClassifier");
    root.addAttribute("id", "Classifier");
    root.addAttribute("version", "1");
    root.addAttribute("numFeatures", toString(_nFeatures).c_str());
    root.addAttribute("numClasses", toString(_nClasses).c_str());
    return true;
}

double svlClassifier::train(const MatrixXd & samples, const VectorXi & labels, 
    const VectorXd & weights)
{
    return train(samples, labels);
}

double svlClassifier::train(const vector<vector<double> >& samples, const vector<int> &labels)
{
    MatrixXd X = stlToEigen(samples);
    VectorXi Y = stlToEigenV(labels);
  
    return train(X, Y);
}

double svlClassifier::train(const MatrixXf & X, const VectorXi & Y)
{
  MatrixXd Xd = MatrixXd::Zero(X.rows(), Y.cols());
  for (int i = 0; i < X.rows(); i++)
    for (int j = 0; j < X.cols(); j++)
      Xd(i,j) = X(i,j); //todo-- do this with a cast
  return train(Xd,Y);
}

double svlClassifier::train(const vector<vector<double> >&samples, const vector<int> &labels,
    const vector<double> &weights)
{
    MatrixXd X = stlToEigen(samples);
    VectorXi Y = stlToEigenV(labels);
    VectorXd W = stlToEigenV(weights);

    return train(X, Y, W);
}

double svlClassifier::train(const char *filename)
{
    SVL_ASSERT(filename != NULL);

    ifstream ifs(filename);
    if (ifs.fail()) {
        SVL_LOG(SVL_LOG_ERROR, "svlClassifier::train(): could not open training file " << filename);
        return -1.0;
    }

    vector<vector<double> > x;
    vector<int> y;

    vector<double> v(_nFeatures);
    int d;
    while (!ifs.fail()) {
        for (unsigned i = 0; i < v.size(); i++) {
            ifs >> v[i];
        }
        ifs >> d;
        if (ifs.fail()) break;
        x.push_back(v);
        y.push_back(d);
    }
    ifs.close();

    return train(x, y);
}

void svlClassifier::getClassScores(const vector<double> &sample,
    vector<double> &outputScores) const
{
    VectorXd x = stlToEigenV(sample);
    VectorXd o;
    getClassScores(x,o);
    outputScores = eigenToSTL(o);
}

void svlClassifier::getClassScores(const MatrixXd &samples, MatrixXd &outputScores) const
{
    outputScores = MatrixXd::Zero(samples.rows(), _nClasses);
    
    VectorXd output;
    for (int i = 0; i < (int)samples.rows(); i++) {
        VectorXd row = samples.row(i); //TODO-- find more efficient way of doing this, or add a getClassScores that works on blocks rather than vectors?
        getClassScores(row, output);
        outputScores.row(i) = output;
    }
}

void svlClassifier::getClassScores(const vector<vector<double> > &samples, 
    vector<vector<double> > &outputScores) const
{
    outputScores.clear();
    outputScores.resize(samples.size());
    for (int i = 0; i < (int)samples.size(); i++) {
        getClassScores(samples[i], outputScores[i]);
    }
}

void svlClassifier::getMarginals(const VectorXd & sample, VectorXd & outputMarginals) const
{
    getClassScores(sample, outputMarginals);
    expAndNormalize(outputMarginals);
}

void svlClassifier::getMarginals(const vector<double> &sample,
    vector<double> &outputMarginals) const
{
    getClassScores(sample, outputMarginals);
    expAndNormalize(outputMarginals);
}

// input: a set of examples, i.e. feature vectors
// output: for each example, a set of normalized marginals, one per class
void svlClassifier::getMarginals(const MatrixXd & samples, MatrixXd & outputMarginals) const
{
    outputMarginals = MatrixXd::Zero(samples.rows(), _nClasses);
    VectorXd output;
    for (int i = 0; i < (int) samples.rows(); i++) {
        VectorXd row = samples.row(i); //TODO-- find more efficient way of doing this
        getMarginals(row, output);
        outputMarginals.row(i) = output;		   
    }
}

void svlClassifier::getMarginals(const vector<vector<double> > &samples, 
    vector<vector<double> > &outputMarginals) const
{
    outputMarginals.clear();
    outputMarginals.resize(samples.size());
    for (int i = 0; i < (int)samples.size(); i++) {
        getMarginals(samples[i], outputMarginals[i]);
    }
}

int svlClassifier::getClassification(const VectorXd & sample) const
{
  VectorXd scores;
  getClassScores(sample, scores);
  return argmax(scores);
}

int svlClassifier::getClassification(const vector<double> &sample) const
{
    vector<double> scores;
    getClassScores(sample, scores);
    return argmax(scores);
}

// input: a set of examples, i.e. feature vectors
// output: for each example, the predicted class label
void svlClassifier::getClassifications(const MatrixXd &samples,
    VectorXi &outputLabels) const
{
  outputLabels = VectorXi::Zero(samples.rows());
    for (int i = 0; i < (int)samples.rows(); i++) {
      outputLabels(i) = getClassification(samples.row(i));
    }
}

void svlClassifier::getClassifications(const vector<vector<double> > &samples,
    vector<int> &outputLabels) const
{
    outputLabels.clear();
    outputLabels.resize(samples.size());
    for (int i = 0; i < (int)samples.size(); i++) {
        outputLabels[i] = getClassification(samples[i]);
    }
}

svlFactory<svlClassifier> & svlClassifierFactory()
{
  static svlFactory<svlClassifier> factory("svlClassifier");
  return factory;
}



SVL_AUTOREGISTER_CPP( LOGISTIC , svlClassifier, svlLogistic);
SVL_AUTOREGISTER_CPP( MULTICLASSLOGISTIC, svlClassifier, svlMultiClassLogistic);
SVL_AUTOREGISTER_CPP( DECISIONTREE, svlClassifier, svlDecisionTree);
SVL_AUTOREGISTER_CPP( DECISIONTREESET, svlClassifier, svlDecisionTreeSet);
SVL_AUTOREGISTER_CPP( CVBOOST, svlClassifier, svlBoostedClassifier);
SVL_AUTOREGISTER_FILECHECKER_CPP( svlCvBoostFileChecker, svlClassifier);
SVL_AUTOREGISTER_CPP( BOOSTEDCLASSIFIERSET, svlClassifier, svlBoostedClassifierSet );
SVL_AUTOREGISTER_CPP( NativeBoostedClassifier, svlClassifier, svlNativeBoostedClassifier);
SVL_AUTOREGISTER_CPP( SVM, svlClassifier, svlSVM);
SVL_AUTOREGISTER_FILECHECKER_CPP( svlSVMFileChecker, svlClassifier);
