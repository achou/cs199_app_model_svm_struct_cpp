/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlDecisionTree.cpp
** AUTHOR(S):   Adam Coates <acoates@stanford.edu>
**	        Paul Baumstarck <pbaumstarck@stanford.edu>
**
*****************************************************************************/

#include "svlDecisionTree.h"

// Constructors and destructors.

svlDecisionTreeNode::svlDecisionTreeNode(const svlDecisionTree *parent)
	: _parent(parent),
	_num_samples(0), _pos_weight(0.0), _neg_weight(0.0), _gini_index(0.0),
	_split_var(-1), _split_val(0.0), _Lp(0.0), _Rp(0.0) {
	_L = _R = NULL;
}

svlDecisionTreeNode::~svlDecisionTreeNode() {
	if ( _L ) delete _L;
	if ( _R ) delete _R;
}

// Promulgate signal to grow tree to the leaf nodes.
void svlDecisionTreeNode::growTree() {
	if ( _L ) _L->growTree();
	else _L = new svlDecisionTreeNode(_parent);

	if ( _R ) _R->growTree();
	else _R = new svlDecisionTreeNode(_parent);
}

// Classify a single sample.
double svlDecisionTreeNode::classify(const VectorXd &sample) const
{
	return sample(_split_var) >= _split_val
			? ( _R ? _R->classify(sample) : _Rp )
			: ( _L ? _L->classify(sample) : _Lp );
}

// Train the leaves. Return true if we either passed the buck or if, as a leaf, we set our Gini properly.
bool svlDecisionTreeNode::train(const MatrixXd &X, const VectorXi &y, const VectorXd *w, svlBitArray *map)
{
	if ( !( _L || _R ) ) {
		// If a leaf, add data to the histograms and determine split.
		int n_dims = X.cols();
		svlDTNhistogram H(n_dims, _parent->_numBins);
		// Inter all marked (mapped) results in histogram.
		for ( int i=0; i<X.rows(); ++i ) {
			if ( map && !map->get(i) )
				continue;
			++H.num_samples;
			for ( int j=0; j<n_dims; ++j ) {
				( y(i) == 1 ? H.Hpos : H.Hneg )( j, _parent->binNum(X(i,j)) ) += ( !w ? 1.0 : (*w)(i) );
				( y(i) == 1 ? H.Wpos : H.Wneg )(j) += ( !w ? 1.0 : (*w)(i) );
			}
		}
		/*svlBinaryWriteMatrixXi("pos.out", H.Hpos);
		svlBinaryWriteMatrixXi("neg.out", H.Hneg);*/
		// Return true on finding a valid Gini index.
		return setGiniIndex(H);
	} else if ( _L && _R ) {
		// Create new boolean masks for children.
		svlBitArray *lmap = NULL, *rmap = NULL;
		if ( map ) {
			// Cone map bitmap.
			lmap = new svlBitArray(*map);
			rmap = new svlBitArray(*map);
		} else {
			// Create new with all bits set.
			lmap = new svlBitArray(X.rows());
			rmap = new svlBitArray(X.rows());
			lmap->setAll();
			rmap->setAll();
		}
		// Set and clear bits in child-masks.
		for ( int i=0; i<X.rows(); ++i ) {
			// If we own this sample, map will be NULL or we'll have the bit.
			if ( !map || map->get(i) ) {
				// If >= split val, goes to right, so clear left; o.w., clear right.
				( X(i,_split_var) >= _split_val ? lmap : rmap )->clear(i);
			}
		}
		if ( !_L->train(X, y, w, lmap) ) {
			/*static int xena; char c[32]; sprintf(c,"left_%d.out",xena++); lmap->save(c);*/
			delete _L;
			_L = NULL;
		}
		if ( !_R->train(X, y, w, rmap) ) {
			delete _R;
			_R = NULL;
		}
		delete lmap;
		delete rmap;
	} else {
		SVL_LOG(SVL_LOG_FATAL, "Node unexpectedly has only one child.");
	}
	return true;
}

// Returns true if node set its Gini index.
bool svlDecisionTreeNode::setGiniIndex(const svlDTNhistogram &H)
{
	_split_var = -1;
	double bestGI = 1.0, split = 0.0, lp = 0.0, rp = 0.0;
	// Find best split by giniIndex
	for ( int i=0; i<H.Hpos.rows(); i++) {
		//printf("Gini loop: %d > %0.3f %0.3f %d\n", i, H.Wpos(i), H.Wneg(i), H.num_samples);

		// Make sure we have sufficient data in this dimension.
		if ( H.Wpos(i) > 0.0 && H.Wneg(i) > 0.0 && H.num_samples > 0 ) {
			double gd = giniIndex(H, i, split, lp, rp);
			//printf(" gd back: %0.3f, %0.3f %0.3f %0.3f\n", gd, split, lp, rp);
			if ( gd < bestGI ) {
				bestGI = gd;
				_num_samples = H.num_samples;
				_pos_weight = H.Wpos(i);
				_neg_weight = H.Wneg(i);
				_gini_index = gd;
				_split_val = split;
				_split_var = i;
				_Lp = lp;
				_Rp = rp;
			}
		}
		// else, do nothing.
	}
	// Return false if we mever set split_var.
	return _split_var != -1;
}

// Calculate Gini index along a specific histogram dimension.
double svlDecisionTreeNode::giniIndex(const svlDTNhistogram &H, int j, double &bestSplit, double &leftProb, double &rightProb) const {
	double Lp = 0, Ln = 0;
	double Rp = H.Wpos(j), Rn = H.Wneg(j); // Mass starts at right side.
	if (Rp + Rn == 0)
		return 1;

	// Compute gini value for each possible split.
	double bestGini = 1;
	const unsigned n_bins = H.Hpos.cols();
	for ( unsigned i=0; i+1<n_bins; ++i ) {
		// If we split at right edge of current bin, then current bin's mass should go to the left side.
		Lp += H.Hpos(j,i);
		Rp -= H.Hpos(j,i);
		Ln += H.Hneg(j,i);
		Rn -= H.Hneg(j,i);
		double Lcount = Lp+Ln, Rcount = Rp+Rn;

		/** BRUTE FORCE **/
		Lp = Rp = Ln = Rn = 0;
		for ( unsigned k=0; k<=i; ++k ) {
			Lp += H.Hpos(j,k);
			Ln += H.Hneg(j,k);
		}
		for ( unsigned k=i+1; k<n_bins; ++k ) {
			Rp += H.Hpos(j,k);
			Rn += H.Hneg(j,k);
		}
		Lcount = Lp+Ln; Rcount=Rp+Rn;

		double Lgini = 1, Rgini = 1;
		if ( Lcount > 0 && Rcount > 0 ) {
			// TODO: these epsilons may cause trouble with small weights.
			if ( Lcount > 1.0e-9 ) Lgini = 1.0 - (Lp*Lp + Ln*Ln) / (Lcount*Lcount);
			if ( Rcount > 1.0e-9 ) Rgini = 1.0 - (Rp*Rp + Rn*Rn) / (Rcount*Rcount);

			double g = ( Lgini*Lcount + Rgini*Rcount )/( Lcount + Rcount );
			if ( g < bestGini ) {
				bestGini = g;
				bestSplit = _parent->binMax(i);
				rightProb = Rp/Rcount;
				leftProb = Lp/Lcount;
			}
		}

	}

	return bestGini;
}

// Prune nodes.
void svlDecisionTreeNode::prune(int minSamples, double minPos, double minNeg)
{
	//if ( _L ) printf("L: %d/%d %0.3f/%0.3f %0.3f/%0.3f\n", _L->_num_samples, minSamples, _L->_pos_weight, minPos, _L->_neg_weight, minNeg);
	if ( _L && ( _L->_num_samples < minSamples || _L->_pos_weight < minPos || _L->_neg_weight < minNeg ) ) {
		delete _L;
		_L = NULL;
	}
	//if ( _R ) printf("R: %d/%d %0.3f/%0.3f %0.3f/%0.3f\n", _R->_num_samples, minSamples, _R->_pos_weight, minPos, _R->_neg_weight, minNeg);
	if ( _R && ( _R->_num_samples < minSamples || _R->_pos_weight < minPos || _R->_neg_weight < minNeg ) ) {
		delete _R;
		_R = NULL;
	}

	// Recurse if we still have children.
	if ( _L ) _L->prune(minSamples, minPos, minNeg);
	if ( _R ) _R->prune(minSamples, minPos, minNeg);
}

// Load from an XML node.
bool svlDecisionTreeNode::load(XMLNode node)
{
	const char *c = node.getAttribute("Value");
	stringstream ss(c);
	unsigned hasLeft = 0, hasRight = 0;
	ss  >> _num_samples
		>> _pos_weight
		>> _neg_weight
		>> _gini_index
		>> _split_var
		>> _split_val
		>> _Lp
		>> _Rp
		>> hasLeft
		>> hasRight;
	ss.clear();

	if ( hasLeft + hasRight > unsigned(node.nChildNode()) ) {
		SVL_LOG(SVL_LOG_FATAL, "Expecting more child nodes: " << hasLeft << "+" << hasRight << " > " << node.nChildNode() );
		return false;
	}

	int i = 0;
	if ( hasLeft ) {
		_L = new svlDecisionTreeNode(_parent);
		_L->load(node.getChildNode(i++));
	}
	if ( hasRight ) {
		_R = new svlDecisionTreeNode(_parent);
		_R->load(node.getChildNode(i++));
	}
	return true;
}

// Save to an XML true, assuming we are given a parent node.
bool svlDecisionTreeNode::save(XMLNode &root) const
{
	XMLNode me = root.addChild("Node");
	stringstream ss;
	ss  << _num_samples << " "
		<< _pos_weight << " "
		<< _neg_weight << " "
		<< _gini_index << " " 
		<< _split_var << " " 
		<< _split_val << " " 
		<< _Lp << " " 
		<< _Rp << " " 
		<< ((_L)?1:0) << " " 
		<< ((_R)?1:0);
	me.addAttribute("Value",ss.str().c_str());
	ss.clear();

	if ( _L ) _L->save(me);
	if ( _R ) _R->save(me);
	return true;
}



// *** svlDecisionTree class ***

svlDecisionTree::svlDecisionTree(double min_val, double max_val, int num_bins)
	: svlBinaryClassifier(), _numBins(num_bins), _minVal(min_val), _maxVal(max_val)
{
}

svlDecisionTree::~svlDecisionTree()
{
	if ( _nodes.size() > 0 )
		delete _nodes.front();
}

// Save the nodes' information.
//bool svlDecisionTree::save(const char *fname) const 
//{
//	XMLNode root = XMLNode::createXMLTopNode("xml",true);
//	save(root);
//#if 1
//	root.getChildNode(0).writeToFile(fname);
//#else
//	root.writeToFile(fname);
//#endif
//	return true;
//}

bool svlDecisionTree::save(XMLNode &node) const
{
	if ( strcmp(node.getAttribute("id"),"DecisionTree") != 0 ) {
		SVL_LOG(SVL_LOG_FATAL, "Improper classifier id" << node.getAttribute("id"));
		return false;
	}
	if ( !this->svlClassifier::save(node) )
		return false;
	
	// Get classifier node as last instantiated child.
	XMLNode me = node.getChildNode(node.nChildNode()-1);
	me.updateAttribute("DecisionTree", NULL, "id");
	
	// Add root-level attributes.
	char c[32];
	sprintf(c,"%d",_numBins);
	me.addAttribute("NumBins",c);
	sprintf(c,"%0.6f",_minVal);
	me.addAttribute("MinVal",c);
	sprintf(c,"%0.6f",_maxVal);
	me.addAttribute("MaxVal",c);

	if ( !_nodes.empty() )
		_nodes.front()->save(me);

	return true;
}
   
bool svlDecisionTree::load(XMLNode& node)
{
	if ( !this->svlClassifier::load(node) )
        return false;

	// Read root-level attributes.
	const char *c;
	if ( (c=node.getAttribute("NumBins")) )
		_numBins = atoi(c);
	else
		SVL_LOG(SVL_LOG_FATAL, "Missing attribute NumBins");

	if ( (c=node.getAttribute("MinVal")) )
		_minVal = atof(c);
	else
		SVL_LOG(SVL_LOG_FATAL, "Missing attribute MinVal");

	if ( (c=node.getAttribute("MaxVal")) )
		_maxVal = atof(c);
	else
		SVL_LOG(SVL_LOG_FATAL, "Missing attribute MaxVal");

	initialize(_nFeatures, _nClasses);

	// Parse child nodes.
	XMLNode child = node.getChildNode("Node");
	if ( !child.isEmpty() ) {
		_nodes.clear();
		_nodes.push_back(new svlDecisionTreeNode(this));
		_nodes.back()->load(child);
	}

	return true;
}


// Score using the root node.
double svlDecisionTree::getScore(const VectorXd &sample) const
{
	return _nodes.front()->classify(sample);
}

// Train the decision tree to a specified depth.
double svlDecisionTree::train(const MatrixXd &X, const VectorXi &y, int depth)
{
	if ( _nodes.size() == 0 ) {
		// Create root.
		_nodes.push_back(new svlDecisionTreeNode(this));
	}
	for ( int d=0; d<depth; ++d ) {
		_nodes.front()->train(X, y);
		if ( d < depth-1 )
			_nodes.front()->growTree();
	}
	// Prune nodes.
	double min_w = 0.01*double(X.rows());
	_nodes.front()->prune(int(min_w), min_w, min_w);

	return -1.0;
}


