/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlGaussian.h
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
** DESCRIPTION:
**  Implements a multi-variate gaussian distribution.
**
*****************************************************************************/

#pragma once

#include <vector>
#include <limits>

#include "Eigen/Core"
#include "Eigen/Array"
#include "Eigen/Cholesky"
#include "Eigen/LU"

#include "svlBase.h"
#include "svlSufficientStats.h"

using namespace std;

// svlGaussian class ---------------------------------------------------------

class svlConditionalGaussian;

class svlGaussian {
 public:
    static bool AUTO_RIDGE;

 private:
    int _n;
    VectorXd _mu;             // n element mean vector
    MatrixXd _Sigma;          // n-by-n element covariance matrix

    mutable MatrixXd *_invSigma; // n-by-n inverse covariance matrix
    mutable double _logZ;        // log partition function (\sqrt(2*pi*det(Sigma)^n))
    mutable MatrixXd *_L;        // _Sigma = _L _L^T (Cholesky decomposition)
                              // _L is only needed for sampling, so constructors/updateCachedParameters make it NULL
                              // _L is generated the first time a sampling method is called

 public:
    // n dimensional standard gaussian
    svlGaussian(int n = 1);
    svlGaussian(const VectorXd& mu, double sigma2);
    svlGaussian(const VectorXd& mu, const MatrixXd& sigma2);
    svlGaussian(const vector<double>& mu, double sigma2);
    svlGaussian(const svlSufficientStats& stats);
    svlGaussian(const svlGaussian& model);
    ~svlGaussian();

    // initialization
    void initialize(int n);
    void initialize(const VectorXd& mu, double sigma2);
    void initialize(const VectorXd& mu, const MatrixXd& sigma2);

    //marginalizing
    svlGaussian *marginalize(const vector<int> & indx) const;

    // conditioning
    svlGaussian reduce(const vector<double>& x,
        const vector<int>& indx) const;
    svlGaussian reduce(const map<int, double>& x) const;
    svlConditionalGaussian conditionOn(const vector<int>& indx) const;

    // i/o
    bool save(const char *filename, bool bBinary = false) const;
    bool load(const char *filename, bool bBinary = false);
    bool load(ifstream &ifs, bool bBinary = false);
    bool save(ofstream &ofs, bool bBinary = false) const;

    // evaluate (log-likelihood)
    void evaluate(const MatrixXd& x, VectorXd& p) const;
    void evaluate(const vector<vector<double> >& x, vector<double>& p) const;
    double evaluateSingle(const VectorXd& x) const;
    double evaluateSingle(const vector<double>& x) const;
    double evaluateSingle(double x) const;

    // sampling
    VectorXd sample() const;
    void sample(VectorXd& x) const;
    
    // learn parameters    
    void train(const MatrixXd& x, double lambda = 0.0);
    void train(const vector<vector<double> >& x, double lambda = 0.0);
    void train(const vector<double> &x, double lambda = 0.0);
    void train(const svlSufficientStats& stats, double lambda = 0.0);

    // access
    unsigned dimension() const { return _n; }
    const VectorXd& mean() const { return _mu; }
    const MatrixXd& covariance() const { return _Sigma; }
    double logPartitionFunction() const;
    
    double klDivergence(const svlGaussian& model) const;
    double klDivergence(const svlSufficientStats& stats) const;

    // standard operators
    svlGaussian& operator=(const svlGaussian& model);

 protected:
    inline void guaranteeInvSigma() const;
    void freeStorage();
    void updateCachedParameters();
};

// svlConditionalGaussian class ----------------------------------------------

class svlConditionalGaussian {
 private:
    int _n, _m;
    VectorXd _mu;             // n element mean vector \mu_1 - \Sigma_{12} \Sigma_{22}^{-1} \mu_2
    MatrixXd _Sigma;          // n-by-n element covariance matrix \Sigma_{11} - \Sigma_{12} \Sigma_{22}^{-1} \Sigma_{21}
    MatrixXd _SigmaGain;      // m-by-n gain matrix: \Sigma_{12} \Sigma_{22}^{-1}

 public:
    svlConditionalGaussian(const VectorXd& mu, const MatrixXd& Sigma, 
        const MatrixXd& SigmaGain);
    svlConditionalGaussian(const svlConditionalGaussian& model);
    ~svlConditionalGaussian();

    // i/o
    bool save(const char *filename) const;
    bool load(const char *filename);

    // construct gaussian given observation
    svlGaussian reduce(const VectorXd& x);
    svlGaussian reduce(const vector<double>& x);
};



