/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlBinaryClassifier.cpp
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
**
*****************************************************************************/

#include <cassert>
#include <iostream>
#include <fstream>
#include <limits>
#include <vector>

#include "cv.h"
#include "cxcore.h"
#include "ml.h"

#include "svlBase.h"
#include "svlBoostedClassifierSet.h"

using namespace std;

// svlBinaryClassifierStats --------------------------------------------------------

double svlBinaryClassifierStats::computeFscore(bool normalize) const
{
    double precision = 0;
    double recall = 0;

    double Fscore = 0;

    int num_pos = tp + fn;
    int num_neg = fp + tn;

    if (num_pos && num_neg) {
        if (normalize) {
            double tp_norm = (double)tp / num_pos;
            double fp_norm = (double)fp / num_neg;

            recall = (double)tp_norm;
            if (tp_norm + fp_norm > 0) precision = (double)tp_norm / (tp_norm + fp_norm);
	} else {
            recall = ((double) tp) / (double) (num_pos);
            if (tp+fp > 0) precision = ((double) tp) / (double) (tp+fp);
	}

        if (precision > 0 && recall > 0)
            Fscore =  2.0f / ((1.0f/precision) + (1.0f/recall));
    }

    return Fscore;
}

// svlBinaryClassifier class -------------------------------------------------------

void svlBinaryClassifier::initialize(unsigned n, unsigned k)
{
    SVL_ASSERT_MSG(k == 2, "number of classes must be 2 for a binary classifier");
    this->initialize(n);
}

void svlBinaryClassifier::initialize(unsigned n)
{
    svlClassifier::initialize(n, 2);
}

double svlBinaryClassifier::train(const vector<vector<double> >& posSamples, const vector<vector<double> >&negSamples)
{
  if (posSamples.size() == 0)
    return 0;

  unsigned width = posSamples[0].size();

  MatrixXd X = MatrixXd::Zero(posSamples.size()+negSamples.size(), width);
  VectorXi Y = VectorXi::Zero(posSamples.size()+negSamples.size());

  for (unsigned i = 0; i < posSamples.size(); i++)
    {
      Y(i) = 1;
      for (unsigned j = 0; j < width; j++)
	X(i,j) = posSamples[i][j];
    }

  for (unsigned i = 0; i < negSamples.size(); i++)
    {
      Y(i) = 0;
      for (unsigned j = 0; j < width; j++)
	X(posSamples.size()+i,j) = negSamples[i][j];
    }

  return dynamic_cast<svlClassifier *>(this)->train(X,Y);
}

double svlBinaryClassifier::train(const MatrixXd& posSamples, const MatrixXd &negSamples)
{
  int width = posSamples.cols();

  SVL_ASSERT(negSamples.cols() == width);

  MatrixXd X = MatrixXd::Zero(posSamples.rows()+negSamples.rows(), width);
  VectorXi Y = VectorXi::Zero(posSamples.rows()+negSamples.rows());

  for (int i = 0; i < posSamples.rows(); i++)
    {
      Y(i) = 1;
      X.row(i) = posSamples.row(i);
    }

  for (int i = 0; i < negSamples.rows(); i++)
    {
      Y(i) = 0;
      X.row(posSamples.rows()+i) = negSamples.row(i);
    }

  return dynamic_cast<svlClassifier *>(this)->train(X,Y);
}

double svlBinaryClassifier::getScore(const vector<double> &sample) const
{
    VectorXd vx = Eigen::Map<VectorXd>(&sample[0], sample.size());
    return getScore(vx);
}

void svlBinaryClassifier::getClassScores(const VectorXd & sample, VectorXd & outputScores) const
{
  outputScores = VectorXd::Zero(2);
  outputScores(1) = getScore(sample);
}

void svlBinaryClassifier::getClassScores(const vector<double> &sample,
    vector<double> &outputScores) const
{
    outputScores.resize(2);
    outputScores[0] = 0.0;
    outputScores[1] = getScore(sample);
}

void svlBinaryClassifier::getScores(const vector<vector<double> > &samples,
    vector<double> &outputScores) const
{
    outputScores.clear();
    outputScores.resize(samples.size());
    for (int i = 0; i < (int)samples.size(); i++) {
        outputScores[i] = getScore(samples[i]);
    }
}

void svlBinaryClassifier::getScores(const MatrixXd &samples,
				    vector<double> &outputScores) const
{
    outputScores.clear();
    outputScores.resize(samples.rows());
    for (int i = 0; i < (int)samples.rows(); i++) {
      VectorXd sample = samples.row(i);
        outputScores[i] = getScore(sample);
    }
}

void svlBinaryClassifier::getScores(const MatrixXf &samples,
				    vector<double> &outputScores) const
{
    outputScores.clear();
    outputScores.resize(samples.rows());
    for (int i = 0; i < (int)samples.rows(); i++) {
      VectorXf floatSample = samples.row(i);
      VectorXd sample(floatSample.size());
      for (int j = 0; i < sample.size(); j++)
	sample(j) = floatSample(j);//todo-- do this with a cast
      outputScores[i] = getScore(sample);
    }
}


double svlBinaryClassifier::getProbability(const vector<double> &sample) const
{
    return scoreToProbability(getScore(sample));
}

void svlBinaryClassifier::getProbabilities(const vector<vector<double> > &samples,
    vector<double> &outputProbs) const
{
    outputProbs.clear();
    outputProbs.resize(samples.size());
    for (int i = 0; i < (int)samples.size(); i++) {
        outputProbs[i] = getProbability(samples[i]);
    }
}

svlBinaryClassifierStats svlBinaryClassifier::getStats(const vector<vector<double> > &posSamples,
    const vector<vector<double> > &negSamples)  const
{
    vector<double> posSampleScores;
    vector<double> negSampleScores;

    getScores(posSamples, posSampleScores);
    getScores(negSamples, negSampleScores);

    svlBinaryClassifierStats stats;
    for (int i = 0; i < (int)posSamples.size(); i++) {
        if (posSampleScores[i] > 0.0)
            stats.tp += 1;
        else
            stats.fn += 1;
    }
    for (int i = 0; i < (int)negSamples.size(); i++) {
        if (negSampleScores[i] > 0.0)
            stats.fp += 1;
        else
            stats.tn += 1;
    }

    return stats;
}

svlBinaryClassifierStats svlBinaryClassifier::getStats(const vector<vector<double> > &samples,
    const vector<int> &labels, int posLabel) const
{
    vector<double> scores;
    getScores(samples, scores);

    svlBinaryClassifierStats stats;
    for (int i = 0; i < (int)samples.size(); i++) {
        if (labels[i] == posLabel) {
            // sample positive
            if (scores[i] > 0.0)
                stats.tp += 1;
            else
                stats.fn += 1;
        } else {
            // sample negative
            if (scores[i] > 0.0)
                stats.fp += 1;
            else
                stats.tn += 1;
        }
    }

    return stats;
}

svlBinaryClassifierStats svlBinaryClassifier::getStats(const MatrixXd & samples, const VectorXi & labels) const
{
    vector<double> scores;
    getScores(samples, scores);

    svlBinaryClassifierStats stats;
    for (int i = 0; i < (int)samples.size(); i++) {
        if (labels[i] == 1) {
            if (scores[i] > 0)
                stats.tp ++;
	    else
                stats.fn++;
	} else if (labels[i] == 0) {
            if (scores[i] > 0)
                stats.fp++;
            else
                stats.tn++;
	} else
            SVL_ASSERT(false);
    }
    return stats;
}

svlBinaryClassifierStats svlBinaryClassifier::getStats(const MatrixXf & samples, const VectorXi & labels) const
{
    vector<double> scores;
    getScores(samples, scores);

    svlBinaryClassifierStats stats;
    for (int i = 0; i < (int) samples.size(); i++) {
        if (labels[i] == 1) {
            if (scores[i] > 0)
                stats.tp ++;
            else
                stats.fn++;
	} else if (labels[i] == 0) {
            if (scores[i] > 0)
                stats.fp++;
            else
                stats.tn++;
	} else
            SVL_ASSERT(false);
    }
    return stats;
}

int svlBinaryClassifier::numClassifiedAsPositive(const vector<vector<double> >& samples) const
{
    vector<double> scores;
    getScores(samples, scores);

    int result = 0;
    for (int j = 0; j < (int)samples.size(); j++) {
        if (scores[j] > 0.0)
            result += 1;
    }

    return result;
}

svlPRCurve svlBinaryClassifier::getPRCurve(const vector<vector<double> > &posSamples,
    const vector<vector<double> > &negSamples) const
{
    vector<double> posSampleScores;
    vector<double> negSampleScores;

    getProbabilities(posSamples, posSampleScores);
    getProbabilities(negSamples, negSampleScores);

    svlPRCurve c;
    c.accumulatePositives(posSampleScores);
    c.accumulateNegatives(negSampleScores);
    return c;
}

double svlBinaryClassifier::validate(const vector<vector<double> > &posSamples,
    const vector<vector<double> > &negSamples,
    const string label, bool useNormedFscore) const
{
    svlBinaryClassifierStats stats = getStats(posSamples, negSamples);
    return validate(stats, label, useNormedFscore);
}

double svlBinaryClassifier::validate(const MatrixXd &X, const VectorXi &Y,
    const string label, bool useNormedFscore) const
{
    svlBinaryClassifierStats stats = getStats(X,Y);
    return validate(stats, label, useNormedFscore);
}

double svlBinaryClassifier::validate(const MatrixXf &X, const VectorXi &Y,
    const string label, bool useNormedFscore) const
{
    svlBinaryClassifierStats stats = getStats(X,Y);
    return validate(stats, label, useNormedFscore);
}


double svlBinaryClassifier::validate(const svlBinaryClassifierStats &stats,
    const string label,  bool useNormedFscore) const
{
    double Fscore = stats.computeFscore(useNormedFscore);
    if (useNormedFscore) {
        SVL_LOG(SVL_LOG_MESSAGE, "Evaluation " << label << " (TP, FN, TN, FP, normed \"F\" score): "
            << stats.tp << ", " << stats.fn << ", " << stats.tn << ", " << stats.fp << ", " << Fscore);
    } else {
        SVL_LOG(SVL_LOG_MESSAGE, "Evaluation " << label << " (TP, FN, TN, FP, F-score): "
            << stats.tp << ", " << stats.fn << ", " << stats.tn << ", " << stats.fp << ", " << Fscore);
    }

    return Fscore;
}
