/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlHistogramOperators.h
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
** DESCRIPTION:
**  Doesn't maintain the storage for the histogram, but provides
** all the insertion operators. svlHistogram contains just one copy
** of it; svl2dHistogram contains two
** TODO: extend to non-uniform bins
**
*****************************************************************************/

#pragma once

#include <string>
#include <vector>
#include <fstream>
#include <cv.h>
#include "svlBase.h"

using namespace std;

// svlSoftBinAssignment ----------------------------------------------------
// Used to return a soft bin histogram assignment
// TODO: extend to more than two bins potentially
struct svlSoftBinAssignment {
  int _bin[2];
  float _binWeight[2];
  
  svlSoftBinAssignment() { 
    _bin[0] = _bin[1] = 0;
    _binWeight[0] = _binWeight[1] = 0.0;
  }
  svlSoftBinAssignment(int a, float aw, int b, float bw) {
    _bin[0] = a;
    _binWeight[0] = aw;
    _bin[1] = b;
    _binWeight[1] = bw;
  }
};

// svlHistogramOperators ----------------------------------------------------
class svlHistogramOperators
{
 private:
  // precomputed in constructor
  float _binSize;
  
  // set by user
  float _minVal;
  bool _circular;
  int _numBins;

  // corrects the bin as necessary to account for possible
  // overflow
  inline float getBin(float binValue) const {
    float bin = (binValue - _minVal) / _binSize;
    if (_circular) {
      while (bin < 0) bin += _numBins;
      while (bin >= _numBins) bin -= _numBins;
    } 
    return bin;
  }

 public:
  svlHistogramOperators(int numBins, float minValue, float maxValue,
			bool inclusive = false) :
    _binSize((maxValue - minValue) / numBins),
    _minVal(minValue), _circular(false),   
    _numBins(inclusive ? (numBins+1) : numBins)
      {
	SVL_ASSERT_MSG(_numBins > 0, "Can't create a histogram with "
		       << _numBins << " bins");
	SVL_ASSERT_MSG(maxValue > minValue, 
		       "Can't create a histogram with range ["
		       << minValue << ", " << maxValue << ")");
      }

  void makeCircular() { _circular = true; }
  
  float getRange() const { return _binSize * _numBins; }

  float getBinSize() const { return _binSize; }

  // return -1 on failure
  inline int getBinNum(float binValue) const
    {
      float bin = getBin(binValue);
      if (bin < 0 || bin >= _numBins) return -1;
      return (int)bin;		       
    }

  inline bool getBinNumLinearSmooth(float binValue,
				    svlSoftBinAssignment &sb) const {

    float bin = getBin(binValue);
    if (bin < 0 || bin >= _numBins) return false;

    // the two bins closest to this value
    int b = (int)bin;
    float diff = bin - b;
    if (diff < 0.5) {
      if (b > 0) {
	sb._bin[0] = b;
	sb._bin[1] = b-1;
	sb._binWeight[0] = diff + 0.5;
	sb._binWeight[1] = 0.5 - diff;
      } else {
	if (_circular) {
	  sb._bin[0] = b;
	  sb._bin[1] = _numBins-1;
	  sb._binWeight[0] = diff + 0.5;
	  sb._binWeight[1] = 0.5 - diff;
	} else {
	  sb._bin[0] = b;
	  sb._bin[1] = 0;
	  sb._binWeight[0] = 1.0;
	  sb._binWeight[1] = 0.0;
	}
      }
    } else {
      if (b+1 < _numBins) {
	sb._bin[0] = b;
	sb._bin[1] = b+1;
	sb._binWeight[0] = 1.5 - diff;
	sb._binWeight[1] = diff - 0.5;
      } else {
	if (_circular) {
	  sb._bin[0] = b;
	  sb._bin[1] = 0;
	  sb._binWeight[0] = 1.5 - diff;
	  sb._binWeight[1] = diff - 0.5;
	} else {
	  sb._bin[0] = b;
	  sb._bin[1] = 0;
	  sb._binWeight[0] = 1.0;
	  sb._binWeight[1] = 0.0;
	}
      }
    }
    
    return true;
  }
};
