/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlNativeBoostedClassifier.cpp
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@cs.stanford.edu>
**
*****************************************************************************/

#include "svlNativeBoostedClassifier.h"

svlNativeBoostedClassifier::svlNativeBoostedClassifier(unsigned n, unsigned k)
	: svlClassifier(n, k), _method(0)
{
	_timer = NULL;
}

svlNativeBoostedClassifier::svlNativeBoostedClassifier(const svlNativeBoostedClassifier &model)
	: svlClassifier(model._nFeatures, model._nClasses), _method(0)
{
	_timer = NULL;
}

svlNativeBoostedClassifier::~svlNativeBoostedClassifier()
{
}

double svlNativeBoostedClassifier::train(const MatrixXd &X, const VectorXi &y, const string &subClassID, int nRounds, const VectorXd *w_in)
{	
	//SVL_ASSERT_MSG(random_frac >= 0.0f && random_frac <= 1.0f, "Random fraction out of range");

	int n = X.rows();
	// Initialize weights.
	VectorXd w;
	if ( !w_in ) {
		w = VectorXd::Ones(n);
		w /= double(n);
	} else {
		w = *w_in;
		w /= w.sum();
	}
	double gd;

	if ( _timer ) _timer->push(nRounds);
	char c[32];
	for ( int m=0; m<nRounds; ++m ) {
		//if ( _timer ) _timer->print();
		printf("*** %d (%0.4f)\n", m, w.sum());
		// Output weights.
		sprintf(c,"weights_%d.out",m); svlBinaryWriteVectorXd(c, w);

		// Do stochastic sampling of results.
		//if ( _random_fraction > 0.0f ) {
		//	printf("In here!\n");
		//	if ( mask )
		//		cur_mask->copy(*mask);
		//	cur_mask->clearAll();
		//	vector<unsigned> sub_ix;
		//	randPerm(n, sub_ix);
		//	// Set rix number bits.
		//	for ( unsigned i=0; i<r_ix; ++i )
		//		cur_mask->set(sub_ix[i]);
		//}

		// Fit the classifier.
		svlClassifier *trainme = svlClassifierFactory().make(subClassID);
		// Set essential parameters as the above calls only a default constructor.
		trainme->initialize(_nFeatures, _nClasses);
		printf("train\n");
		trainme->train(X, y, w);
		sprintf(c,"trainme_%d.out",m); trainme->save(c);

		// Get predictions from just-trained classifier.
		MatrixXd S(X.rows(),X.cols());
		trainme->getMarginals(X, S);
		VectorXd y_cur(X.rows());
		for ( int i=0; i<X.rows(); ++i )
			y_cur(i) = S(i,1);

		// Calculate epsilon.
		double e_num = 0.0;
		for ( int i=0; i<w.size(); ++i )
			if ( round(y_cur(i)) != y(i) )
				e_num += w(i);
		double epsilon = e_num / w.sum();
		printf("epsilon: %0.4f\n", epsilon);
		if ( epsilon > 0.5 ) {
			SVL_LOG(SVL_LOG_WARNING, "Broke epsilon: " << epsilon);
			break;
		}
		// Calculate boosting coefficient.
		gd = 0.5 * max( 0.0, log( ( 1.0 - epsilon ) / epsilon ) );
		gd = 0.5;
		printf("gd: %0.4f\n", gd);
		_coeffs.push_back(gd);
		_models.push_back(trainme);

		// Update and renormalize weights.
		if ( _method == 0 ) {
			// Real AdaBoost
			w = w.cwise() * ( ( ((y_cur.cwise() / ((y_cur*-1.0).cwise() + 1.0)).cwise().log() * 0.5 ).cwise() * ((2.0*y.cast<double>()).cwise() - 1.0) * -gd ).cwise().exp() );
		} else if ( _method == 1 ) {
			// GentleBoost
			w = w.cwise() * ( ( ((2.0*y_cur).cwise() - 1.0).cwise() * ((2.0*y.cast<double>()).cwise() - 1.0) * -gd ).cwise().exp() );
		}
		w /= w.sum();

		if ( _timer ) _timer->inc(); // Increment timer.
	}
	sprintf(c,"weights_%d.out",nRounds); svlBinaryWriteVectorXd(c, w);
	if ( _timer ) _timer->pop();

	// Normalize boosting coefficients.
	gd = 0.0;
	for ( vector<double>::const_iterator it=_coeffs.begin(); it!=_coeffs.end(); ++it )
		gd += *it;
	for ( vector<double>::iterator it=_coeffs.begin(); it!=_coeffs.end(); ++it )
		*it /= gd;

	return 0.0;
}

void svlNativeBoostedClassifier::getClassScores(const VectorXd &sample, VectorXd &outputScores) const
{
	outputScores = VectorXd::Zero(_nClasses);
	for ( int i=0; i<int(_models.size()); ++i ) {
		VectorXd t(_nClasses);
		_models[i]->getClassScores(sample, t);
		outputScores += t * _coeffs[i];
	}
}

void svlNativeBoostedClassifier::getMarginals(const VectorXd &sample, VectorXd &outputMarginals) const
{
#if 1
	outputMarginals = VectorXd::Zero(_nClasses);
	for ( int i=0; i<int(_models.size()); ++i ) {
		VectorXd t(_nClasses);
		_models[i]->getMarginals(sample, t);
		outputMarginals += t * _coeffs[i];
	}
#else
	outputMarginals = VectorXd::Zero(_nClasses);
	cout << "sample:\n" << sample << endl;
	for ( int i=0; i<int(_models.size()); ++i ) {
		VectorXd t(_nClasses);
		_models[i]->getMarginals(sample, t);
		cout << i << ": " << _coeffs[i] << endl << t << endl;
		outputMarginals += t * _coeffs[i];
	}
	exit(-1);
#endif
}

// Save the nodes' information to a file.
bool svlNativeBoostedClassifier::save(const char *filename) const 
{
	XMLNode root = XMLNode::createXMLTopNode("xml",true);
	save(root);
	root.getChildNode(0).writeToFile(filename);
	return true;
}

// Save the classifier as a child to a node.
bool svlNativeBoostedClassifier::save(XMLNode& node) const
{
	if ( !this->svlClassifier::save(node) )
		return false;
	
	// Get classifier node as last instantiated child.
	XMLNode me = node.getChildNode(node.nChildNode()-1);
	me.updateAttribute("NativeBoosted", NULL, "id");
	
	// Add root-level attributes.
	char c[32];
	sprintf(c,"%d",int(_models.size()));
	me.addAttribute("NumModels",c);

	// Instantiate child classifiers.
	XMLNode ch = me.addChild("Models");
	for ( int i=0; i<int(_models.size()); ++i ) {
		_models[i]->save(ch);
		// Add coefficient to last-added child.
		XMLNode last = ch.getChildNode(ch.nChildNode()-1);
		sprintf(c,"%0.6f",_coeffs[i]);
		last.addAttribute("Coefficient",c);
	}

	return true;
}


