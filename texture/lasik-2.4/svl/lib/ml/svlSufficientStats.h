/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlSufficientStats.h
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
** DESCRIPTION:
**  Implements a class for accumulating sufficient statistics (moments) and
**  conditional sufficient statistics:
**    count := \sum_i 1{y_i = k}
**    sum   := \sum_i 1{y_i = k} x_i
**    sum2  := \sum_i 1{y_i = k} x_i x_i^T
**
**  When not maintaining full pairwise statistics, only keeps a vector of
**  second-order statistics.
**
*****************************************************************************/

#pragma once

#include <vector>
#include <limits>

#include "Eigen/Core"

#include "svlBase.h"

using namespace std;
USING_PART_OF_NAMESPACE_EIGEN;

// svlSufficientStats class --------------------------------------------------

typedef enum _svlPairSuffStatsType {
    SVL_PSS_FULL,              // full pairwise sufficient statistics (default)
    SVL_PSS_DIAG,              // only diagonal pairwise terms
    SVL_PSS_NONE,              // no pairwise statistics
} svlPairSuffStatsType;

class svlSufficientStats {
 private:
    int _n;                   // size of feature vector
    svlPairSuffStatsType _pairStats;
    
    double _count;            // sum_i w_i
    VectorXd _sum;            // sum_i w_i x_i
    MatrixXd _sum2;           // sum_i w_i x_i x_i^T

 public:
    svlSufficientStats(int n = 1, svlPairSuffStatsType pairStats = SVL_PSS_FULL);
    svlSufficientStats(const svlSufficientStats& stats);
    ~svlSufficientStats();

    void clear();
    void clear(int n, svlPairSuffStatsType pairStats = SVL_PSS_FULL);
    void diagonalize();

    inline int size() const { return _n; }
    inline bool isDiagonal() const { return (_pairStats == SVL_PSS_DIAG); }
    inline bool hasPairs() const { return (_pairStats != SVL_PSS_NONE); }
    inline double count() const { return _count; }
    inline double sum(int i = 0) const { return _sum(i); }
    inline double sum2(int i = 0, int j = 0) const {
        switch (_pairStats) {
        case SVL_PSS_FULL: return _sum2(i, j);
        case SVL_PSS_DIAG: return (i == j) ? _sum2(i, 0) : 0.0;
        case SVL_PSS_NONE: return 0.0;
        }
        return 0.0;
    }

    inline const VectorXd& firstMoments() const { return _sum; }
    inline MatrixXd secondMoments() const { 
        switch (_pairStats) {
        case SVL_PSS_FULL: return _sum2;
        case SVL_PSS_DIAG: return _sum2.col(0).asDiagonal();
        case SVL_PSS_NONE: return MatrixXd::Zero(_n, _n);
        }
        return _sum2; 
    }

    // i/o
    void save(const char *filename, bool bBinary = false) const;
    void save(ostream& os, bool bBinary = false) const;
    void load(const char *filename, bool bBinary = false);
    void load(istream& is, bool bBinary = false);
    
    // modification
    void accumulate(const vector<double>& x);
    void accumulate(const vector<double>& x, double w);
    void accumulate(const vector<vector<double> >& x, double w = 1.0);
    void accumulate(const svlSufficientStats& stats, double w = 1.0);
    void subtract(const vector<double>& x);
    void subtract(const vector<double>& x, double w);
    void subtract(const vector<vector<double> >& x, double w = 1.0);
    void subtract(const svlSufficientStats& stats, double w = 1.0);

    // standard operators
    svlSufficientStats& operator=(const svlSufficientStats& stats);
};

// svlCondSufficientStats class ----------------------------------------------

class svlCondSufficientStats {
 private:
    int _n;                 // size of feature vector
    int _k;                 // number of states for conditioning

    vector<svlSufficientStats> _stats;

 public:
    svlCondSufficientStats(int n = 1, int k = 2);
    svlCondSufficientStats(const svlCondSufficientStats& condStats);
    ~svlCondSufficientStats();

    void clear();
    void clear(int n, int k);

    inline int size() const { return _n; }
    inline double count() const;
    inline double count(int k) const { return _stats[k].count(); }

    inline svlSufficientStats const& suffStats(int y) const {
        return _stats[y];
    }

    // i/o
    void save(const char *filename) const;
    void load(const char *filename);
    
    // modification
    void accumulate(const vector<double>& x, int y);
    void accumulate(const vector<vector<double> >& x, int y);
    void accumulate(const vector<vector<double> >& x, const vector<int>& y);
    void accumulate(const svlSufficientStats& stats, int y);
    void subtract(const vector<double>& x, int y);
    void subtract(const vector<vector<double> >& x, int y);
    void subtract(const vector<vector<double> >& x, const vector<int>& y);
    void subtract(const svlSufficientStats& stats, int y);
    void redistribute(int y, int k);
};

// Inline Functions ---------------------------------------------------------

inline double svlCondSufficientStats::count() const {
    double c = 0;
    for (int y = 0; y < _k; y++) {
        c += _stats[y].count();
    }

    return c;
}

