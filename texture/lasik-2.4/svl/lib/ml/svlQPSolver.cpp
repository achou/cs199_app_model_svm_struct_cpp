/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlQPSolver.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**
*****************************************************************************/

#include <cstdio>
#include <iostream>

#include "svlBase.h"
#include "svlQPSolver.h"

using namespace std;
USING_PART_OF_NAMESPACE_EIGEN;

// svlQPSolver --------------------------------------------------------------

double svlQPSolver::alpha = 0.25;
double svlQPSolver::beta = 0.5;

svlQPSolver::svlQPSolver() :
    _r(0.0)
{
    // do nothing
}

svlQPSolver::svlQPSolver(const MatrixXd& P, const VectorXd& q, double r)
{
    setObjective(P, q, r);
}

svlQPSolver::~svlQPSolver()
{
    // do nothing
}

// define problem
void svlQPSolver::setObjective(const MatrixXd& P, const VectorXd& q, double r)
{
    SVL_ASSERT((P.rows() == P.cols()) && (P.cols() == q.rows()));
#if 1
    // check that P is positive definite
    Eigen::LDLT<MatrixXd> cholesky(P);
    SVL_ASSERT_MSG(cholesky.isPositiveDefinite(), "P must be positive definite");
#endif

    _P = P;
    _q = q;
    _r = r;

    _x = VectorXd::Zero(_q.rows());

    _l = VectorXd::Constant(_x.rows(), -SVL_DBL_MAX);
    _u = VectorXd::Constant(_x.rows(), SVL_DBL_MAX);
}

void svlQPSolver::setConstraints(const MatrixXd& A, const VectorXd& b)
{
    SVL_ASSERT((A.rows() == b.rows()) && (A.cols() == _P.rows()));

    _A = A;
    _b = b;
}

void svlQPSolver::setBounds(const VectorXd& lb, const VectorXd& ub)
{
    SVL_ASSERT((lb.rows() == _P.rows()) && (ub.rows() == _P.rows()));

    _l = lb;
    _u = ub;
}

void svlQPSolver::clearConstraints()
{
    _A.resize(0, 0);
    _b.resize(0);
}

void svlQPSolver::clearBounds()
{
    _l = VectorXd::Constant(_x.rows(), -SVL_DBL_MAX);
    _u = VectorXd::Constant(_x.rows(), SVL_DBL_MAX);
}

double svlQPSolver::solve()
{
    // invoke specialized methods
    if (_b.rows() == 0) {
        return solveNoEquality();
    }

    if (_b.rows() == 1) {
        return solveSingleEquality();
    }

    if (((_A.cwise() == 1.0) + (_A.cwise() == 0.0)).all()) {
        return solveSimplex();
    }

    // general solvers
    bool bBounds = false;
    for (int i = 0; i < _x.size(); i++) {
        if ((_l[i] != -SVL_DBL_MAX) || (_u[i] != SVL_DBL_MAX)) {
            bBounds = true;
            break;
        }
    }

    return bBounds ? solveGeneral() : solveNoBounds();
}

double svlQPSolver::objective() const
{
    return objective(_x);
}

double svlQPSolver::objective(const VectorXd& x) const
{
    SVL_ASSERT(x.rows() == _P.rows());

    return (0.5 * (x.transpose() * _P * x)[0] + _q.dot(x) + _r);
}

// protected functions ------------------------------------------------------

// special case solvers
double svlQPSolver::solveNoEquality()
{
    SVL_FCN_TIC;
    SVL_ASSERT(_b.rows() == 0);

    _P.ldlt().solve(-_q, &_x);

    vector<bool> constrainedFlag(this->size(), false);

    while (1) {
        vector<int> constrainedVars;
        vector<int> freeVars;
        bool bAdditionalConstraints = false;
        for (int i = 0; i < this->size(); i++) {
            if (constrainedFlag[i]) {
                constrainedVars.push_back(i);
            } else if (_x[i] < _l[i]) {
                _x[i] = _l[i];
                constrainedVars.push_back(i);
                constrainedFlag[i] = true;
                bAdditionalConstraints = true;
            } else if (_x[i] > _u[i]) {
                _x[i] = _u[i];
                constrainedVars.push_back(i);
                constrainedFlag[i] = true;
                bAdditionalConstraints = true;
            } else {
                freeVars.push_back(i);
            }
        }

        // minimum found satisfying all constraints
        if (!bAdditionalConstraints) {
            break;
        }

        // resolve with _x constrained to box (i.e., 1/2 x1^T P11 x1 + (P12 x2 + q1^T) x1)
        MatrixXd freeVarP((int)freeVars.size(), (int)freeVars.size());
        MatrixXd crossVarP((int)freeVars.size(), (int)constrainedVars.size());
        VectorXd freeVarQ((int)freeVars.size());
        VectorXd constrainedX((int)constrainedVars.size());

        for (int i = 0; i < (int)freeVars.size(); i++) {
            freeVarQ[i] = _q[freeVars[i]];
            for (int j = 0; j < (int)freeVars.size(); j++) {
                freeVarP(i, j) = _P(freeVars[i], freeVars[j]);
            }

            for (int j = 0; j < (int)constrainedVars.size(); j++) {
                crossVarP(i, j) = _P(freeVars[i], constrainedVars[j]);
            }
        }

        for (int i = 0; i < (int)constrainedVars.size(); i++) {
            constrainedX[i] = _x[constrainedVars[i]];
        }

        freeVarQ = freeVarQ + crossVarP * constrainedX;
        VectorXd freeX(freeVars.size());

        freeVarP.ldlt().solve(-freeVarQ, &freeX);
        for (int i = 0; i < (int)freeVars.size(); i++) {
            _x[freeVars[i]] = freeX[i];
        }
    }

    SVL_FCN_TOC;
    return objective(_x);
}

double svlQPSolver::solveSingleEquality()
{
    SVL_FCN_TIC;
    SVL_ASSERT(_b.rows() == 1);
    // need _x to be feasible?

    while (1) {
        // compute gradient and objective
        VectorXd g = gradient(_x);
        SVL_LOG(SVL_LOG_VERBOSE, "... f = " << (0.5 * g.dot(_x) + _q.dot(_x)));

        // find most violated pair of variables
        int u = -1;
        int v = -1;
        double Fu = SVL_DBL_MAX;
        double Fv = -SVL_DBL_MAX;
        for (int i = 0; i < _x.rows(); i++) {
            double Fi = g[i] / _A[i];
            if ((_l[i] < _x[i]) && (_x[i] < _u[i])) { // set I_0
                if (Fu > Fi) { Fu = Fi; u = i; }
                if (Fv < Fi) { Fv = Fi; v = i; }
            } else if (((_A[i] > 0.0) && (_x[i] == _l[i])) ||
                ((_A[i] < 0.0) && (_x[i] == _u[i]))) { // set I_1 or I_2
                if (Fu > Fi) { Fu = Fi; u = i; }
            } else if (((_A[i] > 0.0) && (_x[i] == _u[i])) ||
                ((_A[i] < 0.0) && (_x[i] == _l[i]))) { // set I_3 or I_4
                if (Fv < Fi) { Fv = Fi; v = i; }
            }
        }

        // check KKT conditions
        if (Fv - Fu <= SVL_EPSILON) {
            SVL_LOG(SVL_LOG_VERBOSE, "relaxed KKT conditions satisfied");
            break;
        }

        // SMO update
        double tau_lb, tau_ub;
        if (_A[u] > 0.0) {
            tau_lb = _A[u] * (_l[u] - _x[u]);
            tau_ub = _A[u] * (_u[u] - _x[u]);
        } else {
            tau_ub = _A[u] * (_l[u] - _x[u]);
            tau_lb = _A[u] * (_u[u] - _x[u]);
        }

        if (_A[v] > 0.0) {
            tau_lb = std::max(tau_lb, _A[v] * (_x[v] - _u[v]));
            tau_ub = std::min(tau_ub, _A[v] * (_x[v] - _l[v]));
        } else {
            tau_lb = std::max(tau_lb, _A[v] * (_x[v] - _l[v]));
            tau_ub = std::min(tau_ub, _A[v] * (_x[v] - _u[v]));
        }

        double tau = (Fv - Fu) /
            (_P(u,u) / (_A[u] * _A[u]) + _P(v,v) / (_A[v] * _A[v]) - 2.0 * _P(u,v) / (_A[u] * _A[v]));
        tau = std::min(std::max(tau, tau_lb), tau_ub);

        _x[u] += tau / _A[u];
        _x[v] += tau / _A[v];
    }

    SVL_FCN_TOC;
    return objective(_x);
}

double svlQPSolver::solveSimplex()
{
    SVL_FCN_TIC;
    SVL_LOG(SVL_LOG_WARNING, "specialized simplex solver not implemented yet");
    SVL_FCN_TOC;
    return solveGeneral();
}

double svlQPSolver::solveNoBounds()
{
    SVL_FCN_TIC;

    // initialize primal and dual variables
    _x = VectorXd::Zero(_q.rows());
    VectorXd nu = VectorXd::Zero(_A.rows());

    VectorXd dx, dnu;
    
    double r = SVL_DBL_MAX;
    while (r > SVL_EPSILON) {
        solveKKTSystem(_P, _A, _P * _x + _q + _A.transpose() * nu, _A * _x - _b, dx, dnu);
        double t = lineSearchNoBounds(_x, dx, nu, dnu);
        _x = _x + t * dx;
        nu = nu + t * dnu;

        r = (_P * _x + _q + _A.transpose() * nu).norm() + (_A * _x - _b).norm();
        SVL_LOG(SVL_LOG_DEBUG, "..." << r << " (" << t << ")");
    }

    SVL_FCN_TOC;
    return objective(_x);
}

double svlQPSolver::solveGeneral()
{
    SVL_FCN_TIC;

    // initialize primal and dual variables
    _x = 0.5 * (_l + _u);
    VectorXd nu = VectorXd::Zero(_A.rows());

    SVL_ASSERT_MSG(false, "not implemented yet");

    SVL_FCN_TOC;
    return objective(_x);
}

// line search
double svlQPSolver::lineSearchNoBounds(const VectorXd& x, const VectorXd& dx,
    const VectorXd& nu, const VectorXd& dnu) const
{
    SVL_FCN_TIC;
    SVL_ASSERT((alpha > 0.0) && (alpha < 0.5));
    SVL_ASSERT((beta > 0.0) && (beta < 1.0));

    double t = 1.0;
    double r = (_P * x + _q + _A.transpose() * nu).norm() + (_A * x - _b).norm();

    while (t > SVL_DBL_MIN) {
        double r2 = ( _P * (x + t * dx) + _q + _A.transpose() * (nu + t * dnu)).norm() +
            (_A * (x + t * dx) - _b).norm();

        if (r2 <= (1.0 * alpha * t) * r)
            break;

        t *= beta;
    }

    SVL_FCN_TOC;
    return t;
}

double svlQPSolver::lineSearchGeneral(const VectorXd& x, const VectorXd& dx,
    const VectorXd& nu, const VectorXd& dnu) const
{
    SVL_LOG(SVL_LOG_FATAL, "not implemented yet");
    return 0.0;
}

/*
** solve the system of equations:
**    | H_x  A^T | | x | = - | c |
**    |  A   H_y | | y |     | b |
*/
void svlQPSolver::solveKKTSystem(const MatrixXd& Hx, const MatrixXd& Hy,
    const MatrixXd& A, const VectorXd& c, const VectorXd& b,
    VectorXd& x, VectorXd& y) const
{
    // check input
    SVL_ASSERT(Hx.rows() == Hx.cols());
    SVL_ASSERT(Hy.rows() == Hy.cols());
    SVL_ASSERT((A.rows() == Hy.rows()) && (A.cols() == Hx.cols()));
    SVL_ASSERT((Hx.rows() == c.rows()) && (A.rows() == b.rows()));

    // eliminate x = -H_x^{-1} (A^T y + c)
    Eigen::LDLT<MatrixXd> ldl(Hx);
    MatrixXd invHxAt(Hx.rows(), A.rows());
    VectorXd invHxc(Hx.rows());

    for (int i = 0; i < A.rows(); i++) {
        VectorXd tmp;
        ldl.solve(A.row(i).transpose(), &tmp);
        invHxAt.col(i) = tmp;
    }
    ldl.solve(c, &invHxc);

    // solve for y
    MatrixXd G = Hy - A * invHxAt;
    G.ldlt().solve(A * invHxc - b, &y);

    // solve for x
    x = -invHxAt * y - invHxc;
}

/*
** solve the system of equations:
**    | H_x  A^T | | x | = - | c |
**    |  A   0   | | y |     | b |
*/
void svlQPSolver::solveKKTSystem(const MatrixXd& Hx,
    const MatrixXd& A, const VectorXd& c, const VectorXd& b,
    VectorXd& x, VectorXd& y) const
{
    // check input
    SVL_ASSERT((Hx.rows() == Hx.cols()) && (A.cols() == Hx.cols()));
    SVL_ASSERT((Hx.rows() == c.rows()) && (A.rows() == b.rows()));

    // eliminate x = -H_x^{-1} (A^T y + c)
    Eigen::LDLT<MatrixXd> ldl(Hx);
    MatrixXd invHxAt(Hx.rows(), A.rows());
    VectorXd invHxc(Hx.rows());

    for (int i = 0; i < A.rows(); i++) {
        VectorXd tmp;
        ldl.solve(A.row(i).transpose(), &tmp);
        invHxAt.col(i) = tmp;
    }
    ldl.solve(c, &invHxc);

    // solve for y
    MatrixXd G = A * invHxAt;
    G.ldlt().solve(b - A * invHxc, &y);

    // solve for x
    x = -invHxAt * y - invHxc;
}
