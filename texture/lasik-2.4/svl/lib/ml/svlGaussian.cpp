/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlGaussian.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**
** TO DO:
**  1. cache temporary variable z
**
*****************************************************************************/

#include "../base/svlCompatibility.h"

#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <limits>
#include <cmath>

// SVL libraries
#include "svlBase.h"
#include "svlGaussian.h"
#include "svlBoxMuller.h"

using namespace std;

// svlGaussian --------------------------------------------------------------

bool svlGaussian::AUTO_RIDGE = false;

svlGaussian::svlGaussian(int n) :
    _n(n), _invSigma(NULL), _logZ(0.0), _L(NULL)
{
    initialize(n);
}

svlGaussian::svlGaussian(const VectorXd& mu, double sigma2) :
    _invSigma(NULL), _logZ(0.0), _L(NULL)
{
    initialize(mu, sigma2);
}

svlGaussian::svlGaussian(const VectorXd& mu, const MatrixXd& sigma2) :
    _invSigma(NULL), _logZ(0.0), _L(NULL)
{
    initialize(mu, sigma2);
}

svlGaussian::svlGaussian(const vector<double>& mu, double sigma2) :
    _invSigma(NULL), _logZ(0.0), _L(NULL)
{
    initialize(Eigen::Map<VectorXd>(&mu[0], mu.size()), sigma2);
}

svlGaussian::svlGaussian(const svlSufficientStats& stats) :
    _n(stats.size()), _invSigma(NULL), _logZ(0.0), _L(NULL)
{
    initialize(_n);
    train(stats, SVL_EPSILON);
}

svlGaussian::svlGaussian(const svlGaussian& model) :
    _n(model._n), _mu(model._mu), _Sigma(model._Sigma), _invSigma(NULL),
    _logZ(model._logZ), _L(NULL)
{
    if (model._invSigma != NULL) {
        _invSigma = new MatrixXd(*model._invSigma);
    }

    if (model._L != NULL) {
        _L = new MatrixXd(*model._L);
    }
}

svlGaussian::~svlGaussian()
{
    freeStorage();
}

// initialization
void svlGaussian::initialize(int n)
{
    SVL_ASSERT(n > 0);

    _n = n;
    freeStorage();
    _mu = VectorXd::Zero(n);
    _Sigma = MatrixXd::Identity(n, n);
    _invSigma = new MatrixXd(MatrixXd::Identity(n, n));
    _logZ = -0.5f * log(2.0 * M_PI);
}

void svlGaussian::initialize(const VectorXd& mu, double sigma2)
{
    SVL_ASSERT(sigma2 > 0.0);

    _n = mu.rows();

    freeStorage();
    _mu = mu;
    _Sigma = sigma2 * MatrixXd::Identity(_n, _n);

    _invSigma = new MatrixXd(MatrixXd::Identity(_n, _n) / sigma2);
    _logZ = -0.5 * (double)_n * log(2.0 * M_PI * sigma2);

    SVL_ASSERT(!isnan(_logZ));
    SVL_ASSERT(!isinf(_logZ));
}

void svlGaussian::initialize(const VectorXd &mu, const MatrixXd &sigma2)
{
    _n = mu.rows();
    SVL_ASSERT((sigma2.rows() == _n) && (sigma2.cols() == _n));

    freeStorage();
    _mu = mu;
    _Sigma = sigma2;
    updateCachedParameters();
}

// marginalizing
svlGaussian *svlGaussian::marginalize(const vector<int>& indx) const
{
    SVL_ASSERT(!indx.empty());

    svlGaussian *g = new svlGaussian(indx.size());

    for (unsigned i = 0; i < indx.size(); i++) {
        g->_mu(i) = _mu(indx[i]);
        for (unsigned j = 0; j < indx.size(); j++) {
            g->_Sigma(i, j) = _Sigma(indx[i], indx[j]);
	}
    }

    g->freeStorage();
    return g;
}

// conditioning
svlGaussian svlGaussian::reduce(const vector<double>& x2,
    const vector<int>& indx2) const
{
    // TODO: cache g._Sigma and sigmaProduct so that repeated calls
    // with the same indx2 doesn't require additional computation

    SVL_ASSERT((x2.size() == indx2.size()) && (x2.size() < (unsigned)_n));

    int n1 = _n - (int)x2.size();
    int n2 = (int)x2.size();
    vector<int> indxAll(_n);
    for (int i = 0; i < _n; i++) {
        indxAll[i] = i;
    }
    vector<int> indx1;
    set_difference(indxAll.begin(), indxAll.end(),
        indx2.begin(), indx2.end(),
        insert_iterator<vector<int> >(indx1, indx1.end()));

    svlGaussian g(n1);

    VectorXd mu2(n2);
    MatrixXd Sigma22(n2, n2);
    MatrixXd Sigma21(n2, n1);

    for (int i = 0; i < n2; i++) {
        SVL_ASSERT((indx2[i] >= 0) && (indx2[i] < _n));
        mu2(i) = x2[i] - _mu(indx2[i]);
        for (int j = 0; j < n2; j++) {
            Sigma22(i, j) = _Sigma(indx2[i], indx2[j]);
        }
        for (int j = 0; j < n1; j++) {
            Sigma21(i, j) = _Sigma(indx2[i], indx1[j]);
        }
    }

    MatrixXd sigmaProduct(n2, n1);
    Eigen::LDLT<MatrixXd> cholesky(Sigma22);
    for (int i = 0; i < n1; i++) {
        cholesky.solve(Sigma21.col(i), &sigmaProduct.col(i));
    }

    g._Sigma = -1.0 * Sigma21.transpose() * sigmaProduct;
    g._mu = sigmaProduct.transpose() * mu2;

    for (int i = 0; i < n1; i++) {
        g._mu(i) += _mu(indx1[i]);
        for (int j = 0; j < n1; j++) {
            g._Sigma(i, j) += _Sigma(indx1[i], indx1[j]);
        }
    }

    g.updateCachedParameters();
    return g;
}

svlGaussian svlGaussian::reduce(const map<int, double>& x) const
{
    SVL_ASSERT_MSG(false, "TODO");
    return svlGaussian();
}

svlConditionalGaussian svlGaussian::conditionOn(const vector<int>& indx2) const
{
    SVL_ASSERT(indx2.size() < (unsigned)_n);

    int n1 = _n - (int)indx2.size();
    int n2 = (int)indx2.size();
    vector<int> indxAll(_n);
    for (int i = 0; i < _n; i++) {
        indxAll[i] = i;
    }
    vector<int> indx1;
    set_difference(indxAll.begin(), indxAll.end(),
        indx2.begin(), indx2.end(),
        insert_iterator<vector<int> >(indx1, indx1.end()));

    VectorXd mu2(n2);
    MatrixXd Sigma22(n2, n2);
    MatrixXd Sigma21(n2, n1);

    for (int i = 0; i < n2; i++) {
        SVL_ASSERT((indx2[i] >= 0) && (indx2[i] < _n));
        mu2(i) = _mu(indx2[i]);
        for (int j = 0; j < n2; j++) {
            Sigma22(i, j) = _Sigma(indx2[i], indx2[j]);
        }
        for (int j = 0; j < n1; j++) {
            Sigma21(i, j) = _Sigma(indx2[i], indx1[j]);
        }
    }

    MatrixXd sigmaProduct(n2, n1);
    Eigen::LDLT<MatrixXd> cholesky(Sigma22);
    for (int i = 0; i < n1; i++) {
        cholesky.solve(Sigma21.col(i), &sigmaProduct.col(i));
    }

    VectorXd mu = -1.0 * sigmaProduct.transpose() * mu2;
    MatrixXd Sigma = -1.0 * Sigma21.transpose() * sigmaProduct;
    MatrixXd sigmaGain = sigmaProduct.transpose();

    return svlConditionalGaussian(mu, Sigma, sigmaGain);
}

// i/o
bool svlGaussian::save(const char *filename, bool bBinary) const
{
    ofstream ofs;

    ofs.open(filename, bBinary ? ios::out | ios::binary : ios::out);

    if (ofs.fail()) {
        SVL_LOG(SVL_LOG_WARNING, "Failed to open " << filename << " for writing");
        return false;
    }

    bool result = save(ofs, bBinary);
    if (!result) {
        SVL_LOG(SVL_LOG_WARNING, "...while writing to " << filename);
    }

    ofs.close();
    return result;
}

bool svlGaussian::save(ofstream & ofs, bool bBinary) const
{
    if (bBinary) {
        //todo-- make this faster
        //should be able to just write all the data for each matrix in one
        //big chunk, but first need to research whether OpenCV pads matrices
        //or not, etc.
        ofs.write((char *) &_n, sizeof(_n));
        for (int i = 0; i < _n; i++) {
            double temp = _mu(i);
            ofs.write((char *)&temp, sizeof(temp));
	}
        for (int i = 0; i < _n; i++) {
            for (int j = 0; j < _n; j++) {
                double temp = _Sigma(i, j);
                ofs.write( (char *) & temp, sizeof(temp));
	    }
	}

    } else {

        ofs << _n << "\n\n";
        for (int i = 0; i < _n; i++) {
            if (i > 0) ofs << " ";
            ofs << setprecision(16) << _mu(i);
        }
        ofs << "\n\n";

        for (int i = 0; i < _n; i++) {
            for (int j = 0; j < _n; j++) {
                if (j > 0) ofs << " ";
                ofs << setprecision(16) << _Sigma(i, j);
            }
            ofs << "\n";
        }
    }

    if (ofs.fail()) {
        SVL_LOG(SVL_LOG_WARNING, "Failed to save svlGaussian to file stream");
    }

    return !ofs.fail();
}

bool svlGaussian::load(const char *filename, bool bBinary)
{
    ifstream ifs;
    ifs.open(filename, bBinary ? ios::in | ios::binary : ios::in);

    if (ifs.fail()) {
        SVL_LOG(SVL_LOG_WARNING, "Could not open " << filename);
        return false;
    }

    bool result = load(ifs, bBinary);
    if (!result) {
        SVL_LOG(SVL_LOG_WARNING, "...while reading from " << filename);
    }

    ifs.close();
    return result;
}

bool svlGaussian::load(ifstream &ifs, bool bBinary)
{
    if (bBinary) {
        ifs.read((char *)&_n, sizeof(_n));

        if (ifs.fail()) {
            SVL_LOG(SVL_LOG_WARNING, "Failed to load svlGaussian from file stream");
            return false;
	}

        initialize(_n);

        //todo-- speed this up by reading memory in entire block. first research
        //opencv spec to make sure cvmats are not zero-padded or anything like that
        for (int i = 0; i < _n; i++) {
            double temp;
            ifs.read((char *)&temp, sizeof(temp));
            _mu(i) = temp;
	}

        for (int i = 0; i < _n; i++) {
            for (int j = 0; j < _n; j++) {
                double temp;
                ifs.read((char *)&temp, sizeof(temp));
                _Sigma(i, j) = temp;
	    }
	}

    } else {

        ifs >> _n;
        if (ifs.fail()) {
            SVL_LOG(SVL_LOG_WARNING, "Failed to load svlGaussian from file stream");
            return false;
        }

        initialize(_n);

        for (int i = 0; i < _n; i++) {
            ifs >> _mu(i);
        }

        for (int i = 0; i < _n; i++) {
            for (int j = 0; j < _n; j++) {
                ifs >> _Sigma(i, j);
            }
        }
    }

    if (ifs.fail()) {
        SVL_LOG(SVL_LOG_ERROR, "Failed to load svlGaussian from file stream. Gaussian is now corrupted");
    }
    updateCachedParameters();

    return !ifs.fail();
}

// evaluate (log-likelihood)
void svlGaussian::evaluate(const MatrixXd& x, VectorXd& p) const
{
    guaranteeInvSigma();

    SVL_ASSERT((x.cols() == _n) && (x.rows() == p.rows()));

    for (int i = 0; i < x.rows(); i++) {
        VectorXd z = x.row(i).transpose() - _mu;
        p(i) = -0.5 * (z.transpose() * (*_invSigma) * z)(0) + _logZ;
    }
}

void svlGaussian::evaluate(const vector<vector<double> >& x, vector<double>& p) const
{
    guaranteeInvSigma();
    SVL_ASSERT(x.size() == p.size());

    for (unsigned i = 0; i < x.size(); i++) {
        SVL_ASSERT(x[i].size() == (unsigned)_n);
        VectorXd z = Eigen::Map<VectorXd>(&x[i][0], _n) - _mu;
        p[i] = -0.5 * (z.transpose() * (*_invSigma) * z)(0) + _logZ;
    }
}

double svlGaussian::evaluateSingle(const VectorXd& x) const
{
    guaranteeInvSigma();
    SVL_ASSERT(x.rows() == _n);

    VectorXd z = x - _mu;
    return -0.5 * (z.transpose() * (*_invSigma) * z)(0) + _logZ;
}

double svlGaussian::evaluateSingle(const vector<double>& x) const
{
    guaranteeInvSigma();
    SVL_ASSERT(x.size() == (unsigned)_n);

    VectorXd z = Eigen::Map<VectorXd>(&x[0], _n) - _mu;
    return -0.5 * (z.transpose() * (*_invSigma) * z)(0) + _logZ;
}

double svlGaussian::evaluateSingle(double x) const
{
    SVL_ASSERT(_n == 1);
    guaranteeInvSigma();

    double z = (x - _mu(0));
    return -0.5 * (*_invSigma)(0) * z * z + _logZ;
}


// sampling
VectorXd svlGaussian::sample() const
{
    VectorXd x(_n);
    this->sample(x);
    return x;
}

void svlGaussian::sample(VectorXd &x) const
{
    SVL_ASSERT(x.rows() == _n);

    if (_L == NULL) {
        _L = new MatrixXd(_Sigma.llt().matrixL());
    }

    svlBoxMullerSample(x);
    x = (*_L) * x + _mu;
}

// learn parameters
void svlGaussian::train(const MatrixXd& x, double lambda)
{
    SVL_ASSERT((x.cols() == _n) && ((x.rows() > 1) || (lambda > 0.0)));

    // accumulate statistics
    _mu = x.colwise().sum().transpose() / (double)x.rows();
    _Sigma = (x.transpose() * x) / (double)x.rows() -
        _mu.transpose() * _mu + lambda * MatrixXd::Identity(_n, _n);

    updateCachedParameters();
}

void svlGaussian::train(const vector<vector<double> >& x, double lambda)
{
    SVL_ASSERT((x.size() > 1) || (lambda > 0.0));

    _mu.setZero();
    _Sigma.setZero();

    // accumulate statistics
    for (int i = 0; i < (int)x.size(); i++) {
        SVL_ASSERT(x[i].size() == (unsigned)_n);
        for (int j = 0; j < _n; j++) {
            _mu(j) += x[i][j];
            for (int k = 0; k <= j; k++) {
                _Sigma(j, k) += x[i][j] * x[i][k];
            }
        }
    }

    _mu /= (double)x.size();

    // normalize
    for (int j = 0; j < _n; j++) {
        for (int k = 0; k <= j; k++) {
            _Sigma(j, k) = _Sigma(j, k) / (double)x.size() - _mu(j) * _mu(k);
        }
        _Sigma(j, j) += lambda;
    }

    // copy symmetric part
    for (int j = 0; j < _n; j++) {
        for (int k = j + 1; k < _n; k++) {
            _Sigma(j, k) = _Sigma(k, j);
        }
    }

    updateCachedParameters();
}

// univariate training
void svlGaussian::train(const vector<double> &x, double lambda)
{
    SVL_ASSERT((_n == 1) && (x.size() > 1));

    _mu.setZero();
    _Sigma.setZero();

    // accumulate statistics
    for (int i = 0; i < (int)x.size(); i++) {
        _mu(0) += x[i];
        _Sigma(0, 0) += x[i] * x[i];
    }

    // normalize
    _mu /= (double)x.size();
    _Sigma = (_Sigma / (double)x.size() - _mu * _mu.transpose()).cwise() + lambda;

    updateCachedParameters();
}

void svlGaussian::train(const svlSufficientStats& stats, double lambda)
{
    SVL_ASSERT_MSG(stats.size() == _n, stats.size() << " != " << _n);

    // normalize sufficient statistics
    _mu = stats.firstMoments() / stats.count();
    if (stats.isDiagonal()) {
        _Sigma = (stats.secondMoments() / stats.count() - _mu.cwise().square().asDiagonal()) + 
            lambda * MatrixXd::Identity(_n, _n);
    } else {
        _Sigma = stats.secondMoments() / stats.count() -
            _mu * _mu.transpose() + lambda * MatrixXd::Identity(_n, _n);
    }

    updateCachedParameters();
}

double svlGaussian::logPartitionFunction() const
{
    guaranteeInvSigma();
    return _logZ;
}

// kl-divergence, KL(*this || model)
// TODO: test
double svlGaussian::klDivergence(const svlGaussian& model) const
{
    SVL_ASSERT(model._n == _n);
    model.guaranteeInvSigma();

    VectorXd z = model._mu - _mu;
    double s = (z.transpose() * (*_invSigma) * z)(0);
    double d = _logZ - model._logZ +
        0.5 * ((model._invSigma->cwise() * _Sigma).sum() + s - (double)_n);

    return d;
}

double svlGaussian::klDivergence(const svlSufficientStats& stats) const
{
    SVL_ASSERT(stats.size() == _n);
    return klDivergence(svlGaussian(stats));
}

// standard operators
svlGaussian& svlGaussian::operator=(const svlGaussian& model)
{
    if (this != &model) {
        freeStorage();
        _n = model._n;
        _mu = model._mu;
        _Sigma = model._Sigma;
        if (model._invSigma != NULL) {
            _invSigma = new MatrixXd(*model._invSigma);
        }
        _logZ = model._logZ;
        if (model._L != NULL) {
            _L = new MatrixXd(*model._L);
        }
    }

    return *this;
}

// protected functions
void svlGaussian::freeStorage()
{
    if (_invSigma != NULL) delete _invSigma;
    _invSigma = NULL;

    if (_L != NULL) delete _L;
    _L = NULL;
}

void svlGaussian::updateCachedParameters()
{
    if (_L != NULL) {
        delete _L;
        _L = NULL;
    }

    if (_invSigma != NULL) {
        delete _invSigma;
        _invSigma = NULL;
    }
}

inline void svlGaussian::guaranteeInvSigma() const
{
    if (_invSigma == NULL) {
        _invSigma = new MatrixXd(_n, _n);

        double det = _Sigma.determinant();
        if (!isfinite(det) || (det <= 0.0) && (svlLogger::getLogLevel() >= SVL_LOG_DEBUG)) {
            SVL_LOG(SVL_LOG_DEBUG, "svlGaussian::_Sigma =");
            cerr << _Sigma << "\n";
        }

        if ((det <= _n * SVL_DBL_MIN) && (svlGaussian::AUTO_RIDGE)) {
            MatrixXd Sigma(_Sigma);
            while (det <= _n * SVL_DBL_MIN) {
                SVL_LOG(SVL_LOG_WARNING, "using auto-ridge regression for |Sigma| = " << det);
                double delta = 1.01 * exp(log(_n * SVL_DBL_MIN - det) / (double)_n + SVL_DBL_MIN);
                for (int i = 0; i < _n; i++) {
                    Sigma(i, i) += delta;
                    if (Sigma(i, i) < SVL_DBL_MIN) {
                        Sigma(i, i) = SVL_DBL_MIN;
                    }
                }
                det = Sigma.determinant();
            }
            *_invSigma = Sigma.inverse();
        } else {
            *_invSigma = _Sigma.inverse();
        }

        SVL_ASSERT_MSG(isfinite(det) && (det > 0.0), "|Sigma| = " << det);
        _logZ = -0.5 * (float)_n * log(2.0 * M_PI) - 0.5 * log(det);
    }
}

// svlConditionalGaussian ---------------------------------------------------------

svlConditionalGaussian::svlConditionalGaussian(const VectorXd& mu, const MatrixXd &Sigma,
    const MatrixXd& SigmaGain) :
    _mu(mu), _Sigma(Sigma), _SigmaGain(SigmaGain)
{
    _n = SigmaGain.rows();
    _m = SigmaGain.cols();
    SVL_ASSERT(mu.rows() == _n);
    SVL_ASSERT((Sigma.rows() == _n) && (Sigma.cols() == _n));
}

svlConditionalGaussian::svlConditionalGaussian(const svlConditionalGaussian& model) :
    _n(model._n), _m(model._m), _mu(model._mu), _Sigma(model._Sigma), _SigmaGain(model._SigmaGain)
{
    // do nothing
}

svlConditionalGaussian::~svlConditionalGaussian()
{
    // do nothing
}

// i/o
bool svlConditionalGaussian::save(const char *filename) const
{
    ofstream ofs(filename);
    if (ofs.fail()) {
	return false;
    }

    ofs << _n << " " << _m << "\n\n";

    // mu
    ofs << _mu;
    ofs << "\n\n";

    // Sigma
    ofs << _Sigma;
    ofs << "\n\n";

    // SigmaGain
    ofs << _SigmaGain;

    ofs.close();
    return true;
}

bool svlConditionalGaussian::load(const char *filename)
{
    ifstream ifs(filename);
    if (ifs.fail()) {
	return false;
    }

    ifs >> _n >> _m;

    _mu = VectorXd(_n);
    _Sigma = MatrixXd(_n, _n);
    _SigmaGain = MatrixXd(_n, _m);

    for (int i = 0; i < _n; i++) {
        ifs >> _mu(i);
    }
    SVL_ASSERT(!ifs.fail());

    for (int i = 0; i < _n; i++) {
        for (int j = 0; j < _n; j++) {
            ifs >> _Sigma(i, j);
        }
    }
    SVL_ASSERT(!ifs.fail());

    for (int i = 0; i < _n; i++) {
        for (int j = 0; j < _m; j++) {
            ifs >> _SigmaGain(i, j);
        }
    }
    SVL_ASSERT(!ifs.fail());
    ifs.close();

    return true;
}

svlGaussian svlConditionalGaussian::reduce(const VectorXd& x)
{
    SVL_ASSERT(x.rows() == _m);
    return svlGaussian(_SigmaGain * x + _mu, _Sigma);
}

svlGaussian svlConditionalGaussian::reduce(const vector<double>& x)
{
    return reduce(Eigen::Map<VectorXd>(&x[0], x.size()));
}
