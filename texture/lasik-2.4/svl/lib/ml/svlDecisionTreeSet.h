/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlDecisionTreeSet.h
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  A set of weighted decision trees.
**
*****************************************************************************/

#pragma once

#include <iostream>
#include <vector>

#include "Eigen/Core"

#include "svlBase.h"
#include "svlClassifier.h"
#include "svlBinaryClassifier.h"
#include "svlDecisionTree.h"


// *** svlDecisionTreeSet class ***

class svlDecisionTreeSet : public svlBinaryClassifier {
protected:
    int _nTrees;
    double _minVal, _maxVal;
    vector<svlDecisionTree*> _trees;
    vector<double> _weights;

public:
    svlDecisionTreeSet() {}
    virtual ~svlDecisionTreeSet();
    
    double getScore(const VectorXd &sample) const {
        double ret = 0.0;
        for ( int i=0; i<_nTrees; ++i )
            ret += _weights[i] * _trees[i]->getScore(sample);
        return ret;
    }
    
    // I/O
    bool save(const char* filename) const {
        SVL_LOG(SVL_LOG_FATAL, "This save not implemented in svlDecisionTreeSet");
        return false;
    }
    bool save(XMLNode &node) const;
    bool load(XMLNode &node);
    
    // Training.
    double train(const MatrixXd &samples, const VectorXi &labels) {
        SVL_LOG(SVL_LOG_FATAL, "This classifier does not train");
        return -0.0;
    }
};


