/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlLinearRegressor.cpp
** AUTHOR(S):   Beyang Liu <beyangl@cs.stanford.edu>
**              Stephen Gould <sgould@stanford.edu>
**
*****************************************************************************/

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <limits>

#include "svlBase.h"
#include "svlLinearRegressor.h"

using namespace std;

svlLinearRegressor::svlLinearRegressor(bool bHuber, double tau) :
    svlOptimizer(), _features(NULL), _labels(NULL), _useHuber(bHuber), _tau(tau)
{
    // do nothing
}

svlLinearRegressor::~svlLinearRegressor()
{
    // do nothing
}

void svlLinearRegressor::initialize(unsigned n, vector<vector<double> > &features,
    vector<double> &labels, vector<double> &weights, const double *x)
{
    _features = &features;
    _labels = &labels;
    _weights = &weights;
    svlOptimizer::initialize(n, x);
}

double svlLinearRegressor::objectiveAndGradient(const double *x, double *df)
{
    double obj = 0.0;
    vector<double> predictedLabels(_features->size(), 0.0);
    predictLabels(predictedLabels, *(_features), x);

    memset((void *)df, (int)0.0, _n * sizeof(double));

    if (_useHuber) {
        // huber penalty
        double dh;
        for (unsigned i = 0; i < _features->size(); i++) {
            double u = predictedLabels[i] - (*_labels)[i];
            obj += (*_weights)[i] * huberFunctionAndDerivative(u, &dh, _tau);
            dh *= (*_weights)[i];
            for (unsigned j = 0; j < _n; j++) {
                df[j] += dh * (*_features)[i][j];
            }
        }
    } else {
        // L2 penalty
        for (unsigned i = 0; i < _features->size(); i++) {
            double dist =  predictedLabels[i] - (*_labels)[i];
            double wdist =  dist * (*_weights)[i];
            obj += dist * wdist;
            for (unsigned j = 0; j < _n; j++) {
                df[j] += wdist * (*_features)[i][j];
            }
        }

        obj *= 0.5;
    }

    return obj;
}

double svlLinearRegressor::objective(const double *x) {
    double *df = new double[_n];
    double obj = objectiveAndGradient(x, df);
    delete[] df;
    return obj;
}

void svlLinearRegressor::gradient(const double *x, double *df) {
    objectiveAndGradient(x, df);
}

void svlLinearRegressor::predictLabels(vector<double> &labels,
    vector<vector<double> > &features, const double *x) const
{
    labels.resize(features.size());
    if (x == NULL) x = _x;

    for (unsigned i = 0; i < features.size(); i++) {
	labels[i] = 0.0;
	for (unsigned j = 0; j < _n; j++) {
	    labels[i] += features[i][j] * x[j];
	}
    }
}

void svlLinearRegressor::predictLabels(vector<double> &labels,
    vector<vector<double> > &features, vector<double> &x) const
{
    labels.resize(features.size());

    for (unsigned i = 0; i < features.size(); i++) {
	labels[i] = 0.0;
	for (unsigned j = 0; j < x.size(); j++) {
	    labels[i] += features[i][j] * x[j];
	}
    }
}

void svlLinearRegressor::getSolution(vector<double> &soln) const {
    soln.clear();
    for (unsigned j = 0; j < _n; j++) {
	soln.push_back(_x[j]);
    }
}
