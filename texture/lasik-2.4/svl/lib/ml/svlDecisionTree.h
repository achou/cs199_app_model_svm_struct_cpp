/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlDecisionTree.h
** AUTHOR(S):   Adam Coates <acoates@stanford.edu>
**	            Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  A decision tree.
**
*****************************************************************************/

#pragma once

#include <iostream>
#include <list>

#include "Eigen/Core"

#include "svlBase.h"
#include "svlClassifier.h"
#include "svlBinaryClassifier.h"

// *** Struct definition of a simple histogram for use with the decision tree nodes. ***
struct svlDTNhistogram {
    MatrixXi Hpos, Hneg;
    VectorXd Wpos, Wneg;
    int num_samples;
    
    svlDTNhistogram(int n_dims, int num_bins) : num_samples(0) {
        Hpos = MatrixXi::Zero(n_dims, num_bins);
        Hneg = MatrixXi::Zero(n_dims, num_bins);
        Wpos = VectorXd::Zero(n_dims);
        Wneg = VectorXd::Zero(n_dims);
    }
    ~svlDTNhistogram() {}
};

// *** svlDecisionTreeNode class ***

class svlDecisionTree; // Forward declaration.

class svlDecisionTreeNode {
public:
    const svlDecisionTree *_parent;	// Contains binning information.
    
    int _num_samples;
    double _pos_weight, _neg_weight, _gini_index;
    // Split information
    int _split_var;
    double _split_val;
    // Probability of positive label for left and right decision space.
    double _Lp, _Rp;
    // Subtrees.
    svlDecisionTreeNode *_L, *_R;
    
 public:
    svlDecisionTreeNode(const svlDecisionTree *parent);
    ~svlDecisionTreeNode();
    
    // Training functions.
    void growTree();
    bool train(const MatrixXd &X, const VectorXi &y, const VectorXd *w = NULL, svlBitArray *map = NULL);
    double classify(const VectorXd &sample) const;
    void prune(int minSamples, double minPos, double minNeg);
    
    // Gini calculation and setting. Returns true on success; false if no valid Gini index was found.
    bool setGiniIndex(const svlDTNhistogram &H);
    double giniIndex(const svlDTNhistogram &H, int i, double &bestSplit, double &leftProb, double &rightProb) const;
    
    // I/O
    bool load(XMLNode node);
    bool save(XMLNode &root) const;
};


// *** svlDecisionTree class ***

class svlDecisionTree : public svlBinaryClassifier {
 protected:
	list<svlDecisionTreeNode*> _nodes;

 public:
    int _numBins;
    double _minVal, _maxVal;

 public:
    // default "untrained" tree.
    svlDecisionTree(double min_val = -1.0, double max_val = 1.0, int num_bins = 100);
    virtual ~svlDecisionTree();
    
    double getScore(const VectorXd &sample) const;
    
    // Accessors.
    inline svlDecisionTreeNode* getRoot() const {
        return _nodes.front();
    }
    
    // Training routines.
    void growTree(void);
    
    // I/O
    bool save(const char* filename) const {
        SVL_LOG(SVL_LOG_FATAL, "This save not implemented in svlDecisionTree");
        return false;
    }
    bool save(XMLNode &node) const;
    bool load(XMLNode &node);
    
    // Training.
    double train(const MatrixXd &X, const VectorXi &y, int depth);
    double train(const MatrixXd &samples, const VectorXi &labels) {
        return train(samples, labels, 1); // Use default depth.
    }
    
    // Histogramming functions.
    inline int binNum(double x) const {
        return x <= _minVal ? 0 : x >= _maxVal ? _numBins-1 : int( (x - _minVal)/(_maxVal - _minVal)*double(_numBins) );
    }
    inline double binMax(int i) const {
        return _minVal + (_maxVal - _minVal)/double(_numBins)*double(i);
    }
};


