/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlLogistic.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
** DESCRIPTION:
**  See svlLogistic.h
**
*****************************************************************************/

#include <cassert>
#include <iostream>
#include <fstream>
#include <limits>

// SVL libraries
#include "svlBase.h"
#include "svlLogistic.h"
#include "svlMLUtils.h"

using namespace std;

// svlLogistic class ---------------------------------------------------------

int svlLogistic::defaultMaxIterations = numeric_limits<int>::max();
double svlLogistic::defaultEpsilon = 1.0e-6;

// default constructor
svlLogistic::svlLogistic() : svlBinaryClassifier(),
			     _posClassWeight(1.0),
			     _lambda(0.0),
			     _numSamples(0),
			     _stepSize(0.9),
			     _bUseBiasTerm(false)
{
  initialize(1, 1.0, 1.0e-6, false);

    declareOption("maxIterations", defaultMaxIterations);
    declareOption("eps", defaultEpsilon);
}

svlLogistic::svlLogistic(unsigned n, double w, double r, bool b) : svlBinaryClassifier(),
								   _posClassWeight(1.0), _lambda(0.0), _numSamples(0), _stepSize(0.9), _bUseBiasTerm(b)
							   
{
    SVL_ASSERT(n > 0);
    initialize(n, w, r, b);

    declareOption("maxIterations", defaultMaxIterations);
    declareOption("eps", defaultEpsilon);
}

svlLogistic::svlLogistic(const VectorXd& t) : svlBinaryClassifier(),
					      _posClassWeight(1.0), _lambda(0.0), _numSamples(0), _stepSize(0.9), _bUseBiasTerm(false)
{
  initialize(t);

    declareOption("maxIterations", defaultMaxIterations);
    declareOption("eps", defaultEpsilon);
}

svlLogistic::svlLogistic(const vector<double>& t) : svlBinaryClassifier(),
						    _posClassWeight(1.0), _lambda(0.0), _numSamples(0), _stepSize(0.9), _bUseBiasTerm(false)
						    
{
  initialize(t);

    declareOption("maxIterations", defaultMaxIterations);
    declareOption("eps", defaultEpsilon);
}

// copy constructor
svlLogistic::svlLogistic(const svlLogistic& model) : svlBinaryClassifier(model),
  _theta(model._theta), _gradient(model._gradient), _hessian(model._hessian),
  _posClassWeight(model._posClassWeight), _lambda(model._lambda),
  _featureSum(model._featureSum), _featureSum2(model._featureSum2),
  _numSamples(model._numSamples), _stepSize(model._stepSize), 
  _bUseBiasTerm(model._bUseBiasTerm)
{
    // do nothing
}

// destructor
svlLogistic::~svlLogistic()
{
    // do nothing
}

// initialize the parameters
void svlLogistic::initialize(unsigned n, double w, double r, bool b)
{
    SVL_ASSERT(n > 0);

    int sizeOfFV = b ? (n+1) : n;

    _nFeatures = n;
    _theta = VectorXd::Zero(sizeOfFV);
    _gradient = VectorXd::Zero(sizeOfFV);
    _hessian = MatrixXd::Zero(sizeOfFV, sizeOfFV);
    _featureSum = VectorXd::Zero(sizeOfFV);
    _featureSum2 = VectorXd::Zero(sizeOfFV);

    _posClassWeight = w;
    _lambda = r;
    _numSamples = 0;
    _stepSize = 0.9f;
    _bUseBiasTerm = b;
}

void svlLogistic::initialize(const VectorXd& t)
{
    int n = t.rows();

    _theta = t;
    _gradient = VectorXd::Zero(n);
    _hessian = MatrixXd::Zero(n, n);
    _featureSum = VectorXd::Zero(n);
    _featureSum2 = VectorXd::Zero(n);

    _numSamples = 0;
    _stepSize = 0.9f;
}

void svlLogistic::initialize(const vector<double>& t)
{
    _theta = VectorXd::Zero(t.size());
    for (unsigned i = 0; i < t.size(); i++) {
        _theta(i) = t[i];
    }
    initialize(_theta);
}

void svlLogistic::initialize(unsigned n, unsigned k)
{
	initialize(n, k);
}

void svlLogistic::zero()
{
    _theta.setZero();
}

void svlLogistic::randomize()
{
    int n = _theta.rows();

    _theta.Random(n);
    _gradient.setZero();
    _hessian.setZero();
    _featureSum.setZero();
    _featureSum2.setZero();
}

bool svlLogistic::save(const char *filename) const
{
    ofstream ofs(filename);
    if (ofs.fail()) {
	return false;
    }

    ofs << "<svlClassifier id=\"LOGISTIC\"\n"
        << "               version=\"1\"\n"
        << "               numFeatures=\"" << _nFeatures << "\"\n"
        << "               numClasses=\"" << _nClasses << "\"\n"
        << "               useBiasTerm=\"" << (_bUseBiasTerm ? 1 : 0) << "\" >\n";

    ofs << "  <theta rows=\"" << _theta.rows() << "\">\n";
    ofs << "    " << _theta << "\n";
    ofs << "  </theta>\n";

    ofs << "</svlClassifier>\n";

    ofs.close();
    return true;
}

// Save into an XML node.
bool svlLogistic::save(XMLNode &node) const
{
	if ( !this->svlClassifier::save(node) )
		return false;
	
	// Get classifier node as last instantiated child.
	XMLNode me = node.getChildNode(node.nChildNode()-1);
	me.updateAttribute("LOGISTIC", NULL, "id");
	me.updateAttribute(_bUseBiasTerm ? "1" : "0", NULL, "useBiasTerm");

	// Add theta as a sub node.
	stringstream ss;
	XMLNode ch = me.addChild("theta");
	char c[32];
	sprintf(c,"%d",_theta.rows());
	ch.addAttribute("rows",c);
	ss << _theta(0);
	for ( unsigned j=1; j<(unsigned)_theta.rows(); ++j )
		ss << " " << _theta(j);
	ch.addText(ss.str().c_str());

    return true;
}

bool svlLogistic::load(XMLNode& node)
{
    if (!this->svlBinaryClassifier::load(node)) {
        return false;
    }

    XMLNode child = node.getChildNode("theta");
    if (child.isEmpty()) {
        SVL_LOG(SVL_LOG_ERROR, "invalid svlLogistic classifier file");
        return false;
    }

    unsigned n = atoi(child.getAttribute("rows"));
    vector<double> t;
    parseString<double>(string(child.getText()), t);

    SVL_ASSERT(t.size() == n);

    initialize(t);

    if (node.isAttributeSet("useBiasTerm")) {
      unsigned b = atoi(node.getAttribute("useBiasTerm"));
      _bUseBiasTerm = (b > 0);
    } else {
      _bUseBiasTerm = false;
    }

    return true;
}

// TODO: change the ridiculous implementations

static VectorXd addBiasTerm(const VectorXd &x)
{
  vector<double> m = eigenToSTL(x);
  m.push_back(1);
  return Eigen::Map<VectorXd>(&m[0], m.size());;
}
// expand the columns of x to add one more term
static MatrixXd addBiasTermM(const MatrixXd &x)
{
  vector<vector<double> > m(x.rows());

  for (int i = 0; i < x.rows(); i++) {
    m[i] = eigenToSTL(x.row(i));
    m[i].push_back(1);
  }

  return stlToEigen(m);
}

double svlLogistic::getScore(const VectorXd &x) const
{
  if (_bUseBiasTerm)
    return addBiasTerm(x).dot(_theta);

  return x.dot(_theta);
}

double svlLogistic::getScore(const vector<double> &x) const
{
  if (_bUseBiasTerm) {
    SVL_ASSERT(x.size() == (unsigned)_theta.rows() - 1);
    return _theta.dot(addBiasTerm(Eigen::Map<VectorXd>(&x[0], x.size())));
  }

  SVL_ASSERT(x.size() == (unsigned)_theta.rows());
  return _theta.dot(Eigen::Map<VectorXd>(&x[0], x.size()));
}

// evaluate model (caller is responsible for providing correct size matrix)
void svlLogistic::getProbabilitiesFromMatrices(const MatrixXd &x, VectorXd& y) const
{
  if (_bUseBiasTerm) {
    SVL_ASSERT(x.cols() == _theta.rows()-1);
    y = (1.0 + (-1.0 * addBiasTermM(x) * _theta).cwise().exp().cwise()).cwise().inverse();
  }

    SVL_ASSERT(x.cols() == _theta.rows());
    y = (1.0 + (-1.0 * x * _theta).cwise().exp().cwise()).cwise().inverse();
}

// compute mean-square-error between two vectors
double svlLogistic::mse(const VectorXd &y, const VectorXd& p) const
{
    SVL_ASSERT(y.rows() == p.rows());
    return (y - p).squaredNorm() / (double)y.rows();
}

double svlLogistic::mse(const vector<double>& y, const vector<double>& p) const
{
    SVL_ASSERT(y.size() == p.size());
    if (y.empty()) {
	return 0.0;
    }

    double m = 0.0;
    for (unsigned i = 0; i < y.size(); i++) {
	m += (y[i] - p[i]) * (y[i] - p[i]);
    }

    return m / (double)y.size();
}

// accumulate evidence with data x, target y and prediction p
void svlLogistic::accumulate(const MatrixXd& x_given, const VectorXd& y, const VectorXd& p)
{
  const MatrixXd *x_ptr = &x_given;
  MatrixXd x2;
  if (_bUseBiasTerm) {
    x2 = addBiasTermM(x_given);
    x_ptr = &x2;
  }
  const MatrixXd &x = *x_ptr;
    
    SVL_ASSERT((x.cols() == _theta.rows()) && (x.rows() == y.rows()) && (p.rows() == y.rows()));

    // compute (y-p) and (p (1 - p))
    VectorXd y_minus_p = y - p;
    VectorXd pp = p - p.cwise().square();

    // iterate over all training samples
    for (int i = 0; i < y.rows(); i++) {
	// weight positive class
	if (y(i) > 0.5) {
	    pp(i) *= _posClassWeight;
            y_minus_p(i) *= _posClassWeight;
	}
	// compute graident and _hessian
        for (int n = 0; n < _theta.rows(); n++) {
            _gradient(n) += x(i, n) * y_minus_p(i);
            for (int m = 0; m < _theta.rows(); m++) {
                _hessian(n, m) += x(i, n) * x(i, m) * pp(i);
            }
        }
	// accumulate statistics for feature mean and variance
	for (int n = 0; n < _theta.rows(); n++) {
            _featureSum(n) += x(i, n);
            _featureSum2(n) += x(i, n) * x(i, n);
	}
    }

    _numSamples += y.rows();
}

void svlLogistic::accumulate(const vector<vector<double> >& x_given,
			       const vector<double>& y,
			       const vector<double> &p)
{
  const vector<vector<double> > *x_ptr = &x_given;
  vector<vector<double> > x_new(x_given.size());
  if (_bUseBiasTerm) {
    for (unsigned i = 0; i < x_new.size(); i++) {
      x_new[i] = x_given[i];
      x_new[i].push_back(1);
    }
    x_ptr = &x_new;
  }
  const vector<vector<double> > &x = *x_ptr;

    SVL_ASSERT((x.size() == y.size()) && (y.size() == p.size()));

    // compute (y-p)
    vector<double> y_minus_p(y.size());
    for (unsigned i = 0; i < y.size(); i++) {
	y_minus_p[i] = y[i] - p[i];
    }

    // iterate over all training samples
    for (int i = 0; i < (int)y.size(); i++) {
	SVL_ASSERT(x[i].size() == (unsigned)_theta.rows());
        // compute p * (1 - p)
        double pp = p[i] * (1.0 - p[i]);
	// weight positive class
	if (y[i] > 0.5) {
	    pp *= _posClassWeight;
	    y_minus_p[i] *= _posClassWeight;
	}
	// compute _gradient and _hessian
        for (int n = 0; n < _theta.rows(); n++) {
            _gradient(n) += x[i][n] * y_minus_p[i];
            for (int m = 0; m < _theta.rows(); m++) {
                _hessian(n, m) += x[i][n] * x[i][m] * pp;
            }
        }
	// accumulate statistics for feature mean and variance
	for (int n = 0; n < _theta.rows(); n++) {
            _featureSum(n) += x[i][n];
            _featureSum2(n) += x[i][n] * x[i][n];
	}
    }

    _numSamples += (int)y.size();
}

// perform a single training update
void svlLogistic::update()
{
    if(_numSamples == 0)
        return;

    // TODO: should implement using whitener class
    VectorXd sigma = (_featureSum2 / (double)_numSamples) -
        (_featureSum / (double)_numSamples).cwise().square();
    _gradient -= _lambda * _numSamples * (sigma.cwise() * _theta);
    _hessian -= _lambda * _numSamples * sigma.asDiagonal();

    _hessian.ldlt().solveInPlace(_gradient);

    // TO DO: line search
    _theta += _stepSize * _gradient;

    _gradient.setZero();
    _hessian.setZero();
    _featureSum.setZero();
    _featureSum2.setZero();

    _numSamples = 0;
}


double svlLogistic::train(const MatrixXd & X, const VectorXi & Y)
{
    SVL_ASSERT(X.rows() == Y.rows());
    vector<vector<double> >  posSamples, negSamples;

    for (int i = 0; i < Y.rows(); i++) {
        if (Y[i] == 0) {
            VectorXd x = X.row(i);
            negSamples.push_back(eigenToSTL(x));
	} else if (Y[i] == 1) {
            VectorXd x = X.row(i);
            posSamples.push_back(eigenToSTL(x));
	} else {
            SVL_LOG(SVL_LOG_FATAL, "invalid target value " << Y[i]);
        }
    }

    return train(posSamples, negSamples);
}

double svlLogistic::train(const vector<vector<double> >& posSamples,
    const vector<vector<double> >& negSamples)
{
    vector<vector<double> > samples;
    samples.insert(samples.end(), posSamples.begin(), posSamples.end());
    samples.insert(samples.end(), negSamples.begin(), negSamples.end());
    
    vector<double> labels(posSamples.size() + negSamples.size(), 0.0);
    for (unsigned i = 0; i < posSamples.size(); i++)
        labels[i] = 1.0;
    
    return train(samples, labels);
}

double svlLogistic::train(const MatrixXd& x, const VectorXd& y)
{
  double eps = getOptionAsDouble("eps");
  int maxIterations = getOptionAsInt("maxIterations");

    _stepSize = 0.9;
    VectorXd p(y.rows());
    double currentMse, lastMse;
    lastMse = numeric_limits<double>::max();
    VectorXd lastTheta(_theta);
    int iter = 0;
    while (iter++ < maxIterations) {
	getProbabilitiesFromMatrices(x, p);
	currentMse = mse(y, p);
	if ((currentMse > lastMse) || isnan(currentMse)) {
            _theta = lastTheta;
	    getProbabilitiesFromMatrices(x, p);
	    _stepSize *= 0.95;
	} else {
	    if (lastMse - currentMse < eps) break;
	    lastMse = currentMse;
            lastTheta = _theta;
	}
	accumulate(x, y, p);
	update();
    }

    _trained = true;
    return lastMse;
}

double svlLogistic::train(const vector<vector<double> >& x, const vector<double>& y)
{
    double eps = getOptionAsDouble("eps");
    int maxIterations = getOptionAsInt("maxIterations");

    _stepSize = 0.9;
    vector<double> p;
    double currentMse, lastMse;
    lastMse = numeric_limits<double>::max();
    VectorXd lastTheta(_theta);
    int iter = 0;
    while (iter++ < maxIterations) {
	getProbabilities(x, p);
	currentMse = mse(y, p);
	SVL_LOG(SVL_LOG_VERBOSE, iter << " " << currentMse << " (" << _stepSize << ")");
	if ((currentMse > lastMse) || isnan(currentMse)) {
            _theta = lastTheta;
	    getProbabilities(x, p);
	    _stepSize *= 0.95;
	} else {
	    if (lastMse - currentMse < eps) break;
	    lastMse = currentMse;
	    lastTheta = _theta;
	}
	accumulate(x, y, p);
	update();
    }

    _trained = true;
    return lastMse;
}

svlLogistic& svlLogistic::operator=(const svlLogistic& model)
{
    if (this == &model)
        return *this;

    this->svlClassifier::operator=(model);

    _theta = model._theta;
    _gradient = model._gradient;
    _hessian = model._hessian;
    _posClassWeight = model._posClassWeight;
    _lambda = model._lambda;
    _featureSum = model._featureSum;
    _featureSum2 = model._featureSum2;
    _numSamples = model._numSamples;
    _stepSize = model._stepSize;
    _bUseBiasTerm = model._bUseBiasTerm;

    return *this;
}


// svlMultiClassLogistic class -----------------------------------------------

svlMultiClassLogistic::svlMultiClassLogistic(unsigned n, unsigned k, double r) :
    svlClassifier(n, k), _lambda(r)
{
    SVL_ASSERT((_nClasses > 1) && (_nFeatures > 0));
    _theta.resize(_nClasses - 1);
    for (unsigned i = 0; i < _theta.size(); i++) {
	_theta[i].resize(_nFeatures, 0.0);
    }

    declareOption("maxIterations", svlLogistic::defaultMaxIterations);
    declareOption("eps", svlLogistic::defaultEpsilon);
}

svlMultiClassLogistic::svlMultiClassLogistic(const svlMultiClassLogistic& model) :
    svlClassifier(model._nFeatures, model._nClasses),
    _theta(model._theta), _lambda(model._lambda)
{
    // do nothing
}

svlMultiClassLogistic::~svlMultiClassLogistic()
{
    // do nothing
}

// initialize the parameters
void svlMultiClassLogistic::initialize(unsigned n, unsigned k, double r)
{
    _nFeatures = n; _nClasses = k; _lambda = r;

    SVL_ASSERT((_nClasses > 1) && (_nFeatures > 0));
    _theta.resize(_nClasses - 1);
    for (unsigned i = 0; i < _theta.size(); i++) {
        _theta[i] = vector<double>(_nFeatures, 0.0);
    }
}

void svlMultiClassLogistic::initialize(unsigned n, unsigned k)
{
	initialize(n, k, 0.0);
}

void svlMultiClassLogistic::zero()
{
    for (unsigned i = 0; i < _theta.size(); i++) {
        fill(_theta[i].begin(), _theta[i].end(), 0.0);
    }
}

// Save the model to a file.
bool svlMultiClassLogistic::save(const char *filename) const
{
    ofstream ofs(filename);
    if (ofs.fail()) {
		return false;
    }

	ofs << "<svlClassifier id=\"MULTICLASSLOGISTIC\"\n"
        << "               version=\"1\"\n"
        << "               numFeatures=\"" << _nFeatures << "\"\n"
        << "               numClasses=\"" << _nClasses << "\" >\n";

    ofs << "  <theta>\n";    
    for (unsigned i = 0; i < _theta.size(); i++) {
        ofs << "   ";
	for (unsigned j = 0; j < _theta[i].size(); j++) {
	    ofs << " " << _theta[i][j];
	}
	ofs << "\n";
    }
    ofs << "  </theta>\n";

    ofs << "</svlClassifier>\n";

    ofs.close();
    return true;
}

// Save into an XML node.
bool svlMultiClassLogistic::save(XMLNode &node) const
{
	if ( !this->svlClassifier::save(node) )
		return false;
	
	// Get classifier node as last instantiated child.
	XMLNode me = node.getChildNode(node.nChildNode()-1);
	me.updateAttribute("MULTICLASSLOGISTIC", NULL, "id");
	
	// Add theta's as sub nodes.
	stringstream ss;
	for ( unsigned i=0; i<_theta.size(); ++i ) {
		XMLNode ch = me.addChild("theta");
		ss << _theta[i][0];
		for ( unsigned j=1; j<_theta[i].size(); ++j )
			ss << " " << _theta[i][j];
		ch.addText(ss.str().c_str());
	}

    return true;
}

bool svlMultiClassLogistic::load(XMLNode& node)
{
    if (!this->svlClassifier::load(node)) {
        return false;
    }

    XMLNode child = node.getChildNode("theta");
    if (child.isEmpty()) {
        SVL_LOG(SVL_LOG_ERROR, "invalid svlMultiClassLogistic classifier file");
        return false;
    }

    initialize(_nFeatures, _nClasses, _lambda);
    vector<double> t;
    parseString<double>(string(child.getText()), t);
    SVL_ASSERT(t.size() == _theta.size() * _nFeatures);
    
    unsigned k = 0;
    for (unsigned i = 0; i < _theta.size(); i++) {
	for (unsigned j = 0; j < _theta[i].size(); j++) {
	    _theta[i][j] = t[k++];
	}
    }

    return true;
}

void svlMultiClassLogistic::getClassScores(const VectorXd &x, VectorXd &y) const
{
    SVL_ASSERT_MSG(x.size() == _nFeatures, x.size() << " != " << _nFeatures);
    y = VectorXd::Zero(_nClasses);
  
    //y(_nClasses - 1) should remain 0
    for (int k = 0; k < _nClasses - 1; k++) {
        y[k] = x.dot(Eigen::Map<VectorXd>(&_theta[k][0], _theta[k].size()));
    }
}

void svlMultiClassLogistic::getClassScores(const vector<double>& x, vector<double>& y) const
{
    SVL_ASSERT_MSG(x.size() == (unsigned)_nFeatures, x.size() << " != " << _nFeatures);
    y.resize(_nClasses);

    Eigen::Map<VectorXd> mx(&x[0], x.size());

    y[_nClasses - 1] = 0.0;
    for (int k = 0; k < _nClasses - 1; k++) {
        y[k] = mx.dot(Eigen::Map<VectorXd>(&_theta[k][0], _theta[k].size()));
    }
}


// perform full training until log-likelihood no longer decreases
double svlMultiClassLogistic::train(const MatrixXd& x, const VectorXi& y)
{
    //TODO-- convert this to eigen native
    vector<vector<double> > xSTL(x.rows(), vector<double> (x.cols()));
    for (int i = 0; i < x.rows(); i++) {
        for (int j =0; j < x.cols(); j++) {
            xSTL[i][j] = x(i,j);
        }
    }

    vector<int> ySTL(y.size());
    for (int i = 0; i < y.size(); i++) {
        ySTL[i] = y(i);
    }

    return train(xSTL, ySTL);
}

double svlMultiClassLogistic::train(const MatrixXd &X, const VectorXi &y, const VectorXd &w)
{
    //TODO-- convert this to eigen native
	vector<int> ySTL(y.size());
	vector<double> wSTL(w.size());
    vector<vector<double> > xSTL(X.rows(), vector<double> (X.cols()));
	for (int i = 0; i < X.rows(); i++) {
		ySTL[i] = y(i);
		wSTL[i] = w(i);
        for (int j =0; j < X.cols(); j++)
            xSTL[i][j] = X(i,j);
	}

    return train(xSTL, ySTL, wSTL);
}

double svlMultiClassLogistic::train(const vector<vector<double> >& x,
    const vector<int>& y)
{
    double eps = getOptionAsDouble("eps");
    int maxIterations = getOptionAsInt("maxIterations");

    SVL_ASSERT(x.size() == y.size());
    if (x.empty()) return 0.0;

    // check input
    for (unsigned i = 0; i < x.size(); i++) {
	SVL_ASSERT(x[i].size() == (unsigned)_nFeatures);
	SVL_ASSERT(y[i] < _nClasses);
    }

    // instantiate worker class
    svlMultiClassLogisticOptimizer optimizer((_nClasses - 1) * _nFeatures);

    // copy current parameters into optimizer
    for (int k = 0; k < _nClasses - 1; k++) {
	for (int i = 0; i < _nFeatures; i++) {
	    optimizer[k * _nFeatures + i] = _theta[k][i];
	}
    }

    optimizer.nClasses = _nClasses;
    optimizer.nFeatures = _nFeatures;
    optimizer.lambda = _lambda;
    optimizer.pTrainingData = &x;
    optimizer.pTrainingLabels = &y;
    optimizer.pTrainingWeights = NULL;

    // solve
    double negLogL = optimizer.solve(maxIterations, eps, true);

    // copy parameters out of optimizer
    for (int k = 0; k < _nClasses - 1; k++) {
	for (int i = 0; i < _nFeatures; i++) {
	    _theta[k][i] = optimizer[k * _nFeatures + i];
	}
    }

    _trained = true;
    return -negLogL;
}

// perform weighted full training until log-likelihood no longer decreases
double svlMultiClassLogistic::train(const vector<vector<double> >& x,
				    const vector<int>& y, const vector<double> &w)
{
    double eps = getOptionAsDouble("eps");
    int maxIterations = getOptionAsInt("maxIterations");

    SVL_ASSERT(x.size() == y.size());
    SVL_ASSERT(x.size() == w.size());
    if (x.empty()) return 0.0;

    // check input
    for (unsigned i = 0; i < x.size(); i++) {
	SVL_ASSERT(x[i].size() == (unsigned)_nFeatures);
	SVL_ASSERT(y[i] < _nClasses);
        if (w[i] < 0.0) {
            SVL_LOG(SVL_LOG_WARNING, "negative weight for sample " << i <<
                " in " << __PRETTY_FUNCTION__);
        }
    }

    // instantiate worker class
    svlMultiClassLogisticOptimizer optimizer((_nClasses - 1) * _nFeatures);

    // copy current parameters into optimizer
    for (int k = 0; k < _nClasses - 1; k++) {
	for (int i = 0; i < _nFeatures; i++) {
            double val = _theta[k][i];
            optimizer[k * _nFeatures + i] = val;
	}
    }

    optimizer.nClasses = _nClasses;
    optimizer.nFeatures = _nFeatures;
    optimizer.lambda = _lambda;
    optimizer.pTrainingData = &x;
    optimizer.pTrainingLabels = &y;
    optimizer.pTrainingWeights = &w;

    // solve
    double negLogL = optimizer.solve(maxIterations, eps, true);

    // copy parameters out of optimizer
    for (int k = 0; k < _nClasses - 1; k++) {
	for (int i = 0; i < _nFeatures; i++) {
            double val = optimizer[k * _nFeatures + i];
            _theta[k][i] = val;
	}
    }

    _trained = true;
    return -negLogL;
}

void svlMultiClassLogistic::setTheta(const vector<vector<double> > &param)
{
    SVL_ASSERT(!param.empty());

    _theta = param;
    _nClasses = (int)param.size() + 1;
    _nFeatures = (int)param[0].size();
    for (int i = 0; i < _nClasses - 1; i++) {
        SVL_ASSERT_MSG(param[i].size() == (unsigned)_nFeatures,
            "svlMultiClassLogistic::setTheta() non-rectangular input");
    }
}

// standard operators
svlMultiClassLogistic& svlMultiClassLogistic::operator=(const svlMultiClassLogistic& model)
{
    if (this == &model)
        return *this;

    this->svlClassifier::operator=(model);

    _nFeatures = model._nFeatures;
    _nClasses = model._nClasses;
    _theta = model._theta;
    _lambda = model._lambda;

    return *this;
}

// class to do all the grunt work for optimizing the multi-class logistic
double svlMultiClassLogisticOptimizer::objective(const double *x)
{
    double negLogL = 0.0;
    int numTerms = 0;

    vector<double> p(nClasses);
    for (unsigned n = 0; n < pTrainingData->size(); n++) {
	if ((*pTrainingLabels)[n] < 0) continue;
	fill(p.begin(), p.end(), 0.0);

	// compute marginal for training sample
	double maxValue = 0.0;
	for (int k = 0; k < nClasses - 1; k++) {
	    for (int i = 0; i < nFeatures; i++) {
		p[k] += x[k * nFeatures + i] * (*pTrainingData)[n][i];
	    }
	    if (p[k] > maxValue)
		maxValue = p[k];
	}

	// exponentiate and normalize
	double Z = 0.0;
	for (unsigned i = 0; i < p.size(); i++) {
	    p[i] = exp(p[i] - maxValue);
	    Z += p[i];
	}

        if (pTrainingWeights == NULL) {
            negLogL -= log(p[(*pTrainingLabels)[n]] / Z);
        } else {
            negLogL -= (*pTrainingWeights)[n] * log(p[(*pTrainingLabels)[n]] / Z);
        }
	numTerms += 1;
    }

    // regularization
    double weightNorm = 0.0;
    for (unsigned i = 0; i < _n; i++) {
	weightNorm += x[i] * x[i];
    }

    negLogL += 0.5 * (double)numTerms * lambda * weightNorm;

#if 0
    cerr << "svlMultiClassLogisticOptimizer::objective() is " << negLogL << endl;
#endif
    return negLogL;
}

void svlMultiClassLogisticOptimizer::gradient(const double *x, double *df)
{
    int numTerms = 0;
    memset(df, 0, size() * sizeof(double));

    vector<double> p(nClasses);
    for (unsigned n = 0; n < pTrainingData->size(); n++) {
	if ((*pTrainingLabels)[n] < 0) continue;
        double alpha = (pTrainingWeights == NULL) ? 1.0 : (*pTrainingWeights)[n];
	fill(p.begin(), p.end(), 0.0);

	// compute marginal for training sample
	double maxValue = 0.0;
	for (int k = 0; k < nClasses - 1; k++) {
	    for (int i = 0; i < nFeatures; i++) {
		p[k] += x[k * nFeatures + i] * (*pTrainingData)[n][i];
	    }
	    if (p[k] > maxValue)
		maxValue = p[k];
	}

	// exponentiate and normalize
	double Z = 0.0;
	for (unsigned i = 0; i < p.size(); i++) {
	    p[i] = exp(p[i] - maxValue);
	    Z += p[i];
	}
	for (unsigned i = 0; i < p.size(); i++) {
	    p[i] /= Z;
	}

	numTerms += 1;

	// increment derivative
	for (int k = 0; k < nClasses - 1; k++) {
	    for (int i = 0; i < nFeatures; i++) {
		df[k * nFeatures + i] += alpha * p[k] * (*pTrainingData)[n][i];
	    }
	    if ((*pTrainingLabels)[n] == k) {
		for (int i = 0; i < nFeatures; i++) {
		    df[k * nFeatures + i] -= alpha * (*pTrainingData)[n][i];
		}
	    }
	}
    }

    // regularization
    for (unsigned i = 0; i < _n; i++) {
	df[i] += (double)numTerms * lambda * x[i];
    }

#if 0
    cerr << "svlMultiClassLogisticOptimizer::gradient()" << endl;
#endif
}


#define FAST_LOOPS
double svlMultiClassLogisticOptimizer::objectiveAndGradient(const double *x, double *df)
{
    const int nFeaturesDiv4 = nFeatures / 4;
    const int nFeaturesMod4 = nFeatures % 4;

    double negLogL = 0.0;
    int numTerms = 0;
    memset(df, 0, _n * sizeof(double));

    vector<double> p(nClasses);
    for (unsigned n = 0; n < pTrainingData->size(); n++) {
	if ((*pTrainingLabels)[n] < 0) continue;
        double alpha = (pTrainingWeights == NULL) ? 1.0 : (*pTrainingWeights)[n];
        fill(p.begin(), p.end(), 0.0);

        const vector<double> &f = (*pTrainingData)[n];

	// compute marginal for training sample
	double maxValue = 0.0;
        const double *x_ptr = x;
	for (int k = 0; k < nClasses - 1; k++) {
#ifndef FAST_LOOPS
            for (vector<double>::const_iterator it = f.begin(); it != f.end(); ++it) {
		p[k] += (*x_ptr++) * (*it);
	    }
#else
            const double *f_ptr = &f[0];
            for (int i = nFeaturesDiv4; i != 0; i--) {
                p[k] += x_ptr[0] * f_ptr[0] + x_ptr[1] * f_ptr[1] +
                    x_ptr[2] * f_ptr[2] + x_ptr[3] * f_ptr[3];
                x_ptr += 4; f_ptr += 4;
            }
            for (int i = 0; i < nFeaturesMod4; i++) {
                p[k] += (*x_ptr++) * (*f_ptr++);
            }
#endif
	    if (p[k] > maxValue)
		maxValue = p[k];
	}

	// exponentiate and normalize
#if 1
	double Z = 0.0;
	for (vector<double>::iterator it = p.begin(); it != p.end(); ++it) {
	    Z += (*it = exp(*it - maxValue));
	}
#ifndef FAST_LOOPS
	for (vector<double>::iterator it = p.begin(); it != p.end(); ++it) {
	    *it /= Z;
	}
#else
        double *p_ptr = &p[0];
        for (int i = nClasses / 2; i != 0; i--) {
            p_ptr[0] /= Z;
            p_ptr[1] /= Z;
            p_ptr += 2;
        }
        if (nClasses % 2 != 0) {
            *p_ptr /= Z;
        }
#endif
#else
        Eigen::Map<VectorXd> v(&p[0], p.size());
        v = (v.cwise() - maxValue).cwise().exp();
        double Z = v.sum();

        double *p_ptr = &p[0];
        for (int i = nClasses / 2; i != 0; i--) {
            p_ptr[0] /= Z;
            p_ptr[1] /= Z;
            p_ptr += 2;
        }
        if (nClasses % 2 != 0) {
            *p_ptr /= Z;
        }
#endif
	// increment log-likelihood
	negLogL -= alpha * log(p[(*pTrainingLabels)[n]]);
	numTerms += 1;

	// increment derivative
        double *df_ptr = df;
        for (int k = 0; k < nClasses - 1; k++) {
            double nu = alpha * p[k];
#ifndef FAST_LOOPS
            for (vector<double>::const_iterator it = f.begin(); it != f.end(); ++it) {
		(*df_ptr++) += nu * (*it);
	    }
#else
            const double *f_ptr = &f[0];
            for (int i = nFeaturesDiv4; i != 0; i--) {
                df_ptr[0] += nu * f_ptr[0];
                df_ptr[1] += nu * f_ptr[1];
                df_ptr[2] += nu * f_ptr[2];
                df_ptr[3] += nu * f_ptr[3];
                df_ptr += 4; f_ptr += 4;

            }
            for (int i = 0; i < nFeaturesMod4; i++) {
                (*df_ptr++) += nu * (*f_ptr++);
            }
#endif
        }

        if ((*pTrainingLabels)[n] < nClasses - 1) {
            df_ptr = &df[(*pTrainingLabels)[n] * nFeatures];
#ifndef FAST_LOOPS
            for (vector<double>::const_iterator it = f.begin(); it != f.end(); ++it) {
                (*df_ptr++) -= alpha * (*it);
            }
#else
            const double *f_ptr = &f[0];
            for (int i = nFeaturesDiv4; i != 0; i--) {
                df_ptr[0] -= alpha * f_ptr[0];
                df_ptr[1] -= alpha * f_ptr[1];
                df_ptr[2] -= alpha * f_ptr[2];
                df_ptr[3] -= alpha * f_ptr[3];
                df_ptr += 4; f_ptr += 4;

            }
            for (int i = 0; i < nFeaturesMod4; i++) {
                (*df_ptr++) -= alpha * (*f_ptr++);
            }
#endif
	}
    }

    // regularization
    double weightNorm = 0.0;
    double nu = (double)numTerms * lambda;
    for (unsigned i = 0; i < _n; i++) {
	weightNorm += x[i] * x[i];
	df[i] += nu * x[i];
    }

    negLogL += 0.5 * nu * weightNorm;

#if 0
    cerr << "svlMultiClassLogisticOptimizer::objectiveAndGradient() is " << negLogL << endl;
#endif
    return negLogL;
}

// configuration --------------------------------------------------------

class svlLogisticConfig : public svlConfigurableModule {
public:
    svlLogisticConfig() : svlConfigurableModule("svlML.svlLogistic") { }

    void usage(ostream &os) const {
        os << "      maxIterations:: max. number of training iterations (default: " 
           << svlLogistic::defaultMaxIterations << ")\n"
           << "      eps          :: convergence epsilon (default: " << svlLogistic::defaultEpsilon << ")\n";
    }

    void setConfiguration(const char *name, const char *value) {
        // factor operation cache
        if (!strcmp(name, "maxIterations")) {
            svlLogistic::defaultMaxIterations = atoi(value);
        } else if (!strcmp(name, "eps")) {
            svlLogistic::defaultEpsilon = atof(value);
        } else {
            SVL_LOG(SVL_LOG_FATAL, "unrecognized configuration option for " << this->name());
        }
    }
};

static svlLogisticConfig gLogisticConfig;

