/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlFeatureSelector.cpp
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
**
*****************************************************************************/

#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <sys/types.h>
#include <limits>

#include "svlBase.h"
#include "svlVision.h"

#if defined(_WIN32)||defined(WIN32)||defined(__WIN32__)
#define _CRT_SECURE_NO_DEPRECATE
#include "win32/dirent.h"
#else
#include <dirent.h>
#endif

using namespace std;

unsigned svlFeatureSelector::MIN_SIZE = 0;
unsigned svlFeatureSelector::MAX_SIZE = INT_MAX;

bool svlFeatureSelector::filter(vector<vector<double> > &posSamples,
				vector<vector<double> > &negSamples,
				vector<bool> &selectedFeatures,
				int expectedNumFeatures)
{
  if (posSamples.size() == 0 || negSamples.size() == 0) {
    SVL_LOG(SVL_LOG_WARNING, "Need to provide both positive and negative examples");
    return false;
  }

  if (expectedNumFeatures > 0) {
    _numFeatures = expectedNumFeatures;
  } else {
    _numFeatures = posSamples[0].size();
  }

  for (int j = (int)posSamples.size() - 1; j >= 0; j--) {
    if (posSamples[j].size() != _numFeatures) {
      posSamples.erase(posSamples.begin() + j);
      SVL_LOG(SVL_LOG_WARNING, "Had to erase posSample " << j << ", incorrect num features");
    }
  }
  
  for (int j = (int)negSamples.size() - 1; j >= 0; j--) {
    if (negSamples[j].size() != _numFeatures) {
      negSamples.erase(negSamples.begin() + j);
      SVL_LOG(SVL_LOG_WARNING, "Had to erase negSample " << j << ", incorrect num features");
    }
  }

  bool success = cacheExamples(posSamples, negSamples);
  if (!success) {
    SVL_LOG(SVL_LOG_WARNING, "Provided examples are not formatted correctly");
    return false;
  }

  _numSelectedFeatures = 0;
  selectedFeatures.clear();
  selectedFeatures.resize(_numFeatures, false);
  success = chooseFeatures(selectedFeatures);

  if (!success) {
    SVL_LOG(SVL_LOG_WARNING, "Filtering was not successful with required parameters");
    return false;
  }
  return true;
}


// configuration --------------------------------------------------------

class svlFeatureSelectorConfig : public svlConfigurableModule {
public:
    svlFeatureSelectorConfig() : svlConfigurableModule("svlML.svlFeatureSelector") { }

    void usage(ostream &os) const {
      os << "      minSize      :: minimum number of features to choose (default: " << svlFeatureSelector::MIN_SIZE << ")\n"
	 << "      maxSize      :: maximum number of features to choose (default: " << svlFeatureSelector::MAX_SIZE << ")\n";
    }

    void setConfiguration(const char *name, const char *value) {
        if (!strcmp(name, "minSize")) {
	  svlFeatureSelector::MIN_SIZE = atoi(value);
        } else if (!strcmp(name, "maxSize")) {
            svlFeatureSelector::MAX_SIZE = atoi(value);
        } else {
            SVL_LOG(SVL_LOG_FATAL, "unrecognized configuration option for " << this->name());
        }
    }
};

static svlFeatureSelectorConfig gFeatureSelectorConfig;



