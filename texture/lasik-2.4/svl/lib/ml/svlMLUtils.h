/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlMLUtils.h
** AUTHOR(S):   Ian Goodfellow <ia3n@stanford.edu>
** DESCRIPTION:
**  General functions that are useful for machine learning
**
**
*****************************************************************************/

#pragma once

#include "Eigen/Core"
#include "Eigen/Array"
#include <vector>
using namespace std;

USING_PART_OF_NAMESPACE_EIGEN;



//Conversion from STL to Eigen
VectorXd stlToEigenV(const vector<double> & v);
VectorXi stlToEigenV(const vector<int> & v);
MatrixXd stlToEigen(const vector<vector<double> > & m);
vector<double> eigenToSTL(const VectorXd & v);

//Eigen utility stuff
bool svlContainsNanOrInf(const VectorXd & v);


// Eigen I/O
void svlBinaryWriteMatrixXd(const char *fname, const MatrixXd &X, bool row_major = true);
void svlBinaryWriteMatrixXf(const char *fname, const MatrixXf &X, bool row_major = true);
void svlBinaryWriteMatrixXi(const char *fname, const MatrixXi &X, bool row_major = true);
void svlBinaryWriteVectorXd(const char *fname, const VectorXd &X);
void svlBinaryWriteVectorXf(const char *fname, const VectorXf &X);
void svlBinaryWriteVectorXi(const char *fname, const VectorXi &X);
