/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCodebook.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
** DESCRIPTION:
**  See svlCodebook.h
**
*****************************************************************************/

#include <cassert>
#include <iostream>
#include <fstream>
#include <limits>
#include <cmath>

// OpenCV libraries
#include "cxcore.h"

// SVL libraries
#include "svlBase.h"
#include "svlCodebook.h"
#include "../vision/svlOpenCVUtils.h"

using namespace std;

// svlCodebook class ---------------------------------------------------------


svlCodebook::svlCodebook(const CvMat *codes) :
    _n(0), _m(0), _codes(NULL)
{
    if (codes != NULL) {
        _codes = cvCloneMat(codes);
        _n = _codes->rows;
        _m = _codes->cols;
    }
}

svlCodebook::svlCodebook(const svlCodebook& book) :
    _n(book._n), _m(book._m), _codes(NULL)
{
    if (book._codes != NULL) {
        _codes = cvCloneMat(book._codes);
    }
}

svlCodebook::~svlCodebook()
{
    if (_codes != NULL)
        cvReleaseMat(&_codes);
}

void svlCodebook::append(const svlCodebook &book)
{
  if (book._m != _m) {
    SVL_LOG(SVL_LOG_WARNING, "Cannot append a codebook with dimension " 
	    << book._m << " to one with dimension " << _m);
    return;
  }
  
  CvMat *newCodes = cvCreateMat(_n + book._n, _m, CV_32FC1);
 
  for (int i = 0; i < _n; i++)
    for (int j = 0; j < _m; j++)
      CV_MAT_ELEM(*newCodes, float, i, j) = CV_MAT_ELEM(*_codes, float, i, j);

  for (int i = 0; i < book._n; i++)
    for (int j = 0; j < _m; j++)
      CV_MAT_ELEM(*newCodes, float, _n + i, j) = CV_MAT_ELEM(*(book._codes), float, i, j);
    
  cvReleaseMat(&_codes);
  _codes = newCodes;
  _n = _n + book._n;
}

// initialization
void svlCodebook::initialize(int n, int m)
{
    SVL_ASSERT((n > 0) && (m > 0));

    _n = n; _m = m;
    if (_codes != NULL)
        cvReleaseMat(&_codes);
    _codes = cvCreateMat(_n, _m, CV_32FC1);
    cvZero(_codes);
    
    // TO DO: intelligent initialization (say uniform?)
}

void svlCodebook::initialize(const CvMat* codes) 
{
    SVL_ASSERT(codes != NULL);

    if (_codes != NULL)
        cvReleaseMat(&_codes);
    _codes = cvCloneMat(codes);
    _n = _codes->rows;
    _m = _codes->cols;
}
    
// i/o
bool svlCodebook::save(const char *filename) const
{
    SVL_ASSERT(filename != NULL);
    ofstream ofs(filename);

    if (!save(ofs)) return false;
 
    ofs.close();
    return true;
}

bool svlCodebook::save(ofstream &ofs) const
{
  if (ofs.fail())
    return false;

  ofs << _n << " " << _m << "\n";
  if (_codes != NULL)
    writeMatrix(_codes, ofs);
  
  return true;
}

bool svlCodebook::load(const char *filename)
{
    SVL_ASSERT(filename != NULL);

    ifstream ifs(filename);

    if (!load(ifs)) return false;

    ifs.close();
    return true;
}

bool svlCodebook::load(istream &ifs)
{
  if (ifs.fail())
    return false;

  if (_codes != NULL) {
    cvReleaseMat(&_codes);
    _codes = NULL;
  }
  
  ifs >> _n >> _m;
  SVL_ASSERT(!ifs.fail());
  if ((_n == 0) || (_m == 0)) {
    return true;
  }
  
  _codes = cvCreateMat(_n, _m, CV_32FC1);
  readMatrix(_codes, ifs);

  return true;
}

// learning
void svlCodebook::learn(const vector<vector<double> >& samples, int n,
    bool bCodeMustBeASample)
{
    SVL_ASSERT(!samples.empty());

    CvMat *m = cvCreateMat(samples.size(), samples[0].size(), CV_32FC1);
    float *p = (float *)CV_MAT_ELEM_PTR(*m, 0, 0);
    for (int i = 0; i < (int)samples.size(); i++) {
        SVL_ASSERT(samples[i].size() == (unsigned)m->cols);
        for (int j = 0; j < m->cols; j++, p++) {
            *p = (float)samples[i][j];
        }
    }
    
    learn(m, n, bCodeMustBeASample);
    
    cvReleaseMat(&m);
}

void svlCodebook::learn(const CvMat *samples, int n, bool bCodeMustBeASample)
{
    SVL_ASSERT((samples != NULL) &&
        (CV_MAT_TYPE(samples->type) == CV_32FC1));
    
    if (n > 0) _n = n;
    _m = samples->cols;
    if (_codes != NULL) {
        cvReleaseMat(&_codes);
        _codes = NULL;
    }

    if ((_n == 0) || (_m == 0))
        return;

    // k-means clustering for vectors; histogram quantization for scalars
    CvMat *clusterAssignments = cvCreateMat(samples->rows, 1, CV_32SC1);
    if (_m > 1) {
        cvKMeans2(samples, _n, clusterAssignments,
            cvTermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 100, 1.0e-3));
    } else {
        vector<pair<float, int> > sortedSamples(samples->rows);
        for (int i = 0; i < samples->rows; i++) {
            sortedSamples[i].first = CV_MAT_ELEM(*samples, float, i, 0);
            sortedSamples[i].second = i;
        }
        sort(sortedSamples.begin(), sortedSamples.end());
        for (int i = 0; i < samples->rows; i++) {
            CV_MAT_ELEM(*clusterAssignments, int, sortedSamples[i].second, 0) =
                (int)(_n * i / samples->rows);
        }
    }

    // compute centroids (codewords)
    _codes = cvCreateMat(_n, _m, CV_32FC1);
    cvZero(_codes);
    vector<int> counts(_n, 0);
    for (int i = 0; i < samples->rows; i++) {
        int k = CV_MAT_ELEM(*clusterAssignments, int, i, 0);
        for (int j = 0; j < samples->cols; j++) {
            CV_MAT_ELEM(*_codes, float, k, j) += CV_MAT_ELEM(*samples, float, i, j);
        }
        counts[k] += 1;
    }

    for (int k = 0; k < _n; k++) {
        if (counts[k] == 0) continue;
        for (int j = 0; j < _codes->cols; j++) {
            CV_MAT_ELEM(*_codes, float, k, j) /= (float)counts[k];
        }
    }

    // find nearest sample to centroid and use as codeword
    if (bCodeMustBeASample) {
        vector<double> minDistances(_n, numeric_limits<double>::max());
        vector<int> minIndex(_n, -1);
        for (int i = 0; i < samples->rows; i++) {
            int k = CV_MAT_ELEM(*clusterAssignments, int, i, 0);
            double d = 0.0;
            for (int j = 0; j < samples->cols; j++) {
                double d_j = (double)(CV_MAT_ELEM(*_codes, float, k, j) - 
                    CV_MAT_ELEM(*samples, float, i, j));
                d += d_j * d_j;
            }
            if (d < minDistances[k]) {
                minDistances[k] = d;
                minIndex[k] = i;
            }
        }

        for (int k = 0; k < _n; k++) {
            if (minIndex[k] < 0) continue;
            for (int j = 0; j < samples->cols; j++) {
                CV_MAT_ELEM(*_codes, float, k, j) =
                    CV_MAT_ELEM(*samples, float, minIndex[k], j);
            }
        }
    }
    

    cvReleaseMat(&clusterAssignments);
}

// evaluatation
int svlCodebook::encode(double x) const
{
    SVL_ASSERT(_m == 1);
    if (_codes == NULL)
        return -1;

    CvMat *d = cvCreateMat(_n, 1, CV_32FC1);
    cvSubS(_codes, cvScalar(x), d);
    
    double minVal, maxVal;
    CvPoint minLoc, maxLoc;
    cvMinMaxLoc(d, &minVal, &maxVal, &minLoc, &maxLoc);
    cvReleaseMat(&d);

    SVL_ASSERT((minLoc.x == 0) && (maxLoc.x == 0));
    if ((-minVal > maxVal) && (maxVal > 0)) {
        return minLoc.y;
    }
    
    return maxLoc.y;
}

int svlCodebook::encode(const CvMat *x) const
{
    SVL_ASSERT((x != NULL) && (x->rows == 1) && (x->cols == _m));
    if (_codes == NULL)
        return -1;

    // TODO: better search (kd-tree ?)
    double minD = numeric_limits<double>::max();
    int minIndx = -1;
    for (int k = 0; k < _n; k++) {
        double d = 0.0;
        for (int i = 0; i < _m; i++) {
            double d_i = (double)(CV_MAT_ELEM(*_codes, float, k, i) - 
                CV_MAT_ELEM(*x, float, 0, i));
            d += d_i * d_i;
            // if (d > minD) break;
        }
        if (d < minD) {
            minD = d;
            minIndx = k;
        }
    }

    return minIndx;
}

int svlCodebook::encode(const vector<double>& x) const
{    
    SVL_ASSERT_MSG((int)x.size() == _m, 
        "Passed in a vector of size " << x.size() << " while expecting size " << _m);
    CvMat *mx = cvCreateMat(1, _m, CV_32FC1);

    for (int i = 0; i < _m; i++) {
        CV_MAT_ELEM(*mx, float, 0, i) = (float)x[i];
    }   
    int indx = encode(mx);
    cvReleaseMat(&mx);
    return indx;
}

void svlCodebook::decode(int indx, double &x) const
{
    SVL_ASSERT((indx >= 0) && (indx < _n));
    SVL_ASSERT(_m == 1);

    x = (double)CV_MAT_ELEM(*_codes, float, indx, 0);
}

void svlCodebook::decode(int indx, CvMat *x) const
{
    SVL_ASSERT((indx >= 0) && (indx < _n));
    SVL_ASSERT((x != NULL) && (x->rows == 1) && (x->cols == _m));

    for (int i = 0; i < _m; i++) {
        CV_MAT_ELEM(*x, float, 0, i) = CV_MAT_ELEM(*_codes, float, indx, i);
    }   
}

void svlCodebook::decode(int indx, vector<double>& x) const
{
    SVL_ASSERT((indx >= 0) && (indx < _n));

    x.resize(_m);
    for (int i = 0; i < _m; i++) {
        x[i] = (double)CV_MAT_ELEM(*_codes, float, indx, i);
    }
}
