/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlClassifier.h
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
** DESCRIPTION:
**  Implements a generic classifier.
**
*****************************************************************************/

#pragma once

#include <vector>

#include "cv.h"
#include "cxcore.h"
#include "ml.h"

#include "xmlParser/xmlParser.h"

#include "svlBase.h"
#include "svlPRcurve.h"

USING_PART_OF_NAMESPACE_EIGEN;

using namespace std;

// svlClassifier class ------------------------------------------------------------------

class svlClassifier : public svlOptions {
 protected:
    int _nFeatures;            // number of features
    int _nClasses;             // number of classes
    bool _trained;             // checks that the classifier was trained (or loaded)

 public:
    svlClassifier();
    svlClassifier(unsigned n, unsigned k);
    svlClassifier(const svlClassifier &c);
    virtual ~svlClassifier() {
        // do nothing
    }
    
    // access functions
    int numFeatures() const { return _nFeatures; }
    int numClasses() const { return _nClasses; }
    virtual bool trained() const { return _trained; }

    // initialization
    virtual void initialize(unsigned n, unsigned k);

    //File loading

    /* purely virtual functions */
    
    // i/o functions    

    // input: filename or XMLNode (future revision)
    // output: true if successful; false otherwise
    // functionality: writes parameters to XML file. It is the derived
    //   classes responsibility to write the correct file format as
    //   expected by load().

    //File saving
    virtual bool save(const char *filename) const = 0;
    virtual bool save(XMLNode& node) const;

    // input: filename or XMLNode
    // output: true if successful; false otherwise
    // functionality: default implementation for the filename version
    //   reads the XML root node it to XMLNode version. XML version
    //   reads the number of features, classes and sets trained to true.
    //   Expects XML format:
    //     <svlClassifier id="CLASSIFIER_ID"
    //                    version="1"
    //                    numFeatures=""
    //                    numClasses="">
    //    </svlClassifier>
    virtual bool load(const char *filename);
    virtual bool load(XMLNode& node);

    // training functions

    // input: a vector of training examples, i.e. feature vectors
    //        a vector of class labels, one per training example
    // functionality: must set _nFeatures to the appropriate value
    //   While this is the most generic version of the train function,
    // it often makes sense to use a different input type,
    // e.g. see svlBinaryClassifier 
    virtual double train(const MatrixXd & samples, const VectorXi & labels) = 0;

    /* functions with standard implementation provided */
    
    // input: a vector of training examples
    //        a vector of class labels, one per example
    //        a vector of weights, one per example
    virtual double train(const MatrixXf & X, const VectorXi & Y);
    virtual double train(const MatrixXd & samples, const VectorXi & labels, const VectorXd & weights);
    virtual double train(const vector<vector<double> >&samples,
			 const vector<int> &labels, const vector<double> &weights);
    virtual double train(const vector<vector<double> >& samples,
        const vector<int> &labels);
    
    // input: filename
    // functionality: loads features and class labels from a file and
    // trains the classifier. Assumes each row contains n features
    // followed by an integer class label.
    virtual double train(const char *filename);

    // evaluation functions
    
    // input: a feature vector
    // output: unnormalized classifier output, one per each class
    virtual void getClassScores(const VectorXd &sample,
				VectorXd &outputScores) const = 0;
    virtual void getClassScores(const vector<double> &sample,
        vector<double> &outputScores) const;
    
    // input: a set of examples, i.e. feature vectors
    // output: for each example, a set of unnormalized marginals, one per class
    virtual void getClassScores(const MatrixXd & samples, MatrixXd & outputScores) const;
    virtual void getClassScores(const vector<vector<double> > &samples, 
        vector<vector<double> > &outputScores) const;

    // input: a feature vector
    // output: a set of normalized marginals, one per each class
    // functionality: exponentiates the scores and normalizes
    //                them to get the marginals
    virtual void getMarginals(const VectorXd & sample, VectorXd & outputMarginals) const;
    virtual void getMarginals(const vector<double> &sample,
        vector<double> &outputMarginals) const;
    
    // input: a set of examples, i.e. feature vectors
    // output: for each example, a set of normalized marginals, one per class
    virtual void getMarginals(const MatrixXd & samples, MatrixXd & outputMarginals) const;
    virtual void getMarginals(const vector<vector<double> > &samples, 
        vector<vector<double> > &outputMarginals) const; 

    // input: a feature vector
    // output: a determininstic prediction of the class label
    virtual int getClassification(const VectorXd & sample) const;
    virtual int getClassification(const vector<double> &sample) const;
            
    // input: a set of examples, i.e. feature vectors
    // output: for each example, the predicted class label
    virtual void getClassifications(const MatrixXd & samples, VectorXi & outputLabels) const;
    virtual void getClassifications(const vector<vector<double> > &samples,
        vector<int> &outputLabels) const;
};



svlFactory<svlClassifier> & svlClassifierFactory();
