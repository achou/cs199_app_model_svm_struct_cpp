/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlQPSolver.h
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**
** DESCRIPTION:
**  Solves (small scale) quadratic programs of the form:
**    minimize (over x) 1/2 x^T P x + q^T x + r
**    subject to        Ax = b
**                      l <= x <= u
**  where P is positive definite.
**
**  Current implementation is based on the infeasible start Newton method, see:
**    * S. Boyd and L. Vandenberghe, Convex Optimization, 2004.
**
**  Future versions may include specialized algorithms such as:
**    * V. Franc and V. Hlavac. A Novel Algorithm for Learning Support Vector
**      Machines with Structured Output Spaces, CTU-CMP-2006-04, May, 2006.
**    * R. J. Vanderbei, LQOQ: An interior point code for quadratic 
**      programming, Technical Report SOR-94-15, Princeton University, 1998.
**
*****************************************************************************/

#pragma once

#include <cstdlib>
#include <vector>

#include "Eigen/Core"
#include "Eigen/Array"
#include "Eigen/Cholesky"

#include "svlBase.h"
#include "svlClassifier.h"
#include "svlBinaryClassifier.h"

using namespace std;
USING_PART_OF_NAMESPACE_EIGEN;

// svlQPSolver --------------------------------------------------------------

class svlQPSolver {
 public:
    // line search parameters
    static double alpha; // in (0, 1/2)
    static double beta;  // in (0, 1)
    
 protected:
    // objective
    MatrixXd _P;
    VectorXd _q;
    double _r;

    // constraints and bounds
    MatrixXd _A;
    VectorXd _b;
    VectorXd _l;
    VectorXd _u;

    VectorXd _x;

 public:
    svlQPSolver();
    svlQPSolver(const MatrixXd& P, const VectorXd& q, double r = 0.0);
    virtual ~svlQPSolver();

    // define problem
    void setObjective(const MatrixXd& P, const VectorXd& q, double r = 0.0);
    void setConstraints(const MatrixXd& A, const VectorXd& b);
    void setBounds(const VectorXd& lb, const VectorXd& ub);    
    void clearConstraints();
    void clearBounds();

    // solve
    double solve();
    double objective() const;
    double objective(const VectorXd& x) const;
    inline VectorXd solution() const { return _x; }

    // accessors
    inline int size() const { return _x.rows(); }    
    inline double operator[](unsigned i) const { return _x[i]; }

 protected:
    // compute gradient: _P x + _q
    inline VectorXd gradient() const { return _P * _x + _q; }
    inline VectorXd gradient(const VectorXd& x) const { return _P * x + _q; }
    
    // special case solvers
    double solveNoEquality();
    double solveSingleEquality();
    double solveSimplex();
    double solveNoBounds();
    double solveGeneral();

    // line search
    double lineSearchNoBounds(const VectorXd& x, const VectorXd& dx,
        const VectorXd& nu, const VectorXd& dnu) const;
    double lineSearchGeneral(const VectorXd& x, const VectorXd& dx,
        const VectorXd& nu, const VectorXd& dnu) const;

    // solve the system of equations:
    //    | H_x  A^T | | x | = - | c |
    //    |  A   H_y | | y |     | b |
    void solveKKTSystem(const MatrixXd& Hx, const MatrixXd& Hy,
        const MatrixXd& A, const VectorXd& c, const VectorXd& b,
        VectorXd& x, VectorXd& y) const;

    //    | H_x  A^T | | x | = - | c |
    //    |  A   0   | | y |     | b |
    void solveKKTSystem(const MatrixXd& Hx, const MatrixXd& A, 
        const VectorXd& c, const VectorXd& b,
        VectorXd& x, VectorXd& y) const;

};
