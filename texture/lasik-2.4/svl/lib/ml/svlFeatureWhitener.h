/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlFeatureWhitener.h
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
** DESCRIPTION:
**  Whitens (zero mean, unit variance) feature vector.
**
*****************************************************************************/

#pragma once

#include <vector>
#include <limits>

#include "Eigen/Core"

#include "svlBase.h"
#include "svlSufficientStats.h"

using namespace std;
USING_PART_OF_NAMESPACE_EIGEN;

// svlFeatureWhitener class --------------------------------------------------

class svlFeatureWhitener {
 private:
    int _n;                   // size of feature vector

    vector<double> _mu;       // mean vector
    vector<double> _beta;     // 1.0 / standard deviation

 public:
    svlFeatureWhitener(int n = 1);
    svlFeatureWhitener(const svlFeatureWhitener& fw);
    svlFeatureWhitener(const svlSufficientStats& stats);
    ~svlFeatureWhitener();

    void clear();
    void clear(int n);

    inline int size() const { return _n; }

    // i/o
    void save(const char *filename) const;
    void save(ostream& os) const;
    void load(const char *filename);
    void load(istream& is);
    
    // in-place evaluation
    void evaluate(vector<double> &x) const;
    void evaluate(vector<vector<double> >& x) const;
    void evaluate(vector<VectorXd *>& x) const;

    // training
    void train(const vector<vector<double> >& x);
    void train(const vector<VectorXd *>& x);
    void train(const vector<double> &x); // for size() == 1 only
    void train(const svlSufficientStats& stats);

    // standard operators
    svlFeatureWhitener& operator=(const svlFeatureWhitener& fw);
};
