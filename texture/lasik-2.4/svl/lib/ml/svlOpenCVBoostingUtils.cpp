/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlOpenCVBoostingUtils.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**              Olga Russakovsky <olga@cs.stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
**
*****************************************************************************/

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <limits>
#include <list>
#include <map>

#include "cv.h"
#include "cxcore.h"
#include "highgui.h"

#include "svlBase.h"
#include "svlOpenCVBoostingUtils.h"

using namespace std;

CvMat *svlCreateOpenCVFeatureVector(const vector<double> &sample)
{
    CvMat *fv = cvCreateMat(1, (int)sample.size(), CV_32FC1);
    for (int k = 0; k < fv->width; k++) {
        cvmSet(fv, 0, k, (float)sample[k]);
    }
    return fv;
}

CvMat *createOpenCVFeatureVector(const VectorXd &sample)
{
    CvMat *fv = cvCreateMat(1, (int)sample.rows(), CV_32FC1);
    for (int k = 0; k < fv->width; k++) {
        cvmSet(fv, 0, k, (float)sample[k]);
    }
    return fv;
}

void allocateOpenCVTrainingData(int numSamples, int numFeatures,
				CvMat ** data, CvMat *&labels, CvMat *&varType)
{
  // create data structures for training
  if (data)
    {
      *data = cvCreateMat(numSamples, numFeatures, CV_32FC1);
      SVL_ASSERT_MSG((data != NULL), "out of memory");
    }

  labels = cvCreateMat(numSamples, 1, CV_32SC1); 
  SVL_ASSERT_MSG((labels != NULL), "out of memory");

  varType = cvCreateMat(numFeatures + 1, 1, CV_8UC1);
  SVL_ASSERT_MSG((varType != NULL), "out of memory");
  for (int j = 0; j < numFeatures; j++) {
    CV_MAT_ELEM(*varType, unsigned char, j, 0) = CV_VAR_NUMERICAL;
  }
  CV_MAT_ELEM(*varType, unsigned char, numFeatures, 0) = CV_VAR_CATEGORICAL;
}

void releaseOpenCVTrainingData(CvMat *&data, CvMat *&labels, CvMat *&varType) {
  cvReleaseMat(&data);
  cvReleaseMat(&labels);
  cvReleaseMat(&varType);
}

void fillInOpenCVTrainingData(const vector<vector<double> > &x,
			      CvMat *data, bool quiet, int offset) {
 // check input
  double maxValue = -numeric_limits<double>::max();
  double minValue = numeric_limits<double>::max();
  for (int j = 0; j < (int)x.size(); j++) {
    SVL_ASSERT(x[j].size() == x[0].size());
    for (int k = 0; k < data->width; k++) {
      cvmSet(data, j + offset, k, x[j][k]);
      SVL_ASSERT(!isnan(x[j][k]));
      if (x[j][k] > maxValue) maxValue = x[j][k];
      if (x[j][k] < minValue) minValue = x[j][k];
    }
  }
  if (!quiet) SVL_LOG(SVL_LOG_MESSAGE, "Training: " << x[0].size() << " features range from " << minValue << " to " << maxValue);

}

int fillInOpenCVTrainingLabels(const vector<int> &y, CvMat *labels, int posLabel) 
{
    int posCount = 0;
    for (int j = 0; j < (int)y.size(); j++) {
        CV_MAT_ELEM(*labels, int, j, 0) = (y[j] == posLabel) ? 1 : -1;
        posCount += (y[j] == posLabel);
    }
    return posCount;
}

int fillInOpenCVTrainingLabels(const VectorXi &y, CvMat *labels)
{
    int posCount = 0;
    for (int j = 0; j < y.size(); j++) {
        CV_MAT_ELEM(*labels, int, j, 0) = (y[j] == 1) ? 1 : -1;
        posCount += y[j];
    }
    return posCount;
}

int fillInOpenCVTrainingLabels(int numPositiveSamples, CvMat *labels) 
{
    for (int j = 0; j < labels->height; j++) {
        CV_MAT_ELEM(*labels, int, j, 0) = (j < numPositiveSamples) ? 1 : -1;
    }
    return numPositiveSamples;
}

CvMat * svlCreateEmptyCvMat(int height, int width, int type)
{
  CvMat * arr = 0;


  arr = cvCreateMatHeader(height, width, type);

  return arr;
}
