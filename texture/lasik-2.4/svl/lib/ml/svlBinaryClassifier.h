/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlBinaryClassifier.h
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
**              Stephen Gould <sgould@stanford.edu>
** DESCRIPTION:
**  Implements a generic binary classifier.
**
*****************************************************************************/

#pragma once

#include <vector>

#include "cv.h"
#include "cxcore.h"
#include "ml.h"

#include "svlBase.h"
#include "svlPRcurve.h"
#include "svlClassifier.h"

using namespace std;

struct svlBinaryClassifierStats {
    int tp; int tn; int fp; int fn;

    svlBinaryClassifierStats() : tp(0), tn(0), fp(0), fn(0) {}    

    double computeFscore(bool normalize) const;

    // returns true if classifier only returns single class
    inline bool isBlackStick() const {
        return (((tn == 0) && (fn == 0)) || ((tp == 0) && (fp == 0)));
    }
};

// svlBinaryClassifier class ------------------------------------------------------------------

class svlBinaryClassifier : public svlClassifier {
 protected:
    bool _useNormedFScore;

 public:
    svlBinaryClassifier() : svlClassifier() { _nClasses = 2; _useNormedFScore = true; }
    svlBinaryClassifier(const svlBinaryClassifier &c) : svlClassifier(c) { _useNormedFScore = c._useNormedFScore; }
    virtual ~svlBinaryClassifier() {}

    void initialize(unsigned n, unsigned k);
    virtual void initialize(unsigned n);
    
    /* defining some of the svlClassifier's purely virtual functions
     * to instead rely on the optimized ones below */

    void getClassScores(const VectorXd & sample, VectorXd & outputScores) const;
    virtual void getClassScores(const vector<double> &sample,
        vector<double> &outputScores) const;

    virtual double train(const MatrixXd & posSamples,
        const MatrixXd &negSamples);
    virtual double train(const vector<vector<double> >& posSamples,
        const vector<vector<double> >&negSamples);

    // from svlClassifier
    virtual double train(const MatrixXf & X, const VectorXi & Y) { return svlClassifier::train(X, Y); }
    virtual double train(const MatrixXd & samples, const VectorXi & labels, const VectorXd & weights) {
        return svlClassifier::train(samples, labels, weights); }
    virtual double train(const vector<vector<double> >&samples, const vector<int> &labels, const vector<double> &weights) {
        return svlClassifier::train(samples, labels, weights); }
    virtual double train(const vector<vector<double> >& samples, const vector<int> &labels) {
        return svlClassifier::train(samples, labels);
    }

    /* since this is a binary classifier, can request just a single score
     * per sample; score >= 0 => positive detection */

    virtual double getScore(const VectorXd  &sample) const = 0;
    virtual double getScore(const vector<double> &sample) const;

    /* useful functions implemented generally, relying on getScore */
  
    virtual void getScores(const vector<vector<double> > &samples,
        vector<double> &outputScores) const;
    void getScores(const MatrixXd &samples,
		   vector<double> &outputScores) const;
    void getScores(const MatrixXf & samples,
		   vector<double> & outputScores) const;
    

    virtual double getProbability(const vector<double> &sample) const;

    void getProbabilities(const vector<vector<double> > &samples,
        vector<double> &outputProbs) const;

    /* useful functions relying on the potentially optimized getScores */

    svlBinaryClassifierStats getStats(const vector<vector<double> > &posSamples,
        const vector<vector<double> > &negSamples) const;
    svlBinaryClassifierStats getStats(const vector<vector<double> > &samples,
        const vector<int> &labels, int posLabel) const;
    svlBinaryClassifierStats getStats(const MatrixXd &samples,
        const VectorXi &labels) const;
    svlBinaryClassifierStats getStats(const MatrixXf &samples,
        const VectorXi &labels) const;
    int numClassifiedAsPositive(const vector<vector<double> >& samples) const;
    svlPRCurve getPRCurve(const vector<vector<double> > &posSamples,
        const vector<vector<double> > &negSamples) const;
    
    // returns "normed F-score" (or optionally real F-score)   
    inline void useRealFScoreByDefault() { _useNormedFScore = false;}
    double validate(const vector<vector<double> > &posSamples,
        const vector<vector<double> > &negSamples,
        const string label = "", bool useNormedFScore = true) const;
    double validate(const MatrixXd &X, const VectorXi &Y, const string label = "", bool useNormedFscore = true) const;
    double validate(const MatrixXf &X, const VectorXi &Y, const string label = "", bool useNormedFscore = true) const;
    double validate(const svlBinaryClassifierStats &stats, const string label = "",  bool useNormedFscore = true) const;

    inline double scoreToProbability(double score) const {
        return 1.0 / (1.0 + exp(-score));
    }
    inline double probabilityToScore(double pr) const {
        return (log(pr) - log(1.0 - pr));
    }
};



