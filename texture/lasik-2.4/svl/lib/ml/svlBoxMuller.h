/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlBoxMuller.h
** AUTHOR(S):   Ian Goodfellow <ia3n@stanford.edu>
** DESCRIPTION:
**     Functions/classes for sampling from N(0,1) using the
**     polar Box Muller algorithm.
**     
**
*****************************************************************************/

#pragma once
#include "svlMLUtils.h"

//To get a large number of samples simultaneously, use this function
//(The only overhead is that if you have an odd-length array, it wastes
//a sample)
inline void svlBoxMullerSample(VectorXd & output);

//If you periodically request one sample, use this class. Box Muller generates
//two samples at a time; this class will store the second sample until the
//next request.
class svlBoxMuller
{
 public:

 svlBoxMuller() : _sampleReady(false) {}

  inline double sample();

 private:

  bool _sampleReady;
  double _waitingSample;
  Vector2d _uv;
  double _s;
  double _scale;
};

//Implementation of inline methods / functions
void svlBoxMullerSample(VectorXd & output)
{
  unsigned last = output.size() - 1;
  Vector2d uv;
  double s;
  double scale;

  for (unsigned i = 0; i < last; i += 2)
    {
      uv.setRandom();
      s = uv.dot(uv);
      while (s >= 1.0 || s <= 0.0)
	{
	  uv.setRandom();
	  s = uv.dot(uv);
	}
      scale = sqrt(-2.0*log(s)/s);
      output(i) = uv(0) * scale;
      output(i+1) = uv(1) * scale;
    }
  if (last %2 == 0)
    {
      uv.setRandom();
      s = uv.dot(uv);
      while (s >= 1.0 || s <= 0.0)
	{
	  uv.setRandom();
	  s = uv.dot(uv);
	}
      scale = sqrt(-2.0*log(s)/s);
      output(last) = uv(0)*scale;
    }
}

double svlBoxMuller::sample()
{
    if (_sampleReady)
      {
	_sampleReady = false;
	return _waitingSample;
      }
    else
      {
	_uv.setRandom();
	_s = _uv.dot(_uv);
	while (_s >= 1.0 || _s <= 0.0)
	  {
	    _uv.setRandom();
	    _s = _uv.dot(_uv);
	  }
	_sampleReady = true;
	_scale = sqrt(-2.0*log(_s)/_s);
	_waitingSample = _uv(0)*_scale;
	return _uv(1)*_scale;
      }
}
