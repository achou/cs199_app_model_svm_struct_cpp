/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlFeatureSelector.h
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
**
** DESCRIPTION:
**  Implements a generic abstract class for selecting informative features.
**
*****************************************************************************/

#pragma once

#include <cassert>
#include <iostream>
#include <vector>
#include <map>

#include "cv.h"
#include "cxcore.h"
#include "ml.h"

#include "svlBase.h"
using namespace std;

// svlFeatureSelector ----------------------------------------------------------

class svlFeatureSelector {
 public:
  // determines the minimum and maximum number of features to be
  // selected; both override any other set thresholds
  static unsigned MIN_SIZE;
  static unsigned MAX_SIZE;

  virtual ~svlFeatureSelector() {}

  // Input: posExampleResponses[i][j] is the response of feature j on the
  //        i^th positive training example, similarly for negExampleResponses
  // Output: - a vector of booleans indicating which features to keep
  //         - boolean indicator of success (prints warning messages on failure)
  // 
  // Computes the expected number of features based on the first
  // training example (unless specified), ensures that the
  // input is formatted correctly, e.g. deletes all examples with an
  // inconsistent number of features, and calls the pure virtual
  // methods cacheExamples and chooseFeatures
  bool filter(vector<vector<double> > &posExampleResponses,
	      vector<vector<double> > &negExampleResponses,
	      vector<bool> &returnSelectedFeatures, int expectedNumFeatures = -1);

 protected:
  unsigned _numFeatures;
  unsigned _numSelectedFeatures;

  virtual bool cacheExamples(const vector<vector<double> > &posExampleResponses,
			     const vector<vector<double> > &negExampleResponses) = 0;
  virtual bool chooseFeatures(vector<bool> &selectedFeatures) = 0;
};

