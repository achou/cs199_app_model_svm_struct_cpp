/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlSVM.cpp
** AUTHOR(S):   Ian Goodfellow <ia3n@cs.stanford.edu>
**              Ethan Dreyfuss <ethan@cs.stanford.edu>
**              Olga Russakovsky <olga@cs.stanford.edu>
** DESCRIPTION:
**
*****************************************************************************/
#include "svlSVM.h"
#include "svlBase.h"

#include <float.h>
#include <stdio.h> // to be more compatible with libsvm
#include <math.h>

using namespace std;

string svlSVM::_defaultKernel = string("LINEAR");
int svlSVM::_defaultDegree = 3;
double svlSVM::_defaultEps = 0.0001;
double svlSVM::_defaultPosWeight = 1.0;
int svlSVM::_defaultNumFolds = 2;
svlParamForCV svlSVM::_paramC;
double svlSVM::_defaultC = 0.0;
svlParamForCV svlSVM::_paramGamma;
double svlSVM::_defaultGamma = -1.0;
string svlSVM::HEADER = string("svlClassifier SVM");


static void init_params(struct svm_parameter &params,
			double gamma, double C,
			int nPos, int nNeg, bool need_probs) {
  params.svm_type = C_SVC;

  if (!strNoCaseCompare(svlSVM::_defaultKernel, string("LINEAR")))
    params.kernel_type = LINEAR;
  else if (!strNoCaseCompare(svlSVM::_defaultKernel, string("RBF")))
    params.kernel_type = RBF;
  else if (!strNoCaseCompare(svlSVM::_defaultKernel, string("POLY")))
    params.kernel_type = POLY;
  else
    SVL_ASSERT_MSG(false, "unknown kernel type " << svlSVM::_defaultKernel);

  params.degree = svlSVM::_defaultDegree;
  params.coef0 = 1.0;
  params.p = 0;
  params.nu = 0.5;
  params.eps = svlSVM::_defaultEps; //0.0001;//0.00001
  params.cache_size = 800;
  params.shrinking = true;
  params.probability = need_probs;
  params.nr_weight = 0;
  params.weight = NULL;
  params.weight_label = NULL;

  // as supplied by user
  params.gamma = gamma;
  params.C = C;

  // change the weight of the positive samples
  if (svlSVM::_defaultPosWeight != 1.0) {
    params.nr_weight = 1;
    params.weight_label = new int[1];
    params.weight_label[0] = 1;
    params.weight = new double[1];
    if (svlSVM::_defaultPosWeight < 0)
      params.weight[0] = (double)nNeg / nPos;
    else
      params.weight[0] = svlSVM::_defaultPosWeight;
  }
}

#define TOTAL_THREADS 8

struct CrossValidateArgs {
  CrossValidateArgs(unsigned numPos, unsigned numNeg, int numFolds,
		    double g, double c,
		    const struct svm_problem *p,
		    double *res) :
    nPos(numPos), nNeg(numNeg), nFolds(numFolds),
    gamma(g), C(c), problem(p), result(res) {}
    
  unsigned nPos;
  unsigned nNeg;
  int nFolds;
  double gamma;
  double C;
  const struct svm_problem *problem;
  double *result;		    
};

void *crossValidate(void *argsP, unsigned tid) {
  CrossValidateArgs *args = (CrossValidateArgs*)argsP;

  SVL_LOG(SVL_LOG_VERBOSE, "Starting to cross-validate gamma="<<args->gamma
	  <<", C="<<args->C);

   //Set up parameters
  struct svm_parameter params;
  init_params(params, args->gamma, args->C, args->nPos, args->nNeg, false);
  
    //Cross validate
  double * crossValidationResults = new double[args->nPos + args->nNeg];
  SVL_ASSERT(crossValidationResults);

  const char *msg = svm_check_parameter(args->problem, &params);
  if (msg != NULL) {
    SVL_LOG(SVL_LOG_WARNING, "SVM skipping cross validation of invalid parameters");
    *(args->result) = -1;
  }

  svm_cross_validation(args->problem, &params, args->nFolds, crossValidationResults);

  //Actually computing F score rather than percent correct
  double tp = 0, fp = 0, fn = 0;

  for (unsigned int k = 0; k < args->nPos + args->nNeg; k++) {
    if (args->problem->y[k]) {
      if (crossValidationResults[k])
	tp++;
      else
	fn++;
    } else {
      if (crossValidationResults[k])
	fp++;
    }
  }

  double p;
  double r;

  if (tp+fp==0 || tp+fn == 0) {
    *(args->result) = 0;
  } else {
    // normalize
    tp /= args->nPos;
    fn /= args->nPos;
    fp /= args->nNeg;
    
    p = tp / (tp+fp);
    r = tp / (tp+fn);
    
    if (p+r == 0)
      *(args->result) = 0;
    else
      *(args->result) = 2.0*p*r/(p+r);
  }
  SVL_LOG(SVL_LOG_VERBOSE, "Cross-validated gamma="<<args->gamma
	  <<", C="<<args->C << ", got F score of " << *(args->result));

  delete[] crossValidationResults;
  delete args;
  return NULL;
}


svlSVM::svlSVM() : _numFeatures(0), _svm(NULL), _predictNodes(NULL)
{
  // do nothing
}

svlSVM::~svlSVM()
{
  if (_svm)
    free(_svm);

  if (_predictNodes)
    delete [] _predictNodes;
}

double svlSVM::train(const MatrixXd & samples, const VectorXi & labels)
{
    vector<vector<double> > posSamples;
    vector<vector<double> > negSamples;
    
    for (int i = 0; i < samples.rows(); i++) {
        if (labels[i] > 0) {
            posSamples.push_back(vector<double>(samples.cols()));
            Eigen::Map<MatrixXd>(&posSamples.back()[0], 1, samples.cols()) = samples.row(i);
        } else {
            negSamples.push_back(vector<double>(samples.cols()));
            Eigen::Map<MatrixXd>(&negSamples.back()[0], 1, samples.cols()) = samples.row(i);
        }
    }

    return train(posSamples, negSamples);
}

// no longer shuffles the training data; can be done outside 
// (svm will put all same-classes training examples together anyway)
double svlSVM::train(const vector<vector<double> > &posSamples,
		    const vector<vector<double> > &negSamples)
{
  if (posSamples.size() == 0 || negSamples.size() == 0) {
    SVL_LOG(SVL_LOG_WARNING, "providing " << posSamples.size() 
	    << " positive and " << negSamples.size() << " negative examples;"
	    << " can't train SVM");
    return 0;
  }

  unsigned int numExamples = posSamples.size() + negSamples.size();
  unsigned int numPos = posSamples.size();
  _numFeatures = posSamples[0].size();

  //Set up a libSVM problem
  struct svm_problem problem;
  problem.l = numExamples;
  
  //Copy labels into the problem
  problem.y = new double[numExamples];
  SVL_ASSERT(problem.y);

  for (unsigned i = 0; i < numExamples; i++)
    problem.y[i] = (i < numPos) ? 1 : 0;

  //Copy feature vectors into the problem
  problem.x = new svm_node * [numExamples];
  SVL_ASSERT(problem.x);
  
  struct svm_node * nodes = new svm_node[numExamples*(_numFeatures+1)];
  
  unsigned int idx = 0;
  for (unsigned int i = 0; i < numExamples; i++)
    {
      problem.x[i] = nodes+idx;
      
      for (unsigned int j = 0; j < _numFeatures; j++)
	{
	  problem.x[i][j].index = j+1;
	  problem.x[i][j].value  = (i < numPos ? posSamples[i][j] : negSamples[i - numPos][j]);
 	}
      
      problem.x[i][_numFeatures].index = -1;
      idx += _numFeatures + 1;
    }

  bool bRBFkernel = !strNoCaseCompare(svlSVM::_defaultKernel, string("RBF"));
  bool bCrossValidate = _paramC.used || (bRBFkernel && _paramGamma.used);

  // run cross-validation if necessary
  double bestGamma = (_defaultGamma > 0) ? _defaultGamma : 1.0 / _numFeatures;
  double bestC = _defaultC;
  if (bCrossValidate) {
    double min_i = bestGamma;
    double max_i = bestGamma;
    double delta_i = 1.0;
    if (bRBFkernel && _paramGamma.used) {
      min_i = _paramGamma.min_value;
      max_i = _paramGamma.max_value;
      delta_i = _paramGamma.delta;
    }
    
    double min_j = bestC;
    double max_j = bestC;
    double delta_j = 1.0;
    if (_paramC.used) {
      min_j = _paramC.min_value;
      max_j = _paramC.max_value;
      delta_j = _paramC.delta;
    }
    
    int num_i = (int)floor(max_i - min_i + delta_i);
    int num_j = (int)floor(max_j - min_j + delta_j);
    int num_to_consider = num_i * num_j;
    
    SVL_ASSERT_MSG(num_to_consider > 1, "invalid ranges for cross-validation parameters"
        << " max_j: " << max_j << " min_j: " << min_j << " delta_j: " << delta_j
        << " max_i: " << max_i << " min_i: " << min_i << " delta_i: " << delta_i
        << " num_i: " << num_i << " num_j: " << num_j << " num_to_consider: " << num_to_consider);

    vector<double> Fscores(num_to_consider, -1);
    // just a way to keep track of the parameters corresponding to Fscores
    vector<pair<double, double> > paramValues(num_to_consider); 
    
    svlThreadPool vThreadPool(TOTAL_THREADS);
    vThreadPool.start();
    int index = 0;
    for (double i = min_i; i <= max_i; i+= delta_i) {
      for (double j = min_j; j <= max_j; j+= delta_j, index++) {
	double gamma = i;
	if (bRBFkernel && _paramGamma.used && !strNoCaseCompare(_paramGamma.type, string("POW")))
	  gamma = pow(2.0, i);
	
	double C = j;
	if (_paramC.used && !strNoCaseCompare(_paramC.type, string("POW")))
	  C = pow(2.0, j);
	
	paramValues[index] = make_pair<double, double>(gamma, C);
	
	CrossValidateArgs *args = new CrossValidateArgs(posSamples.size(), negSamples.size(),
							_defaultNumFolds, gamma, C, &problem,
							&(Fscores[index]));
	vThreadPool.addJob(crossValidate, args);					      
      }
    }
    vThreadPool.finish();
    
    double bestFscore = -1.0;
    
    for (index = 0; index < num_to_consider; index++) {
      if (Fscores[index] > bestFscore) {
	bestFscore = Fscores[index];
	bestGamma = paramValues[index].first;
	bestC = paramValues[index].second;
      }
    }
    SVL_ASSERT(bestFscore != -1.0);
    
    SVL_LOG(SVL_LOG_VERBOSE, "Finished cross-validating, chose gamma " << bestGamma << ", C " << bestC << " with F-score "<< bestFscore);
  } 

  //Having cross-validated, run on the best settings
  struct svm_parameter params;
  init_params(params, bestGamma, bestC, posSamples.size(), negSamples.size(), true);
  _svm = svm_train(&problem, &params);

  SVL_ASSERT(_svm);

  unsigned int numClasses = svm_get_nr_class(_svm);

  SVL_ASSERT(numClasses == 2);

  //Allocate space for holding feature vectors for prediction
  if (_predictNodes)
    delete _predictNodes;

  _predictNodes = new svm_node[_numFeatures+1];
  SVL_ASSERT(_predictNodes);
  for (unsigned int i = 0; i < _numFeatures; i++) {
      _predictNodes[i].index = i+1;
  }
  _predictNodes[_numFeatures].index = -1;
  
  //Track which index corresponds to the positive class
  if (problem.y[0])
    _posIdx = 0;
  else
    _posIdx = 1;

  return validate(posSamples, negSamples,"", _useNormedFScore);
}

double svlSVM::getProbability(const vector<double> &sample) const
{
  //Check that the SVM has been trained
  SVL_ASSERT(_svm);

  //Check that we got the right size of vector
  SVL_ASSERT_MSG(sample.size() == _numFeatures, "sample feature size: " << sample.size()
      << " numFeatures: " << _numFeatures);

  SVL_ASSERT(_predictNodes);

  //Copy the stl vector into libsvm format
  /*
  for (unsigned int i = 0; i < _numFeatures; i++)
    {
      _predictNodes[i].index = i+1;
      _predictNodes[i].value = sample[i];
    }
  _predictNodes[_numFeatures].index = -1;
  */
  static int hCopy = svlCodeProfiler::getHandle("getProbability::copy");
  svlCodeProfiler::tic(hCopy);
  for (int i = 0; i < _numFeatures/2; i++) {
      _predictNodes[2*i].value = sample[i*2];
      _predictNodes[2*i+1].value = sample[i*2+1];

  }
  svlCodeProfiler::toc(hCopy);
  
  if (_numFeatures % 2 != 0) {
      _predictNodes[_numFeatures-1].value = sample[_numFeatures-1];
  }
  
  static int h = svlCodeProfiler::getHandle("getProbability::svm_predict_probability");
  svlCodeProfiler::tic(h);

  //Get the predictive distribution
  svm_predict_probability(_svm, _predictNodes, _probs);
  
  svlCodeProfiler::toc(h);

  return _probs[_posIdx];
}

bool svlSVM::save(const char *filename) const
{
  FILE *fp = fopen(filename,"w");
  if (fp == NULL) return false;

  // the svmlib model reader will scan the file until it reaches the
  // end, and so we can't create a full XML wrapper around it, we just
  // create the header for compatibility with the svlFactory
  fprintf(fp, "%s\n", HEADER.c_str());

  fprintf(fp, "numFeatures %i\n", _numFeatures);
  fprintf(fp, "posIndex %i\n", _posIdx);

  if (svm_save(fp, _svm) == -1) return false;
  return true;
}

bool svlSVM::load(XMLNode& node)
{
    SVL_LOG(SVL_LOG_FATAL, "XML load not implemented for svlSVM");
    return false;
}


bool svlSVM::load(const char *filename)
{
  SVL_LOG(SVL_LOG_DEBUG, "Loading SVM");

  FILE *fp = fopen(filename, "r");
  if (fp == NULL) return false;
  
  char cmd[81];
  fgets(cmd, 80, fp);
  if (!strcmp(cmd, HEADER.c_str())) {
    SVL_LOG(SVL_LOG_WARNING, "Can't load SVM from " << filename
	    << ", header doesn't match " << HEADER);
    return false;
  }
  
  fscanf(fp, "%80s %i", cmd, &_numFeatures);
  if (strcmp(cmd, "numFeatures")) {
    SVL_LOG(SVL_LOG_WARNING, "Can't load SVM from " << filename << ", on command " << cmd << " with " << _numFeatures);
    return false;
  }

  fscanf(fp, "%80s %i", cmd, &_posIdx);
  if (strcmp(cmd, "posIndex")) {
    SVL_LOG(SVL_LOG_WARNING, "Can't load SVM from " << filename << ", on command " << cmd << " with " << _posIdx);
    return false;
  }

  _svm = svm_load(fp);

  if (_svm == NULL) return false;

  if (_predictNodes)
    delete[] _predictNodes;
  _predictNodes = new svm_node[_numFeatures+1];
  for (unsigned int i = 0; i < _numFeatures; i++) {
      _predictNodes[i].index = i+1;
  }
  _predictNodes[_numFeatures].index = -1;
  
  return true;
}


class svlSVMConfig : public svlConfigurableModule {
public:
    svlSVMConfig() : svlConfigurableModule("svlML.svlSVM") { }

    void usage(ostream &os) const {
        os << "      kernel    :: options: LINEAR (default), POLY, RBF\n"
           << "      degree    :: degree of POLY kernel (default: " 
           << svlSVM::_defaultDegree << ")\n"
           << "      epsilon   :: convergence criteria (default: " 
           << svlSVM::_defaultEps << ")\n"
           << "      posWeight :: weight for pos class (default: " 
           << svlSVM::_defaultPosWeight << ", < 0 means the total weight of pos."
	   << " and neg. examples will be the same)\n"
           << "      C         :: regularization parameter (default: " 
           << svlSVM::_defaultC << "; can be cross-validated from config file)\n"
           << "      gamma     :: width of RBF kernel (default: 1.0/numFeatures; " 
           << "can be cross-validated from config file)\n"
	   << "      numFolds  :: num folds for CV (default: " 
           << svlSVM::_defaultNumFolds << ")\n";
    }

  void readConfiguration(XMLNode &node) {
    svlConfigurableModule::readConfiguration(node);

    for (int i = 0; i < node.nChildNode("cvparam"); i++) {
      XMLNode child = node.getChildNode("cvparam", i);
      const char *name = child.getAttribute("name");
      SVL_ASSERT_MSG(name, "cvparam must have name specified");
      
      svlParamForCV *param;
      if (!strcmp(name, "C"))
	param = &(svlSVM::_paramC);
      else if (!strcmp(name, "gamma"))
	param = &(svlSVM::_paramGamma);
      else
	SVL_ASSERT_MSG(false, "Unknown cvparam name " << name);

      param->used = true;
      param->min_value = atof(child.getAttribute("min"));
      param->max_value = atof(child.getAttribute("max"));
      const char *tmp = child.getAttribute("type");
      if (tmp != NULL) param->type = string(tmp);
      tmp = child.getAttribute("delta");
      if (tmp != NULL) param->delta = atof(tmp);
    }
  }

    void setConfiguration(const char *name, const char *value) {
        // factor operation cache
        if (!strcmp(name, "kernel")) {
	  svlSVM::_defaultKernel = string(value);
	} else if (!strcmp(name, "degree")) {
            svlSVM::_defaultDegree = atoi(value);
        } else if (!strcmp(name, "epsilon")) {
            svlSVM::_defaultEps = atof(value);
        } else if (!strcmp(name, "posWeight")) {
            svlSVM::_defaultPosWeight = atof(value);
        } else if (!strcmp(name, "C")) {
            svlSVM::_defaultC = atof(value);
	} else if (!strcmp(name, "gamma")) {
            svlSVM::_defaultGamma = atof(value);
	} else if (!strcmp(name, "numFolds")) {
            svlSVM::_defaultNumFolds = atoi(value);
        } else {
            SVL_LOG(SVL_LOG_FATAL, "unrecognized configuration option for " << this->name());
        }
    }
};

static svlSVMConfig gSVMConfig;

/* The default checker works despite the model file not being a properly formatted XML */
string svlSVMFileChecker(const char * filepath)
{
    ifstream ifs;
    ifs.open(filepath, ios::binary);
    
    const char *header = svlSVM::HEADER.c_str();

    char buff[256];
    memset((void *)buff, 0, 256 * sizeof(char));
    ifs.read(buff, strlen(header));
    ifs.close();
    
    if (!strncmp(header, buff, strlen(header)))
        return "SVM";
    return "";
}

