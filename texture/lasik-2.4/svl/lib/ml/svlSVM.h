/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlSVM.h
** AUTHOR(S):   Ian Goodfellow <ia3n@cs.stanford.edu>
**              Ethan Dreyfuss <ethan@cs.stanford.edu>
**              Olga Russakovsky <olga@cs.stanford.edu>
** DESCRIPTION:
**  Wraps the libSVM class to incorporate it into the svlClassifier framework
**
*****************************************************************************/

#pragma once

//#include "svm.h"
#include "svlBase.h"
#include "svlBinaryClassifier.h"
#include <iostream>
#include <fstream> 
#include "libsvm/svm.h"

using namespace std;

// set through the configuration file
struct svlParamForCV {
  double min_value;
  double max_value;
  string type; // LIN or POW
  double delta;
  bool used; // 
  
  svlParamForCV() : min_value(0), max_value(-1), type("POW"), delta(1.0), used(false) {}
};

// TODO: check documentation to see if it's safe to free the
// training data from the svm_problem-- it should just need support
// vectors, and leaving entire training set allocated is quite a
// memory leak
class svlSVM : public svlBinaryClassifier
{
 public:
  
  svlSVM();
  virtual ~svlSVM();

  // training functions
  virtual double train(const vector<vector<double> >& posSamples,
      const vector<vector<double> >&negSamples);
  virtual double train(const MatrixXd & samples, const VectorXi & labels);

      // from svlClassifier
    virtual double train(const MatrixXf & X, const VectorXi & Y) { return svlBinaryClassifier::train(X, Y); }
    virtual double train(const MatrixXd & samples, const VectorXi & labels, const VectorXd & weights) {
        return svlBinaryClassifier::train(samples, labels, weights); }
    virtual double train(const vector<vector<double> >&samples, const vector<int> &labels, const vector<double> &weights) {
        return svlBinaryClassifier::train(samples, labels, weights); }
    virtual double train(const vector<vector<double> >& samples, const vector<int> &labels) {
        return svlBinaryClassifier::train(samples, labels);
    }

  // scoring functions
  double getScore(const VectorXd  &sample) const {
    vector<double> d(sample.rows());
    Eigen::Map<VectorXd>(&d[0], d.size()) = sample;
    return probabilityToScore(getProbability(d));
  }
  double getScore(const vector<double> &sample) const {
    return probabilityToScore(getProbability(sample));
  }

  //  void getScores(const vector<vector<double> > &samples, vector<double>& scores) const;

  double getProbability(const vector<double> &sample) const;

  //  void getProbability(const vector<vector<double> > &samples, vector<double>& probs) const;

  bool save(const char *filename) const;
  bool load(XMLNode& node); // asserts false
  bool load(const char *filename);

 protected:
  unsigned int _numFeatures;
  svm_model * _svm;
  svm_node * _predictNodes;
  unsigned int _posIdx;
  
  mutable double _probs[2];

 public:
  static string _defaultKernel;
  static int _defaultDegree;
  static double _defaultEps;
  static double _defaultPosWeight;
  static int _defaultNumFolds;
  static double _defaultC;
  static svlParamForCV _paramC;
  static double _defaultGamma;
  static svlParamForCV _paramGamma;
  static string HEADER;
};

SVL_AUTOREGISTER_H ( SVM, svlClassifier );

string svlSVMFileChecker(const char * filepath);
SVL_AUTOREGISTER_FILECHECKER_H ( svlSVMFileChecker, svlClassifier);
