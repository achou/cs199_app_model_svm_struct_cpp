/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCodebook.h
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
** DESCRIPTION:
**  Implements a generic codebook.
**
*****************************************************************************/

#pragma once

#include <vector>

#include <cxcore.h>

#include "svlBase.h"

using namespace std;

// svlCodebook class ---------------------------------------------------------

class svlCodebook {
    int _n;           // size of codebook
    int _m;           // dimension of codeword
    CvMat *_codes;    // representative codes

 public:
    svlCodebook(const CvMat *codes = NULL);
    svlCodebook(const svlCodebook& book);
    virtual ~svlCodebook();

    // size
    inline int size() const { return _n; }
    inline int dimension() const { return _m; }

    // initialization
    void initialize(int n, int m);
    void initialize(const CvMat* codes);
    
    // i/o
    bool save(const char *filename) const;
    bool load(const char *filename);
    bool save(ofstream &ofs) const;
    bool load(istream &ifs);

    // learning (each row represents a codeword)
    virtual void learn(const vector<vector<double> >& samples, int n = -1,
        bool bCodeMustBeASample = false);
    virtual void learn(const CvMat *samples, int n = -1,
        bool bCodeMustBeASample = false);

    // evaluatation
    virtual int encode(double x) const;
    virtual int encode(const CvMat *x) const;
    virtual int encode(const vector<double>& x) const;
    void decode(int indx, double& x) const;
    void decode(int indx, CvMat *x) const;
    void decode(int indx, vector<double>& x) const;

    void append(const svlCodebook &book);
};



