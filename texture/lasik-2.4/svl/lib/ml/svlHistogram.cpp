/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlHistogram.cpp
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
** DESCRIPTION:
**
*****************************************************************************/

#include "svlHistogram.h"

#define HISTOGRAM_ELEM(i) _values->at(i+_offset)
#define HISTOGRAM_2D_ELEM(i, j) HISTOGRAM_ELEM(i*_numBins2 + j + _offset)

using namespace std;

// svlHistogram -----------------------------------------------------------------

svlHistogram::svlHistogram(unsigned numBins, bool inclusive) :
  _ops(numBins, 0, numBins, inclusive),
  _numValues(inclusive ? (numBins + 1) : numBins),
  _ownsValues(true),
  _values(new vector<double>(_numValues, 0.0)),
  _offset(0)  
{
  // do nothing
}

svlHistogram::svlHistogram(unsigned numBins, double minValue, double maxValue, bool inclusive) :
  _ops(numBins, minValue, maxValue, inclusive),
  _numValues(inclusive ? (numBins + 1) : numBins),
  _ownsValues(true),
  _values(new vector<double>(_numValues, 0.0)),
  _offset(0)
{
  // do nothing
}

svlHistogram::svlHistogram(unsigned numBins, vector<double> *v, unsigned offset) :
  _ops(numBins, 0, numBins),
  _numValues(numBins),
  _ownsValues(false)
{
  set(v, offset);
}

svlHistogram::svlHistogram(unsigned numBins, double minValue, double maxValue, 
			   vector<double> *v, unsigned offset) :
  _ops(numBins, minValue, maxValue),
  _numValues(numBins),
  _ownsValues(false)
{
  set(v, offset);
}

svlHistogram::svlHistogram(const svlHistogram &h) :
  _ops(h._ops), _numValues(h._numValues), _ownsValues(h._ownsValues),
  _values(h._values),  _offset(h._offset)
{
  if (_ownsValues) {
    _values = new vector<double>(_numValues);
    for(unsigned i = 0; i < _numValues; i++)
      _values->at(i) = h._values->at(i);
  } 
}

svlHistogram::~svlHistogram()
{
  if (_ownsValues && _values)
    delete _values;
}

void svlHistogram::set(vector<double> *fv, unsigned offset)
{
  if (_ownsValues && _values)
    delete _values;

  _ownsValues = false;
  _values = fv;
  _offset = offset;
  if (_values->size() < _numValues + _offset)
    _values->resize(_numValues + _offset);
  clear();
}

bool svlHistogram::insert(double binVal, double weight)
{
  SVL_ASSERT_MSG(_values, "storage for the feature vector must be provided");
  int bin = _ops.getBinNum(binVal);
  if (bin < 0) return false;
  HISTOGRAM_ELEM(bin) += weight;
  return true;
}

bool svlHistogram::insert(const vector<double> &binValues)
{
  SVL_ASSERT_MSG(_values, "storage for the feature vector must be provided");

  for (unsigned i = 0; i < binValues.size(); i++) {
    int bin = _ops.getBinNum(binValues[i]);
    if (bin < 0) return false;
    HISTOGRAM_ELEM(bin) += 1.0;
  }

  return true;
}

bool svlHistogram::insertLinearSmooth(double binVal, double weight)
{
  SVL_ASSERT_MSG(_values, "storage for the feature vector must be provided");
  svlSoftBinAssignment sb;
  if (!_ops.getBinNumLinearSmooth(binVal, sb)) return false;

  HISTOGRAM_ELEM(sb._bin[0]) += weight * sb._binWeight[0];
  HISTOGRAM_ELEM(sb._bin[1]) += weight * sb._binWeight[1];
  return true;
}

void svlHistogram::clear()
{
  SVL_ASSERT_MSG(_values, "storage for the histogram must be provided");
  for (unsigned i = 0; i < _numValues; i++)
    HISTOGRAM_ELEM(i) = 0.0;
}

void svlHistogram::normalize()
{
  SVL_ASSERT_MSG(_values, "storage for the histogram must be provided");
  double sum = 0.0;
  for (unsigned i = 0; i < _numValues; i++)
    sum += HISTOGRAM_ELEM(i);

  if (abs(sum) < 1e-20) {
    SVL_LOG(SVL_LOG_WARNING, "histogram has sum close to 0, refusing to normalize");
    return;
  }

  for (unsigned i = 0; i < _numValues; i++)
    HISTOGRAM_ELEM(i) /= sum;
}

void svlHistogram::truncate(double val)
{
  SVL_ASSERT_MSG(_values, "storage for the histogram must be provided");
  for (unsigned i = 0; i < _numValues; i++) {
    if (HISTOGRAM_ELEM(i) > val)
      HISTOGRAM_ELEM(i) = val;
  }
}

// svl2dHistogram -----------------------------------------------------------------

svl2dHistogram::svl2dHistogram(unsigned numBins1, unsigned numBins2) :
  _ops1(numBins1, 0, numBins1),
  _ops2(numBins2, 0, numBins2),
  _numBins2(numBins2),
  svlHistogram(numBins1*numBins2)
{
  // do nothing
}

svl2dHistogram::svl2dHistogram(unsigned numBins1, double minValue1, double maxValue1,
			       unsigned numBins2, double minValue2, double maxValue2) :
  _ops1(numBins1, minValue1, minValue2),
  _ops2(numBins2, minValue2, maxValue2),
  _numBins2(numBins2),
  svlHistogram(numBins1*numBins2)
{
  // do nothing
}

svl2dHistogram::svl2dHistogram(unsigned numBins1, unsigned numBins2, 
			       vector<double> *v, unsigned offset) :
  _ops1(numBins1, 0, numBins1),
  _ops2(numBins2, 0, numBins2),
  _numBins2(numBins2),
  svlHistogram(numBins1*numBins2, v, offset)
{
  // do nothing
}

svl2dHistogram::svl2dHistogram(unsigned numBins1, double minValue1, double maxValue1,
			       unsigned numBins2, double minValue2, double maxValue2,
			       vector<double> *v, unsigned offset) :
  _ops1(numBins1, minValue1, maxValue1),
  _ops2(numBins2, minValue2, maxValue2),
  _numBins2(numBins2),
  svlHistogram(numBins1*numBins2, v, offset)
{

}

bool svl2dHistogram::insert(double binVal1, double binVal2, double weight)
{
  SVL_ASSERT_MSG(_values, "storage for the feature vector must be provided");
  int bin1 = _ops1.getBinNum(binVal1);
  if (bin1 < 0) return false;
  int bin2 = _ops2.getBinNum(binVal2);
  if (bin2 < 0) return false;

  HISTOGRAM_2D_ELEM(bin1, bin2) += weight;
  return true;
}

bool svl2dHistogram::insertLinearSmooth(double binVal1, double binVal2,
					double weight)
{
  SVL_ASSERT_MSG(_values, "storage for the feature vector must be provided");
  svlSoftBinAssignment sb1;
  if (!_ops1.getBinNumLinearSmooth(binVal1, sb1)) return false;

  svlSoftBinAssignment sb2;
  if (!_ops2.getBinNumLinearSmooth(binVal2, sb2)) return false;

  // insert values into all of the four possible bins
  for (unsigned i = 0; i < 2; i++) {
    for (unsigned j = 0; j < 2; j++) {
      double w = weight * sb1._binWeight[i] * sb2._binWeight[j];
      HISTOGRAM_2D_ELEM(sb1._bin[i], sb2._bin[j]) += w;
    }
  }

  return true; 
}

bool svl2dHistogram::insertLinearSmooth(svlSoftBinAssignment sb1, double binVal2,
					double weight)
{
  SVL_ASSERT_MSG(_values, "storage for the feature vector must be provided");

  svlSoftBinAssignment sb2;
  if (!_ops2.getBinNumLinearSmooth(binVal2, sb2)) return false;
  
  // insert values into all of the four possible bins
  for (unsigned i = 0; i < 2; i++) {
    for (unsigned j = 0; j < 2; j++) {
      double w = weight * sb1._binWeight[i] * sb2._binWeight[j];
      HISTOGRAM_2D_ELEM(sb1._bin[i], sb2._bin[j]) += w;
    }
  }

  return true;
}

