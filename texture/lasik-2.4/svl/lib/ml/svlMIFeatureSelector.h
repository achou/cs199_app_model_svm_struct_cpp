/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlMIFeatureSelector.h
** AUTHOR(S):   Olga Russakovsky <olga@cs.stanford.edu>
**
** DESCRIPTION:
**  Implements feature selection using mutual information.
**     Michel Vidal-Naquet and Shimon Ullman. Object Recognition with
**     Informative Features and Linear Classification, ICCV 2003
**
*****************************************************************************/

#pragma once

#include <cassert>
#include <iostream>
#include <vector>
#include <map>

#include "cv.h"
#include "cxcore.h"
#include "ml.h"

#include "svlBase.h"
#include "svlPRcurve.h"
#include "svlFeatureSelector.h"
using namespace std;

// svlFeatureResponse ------------------------------------------------

// encompasses the mutual information computations between a given feature
// and the class label

class svlFeatureResponse : public svlClassifierSummaryStatistics {
 private:
  // needed to keep the original order of provided examples
  vector<pair<double, bool> > _responses;
 
  // after binarization, _cache[0][0] contains a list of all example indices
  // where the feature didn't fire and the example was negative; _cache[0][1]
  // are indices of examples where the feature didn't fire and the example was
  // positive, etc.
  vector<int> _cache[2][2];

 public:
  void insert(double score, bool positive);

  // Computes the threshold at which the mutual information between this
  // feature and the class label is the highest
  void findHighestMI(double &bestMI, double &bestThreshold) const;

  // Thresholds all responses to be binary, i.e. corresponding to
  // 1[responses > threshold], and caches them (see _cache above)
  // - should be called before computeJointMI
  void binarizeFeature(double threshold);

  // Computes the I(X, Y; C), i.e. the joint mutual information of the two features
  // - assumes that both features have been binarized to achieve the maximum
  // mutual information between the feature and the class
  // - assumes that the examples were provided in the same order, so the indices
  // match up correctly
  double computeJointMI(const svlFeatureResponse &feature) const;
};

// svlMIFeatureSelector ------------------------------------------------------

// uses joint mutual information to select a subset of informative features
// from the given set

class svlMIFeatureSelector : public svlFeatureSelector {
 public:
  static double MI_THRESHOLD;

 protected:
  // posExampleResponses[i][j] is the response of feature j on the i^th positive
  // training example  
  bool cacheExamples(const vector<vector<double> > &posExampleResponses,
		     const vector<vector<double> > &negExampleResponses);

  // returns a list of indices corresponding to features to keep
  bool chooseFeatures(vector<bool> &selectedFeatures);

 private:
  vector<svlFeatureResponse> _features;
  vector<double> _MI;
};
