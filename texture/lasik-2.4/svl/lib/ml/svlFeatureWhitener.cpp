/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlFeatureWhitener.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**
*****************************************************************************/

#include <vector>
#include <limits>

#include "../base/svlCompatibility.h"

#include <cassert>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <limits>
#include <cmath>

#include "svlBase.h"
#include "svlSufficientStats.h"
#include "svlFeatureWhitener.h"

using namespace std;

// svlFeatureWhitener class --------------------------------------------------

svlFeatureWhitener::svlFeatureWhitener(int n) :
    _n(n)
{
    SVL_ASSERT(_n > 0);
    _mu.resize(_n, 0.0);
    _beta.resize(_n, 1.0);
}

svlFeatureWhitener::svlFeatureWhitener(const svlFeatureWhitener& fw) :
    _n(fw._n), _mu(fw._mu), _beta(fw._beta)
{
    // do nothing
}

svlFeatureWhitener::svlFeatureWhitener(const svlSufficientStats& stats) :
    _n(stats.size())
{
    SVL_ASSERT(_n > 0);
    train(stats);
}
 
svlFeatureWhitener::~svlFeatureWhitener()
{
    // do nothing
}

void svlFeatureWhitener::clear()
{
    fill(_mu.begin(), _mu.end(), 0.0);
    fill(_beta.begin(), _beta.end(), 1.0);
}

void svlFeatureWhitener::clear(int n)
{
    SVL_ASSERT(n > 0);
    _n = n;
    _mu.resize(_n);
    _beta.resize(_n);
    clear();
}

// i/o
void svlFeatureWhitener::save(const char *filename) const
{
    SVL_ASSERT(filename != NULL);
    ofstream ofs(filename);
    SVL_ASSERT_MSG(!ofs.fail(), filename);    
    this->save(ofs);
    ofs.close();
}

void svlFeatureWhitener::save(ostream& os) const
{
    os << _n << "\n";
    for (int i = 0; i < _n; i++) {
        os << _mu[i] << " " << _beta[i] << "\n";
    }
}

void svlFeatureWhitener::load(const char *filename)
{
    SVL_ASSERT(filename != NULL);
    ifstream ifs(filename);
    SVL_ASSERT_MSG(!ifs.fail(), filename);    
    this->load(ifs);
    ifs.close();
}

void svlFeatureWhitener::load(istream& is)
{
    is >> _n;
    SVL_ASSERT(_n > 0);
    clear(_n);    
    for (int i = 0; i < _n; i++) {
        is >> _mu[i] >> _beta[i];
    }
    SVL_ASSERT(!is.fail());
}
    
// in-place evaluation
void svlFeatureWhitener::evaluate(vector<double> &x) const
{
    SVL_ASSERT_MSG((int)x.size() == _n, x.size() << "!=" << _n);
#if 0
    for (int i = 0; i < _n; i++) {
        x[i] = _beta[i] * (x[i] - _mu[i]);
    }
#else
    for (int i = 0; i < _n - 1; i += 2) {
        x[i] = _beta[i] * (x[i] - _mu[i]);
        x[i + 1] = _beta[i + 1] * (x[i + 1] - _mu[i + 1]);
    }

    if (_n % 2 == 1) {
        x[_n - 1] = _beta[_n - 1] * (x[_n - 1] - _mu[_n - 1]);
    }
#endif
}

void svlFeatureWhitener::evaluate(vector<vector<double> >& x) const
{
    for (int i = 0; i < (int)x.size(); i++) {
        evaluate(x[i]);
    }
}

void svlFeatureWhitener::evaluate(vector<VectorXd *>& x) const
{
    vector<double> t(_n);
    for (int i = 0; i < (int)x.size(); i++) {
        if (x[i] == NULL) continue;
        Eigen::Map<VectorXd>(&t[0], _n) = *x[i];
        evaluate(t);
        *x[i] = Eigen::Map<VectorXd>(&t[0], _n);
    }
}

// training
void svlFeatureWhitener::train(const vector<vector<double> >& x)
{
    svlSufficientStats stats(_n, SVL_PSS_DIAG);
    stats.accumulate(x, 1.0);
    this->train(stats);
}

void svlFeatureWhitener::train(const vector<VectorXd *>& x)
{
    svlSufficientStats stats(_n, SVL_PSS_DIAG);
    vector<double> t(_n);
    for (int i = 0; i < (int)x.size(); i++) {
        if (x[i] == NULL) continue;
        Eigen::Map<VectorXd>(&t[0], _n) = *x[i];
        stats.accumulate(t, 1.0);
    }
    this->train(stats);
}

void svlFeatureWhitener::train(const vector<double> &x)
{
    SVL_ASSERT(_n == 1);
    
    _mu[0] = 0.0;
    double sigma = 0.0;
    for (int i = 0; i < (int)x.size(); i++) {
        _mu[i] += x[i];
        sigma += x[i] * x[i];
    }

    _mu[0] /= (double)(x.size() + SVL_DBL_MIN);
    sigma = sigma / (double)(x.size() + SVL_DBL_MIN) - _mu[0] * _mu[0];
    if (sigma < SVL_DBL_MIN) {
        _beta[0] = (fabs(_mu[0]) > SVL_DBL_MIN) ? (1.0 / _mu[0]) : 1.0;
        _mu[0] = 0.0;
    } else {
        _beta[0] = 1.0 / sqrt(sigma);
    }
}


void svlFeatureWhitener::train(const svlSufficientStats& stats)
{
    SVL_ASSERT(_n == stats.size());
    
    for (int i = 0; i < _n; i++) {
        _mu[i] = stats.sum(i) / (stats.count() + SVL_DBL_MIN);
        double sigma = stats.sum2(i, i) / (stats.count() + SVL_DBL_MIN) -
            _mu[i] * _mu[i];
        if (sigma < SVL_DBL_MIN) {
            _beta[i] = (fabs(_mu[i]) > SVL_DBL_MIN) ? (1.0 / _mu[i]) : 1.0;
            _mu[i] = 0.0;
        } else {
            _beta[i] = 1.0 / sqrt(sigma);
        }
    }
}

// standard operators
svlFeatureWhitener& svlFeatureWhitener::operator=(const svlFeatureWhitener& fw)
{
    _n = fw._n;
    _mu = fw._mu;
    _beta = fw._beta;

    return *this;
}

