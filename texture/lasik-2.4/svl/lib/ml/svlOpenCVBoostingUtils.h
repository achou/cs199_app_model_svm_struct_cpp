/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlOpenCVBoostingUtils.h
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**              Olga Russakovsky <olga@cs.stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
** DESCRIPTION:
**  OpenCV type conversion utility functions for boosting. In the future
** should hopefully not depend on openCV at all. 
**
*****************************************************************************/

#pragma once

#include <vector>
#include <iostream>
#include <sstream>

#include "cv.h"
#include "cxcore.h"
#include "ml.h"

#include "svlBase.h"

// These functions are used by svlBoostedClassifier and svlBoostedClassifierSet

CvMat *createOpenCVFeatureVector(const vector<double> &sample);
CvMat *createOpenCVFeatureVector(const VectorXd &sample);

void allocateOpenCVTrainingData(int numSamples, int numFeatures,
				CvMat ** data, CvMat *&labels, CvMat *&varType);
void releaseOpenCVTrainingData(CvMat *&data,
			       CvMat *&labels, CvMat *&varType);

// can be called multiple times with different sets of training examples
void fillInOpenCVTrainingData(const vector<vector<double> > &x,
			      CvMat *data, bool quiet, int offset = 0);

// returns the number of positive examples
int fillInOpenCVTrainingLabels(const vector<int> &y, CvMat *labels, int posLabel);

// assumes the first batch of examples will be positive
// and everything from then onwards is negative
int fillInOpenCVTrainingLabels(int numPositiveSamples, CvMat *labels);
int fillInOpenCVTrainingLabels(const VectorXi & Y, CvMat *labels);

CvMat * svlCreateEmptyCvMat(int height, int width, int type);
