/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlBoostedClassifier.h
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**              Olga Russakovsky <olga@cs.stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
** DESCRIPTION:
**  Implements a single binary boosted classifier. Uses options:
**    boostMethod    : boosting method: GENTLE, DISCRETE, etc.
**    boostingRounds : number of rounds of boosting
**    numSplits      : number of decision tree splits
**    trimRate       : boosting trim rate
**    pseudoCounts   : Dirichlet prior pseudo-counts
**
*****************************************************************************/

#pragma once

#include <vector>

#include "cv.h"
#include "cxcore.h"
#include "ml.h"

#include "svlBase.h"
#include "svlML.h"
#include "svlPRcurve.h"
#include "svlOpenCVBoostingUtils.h"
#include "svlBinaryClassifier.h"

using namespace std;

// svlBoostedClassifier class ---------------------------------------------

class svlBoostedClassifier : public svlBinaryClassifier {
    friend class svlBoostedClassifierSet;
    friend class svlBoostedClassifierConfig;

 private:
    // the number of weak classifiers considered by the
    // getScore function can be restricted if necessary
    // (more efficient than accessing the svlOptions every
    // time)
    // TODO: this should be modified using the optionUpdated function
    int _numWeakToConsider;

 protected:
    svlSmartPointer<CvBoost> _model;

    // used in svlBoosedClassifierSet as well
    static string defaultBoostMethod;
    static int defaultBoostingRounds;
    static double defaultTrimRate;
    static int defaultNumSplits;
    static int defaultPseudoCounts;
    static bool defaultQuietTraining;
    static bool bDetectOpenCVErrors;

 public:
    svlBoostedClassifier();
    svlBoostedClassifier(const char *filename);
    svlBoostedClassifier(const svlBoostedClassifier& bc);
    virtual ~svlBoostedClassifier();

    bool load(const char * filename);

    // required virtual functions from svlClassifier
    bool save(const char *filename) const;
	bool save(XMLNode &node) const;
    bool load(XMLNode& node);
    double train(const MatrixXd & X, const VectorXi & Y);
    double train(const MatrixXf & X, const VectorXi & Y);

    // required virtual functions from svlBinaryClassifier
    double train(const vector<vector<double> >& posSamples,
		 const vector<vector<double> >&negSamples);
    double getScore(const VectorXd  &x) const;
    double getScore(const vector<double> &sample) const {
        return svlBinaryClassifier::getScore(sample);
    }

    // training code specific to this classifier type
    // (previously in svlSlidingWindowDetector)
    void trainCV(const vector<vector<double> >& posSamples,
		 const vector<vector<double> >& negSamples,
		 const vector<vector<double> >& posCVsamples,
		 const vector<vector<double> >& negCVsamples,
		 bool retrain = true);
    void trainFullCV(const vector<vector<double> >& posSamples,
		     const vector<vector<double> >& negSamples,
		     int kfold);

    const CvMat *getActiveVars();

    // reorder the variables in the model and return the new mapping
    vector<int> reorderActiveVars();

 protected:
    void trainUsingOpenCVData(CvMat *data, CvMat *labels, CvMat *varType, int format = CV_ROW_SAMPLE);

};

string svlCvBoostFileChecker(const char * filepath);

SVL_AUTOREGISTER_H ( CVBOOST, svlClassifier );
SVL_AUTOREGISTER_FILECHECKER_H ( svlCvBoostFileChecker, svlClassifier);


