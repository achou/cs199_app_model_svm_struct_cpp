/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlDecisionTreeSet.cpp
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
**
*****************************************************************************/

#include "svlDecisionTreeSet.h"


// Destructor
svlDecisionTreeSet::~svlDecisionTreeSet()
{
	for ( vector<svlDecisionTree*>::iterator t=_trees.begin(); t!=_trees.end(); ++t )
		delete *t;
}


// Save to an XML node.
bool svlDecisionTreeSet::save(XMLNode &node) const
{
	return false; // Pawned.	
}


// Load from an XML node.
bool svlDecisionTreeSet::load(XMLNode &node)
{
	if ( strcmp(node.getAttribute("id"),"DecisionTreeSet") != 0 ) {
		SVL_LOG(SVL_LOG_FATAL, "Improper classifier id" << node.getAttribute("id"));
		return false;
	}
	if ( !this->svlClassifier::load(node) )
        return false;

	// Read root-level attributes.
	const char *c;
	if ( (c=node.getAttribute("numTrees")) )
		_nTrees = atoi(c);
	else
		SVL_LOG(SVL_LOG_FATAL, "Missing attribute numTrees");

	if ( (c=node.getAttribute("MinVal")) )
		_minVal = atof(c);
	else
		SVL_LOG(SVL_LOG_FATAL, "Missing attribute MinVal");

	if ( (c=node.getAttribute("MaxVal")) )
		_maxVal = atof(c);
	else
		SVL_LOG(SVL_LOG_FATAL, "Missing attribute MaxVal");

	initialize(_nFeatures, _nClasses);

	if ( _nTrees != node.nChildNode()  ) {
		SVL_LOG(SVL_LOG_FATAL, "Mismatched number of child nodes and trees");
		return false;
	}
	// Parse child trees.
	_trees.clear();
	_weights.clear();
	_trees.reserve(_nTrees);
	_weights.reserve(_nTrees);
	for ( int i=0; i<_nTrees; ++i ) {
		svlDecisionTree *t = new svlDecisionTree();
		// Read tree and save.
		XMLNode n = node.getChildNode(i);
		t->load(n);
		_trees.push_back(t);
		// Read weight.
		_weights.push_back(atof(n.getAttribute("weight")));
	}

	return true;
}


