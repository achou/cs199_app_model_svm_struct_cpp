/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaMath.h
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  Cuda math functions.
**
*****************************************************************************/

#pragma once

#ifndef __SVL_CUDA_MATH_H
#define __SVL_CUDA_MATH_H

#include <iostream>
#include <vector>

#include "cv.h"
#include "cxcore.h"

#include "svlCudaCommon.h"
#include "svlCudaMemHandler.h"
using namespace std;

class svlCudaMath {
public:
	svlCudaMath();
	virtual ~svlCudaMath();

	// *** Functions VERIFIED in cudaVerify app ***

	// Elementary math operations:
	// Add a scaled pitch to another: <a += fac*b;>.
	inline static void add(svlCudaPitch *a, const svlCudaPitch *b, float fac = 1.0f, int img_w = -1, int img_h = -1) {
		svlCudaAdd(a, b, fac, img_w, img_h);
	}
	// <out = fac_a*a + fac_b*b;>
	inline static void add(svlCudaPitch *out, const svlCudaPitch *a, const svlCudaPitch *b, float fac_a = 1.0f, float fac_b = 1.0f, int img_w = -1, int img_h = -1) {
		svlCudaAdd3(out, a, b, fac_a, fac_b, img_w, img_h);
	}
	// Multiply two pitches with a factor: <a *= fac*b;>.
	inline static void mult(svlCudaPitch *a, const svlCudaPitch *b, float fac = 1.0f, int img_w = -1, int img_h = -1) {
		svlCudaMult(a, b, fac, img_w, img_h);
	}
	// <out = fac*a*b;>
	inline static void mult(svlCudaPitch *out, const svlCudaPitch *a, const svlCudaPitch *b, float fac = 1.0f, int img_w = -1, int img_h = -1) {
		svlCudaMult3(out, a, b, fac, img_w, img_h);
	}
	// Divide one pitch by another with a factor: <a *= fac/b;>.
	inline static void div(svlCudaPitch *a, const svlCudaPitch *b, float fac = 1.0f, int img_w = -1, int img_h = -1) {
		svlCudaDiv(a, b, fac, img_w, img_h);
	}
	// <out = fac*a/b;>
	inline static void div(svlCudaPitch *out, const svlCudaPitch *a, const svlCudaPitch *b, float fac = 1.0f, int img_w = -1, int img_h = -1) {
		svlCudaDiv3(out, a, b, fac, img_w, img_h);
	}

	// Scale and offset a pitch.
	inline static void scaleOffset(svlCudaPitch *a, float scale = 1.0f, float offset = 0.0f, int img_w = -1, int img_h = -1) {
		svlCudaScaleOffset(a, scale, offset, img_w, img_h);
	}

	// Subsample a in x and y, save to b.
	inline static void decimate(const svlCudaPitch *a, svlCudaPitch *b, int delta_x = 2, int delta_y = 2, int img_w = -1, int img_h = -1) {
		svlCudaDecimate(a, b, delta_x, delta_y, img_w, img_h);
	}
	// Add in quadrature: a <- sqrt( a.^2 + b.^2 );
	inline static void addQuadrature(svlCudaPitch *a, const svlCudaPitch *b, int img_w = -1, int img_h = -1) {
		svlCudaAddQuadrature(a, b, img_w, img_h);
	}

	// Min/max operations:
	// Compute min and max in all 4-by-4 windows of pitch_in.
	inline static void minMax4x4(const svlCudaPitch *pitch_in, svlCudaPitch *min_out = NULL, svlCudaPitch *max_out = NULL, int img_w = -1, int img_h = -1) {
		svlCudaMinMax4x4(pitch_in, min_out, max_out, img_w, img_h);
	}
	// Calculate min and max over 32-by-32 windows.
	inline static void minMax32x32(const svlCudaPitch *pin, svlCudaPitch *temp, svlCudaPitch *min_out = NULL, svlCudaPitch *max_out = NULL, int img_w = -1, int img_h = -1) {
		svlCudaMinMax32x32(pin, temp, min_out, max_out, img_w, img_h);
	}
	// Lasik-correct max window.
	inline static void lasikCorrectedMax(const svlCudaPitch *conv, const svlCudaPitch *sum, const svlCudaPitch *sum2, const svlCudaPitch *correct,
		svlCudaPitch *maxres, float N, float patchSum, float patchNorm, int max_w, int max_h, int img_w = -1, int img_h = -1) {
		svlCudaLasikCorrectedMax(conv, sum, sum2, correct, maxres, N, patchSum, patchNorm, max_w, max_h, img_w, img_h);
	}
	// 4-point max.
	inline static void fourPointMax(const svlCudaPitch *pin, svlCudaPitch *pout, int max_w, int max_h, int img_w = -1, int img_h = -1) {
		svlCudaFourPointMax(pin, pout, max_w, max_h, img_w, img_h);
	}

	// Integration functions:
	// Integrals over a line.
	inline static void integralLine(const svlCudaLine *n, svlCudaLine *integ, int w = -1, int dim = 128, bool do_cumsum = false, int stride = -1) {
		svlCudaIntegralLine(n, integ, w, dim, do_cumsum, stride);
	}
	// Plain integral image.
	inline static void integral(const svlCudaPitch *pin, svlCudaPitch *integ, svlCudaPitch *tempint, svlCudaPitch *integ2 = NULL, svlCudaPitch *tempint2 = NULL, int img_w = -1, int img_h = -1) {
		svlCudaIntegral(pin, integ, tempint, integ2, img_w, img_h);
	}
	// Blockwise integration.
	inline static void integralBlock(const svlCudaPitch *pin, svlCudaPitch *integ, svlCudaPitch *tempint, svlCudaPitch *integ2 = NULL, svlCudaPitch *tempint2 = NULL, int img_w = -1, int img_h = -1, int dim = 128) {
		svlCudaIntegralBlock(pin, integ, tempint, integ2, tempint2, img_w, img_h, dim);
	}
	// Row- and block-wise integration.
	inline static void rowIntegralBlock(const svlCudaPitch *pin, svlCudaPitch *integ, svlCudaPitch *integ2 = NULL, int img_w = -1, int img_h = -1, int dim = 128) {
		svlCudaRowIntegralBlock(pin, integ, integ2, img_w, img_h, dim);
	}

	// Mean functions.
	inline static void mean32x32(const svlCudaPitch *pin, svlCudaPitch *mean, svlCudaPitch *temp, int img_w = -1, int img_h = -1) {
		svlCudaMean32x32(pin, mean, temp, img_w, img_h);
	}


	// *** UNVERIFIED or old functions ***

	// Elementary math operations:
	// Add a scaled pitch to another with an offset on b: <a += fac*b[+w,+h];>.
	inline static void addOffset(svlCudaPitch *a, const svlCudaPitch *b, float fac = 1.0f, int img_w = -1, int img_h = -1, int off_x = 0, int off_y = 0) {
		svlCudaAddOff(a, b, fac, img_w, img_h, off_x, off_y);
	}

	// Do max operation over variable width and height (only correct for width and height <= 16).
	inline static void maxWindow(svlCudaPitch *pitch_in, svlCudaPitch *pitch_out,
		int max_w = 7, int max_h = 7, int img_w = -1, int img_h = -1, svlCudaCoveringGrid *cudagrid = NULL) {
		svlCudaMax(*pitch_in, *pitch_out, max_w, max_h, img_w, img_h, cudagrid);
	}
	// Compute max in all 7-by-7 windows of pitch_in.
	inline static void max7x7(svlCudaPitch *pitch_in, svlCudaPitch *pitch_out, int img_w = -1, int img_h = -1, svlCudaCoveringGrid *cudagrid = NULL) {
		svlCudaMax7x7(*pitch_in, *pitch_out, img_w, img_h, cudagrid);
	}
	// Compute max in all 7-by-7 (centered) windows of pitch_in.
	inline static void max7x7Cent(svlCudaPitch *pitch_in, svlCudaPitch *pitch_out, int img_w = -1, int img_h = -1) {
		svlCudaMax7x7Cent(*pitch_in, *pitch_out, img_w, img_h);
	}

	// Median selection.
	inline static void median8U_1024(svlCudaLine *ln, svlCudaLine *lout, int w = -1) {
		svlCudaMedian8U_1024(ln, lout, w);
	}
	inline static void windowMedians8U(svlCudaPitch* pin, svlCudaLine *ixes, svlCudaLine *medians, int n = -1, bool yx = false) {
		svlCudaWindowMedians8U(pin, ixes, medians, n, yx);
	}
	// Quantized floating-point medians.
	inline static void windowMedians32F(svlCudaPitch* pin, svlCudaLine *ixes, svlCudaLine *medians, float gmin, float gmax, int n = -1, bool yx = false) {
		svlCudaWindowMedians32F(pin, ixes, medians, gmin, gmax, n, yx);
	}

	// Matrix operations.
	// Set to identity matrix.
	inline static void eye(svlCudaPitch *M) {
		svlCudaEye(M);
	}
	// Transpose.
	inline static void transpose(const svlCudaPitch *pin, svlCudaPitch *pout, int img_w = -1, int img_h = -1) {
		svlCudaTranspose(pin, pout, img_w, img_h);
	}
	// Inverse
	inline static void inverse(svlCudaPitch *A, svlCudaPitch *Ainv, svlCudaPitch *T) {
		svlCudaInverse(A, Ainv, T);
	}


	// *** Miscellaneous functions ***

	// Round a/b to nearest higher integer value
	static int iDivUp(int a, int b) {
		return (a % b != 0) ? (a / b + 1) : (a / b);
	}
	// Round a/b to nearest lower integer value
	static int iDivDown(int a, int b) {
		return a / b;
	}
	// Align a to nearest higher multiple of b
	static int iAlignUp(int a, int b) {
		return (a % b != 0) ?  (a - a % b + b) : a;
	}
	// Align a to nearest lower multiple of b
	static int iAlignDown(int a, int b) {
		return a - a % b;
	}
	static inline int iAlignUpPow2(int a) {
		return int( pow( 2.0, ceil( log((double)a) / log(2.0) ) ) );
	}
};


#endif

