/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaCommon.h
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  Defines Cuda headers, data structures, #defines, and all function externs.
**
*****************************************************************************/

#pragma once

#ifndef __SVL_CUDA_COMMON_H
#define __SVL_CUDA_COMMON_H

// Hack for Windows.
#ifdef _WIN32
# define USE_CUDA
#endif

#ifdef USE_CUDA
# ifdef _WIN32
//# 	include <C:\Program Files\NVIDIA Corporation\NVIDIA CUDA SDK\common\inc\cuda.h>
#	 include <C:\Program Files\NVIDIA Corporation\NVIDIA CUDA SDK\common\inc\cutil.h>
# else
#	 include <cuda.h>
#	 include <cutil.h>
#  include <cuda_runtime_api.h>
# endif
#endif

#include <iostream>
#include <vector>

#include "cv.h"
#include "cxcore.h"
#include "svlCudaBuild.h"
using namespace std;

extern int iDivUp(int a, int b);
extern int iDivDown(int a, int b);
extern SVL_CUDA_KERNEL( int, iAlignUp(int a, int b));
extern int iAlignDown(int a, int b);

typedef unsigned char uint8;

#define NUM_BANKS 16
#define LOG_NUM_BANKS 4
//#define CONFLICT_FREE_OFFSET(index) ((index) >> LOG_NUM_BANKS)
#define CONFLICT_FREE_OFFSET(index) 0


// *** Device-side memory structures ***

// 1d memory: "line."
struct svlCudaLine {
	int w;				// Width
	float *data;		// Device pointer
	void *source;		// Host data pointer
	svlCudaLine() : w(-1) {
		data = NULL;
		source = NULL;
	}
};

// 2d memory: "pitch."
struct svlCudaPitch {
	int pitch, w, h;	// Device pitch, width, and height.
	float *data;		// Device pointer
	void *source;		// Host data pointer
	svlCudaPitch() : pitch(-1), w(-1), h(-1) {
		data = NULL;
		source = NULL;
	}
	svlCudaPitch(int pitch, int w, int h, float *data)
		: pitch(pitch), w(w), h(h), data(data) {}
	inline int size() const { return w*h; }
};

// 3d memory: "3d pitch."
struct svlCudaPitch3d {
	int pitch, w, h, z;	// Device pitch, width, height, and depth.
	float *data;		// Device pointer
	void *source;		// Host data pointers
	svlCudaPitch3d() : pitch(-1), w(-1), h(-1), z(-1) {
		data = NULL;
		source = NULL;
	}
	inline int size() const { return w*h*z; }
	inline int size2d() const { return w*h; }
	inline float* get(int i) {
		return (float*)(((char*)data) + pitch*h*i);
	}
	inline const float* get(int i) const {
		return (float*)(((char*)data) + pitch*h*i);
	}
	inline svlCudaPitch getPitch(int i) const {
		return svlCudaPitch(pitch, w, h, (float*)(((char*)data) + pitch*h*i) );
	}
};


// A cached patch for convolution on the device.
struct CachedPatch {
	int w, h, N;				// Padded width, height, and original numel.
	float mean, std;			// Patch mean and standard deviation.
	svlCudaLine *patch, *mask;	// Device pointers to padded patch and padded mask.
};

// Struct for binary masks on which blocks in a cuda function need to execute.
struct svlCudaCoveringGrid {
	int grid_x, grid_y;	// grid x and y sizes of grid.
	float *data;		// The grid data

	// Initialize with grid sizes directly.
	inline svlCudaCoveringGrid(int grid_x, int grid_y)
		: grid_x(grid_x), grid_y(grid_y) {
			data = (float*)malloc(sizeof(float)*grid_x*grid_y);
	}
	inline svlCudaCoveringGrid(int grid_x, int grid_y, float *data)
		: grid_x(grid_x), grid_y(grid_y), data(data) {}
	// Initialize with image and block sizes.
	inline svlCudaCoveringGrid(int img_w, int img_h, int block_x, int block_y) {
		grid_x = iDivUp(img_w, block_x);
		grid_y = iDivUp(img_h, block_y);
		data = (float*)malloc(sizeof(float)*grid_x*grid_y);
	}
	~svlCudaCoveringGrid() {
		if ( data != NULL ) free(data);
	}

	// Other members
	inline int size() {
		return grid_x * grid_y;
	}
	inline void clearGrid() {
		memset(data, 0, grid_x*grid_y);
	}
	// Direct element access functions.
	inline float operator[](int i) const {
		return this->data[i];
	}
	inline float& operator[](int i) {
		return this->data[i];
	}
};

// A struct for holding the information on a padded patch.
struct PaddedPatch {
	float *data;
	int w, h, orig_w, orig_h;
	PaddedPatch()
		: w(0), h(0), orig_w(0), orig_h(0) {
		data = NULL;
	}
	PaddedPatch(float *data, int w, int h, int orig_w, int orig_h)
		: data(data), w(w), h(h), orig_w(orig_w), orig_h(orig_h) {
		/*data = (float*)malloc(sizeof(float)*w*h);
		memcpy(data, datain, sizeof(float)*w*h);*/
	}
	inline int size() { return w*h; }
};


// *** Cuda functions ***

extern "C" {
// *** svlCudaCommon.cu functions ***
  // Line functions
  SVL_CUDA_KERNEL_VOID(svlCudaAllocLine(int w, svlCudaLine &n));
  SVL_CUDA_KERNEL_VOID(svlCudaFreeLine(svlCudaLine &n));
  SVL_CUDA_KERNEL_VOID(svlCudaLineToGPU(svlCudaLine &n, const float *src, int w = -1));
  SVL_CUDA_KERNEL_VOID(svlCudaLineFromGPU(float *dest, const svlCudaLine &n, int w = -1));
  // Pitch functions
  SVL_CUDA_KERNEL_VOID(svlCudaAllocPitch(int w, int h, svlCudaPitch *pitch));
  SVL_CUDA_KERNEL_VOID(svlCudaFreePitch(svlCudaPitch *p));
  SVL_CUDA_KERNEL_VOID(svlCudaAllocPitch3d(int w, int h, int z, svlCudaPitch3d *pitch));
  SVL_CUDA_KERNEL_VOID(svlCudaFreePitch3d(svlCudaPitch3d * p));
  SVL_CUDA_KERNEL_VOID(svlCudaMemset(svlCudaLine *p, int value = 0, int w = -1));
  SVL_CUDA_KERNEL_VOID(svlCudaMemset2D(svlCudaPitch *p, int value = 0, int w = -1, int h = -1));
  SVL_CUDA_KERNEL_VOID(svlCudaPitchToGPU(svlCudaPitch *p, const float *src, int w = -1, int h = -1));
  SVL_CUDA_KERNEL_VOID(svlCudaPitch3dToGPU(svlCudaPitch3d *p, const float *src, int w = -1, int h = -1));
  //void svlCudaPitchToGPUasync(svlCudaPitch &p, const float *src, int w = -1, int h = -1);
  SVL_CUDA_KERNEL_VOID(svlCudaPitchToGPU_all(svlCudaPitch *p, const float *src, int s_pitch, int w = -1, int h = -1));
  SVL_CUDA_KERNEL_VOID(svlCudaPitchFromGPU(float *dest, const svlCudaPitch *p, int w = -1, int h = -1));
  SVL_CUDA_KERNEL_VOID(svlCudaPitch3dFromGPU(float *dest, const svlCudaPitch3d *p, int w = -1, int h = -1));
  SVL_CUDA_KERNEL_VOID(svlCudaPitchFromGPU_all(float *dest, int d_pitch, int w, int h, const svlCudaPitch *p));
  SVL_CUDA_KERNEL_VOID(svlCudaPitchToPitch(svlCudaPitch *dest, const svlCudaPitch *src, int w = -1, int h = -1));
  // Other accessing functions
  void svlCudaRowRandomAccess(const IplImage *ixes, IplImage *data_out, const vector<int> row_counts, float *dest, svlCudaPitch &data, svlCudaPitch &addys);
  void svlCudaLinearRandomAccess(const IplImage *ixes, int max_row, float *dest, svlCudaPitch &data, svlCudaPitch &addys, svlCudaPitch &data_out);
  SVL_CUDA_KERNEL_VOID(svlCudaPitchIx2line(const svlCudaLine *ixline, const svlCudaPitch *data, svlCudaLine *res, int w = -1, bool yx = false, int line_off = 0, int line_stride = 1));
  SVL_CUDA_KERNEL_VOID(svlCudaXYpoint2Line(const svlCudaLine *lin, int x, int y, svlCudaLine *lout, int w = -1, bool yx = false));
  void svlCudaFill2DRandomAccessInt(svlCudaPitch *pout, const svlCudaLine *ixline, const svlCudaLine *res, int w = -1, bool yx = false);
  SVL_CUDA_KERNEL_VOID(svlCudaLine2pitchIx(svlCudaPitch *pout, const svlCudaLine *ixline, const svlCudaLine *res, int w = -1, bool yx = false));
  // Timer functions.
  SVL_CUDA_KERNEL_VOID(svlCudaTimerInit());
  SVL_CUDA_KERNEL_VOID(svlCudaTimerDestruct());
  void svlCudaTimerTic();
  float svlCudaTimerToc();

// *** svlCudaConvolution.cu functions ***
	// Correlation coefficient functions.
  SVL_CUDA_KERNEL_VOID(svlCudaCcoeffNormed2d_v2(const svlCudaPitch *pitch_in, svlCudaPitch *pitch_out, float *patch, float *mask,
		int patch_w, int patch_h, int patch_real_numel, float patch_mean = 0.0f, float patch_std = 1.0f, int img_w = -1, int img_h = -1,
						int out_w = -1, int out_h = -1, bool cached = false));
	/*void svlCudaCcoeffNormed2d_v3(const svlCudaPitch *pin, svlCudaPitch *pout, float *patch, float *mask, svlCudaPitch *integ, svlCudaPitch *integ2,
		int patch_w, int patch_h, int patch_real_numel, float patch_mean, float patch_std, int img_w, int img_h, bool cached = false);*/
	/*void svlCudaCcorrNormed(const svlCudaPitch *pin, svlCudaPitch *pout, const float *patch, const svlCudaPitch *integ, const svlCudaPitch *integ2, const svlCudaPitch *meds,
		int patch_w, int patch_h, int patch_real_numel, float patchSum, float patchNorm, int img_w, int img_h, bool cached = false);*/
  SVL_CUDA_KERNEL_VOID(svlCudaPlainConv2d_16(const svlCudaPitch *pin, svlCudaPitch *pout, float *patch, int patch_w, int patch_h,
					     int img_w = -1, int img_h = -1, int out_w = -1, int out_h = -1));
  SVL_CUDA_KERNEL_VOID(svlCudaPlainConv2d_32(const svlCudaPitch *pin, svlCudaPitch *pout, svlCudaPitch *temp, vector<PaddedPatch> &pps,
					     int img_w = -1, int img_h = -1, int out_w = -1, int out_h = -1));
  SVL_CUDA_KERNEL_VOID(svlCudaPlainConv2d_full(const svlCudaPitch *pin, svlCudaPitch *pout, float *patch, int patch_w, int patch_h, int img_w = -1, int img_h = -1));
  SVL_CUDA_KERNEL_VOID(svlCudaLasikConvolution(const svlCudaPitch *pin, CachedPatch *cp, const svlCudaPitch *integ, const svlCudaPitch *integ2,
					  svlCudaPitch *conv, svlCudaPitch *sum, svlCudaPitch *sum2, int img_w = -1, int img_h = -1));
  SVL_CUDA_KERNEL_VOID(svlCudaSuperSizeConv(const svlCudaPitch *A, const svlCudaPitch *P, svlCudaPitch *R, int A_w = -1, int A_h = -1, int R_w = -1, int R_h = -1));
	void svlCudaSuperSizeConv2(const svlCudaPitch *A, const svlCudaPitch *P, svlCudaPitch *R, int A_w = -1, int A_h = -1, int R_w = -1, int R_h = -1);

// *** svlCudaFilter.cu function ***
	void svlCudaSobel(const svlCudaPitch *pin, svlCudaPitch *edge, svlCudaPitch *temp1, svlCudaPitch *temp2, int img_w = -1, int img_h = -1);
	void svlCudaResize(const svlCudaPitch *pin, svlCudaPitch *pout, int new_w, int new_h, svlCudaPitch *temp1, svlCudaPitch *temp2, int img_w = -1, int img_h = -1);

// *** svlCudaNormalize.cu functions ***
	void svlCudaWindowNormalize(svlCudaPitch &pitch_in, const svlCudaPitch &integral, const svlCudaPitch &integral2,
		float patch_mean, float patch_std, int patch_w, int patch_h, int img_w = -1, int img_h = -1);
	void svlCudaWindowNormalize1(svlCudaPitch &pitch_in, const svlCudaPitch &integral, const svlCudaPitch &integral2,
		float patch_mean, float patch_std, int patch_w, int patch_h, int img_w = -1, int img_h = -1);
	void svlCudaWindowNormalize1DimSub(svlCudaPitch &pitch_in, const svlCudaPitch &integral, const svlCudaPitch &integral2,
		float patch_mean, float patch_std, int patch_w, int patch_h, int img_w = -1, int img_h = -1, int dim = 128);
	void svlCudaWindowSumSumSq(const svlCudaPitch *pin, svlCudaPitch *sum, svlCudaPitch *sum2, int win_w, int win_h, int img_w = -1, int img_h = -1);
	void svlCudaPatchNormalize(svlCudaPitch *pout, svlCudaPitch *sum, svlCudaPitch *sum2, float patch_mean, float patch_std, int patch_N, int img_w = -1, int img_h = -1);
  SVL_CUDA_KERNEL_VOID(svlCudaWindowNormalizeRow(svlCudaPitch *pin, svlCudaPitch *pout, int patch_w, int img_w = -1, int img_h = -1, int dim = 128));
// *** svlCudaMath.cu functions ***
  SVL_CUDA_KERNEL_VOID(svlCudaAdd(svlCudaPitch *a, const svlCudaPitch *b, float fac = 1.0f, int img_w = -1, int img_h = -1));
	void svlCudaAddOff(svlCudaPitch *a, const svlCudaPitch *b, float fac, int img_w = -1, int img_h = -1, int off_x = 0, int off_y = 0);
	void svlCudaAdd3(svlCudaPitch *out, const svlCudaPitch *a, const svlCudaPitch *b, float fac_a = 1.0f, float fac_b = 1.0f, int img_w = -1, int img_h = -1);
	void svlCudaMult(svlCudaPitch *a, const svlCudaPitch *b, float fac = 1.0f, int img_w = -1, int img_h = -1);
	void svlCudaMult3(svlCudaPitch *out, const svlCudaPitch *a, const svlCudaPitch *b, float fac = 1.0f, int img_w = -1, int img_h = -1);
	void svlCudaDiv(svlCudaPitch *a, const svlCudaPitch *b, float fac = 1.0f, int img_w = -1, int img_h = -1);
	void svlCudaDiv3(svlCudaPitch *out, const svlCudaPitch *a, const svlCudaPitch *b, float fac = 1.0f, int img_w = -1, int img_h = -1);
	void svlCudaScaleOffset(svlCudaPitch *a, float scale = 1.0f, float offset = 0.0f, int img_w = -1, int img_h = -1);
  SVL_CUDA_KERNEL_VOID(svlCudaDecimate(const svlCudaPitch *a, svlCudaPitch *b, int delta_x = 2, int delta_y = 2, int img_w = -1, int img_h = -1));
	void svlCudaAddQuadrature(svlCudaPitch *a, const svlCudaPitch *b, int img_w = -1, int img_h = -1);
	// Min-max'ing
	void svlCudaMax(svlCudaPitch &pitch_in, svlCudaPitch &pitch_out, int max_w = 7, int max_h = 7, int img_w = -1, int img_h = -1, svlCudaCoveringGrid *cudagrid = NULL);
	void svlCudaMax7x7(svlCudaPitch &pitch_in, svlCudaPitch &pitch_out, int img_w = -1, int img_h = -1, svlCudaCoveringGrid *cudagrid = NULL);
	void svlCudaMax7x7Cent(svlCudaPitch &pitch_in, svlCudaPitch &pitch_out, int img_w = -1, int img_h = -1);
	void svlCudaMinMax4x4(const svlCudaPitch *pitch_in, svlCudaPitch *min_out = NULL, svlCudaPitch *max_out = NULL, int img_w = -1, int img_h = -1);
  SVL_CUDA_KERNEL_VOID(svlCudaMinMax32x32(const svlCudaPitch *pin, svlCudaPitch *temp, svlCudaPitch *min_out = NULL, svlCudaPitch *max_out = NULL, int img_w = -1, int img_h = -1));
	// Others
  SVL_CUDA_KERNEL_VOID(svlCudaMean32x32(const svlCudaPitch *pin, svlCudaPitch *mean, svlCudaPitch *temp, int img_w, int img_h));
  SVL_CUDA_KERNEL_VOID(svlCudaLasikCorrectedMax(const svlCudaPitch *conv, const svlCudaPitch *sum, const svlCudaPitch *sum2, const svlCudaPitch *correct,
						svlCudaPitch *maxres, float N, float patchSum, float patchNorm, int max_w, int max_h, int data_w, int data_h));
  SVL_CUDA_KERNEL_VOID(svlCudaFourPointMax(const svlCudaPitch *pin, svlCudaPitch *pout, int max_w, int max_h, int img_w = -1, int img_h = -1));

// *** svlCudaMatrix.cu functions ***
	void svlCudaEye(svlCudaPitch *M);
	void svlCudaInverse(svlCudaPitch *A, svlCudaPitch *Ainv, svlCudaPitch *T);
	void svlCudaTranspose(const svlCudaPitch *pin, svlCudaPitch *pout, int img_w = -1, int img_h = -1);

// *** svlCudaMedian.cu functions ***
	void svlCudaMedian8U_1024(svlCudaLine *ln, svlCudaLine *lout, int w = -1);
	void svlCudaWindowMedians8U(svlCudaPitch* pin, svlCudaLine *ixes, svlCudaLine *medians, int n = -1, bool yx = false);
	void svlCudaWindowMedians32F(svlCudaPitch* pin, svlCudaLine *ixes, svlCudaLine *medians, float gmin, float gmax, int n = -1, bool yx = false);

// *** svlCudaIntegral.cu functions ***
	void svlCudaIntegral(const svlCudaPitch *pin, svlCudaPitch *integ, svlCudaPitch *temp, svlCudaPitch *integ2 = NULL, int img_w = -1, int img_h = -1);
  SVL_CUDA_KERNEL_VOID(svlCudaIntegralLine(const svlCudaLine *n, svlCudaLine *integ, int w = -1, int dim = 128, bool do_cumsum = false, int stride = -1));
	void svlCudaIntegralLineInt(svlCudaLine *n, svlCudaLine *integ, int w = -1, int dim = 128, bool do_cumsum = false, int stride = -1);
	void svlCudaIntegralBlock(const svlCudaPitch *pin, svlCudaPitch *integ, svlCudaPitch *tempint, svlCudaPitch *integ2 = NULL, svlCudaPitch *tempint2 = NULL, int img_w = -1, int img_h = -1, int dim = 128);
  SVL_CUDA_KERNEL_VOID(svlCudaRowIntegralBlock(const svlCudaPitch *pin, svlCudaPitch *integ, svlCudaPitch *integ2 = NULL, int img_w = -1, int img_h = -1, int dim = 128));

// *** svlCudaDecisionTreeSet.cu functions ***
	//void svlCudaDecisionTreeSetEvaluate(const svlCudaLine *dtree, int n_trees, svlCudaLine *features, int feature_len, int feature_stride, int n_fvs, int execs_per_block);
  SVL_CUDA_KERNEL_VOID(svlCudaDecisionTreeSet15Evaluate(const svlCudaLine *dtreei, const svlCudaLine *dtreef, svlCudaLine *invalid, int n_trees, svlCudaLine *features, int feature_len, int feature_stride, int n_fvs, int execs_per_block));
  SVL_CUDA_KERNEL_VOID(svlCudaDecisionTreeSetCollectResults(svlCudaLine *features, svlCudaLine *res, int n_trees, int n_fvs, int feature_stride));
};

// Fast integer multiplication and easy pitch accessor
#define IMUL(a, b) __mul24(a, b)
// Get a pointer to a row of a pitch. Takes the data pointer, pitch size (in bytes), and row offset.
#define PITCH_GET(data,pitch,y)		(float*)((char*)data + IMUL(pitch,y))
#define INT_PITCH_GET(data,pitch,y)	(int*)((char*)data + IMUL(pitch,y))
#define UINT_PITCH_GET(data,pitch,y)	(unsigned int*)((char*)data + IMUL(pitch,y))
// Get one l-value in a pitch. Takes the data poitner, pitch, row offset, and column offset.
#define PITCH_GET_X(data,pitch,y,x)		(*( (float*)((char*)data + IMUL(pitch,y)) + x ))

// Some macros for pre-kernel functions.
#define SVLCUDA_DEFAULT_SIZE(ww,hh,pitch)   if ( ww == -1 ) ww = pitch->w; if ( hh == -1 ) hh = pitch->h;
#define SVLCUDA_BLOCK2GRID(w,h,block,grid)   dim3 grid( iDivUp(w, block.x), iDivUp(h, block.y) );

#endif

