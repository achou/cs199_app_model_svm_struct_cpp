/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaSlidingWindowDetector.cpp
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
**
*****************************************************************************/

#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <sys/types.h>
#include <limits>

#include "cxcore.h"
#include "ml.h"

#include "svlCudaSlidingWindowDetector.h"
#include "svlPatchDictionary.h"

#if defined(_WIN32)||defined(WIN32)||defined(__WIN32__)
#define _CRT_SECURE_NO_DEPRECATE
#include "win32/dirent.h"
#ifndef isnan
#define isnan(x) (_isnan(x))
#endif
#ifndef isinf
#define isinf(x) (!_finite(x))
#endif
#ifndef isfinite
#define isfinite(x) (_finite(x))
#endif
#else
#include <dirent.h>
#endif

#undef DEBUG

using namespace std;

#if 0
svlCudaMemHandler *cm; // Faster access to static members than "svlCudaMemHandler::..."

static inline bool isSampleValid(vector<double> &sample) {
	for (unsigned i = 0; i < sample.size(); i++)
		if (isinf(sample[i]) || isnan(sample[i])) return false;
	return true;
}

// Score-to-probability conservsions and vice versa.
inline float scoreToProbability(float score) {
	return 1.0f / (1.0f + exp(-score));
}
inline float probabilityToScore(float pr) {
	return (float)(log(pr) - log(1.0f - pr));
}

// Dumps an IplImage to a named file.
template <class C>
void iplimage2file(const char *file, const IplImage *im)
{
	FILE *fo = fopen(file,"wb");
	fwrite(&im->width,sizeof(int),1,fo);
	fwrite(&im->height,sizeof(int),1,fo);
	C gf;
	for ( int i=0; i<im->height; ++i ) {
		for ( int j=0; j<im->width; ++j ) {
			//gf = float(im->imageData[ i*im->widthStep + j ]);
			gf = CV_IMAGE_ELEM(im, C, i, j);
			fwrite(&gf,sizeof(C),1,fo);
		}
	}
	fclose(fo);
}

// Constructors/Destructors --------------------------------------------------

svlCudaSlidingWindowDetector::svlCudaSlidingWindowDetector(int max_img_w, int max_img_h, const char *name, int w, int h)
: _windowWidth(w), _windowHeight(h), _threshold(0.5)
{
	_objectName = !name ? "thing" : string(name);

	// Allocate pitch memory.
	_IMG = cm->allocPitch(max_img_w, max_img_h, "image");
	_INTEG = cm->allocPitch(max_img_w + 1, max_img_h + 1, "integral");
	_INTEG2 = cm->allocPitch(max_img_w + 1, max_img_h + 1, "integral2");
	_CONV = cm->allocPitch(max_img_w, max_img_h, "convolution");
	_TEMP1 = cm->allocPitch(max_img_w + 1, max_img_h, "temp1");
	_TEMP2 = cm->allocPitch(max_img_w + 1, max_img_h, "temp2"); // Plus one as they could possibly used for device integral images.
	_TEMP3 = cm->allocPitch(max_img_w + 1, max_img_h, "temp3");
	_TEMP4 = cm->allocPitch(max_img_w + 1, max_img_h, "temp4");
	_RESP = cm->allocPitch(max_img_w + 1, max_img_h, "resp");
	_FACTORS = cm->allocPitch(max_img_w, max_img_h, "factors");

	// All pointer members to NULL.
	cachedLine = NULL;
	dtree = NULL;
	cachedPatches = NULL;

	patches_cached = false;
}

svlCudaSlidingWindowDetector::~svlCudaSlidingWindowDetector()
{
	// Free all device memory.
	cm->freePitch(_IMG);
	cm->freePitch(_INTEG);
	cm->freePitch(_INTEG2);
	cm->freePitch(_CONV);
	cm->freePitch(_RESP);
	cm->freePitch(_FACTORS);
	cm->freePitch(_TEMP1);
	cm->freePitch(_TEMP2);
	cm->freePitch(_TEMP3);
	cm->freePitch(_TEMP4);

	if ( cachedLine )
		cm->freeLine(cachedLine);
	if ( dtree )
		delete dtree;
}

// Compute a binary mask (in floats) 
extern void computeCudaCoveringGrid(vector<CvPoint>& locations, int block_x, int block_y, int dx, int dy, svlCudaCoveringGrid &cgrid);

// Computes interesting window locations for image saved at pitch_ix.
void svlCudaSlidingWindowDetector::createInterestingWindowLocations(const svlCudaPitch *img, int img_w, int img_h, vector<CvPoint>& locations) const
{
	int maxX = img_w - _windowWidth;
	int maxY = img_h - _windowHeight;
	if ( maxX <= 0 || maxY <= 0 )
		return;

	// Image IMG to min CONV and max TEMP1.
	svlCudaPitch *minp = _CONV, *maxp = _TEMP1; // Alias owned variables.
	svlCudaMath::minMax32x32(img, _TEMP2, minp, maxp, img_w, img_h);

	// Perform <max -= min;>.
	svlCudaMath::add(maxp, minp, -1.0f, img_w, img_h);
	// Decimate by factors to retrieve only needed data; maxp to TEMP2.
	svlCudaMath::decimate(maxp, _TEMP2, _delta_x, _delta_y, img_w, img_h);

	// Copy out result.
	const int sub_width = img_w / _delta_x;
	const int sub_height = img_h / _delta_y;
	float *back = (float*)malloc(sizeof(float)*sub_width*sub_height);
	//cm->floatFromGPU(back, sub_width, sub_height, _TEMP2);
	cm->pitchFromGPU(back, _TEMP2, sub_width, sub_height);

	// Get min_delta of image uniformity conditions.
	float min_delta = 0.125f;
	locations.reserve((maxX + _delta_x) * (maxY + _delta_y) / (_delta_x * _delta_y));
	int ix, iy = 0;
	for (int y=0; y <= maxY; y += _delta_y) {
		ix = 0;
		for (int x=0; x <= maxX; x += _delta_x) {
			if ( back[ iy*sub_width + ix ] > min_delta ) {
				locations.push_back(cvPoint(x, y));
			}
			++ix;
		}
		++iy;
	}
	locations.resize(locations.size()); // Throw away extra pre-allocated things.
	free(back);
}

void svlCudaSlidingWindowDetector::computeLabels(vector<CvPoint> &locations, double scale, 
												 const svlObject2dFrame *posFrames, 
												 const svlObject2dFrame *negFrames,
												 vector<int> &labels)
{
	labels = vector<int>(locations.size(),0); // All zero labels.
}

void svlCudaSlidingWindowDetector::featurePostProcessing(const vector<CvPoint> &locations,
														 const vector<int> &labels,
														 const vector<vector<float> > F)
{
	return;
}

// Classifies a multi-channel image.
void svlCudaSlidingWindowDetector::classifyImage(const vector<IplImage *>& images_in,
												 svlObject2dFrame *objects,
												 ScaleLocations *locs,
												 ScaleFeatures *F,
												 const svlObject2dFrame *posFrames,
												 const svlObject2dFrame *negFrames,
												 bool lasmeds)
{
	const bool ret_objects = objects != NULL;
	const bool ret_features = locs != NULL && F != NULL;
	if ( ret_objects && !dtree )
	{	cout << "svlCudaSlidingWindowDetector::classifyImage: Missing Cuda decision tree.\n"; return;	}
	if ( !patches_cached )
	{	cout << "svlCudaSlidingWindowDetector::classifyImage: Have not cached the patch dictionary on the device.\n"; return;	}
	if ( !ret_objects && !ret_features )
	{	cout << "svlCudaSlidingWindowDetector::classifyImage: Not asked to return anything.\n"; return;	}

	// Assert that we have not been given any NULL images or images of different size.
	vector<IplImage*> images(images_in.size(),NULL), scaledImages(images_in.size(),NULL);
	for ( int i=0; i<int(images_in.size()); ++i ) {
		assert(images_in[i] != NULL);
		assert(images_in[i]->width == images_in[0]->width);
		assert(images_in[i]->height == images_in[0]->height);
		images[i] = create32Fimage(images_in[i]);
		scaledImages[i] = cvCloneImage(images[i]);
	}

	SVL_LOG(SVL_LOG_VERBOSE, "Running classifier on " << int(images_in.size()) << "-channel image");

	// Code profiling handles.
	const int prof = svlCodeProfiler::getHandle("cSWD :: classifyImage");
	const int inner_cuda = svlCodeProfiler::getHandle("cSWD :: CUDA inner loop");
	const int eval_model = svlCodeProfiler::getHandle("cSWD :: eval model");
	const int medh = svlCodeProfiler::getHandle("cSWD :: medians");
	svlCodeProfiler::tic(prof);

	// Various important members.
	int dim = 256;							// Integration dimension (must be power of 2).
	float scoreThreshold = probabilityToScore((float)_threshold);
	double scale = double(1), gd;			// Accumulated scaling factor.
	int scaled_w = images[0]->width, scaled_h = images[0]->height; // Set current working image scale.
	bool yx = true;							// Whether CvPoints are stored (y,x) on the GPU.
	int feature_stride = dict.numFeatures(),// Stride of features on device.
		aligned_n_trees = -1;				// If stride needs to be bumped up for tree size.
	bool native_pow_2 = false;				// True if rounded-up stride is equal to original stride, which dictates using cumsum-style summation instead of scan-style.
	if ( ret_objects ) {
		// Find feature stride for device-side decision tree computation (max of feature size and tree size, rounded up to nearest factor of 2).
		aligned_n_trees = svlCudaMath::iAlignUpPow2( dtree->getNtrees() );
		feature_stride = max( aligned_n_trees, dict.numFeatures() );
		native_pow_2 = dtree->getNtrees() == feature_stride;
	}
	svlCudaLine *dfeatures = NULL, *ixline = NULL, *locline = NULL, *res = NULL, *valid = NULL, *facline = NULL;
	cm->pitchSet(_FACTORS, 0);

	// Can do sub-7x7 max'es if decimation is >= 4 and no patches have more than 7 as a max dim.
	bool cmax_opt = _delta_x >= 4 && _delta_y >= 4;
	for ( vector<svlPatch*>::const_iterator p=dict._entries->begin(); cmax_opt && p!=dict._entries->end(); ++p )
		cmax_opt &= (*p)->rect.width <= 7 && (*p)->rect.height <= 7;
	//cmax_opt = false;

	// Iterate over all scales
	while ( scaled_w >= _windowWidth && scaled_h >= _windowHeight )
	{
		// TODO paulb: if verbose.
		//t.printETC(ii++, 15, cout, powf(_delta_scale, -1.8f) );
#if 0
		{ // Output images
			static int xena; char c[32];
			for ( int i=0; i<int(scaledImages.size()); ++i ) {
				sprintf(c,"cudas_%d_%d.out",xena,i);
				svlBinaryWriteIpl(c, scaledImages[i]);
			}
			++xena;
		}
#endif

		// Copy first image over for finding locations.
		cm->IplToGPU(_IMG, scaledImages[0]);
		vector<CvPoint> locations;
		createInterestingWindowLocations(_IMG, scaled_w, scaled_h, locations);

		// Compute labels.
		vector<int> labels;
		if ( posFrames && negFrames ) {
			computeLabels(locations, scale, posFrames, negFrames, labels);
		}

		if ( !locations.empty() ) {
			// Allocate feature space if first time or if old data structures are too small (unlikely).
			int gi = feature_stride * int(locations.size());
			bool do_alloc = dfeatures == NULL || dfeatures->w < gi;
			if ( do_alloc && dfeatures ) {
				// Free memory first.
				cm->freeLine(dfeatures);
				cm->freeLine(res);
				cm->freeLine(ixline);
				cm->freeLine(locline);
				cm->freeLine(valid);
				cm->freeLine(facline);
			}
			if ( do_alloc ) {
				dfeatures = cm->allocLine(gi, "dfeatures");
				ixline = cm->allocLine( 2*int(locations.size()), "ixline" );
				locline = cm->allocLine( 2*int(locations.size()), "locline" );
				res = cm->allocLine(int(locations.size()), "res");
				valid = cm->allocLine(int(locations.size()), "valid");
				facline = cm->allocLine(int(locations.size()), "facline");
			}
			svlCudaMemHandler::CvPoint2IntLine(locations, locline, yx); // Place permanent locations in "locline."
			cm->lineSet(valid, 0, int(locations.size())); // Zeralize valids.

			SVL_LOG(SVL_LOG_VERBOSE, "Computing features over " << locations.size() << " windows for image size "
				<< scaled_w << " x " << scaled_h << "...");

			// Compute feature responses from dictionary.
			for ( int chan = 0; chan < int(images.size()); ++chan ) {
				// Copy over to GPU.
				if ( chan > 0 )
					cm->IplToGPU(_IMG, scaledImages[chan]);
				svlCudaMath::rowIntegralBlock(_IMG, _INTEG, _INTEG2, scaled_w, scaled_h, dim); // Row-integrate

				// Compute window normalization constants.
				if ( chan != 2 ) {
					// 32-by-32 means.
					svlCudaMath::mean32x32(_IMG, _TEMP3, _TEMP1, scaled_w, scaled_h);
					svlCudaMemHandler::pitchIx2line(_TEMP3, locline, facline, int(locations.size()), yx);
				} else {
					// Medians.
					svlCodeProfiler::tic(medh);
					vector<float> consts;
					computeNormalizationConstants(scaledImages[2], SVL_DEPTH_PATCH, locations, cvSize(32,32), consts, NULL);
					//svlPatchDictionary::computeNormalizationConstantsStatic(scaledImages[2], SVL_DEPTH_PATCH, locations, cvSize(32,32), consts);
					cm->lineToGPU(facline, (float*)&consts[0], int(locations.size()));
					svlCodeProfiler::toc(medh);
				}
/*for ( int i=0; i<dict.numFeatures(); ++i ) {
				// Pre-normalize the row sums.
				int chan = (*dict._entries)[i]->channel;
				svlPatch *p = (*dict._entries)[i];
				CachedPatch *cp = &(*cachedPatches)[i];
				cm->IplToGPU(scaledImages[chan], _IMG);
				svlCudaMath::rowIntegralBlock(_IMG, _INTEG, _INTEG2, scaled_w, scaled_h, dim); // Row-integrate
				svlCudaConvolution::windowNormalizeRow(_INTEG, _TEMP1, p->patch->width, scaled_w, scaled_h, dim);
				svlCudaConvolution::windowNormalizeRow(_INTEG2, _TEMP2, p->patch->width, scaled_w, scaled_h, dim);
				facline = NULL;*/

				for ( int w = dict.min_width; w <= dict.max_width; ++w ) {
					if ( int(patchBins[w][chan].size()) == 0 )
						continue;
					svlCudaConvolution::windowNormalizeRow(_INTEG, _TEMP1, w, scaled_w, scaled_h, dim);
					svlCudaConvolution::windowNormalizeRow(_INTEG2, _TEMP2, w, scaled_w, scaled_h, dim);

					for ( vector<pair<int,svlPatch*> >::const_iterator pp = patchBins[w][chan].begin();
						pp != patchBins[w][chan].end(); ++pp )
					{
						svlCodeProfiler::tic(inner_cuda);
						svlPatch *p = pp->second;
						int i = pp->first;
						CachedPatch *cp = &(*cachedPatches)[i]; // Use device-cached patch.
						// Add rect offset to locations stored on the GPU ("locline") to update "ixline."
//cm->line2file("locline.out", locline);
						svlCudaMemHandler::addXYpoint2Line(locline, p->rect.x, p->rect.y, ixline, 2*int(locations.size()), yx);

						if ( cmax_opt ) {
							// Fill other indices and provide offsets.
							int x_add = p->rect.width - 4, 
								y_add = p->rect.height - 4;
							svlCudaMemHandler::addXYpoint2Line(locline, p->rect.x, p->rect.y + y_add, ixline, 2*int(locations.size()), yx);
							svlCudaMemHandler::line2pitchIx(_FACTORS, ixline, facline, int(locations.size()), yx);
							svlCudaMemHandler::addXYpoint2Line(locline, p->rect.x + x_add, p->rect.y, ixline, 2*int(locations.size()), yx);
							svlCudaMemHandler::line2pitchIx(_FACTORS, ixline, facline, int(locations.size()), yx);
							svlCudaMemHandler::addXYpoint2Line(locline, p->rect.x + x_add, p->rect.y + y_add, ixline, 2*int(locations.size()), yx);
							svlCudaMemHandler::line2pitchIx(_FACTORS, ixline, facline, int(locations.size()), yx);
						}
						// Add rect offset to locations stored on the GPU ("locline") to update "ixline."
						svlCudaMemHandler::addXYpoint2Line(locline, p->rect.x, p->rect.y, ixline, 2*int(locations.size()), yx);
						svlCudaMemHandler::line2pitchIx(_FACTORS, ixline, facline, int(locations.size()), yx);
//cm->line2file("ixline_pre.out", ixline);
						// Three-output convolution.
/*cm->pitch2file("img.out", _IMG);
cm->pitch2file("temp1.out", _TEMP1);
cm->pitch2file("temp2.out", _TEMP2);*/
						svlCudaConvolution::lasikPatchConv2(_IMG, cp, _TEMP1, _TEMP2, _CONV, _TEMP3, _TEMP4, scaled_w, scaled_h);
/*cm->pitch2file("conv.out", _CONV);
cm->pitch2file("temp3.out", _TEMP3);
cm->pitch2file("temp4.out", _TEMP4);
printf("three-output conv\n");*/
						// Correction.
						if ( !cmax_opt ) {
							svlCudaMath::lasikCorrectedMax(_CONV, _TEMP3, _TEMP4, _FACTORS, _RESP, cp->N, cp->mean, cp->std, p->rect.width, p->rect.height, scaled_w, scaled_h);
							svlCudaMemHandler::pitchIx2line(_RESP, ixline, dfeatures, int(locations.size()), yx, i, feature_stride);
						} else {
							// 4-by-4 max; then do 4-point max to get proper size.
//printf("Za!\n");
							svlCudaMath::lasikCorrectedMax(_CONV, _TEMP3, _TEMP4, _FACTORS, _RESP, cp->N, cp->mean, cp->std, 4, 4, scaled_w, scaled_h);
//cm->pitch2file("resp.out", _RESP);
							svlCudaMath::fourPointMax(_RESP, _TEMP3, p->rect.width - 4, p->rect.height - 4, scaled_w, scaled_h);
//cm->pitch2file("max4.out", _TEMP3);
//cm->line2file("ixline.out", ixline);
//printf("i: %d\n", i);
							svlCudaMemHandler::pitchIx2line(_TEMP3, ixline, dfeatures, int(locations.size()), yx, i, feature_stride);
//cm->line2file("feats.out", dfeatures);
						}
//exit(-1);
						svlCodeProfiler::toc(inner_cuda);
					}
				}
			}
			SVL_LOG(SVL_LOG_VERBOSE, "...done");

			// Copy out features to return natively.
			if ( ret_features ) {
				// Copy locations.
				locs->push_back(locations);
				// Copy out features.
				float *feats = (float*)malloc( sizeof(float)*locations.size()*feature_stride );
				cm->lineFromGPU(feats, dfeatures, int(locations.size())*feature_stride);
				F->push_back(vector<vector<float> >());
				F->back().reserve(locations.size());
				float *ptr = feats;
				for ( int i=0; i<int(locations.size()); ++i ) {
					F->back().push_back(vector<float>(ptr, ptr+feature_stride));
					ptr += feature_stride;
				}
				free(feats);

				featurePostProcessing(locations, labels, F->back());
			}

			// Evaluate decision tree model.
			if ( ret_objects ) {
				svlCodeProfiler::tic(eval_model);
				// Calculate decision tree nodes on device.
				dtree->evaluate(dfeatures, valid, dict.numFeatures(), feature_stride, int(locations.size()), 100);
				// Integrate features to get response of all trees.
				svlCudaMath::integralLine(dfeatures, dfeatures, int(locations.size())*feature_stride, aligned_n_trees, native_pow_2, feature_stride);

				// Compact results and read back out.
				dtree->collectResults(dfeatures, res, int(locations.size()), feature_stride);
				float *hres = (float*)malloc(sizeof(float)*locations.size());
				float *hval = (float*)malloc(sizeof(float)*locations.size());
				cm->lineFromGPU(hres, res, int(locations.size()));
				cm->lineFromGPU(hval, valid, int(locations.size()));

				// Compute objects from features.
				float *ptr = hres, *vtr = hval;
				int ct = 0;
				for ( vector<CvPoint>::const_iterator loc = locations.begin(); loc != locations.end(); ++loc, ++ptr, ++vtr ) {
					if ( isinf(*ptr) || isnan(*ptr) || *vtr != 0.0f )
						continue;
					if ( !NO_OBJECTS && *ptr > scoreThreshold ) {
						++ct;
						objects->push_back(svlObject2d(scale * loc->x, scale * loc->y,
							scale * _windowWidth, scale * _windowHeight));
						objects->back().name = _objectName;
						objects->back().pr = scoreToProbability(*ptr);
					}
				}
				free(hres);
				free(hval);
				svlCodeProfiler::toc(eval_model);
			}
		}

		// Scale down the image channels.
		scale *= _delta_scale;
		scaled_w = int( gd = double(images[0]->width) / scale );
		if ( int(gd+1e-4) != scaled_w ) ++scaled_w;
		scaled_h = int( gd = double(images[0]->height) / scale );
		if ( int(gd+1e-4) != scaled_h ) ++scaled_h;
		for ( int i=0; i<int(images.size()); ++i ) {
			cvReleaseImage(&scaledImages[i]);
			scaledImages[i] = cvCreateImage( cvSize(scaled_w, scaled_h), images[i]->depth, images[i]->nChannels);
			cvResize(images[i], scaledImages[i]);
		}
	}

	if ( dfeatures ) {
		// Free lines.
		cm->freeLine(dfeatures);
		cm->freeLine(res);
		cm->freeLine(ixline);
		cm->freeLine(locline);
		cm->freeLine(valid);
		cm->freeLine(facline);
	}
	// Release images.
	for ( int i=0; i<int(images.size()); ++i ) {
		cvReleaseImage(&images[i]);
		cvReleaseImage(&scaledImages[i]);
	}

	svlCodeProfiler::toc(prof);
}

// Pad all the patches in the dictionary for device convolution, then send to GPU.
void svlCudaSlidingWindowDetector::padAndCachePatches()
{
	// Manage memory.
	if ( cachedLine != NULL && cachedLine->data != NULL )
		cm->freeLine(cachedLine);

	if ( cachedPatches != NULL ) {
		cachedPatches->clear();
	} else {
		cachedPatches = new vector<CachedPatch>();
	}

	// Create cached pointers for host-side use.
	cachedPatches->reserve(dict._entries->size());

	vector<int> paddedSizes;
	paddedSizes.reserve(dict._entries->size());
	int total_sz = 0, max_chan = 0;
	pair<int,int> pad_wh;
	for ( vector<svlPatch*>::const_iterator p = dict._entries->begin(); p != dict._entries->end(); ++p ) {
		pad_wh = svlCudaConvolution::paddedPatchSize( (*p)->patch );
		max_chan = max(max_chan, (*p)->channel);

		cachedPatches->push_back(CachedPatch());
		cachedPatches->back().w = pad_wh.first;
		cachedPatches->back().h = pad_wh.second;
		cachedPatches->back().N = (*p)->patch->width * (*p)->patch->height; // Original numel.

		// Calculate patch statistics.
		if ( (*p)->patch->depth != IPL_DEPTH_32F ) {
			printf("svlCudaSlidingWindowDetector::padAndCachePatches -- Unexpected svlPatch depth: %08x\n", (*p)->patch->depth);
		} else {
			float gf, patchSum = 0.0, patchNorm = 0.0;
			for ( int y=0; y<(*p)->patch->height; ++y ) {
				for ( int x=0; x<(*p)->patch->width; ++x ) {
					patchSum += gf = CV_IMAGE_ELEM((*p)->patch, float, y, x);
					patchNorm += gf * gf;
				}
			}
			cachedPatches->back().mean = patchSum;
			cachedPatches->back().std = patchNorm;
		}

		paddedSizes.push_back( pad_wh.first * pad_wh.second );
		total_sz += paddedSizes.back();
	}
	++max_chan;

	// Allocate memory.
	float *patchData = (float*)malloc(sizeof(float)*total_sz);
	float *ptr = patchData;
	// Pad patches.
	vector<int>::const_iterator szs = paddedSizes.begin();
	for ( vector<svlPatch*>::const_iterator p = dict._entries->begin(); p != dict._entries->end(); ++p ) {
		svlCudaConvolution::padPatchOnly( (*p)->patch, ptr );
		ptr += *szs; // Advance pointer by padded size.
		++szs;
	}

	// Allocate device meory.
	cachedLine = cm->allocLine(total_sz, "patch cache");
	// Copy over to GPU.
	cm->lineToGPU(cachedLine, patchData, total_sz);

	// Free host-side memory.
	free(patchData);

	// Create individual svlCudaLine pointers.
	float *dev_ptr = cachedLine->data;
	for ( int i=0; i<(int)cachedPatches->size(); ++i ) {
		// Patch line.
		(*cachedPatches)[i].patch = new svlCudaLine();
		(*cachedPatches)[i].patch->w = paddedSizes[i];
		(*cachedPatches)[i].patch->data = dev_ptr;
		dev_ptr += paddedSizes[i];
#if 0
		// Mask line.
		(*cachedPatches)[i].mask = new svlCudaLine();
		(*cachedPatches)[i].mask->w = paddedSizes[i];
		(*cachedPatches)[i].mask->data = dev_ptr;
		dev_ptr += paddedSizes[i];
#else
		(*cachedPatches)[i].mask = NULL; // Do not use masks.
#endif
	}

	// Clear this flag.
	patches_cached = true;

	// Bin the patches in the patchBins structure.
	patchBins.clear();
	for ( int w=0; w<=dict.max_width; ++w ) {
		patchBins.push_back(vector<vector<pair<int,svlPatch*> > >());
		for ( int chan=0; chan<max_chan; ++chan )
			patchBins.back().push_back(vector<pair<int,svlPatch*> >());
	}
	// Fill patch bins.
	int i=0;
	for ( vector<svlPatch*>::const_iterator p=dict._entries->begin(); p!=dict._entries->end(); ++p, ++i ) {
		patchBins[ (*p)->patch->width ][ (*p)->channel ].push_back(pair<int,svlPatch*>(i,*p));
	}

	/*if ( 1 ) {
	printf("Outputting patches\n");
	FILE *fo = fopen("cudadict.out","wb");
	int gi = int(dict._entries->size());
	fwrite(&gi,sizeof(int),1,fo);
	for ( unsigned i=0; i<dict._entries->size(); ++i ) {
	const IplImage *img = (*dict._entries)[i]->patch;
	fwrite(&(gi=int(img->width)),sizeof(int),1,fo);
	fwrite(&(gi=int(img->height)),sizeof(int),1,fo);
	if ( img->depth != IPL_DEPTH_32F ) {
	printf("Unexpected depth.\n"); assert(false);
	}
	for ( int r=0; r<int(img->height); ++r ) {
	for ( int c=0; c<int(img->width); ++c ) {
	fwrite(&CV_IMAGE_ELEM(img, float, r, c),sizeof(float),1,fo);
	}
	}
	}
	fclose(fo);
	}*/
	//cm->line2file("cudacache.out", cachedLine);
}

// Load a decision tree.
void svlCudaSlidingWindowDetector::loadCudaDTree(const char *filename)
{
	if ( dtree != NULL ) {
		delete dtree;
	}
	dtree = new svlCudaDecisionTreeSet(filename);
}

// Load a 15-node-model decision tree.
void svlCudaSlidingWindowDetector::loadCudaDTree15(const char *filename)
{
	if ( dtree != NULL ) {
		delete dtree;
	}
	dtree = new svlCudaDecisionTreeSet15(filename);
}

#endif
