/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaDecisionTreeSet.h
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  Implements a Cuda decision tree of depth 3.
**
*****************************************************************************/

#pragma once

#include <iostream>
#include <vector>

#include "cv.h"
#include "cxcore.h"

#include "svlCudaCommon.h"
#include "svlCudaMath.h"
#include "svlCudaMemHandler.h"
#include "svlML.h"
using namespace std;


// Cuda decision tree class.

extern svlCudaMemHandler *cm;

// TODO: subclass off of Dtree ...
class svlCudaDecisionTreeSet : public svlDecisionTreeSet {
protected:
	int *treei;
	float *treef;
	svlCudaLine *dtreei, *dtreef;

public:
	svlCudaDecisionTreeSet();
	svlCudaDecisionTreeSet(const char *file);
	virtual ~svlCudaDecisionTreeSet();

	// Accessors.
	inline int getNtrees() { return _nTrees; }

	// Tree evaluation functions.
	inline void collectResults(svlCudaLine *features, svlCudaLine *res, int n_fvs, int feature_stride) const {
		svlCudaDecisionTreeSetCollectResults(features, res, _nTrees, n_fvs, feature_stride);
	}
	inline void evaluate(svlCudaLine *features, svlCudaLine *invalid, int feature_len, int feature_stride, int n_fvs, int execs_per_block = 100) const {
		svlCudaDecisionTreeSet15Evaluate(dtreei, dtreef, invalid, _nTrees, features, feature_len, feature_stride, n_fvs, execs_per_block);
	}
	void hostEvaluate(const float *features, float *invalid, float *res, int feature_len, int feature_stride, int n_fvs) const;
	void hostEvaluate(const MatrixXd &X, float *res) const;

	// I/O
	bool load(XMLNode &node);
#if 0
	bool load(const char *filename);
	bool save(const char *filename) const {
		SVL_LOG(SVL_LOG_WARNING, "save not in use in Cuda decision tree class");
		return false;
	}
	bool save(XMLNode &node) const {
		SVL_LOG(SVL_LOG_WARNING, "save not in use in Cuda decision tree class");
		return false;
	}

	// Obscured virtual members.
	double getScore(const VectorXd  &sample) const {
		SVL_LOG(SVL_LOG_WARNING, "getScore not in use in Cuda decision tree class");
		return -1.0;
	}
	double train(const MatrixXd & samples, const VectorXi & labels) {
		SVL_LOG(SVL_LOG_WARNING, "train not in use in Cuda decision tree class");
		return -1.0;
	}
#endif

protected:
	// Free memory.
	void freeMem();
	// Read data out of a decision tree and cache in linear format.
	void readFromDecisionTree(svlDecisionTree *dt, double w, int *iptr, float *fptr);
};


// Special line for use with features.

// TODO paulb: Right now tree evaluation is in-place, overwriting the features. An option not to?
class svlCudaFeatureLine {
public:
	svlCudaLine *_features,		// Storage for features.
				*_invalid,		// Invalid flags.
				*_results;		// Decision tree results.
	int _n_features,				// Number of features (not necessarily storage stride).
		_feature_stride,			// Storage stride of features on device.
		_tree_aligned_stride,	// If stride needs to be bumped up for tree size.
		_n;						// Number of allocated vectors.
	bool _native_pow_2,			// True if rounded-up stride is equal to original stride, which dictates using cumsum-style summation instead of scan-style.
		_need_objects;			// If true, allocate _invalid and _results.

public:
	svlCudaFeatureLine(int n_features, int _nTrees, bool need_objects = true) {
		init(n_features, _nTrees, need_objects);
		/*_n_features = n_features;
		_need_objects = need_objects;
		if ( _nTrees > 0 ) {
			_tree_aligned_stride = svlCudaMath::iAlignUpPow2(_nTrees);
			_feature_stride = max( _tree_aligned_stride, _n_features );
			_native_pow_2 = _nTrees == _feature_stride;
		} else {
			_tree_aligned_stride = -1;
			_feature_stride = _n_features;
			_native_pow_2 = false;
		}
		_n = -1;
		_features = NULL;
		_invalid = NULL;
		_results = NULL;*/
	}
	svlCudaFeatureLine(int n_features, svlCudaDecisionTreeSet *tree) {
		init(n_features, tree ? tree->getNtrees() : -1, tree != NULL);
	}
	virtual ~svlCudaFeatureLine() {
		clear();
	}
	void init(int n_features, int _nTrees, bool need_objects = true) {
		_n_features = n_features;
		_need_objects = need_objects;
		if ( _nTrees > 0 ) {
			_tree_aligned_stride = svlCudaMath::iAlignUpPow2(_nTrees);
			_feature_stride = max( _tree_aligned_stride, _n_features );
			_native_pow_2 = _nTrees == _feature_stride;
		} else {
			_tree_aligned_stride = -1;
			_feature_stride = _n_features;
			_native_pow_2 = false;
		}
		_n = -1;
		_features = NULL;
		_invalid = NULL;
		_results = NULL;
	}

	inline int featureSize() {
		return _n * _feature_stride;
	}
	void alloc(int n) {
		// Only explicitly reallocate if we need more room.
		if ( _n <= 0 || n > _n ) {
			clear();
			_features = cm->alloc(_feature_stride * n);
			if ( _need_objects ) {
				_invalid = cm->alloc(n);
				_results = cm->alloc(n);
			}
		}
		_n = n;
	}
	void alloc(int n, bool need_objects) {
		// Only explicitly reallocate if we need more room.
		if ( _n <= 0 || n > _n ) {
			clear();
			_need_objects = need_objects;
			_features = cm->alloc(_feature_stride * n);
			if ( _need_objects ) {
				_invalid = cm->alloc(n);
				_results = cm->alloc(n);
			}
		}
		_n = n;
	}
	void clear() {
		if ( _features ) {
			cm->Free(_features);
			if ( _need_objects ) {
				cm->Free(_invalid);
				cm->Free(_results);
			}
		}
		_n = -1;
	}

	void evaluate(const svlCudaDecisionTreeSet *tree) {
		 // Clear invalid bits and evaluate trees over features.
		cm->lineSet(_invalid, 0);
		tree->evaluate(_features, _invalid, _n_features, _feature_stride, _n);
		// Integrate features to get response of all trees.
		svlCudaMath::integralLine(_features, _features, _n*_feature_stride, _tree_aligned_stride, _native_pow_2, _feature_stride);
		// Compact results and read back out.
		tree->collectResults(_features, _results, _n, _feature_stride);
	}
};


