/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2009, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaML.cu
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  Cuda machine learning functions.
**
*****************************************************************************/

#include "svlCudaCommon.h"
#include "svlCudaMemHandler.h"

extern svlCudaMemHandler *cm;


// Collect results from feature computation.
__global__ void collect_results_slice(float *features, float *results, int ix, int n_fvs, int feature_stride)
{
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;
	if ( x >= n_fvs )
		return;

	results[x] = features[ IMUL(x, feature_stride) + ix ];
}

// Collect feature results into res (by reading to end of every n_trees'th entry).
void svlCudaDecisionTreeSetCollectResults(svlCudaLine *features, svlCudaLine *res, int n_trees, int n_fvs, int feature_stride)
{
	dim3 block(256);
	dim3 grid( iDivUp(n_fvs, block.x) );
	if ( n_trees == feature_stride ) {
		// This means we did cumsum-style integration so as not to overrun feature_stride, so the reference index is one less.
		--n_trees;
	}
	//printf("Collecting n: %d; fstride: %d; fvs: %d\n", n_trees, feature_stride, n_fvs);
	collect_results_slice<<< grid, block >>>(features->data, res->data, n_trees, n_fvs, feature_stride);
}

// Evaluate decision tree on 15-node model.
__global__ void dtree15_eval_slice(const int *dtreei, const float *dtreef, float *invalid, int n_trees, float *feats, int feature_len, int feature_stride, int n_fvs, int execs_per_block)
{
	// Max tree size
	__shared__ float data[ 1538 ];

	// Use threadIdx.x for tree index.
	const int tree_ix = threadIdx.x;
	//if ( tree_ix >= n_trees )
	//	return; // Just to make sure.

	if ( tree_ix < n_trees ) {
		// Offsets to tree integer and float constants in the dtreei and dtreef data structure.
		const int tixi = 7*tree_ix;
		const int tixf = 21*tree_ix;

		// Process all of the results assigned to us.
		int fv_i = blockIdx.x;
		for ( int i=0; i<execs_per_block; ++i ) {
			// Check if we have run out of features.
			if ( fv_i >= n_fvs )
				return;

			// Read in data.
			const int feature_ix = IMUL(feature_stride, fv_i);
			if ( n_trees >= feature_len ) {
				// More than enough threads for every feature.
				if ( tree_ix < feature_len ) {
					const float gf = data[tree_ix] = feats[ feature_ix + tree_ix ];
					if ( isinf(gf) || isnan(gf) )
						invalid[fv_i] = 1.0f; // Set invalid bit (collision-prone).
				}
			} else {
				for ( int j=tree_ix; j<feature_len; j+=n_trees ) {
					const float gf = data[j] = feats[ feature_ix + j ];
					if ( isinf(gf) || isnan(gf) )
						invalid[fv_i] = 1.0f; // Set invalid bit (collision-prone).
				}
			}

			// Sync after all data reads.
			__syncthreads();

			float gf = 0.0f;
			if ( data[ dtreei[tixi] ] <= dtreef[tixf] ) {
				// L
				if ( dtreei[tixi + 1] < 0 )
					gf = dtreef[tixf + 2];
				else if ( data[ dtreei[tixi + 1] ] <= dtreef[tixf + 1] ) {
					// LL
					if ( dtreei[tixi + 2] < 0 )
						gf = dtreef[tixf + 4];
					else if ( data[ dtreei[tixi + 2] ] <= dtreef[tixf + 3] )
						gf = dtreef[tixf + 5];
					else
						gf = dtreef[tixf + 6];
				} else {
					// LR
					if ( dtreei[tixi + 3] < 0 )
						gf = dtreef[tixf + 8];
					else if ( data[ dtreei[tixi + 3] ] <= dtreef[tixf + 7] )
						gf = dtreef[tixf + 9];
					else
						gf = dtreef[tixf + 10];
				}
			} else {
				// R
				if ( dtreei[tixi + 4] < 0 )
					gf = dtreef[tixf + 12];
				else if ( data[ dtreei[tixi + 4] ] <= dtreef[tixf + 11] ) {
					// RL
					if ( dtreei[tixi + 5] < 0 )
						gf = dtreef[tixf + 14];
					else if ( data[ dtreei[tixi + 5] ] <= dtreef[tixf + 13] )
						gf = dtreef[tixf + 15];
					else
						gf = dtreef[tixf + 16];
				} else {
					// RR
					if ( dtreei[tixi + 6] < 0 )
						gf = dtreef[tixf + 18];
					else if ( data[ dtreei[tixi + 6] ] <= dtreef[tixf + 17] )
						gf = dtreef[tixf + 19];
					else
						gf = dtreef[tixf + 20];
				}
			}
			feats[ feature_ix + tree_ix ] = gf;

			// Move feature n through grid size.
			fv_i += gridDim.x;

			__syncthreads();
		}
	}
}

// Evaluates a 15-node (depth-3) decision tree.
void svlCudaDecisionTreeSet15Evaluate(const svlCudaLine *dtreei, const svlCudaLine *dtreef, svlCudaLine *invalid, int n_trees, svlCudaLine *features, int feature_len, int feature_stride, int n_fvs, int execs_per_block)
{
	dim3 block(1), grid(1);
	if ( n_trees > 512 ) {
		// Too many trees to put all in one block; use blocking in y.
		// ...
	} else {
		// One thread for every tree.
		block.x = iAlignUp(n_trees, 32);
		//block.x = iAlignUp(n_trees, 128);
		// Each block will process execs_per_block results, so only grid in x for the remainder.
		grid.x = iDivUp( n_fvs, execs_per_block );
	}
	dtree15_eval_slice<<< grid, block >>>((int*)dtreei->data, dtreef->data, invalid->data, n_trees,
		features->data, feature_len, feature_stride, n_fvs, execs_per_block);
}


// Old, smaller decision trees.
#if 0
__global__ void dtree_eval_slice(const float *dtree, int n_trees, float *feats, int feature_len, int feature_stride, int n_fvs, int execs_per_block)
{
	// Max tree size
	__shared__ float data[ 512 ];

	// Use threadIdx.x for tree index.
	if ( threadIdx.x >= n_trees )
		return;

	// Read in tree constants.
	const int tix = 11*threadIdx.x;
	const int root_ix = ((int*)dtree)[tix],
		l_ix    = ((int*)dtree)[tix + 2],
		r_ix    = ((int*)dtree)[tix + 6];
	const float root_les = dtree[tix + 1],
		l_les    = dtree[tix + 3],
		r_les    = dtree[tix + 7],
		ll_val   = dtree[tix + 4],
		lr_val   = dtree[tix + 5],
		r_val    = dtree[tix + 8],
		rl_val   = dtree[tix + 9],
		rr_val   = dtree[tix + 10];

	// Process all of the results assigned to us.
	int feature_n = blockIdx.x;
	for ( int i=0; i<execs_per_block; ++i ) {
		// Check if we have run out of features.
		if ( feature_n >= n_fvs )
			break;

		// Read in data.
		const int feature_ix = IMUL(feature_stride, feature_n);
		if ( blockDim.x >= feature_len ) {
			// More than enough threads for every feature.
			if ( threadIdx.x < feature_len ) { // TOTRY paulb: Might be able to take out checks and see if they speed anything appreciably.
				data[threadIdx.x] = feats[ feature_ix + threadIdx.x ];
			}
		} else {
			int j = threadIdx.x;
			do {
				data[j] = feats[ feature_ix + j ];
				j += blockDim.x;
				// __syncthreads(); ? To make the threads update in lockstep?
			} while ( j < feature_len );
		}

		// Sync after all data reads.
		__syncthreads();

		// Evaluate model.
		float gf = 0.0f;
		if ( data[root_ix] <= root_les ) {
			// Check left child.
			if ( data[l_ix] <= l_les )
				gf = ll_val;
			else
				gf = lr_val;
		} else if ( r_ix >= 0 ) {
			// Check right child.
			if ( data[r_ix] <= r_les )
				gf = rl_val;
			else
				gf = rr_val;
		} else {
			// Just assign the right value.
			gf = r_val;
		}

		// Move feature n through grid size.
		feature_n += gridDim.x;

		// Sync after we have read in features.
		__syncthreads();

		// Overwrite features with components of results.
		feats[ feature_ix + threadIdx.x ] = gf;
	}
}

void svlCudaDecisionTreeSetEvaluate(const svlCudaLine *dtree, int n_trees, svlCudaLine *features, int feature_len, int feature_stride, int n_fvs, int execs_per_block)
{
	//cout << "Running tree evaluation.\n";
	dim3 block(1), grid(1);
	if ( n_trees > 512 ) {
		// Too many trees to put all in one block; use blocking in y.
		// ...
	} else {
		// One thread for every tree in the "decision forest."
		block.x = iAlignUp(n_trees, 16);
		// Each block will process execs_per_block results, so only grid in x for the remainder.
		grid.x = iDivUp( n_fvs, execs_per_block );
	}
	/*printf("block: %d,%d\n", block.x, block.y);
	printf("grid: %d,%d\n", grid.x, grid.y);
	printf("execs: %d; stride: %d; len: %d\n", execs_per_block, feature_stride, feature_len);*/
	dtree_eval_slice<<< grid, block >>>(dtree->data, n_trees, features->data, feature_len, feature_stride, n_fvs, execs_per_block);
}
#endif


