/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlFinalPatchDictionary.h
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**   Stores a patch dictionary.
** 
*****************************************************************************/


#pragma once

#include <cassert>
#include <iostream>
#include <vector>

#include "cv.h"
#include "cxcore.h"

#include "xmlParser/xmlParser.h"
#include "svlBase.h"
#include "../vision/svlPatchDefinition.h"

using namespace std;

// A patch dictionary.
class svlFinalPatchDictionary {
public:
	CvSize _windowSize;				// Window size
	vector<svlPatch*> *_entries;	// Patches
	int min_width, max_width, min_height, max_height;

public:
	svlFinalPatchDictionary(const char *file);
	svlFinalPatchDictionary(unsigned width = 0, unsigned height = 0);
	svlFinalPatchDictionary(const svlFinalPatchDictionary& d);
	virtual ~svlFinalPatchDictionary();

	void clear();
	bool read(const char *filename, bool normalize = false);
	bool read(XMLNode &root, bool normalize = false);
	static svlPatch* readPatch(XMLNode& node, bool normalize);

	inline int numFeatures() const {
		return int(_entries->size());
	}
	void print();
	void print(int n);
	void print(int n, int m);

	// Remove unnecessary patches.
	void decimate(const vector<int> &ix);
};


