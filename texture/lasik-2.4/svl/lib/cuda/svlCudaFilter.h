/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaFilter.h
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  Filter functions for Cuda.
**
*****************************************************************************/

#pragma once

#ifndef __SVL_CUDA_FILTER_H
#define __SVL_CUDA_FILTER_H

#include <iostream>
#include <vector>

#include "cv.h"
#include "cxcore.h"

#include "svlBase.h"
#include "svlCudaCommon.h"
#include "svlCudaMemHandler.h"
using namespace std;

class svlCudaFilter {
public:
	svlCudaFilter();
	virtual ~svlCudaFilter();

	// *** Functions VERIFIED in cudaVerify app ***

	// Create a soft edge map with Sobel convolution.
	inline static void sobel(const svlCudaPitch *pin, svlCudaPitch *edge, svlCudaPitch *temp1, svlCudaPitch *temp2, int img_w = -1, int img_h = -1) {
		svlCudaSobel(pin, edge, temp1, temp2, img_w, img_h);
	}


	// *** UNVERIFIED or old functions ***

	// Resize a pitch (downwards).
	inline static void resize(const svlCudaPitch *pin, svlCudaPitch *pout, int new_w, int new_h, svlCudaPitch *temp1, svlCudaPitch *temp2, int img_w = -1, int img_h = -1) {
		SVL_ASSERT_MSG(new_w <= (img_w == -1 ? pin->w : img_w) && new_h <= (img_h == -1 ? pin->h : img_h), "Resize function only minifies");
		svlCudaResize(pin, pout, new_w, new_h, temp1, temp2, img_w, img_h);
	}
	// The above, but with a CvSize parameter for the new size.
	inline static void resize(const svlCudaPitch *pin, svlCudaPitch *pout, CvSize sz, svlCudaPitch *temp1, svlCudaPitch *temp2, int img_w = -1, int img_h = -1) {
		resize(pin, pout, sz.width, sz.height, temp1, temp2, img_w, img_h);
	}
};

#endif


