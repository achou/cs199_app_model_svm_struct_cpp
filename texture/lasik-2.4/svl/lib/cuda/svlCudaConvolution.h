/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaConvolution.h
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  Handles NVIDIA Cuda memory interface.
**
*****************************************************************************/

#pragma once

#ifndef __SVL_CUDA_CONVOLUTION_H
#define __SVL_CUDA_CONVOLUTION_H

#include <iostream>
#include <vector>

#include "cv.h"
#include "cxcore.h"

#include "svlCudaCommon.h"
#include "svlCudaMemHandler.h"
#include "svlCudaTimer.h"
using namespace std;

// Convolution styles.
#define CUDA_TM_CCORR			0
#define CUDA_TM_CCOEFF			1 // Not implemented
#define CUDA_TM_CCOEFF_NORMED	2

// Convolution sizes.
#define CUDA_TM_SAME	0
#define CUDA_TM_VALID	1
#define CUDA_TM_FULL	2

class svlCudaConvolution {
public:
	svlCudaConvolution();
	virtual ~svlCudaConvolution();

	// *** Functions VERIFIED in cudaVerify app ***
public:
	// 2d convolution with 'same' or 'full' size.
	static void cudaMatchTemplate(const svlCudaPitch *pin, const float *patch, int pw, int ph, svlCudaPitch *pout,
		int type = CUDA_TM_CCORR, int size = CUDA_TM_SAME, int img_w = -1, int img_h = -1, svlCudaPitch *temp = NULL);
	// The above but accepting an IplImage for the patch.
	static void cudaMatchTemplate(const svlCudaPitch *pin, const IplImage *patch, svlCudaPitch *pout,
		int type = CUDA_TM_CCORR, int size = CUDA_TM_SAME, int img_w = -1, int img_h = -1, svlCudaPitch *temp = NULL);

	// 2d convolution with 3d pitches.
	static void cudaMatchTemplate(const svlCudaPitch3d *pitch_in, const float *patch, int pw, int ph, svlCudaPitch3d *pitch_out,
		int type = CUDA_TM_CCORR, int size = CUDA_TM_SAME, int img_w = -1, int img_h = -1);
	static void cudaMatchTemplate(const svlCudaPitch3d *pitch_in, const IplImage *patch, svlCudaPitch3d *pitch_out,
		int type = CUDA_TM_CCORR, int size = CUDA_TM_SAME, int img_w = -1, int img_h = -1);
	
	// Curious, Lasik-corrected convolution to output convolution, sums, and sum-2s.
	// (Verified by sliding window feature extraction)
	inline static void lasikPatchConv2(const svlCudaPitch *pin, CachedPatch *cp, const svlCudaPitch *integ, const svlCudaPitch *integ2,
		svlCudaPitch *conv, svlCudaPitch *sum, svlCudaPitch *sum2, int img_w = -1, int img_h = -1) {
		svlCudaLasikConvolution(pin, cp, integ, integ2, conv, sum, sum2, img_w, img_h);
	}

	// Super-size convolution between pitches.
	static void cudaPitchConv(const svlCudaPitch *pin, const svlCudaPitch *patch, svlCudaPitch *pout, const svlCudaPitch *orig_patch = NULL,
		int img_w = -1, int img_h = -1, int out_w = -1, int out_h = -1);

protected:
	// Internal functions.
	static void cudaMatchTemplate_16(const svlCudaPitch *pin, const float *patch, int pw, int ph, svlCudaPitch *pout, int type, int size, int img_w, int img_h);
	static void cudaMatchTemplate_16(const svlCudaPitch *pin, const IplImage *patch, svlCudaPitch *pout, int type, int size, int img_w, int img_h);
	static void cudaMatchTemplate_32(const svlCudaPitch *pin, const float *patch, int pw, int ph, svlCudaPitch *pout, svlCudaPitch *temp,
		int type, int size, int img_w, int img_h);
	//static void cudaMatchTemplate_32(const svlCudaPitch *pin, const IplImage *patch, svlCudaPitch *pout, int type, int size, int img_w, int img_h);
	// Normalized correlation coefficient via direct summation over the image to compute the image statistics.
	static void ccoeffNormed(const svlCudaPitch *pitch_in, const float *patch, int pw, int ph, svlCudaPitch *pitch_out, int img_w, int img_h, int size);
	static void ccoeffNormed(const svlCudaPitch *pitch_in, const IplImage *patch, svlCudaPitch *pitch_out, int img_w, int img_h, int size);


	// *** UNVERIFIED or old functions ***

public:
	// Get pre-computed window sums and sum-squareds.
	inline static void windowSumSumSq(const svlCudaPitch *pin, svlCudaPitch *sum, svlCudaPitch *sum2, int win_w, int win_h, int img_w = -1, int img_h = -1) {
		svlCudaWindowSumSumSq(pin, sum, sum2, win_w, win_h, img_w, img_h);
	}
	// Normalize an image using aligned sum and sum-squared images.
	inline static void patchNormalize(svlCudaPitch *pout, svlCudaPitch *sum, svlCudaPitch *sum2, float patch_mean, float patch_std, int patch_N, int img_w = -1, int img_h = -1) {
		svlCudaPatchNormalize(pout, sum, sum2, patch_mean, patch_std, patch_N, img_w, img_h);
	}

	// Patch-sized window normalization
	inline static void windowNormalize(svlCudaPitch *pitch_in, const svlCudaPitch *integral, const svlCudaPitch *integral2,
		float patch_mean, float patch_std, int patch_w, int patch_h, int img_w = -1, int img_h = -1) {
		svlCudaWindowNormalize(*pitch_in, *integral, *integral2, patch_mean, patch_std, patch_w, patch_h, img_w, img_h);
	}
	// Normalize with off-by-one integral images.
	inline static void windowNormalize1(svlCudaPitch *pitch_in, const svlCudaPitch *integral, const svlCudaPitch *integral2,
		float patch_mean, float patch_std, int patch_w, int patch_h, int img_w = -1, int img_h = -1) {
		svlCudaWindowNormalize1(*pitch_in, *integral, *integral2, patch_mean, patch_std, patch_w, patch_h, img_w, img_h);
	}
	inline static void windowNormalize1DimSub(svlCudaPitch *pitch_in, const svlCudaPitch *integral, const svlCudaPitch *integral2,
		float patch_mean, float patch_std, int patch_w, int patch_h, int img_w = -1, int img_h = -1, int dim = 128) {
		svlCudaWindowNormalize1DimSub(*pitch_in, *integral, *integral2, patch_mean, patch_std, patch_w, patch_h, img_w, img_h, dim);
	}
	inline static void windowNormalizeRow(svlCudaPitch *pin, svlCudaPitch *pout, int patch_w, int img_w = -1, int img_h = -1, int dim = 128) {
		svlCudaWindowNormalizeRow(pin, pout, patch_w, img_w, img_h, dim);
	}


	// TODO paulb: Update and verify.
	// Calculates normalized correlation coefficient via row-integral images.
	//static void ccoeffNormed(const svlCudaPitch *pin, const IplImage *patch, svlCudaPitch *pout, svlCudaPitch *integ, svlCudaPitch *integ2, int img_w, int img_h);
	// The above with patches already padded and cached on the device.
	/*inline static void ccoeffNormed(const svlCudaPitch *pin, svlCudaPitch *pout, float *ppatch, float *pmask, svlCudaPitch *integ, svlCudaPitch *integ2,
		int patch_w, int patch_h, int patch_numel, float patch_mean, float patch_std, int img_w = -1, int img_h = -1) {
		svlCudaCcoeffNormed2d_v3(pin, pout, ppatch, pmask, integ, integ2, patch_w, patch_h, patch_numel, patch_mean, patch_std, img_w, img_h, true);
	}*/

	// Calculations normalized cross-correlation using row-integral-squared images.
	/*inline static void ccorrNormed(const svlCudaPitch *pin, svlCudaPitch *pout, const float *patch, const svlCudaPitch *integ, const svlCudaPitch *integ2,
		const svlCudaPitch *meds, int patch_w, int patch_h, int patch_real_numel, float patchSum, float patchNorm, int img_w, int img_h) {
		svlCudaCcorrNormed(pin, pout, patch, integ, integ2, meds, patch_w, patch_h, patch_real_numel, patchSum, patchNorm, img_w, img_h, true);
	}*/

	// Using pre-padded patches and masks.
	/*inline static void normCCorrPadded(const svlCudaPitch *pitch_in, svlCudaPitch *pitch_out, float *patch, float *mask,
		int patch_w, int patch_h, int patch_real_numel, float patch_mean = 0.0f, float patch_std = 1.0f, int img_w = -1, int img_h = -1,
		svlCudaCoveringGrid *cudagrid = NULL) {
		svlCudaCcoeffNormed2d(*pitch_in, *pitch_out, patch, mask, patch_w, patch_h, patch_real_numel, patch_mean, patch_std, img_w, img_h, cudagrid);
	}*/


	// *** Miscellaneous functions ***
	static pair<int,int> halfPad(const float *patch, int pw, int ph, float* &pp, int sub_w = -1, int sub_h = -1, int w_off = 0, int h_off = 0);
	static pair<int,int> halfPad(const IplImage *patch, float* &pp, int w_off = 0, int h_off = 0);
	static pair<int,int> halfPadLeft(const float *patch, int pw, int ph, float* &pp);
	static pair<int,int> halfPadLeft(const IplImage *patch, float* &pp);
	// Pad a 32-by-32 patch into multiple, 16-by-16 sub-patches.
	static void halfPad32(const float *patch, int pw, int ph, vector<PaddedPatch> &pps);
	static pair<int,int> paddedPatchSize(const IplImage *patch);
	static void padPatchWithMask(const IplImage *patch, float *mem);
	static void padPatchOnly(const IplImage *patch, float *mem);
};

#endif


