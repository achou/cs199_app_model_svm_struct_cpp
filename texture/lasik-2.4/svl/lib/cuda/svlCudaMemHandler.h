/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaMemHandler.h
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  Handles Cuda memory interface.
**
*****************************************************************************/

#pragma once

#ifndef __SVL_CUDA_MEM_HANDLER
#define __SVL_CUDA_MEM_HANDLER

#include <iostream>
#include <vector>
#include <list>

#include "cv.h"
#include "cxcore.h"

#include "svlCudaCommon.h"
using namespace std;

typedef pair<svlCudaLine*,const char*> NamedLine;
typedef pair<svlCudaPitch*,const char*> NamedPitch;
typedef pair<svlCudaPitch3d*,const char*> NamedPitch3d;

class svlCudaMemHandler {
protected:
	static list<NamedLine> nls;
	static list<NamedPitch> nps;
	static list<NamedPitch3d> np3s;
	static bool track_allocations;

public:
	svlCudaMemHandler();
	virtual ~svlCudaMemHandler();

	// Memory allocation tracking functions.
	static bool getTracking() { return track_allocations; }
	static void setTracking(bool in_the_track) { track_allocations = in_the_track; }
	static void printTotalAllocation();
	static void printAllAllocations();
	static bool activeAllocations() {
		return !nls.empty() || !nps.empty() || !np3s.empty();
	}

	static bool testMemoryAccess(int w = 640, int h = 480);

	// *** Memory management functions ***
	inline static svlCudaLine* alloc(int w, const char *name = NULL) {
		return allocLine(w,name);
	}
	inline static svlCudaPitch* alloc(int w, int h, const char *name = NULL) {
		return allocPitch(w,h,name);
	}
	inline static svlCudaPitch* alloc(const IplImage *img, const char *name = NULL) {
		return allocPitch(img,name);
	}
	inline static svlCudaPitch3d* alloc(int w, int h, int z, const char *name = NULL) {
		return allocPitch3d(w,h,z,name);
	}
	inline static void Free(svlCudaLine* &n) {
		freeLine(n);
	}
	inline static void Free(svlCudaPitch* &p) {
		freePitch(p);
	}
	inline static void Free(svlCudaPitch3d* &p3) {
		freePitch3d(p3);
	}
	// Allocate linear device memory.
	static svlCudaLine* allocLine(int w, const char *name = NULL);
	static void freeLine(svlCudaLine* &n);
	// Allocate device memory in the form a pitch and return addressable index.
	static svlCudaPitch* allocPitch(int w, int h, const char *name = NULL);
	static svlCudaPitch* allocPitch(const IplImage *img, const char *name = NULL); // Allocate with size of IplImage and copy data over.
	static svlCudaPitch* allocPitch3d(svlCudaPitch *p, const char *name = NULL);
	static void freePitch(svlCudaPitch* &p);
	// Allocate and free 3d pitches.
	static svlCudaPitch3d* allocPitch3d(int w, int h, int z, const char *name = NULL);
	static svlCudaPitch3d* allocPitch3d(svlCudaPitch3d *p, const char *name = NULL);
	static void freePitch3d(svlCudaPitch3d* &p);
	

	// *** Memory transfer functions ***
	// Line functions.
	// Copy from GPU to linear memory.
	static inline void lineFromGPU(float *dest, const svlCudaLine *n, int w = -1) {
		svlCudaLineFromGPU(dest, *n, w);
	}
	// Copy linear memory to GPU pitch.
	static inline void lineToGPU(svlCudaLine *n, float *src, int w = -1) {
		svlCudaLineToGPU(*n, src, w);
		n->source = (void*)src;
	}
	// Pitch functions.
	// Copy from GPU to linear memory.
	static inline void pitchFromGPU(float *dest, const svlCudaPitch *p, int img_w = -1, int img_h = -1) {
		svlCudaPitchFromGPU(dest, p, img_w, img_h);
	}
	static inline void pitch3dFromGPU(float *dest, const svlCudaPitch3d *p, int img_w = -1, int img_h = -1) {
		svlCudaPitch3dFromGPU(dest, p, img_w, img_h);
	}
	// Copy linear memory to GPU pitch.
	static inline void pitchToGPU(svlCudaPitch *p, const float *src, int w = -1, int h = -1) {
		svlCudaPitchToGPU(p, src, w, h);
		p->source = (void*)src;
	}
	// Copy linear memory to GPU pitch3d.
	static inline void pitch3dToGPU(svlCudaPitch3d *p, const float *src) {
		svlCudaPitch3dToGPU(p, src);
		p->source = (void*)src;
	}
	static inline void pitchToPitch(svlCudaPitch *dest, const svlCudaPitch *src, int img_w = -1, int img_h = -1) {
		svlCudaPitchToPitch(dest, src, img_w, img_h);
		dest->source = src->source;
	}
	// Copy linear float to a pitch on the GPU.
	static inline void floatToGPU(float *src, svlCudaPitch *p) {
		svlCudaPitchToGPU(p, src);
		p->source = (void*)src;
	}

	// Set memory.
	static inline void memset(svlCudaLine *p, int value = 0, int w = -1) {
		svlCudaMemset(p, value, w);
	}
	static inline void memset(svlCudaPitch *p, int value = 0, int w = -1, int h = -1) {
		svlCudaMemset2D(p, value, w, h);
	}
	static inline void lineSet(svlCudaLine *p, int value = 0, int w = -1) {
		svlCudaMemset(p, value, w);
	}
	static inline void pitchSet(svlCudaPitch *p, int value = 0, int w = -1, int h = -1) {
		svlCudaMemset2D(p, value, w, h);
	}
	static inline void pitch3dSet(svlCudaPitch3d *p, int value = 0, int w = -1, int h = -1, int z = -1) {
		svlCudaPitch t = p->getPitch(0);
		svlCudaMemset2D(&t, value, w, h*( z == -1 ? p->z : z ) );
	}


	// Copy float Ipl to GPU (TODO paulb: this as an optimized template).
	/*static inline void floatIplToGPU(const IplImage *im, svlCudaPitch *p) {
		svlCudaPitchToGPU_all(p, (float*)im->imageData, im->widthStep, im->width, im->height);
	}
	static inline void ucharIplToGPU(const IplImage *im, svlCudaPitch *p) {
		svlCudaPitchToGPU_all(p, (float*)im->imageData, im->widthStep, iDivUp(im->width,sizeof(float)), im->height);
	}*/
	// Copy a pitch out from the GPU into dest.
	//static inline void floatFromGPU(float *dest, svlCudaPitch *p) {
	//	svlCudaPitchFromGPU(dest, p);
	//}
	//// Copy a pitch out from the GPU into dest using width and height.
	//static inline void floatFromGPU(float *dest, int img_w, int img_h, svlCudaPitch *p) {
	//	svlCudaPitchFromGPU(dest, p, img_w, img_h);
	//}

	static void IplToGPU(svlCudaPitch *p, const IplImage *im, int row_offset = 0, int col_offset = 0);
	static void IplFromGPU(IplImage *im, const svlCudaPitch *p, int row_offset = 0, int col_offset = 0);

	// Random-access indexing functions.
	static int fillIndexImage(const vector<CvPoint> &locations, IplImage *ixage); // Returns max populated row.
	static void fillIndexLine(const vector<CvPoint> &locations, svlCudaLine *ixline);
	inline static void addXYpoint2Line(const svlCudaLine *lin, int x, int y, svlCudaLine *lout, int w = -1, bool yx = false) {
		svlCudaXYpoint2Line(lin, x, y, lout, w, yx);
	}
	// Row random access functions.
	static inline void rowRandomAccess(const IplImage *ixes, IplImage *data_out, const vector<int> row_counts, float *dest, svlCudaPitch *data_p, svlCudaPitch *addy_p) {
		svlCudaRowRandomAccess(ixes, data_out, row_counts, dest, *data_p, *addy_p);
	}
	// Linear stacked random access.
	static inline void linearRandomAccess(const IplImage *ixes, int max_row, float *dest, svlCudaPitch *data_p, svlCudaPitch *addy_p, svlCudaPitch *data_out_p) {
		svlCudaLinearRandomAccess(ixes, max_row, dest, *data_p, *addy_p, *data_out_p);
	}
	// Reverse of the above: use indices to fill a 2D pitch from a line.
	static inline void fill2DRandomAccessInt(svlCudaPitch *pout, svlCudaLine *ixline, svlCudaLine *res, int w = -1, bool yx = false) {
		svlCudaFill2DRandomAccessInt(pout, ixline, res, w, yx);
	}
	// Write a line into a pitch using pitch indices.
	static inline void line2pitchIx(svlCudaPitch *pout, const svlCudaLine *ixline, const svlCudaLine *res, int w = -1, bool yx = false) {
		svlCudaLine2pitchIx(pout, ixline, res, w, yx);
	}
	// Write indices from a pitch to a line with a stride.
	static inline void pitchIx2line(const svlCudaPitch *data, const svlCudaLine *ixline, svlCudaLine *res, int w = -1, bool yx = false, int line_off = 0, int line_stride = 1) {
		svlCudaPitchIx2line(ixline, data, res, w, yx, line_off, line_stride);
	}

	// Align padded to the given width.
	// If it is already aligned, nothing is done, padded is set to orig and orig is set to NULL. Otherwise, padded becomes the padded pitch.
	static void alignPitchWidth(svlCudaPitch* &orig, svlCudaPitch* &padded, int align = 16);

	template<class C_IN> static void vector2FloatLine(const vector<C_IN> &vec, svlCudaLine *line);
	template<class C_IN> static void vector2IntLine(const vector<C_IN> &vec, svlCudaLine *line);
	static void CvPoint2IntLine(const vector<CvPoint> &vec, svlCudaLine *line, bool yx = false);

	// File I/O.
	static void line2file(const char *file, const svlCudaLine *n, int w = -1);
	static void ucharPitch2file(const char *file, const svlCudaPitch *p);
	static void pitch2file(const char *file, const svlCudaPitch *p);
	static void pitch3d2file(const char *file, const svlCudaPitch3d *p);
	static svlCudaPitch3d* file2pitch3d(const char *file);

	// *** Miscellaneous access functions ***
	static inline bool pitchHasCached(void *ptr, svlCudaPitch *p) {
		return p->source == ptr;
	}
};

// Fill a line with the contents of a vector.
template<class C_IN>
void svlCudaMemHandler::vector2FloatLine(const vector<C_IN> &vec, svlCudaLine *line)
{
	// Collaps row-column indices into line linearly.
	float *data = (float*)malloc(sizeof(float)*int(vec.size()));
	float *ptr = data;
	for ( typename vector<C_IN>::const_iterator v=vec.begin(); v!=vec.end(); ++v ) {
		*ptr++ = (float)*v;
	}
	lineToGPU(line, data, int(vec.size()));
	free(data);
}

// Fill an int line with the contents of a vector.
template<class C_IN>
void svlCudaMemHandler::vector2IntLine(const vector<C_IN> &vec, svlCudaLine *line)
{
	// Collaps row-column indices into line linearly.
	int *data = (int*)malloc(sizeof(int)*int(vec.size()));
	int *ptr = data;
	for ( typename vector<C_IN>::const_iterator v=vec.begin(); v!=vec.end(); ++v ) {
		*ptr++ = (int)*v;
	}
	lineToGPU(line, (float*)data, int(vec.size()));
	free(data);
}

#endif

