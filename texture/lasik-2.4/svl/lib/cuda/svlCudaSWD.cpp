/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaSWD.cpp
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
**
*****************************************************************************/

#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <sys/types.h>
#include <limits>

#include "cxcore.h"
#include "ml.h"

#include "svlCudaSWD.h"

using namespace std;

#if defined(_WIN32)||defined(WIN32)||defined(__WIN32__)
# undef max
# undef min
# undef USE_THREADS
#endif

extern svlCudaMemHandler *cm;


svlCudaSWD::svlCudaSWD(const char *name, int w, int h)
: svlSlidingWindowDetector(name, w, h)
{
	init();
}

svlCudaSWD::svlCudaSWD(const svlCudaSWD& c)
: svlSlidingWindowDetector( (svlSlidingWindowDetector)c )
{
	init();
}

svlCudaSWD::svlCudaSWD(const svlBinaryClassifier *c, const svlFeatureExtractor *e)
: svlSlidingWindowDetector(c, e)
{
	init();
	_cpdExtractor = new svlCudaPatchDictionary(_extractor);
	if ( !_cpdExtractor ) {
		SVL_LOG(SVL_LOG_FATAL, "Could not instantiate Cuda patch dictionary from extractor");
	}

	// Pad and cache patches on device.
	_cpdExtractor->padAndCache();
}

void svlCudaSWD::init()
{
	_cpdExtractor = NULL;
	_dtree = NULL;
	// Remove any inherited discarders.
	for ( vector<svlWindowDiscarder*>::iterator it=_discarders.begin(); it!=_discarders.end(); ++it )
		delete *it;
	_discarders.clear();
}

svlCudaSWD::~svlCudaSWD()
{
	if ( _cpdExtractor )
		delete _cpdExtractor;
	if ( _dtree )
		delete _dtree;
}

bool svlCudaSWD::load(const char *extractorFilename, const char *modelFilename, const char *pruningFilename)
{
	bool ret = svlSlidingWindowDetector::load(extractorFilename, NULL);

	_cpdExtractor = new svlCudaPatchDictionary(_extractor);
	if ( !_cpdExtractor ) {
		SVL_LOG(SVL_LOG_FATAL, "Could not instantiate Cuda patch dictionary from extractor");
	}
	if ( !ret )
		return ret;

	if ( pruningFilename )
		_cpdExtractor->prune(pruningFilename); // Prune dictionary.

	// Pad and cache patches on device.
	_cpdExtractor->padAndCache();

	if ( modelFilename ) {
		// Read pruned Cuda decision tree.
		if ( _dtree )
			delete _dtree;
		_dtree = new svlCudaDecisionTreeSet(modelFilename);
	}

	return ret;
}

// Computes interesting window locations over a pitch (image).
void svlCudaSWD::createInterestingLocations(const svlCudaPitch *img, svlCudaMultiPitch &mp, int img_w, int img_h, vector<CvPoint>& locations)
{
	if ( mp.getNumPitches(1,0) < 6 ) {
		SVL_LOG(SVL_LOG_FATAL, "Not enough pitches in multi pitch");
		return;
	}
	int maxX = img_w - _windowWidth;
	int maxY = img_h - _windowHeight;
	if ( maxX <= 0 || maxY <= 0 )
		return;

	// Pull three temporary pitches out of the MP, skipping the first three (1,0) because they have been zeralized.
	svlCudaPitch *minp = mp.get(1,0,0),
				 *maxp = mp.get(1,0,1),
				 *resp = mp.get(1,0,2);
	// Get min and max of image.
	svlCudaMath::minMax32x32(img, resp, minp, maxp, img_w, img_h);
	// Perform <max -= min;>.
	svlCudaMath::add(maxp, minp, -1.0f, img_w, img_h);
	// Decimate by factors to retrieve only needed data; maxp to TEMP2.
	svlCudaMath::decimate(maxp, resp, _delta_x, _delta_y, img_w, img_h);

	// Copy out result.
	const int sub_width = img_w / _delta_x;
	const int sub_height = img_h / _delta_y;
	float *back = (float*)malloc(sizeof(float)*sub_width*sub_height);
	cm->pitchFromGPU(back, resp, sub_width, sub_height);

	// Get min_delta of image uniformity conditions.
	float min_delta = 0.125f;
	locations.reserve((maxX + _delta_x) * (maxY + _delta_y) / (_delta_x * _delta_y));
	int ix, iy = 0;
	for (int y=0; y <= maxY; y += _delta_y) {
		ix = 0;
		for (int x=0; x <= maxX; x += _delta_x) {
			if ( back[ iy*sub_width + ix ] > min_delta ) {
				locations.push_back(cvPoint(x, y));
			}
			++ix;
		}
		++iy;
	}
	locations.resize(locations.size()); // Throw away extra pre-allocated things.
	free(back);
}

// Run classification.
void svlCudaSWD::classifyImageF(const vector<IplImage *>& images_in,
								svlObject2dFrame *objects,
								svlFeatureVectorsF *vecs)
{
	/*if ( !ret_objects && !ret_features ) {
		SVL_LOG(SVL_LOG_WARNING, "classifyImage not asked to return anything");
		return;
	}*/ // Overriden for now.
	SVL_ASSERT_MSG(!objects || _dtree, "Missing Cuda decision tree");
	SVL_ASSERT_MSG(_cpdExtractor->getPatchesCached(), "Have not cached patches on device");

	// Assert that we have not been given any NULL images or images of different size.
	vector<IplImage*> images(images_in.size(),NULL), scaledImages(images_in.size(),NULL);
	for ( int i=0; i<int(images_in.size()); ++i ) {
		assert(images_in[i] != NULL);
		assert(images_in[i]->width == images_in[0]->width);
		assert(images_in[i]->height == images_in[0]->height);
		images[i] = create32Fimage(images_in[i]);
		scaledImages[i] = cvCloneImage(images[i]);
	}

	SVL_LOG(SVL_LOG_VERBOSE, "Running classifier on " << int(images_in.size()) << "-channel image");

	// Various important members.
	double scale = double(1), gd;			// Accumulated scaling factor.
	int scaled_w = images[0]->width,
		scaled_h = images[0]->height;		// Set current working image scale.

	// Allocate device memory in form of a multi-pitch.
	if ( !_MP.accommodatesSize(images[0]->width, images[0]->height) ) {
		_MP.setImageSize(images[0]->width, images[0]->height);
		// Have extractor allocate its needed memory.
		_cpdExtractor->allocPitches(_MP);
	}
	// Use a feature line to hold feature space.
	svlCudaFeatureLine features(_cpdExtractor->numFeatures(), objects ? _dtree->getNtrees() : -1, objects != NULL);

	// Code profiling handles.
	const int prof = svlCodeProfiler::getHandle("cSWD :: classifyImage");
	const int eval_model = svlCodeProfiler::getHandle("cSWD :: eval model");
	svlCodeProfiler::tic(prof);

	// Can do sub-7x7 max'es if decimation is >= 4 and no patches have more than 7 as a max dim.
	bool cmax_opt = _delta_x >= 4 && _delta_y >= 4;
	//for ( vector<svlPatch*>::const_iterator p=dict._entries->begin(); cmax_opt && p!=dict._entries->end(); ++p )
	// TODO paulb: Restore this.
	/*for ( unsigned i=0; i<_cpdExtractor->numFeatures(); ++i )
		cmax_opt &= (*p)->rect.width <= 7 && (*p)->rect.height <= 7;*/

	// Iterate over all scales
	while ( scaled_w >= _windowWidth && scaled_h >= _windowHeight )
	{
		// Copy first image over to calculate locations.
		cm->IplToGPU(_MP.get(), scaledImages[0]);
		vector<CvPoint> locations;
		createInterestingLocations(_MP.get(), _MP, scaled_w, scaled_h, locations);
		discardUninteresting(scaledImages, scale, locations);
		int n_locs = int(locations.size());

		if ( !locations.empty() ) {
			// Extract features.
			_cpdExtractor->extract(scaledImages, locations, _MP, features, cmax_opt, 0);

			// Copy out features to return natively.
			if ( vecs ) {
				if ( vecs->_want_scales ) vecs->_scales.push_back(cvSize(scaled_w,scaled_h));
				// Copy locations.
				if ( vecs->_want_locations ) vecs->_locations.push_back(locations);
				// Copy out features.
				if ( vecs->_want_features ) {
					int fs = features._feature_stride;
					float *feats = (float*)malloc( sizeof(float) * features.featureSize() );
					cm->lineFromGPU(feats, features._features, features.featureSize());
					vecs->_features.push_back(vector<vector<float> >());
					vecs->_features.back().reserve(n_locs);
					float *ptr = feats;
					for ( int i=0; i<n_locs; ++i ) {
						vecs->_features.back().push_back(vector<float>(ptr, ptr + fs));
						ptr += fs;
					}
					free(feats);
				}
			}

			// Evaluate decision tree on features.
			if ( objects ) {
				svlCodeProfiler::tic(eval_model);
				features.evaluate(_dtree);
				float *hres = (float*)malloc(sizeof(float)*n_locs);
				float *hinval = (float*)malloc(sizeof(float)*n_locs);
				cm->lineFromGPU(hres, features._results, n_locs);
				cm->lineFromGPU(hinval, features._invalid, n_locs);

				// Push objects on if over score threshold.
				float *ptr = hres, *vtr = hinval;
				int ct = 0;
				//float scoreThreshold = svlBinaryClassifier::probabilityToScore((float)_threshold);
				//float scoreThreshold = _threshold;
				float scoreThreshold = _dtree->probabilityToScore(_threshold);
				//printf("thresh: %0.5f\n", scoreThreshold);
				for ( vector<CvPoint>::const_iterator loc = locations.begin(); loc != locations.end(); ++loc, ++ptr, ++vtr ) {
					if ( isinf(*ptr) || isnan(*ptr) || *vtr != 0.0f || *ptr < scoreThreshold )
						continue;
					++ct;
					objects->push_back(svlObject2d(scale * loc->x, scale * loc->y,
						scale * _windowWidth, scale * _windowHeight));
					objects->back().name = _objectName;
					//objects->back().pr = 1.0 / (1.0 + exp(*ptr));
					_dtree->scoreToProbability(-*ptr);
				}
				free(hres);
				free(hinval);
				svlCodeProfiler::toc(eval_model);
			}
		}

		// Scale down image channels.
		scale *= _delta_scale;
		scaled_w = int( gd = double(images[0]->width) / scale );
		if ( int(gd+1e-4) != scaled_w ) ++scaled_w;
		scaled_h = int( gd = double(images[0]->height) / scale );
		if ( int(gd+1e-4) != scaled_h ) ++scaled_h;
		for ( int i=0; i<int(images.size()); ++i ) {
			cvReleaseImage(&scaledImages[i]);
			scaledImages[i] = cvCreateImage( cvSize(scaled_w, scaled_h), images[i]->depth, images[i]->nChannels);
			cvResize(images[i], scaledImages[i]);
		}
	}

	// Release images.
	for ( int i=0; i<int(images.size()); ++i ) {
		cvReleaseImage(&images[i]);
		cvReleaseImage(&scaledImages[i]);
	}

	svlCodeProfiler::toc(prof);
}
// double version of above.
void svlCudaSWD::classifyImage(const vector<IplImage *>& images_in,
							   svlObject2dFrame *objects,
							   svlFeatureVectors *vecs)
{
	if ( !vecs ) {
		classifyImageF(images_in, objects);
	} else {
		SVL_LOG(SVL_LOG_WARNING, "Called a float extractor asking for double features");
		// Compute double features, then covert back to float.
		svlFeatureVectorsF F;
		classifyImageF(images_in, objects, &F);
		F.convertTo(*vecs);
	}
}

// Run classification at given scale at given locations.
void svlCudaSWD::classifyImageF(const vector<IplImage*> &imagesin,
								const vector<CvPoint>& locations,
								svlObject2dFrame *objects,
								svlFeatureVectorsF *vecs,
								bool sparse)
{
	SVL_ASSERT_MSG(!objects || _dtree, "Missing Cuda decision tree");
	SVL_ASSERT_MSG(_cpdExtractor->getPatchesCached(), "Have not cached patches on device");

	// Assert that we have not been given any NULL images or images of different size.
	vector<IplImage*> images(imagesin.size(),NULL);
	vector<bool> freeThese(imagesin.size(),false);
	for ( int i=0; i<int(imagesin.size()); ++i ) {
		assert(imagesin[i] != NULL);
		assert(imagesin[i]->width == imagesin[0]->width);
		assert(imagesin[i]->height == imagesin[0]->height);
		if ( imagesin[i]->depth == IPL_DEPTH_32F ) {
			images[i] = imagesin[i]; // Just grab pointer.
		} else {
			images[i] = create32Fimage(imagesin[i]);
			freeThese[i] = true;
		}
	}

	SVL_LOG(SVL_LOG_VERBOSE, "Running classifier on " << int(imagesin.size()) << "-channel image");

	// Various important members.
	double scale = double(1); // Accumulated scaling factor.
	int scaled_w = images[0]->width,
		scaled_h = images[0]->height;		// Set current working image scale.
	bool cmax_opt = _delta_x >= 4 && _delta_y >= 4; // TODO paulb: A better condition for this.
	int n_locs = int(locations.size());

	// Allocate device memory in form of a multi-pitch.
	if ( !_MP.accommodatesSize(images[0]->width, images[0]->height) ) {
		_MP.setImageSize(images[0]->width, images[0]->height);
		_cpdExtractor->allocPitches(_MP); // Have extractor allocate its needed memory.
	}
	// Use a feature line to hold feature space.
	// TODO: Make features dynamic memory.
	svlCudaFeatureLine features(_cpdExtractor->numFeatures(), objects ? _dtree->getNtrees() : -1, objects != NULL);

	// Code profiling handles.
	const int prof = svlCodeProfiler::getHandle("cSWD :: classifyImage");
	const int eval_model = svlCodeProfiler::getHandle("cSWD :: eval model");
	svlCodeProfiler::tic(prof);

	// Extract features.
	_cpdExtractor->extract(images, locations, _MP, features, cmax_opt);

	// Copy out features to return.
	if ( vecs ) {
		if ( vecs->_want_scales ) vecs->_scales.push_back(cvSize(scaled_w,scaled_h));
		// Copy locations.
		if ( vecs->_want_locations ) vecs->_locations.push_back(locations);
		// Copy out features.
		if ( vecs->_want_features ) {
			int fs = features._feature_stride;
			float *feats = (float*)malloc( sizeof(float) * features.featureSize() );
			cm->lineFromGPU(feats, features._features, features.featureSize());
			vecs->_features.push_back(vector<vector<float> >());
			vecs->_features.back().reserve(n_locs);
			float *ptr = feats;
			for ( int i=0; i<n_locs; ++i ) {
				vecs->_features.back().push_back(vector<float>(ptr, ptr + fs));
				ptr += fs;
			}
			free(feats);
		}
	}
	// Evaluate decision tree.
	if ( objects ) {
		assert(false); // Till I reimplement a test for this.
	}

	// Release images.
	for ( int i=0; i<int(images.size()); ++i ) {
		if ( freeThese[i] )
			cvReleaseImage(&images[i]);
	}

	svlCodeProfiler::toc(prof);
}
// double version of above.
void svlCudaSWD::classifyImage(const vector<IplImage*> &imagesin,
							   const vector<CvPoint>& locations,
							   svlObject2dFrame *objects,
							   svlFeatureVectors *vecs,
							   bool sparse)
{
	if ( !vecs ) {
		classifyImageF(imagesin, locations, objects, NULL, sparse);
	} else {
		SVL_LOG(SVL_LOG_WARNING, "Called a float extractor asking for double features");
		// Compute double features, then covert back to float.
		svlFeatureVectorsF F;
		classifyImageF(imagesin, locations, objects, NULL, sparse);
		F.convertTo(*vecs);
	}
}


