/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2009, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaConvolution.cu
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  Cuda convolution and related functions.
**
*****************************************************************************/

#include "svlCudaCommon.h"
#include "svlCudaMemHandler.h"
#include "svlCudaTimer.h"
#include <limits>

extern cudaEvent_t ct_start, ct_stop;

int iDivUp(int a, int b){
	return (a % b != 0) ? (a / b + 1) : (a / b);
}
//Round a/b to nearest lower integer value
int iDivDown(int a, int b){
	return a / b;
}
//Align a to nearest higher multiple of b
int iAlignUp(int a, int b){
	return (a % b != 0) ?  (a - a % b + b) : a;
}
//Align a to nearest lower multiple of b
int iAlignDown(int a, int b){
	return a - a % b;
}

//extern bool sizesMatch(svlCudaCoveringGrid *cudagrid, dim3 &grid);

// Device memory.
__device__ __constant__ float kern_patch[1024];	// Patch kernel for convolution.
__device__ __constant__ float mask_patch[1024];	// Mask kernel for convolution.
// Elsewhere-defined device memory;
//extern __device__ __constant__ float *covering_grid; // For holding bits on whether to operation on this block or not.
//__device__ __constant__ float covering_grid[ 1536 ]; // For holding bits on whether to operation on this block or not.


// 2D patch width-by-height normalized cross-correlation.
template<int NUM_COLS>
__global__ void conv2d_halfT_normed_slice(float *din, int din_pitch, float *res, int res_pitch,
										  int data_w, int data_h, float patch_mean, float patch_inv_std, float N_inv, 
										  int out_w, int out_h, int NUM_ROWS)
{
	// Load din data into shared cache.
	__shared__ float data[ 1024 ];
	const int data_width = 32;
	const int data_size = IMUL(data_width, blockDim.y);
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.

	// Read in data.
	const int ix = IMUL(data_width, threadIdx.y) + threadIdx.x;		// Shared thread memory index.
	const float *dinf = PITCH_GET(din, din_pitch, y);				// Calculate my row of din.
	const bool on_x = x < data_w, on_y = y < data_h;
	data[ix] = on_x && on_y ? dinf[x] : 0.0f;

	// Read in rightward entry.
	const bool right_on = on_y && x + blockDim.x < data_w;
	data[ix + blockDim.x] = right_on ? dinf[x + blockDim.x] : 0.0f;

	if ( threadIdx.y < NUM_ROWS ) {
		if ( on_x && y + blockDim.y < data_h ) {
			// Read in upper entries
			const float *dinf = PITCH_GET(din, din_pitch, y + blockDim.y);
			data[ ix + data_size ] = dinf[x];
			// Read in rightward entry.
			data[ ix + data_size + blockDim.x ] = right_on ? dinf[x + blockDim.x] : 0.0f;
		} else {
			data[ ix + data_size ] = data[ ix + data_size + blockDim.x ] = 0.0f;
		}
	}

	//if ( !on_x || !on_y )
	if ( x >= out_w || y >= out_h )
		return;

	__syncthreads();

	// Compute my convolution result.
	float gf = 0.0f, window_sum = 0.0f, window_sq_sum = 0.0f;
	#pragma unroll
	for ( int i=0; i<NUM_ROWS; ++i ) {
		#pragma unroll
		for ( int j=0; j<NUM_COLS; ++j ) {
			const int pix = IMUL(NUM_COLS,i) + j;
			if ( mask_patch[ pix ] != 0.0f ) {
				const float d = data[ ix + IMUL(data_width,i) + j ];
				gf += kern_patch[ pix ] * d;
				window_sum += d;
				window_sq_sum += d * d;
			}
		}
	}

	// Calculate my row of the results.
	float *my_res = PITCH_GET(res, res_pitch, y);
	/*const float window_mean = window_sum * N_inv;
	const float window_sq_mean = window_sq_sum * N_inv;*/
	//my_res[x] = window_sq_sum;// - N_inv * window_sum * window_sum;
	my_res[x] = __fdividef( gf - patch_mean * window_sum, sqrtf( window_sq_sum - N_inv * window_sum * window_sum ) ) * patch_inv_std;
}

// Normalized cross correlation with half-T convolution.
void svlCudaCcoeffNormed2d_v2(const svlCudaPitch *pin, svlCudaPitch *pout, float *patch, float *mask,
							  int patch_w, int patch_h, int patch_real_numel, float patch_mean, float patch_std,
							  int img_w, int img_h, int out_w, int out_h, bool cached)
{
	// Copy patches over to GPU.
	if ( !cached ) {
		// We were given host pointer for patch and mask.
		CUDA_SAFE_CALL( cudaMemcpyToSymbol( kern_patch, patch, patch_w*patch_h*sizeof(float), 0, cudaMemcpyHostToDevice) );
		CUDA_SAFE_CALL( cudaMemcpyToSymbol( mask_patch, mask, patch_w*patch_h*sizeof(float), 0, cudaMemcpyHostToDevice) );
	} else {
		// Device pointers; yay.
		CUDA_SAFE_CALL( cudaMemcpyToSymbol( kern_patch, patch, patch_w*patch_h*sizeof(float), 0, cudaMemcpyDeviceToDevice) );
		CUDA_SAFE_CALL( cudaMemcpyToSymbol( mask_patch, mask, patch_w*patch_h*sizeof(float), 0, cudaMemcpyDeviceToDevice) );
	}

	// Convolve.
	dim3 block(16, 16);
	if ( img_w == -1 ) img_w = pin->w;
	if ( img_h == -1 ) img_h = pin->h;
	if ( out_w == -1 ) out_w = min(img_w,pout->w);
	if ( out_h == -1 ) out_h = min(img_h,pout->h);
	dim3 grid( iDivUp(img_w, block.x), iDivUp(img_h, block.y) );

	patch_std = 1.0f / patch_std; // Pre-reciprocalize standard deviation

	svlCudaTimer::tic();
	float N_inv = 1.0f / float(patch_real_numel);
	if ( patch_w == 4 ) {
		conv2d_halfT_normed_slice <4> <<< grid, block >>>(pin->data, pin->pitch, pout->data, pout->pitch, img_w, img_h, patch_mean, patch_std, N_inv,
			out_w, out_h, patch_h);
	} else if ( patch_w == 8 ) {
		conv2d_halfT_normed_slice <8> <<< grid, block >>>(pin->data, pin->pitch, pout->data, pout->pitch, img_w, img_h, patch_mean, patch_std, N_inv,
			out_w, out_h, patch_h);
	} else if ( patch_w == 16 ) {
		conv2d_halfT_normed_slice <16> <<< grid, block >>>(pin->data, pin->pitch, pout->data, pout->pitch, img_w, img_h, patch_mean, patch_std, N_inv,
			out_w, out_h, patch_h);
	}
	svlCudaTimer::toc();
}

// 2D patch width-by-height normalized cross-correlation.
template<int NUM_COLS, int NUM_ROWS> __global__ void crosscorr_multi_slice(float *din, int din_pitch, float *res, int res_pitch,
																		   float *sum_data, int sum_pitch, float *sum_sq_data, int sum_sq_pitch,
																		   int data_w, int data_h, float patch_mean, float patch_inv_std, float N_inv)
{
	// Load din data into shared cache.
	__shared__ float data[ 1536 ];
	const int data_width = 32;
	const int data_size = IMUL(data_width, blockDim.y);
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.

	// Read in data.
	const int ix = IMUL(data_width, threadIdx.y) + threadIdx.x;		// Shared thread memory index.
	const float *my_din = PITCH_GET(din, din_pitch, y);				// Calculate my row of din.
	data[ix] = my_din[x];
	if ( x + blockDim.x < data_w ) {
		// Read in right-ward entry.
		data[ix + blockDim.x] = my_din[x + blockDim.x];
	} else {
		data[ix + blockDim.x] = 0.0f;
	}
	if ( threadIdx.y < NUM_ROWS && y + blockDim.y < data_h ) {
		// Read in upper entries
		const float *in_up = PITCH_GET(din, din_pitch, y + blockDim.y);
		data[ ix + data_size ] = in_up[x];
		if ( x + blockDim.x < data_w ) {
			// Read in right-ward entry.
			data[ ix + data_size + blockDim.x ] = in_up[x + blockDim.x];
		} else {
			data[ ix + data_size + blockDim.x ] = 0.0f;
		}
	} else {
		data[ ix + data_size ] = 0.0f;
		data[ ix + data_size + blockDim.x ] = 0.0f;
	}

	__syncthreads();

	// Compute my convolution result.
	float gf = 0.0f, window_sum = 0.0f, window_sq_sum = 0.0f, d;
#pragma unroll
	for ( int i=0; i<NUM_ROWS; ++i ) {
#pragma unroll
		for ( int j=0; j<NUM_COLS; ++j ) {
			if ( mask_patch[ IMUL(NUM_COLS,i) + j ] != 0.0f ) {
				d = data[ ix + IMUL(data_width,i) + j ];
				gf += kern_patch[ IMUL(NUM_COLS,i) + j ] * d;
				window_sum += d;
				window_sq_sum += d * d;
			}
		}
	}

	// Calculate my row of the results.
	float *my_res = PITCH_GET(res, res_pitch, y);
	float *sum_out = PITCH_GET(sum_data, sum_pitch, y);
	float *sum_sq_out = PITCH_GET(sum_sq_data, sum_sq_pitch, y);
	my_res[x] = gf;
	sum_out[x] = window_sum;
	sum_sq_out[x] = window_sq_sum;
}

// 2D patch width-by-height normalized cross-correlation.
__global__ void crosscorr_normit_slice(float *res, int res_pitch, float *sum_data, int sum_pitch, float *sum_sq_data, int sum_sq_pitch,
									   int img_w, int img_h, float patch_mean, float patch_inv_std, float N_inv)
{
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.
	if ( x >= img_w || y >= img_h )
		return;

	float *res_it = PITCH_GET(res, res_pitch, y);
	const float *sum = PITCH_GET(sum_data, sum_pitch, y);
	const float *sum_sq = PITCH_GET(sum_sq_data, sum_sq_pitch, y);
	res_it[x] = __fdividef( res_it[x] - patch_mean * sum[x], __powf( N_inv * (sum_sq[x] - sum[x]*sum[x]*N_inv), 0.5f) ) * patch_inv_std * N_inv;
	/*const float subtraction = res_it[x] - patch_mean * sum[x];
	const float denominator = sum_sq[x] - sum[x] * sum[x] / N_inv;
	res_it[x] = subtraction / ( patch_inv_std * sqrt( denominator ) );*/
}


// Execute a 2D convolution between pitches using a patch.
void svlCudaCcoeffNormed2d_twostep(const svlCudaPitch &pitch_in, svlCudaPitch &pitch_out, svlCudaPitch &temp1, svlCudaPitch &temp2,
								   float *patch, float *mask, int patch_w, int patch_h, int patch_real_numel,
								   float patch_mean, float patch_std, int img_w, int img_h)
{
	// Copy patches over to GPU.
	CUDA_SAFE_CALL( cudaMemcpyToSymbol( kern_patch, patch, patch_w*patch_h*sizeof(float), 0, cudaMemcpyHostToDevice) );
	CUDA_SAFE_CALL( cudaMemcpyToSymbol( mask_patch, mask, patch_w*patch_h*sizeof(float), 0, cudaMemcpyHostToDevice) );

	// Convolve.
	dim3 block(16, 16);
	dim3 grid( iDivUp( ( img_w == -1 ? pitch_in.w : img_w ), block.x), iDivUp( ( img_h == -1 ? pitch_in.h : img_h ), block.y) );

	// Pre-reciprocalize standard deviation
	patch_std = 1.0f / patch_std; // Invert standard deviation.
	if ( patch_w <= 4 && patch_h <= 4 ) {
		crosscorr_multi_slice<4,4><<< grid, block >>>(pitch_in.data, pitch_in.pitch, pitch_out.data, pitch_out.pitch,
			temp1.data, temp1.pitch, temp2.data, temp2.pitch,
			pitch_in.w, pitch_in.h, patch_mean, patch_std, 1.0f/float(patch_real_numel));
	}
	else if ( patch_w <= 8 && patch_h <= 8 ) {
		crosscorr_multi_slice<8,8><<< grid, block >>>(pitch_in.data, pitch_in.pitch, pitch_out.data, pitch_out.pitch,
			temp1.data, temp1.pitch, temp2.data, temp2.pitch,
			pitch_in.w, pitch_in.h, patch_mean, patch_std, 1.0f/float(patch_real_numel));
	}
	else if ( patch_w <= 8 && patch_h <= 16 ) {
		crosscorr_multi_slice<8,16><<< grid, block >>>(pitch_in.data, pitch_in.pitch, pitch_out.data, pitch_out.pitch,
			temp1.data, temp1.pitch, temp2.data, temp2.pitch,
			pitch_in.w, pitch_in.h, patch_mean, patch_std, 1.0f/float(patch_real_numel));
	}
	else if ( patch_w <= 16 && patch_h <= 8 ) {
		crosscorr_multi_slice<16,8><<< grid, block >>>(pitch_in.data, pitch_in.pitch, pitch_out.data, pitch_out.pitch,
			temp1.data, temp1.pitch, temp2.data, temp2.pitch,
			pitch_in.w, pitch_in.h, patch_mean, patch_std, 1.0f/float(patch_real_numel));
	}
	else if ( patch_w <= 16 && patch_h <= 16 ) {
		crosscorr_multi_slice<16,16><<< grid, block >>>(pitch_in.data, pitch_in.pitch, pitch_out.data, pitch_out.pitch,
			temp1.data, temp1.pitch, temp2.data, temp2.pitch,
			pitch_in.w, pitch_in.h, patch_mean, patch_std, 1.0f/float(patch_real_numel));
	}
	crosscorr_normit_slice<<< grid, block >>>(pitch_out.data, pitch_out.pitch, temp1.data, temp1.pitch, temp2.data, temp2.pitch,
		img_w, img_h, patch_mean, patch_std, 1.0f/float(patch_real_numel));
}


// Window sum and sum-sq function. Maximum window size of 16x16.
__global__ void window_sum_sum2_slice(float *din, int din_pitch, float *sum, int sum_pitch, float *sum2, int sum2_pitch, int img_w, int img_h, int patch_w, int patch_h)
{
	// Load din data into shared cache.
	__shared__ float data[ 1024 ];
	const int data_width = 32;	// Width of row in shared memory
	const int data_size = IMUL(data_width, blockDim.y);
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.

	// Read in data.
	const int ix = IMUL(data_width, threadIdx.y) + threadIdx.x;		// Shared thread memory index.
	const float *my_din = PITCH_GET(din, din_pitch, y);				// Calculate my row of din.
	data[ix] = my_din[x];
	// Read in right-ward entry.
	const bool right_on = x + blockDim.x < img_w && threadIdx.x < patch_w;
	data[ix + blockDim.x] = right_on ? my_din[x + blockDim.x] : 0.0f;
	// Read in downward entries.
	if ( threadIdx.y < patch_h && y + blockDim.y < img_h ) {
		// Read in upper entries
		const float *in_up = PITCH_GET(din, din_pitch, y + blockDim.y);
		data[ ix + data_size ] = in_up[x];
		// Read in right-ward entry.
		data[ ix + data_size + blockDim.x ] = right_on ? in_up[x + blockDim.x] : 0.0f;
	} else {
		data[ ix + data_size ] = data[ ix + data_size + blockDim.x ] = 0.0f;
	}

	__syncthreads();

	// Compute my convolution result.
	float sumf = 0.0f, sumf2 = 0.0f;
#pragma unroll
	for ( int i=0; i<patch_h; ++i ) {
#pragma unroll
		for ( int j=0; j<patch_w; ++j ) {
			const float d = data[ ix + IMUL(data_width, i) + j ];
			sumf += d;
			sumf2 += d * d;
		}
	}

	// Write out results.
	float *sum_out = PITCH_GET(sum, sum_pitch, y);
	sum_out[x] = sumf;
	float *sum2_out = PITCH_GET(sum2, sum2_pitch, y);
	sum2_out[x] = sumf2;
}

// Compute windos sum and sum-squareds. Maximum window size is 16x16.
void svlCudaWindowSumSumSq(const svlCudaPitch *pin, svlCudaPitch *sum, svlCudaPitch *sum2, int win_w, int win_h, int img_w, int img_h)
{
	// TODO paulb: Make sum2 optional, in which case it uses a simpler function or something.
	if ( img_w == -1 ) img_w = pin->w;
	if ( img_h == -1 ) img_h = pin->h;
	dim3 block( 16, 16 );
	dim3 grid( iDivUp(img_w, block.x), iDivUp(img_h, block.y) );
	window_sum_sum2_slice<<< grid, block >>>(pin->data, pin->pitch, sum->data, sum->pitch, sum2->data, sum2->pitch, img_w, img_h, win_w, win_h);
}

// Do simple normalization using data plus corresponding sum and sum-squared images
void svlCudaPatchNormalize(svlCudaPitch *pout, svlCudaPitch *sum, svlCudaPitch *sum2, float patch_mean, float patch_std, int patch_N, int img_w, int img_h)
{
	if ( img_w == -1 ) img_w = pout->w;
	if ( img_h == -1 ) img_h = pout->h;
	dim3 block( 16, 16 );
	dim3 grid( iDivUp(img_w, block.x), iDivUp(img_h, block.y) );
	crosscorr_normit_slice<<< grid, block >>>(pout->data, pout->pitch, sum->data, sum->pitch, sum2->data, sum2->pitch,
		img_w, img_h, patch_mean, patch_std, 1.0f/float(patch_N)); 
}

// 2D convolution with normalization via pre-summed row-integral images.
template<int NUM_COLS>
__global__ void conv2d_halfT_normed_v2_slice(float *din, int din_pitch, float *res, int res_pitch,
											 float *integ, int integ_pitch, float *integ2, int integ2_pitch,
											 int data_w, int data_h, float patch_mean, float patch_inv_std, float N_inv, int NUM_ROWS)
{
	// Load din data into shared cache.
	__shared__ float data[ 1024 ];
	__shared__ float integd[ 512 ];
	__shared__ float integd2[ 512 ];

	const int data_width = 32;
	const int data_size = IMUL(data_width, blockDim.y);
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.

	// Read in data.
	const int ix = IMUL(data_width, threadIdx.y) + threadIdx.x;		// Shared thread memory index.
	const float *my_din = PITCH_GET(din, din_pitch, y);				// Calculate my row of din.
	data[ix] = my_din[x];
	// Read in rightward entry.
	const bool right_on = x + blockDim.x < data_w;
	data[ix + blockDim.x] = right_on ? my_din[x + blockDim.x] : 0.0f;

	// Read in integral data (should be coalesced this way).
	const int i_ix = IMUL(16, threadIdx.y) + threadIdx.x;
	const float *integf = PITCH_GET(integ, integ_pitch, y);
	integd[i_ix] = integf[x];
	const float *integ2f = PITCH_GET(integ2, integ2_pitch, y);
	integd2[i_ix] = integ2f[x];

	// Read in upward data.
	if ( threadIdx.y < NUM_ROWS ) {
		if ( y + blockDim.y < data_h ) {
			// Read in upper entries
			const float *in_up = PITCH_GET(din, din_pitch, y + blockDim.y);
			data[ ix + data_size ] = in_up[x];
			// Read in rightward entry.
			data[ ix + data_size + blockDim.x ] = right_on ? in_up[x + blockDim.x] : 0.0f;

			// Integral data.
			const float *integf_up = PITCH_GET(integ, integ_pitch, y + blockDim.y);
			integd[ i_ix + 256 ] = integf_up[x];
			const float *integ2f_up = PITCH_GET(integ2, integ2_pitch, y + blockDim.y);
			integd2[ i_ix + 256 ] = integ2f_up[x];
		} else {
			data[ ix + data_size ] = data[ ix + data_size + blockDim.x ] = 0.0f;
			// Integral data.
			integd[ i_ix + 256 ] = integd2[ i_ix + 256 ] = 0.0f;
		}
	}

	__syncthreads();

	// Compute my convolution result.
	float gf = 0.0f, window_sum = 0.0f, window_sq_sum = 0.0f;
#pragma unroll
	for ( int i=0; i<NUM_ROWS; ++i ) {
		window_sum += integd[ i_ix + IMUL(16, i) ];
		window_sq_sum += integd2[ i_ix + IMUL(16, i) ];
#pragma unroll
		for ( int j=0; j<NUM_COLS; ++j ) {
			gf += kern_patch[ IMUL(NUM_COLS,i) + j ] * data[ ix + IMUL(data_width,i) + j ];
		}
	}

	// Calculate my row of the results.
	float *my_res = PITCH_GET(res, res_pitch, y);
	const float window_mean = window_sum * N_inv;
	const float window_sq_mean = window_sq_sum * N_inv;
	my_res[x] = __fdividef( gf - patch_mean * window_sum, sqrtf(window_sq_mean - window_mean*window_mean) ) * patch_inv_std * N_inv;
}

// Normalized cross correlation with half-T convolution.
void svlCudaCcoeffNormed2d_v3(const svlCudaPitch *pin, svlCudaPitch *pout, float *patch, float *mask, svlCudaPitch *integ, svlCudaPitch *integ2,
							  int patch_w, int patch_h, int patch_real_numel, float patch_mean, float patch_std, int img_w, int img_h, bool cached)
{
	// Copy patches over to GPU.
	if ( !cached ) {
		CUDA_SAFE_CALL( cudaMemcpyToSymbol( kern_patch, patch, patch_w*patch_h*sizeof(float), 0, cudaMemcpyHostToDevice) );
		//CUDA_SAFE_CALL( cudaMemcpyToSymbol( mask_patch, mask, patch_w*patch_h*sizeof(float), 0, cudaMemcpyHostToDevice) );
	} else {
		CUDA_SAFE_CALL( cudaMemcpyToSymbol( kern_patch, patch, patch_w*patch_h*sizeof(float), 0, cudaMemcpyDeviceToDevice) );
		//CUDA_SAFE_CALL( cudaMemcpyToSymbol( mask_patch, mask, patch_w*patch_h*sizeof(float), 0, cudaMemcpyDeviceToDevice) );
	}

	// Convolve.
	dim3 block(16, 16);
	if ( img_w == -1 ) img_w = pin->w;
	if ( img_h == -1 ) img_h = pin->h;
	dim3 grid( iDivUp(img_w, block.x), iDivUp(img_h, block.y) );

	patch_std = 1.0f / patch_std; // Pre-reciprocalize standard deviation

	float N_inv = 1.0f / float(patch_real_numel);
	if ( patch_w == 4 ) {
		conv2d_halfT_normed_v2_slice <4> <<< grid, block >>>(pin->data, pin->pitch, pout->data, pout->pitch,
			integ->data, integ->pitch, integ2->data, integ2->pitch, img_w, img_h, patch_mean, patch_std, N_inv, patch_h);
	} else if ( patch_w == 8 ) {
		conv2d_halfT_normed_v2_slice <8> <<< grid, block >>>(pin->data, pin->pitch, pout->data, pout->pitch,
			integ->data, integ->pitch, integ2->data, integ2->pitch, img_w, img_h, patch_mean, patch_std, N_inv, patch_h);
	} else if ( patch_w == 16 ) {
		conv2d_halfT_normed_v2_slice <16> <<< grid, block >>>(pin->data, pin->pitch, pout->data, pout->pitch,
			integ->data, integ->pitch, integ2->data, integ2->pitch, img_w, img_h, patch_mean, patch_std, N_inv, patch_h);
	} else {
		cout << "svlCudaCcoeffNormed2d_v3: Mis-padded patch.\n"; assert(false);
	}
}


// Lasik-corrected convolution preparation function.
template<int NUM_COLS>
__global__ void corr_lasik_slice(const float *din, int din_pitch, const float *integ, int integ_pitch, const float *integ2, int integ2_pitch,
							     float *conv, int cpitch, float *sum, int spitch, float *sum2, int s2pitch, int data_w, int data_h, int NUM_ROWS)
{
	// Load din data into shared cache.
	__shared__ float data[ 1024 ];
	__shared__ float integd[ 512 ];
	__shared__ float integd2[ 512 ];

	const int data_width = 32;
	const int data_size = IMUL(data_width, blockDim.y);
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.

	// Read in data.
	const int ix = IMUL(data_width, threadIdx.y) + threadIdx.x;		// Shared thread memory index.
	const float *my_din = PITCH_GET(din, din_pitch, y);				// Calculate my row of din.
	const bool on_x = x < data_w, on_y = y < data_h;
	const bool on_on = on_x && on_y;
	data[ix] = on_on ? my_din[x] : 0.0f;
	// Read in integral data.
	const int i_ix = IMUL(16, threadIdx.y) + threadIdx.x;
	const float *integf = PITCH_GET(integ, integ_pitch, y);
	integd[i_ix] = on_on ? integf[x] : 0.0f;
	const float *integ2f = PITCH_GET(integ2, integ2_pitch, y);
	integd2[i_ix] = on_on ? integ2f[x] : 0.0f;

	// Read in rightward entry.
	const bool right_on = on_on && x + blockDim.x < data_w;
	data[ix + blockDim.x] = right_on ? my_din[x + blockDim.x] : 0.0f;

	// Read in upward data.
	if ( threadIdx.y < NUM_ROWS ) {
		if ( y + blockDim.y < data_h && on_x ) {
			// Read in upper entries
			const float *in_up = PITCH_GET(din, din_pitch, y + blockDim.y);
			data[ ix + data_size ] = in_up[x];
			// Integral data.
			const float *integf_up = PITCH_GET(integ, integ_pitch, y + blockDim.y);
			integd[ i_ix + 256 ] = integf_up[x];
			const float *integ2f_up = PITCH_GET(integ2, integ2_pitch, y + blockDim.y);
			integd2[ i_ix + 256 ] = integ2f_up[x];
			
			// Read in rightward entry.
			data[ ix + data_size + blockDim.x ] = right_on ? in_up[x + blockDim.x] : 0.0f;
		} else {
			data[ ix + data_size ] = data[ ix + data_size + blockDim.x ] =
			integd[ i_ix + 256 ] = integd2[ i_ix + 256 ] = 0.0f;
		}
	}

	if ( !on_on )
		return;

	__syncthreads();

	// Compute my convolution result.
	float gf = 0.0f, window_sum = 0.0f, window_sq_sum = 0.0f;
#pragma unroll
	for ( int i=0; i<NUM_ROWS; ++i ) {
		window_sum += integd[ i_ix + IMUL(16, i) ];
		window_sq_sum += integd2[ i_ix + IMUL(16, i) ];
#pragma unroll
		for ( int j=0; j<NUM_COLS; ++j ) {
			gf += kern_patch[ IMUL(NUM_COLS,i) + j ] * data[ ix + IMUL(data_width,i) + j ];
		}
	}

	// Save results.
	float *convf = PITCH_GET(conv, cpitch, y);
	convf[x] = gf;
	float *sumf = PITCH_GET(sum, spitch, y);
	sumf[x] = window_sum;
	float *sum2f = PITCH_GET(sum2, s2pitch, y);
	sum2f[x] = window_sq_sum;
}

// Cross-correlation.
void svlCudaLasikConvolution(const svlCudaPitch *pin, CachedPatch *cp, const svlCudaPitch *integ, const svlCudaPitch *integ2,
							svlCudaPitch *conv, svlCudaPitch *sum, svlCudaPitch *sum2, int img_w, int img_h)
{
	// Copy patches over to GPU.
	CUDA_SAFE_CALL( cudaMemcpyToSymbol( kern_patch, cp->patch->data, cp->w*cp->h*sizeof(float), 0, cudaMemcpyDeviceToDevice) );

	// Convolve.
	dim3 block(16, 16);
	if ( img_w == -1 ) img_w = pin->w;
	if ( img_h == -1 ) img_h = pin->h;
	dim3 grid( iDivUp(img_w, block.x), iDivUp(img_h, block.y) );

	if ( cp->w == 4 ) {
		corr_lasik_slice <4> <<< grid, block >>>(pin->data, pin->pitch, integ->data, integ->pitch, integ2->data, integ2->pitch,
			conv->data, conv->pitch, sum->data, sum->pitch, sum2->data, sum2->pitch, img_w, img_h, cp->h);
	} else if ( cp->w == 8 ) {
		corr_lasik_slice <8> <<< grid, block >>>(pin->data, pin->pitch, integ->data, integ->pitch, integ2->data, integ2->pitch,
			conv->data, conv->pitch, sum->data, sum->pitch, sum2->data, sum2->pitch, img_w, img_h, cp->h);
	} else if ( cp->w == 16 ) {
		corr_lasik_slice <16> <<< grid, block >>>(pin->data, pin->pitch, integ->data, integ->pitch, integ2->data, integ2->pitch,
			conv->data, conv->pitch, sum->data, sum->pitch, sum2->data, sum2->pitch, img_w, img_h, cp->h);
	} else {
		cout << "svlCudaLasikConvolution: Mis-padded patch.\n"; assert(false);
	}
}


// 2D convolution with normalization via pre-summed row-integral images.
template<int NUM_COLS, int KERN_OFF>
__global__ void plain_conv2d_slice(float *din, int din_pitch, float *res, int res_pitch,
								   int data_w, int data_h, int out_w, int out_h, int NUM_ROWS)
{
	// Load din data into shared cache.
	__shared__ float data[ 1024 ];

#if 0
	const int data_width = 32;
	const int data_size = IMUL(data_width, blockDim.y);
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.

	// Read in data.
	const int ix = IMUL(data_width, threadIdx.y) + threadIdx.x;		// Shared thread memory index.
	const float *my_din = PITCH_GET(din, din_pitch, y);				// Calculate my row of din.
	const bool on_on = x < data_w && y < data_h;
	data[ix] = on_on ? my_din[x] : 0.0f;
	// Read in rightward entry.
	const bool right_on = x + blockDim.x < data_w && y < data_h;
	data[ix + blockDim.x] = right_on ? my_din[x + blockDim.x] : 0.0f;

	// Read in upward data.
	if ( threadIdx.y < NUM_ROWS ) {
		if ( y + blockDim.y < data_h ) {
			// Read in upper entries
			const float *in_up = PITCH_GET(din, din_pitch, y + blockDim.y);
			data[ ix + data_size ] = on_on ? in_up[x] : 0.0f;
			// Read in rightward entry.
			data[ ix + data_size + blockDim.x ] = right_on ? in_up[x + blockDim.x] : 0.0f;
		} else {
			data[ ix + data_size ] = data[ ix + data_size + blockDim.x ] = 0.0f;
		}
	}

	//if ( x >= data_w || y >= data_h )
	if ( x >= out_w || y >= out_h )
		return;

	__syncthreads();

	// Compute my convolution result.
	float gf = 0.0f;
#pragma unroll
	for ( int i=0; i<NUM_ROWS; ++i ) {
#pragma unroll
		for ( int j=0; j<NUM_COLS; ++j ) {
			gf += kern_patch[ KERN_OFF + IMUL(NUM_COLS,i) + j ] * data[ ix + IMUL(data_width,i) + j ];
		}
	}

	// Calculate my row of the results.
	float *my_res = PITCH_GET(res, res_pitch, y);
	my_res[x] = gf;

#else // Manually replacing data_width and assuming 16-by-16 block.

	const int x = IMUL(16, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(16, blockIdx.y) + threadIdx.y;	// y index to patch.

	// Read in data.
	const int ix = IMUL(32, threadIdx.y) + threadIdx.x;		// Shared thread memory index.
	const float *my_din = PITCH_GET(din, din_pitch, y);				// Calculate my row of din.
	const bool on_on = x < data_w && y < data_h;
	data[ix] = on_on ? my_din[x] : 0.0f;
	// Read in rightward entry.
	const bool right_on = x + 16 < data_w && y < data_h;
	data[ ix + 16 ] = right_on ? my_din[ x + 16 ] : 0.0f;

	// Read in upward data.
	if ( threadIdx.y < NUM_ROWS ) {
		if ( y + 16 < data_h ) {
			// Read in upper entries
			const float *in_up = PITCH_GET(din, din_pitch, y + 16);
			data[ ix + 512 ] = on_on ? in_up[x] : 0.0f;
			// Read in rightward entry.
			data[ ix + 528 ] = right_on ? in_up[ x + 16 ] : 0.0f;
		} else {
			data[ ix + 512 ] = data[ ix + 528 ] = 0.0f;
		}
	}

	//if ( x >= data_w || y >= data_h )
	if ( x >= out_w || y >= out_h )
		return;
	__syncthreads();

	// Compute my convolution result.
	float gf = 0.0f;
#pragma unroll
	for ( int i=0; i<NUM_ROWS; ++i ) {
#pragma unroll
		for ( int j=0; j<NUM_COLS; ++j ) {
			gf += kern_patch[ KERN_OFF + IMUL(NUM_COLS,i) + j ] * data[ ix + IMUL(32,i) + j ];
		}
	}

	// Calculate my row of the results.
	float *my_res = PITCH_GET(res, res_pitch, y);
	my_res[x] = gf;
#endif
}

// Cross correlation with half-T convolution; max template size of 16.
void svlCudaPlainConv2d_16(const svlCudaPitch *pin, svlCudaPitch *pout, float *patch, int patch_w, int patch_h, int img_w, int img_h, int out_w, int out_h)
{
	// Copy patch over to GPU.
	CUDA_SAFE_CALL( cudaMemcpyToSymbol( kern_patch, patch, patch_w*patch_h*sizeof(float), 0, cudaMemcpyHostToDevice) );

	// Convolve.
	dim3 block(16, 16);
	if ( img_w == -1 ) img_w = pin->w;
	if ( img_h == -1 ) img_h = pin->h;
	if ( out_w == -1 ) out_w = min(img_w, pout->w);
	if ( out_h == -1 ) out_h = min(img_h, pout->h);
	dim3 grid( iDivUp(img_w, block.x), iDivUp(img_h, block.y) );

	svlCudaTimer::tic();
	if ( patch_w == 4 ) {
		plain_conv2d_slice <4,0> <<< grid, block >>>(pin->data, pin->pitch, pout->data, pout->pitch, img_w, img_h, out_w, out_h, patch_h);
	} else if ( patch_w == 8 ) {
		plain_conv2d_slice <8,0> <<< grid, block >>>(pin->data, pin->pitch, pout->data, pout->pitch, img_w, img_h, out_w, out_h, patch_h);
	} else if ( patch_w == 16 ) {
		plain_conv2d_slice <16,0> <<< grid, block >>>(pin->data, pin->pitch, pout->data, pout->pitch, img_w, img_h, out_w, out_h, patch_h);
	} else {
		cout << "svlCudaPlainConv2d: Mis-padded patch.\n"; assert(false);
	}
	svlCudaTimer::toc();
}

// Cross correlation with half-T convolution; max template size of 32.
void svlCudaPlainConv2d_32(const svlCudaPitch *pin, svlCudaPitch *pout, svlCudaPitch *temp, vector<PaddedPatch> &pps, int img_w, int img_h, int out_w, int out_h)
{
	// Run four separate convolutions and sum the results.
	// Copy all patches to GPU.
	if ( pps.size() == 5 ) {
		// Use last patch for a batch copy as it has all of the patches properly spaced and coalesced.
		CUDA_SAFE_CALL( cudaMemcpyToSymbol( kern_patch, pps[4].data, pps[4].size()*sizeof(float), 0, cudaMemcpyHostToDevice) );
	} else {
		// Have to do them individually.
		if ( pps[0].data )
			CUDA_SAFE_CALL( cudaMemcpyToSymbol( kern_patch, pps[0].data, pps[0].size()*sizeof(float), 0, cudaMemcpyHostToDevice) );
		if ( pps[1].data )
			CUDA_SAFE_CALL( cudaMemcpyToSymbol( kern_patch, pps[1].data, pps[1].size()*sizeof(float), 1024, cudaMemcpyHostToDevice) );
		if ( pps[2].data )
			CUDA_SAFE_CALL( cudaMemcpyToSymbol( kern_patch, pps[2].data, pps[2].size()*sizeof(float), 2048, cudaMemcpyHostToDevice) );
		if ( pps[3].data )
			CUDA_SAFE_CALL( cudaMemcpyToSymbol( kern_patch, pps[3].data, pps[3].size()*sizeof(float), 3072, cudaMemcpyHostToDevice) );
	}

	// Convolve.
	dim3 block(16, 16);
	if ( img_w == -1 ) img_w = pin->w;
	if ( img_h == -1 ) img_h = pin->h;
	if ( out_w == -1 ) out_w = min(img_w, pout->w);
	if ( out_h == -1 ) out_h = min(img_h, pout->h);
	int new_w = min(out_w + 16, img_w);
	int new_h = min(out_h + 16, img_h);
	//printf("img: %d,%d; out: %d,%d; new: %d,%d\n", img_w, img_h, out_w, out_h, new_w, new_h);
	dim3 grid( iDivUp(img_w, block.x), iDivUp(img_h, block.y) );

	svlCudaTimer::tic();
	svlCudaPitch *t = pout;
	switch ( pps[0].w ) { // First sub-patch: +0,+0
		case 4:
			plain_conv2d_slice <4,0> <<< grid, block >>>(pin->data, pin->pitch, t->data, t->pitch, img_w, img_h, out_w, out_h, pps[0].h); break;
		case 8:
			plain_conv2d_slice <8,0> <<< grid, block >>>(pin->data, pin->pitch, t->data, t->pitch, img_w, img_h, out_w, out_h, pps[0].h); break;
		case 16:
			plain_conv2d_slice <16,0> <<< grid, block >>>(pin->data, pin->pitch, t->data, t->pitch, img_w, img_h, out_w, out_h, pps[0].h); break;
	}
	t = temp;
	if ( pps[1].data ) { // Second sub-patch: +16,+0
		switch ( pps[1].w ) {
			case 4:
				plain_conv2d_slice <4,256> <<< grid, block >>>(pin->data, pin->pitch, t->data, t->pitch, img_w, img_h, new_w, out_h, pps[1].h); break;
			case 8:
				plain_conv2d_slice <8,256> <<< grid, block >>>(pin->data, pin->pitch, t->data, t->pitch, img_w, img_h, new_w, out_h, pps[1].h); break;
			case 16:
				plain_conv2d_slice <16,256> <<< grid, block >>>(pin->data, pin->pitch, t->data, t->pitch, img_w, img_h, new_w, out_h, pps[1].h); break;
		}
		// Add appropriately to pout.
		svlCudaAddOff(pout, temp, 1.0f, out_w, out_h, 16, 0);
	}
	if ( pps[2].data ) { // Third sub-patch: +0,+16
		switch ( pps[2].w ) {
			case 4:
				plain_conv2d_slice <4,512> <<< grid, block >>>(pin->data, pin->pitch, t->data, t->pitch, img_w, img_h, out_w, new_h, pps[2].h); break;
			case 8:
				plain_conv2d_slice <8,512> <<< grid, block >>>(pin->data, pin->pitch, t->data, t->pitch, img_w, img_h, out_w, new_h, pps[2].h); break;
			case 16:
				plain_conv2d_slice <16,512> <<< grid, block >>>(pin->data, pin->pitch, t->data, t->pitch, img_w, img_h, out_w, new_h, pps[2].h); break;
		}
		// Add appropriately to pout.
		svlCudaAddOff(pout, temp, 1.0f, out_w, out_h, 0, 16);
	}
	if ( pps[3].data ) { // Fourth sub-patch: +16,+16
		switch ( pps[3].w ) {
			case 4:
				plain_conv2d_slice <4,768> <<< grid, block >>>(pin->data, pin->pitch, t->data, t->pitch, img_w, img_h, new_w, new_h, pps[3].h); break;
			case 8:
				plain_conv2d_slice <8,768> <<< grid, block >>>(pin->data, pin->pitch, t->data, t->pitch, img_w, img_h, new_w, new_h, pps[3].h); break;
			case 16:
				plain_conv2d_slice <16,768> <<< grid, block >>>(pin->data, pin->pitch, t->data, t->pitch, img_w, img_h, new_w, new_h, pps[3].h); break;
		}
		// Add appropriately to pout.
		svlCudaAddOff(pout, temp, 1.0f, out_w, out_h, 16, 16);
	}
	svlCudaTimer::toc();
}

// 2D convolution with normalization via pre-summed row-integral images.
template<int NUM_COLS>
__global__ void plain_conv2d_slice_full(float *din, int din_pitch, int din_w, int din_h, float *res, int res_pitch, int res_w, int res_h, int NUM_ROWS)
{
	// Load din data into shared cache.
	__shared__ float data[ 1024 ];

#if 0
	const int data_width = 32;
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.

	// Read in data.
	const int ix = IMUL(data_width, threadIdx.y) + threadIdx.x; // Shared thread memory index.
	const float *dinf = PITCH_GET(din, din_pitch, y);			// Input row.
	const bool in_x_on = x < din_w;
	const bool in_y_on = y < din_h;
	const bool in_on = in_x_on && in_y_on; // If pixel is on input image.
	data[ 528 + ix ] = in_on ? dinf[x] : 0.0f;
	// Read in leftward entry.
	const bool left_x_on = x >= blockDim.x && x - blockDim.x < din_w;
	const bool left_on = in_y_on && left_x_on;
	data[ 528 + ix - blockDim.x ] = left_on ? dinf[ x - blockDim.x ] : 0.0f;

	// Read in downwards data.
	if ( threadIdx.y < NUM_ROWS ) {
		const int y_off = -2*threadIdx.y - 1;
		if ( y + y_off >= 0 && y + y_off < din_h ) {
			// Read in downard entries
			const float *dinf = PITCH_GET(din, din_pitch, y + y_off);
			data[ 528 + ix + IMUL(data_width, y_off) ] = in_x_on ? dinf[x] : 0.0f;
			// Read in down-leftward entry.
			data[ 528 + ix + IMUL(data_width, y_off) - blockDim.x ] = left_x_on ? dinf[ x - blockDim.x ] : 0.0f;
		} else {
			data[ 528 + ix + IMUL(data_width, y_off) ] = 
			data[ 528 + ix + IMUL(data_width, y_off) - blockDim.x ] = 0.0f;
		}
	}

	if ( x >= res_w || y >= res_h )
		return;

	__syncthreads();

	// Compute my convolution result.
	float gf = 0.0f;
#pragma unroll
	for ( int i=0; i<NUM_ROWS; ++i ) {
#pragma unroll
		for ( int j=0; j<NUM_COLS; ++j ) {
			gf += kern_patch[ IMUL(NUM_COLS, i) + j ] * data[ 528 + ix + IMUL(data_width, i - NUM_ROWS + 1) + j - NUM_COLS + 1 ];
		}
	}

	// Calculate my row of the results.
	float *resf = PITCH_GET(res, res_pitch, y);
	resf[x] = gf;
#else
	// Manually replacing data_width with 32 and assuming 16-by-16 block size.
	const int x = IMUL(16, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(16, blockIdx.y) + threadIdx.y;	// y index to patch.

	// Read in data.
	const int ix = IMUL(32, threadIdx.y) + threadIdx.x; // Shared thread memory index.
	const float *dinf = PITCH_GET(din, din_pitch, y);			// Input row.
	const bool in_x_on = x < din_w;
	const bool in_y_on = y < din_h;
	const bool in_on = in_x_on && in_y_on; // If pixel is on input image.
	data[ 528 + ix ] = in_on ? dinf[x] : 0.0f;
	// Read in leftward entry.
	const bool left_x_on = x >= 16 && x - 16 < din_w;
	const bool left_on = in_y_on && left_x_on;
	data[ 512 + ix ] = left_on ? dinf[ x - 16 ] : 0.0f;

	// Read in downwards data.
	if ( threadIdx.y < NUM_ROWS ) {
		const int y_off = -2*threadIdx.y - 1;
		if ( y + y_off >= 0 && y + y_off < din_h ) {
			// Read in downard entries
			const float *dinf = PITCH_GET(din, din_pitch, y + y_off);
			data[ 528 + ix + IMUL(32, y_off) ] = in_x_on ? dinf[x] : 0.0f;
			// Read in down-leftward entry.
			data[ 512 + ix + IMUL(32, y_off) ] = left_x_on ? dinf[ x - blockDim.x ] : 0.0f;
		} else {
			data[ 528 + ix + IMUL(32, y_off) ] = 
			data[ 512 + ix + IMUL(32, y_off) ] = 0.0f;
		}
	}

	if ( x >= res_w || y >= res_h )
		return;
	__syncthreads();

	// Compute my convolution result.
	float gf = 0.0f;
#pragma unroll
	for ( int i=0; i<NUM_ROWS; ++i ) {
#pragma unroll
		for ( int j=0; j<NUM_COLS; ++j ) {
			gf += kern_patch[ IMUL(NUM_COLS, i) + j ] * data[ 561 + ix + IMUL(32, i - NUM_ROWS) + j - NUM_COLS ];
		}
	}

	// Calculate my row of the results.
	float *resf = PITCH_GET(res, res_pitch, y);
	resf[x] = gf;
#endif
}

// Normalized cross correlation with half-T convolution.
void svlCudaPlainConv2d_full(const svlCudaPitch *pin, svlCudaPitch *pout, float *patch, int patch_w, int patch_h, int img_w, int img_h)
{
	// Copy patch over to GPU.
	CUDA_SAFE_CALL( cudaMemcpyToSymbol( kern_patch, patch, patch_w*patch_h*sizeof(float), 0, cudaMemcpyHostToDevice) );

	// Convolve.
	dim3 block(16, 16);
	if ( img_w == -1 ) img_w = pin->w;
	if ( img_h == -1 ) img_h = pin->h;
	dim3 grid( iDivUp(pout->w, block.x), iDivUp(pout->h, block.y) );

	svlCudaTimer::tic();
	if ( patch_w == 4 ) {
		plain_conv2d_slice_full<4> <<< grid, block >>>(pin->data, pin->pitch, img_w, img_h, pout->data, pout->pitch, pout->w, pout->h, patch_h);
	} else if ( patch_w == 8 ) {
		plain_conv2d_slice_full<8> <<< grid, block >>>(pin->data, pin->pitch, img_w, img_h, pout->data, pout->pitch, pout->w, pout->h, patch_h);
	} else if ( patch_w == 16 ) {
		plain_conv2d_slice_full<16> <<< grid, block >>>(pin->data, pin->pitch, img_w, img_h, pout->data, pout->pitch, pout->w, pout->h, patch_h);
	} else {
		cout << "svlCudaPlainConv2d_full: Mis-padded patch.\n"; assert(false);
	}
	svlCudaTimer::toc();
}


// Super-sized convolution, using two svlCudaPitch's.
#if 1
template<int P_w>
__global__ void conv_supersize_slice(const float *A, int apitch, const float *P, int ppitch, float *R, int rpitch,
									 int A_w, int A_h, int P_h, int R_w, int R_h)
{
	__shared__ float dataA[256];
	__shared__ float dataP[128];

	// Position variables.
	const int ix = threadIdx.x;
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;
	int y = blockIdx.y;
	const int out_y = y;
	// Position qualifiers.
	const bool on_Ax = x < A_w,
			   on_overAx = x + blockDim.x < A_w, // If can read in overflow data.
			   on_Rx = x < R_w,
			   on_Px = ix < P_w; // If threadIdx is on patch.

	// Loop over number of rows in convolution.
	float res = 0.0f;
	for ( int i=0; i<P_h; ++i ) {
		// Load in new row of A.
		const float *Af = PITCH_GET(A, apitch, y);
		dataA[ix] = on_Ax ? Af[x] : 0.0f;
		// Load in new row of patch and overflow data for A.
		if ( on_Px ) {
			dataA[ ix + blockDim.x ] = on_overAx ? Af[ x + blockDim.x ] : 0.0f;
			const float *Pf = PITCH_GET(P, ppitch, i);
			dataP[ix] = Pf[ix];
		}
		__syncthreads();

		// Compute incremental convolution result.
		if ( on_Rx ) {
			#pragma unroll
			for ( int j=0; j<P_w; ++j )
				res += dataA[ ix + j ] * dataP[j];
		}
		if ( ++y >= A_h )
			break;
		__syncthreads();
	}

	// Output result.
	if ( on_Rx ) {
		float *Rf = PITCH_GET(R, rpitch, out_y);
		Rf[x] = res;
	}
}
#elif 0
template<int P_w, int HELPERS>
__global__ void conv_supersize_slice(const float *A, int apitch, const float *P, int ppitch, float *R, int rpitch,
									 int A_w, int A_h, int P_h, int R_w, int R_h)
{
	__shared__ float dataA[256*HELPERS*2];
	__shared__ float dataP[128*HELPERS*2];

	// Position variables.
	const int ix = threadIdx.x;
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;
	const int A_off = 256*threadIdx.y;
	const int P_off = 128*threadIdx.y;
	const int help_off = (P_h/HELPERS)*threadIdx.y;
	int y = blockIdx.y + help_off;
	const int out_y = y;
	// Position qualifiers.
	const bool on_Ax = x < A_w,
			   on_overAx = x + blockDim.x < A_w, // If can read in overflow data.
			   on_Rx = x < R_w,
			   on_Px = ix < P_w; // If threadIdx is on patch.

	// Loop over number of rows in convolution.
	float res = 0.0f;
	for ( int i=0; i<P_h/HELPERS; ++i, ++y ) {
		// Load in new rows of A.
		const float *Af = PITCH_GET(A, apitch, y);
		dataA[ A_off + ix ] = on_Ax ? Af[x] : 0.0f;
		// Load in new row of patch and overflow data for A.
		if ( on_Px ) {
			dataA[ A_off + ix + blockDim.x ] = on_overAx ? Af[ x + blockDim.x ] : 0.0f;
			const float *Pf = PITCH_GET(P, ppitch, i + help_off);
			dataP[ P_off + ix ] = Pf[ix];
		}
		__syncthreads();

		// Compute incremental convolution result.
		if ( on_Rx ) {
			#pragma unroll
			for ( int j=0; j<P_w; ++j )
				res += dataA[ A_off + ix + j ] * dataP[ P_off + j ];
		}
		/*if ( ++y >= A_h )
			break;*/
		__syncthreads();
	}

	// Write helper data out.
	if ( HELPERS > 1 ) {
		dataA[ A_off + ix ] = res;
		__syncthreads();
		
		res = 0.0f;
		#pragma unroll
		for ( int i=0; i<HELPERS; ++i )
			res += dataA[ 256*i + ix ];
	}

	// Output result.
	if ( on_Rx && threadIdx.y == 0 ) {
		float *Rf = PITCH_GET(R, rpitch, out_y);
		Rf[x] = res;
	}
}
#endif

// Convolve two pitches, allowing for super-size convolutions.
void svlCudaSuperSizeConv(const svlCudaPitch *A, const svlCudaPitch *P, svlCudaPitch *R, int A_w, int A_h, int R_w, int R_h)
{
	if ( A_w == -1 ) A_w = A->w;
	if ( A_h == -1 ) A_h = A->h;
	// Do valid-size convolution by default.
	if ( R_w == -1 ) R_w = A_w - P->w + 1;
	if ( R_h == -1 ) R_h = A_h - P->h + 1;

	dim3 block(128, 1);
	dim3 grid( iDivUp(R_w, block.x), R_h );
	/*const int HELPERS = 4;
	dim3 block(128, HELPERS);*/
	/*const int HELPERS = 2;
	dim3 block(256, HELPERS);
	dim3 grid( iDivUp(R_w, block.x), R_h );*/

#define SUPER_CASE(num) case num: conv_supersize_slice<num> <<< grid, block >>>(A->data, A->pitch, P->data, P->pitch, R->data, R->pitch, A_w, A_h, P->h, R_w, R_h); break;
//#define SUPER_CASE(num) case num: conv_supersize_slice<num,HELPERS> <<< grid, block >>>(A->data, A->pitch, P->data, P->pitch, R->data, R->pitch, A_w, A_h, P->h, R_w, R_h); break;
//#define SUPER_CASE(num) case num: conv_supersize_slice<num> <<< grid, block >>>(A->data, A->pitch, R->data, R->pitch, A_w, A_h, P->h, R_w, R_h); break;
	svlCudaTimer::tic();
	switch ( P->w ) {
		SUPER_CASE(16);
		SUPER_CASE(32);
		SUPER_CASE(48);
		SUPER_CASE(64);
		SUPER_CASE(80);
		SUPER_CASE(96);
		SUPER_CASE(112);
		SUPER_CASE(128);
	}
	svlCudaTimer::toc();
}


