/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaMemHandler.cpp
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  Handles NVIDIA Cuda memory interface.
**
*****************************************************************************/

#include "svlBase.h"
#include "svlCudaMemHandler.h"
#include <iomanip>

// Static members.
bool svlCudaMemHandler::track_allocations = false;
list<NamedLine> svlCudaMemHandler::nls;
list<NamedPitch> svlCudaMemHandler::nps;
list<NamedPitch3d> svlCudaMemHandler::np3s;

// Default constructor
svlCudaMemHandler::svlCudaMemHandler()
{
}

svlCudaMemHandler::~svlCudaMemHandler()
{
}

// *** Allocation and free'ing functions ***

// Allocate linear device memory.
svlCudaLine* svlCudaMemHandler::allocLine(int w, const char *name)
{
	svlCudaLine *ret = new svlCudaLine();
	svlCudaAllocLine(w, *ret);
	if ( track_allocations )
		nls.push_back( NamedLine(ret, name) );
	return ret;
}

void svlCudaMemHandler::freeLine(svlCudaLine* &n)
{
	if ( track_allocations ) {
		// Remove from list of allocations.
		for ( list<NamedLine>::iterator it = nls.begin(); it != nls.end(); ++it ) {
			if ( it->first == n ) {
				svlCudaFreeLine(*n);
				nls.erase(it);
				n = NULL;
				return;
			}
		}
		SVL_LOG(SVL_LOG_WARNING, "Freeing un-tracked device memory: line @ " << n->data);
	}
	else
		svlCudaFreeLine(*n); // Just free it.
	n = NULL;
}

// Allocate and free pitches.
svlCudaPitch* svlCudaMemHandler::allocPitch(int w, int h, const char *name)
{
	svlCudaPitch *ret = new svlCudaPitch();
	svlCudaAllocPitch(w, h, ret);
	if ( track_allocations )
		nps.push_back( NamedPitch(ret, name) );
	return ret;
}

svlCudaPitch* svlCudaMemHandler::allocPitch(const IplImage *img, const char *name)
{
	svlCudaPitch *ret = new svlCudaPitch();
	svlCudaAllocPitch(img->width, img->height, ret);
	if ( track_allocations )
		nps.push_back( NamedPitch(ret, name) );
	IplToGPU(ret, img);
	return ret;
}

svlCudaPitch* svlCudaMemHandler::allocPitch3d(svlCudaPitch *p, const char *name)
{
	svlCudaPitch *ret = new svlCudaPitch();
	svlCudaAllocPitch(p->w, p->h, ret);
	if ( track_allocations )
		nps.push_back( NamedPitch(ret, name) );
	return ret;
}

void svlCudaMemHandler::freePitch(svlCudaPitch* &p)
{
	if ( track_allocations ) {
		// Remove from list of allocations.
		for ( list<NamedPitch>::iterator it = nps.begin(); it != nps.end(); ++it ) {
			if ( it->first == p ) {
				svlCudaFreePitch(p);
				nps.erase(it);
				p = NULL;
				return;
			}
		}
		SVL_LOG(SVL_LOG_WARNING, "Freeing un-tracked device memory: pitch @ " << p->data);
	}
	else
		svlCudaFreePitch(p); // Just free it.
	p = NULL;
}

// Allocate and free 3d pitches.
svlCudaPitch3d* svlCudaMemHandler::allocPitch3d(int w, int h, int z, const char *name)
{
	svlCudaPitch3d *ret = new svlCudaPitch3d();
	svlCudaAllocPitch3d(w, h, z, ret);
	if ( track_allocations )
		np3s.push_back( NamedPitch3d(ret, name) );
	return ret;
}

svlCudaPitch3d* svlCudaMemHandler::allocPitch3d(svlCudaPitch3d *p, const char *name)
{
	svlCudaPitch3d *ret = new svlCudaPitch3d();
	svlCudaAllocPitch3d(p->w, p->h, p->z, ret);
	if ( track_allocations )
		np3s.push_back( NamedPitch3d(ret, name) );
	return ret;
}

void svlCudaMemHandler::freePitch3d(svlCudaPitch3d* &p)
{
	if ( track_allocations ) {
		// Remove from list of allocations.
		for ( list<NamedPitch3d>::iterator it = np3s.begin(); it != np3s.end(); ++it ) {
			if ( it->first == p ) {
				svlCudaFreePitch3d(p);
				np3s.erase(it);
				p = NULL;
				return;
			}
		}
		SVL_LOG(SVL_LOG_WARNING, "Freeing un-tracked device memory: pitch 3d @ " << p->data);
	}
	else
		svlCudaFreePitch3d(p); // Just free it.
	p = NULL;
}


// Print a pretty number.
void pretty(ostream &os, int num)
{
	if ( num == 0 ) {
		os << "0";
		return;
	}
	int maxexp = int( ceil( ( floor( log(double(num))/log(10.0) ) + 1.0 ) / 3.0 ) );
	if ( maxexp == 0 ) {
		os << num; 
		return;
	}
	--maxexp;
	int div = int(pow(10.0, maxexp*3)), gi;
	os << (gi=num/div);
	int rem = num - gi*div;
	for ( ; maxexp>0; --maxexp ) {
		div /= 1000;
		gi = rem/div;
		os << "," << setw(3) << setfill('0') << gi;
		rem -= gi*div;
	}
}

// Send some random memory back and forth to the GPU, checking that it works (sanity check).
bool svlCudaMemHandler::testMemoryAccess(int w, int h)
{
	svlCudaPitch p;
	svlCudaAllocPitch(w, h, &p);
	// Create random memory.
	float *data = (float*)malloc(w*h*sizeof(float));
	for ( int i=0; i<w*h; ++i )
		data[i] = float(i);
	// Copy to and back out in verify.
	svlCudaPitchToGPU(&p, data);
	float *verify = (float*)malloc(w*h*sizeof(float));
	svlCudaPitchFromGPU(verify, &p);
	// Verify that verify matches data.
	bool ret = true;
	for ( int i=0; i<w*h && ret; ++i ) {
		if ( data[i] != verify[i] ) {
			printf("Mismatch at %d: %f %f\n", i, data[i], verify[i]);
			ret = false;
		}
	}
	svlCudaFreePitch(&p);
	free(data);
	free(verify);
	return ret;
}

// Write a line out to file.
void svlCudaMemHandler::line2file(const char *file, const svlCudaLine *n, int w)
{
	if ( w == -1 ) w = n->w;
	float *back = (float*)malloc(sizeof(float)*w);
	lineFromGPU(back, n);
	FILE *fo = fopen(file,"wb");
	//fwrite(&n->w,sizeof(int),1,fo);
	fwrite(back,sizeof(float),w,fo);
	fclose(fo);
	free(back);
}

// Write a pitch out to file.
void svlCudaMemHandler::ucharPitch2file(const char *file, const svlCudaPitch *p)
{
	float *back = (float*)malloc(sizeof(float)*p->size());
	pitchFromGPU(back, p);
	FILE *fo = fopen(file,"wb");
	int gi = p->w * 4;
	fwrite(&gi,sizeof(int),1,fo);
	fwrite(&p->h,sizeof(int),1,fo);
	fwrite(back,sizeof(float),p->w*p->h,fo);
	fclose(fo);
	free(back);
}

// Write a pitch out to file.
void svlCudaMemHandler::pitch2file(const char *file, const svlCudaPitch *p)
{
	float *back = (float*)malloc(sizeof(float)*p->size());
	pitchFromGPU(back, p);
	FILE *fo = fopen(file,"wb");
	fwrite(&p->w,sizeof(int),1,fo);
	fwrite(&p->h,sizeof(int),1,fo);
	fwrite(back,sizeof(float),p->size(),fo);
	fclose(fo);
	free(back);
}

// Write a 3d pitch to file.
void svlCudaMemHandler::pitch3d2file(const char *file, const svlCudaPitch3d *p)
{
	float *back = (float*)malloc(sizeof(float)*p->size());
	pitch3dFromGPU(back, p);
	FILE *fo = fopen(file,"wb");
	fwrite(&p->w,sizeof(int),1,fo);
	fwrite(&p->h,sizeof(int),1,fo);
	fwrite(&p->z,sizeof(int),1,fo);
	fwrite(back,sizeof(float),p->size(),fo);
	fclose(fo);
	free(back);
}

// Read a 3d pitch from file.
svlCudaPitch3d* svlCudaMemHandler::file2pitch3d(const char *file)
{
	FILE *fp = fopen(file,"rb");
	int w, h, z;
	fread(&w,sizeof(int),1,fp);
	fread(&h,sizeof(int),1,fp);
	fread(&z,sizeof(int),1,fp);
	float *data = (float*)malloc(sizeof(float)*w*h*z);
	fread(data,sizeof(float),w*h*z,fp);
	fclose(fp);

	svlCudaPitch3d *ret = svlCudaMemHandler::allocPitch3d(w,h,z);
	pitch3dToGPU(ret, data);
	free(data);
	return ret;
}

// Create an image containing the random indices of locations for fast cuda memory transfer and access on the device.
int svlCudaMemHandler::fillIndexImage(const vector<CvPoint> &locations, IplImage *ixage)
{
#if 0
	// Pointers for filling in each row.
	vector<int> col_ptrs(ixage->height, 0);
	// Fill in index image.
	for ( vector<CvPoint>::const_iterator it=locations.begin(); it!=locations.end(); ++it ) {
		CV_IMAGE_ELEM(ixage, int, it->y, col_ptrs[it->y]++) = it->x;
	}
	// Find max population in each row.
	int max_gi = col_ptrs[0];
	for ( vector<int>::const_iterator it=col_ptrs.begin(); it!=col_ptrs.end(); ++it )
		if ( *it > max_gi )
			max_gi = *it;
	// -1-alize remaining entries up to align-by-sixteen.
	int max_col = iAlignUp(max_gi, 16);
	for ( int r=0; r<ixage->height; ++r ) {
		for ( int c=col_ptrs[r]; c<max_col; ++c ) {
			CV_IMAGE_ELEM(ixage, int, r, c) = -1;
		}
	}
#else
	// Collaps row-column indices into image linearly.
	int ix = 0, r = 0, c = 0;
	for ( vector<CvPoint>::const_iterator loc=locations.begin(); loc!=locations.end(); ++loc ) {
		CV_IMAGE_ELEM(ixage, int, r, c++) = loc->y; // Row
		CV_IMAGE_ELEM(ixage, int, r, c++) = loc->x; // Column
		if ( c == ixage->width ) {
			++r;
			c = 0;
		}
	}
	if ( c == 0 ) {
		--r;
		c = ixage->width;
	}
	// -1-alize last row;
	for ( ; c<ixage->width; ++c ) {
		CV_IMAGE_ELEM(ixage, int, r, c) = -1;
	}
	return r+1;
#endif
}

// Fill a line with the random indices of locations for fast cuda memory transfer and access on the device.
void svlCudaMemHandler::fillIndexLine(const vector<CvPoint> &locations, svlCudaLine *ixline)
{
	// Collaps row-column indices into line linearly.
	int *data = (int*)malloc(sizeof(int)*int(locations.size())*2);
	int *ptr = data;
	for ( vector<CvPoint>::const_iterator loc=locations.begin(); loc!=locations.end(); ++loc ) {
		*ptr++ = loc->y; // Row
		*ptr++ = loc->x; // Column
	}
	lineToGPU(ixline, (float*)data, int(locations.size())*2);
	free(data);
}

// Print total amount of allocation.
void svlCudaMemHandler::printTotalAllocation() {
	if ( !track_allocations ) {
		//cout << "Allocations are not being tracked.\n";
		return;
	}
	int line_sz = 0, pitch_sz = 0, pitch3d_sz = 0;
	for ( list<NamedLine>::const_iterator it = nls.begin(); it != nls.end(); ++it )
		line_sz += it->first->w;
	for ( list<NamedPitch>::const_iterator it = nps.begin(); it != nps.end(); ++it )
		pitch_sz += it->first->size();
	for ( list<NamedPitch3d>::const_iterator it = np3s.begin(); it != np3s.end(); ++it )
		pitch_sz += it->first->size();

	printf("Total device allocation:\n");
	printf("  %d lines    for ", int(nls.size())); pretty(cout, line_sz*sizeof(float)); printf(" bytes\n");
	printf("  %d pitches  for ", int(nps.size())); pretty(cout, pitch_sz*sizeof(float)); printf(" bytes\n");
	printf("  %d pitch3ds for ", int(np3s.size())); pretty(cout, pitch3d_sz*sizeof(float)); printf(" bytes\n");
	printf("Total: %d structures for ", nls.size() + nps.size() + np3s.size());
	pretty(cout, (line_sz + pitch_sz + pitch3d_sz)*sizeof(float)); printf(" bytes\n");
}

// Print all individual allocations.
void svlCudaMemHandler::printAllAllocations() {
	if ( !track_allocations ) {
		//cout << "Allocations are not being tracked.\n";
		return;
	}
	int total_sz = 0;
	printf("All device allocations:\n");
	for ( list<NamedLine>::const_iterator it = nls.begin(); it != nls.end(); ++it ) {
		printf("  line    @ %08x: ", it->first->data); pretty(cout, it->first->w*sizeof(float)); printf(" bytes");
		if ( it->second )
			printf(" \"%s\"", it->second);
		cout << endl;
		total_sz += it->first->w;
	}
	for ( list<NamedPitch>::const_iterator it = nps.begin(); it != nps.end(); ++it ) {
		printf("  pitch   @ %08x: ", it->first->data); pretty(cout, it->first->size()*sizeof(float)); printf(" bytes");
		if ( it->second )
			printf(" \"%s\"", it->second);
		cout << endl;
		total_sz += it->first->w * it->first->h;
	}
	for ( list<NamedPitch3d>::const_iterator it = np3s.begin(); it != np3s.end(); ++it ) {
		printf("  pitch3d @ %08x: ", it->first->data); pretty(cout, it->first->size()*sizeof(float)); printf(" bytes");
		if ( it->second )
			printf(" \"%s\"", it->second);
		cout << endl;
		total_sz += it->first->size();
	}
	printf("Total device allocation: %d structures for ", nls.size() + nps.size() + np3s.size());
	pretty(cout, total_sz*sizeof(float)); printf(" bytes\n");
}

// Specialization for CvPoint.
void svlCudaMemHandler::CvPoint2IntLine(const vector<CvPoint> &vec, svlCudaLine *line, bool yx)
{
	// Collaps row-column indices into line linearly.
	int *data = (int*)malloc(sizeof(int)*2*int(vec.size()));
	int *ptr = data;
	for ( vector<CvPoint>::const_iterator v=vec.begin(); v!=vec.end(); ++v ) {
		if ( yx ) {
			*ptr++ = v->y;
			*ptr++ = v->x;
		} else {
			*ptr++ = v->x;
			*ptr++ = v->y;
		}
	}
	lineToGPU(line, (float*)data, 2*int(vec.size()));
	free(data);
}

// Ipl to GPU.
void svlCudaMemHandler::IplToGPU(svlCudaPitch *p, const IplImage *im, int row_offset, int col_offset)
{
	if ( im->depth != IPL_DEPTH_32F ) {
		float *data = (float*)malloc( sizeof(float)*( im->width - col_offset )*( im->height - row_offset ) );
		float *ptr = data;
		for ( int i=row_offset; i<im->height; ++i ) {
			for ( int j=col_offset; j<im->width; ++j ) {
				switch ( im->depth ) {
					case IPL_DEPTH_8U:
						*ptr++ = (float)CV_IMAGE_ELEM(im, unsigned char, i, j); break;
					case IPL_DEPTH_32S:
						*ptr++ = (float)CV_IMAGE_ELEM(im, int, i, j); break;
					case IPL_DEPTH_32F:
						*ptr++ = (float)CV_IMAGE_ELEM(im, float, i, j); break;
					case IPL_DEPTH_64F:
						*ptr++ = (float)CV_IMAGE_ELEM(im, double, i, j); break;
				}
			}
		}
		svlCudaPitchToGPU(p, data, im->width - col_offset, im->height - row_offset);
		free(data);
	} else if ( im->depth == IPL_DEPTH_32F ) {
		svlCudaPitchToGPU_all(p, (float*)( im->imageData + col_offset + row_offset*im->widthStep ), im->widthStep, im->width - col_offset, im->height - row_offset);
	}
	p->source = (void*)im;
}

// Ipl from GPU.
void svlCudaMemHandler::IplFromGPU(IplImage *im, const svlCudaPitch *p, int row_offset, int col_offset)
{
	if ( im->depth == IPL_DEPTH_32F ) {
		svlCudaPitchFromGPU_all((float*)( im->imageData + col_offset + row_offset*im->widthStep ), im->widthStep, p->w, p->h, p);
	} else {
		float *data = (float*)malloc(sizeof(float)*p->size());
		svlCudaPitchFromGPU(data, p);
		float *ptr = data;
		for ( int i=0; i<p->w; ++i ) {
			for ( int j=0; j<p->h; ++j ) {
				switch ( im->depth ) {
					case IPL_DEPTH_8U:
						CV_IMAGE_ELEM(im, unsigned char, row_offset + i, col_offset + j) = (unsigned char)*ptr++; break;
					case IPL_DEPTH_32S:
						CV_IMAGE_ELEM(im, int, row_offset + i, col_offset + j) = (int)*ptr++; break;
					case IPL_DEPTH_32F:
						CV_IMAGE_ELEM(im, float, row_offset + i, col_offset + j) = (float)*ptr++; break;
					case IPL_DEPTH_64F:
						CV_IMAGE_ELEM(im, double, row_offset + i, col_offset + j) = (double)*ptr++; break;
				}
			}
		}
		free(data);
	}
}

// Align a pitch to a specified width.
void svlCudaMemHandler::alignPitchWidth(svlCudaPitch* &orig, svlCudaPitch* &padded, int align)
{
	if ( orig->w % align != 0 ) {
		// Copy patch to padded patch.
		padded = alloc(iAlignUp(orig->w, align), orig->h);
		memset(padded, 0);
		pitchToPitch(padded, orig);
	} else {
		padded = orig;
		orig = NULL;
	}
}


