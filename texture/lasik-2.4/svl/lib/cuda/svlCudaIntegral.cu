/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2009, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaInegral.cu
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  Cuda integral image functions.
**
*****************************************************************************/

#include "svlCudaCommon.h"
#include "svlCudaMath.h"
#include "svlCudaTimer.h"

/*
 * Copyright 1993-2007 NVIDIA Corporation.  All rights reserved.
 *
 * NOTICE TO USER:   
 *
 * This source code is subject to NVIDIA ownership rights under U.S. and 
 * international Copyright laws.  Users and possessors of this source code 
 * are hereby granted a nonexclusive, royalty-free license to use this code 
 * in individual and commercial software.
 *
 * NVIDIA MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE 
 * CODE FOR ANY PURPOSE.  IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR 
 * IMPLIED WARRANTY OF ANY KIND.  NVIDIA DISCLAIMS ALL WARRANTIES WITH 
 * REGARD TO THIS SOURCE CODE, INCLUDING ALL IMPLIED WARRANTIES OF 
 * MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE.
 * IN NO EVENT SHALL NVIDIA BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, 
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS 
 * OF USE, DATA OR PROFITS,  WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE 
 * OR OTHER TORTIOUS ACTION,  ARISING OUT OF OR IN CONNECTION WITH THE USE 
 * OR PERFORMANCE OF THIS SOURCE CODE.  
 *
 * U.S. Government End Users.   This source code is a "commercial item" as 
 * that term is defined at  48 C.F.R. 2.101 (OCT 1995), consisting  of 
 * "commercial computer  software"  and "commercial computer software 
 * documentation" as such terms are  used in 48 C.F.R. 12.212 (SEPT 1995) 
 * and is provided to the U.S. Government only as a commercial end item.  
 * Consistent with 48 C.F.R.12.212 and 48 C.F.R. 227.7202-1 through 
 * 227.7202-4 (JUNE 1995), all U.S. Government End Users acquire the 
 * source code with only those rights set forth herein. 
 *
 * Any use of this source code in individual and commercial software must 
 * include, in the user documentation and internal comments to the code,
 * the above Disclaimer and U.S. Government End Users Notice.
 */


// Scan algorithm inner loop for inlining.
__device__ void scan_inner_loop(int n, int ai, int bi, int thid, float *temp_data)
{
	int offset = 1;
	// Build the sum in place up the tree
    for (int d = n/2; d > 0; d >>= 1) {
        __syncthreads();
        if (thid < d) {
            int ai = offset*(2*thid+1)-1;
            int bi = offset*(2*thid+2)-1;
            ai += CONFLICT_FREE_OFFSET(ai);
            bi += CONFLICT_FREE_OFFSET(bi);
            temp_data[bi] += temp_data[ai];
        }
        offset *= 2;
    }

    // Scan back down the tree
    // Clear the last element
    if ( thid == 0 ) {
        int index = n - 1;
        index += CONFLICT_FREE_OFFSET(index);
        temp_data[index] = 0;
    }
    // Traverse down the tree building the scan in place
    for (int d = 1; d < n; d *= 2) {
        offset /= 2;
        __syncthreads();
        if (thid < d) {
            int ai = offset*(2*thid+1)-1;
            int bi = offset*(2*thid+2)-1;
            ai += CONFLICT_FREE_OFFSET(ai);
            bi += CONFLICT_FREE_OFFSET(bi);
            float t = temp_data[ai];
            temp_data[ai] = temp_data[bi];
            temp_data[bi] += t;
        }
    }
    __syncthreads();
}

// Scan algorithm inner loop for inlining.
__device__ inline void scan_inner_loop_int(int n, int ai, int bi, int thid, int *temp_data)
{
	int offset = 1;
	// Build the sum in place up the tree
    for (int d = n/2; d > 0; d >>= 1) {
        __syncthreads();
        if (thid < d) {
            int ai = offset*(2*thid+1)-1;
            int bi = offset*(2*thid+2)-1;
            ai += CONFLICT_FREE_OFFSET(ai);
            bi += CONFLICT_FREE_OFFSET(bi);
            temp_data[bi] += temp_data[ai];
        }
        offset *= 2;
    }

    // Scan back down the tree
    // Clear the last element
    if ( thid == 0 ) {
        int index = n - 1;
        index += CONFLICT_FREE_OFFSET(index);
        temp_data[index] = 0;
    }
    // Traverse down the tree building the scan in place
    for (int d = 1; d < n; d *= 2) {
        offset /= 2;
        __syncthreads();
        if (thid < d) {
            int ai = offset*(2*thid+1)-1;
            int bi = offset*(2*thid+2)-1;
            ai += CONFLICT_FREE_OFFSET(ai);
            bi += CONFLICT_FREE_OFFSET(bi);
            int t = temp_data[ai];
            temp_data[ai] = temp_data[bi];
            temp_data[bi] += t;
        }
    }
    __syncthreads();
}

// Scan-scan2 parallel algorithm inner loop for inlining.
__device__ void scan_scan2_inner_loop(int n, int ai, int bi, int thid, float *temp_data, float *temp_data2)
{
	int offset = 1;
	// Build the sum in place up the tree
    for (int d = n/2; d > 0; d >>= 1) {
        __syncthreads();
        if (thid < d) {
            int ai = offset*(2*thid+1)-1;
            int bi = offset*(2*thid+2)-1;
            ai += CONFLICT_FREE_OFFSET(ai);
            bi += CONFLICT_FREE_OFFSET(bi);
            temp_data[bi] += temp_data[ai];
			temp_data2[bi] += temp_data2[ai];
        }
        offset *= 2;
    }

    // Scan back down the tree
    // Clear the last element
    if ( thid == 0 ) {
        int index = n - 1;
        index += CONFLICT_FREE_OFFSET(index);
        temp_data[index] = 0;
		temp_data2[index] = 0;
    }
    // Traverse down the tree building the scan in place
    for (int d = 1; d < n; d *= 2) {
        offset /= 2;
        __syncthreads();
        if (thid < d) {
            int ai = offset*(2*thid+1)-1;
            int bi = offset*(2*thid+2)-1;
            ai += CONFLICT_FREE_OFFSET(ai);
            bi += CONFLICT_FREE_OFFSET(bi);
            float t  = temp_data[ai];
			float t2 = temp_data2[ai];
            temp_data[ai] = temp_data[bi];
			temp_data2[ai] = temp_data2[bi];
            temp_data[bi] += t;
			temp_data2[bi] += t2;
        }
    }
    __syncthreads();
}

// NVIDIA scan algorithm implementation.
__global__ void row_scan_slice(float *din, int din_pitch, float *integ, int integ_pitch, int n)
{
    // Shared memory for scan kernels.
    __shared__ float temp_data[ 512 ];

	const int x_base = IMUL(n, blockIdx.x);
	//const int x_base = 256 * blockIdx.x;
	const int y = blockIdx.y;//IMUL(blockDim.y, blockIdx.y) + threadIdx.y; // Standard y value get

    int thid = threadIdx.x;
    int ai = thid;
    int bi = thid + (n/2);

    // Compute spacing to avoid bank conflicts
    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);

    // Cache the computational window in shared memory
	const float *din_row = PITCH_GET(din, din_pitch, y);
	temp_data[ai + bankOffsetA] = din_row[ x_base + ai ]; //g_idata[ai];
    temp_data[bi + bankOffsetB] = din_row[ x_base + bi ]; //g_idata[bi];

	scan_inner_loop(n, ai, bi, thid, temp_data);

    // Write results to global memory
	float *integ_row = PITCH_GET(integ, integ_pitch, y);
	integ_row[ x_base + ai ] = temp_data[ai + bankOffsetA];
    integ_row[ x_base + bi ] = temp_data[bi + bankOffsetB];
}
// Column partner of above.
__global__ void col_scan_slice(float *din, int din_pitch, float *integ, int integ_pitch, int n)
{
    // Shared memory for scan kernels.
    __shared__ float temp_data[ 512 ];

	const int y_base = IMUL(n, blockIdx.y);

    int thid = threadIdx.x;
    int ai = thid;
    int bi = thid + (n/2);

    // Compute spacing to avoid bank conflicts
    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);

    // Cache the computational window in shared memory
	const float *din_row_a = PITCH_GET(din, din_pitch, y_base + ai);
	//temp_data[ai + bankOffsetA] = din_row[ x_base + ai ]; //g_idata[ai];
	temp_data[ai + bankOffsetA] = din_row_a[blockIdx.x];
	const float *din_row_b = PITCH_GET(din, din_pitch, y_base + bi);
	temp_data[bi + bankOffsetB] = din_row_b[blockIdx.x];

	scan_inner_loop(n, ai, bi, thid, temp_data);

    // Write results to global memory
	float *integ_row_a = PITCH_GET(integ, integ_pitch, y_base + ai);
	integ_row_a[blockIdx.x] = temp_data[ai + bankOffsetA];
	float *integ_row_b = PITCH_GET(integ, integ_pitch, y_base + bi);
    integ_row_b[blockIdx.x] = temp_data[bi + bankOffsetB];
}

// Scan squared mirror implementation.
__global__ void row_scan2_slice(float *din, int din_pitch, float *integ, int integ_pitch, int n)
{
    // Shared memory for scan kernels.
    __shared__ float temp_data[ 512 ];

	const int x_base = IMUL(n, blockIdx.x);
	//const int x_base = 256 * blockIdx.x;
	const int y = blockIdx.y;//IMUL(blockDim.y, blockIdx.y) + threadIdx.y; // Standard y value get

    int thid = threadIdx.x;
    int ai = thid;
    int bi = thid + (n/2);

    // Compute spacing to avoid bank conflicts
    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);

    // Cache the computational window in shared memory
	const float *din_row = PITCH_GET(din, din_pitch, y);
	float d = din_row[ x_base + ai ];
	temp_data[ai + bankOffsetA] = d * d; //g_idata[ai];
	d = din_row[ x_base + bi ];
    temp_data[bi + bankOffsetB] = d * d; //g_idata[bi];

	scan_inner_loop(n, ai, bi, thid, temp_data);

    // Write results to global memory
	float *integ_row = PITCH_GET(integ, integ_pitch, y);
	integ_row[ x_base + ai ] = temp_data[ai + bankOffsetA];
    integ_row[ x_base + bi ] = temp_data[bi + bankOffsetB];
}
//// Column partner of above.
//__global__ void col_scan2_slice(float *din, int din_pitch, float *integ, int integ_pitch, int n)
//{
//    // Shared memory for scan kernels.
//    __shared__ float temp_data[ 512 ];
//
//	const int y_base = IMUL(n, blockIdx.y);
//
//    int thid = threadIdx.x;
//    int ai = thid;
//    int bi = thid + (n/2);
//
//    // Compute spacing to avoid bank conflicts
//    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
//    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);
//
//    // Cache the computational window in shared memory
//	const float *din_row_a = PITCH_GET(din, din_pitch, y_base + ai);
//	//temp_data[ai + bankOffsetA] = din_row[ x_base + ai ]; //g_idata[ai];
//	float d = din_row_a[blockIdx.x];
//	temp_data[ai + bankOffsetA] = d * d;
//	const float *din_row_b = PITCH_GET(din, din_pitch, y_base + bi);
//	d = din_row_b[blockIdx.x];
//	temp_data[bi + bankOffsetB] = d * d;
//
//	scan_inner_loop(n, ai, bi, thid, temp_data);
//
//    // Write results to global memory
//	float *integ_row_a = PITCH_GET(integ, integ_pitch, y_base + ai);
//	integ_row_a[blockIdx.x] = temp_data[ai + bankOffsetA];
//	float *integ_row_b = PITCH_GET(integ, integ_pitch, y_base + bi);
//    integ_row_b[blockIdx.x] = temp_data[bi + bankOffsetB];
//}

// Parallel scan and scan2 in same pass. Both integ and integ2 are computed off of din.
__global__ void row_scan_scan2_slice(float *din, int din_pitch, float *integ, int integ_pitch, float *integ2, int integ2_pitch, int n, int img_w)
{
    // Shared memory for scan kernels.
    __shared__ float temp_data[ 512 ];
	__shared__ float temp_data2[ 512 ];

	const int x_base = IMUL(n, blockIdx.x);
	//const int x_base = 256 * blockIdx.x;
	const int y = blockIdx.y;//IMUL(blockDim.y, blockIdx.y) + threadIdx.y; // Standard y value get

    int thid = threadIdx.x;
    int ai = thid;
    int bi = thid + (n/2);

    // Compute spacing to avoid bank conflicts
    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);

    // Cache the computational window in shared memory
	const float *din_row = PITCH_GET(din, din_pitch, y);
	bool right_on = x_base + ai < img_w;
	float d = right_on ? din_row[ x_base + ai ] : 0.0f;
	temp_data[ai + bankOffsetA] = d;
	temp_data2[ai + bankOffsetA] = d * d;

	right_on = x_base + bi < img_w;
	d = right_on ? din_row[ x_base + bi ] : 0.0f;
    temp_data[bi + bankOffsetB] = d;
    temp_data2[bi + bankOffsetB] = d * d;

	scan_scan2_inner_loop(n, ai, bi, thid, temp_data, temp_data2);

    // Write results to global memory
	float *integ_row = PITCH_GET(integ, integ_pitch, y);
	float *integ_row2 = PITCH_GET(integ2, integ2_pitch, y);
	if ( x_base + ai <= img_w ) {
		integ_row[ x_base + ai ] = temp_data[ai + bankOffsetA];
		integ_row2[ x_base + ai ] = temp_data2[ai + bankOffsetA];
	}
	if ( x_base + bi <= img_w ) {
		integ_row[ x_base + bi ] = temp_data[bi + bankOffsetB];
		integ_row2[ x_base + bi ] = temp_data2[bi + bankOffsetB];
	}
}
// Column partner of above. But integ and integ2 are computed off of each other.
// Actually slower than not.
//__global__ void col_scan_scan2_slice(float *integ, int integ_pitch, float *integ2, int integ2_pitch, int n)
//{
//    // Shared memory for scan kernels.
//    __shared__ float temp_data[ 512 ];
//	__shared__ float temp_data2[ 512 ];
//
//	const int y_base = IMUL(n, blockIdx.y);
//
//    int thid = threadIdx.x;
//    int ai = thid;
//    int bi = thid + (n/2);
//
//    // Compute spacing to avoid bank conflicts
//    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
//    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);
//
//    // Cache the computational window in shared memory
//	const float *din_row_a = PITCH_GET(integ, integ_pitch, y_base + ai);
//	temp_data[ai + bankOffsetA] = din_row_a[blockIdx.x];
//	const float *din_row_a2 = PITCH_GET(integ2, integ2_pitch, y_base + ai);
//	temp_data2[ai + bankOffsetA] = din_row_a2[blockIdx.x];
//	const float *din_row_b = PITCH_GET(integ, integ_pitch, y_base + bi);
//	temp_data[bi + bankOffsetB] = din_row_b[blockIdx.x];
//	const float *din_row_b2 = PITCH_GET(integ2, integ2_pitch, y_base + bi);
//	temp_data2[bi + bankOffsetB] = din_row_b2[blockIdx.x];
//
//	scan_scan2_inner_loop(n, ai, bi, thid, temp_data, temp_data2);
//
//    // Write results to global memory
//	float *integ_row_a = PITCH_GET(integ, integ_pitch, y_base + ai);
//	integ_row_a[blockIdx.x] = temp_data[ai + bankOffsetA];
//	float *integ_row_a2 = PITCH_GET(integ2, integ2_pitch, y_base + ai);
//	integ_row_a2[blockIdx.x] = temp_data2[ai + bankOffsetA];
//	float *integ_row_b = PITCH_GET(integ, integ_pitch, y_base + bi);
//    integ_row_b[blockIdx.x] = temp_data[bi + bankOffsetB];
//	float *integ_row_b2 = PITCH_GET(integ2, integ2_pitch, y_base + bi);
//    integ_row_b2[blockIdx.x] = temp_data2[bi + bankOffsetB];
//}


//// NVIDIA scan with batched column runs (actually slower).
//__global__ void col_scan_batched_slice(float *din, int din_pitch, float *integ, int integ_pitch, int n)
//{
//    // Shared memory for scan kernels.
//    __shared__ float temp_data[ 1024 ];
//
//	const int x = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;
//	const int y_base = IMUL(n, blockIdx.x);
//
//    int thid = threadIdx.x;
//    int ai = thid;
//    int bi = thid + (n/2);
//
//    // Compute spacing to avoid bank conflicts
//    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
//    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);
//
//	// Offset in shared data to this thread-row's memory.
//	float *temp_off = temp_data + IMUL(2*n, threadIdx.y);
//
//    // Cache the computational window in shared memory
//	const float *din_row_a = PITCH_GET(din, din_pitch, y_base + ai);
//	temp_off[ai + bankOffsetA] = din_row_a[x];
//	const float *din_row_b = PITCH_GET(din, din_pitch, y_base + bi);
//	temp_off[bi + bankOffsetB] = din_row_b[x];
//
//    int offset = 1;
//    // build the sum in place up the tree
//    for (int d = n/2; d > 0; d >>= 1) {
//        __syncthreads();
//        if (thid < d) {
//            int ai = offset*(2*thid+1)-1;
//            int bi = offset*(2*thid+2)-1;
//            ai += CONFLICT_FREE_OFFSET(ai);
//            bi += CONFLICT_FREE_OFFSET(bi);
//            temp_off[bi] += temp_off[ai];
//        }
//        offset *= 2;
//    }
//    // Sscan back down the tree
//    // Clear the last element
//    if (thid == 0) {
//        int index = n - 1;
//        index += CONFLICT_FREE_OFFSET(index);
//        temp_off[index] = 0;
//    }
//    // Traverse down the tree building the scan in place
//    for (int d = 1; d < n; d *= 2) {
//        offset /= 2;
//        __syncthreads();
//        if (thid < d) {
//            int ai = offset*(2*thid+1)-1;
//            int bi = offset*(2*thid+2)-1;
//            ai += CONFLICT_FREE_OFFSET(ai);
//            bi += CONFLICT_FREE_OFFSET(bi);
//            float t  = temp_off[ai];
//            temp_off[ai] = temp_off[bi];
//            temp_off[bi] += t;
//        }
//    }
//    __syncthreads();
//
//    // Write results to global memory
//	float *integ_row_a = PITCH_GET(integ, integ_pitch, y_base + ai);
//	integ_row_a[x] = temp_off[ai + bankOffsetA];
//	float *integ_row_b = PITCH_GET(integ, integ_pitch, y_base + bi);
//    integ_row_b[x] = temp_off[bi + bankOffsetB];
//}

// Missing row scan
__global__ void missing_row_scan_slice(float *din, int din_pitch, float *integ, int integ_pitch, int n)
{
	// Shared memory for scan kernels.
    __shared__ float temp_data[ 512 ];

	const int x_base = IMUL(n, blockIdx.x);
	const int y = IMUL(n, blockIdx.y+1);

    int thid = threadIdx.x;
    int ai = thid;
    int bi = thid + (n/2);

    // Compute spacing to avoid bank conflicts
    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);

    // Cache the computational window in shared memory
	const float *din_row = PITCH_GET(din, din_pitch, y - 1);
	temp_data[ai + bankOffsetA] = din_row[ x_base + ai ]; //g_idata[ai];
    temp_data[bi + bankOffsetB] = din_row[ x_base + bi ]; //g_idata[bi];

	scan_inner_loop(n, ai, bi, thid, temp_data);

    // Write results to global memory
	float *integ_row = PITCH_GET(integ, integ_pitch, y);
	integ_row[ x_base + ai ] = temp_data[ai + bankOffsetA];
    integ_row[ x_base + bi ] = temp_data[bi + bankOffsetB];
}
// Column partner of above.
__global__ void missing_col_scan_slice(float *din, int din_pitch, float *integ, int integ_pitch, int n)
{
    // Shared memory for scan kernels.
    __shared__ float temp_data[ 512 ];

	const int y_base = IMUL(n, blockIdx.y);
	const int x = IMUL(n, blockIdx.x+1);

    int thid = threadIdx.x;
    int ai = thid;
    int bi = thid + (n/2);

    // Compute spacing to avoid bank conflicts
    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);

    // Cache the computational window in shared memory
	const float *din_row_a = PITCH_GET(din, din_pitch, y_base + ai);
	temp_data[ai + bankOffsetA] = din_row_a[x - 1];
	const float *din_row_b = PITCH_GET(din, din_pitch, y_base + bi);
	temp_data[bi + bankOffsetB] = din_row_b[x - 1];

	scan_inner_loop(n, ai, bi, thid, temp_data);

    // Write results to global memory
	float *integ_row_a = PITCH_GET(integ, integ_pitch, y_base + ai);
	integ_row_a[x] = temp_data[ai + bankOffsetA];
	float *integ_row_b = PITCH_GET(integ, integ_pitch, y_base + bi);
    integ_row_b[x] = temp_data[bi + bankOffsetB];
}

__global__ void missing_row_scan2_slice(float *din, int din_pitch, float *integ, int integ_pitch, int n)
{
	// Shared memory for scan kernels.
    __shared__ float temp_data[ 512 ];

	const int x_base = IMUL(n, blockIdx.x);
	const int y = IMUL(n, blockIdx.y+1);

    int thid = threadIdx.x;
    int ai = thid;
    int bi = thid + (n/2);

    // Compute spacing to avoid bank conflicts
    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);

    // Cache the computational window in shared memory
	const float *din_row = PITCH_GET(din, din_pitch, y - 1);
	float d = din_row[ x_base + ai ];
	temp_data[ai + bankOffsetA] = d * d;
	d = din_row[ x_base + bi ];
    temp_data[bi + bankOffsetB] = d * d;

	scan_inner_loop(n, ai, bi, thid, temp_data);

    // Write results to global memory
	float *integ_row = PITCH_GET(integ, integ_pitch, y);
	integ_row[ x_base + ai ] = temp_data[ai + bankOffsetA];
    integ_row[ x_base + bi ] = temp_data[bi + bankOffsetB];
}

// Missing row scan
__global__ void missing_row_scan_scan2_slice(float *din, int din_pitch, float *integ, int integ_pitch, float *integ2, int integ2_pitch, int n)
{
    // Shared memory for scan kernels.
    __shared__ float temp_data[ 512 ];
	__shared__ float temp_data2[ 512 ];

	const int x_base = IMUL(n, blockIdx.x);
	const int y = IMUL(n, blockIdx.y+1);

    int thid = threadIdx.x;
    int ai = thid;
    int bi = thid + (n/2);

    // Compute spacing to avoid bank conflicts
    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);

    // Cache the computational window in shared memory
	const float *din_row = PITCH_GET(din, din_pitch, y - 1);
	float d = din_row[ x_base + ai ];
	temp_data[ai + bankOffsetA] = d;
	temp_data2[ai + bankOffsetA] = d * d;
	d = din_row[ x_base + bi ];
    temp_data[bi + bankOffsetB] = d;
	temp_data2[bi + bankOffsetB] = d * d;

	scan_scan2_inner_loop(n, ai, bi, thid, temp_data, temp_data2);

    // Write results to global memory
	float *integ_row = PITCH_GET(integ, integ_pitch, y);
	integ_row[ x_base + ai ] = temp_data[ai + bankOffsetA];
    integ_row[ x_base + bi ] = temp_data[bi + bankOffsetB];
	float *integ2_row = PITCH_GET(integ2, integ2_pitch, y);
	integ2_row[ x_base + ai ] = temp_data2[ai + bankOffsetA];
    integ2_row[ x_base + bi ] = temp_data2[bi + bankOffsetB];
}
// Column analogue of the above, but with only integ2.
__global__ void missing_col_scan2_slice(float *din, int din_pitch, float *integ2, int integ2_pitch, int n)
{
    // Shared memory for scan kernels.
    __shared__ float temp_data[ 512 ];

	const int y_base = IMUL(n, blockIdx.y);
	const int x = IMUL(n, blockIdx.x+1);

    int thid = threadIdx.x;
    int ai = thid;
    int bi = thid + (n/2);

    // Compute spacing to avoid bank conflicts
    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);

    // Cache the computational window in shared memory
	const float *din_row_a = PITCH_GET(din, din_pitch, y_base + ai);
	float d = din_row_a[x - 1];
	temp_data[ai + bankOffsetA] = d * d;
	const float *din_row_b = PITCH_GET(din, din_pitch, y_base + bi);
	d = din_row_b[x - 1];
	temp_data[bi + bankOffsetB] = d * d;

	scan_inner_loop(n, ai, bi, thid, temp_data);

    // Write results to global memory
	float *integ_row_a = PITCH_GET(integ2, integ2_pitch, y_base + ai);
	integ_row_a[x] = temp_data[ai + bankOffsetA];
	float *integ_row_b = PITCH_GET(integ2, integ2_pitch, y_base + bi);
    integ_row_b[x] = temp_data[bi + bankOffsetB];
}

// Fix block-starting rows in inegral image calculation (the first row of each new grid is zero but should be the cumulative sum from the last row).
__global__ void row_fix_slice(float *integ, int integ_pitch, int img_w, int dim)
{
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x + 1;
	if ( x > img_w )
		return;

	const int y = IMUL(dim, blockIdx.y + 1);
	const float *integ_inf = PITCH_GET(integ, integ_pitch, y - 1);
	float *integ_outf = PITCH_GET(integ, integ_pitch, y);
	integ_outf[x] += integ_inf[x];// + dataf[x - 1];
}

// Fix block-starting columns in inegral image calculation.
__global__ void col_fix_slice(float *data, int data_pitch, float *integ, int integ_pitch, int img_h, int dim)
{
	const int y = IMUL(blockDim.x, blockIdx.x) + threadIdx.x + 1;
	if ( y > img_h )
		return;

	const int x = IMUL(dim, blockIdx.y + 1);
	float *integf = PITCH_GET(integ, integ_pitch, y);
	float gf = integf[x] + integf[x - 1];

	// Add loss to dots.
	if ( y%dim == 0 ) {
		const float *dataf = PITCH_GET(data, data_pitch, y - 1);
		const float *integ_inf = PITCH_GET(integ, integ_pitch, y - 1);
		gf += integ_inf[x] + dataf[x - 1];
	}

	__syncthreads();
	
	// Write out result.
	integf[x] = gf;
}
// Fix for integral-squared data.
__global__ void col_fix2_slice(float *data, int data_pitch, float *integ, int integ_pitch, int img_h, int dim)
{
	const int y = IMUL(blockDim.x, blockIdx.x) + threadIdx.x + 1;
	if ( y > img_h )
		return;

	const int x = IMUL(dim, blockIdx.y + 1);
	float *integf = PITCH_GET(integ, integ_pitch, y);
	float gf = integf[x] + integf[x - 1];

	// Add loss to dots.
	if ( y%dim == 0 ) {
		const float *dataf = PITCH_GET(data, data_pitch, y - 1);
		const float *integ_inf = PITCH_GET(integ, integ_pitch, y - 1);
		float d = dataf[x - 1];
		gf += integ_inf[x] + d * d;
	}

	__syncthreads();
	
	// Write out result.
	integf[x] = gf;
}

/*
// Compute the integral image of pin; save into integ.
void svlCudaIntegral(svlCudaPitch &pin, svlCudaPitch *integ, svlCudaPitch *integ2, int img_w, int img_h, int dim)
{
	// Do one pass for both scan and scan2.
	dim3 row_block( dim/2, 1 );
	dim3 row_grid( iDivUp(img_w+1, dim), img_h );
	dim3 col_block( dim/2, 1 );
	dim3 col_grid( img_w+1, iDivUp(img_h+1, dim) );
	if ( integ2 == NULL ) {
		// Just do integral image.
		// Do row-wise cumulative summation.
		row_scan_slice<<< row_grid, row_block >>>(pin.data, pin.pitch, integ->data, integ->pitch, dim);
		// Do col-wise cumulative summation.
		col_scan_slice<<< col_grid, col_block >>>(integ->data, integ->pitch, integ->data, integ->pitch, dim);
	} else {
		// Do combined pass.
		row_scan_scan2_slice<<< row_grid, row_block >>>(pin.data, pin.pitch, integ->data, integ->pitch, integ2->data, integ2->pitch, dim, img_w);
		//col_scan_scan2_slice<<< col_grid, col_block >>>(integ->data, integ->pitch, integ2->data, integ2->pitch, 2*col_block.x);
		col_scan_slice<<< col_grid, col_block >>>(integ->data, integ->pitch, integ->data, integ->pitch, dim);
		col_scan_slice<<< col_grid, col_block >>>(integ2->data, integ2->pitch, integ2->data, integ2->pitch, dim);
	}
	//// Using the batched-columns function is actually slower.
	//dim3 col_block( 32, 4 );
	//dim3 col_grid( iDivUp(img_h, 2*col_block.x), iDivUp(img_w, col_block.y) );
	//col_scan_batched_slice<<< col_grid, col_block >>>(integ.data, integ.pitch, integ.data, integ.pitch, 2*col_block.x);

	// Manually scan and fix zero rows and columns.
	// Declare all blocks and sizes. Row dims:
	dim3 missing_row_block( dim/2, 1 );
	dim3 missing_row_grid( iDivUp(img_w, dim), img_h/dim );
	dim3 row_fix_block( iAlignUp(img_w,16), 1 );
	dim3 row_fix_grid( 1, img_h/dim );
	if ( row_fix_block.x > 512 ) {
		row_fix_block.x /= 2;
		row_fix_grid.x *= 2;
	}
	// Col dims.
	dim3 missing_col_block( dim/2, 1 );
	dim3 missing_col_grid( img_w/dim, iDivUp(img_h, dim) );
	dim3 col_fix_block( iAlignUp(img_h,16), 1 );
	dim3 col_fix_grid( 1, img_w/dim );
	if ( col_fix_block.x > 512 ) {
		col_fix_block.x /= 2;
		col_fix_grid.x *= 2;
	}
	// Integ.
	if ( img_h+1 >= dim ) {
		missing_row_scan_slice<<< missing_row_grid, missing_row_block >>>(pin.data, pin.pitch, integ->data, integ->pitch, dim);
		row_fix_slice<<< row_fix_grid, row_fix_block >>>(integ->data, integ->pitch, img_w, dim);
	}
	if ( img_w+1 >= dim ) {
		missing_col_scan_slice<<< missing_col_grid, missing_col_block >>>(pin.data, pin.pitch, integ->data, integ->pitch, dim);
		col_fix_slice<<< col_fix_grid, col_fix_block >>>(pin.data, pin.pitch, integ->data, integ->pitch, img_h, dim);
	}

	// Integral squared.
	if ( integ2 != NULL ) {
		if ( img_h+1 >= dim ) {
			missing_row_scan2_slice<<< missing_row_grid, missing_row_block >>>(pin.data, pin.pitch, integ2->data, integ2->pitch, dim);
			row_fix_slice<<< row_fix_grid, row_fix_block >>>(integ2->data, integ2->pitch, img_w, dim);
		}
		if ( img_w+1 >= dim ) {
			missing_col_scan2_slice<<< missing_col_grid, missing_col_block >>>(pin.data, pin.pitch, integ2->data, integ2->pitch, dim);
			col_fix2_slice<<< col_fix_grid, col_fix_block >>>(pin.data, pin.pitch, integ2->data, integ2->pitch, img_h, dim);
		}
	}
}*/

// Scan on lines with a max size of 1024.
template<int MAX_MEM>
__global__ void scan_mem_slice(const float *din, float *integ, int n, int stride)
{
    // Shared memory for scan kernels.
    __shared__ float temp_data[ MAX_MEM ];

	const int x_base = IMUL(stride, blockIdx.x);

    int thid = threadIdx.x;
    int ai = thid;
    int bi = thid + (n/2);

    // Compute spacing to avoid bank conflicts
    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);

    // Cache the computational window in shared memory
	temp_data[ ai + bankOffsetA ] = din[ x_base + ai ];
    temp_data[ bi + bankOffsetB ] = din[ x_base + bi ];

	scan_inner_loop(n, ai, bi, thid, temp_data);

    // Write results to global memory
	integ[ x_base + ai ] = temp_data[ ai + bankOffsetA ];
    integ[ x_base + bi ] = temp_data[ bi + bankOffsetB ];
}

// Do Matlab-style cumsum (i.e., integ(x) = \sum_{i=0:x} din(x)) on lines with a max size of 1024.
template<int MAX_MEM>
__global__ void cumsum_mem_slice(const float *din, float *integ, int n, int stride)
{
    // Shared memory for scan kernels.
    __shared__ float temp_data[ MAX_MEM ];

	const int x_base = IMUL(stride, blockIdx.x);

    int thid = threadIdx.x;
    int ai = thid;
    int bi = thid + (n/2);

    // Compute spacing to avoid bank conflicts
    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);

    // Cache the computational window in shared memory
	temp_data[ai + bankOffsetA] = din[ x_base + ai ]; //g_idata[ai];
    temp_data[bi + bankOffsetB] = din[ x_base + bi ]; //g_idata[bi];

	scan_inner_loop(n, ai, bi, thid, temp_data);

    // Write results to global memory (adding on original data value, too).
	integ[ x_base + ai ] = temp_data[ai + bankOffsetA] + din[ x_base + ai ];
    integ[ x_base + bi ] = temp_data[bi + bankOffsetB] + din[ x_base + bi ];
}

// Integration overload that works on lines.
void svlCudaIntegralLine(const svlCudaLine *n, svlCudaLine *integ, int w, int dim, bool do_cumsum, int stride)
{
	// Do one pass for both scan and scan2.
	dim3 block( dim/2, 1 );
	if ( w == -1 ) w = n->w;
	if ( stride == -1 ) stride = dim;
	dim3 grid( iDivUp(w, stride), 1 );
	if ( dim > 256 ) {
		if ( do_cumsum ) {
			cumsum_mem_slice <2048> <<< grid, block >>>(n->data, integ->data, dim, stride);
		} else {
			scan_mem_slice <2048> <<< grid, block >>>(n->data, integ->data, dim, stride);
		}
	} else {
		if ( do_cumsum ) {
			cumsum_mem_slice <512> <<< grid, block >>>(n->data, integ->data, dim, stride);
		} else {
			scan_mem_slice <512> <<< grid, block >>>(n->data, integ->data, dim, stride);
		}
	}
}

// The above, but templated.
// Scan on lines with a max size of 1024.
template<int MAX_MEM>
__global__ void scan_mem_int_slice(const int *din, int *integ, int n, int stride)
{
    // Shared memory for scan kernels.
    __shared__ int temp_data[ MAX_MEM ];

	const int x_base = IMUL(stride, blockIdx.x);

    int thid = threadIdx.x;
    int ai = thid;
    int bi = thid + (n/2);

    // Compute spacing to avoid bank conflicts
    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);

    // Cache the computational window in shared memory
	temp_data[ai + bankOffsetA] = din[ x_base + ai ]; //g_idata[ai];
    temp_data[bi + bankOffsetB] = din[ x_base + bi ]; //g_idata[bi];

	scan_inner_loop_int(n, ai, bi, thid, temp_data);

    // Write results to global memory
	integ[ x_base + ai ] = temp_data[ai + bankOffsetA];
    integ[ x_base + bi ] = temp_data[bi + bankOffsetB];
}

// Do Matlab-style cumsum (i.e., integ(x) = \sum_{i=0:x} din(x)) on lines with a max size of 1024.
template<int MAX_MEM>
__global__ void cumsum_mem_int_slice(const int *din, int *integ, int n, int stride)
{
    // Shared memory for scan kernels.
    __shared__ int temp_data[ MAX_MEM ];

	const int x_base = IMUL(stride, blockIdx.x);

    int thid = threadIdx.x;
    int ai = thid;
    int bi = thid + (n/2);

    // Compute spacing to avoid bank conflicts
    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);

    // Cache the computational window in shared memory
	temp_data[ai + bankOffsetA] = din[ x_base + ai ]; //g_idata[ai];
    temp_data[bi + bankOffsetB] = din[ x_base + bi ]; //g_idata[bi];

	scan_inner_loop_int(n, ai, bi, thid, temp_data);

    // Write results to global memory (adding on original data value, too).
	integ[ x_base + ai ] = temp_data[ai + bankOffsetA] + din[ x_base + ai ];
    integ[ x_base + bi ] = temp_data[bi + bankOffsetB] + din[ x_base + bi ];
}

// Integration overload that works on lines.
void svlCudaIntegralLineInt(const svlCudaLine *n, svlCudaLine *integ, int w, int dim, bool do_cumsum, int stride)
{
	// Do one pass for both scan and scan2.
	dim3 block( dim/2, 1 );
	if ( w == -1 ) w = n->w;
	if ( stride == -1 ) stride = dim;
	dim3 grid( iDivUp(w, stride), 1 );
	if ( dim > 256 ) {
		if ( do_cumsum ) {
			cumsum_mem_int_slice <2048> <<< grid, block >>>((int*)n->data, (int*)integ->data, dim, stride);
		} else {
			scan_mem_int_slice <2048> <<< grid, block >>>((int*)n->data, (int*)integ->data, dim, stride);
		}
	} else {
		if ( do_cumsum ) {
			cumsum_mem_int_slice <512> <<< grid, block >>>((int*)n->data, (int*)integ->data, dim, stride);
		} else {
			scan_mem_int_slice <512> <<< grid, block >>>((int*)n->data, (int*)integ->data, dim, stride);
		}
	}
}

// Off-by-one row scan implementation.
__global__ void row1_scan_slice(float *din, int din_pitch, float *integ, int integ_pitch, int n)
{
    // Shared memory for scan kernels.
    __shared__ float temp_data[ 512 ];

	const int x_base = IMUL(n, blockIdx.x);
	//const int x_base = 256 * blockIdx.x;
	const int y = blockIdx.y;//IMUL(blockDim.y, blockIdx.y) + threadIdx.y; // Standard y value get

    int thid = threadIdx.x;
    int ai = thid;
    int bi = thid + (n/2);

    // Compute spacing to avoid bank conflicts
    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);

    // Cache the computational window in shared memory
	const float *din_row = PITCH_GET(din, din_pitch, y);
	const float da = din_row[ x_base + ai ];
	temp_data[ai + bankOffsetA] = da;
	const float db = din_row[ x_base + bi ];
    temp_data[bi + bankOffsetB] = db;

	scan_inner_loop(n, ai, bi, thid, temp_data);

    // Write results to global memory
	float *integ_row = PITCH_GET(integ, integ_pitch, y); // One row up.
	integ_row[ x_base + 1 + ai ] = temp_data[ai + bankOffsetA] + da;
    integ_row[ x_base + 1 + bi ] = temp_data[bi + bankOffsetB] + db;
}
// Column partner of above.
template<int MAX_MEM>
__global__ void col1_scan_slice(float *din, int din_pitch, float *integ, int integ_pitch, int n, int img_h)
{
    // Shared memory for scan kernels.
    __shared__ float temp_data[ MAX_MEM ];

	const int x = blockIdx.x + 1;
	const int y_base = IMUL(n, blockIdx.y);

    int thid = threadIdx.x;
    int ai = thid;
    int bi = thid + (n/2);

    // Compute spacing to avoid bank conflicts
    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);

    // Cache the computational window in shared memory
	const float *din_row_a = PITCH_GET(din, din_pitch, y_base + ai);
	const bool a_on = y_base + ai < img_h;
	const float da = a_on ? din_row_a[x] : 0.0f;
	temp_data[ai + bankOffsetA] = da;
	const float *din_row_b = PITCH_GET(din, din_pitch, y_base + bi);
	const bool b_on = y_base + bi < img_h;
	const float db = b_on ? din_row_b[x] : 0.0f;
	temp_data[bi + bankOffsetB] = db;

	scan_inner_loop(n, ai, bi, thid, temp_data);

	__syncthreads();

    // Write results to global memory
	if ( a_on ) {
		float *integ_row_a = PITCH_GET(integ, integ_pitch, y_base + ai + 1);
		integ_row_a[x] = temp_data[ai + bankOffsetA] + da; // One column over.
	}
	if ( b_on ) {
		float *integ_row_b = PITCH_GET(integ, integ_pitch, y_base + bi + 1);
		integ_row_b[x] = temp_data[bi + bankOffsetB] + db; // One column over.
	}
}

// Parallel off-by-one scan and scan2 in same pass. Both integ and integ2 are computed off of din.
template<int MAX_MEM>
__global__ void row1_scan_scan2_slice(float *din, int din_pitch, float *integ, int integ_pitch, float *integ2, int integ2_pitch, int n, int img_w)
{
    // Shared memory for scan kernels.
    __shared__ float temp_data[ MAX_MEM ];
	__shared__ float temp_data2[ MAX_MEM ];

	int x_base = IMUL(n, blockIdx.x);
	const int y = blockIdx.y;//IMUL(blockDim.y, blockIdx.y) + threadIdx.y; // Standard y value get

    int thid = threadIdx.x;
    int ai = thid;
    int bi = thid + (n/2);

    // Compute spacing to avoid bank conflicts
    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);

    // Cache the computational window in shared memory
	const float *din_row = PITCH_GET(din, din_pitch, y);
	bool a_on = x_base + ai < img_w;
	const float da = a_on ? din_row[ x_base + ai ] : 0.0f;
	const float da2 = da * da;
	temp_data[ai + bankOffsetA] = da;
	temp_data2[ai + bankOffsetA] = da2;
	bool b_on = x_base + bi < img_w;
	const float db = b_on ? din_row[ x_base + bi ] : 0.0f;
	const float db2 = db * db;
    temp_data[bi + bankOffsetB] = db;
    temp_data2[bi + bankOffsetB] = db2;

	scan_scan2_inner_loop(n, ai, bi, thid, temp_data, temp_data2);

  // Write results to global memory
	float *integ_row = PITCH_GET(integ, integ_pitch, y);
	float *integ_row2 = PITCH_GET(integ2, integ2_pitch, y);
	if ( blockIdx.x == 0 && threadIdx.x == 0 ) {
		integ_row[0] = 0.0f;
		integ_row2[0] = 0.0f;
	}
	a_on = x_base + ai < img_w;
	b_on = x_base + bi < img_w;
	if ( a_on ) {
		integ_row[ x_base + 1 + ai ] = temp_data[ai + bankOffsetA] + da;
		integ_row2[ x_base + 1 + ai ] = temp_data2[ai + bankOffsetA] + da2;
	}
	if ( b_on ) {
		integ_row[ x_base + 1 + bi ] = temp_data[bi + bankOffsetB] + db;
		integ_row2[ x_base + 1 + bi ] = temp_data2[bi + bankOffsetB] + db2;
	}
}

void svlCudaIntegralBlock(const svlCudaPitch *pin, svlCudaPitch *integ, svlCudaPitch *tempint, svlCudaPitch *integ2, svlCudaPitch *tempint2, int img_w, int img_h, int dim)
{
	if ( dim > 512 ) {
		cout << "svlCudaInegralBlock not implemented for dim > 512.\n";
		assert(false);
	}
	// Do one pass for both scan and scan2.
	dim3 row_block( dim/2, 1 );
	dim3 row_grid( iDivUp(img_w, dim), img_h ); 
	dim3 col_block( dim/2, 1 );
	dim3 col_grid( img_w, iDivUp(img_h, dim) );
	/*printf("row block: %d,%d\n", row_block.x, row_block.y);
	printf("row grid: %d,%d\n", row_grid.x, row_grid.y);
	printf("col block: %d,%d\n", col_block.x, col_block.y);
	printf("col grid: %d,%d\n", col_grid.x, col_grid.y);*/
	if ( integ2 == NULL ) {
		// Just do integral image.
		assert(false);
	} else if ( tempint2 == NULL ) {
		// Just use the one tempint, so do both individually (no parallel row scan).
		assert(false);
	} else {
		// Do combined pass.
		if ( dim == 32 ) {
			row1_scan_scan2_slice <64> <<< row_grid, row_block >>>(pin->data, pin->pitch, tempint->data, tempint->pitch, tempint2->data, tempint2->pitch, dim, img_w);
		} else if ( dim == 64 ) {
			row1_scan_scan2_slice <128> <<< row_grid, row_block >>>(pin->data, pin->pitch, tempint->data, tempint->pitch, tempint2->data, tempint2->pitch, dim, img_w);
		} else if ( dim == 128 ) {
			row1_scan_scan2_slice <256> <<< row_grid, row_block >>>(pin->data, pin->pitch, tempint->data, tempint->pitch, tempint2->data, tempint2->pitch, dim, img_w);
		} else if ( dim == 512 ) {
			row1_scan_scan2_slice <1024> <<< row_grid, row_block >>>(pin->data, pin->pitch, tempint->data, tempint->pitch, tempint2->data, tempint2->pitch, dim, img_w);
		} else {
			row1_scan_scan2_slice <512> <<< row_grid, row_block >>>(pin->data, pin->pitch, tempint->data, tempint->pitch, tempint2->data, tempint2->pitch, dim, img_w);
		}

		if ( dim == 512 ) {
			col1_scan_slice <1024> <<< col_grid, col_block >>>(tempint->data, tempint->pitch, integ->data, integ->pitch, dim, img_h);
			col1_scan_slice <1024> <<< col_grid, col_block >>>(tempint2->data, tempint2->pitch, integ2->data, integ2->pitch, dim, img_h);
		} else {
			col1_scan_slice <512> <<< col_grid, col_block >>>(tempint->data, tempint->pitch, integ->data, integ->pitch, dim, img_h);
			col1_scan_slice <512> <<< col_grid, col_block >>>(tempint2->data, tempint2->pitch, integ2->data, integ2->pitch, dim, img_h);
		}
	}
}

void svlCudaRowIntegralBlock(const svlCudaPitch *pin, svlCudaPitch *integ, svlCudaPitch *integ2, int img_w, int img_h, int dim)
{
	if ( dim > 512 ) {
		cout << "svlCudaInegralBlock not implemented for dim > 512.\n";
		assert(false);
	}

	if ( img_w == -1 ) img_w = pin->w;
	if ( img_h == -1 ) img_h = pin->h;
	dim3 row_block( dim/2, 1 );
	dim3 row_grid( iDivUp(img_w, dim), img_h );
	if ( integ2 == NULL ) {
		// Just do integral image.
		assert(false);
	} else {
		// Do combined pass on scan and scan2.
		if ( dim == 32 ) {
			row1_scan_scan2_slice <64> <<< row_grid, row_block >>>(pin->data, pin->pitch, integ->data, integ->pitch, integ2->data, integ2->pitch, dim, img_w);
		} else if ( dim == 64 ) {
			row1_scan_scan2_slice <128> <<< row_grid, row_block >>>(pin->data, pin->pitch, integ->data, integ->pitch, integ2->data, integ2->pitch, dim, img_w);
		} else if ( dim == 128 ) {
			row1_scan_scan2_slice <256> <<< row_grid, row_block >>>(pin->data, pin->pitch, integ->data, integ->pitch, integ2->data, integ2->pitch, dim, img_w);
		} else if ( dim == 512 ) {
			row1_scan_scan2_slice <1024> <<< row_grid, row_block >>>(pin->data, pin->pitch, integ->data, integ->pitch, integ2->data, integ2->pitch, dim, img_w);
		} else {
			row1_scan_scan2_slice <512> <<< row_grid, row_block >>>(pin->data, pin->pitch, integ->data, integ->pitch, integ2->data, integ2->pitch, dim, img_w);
		}
	}
}


// Do integral image row integration.
template<bool square>
__global__ void integral_row_slice(const float *din, int dpitch, float *integ, int ipitch, int img_w, int img_h)
{
    // Shared memory for scan kernels.
    __shared__ float temp_data[ 1024 ];

	const int x_base = IMUL(2*blockDim.x, blockIdx.x);

    int thid = threadIdx.x;
    int ai = thid;
    int bi = thid + blockDim.x;

    // Compute spacing to avoid bank conflicts
    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);
	const int xa = x_base + ai, xb = x_base + bi;

    // Cache the computational window in shared memory
	const float *dinf = PITCH_GET(din, dpitch, blockIdx.y);
	temp_data[ ai + bankOffsetA ] = xa < img_w ? ( square ? dinf[xa]*dinf[xa] : dinf[xa] ) : 0.0f;
    temp_data[ bi + bankOffsetB ] = xb < img_w ? ( square ? dinf[xb]*dinf[xb] : dinf[xb] ) : 0.0f;
	// Add in last-block far right column entry.
	float gf = 0.0f;
	const bool add_prev = threadIdx.x == 0 && blockIdx.x > 0;
	if ( add_prev )
		temp_data[ ai + bankOffsetA ] += gf = x_base - 1 < img_w ? ( square ? dinf[x_base-1]*dinf[x_base-1] : dinf[x_base-1] ) : 0.0f;

	scan_inner_loop(2*blockDim.x, ai, bi, thid, temp_data);

    // Write results to global memory
	float *integf = PITCH_GET(integ, ipitch, blockIdx.y);
	if ( xa <= img_w )
		integf[xa] = temp_data[ ai + bankOffsetA ] + ( add_prev ? gf : 0.0f );
	if ( xb <= img_w )
		integf[xb] = temp_data[ bi + bankOffsetB ];
}

// Column partner of above.
__global__ void integral_col_slice(const float *din, int dpitch, float *integ, int ipitch, int img_w, int img_h)
{
    // Shared memory for scan kernels.
    __shared__ float temp_data[ 1024 ];

	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;
	const bool x_on = x <= img_w;
	const int y_base = IMUL(2*blockDim.y, blockIdx.y);
	float *temp_off = temp_data + IMUL(2*blockDim.y, threadIdx.x);

    int thid = threadIdx.y;
    int ai = thid;
    int bi = thid + blockDim.y;

    // Compute spacing to avoid bank conflicts
    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);

    // Cache the computational window in shared memory
	int ya = y_base + ai, yb = y_base + bi;
	const float *dinfa = PITCH_GET(din, dpitch, ya);
	const float *dinfb = PITCH_GET(din, dpitch, yb);
	temp_off[ ai + bankOffsetA ] = x_on && ya < img_h ? dinfa[x] : 0.0f;
    temp_off[ bi + bankOffsetB ] = x_on && yb < img_h ? dinfb[x] : 0.0f;
	// Add in last-block top row entry.
	float gf = 0.0f;
	const bool add_prev = threadIdx.y == 0 && x <= img_w && blockIdx.y > 0;
	if ( add_prev ) {
		const float *dinf = PITCH_GET(din, dpitch, y_base - 1);
		temp_off[ ai + bankOffsetA ] += gf = dinf[x];
	}

	scan_inner_loop(2*blockDim.y, ai, bi, thid, temp_off);

    // Write results to global memory
	if ( x_on ) {
		if ( ya <= img_h ) {
			float *integf = PITCH_GET(integ, ipitch, ya);
			integf[x] = temp_off[ ai + bankOffsetA ] + ( add_prev ? gf : 0.0f );
		}
		if ( yb <= img_h ) {
			float *integf = PITCH_GET(integ, ipitch, yb);
			integf[x] = temp_off[ bi + bankOffsetB ];
		}
	}
}

// Sum up the blocks of the integral in the columnwise sense.
__global__ void integral_col_sum_slice(float *integ, int ipitch, int img_w, int img_h, int n)
{
	__shared__ float integ_prev[16];
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;
	int y = blockDim.y + threadIdx.y;

	if ( x <= img_w ) {
		// Read in previous block's top integral row.
		if ( threadIdx.y == 0 ) {
			float *integf = PITCH_GET(integ, ipitch, blockDim.y - 1);
			integ_prev[threadIdx.x] = integf[x];
		}
		__syncthreads();

		// Loop.
		for ( int i=1; i<n-1; ++i ) {
			float *integf = PITCH_GET(integ, ipitch, y);
			float gf = integf[x] += integ_prev[threadIdx.x];
			__syncthreads();

			// Save top row.
			if ( threadIdx.y == blockDim.y - 1 )
				integ_prev[threadIdx.x] = gf;

			y += blockDim.y;
			__syncthreads();
		}

		// Have to check y bounds on last iteration.
		if ( y <= img_h ) {
			float *integf = PITCH_GET(integ, ipitch, y);
			integf[x] += integ_prev[threadIdx.x];
		}
	}
}
// Sum up the blocks of the integral in the rowwise sense.
__global__ void integral_row_sum_slice(float *integ, int ipitch, int img_w, int n)
{
	__shared__ float prev[1];
	int x = blockDim.x + threadIdx.x;
	float *integf = PITCH_GET(integ, ipitch, blockIdx.y);

	// Read in previous block's top integral row.
	if ( threadIdx.x == 0 )
		*prev = integf[ blockDim.x - 1 ];
	__syncthreads();

	// Loop.
	for ( int i=1; i<n-1; ++i ) {
		float gf = integf[x] += *prev;
		__syncthreads();

		// Save far right entry.
		if ( threadIdx.x == blockDim.x - 1 )
			*prev = gf;

		x += blockDim.x;
		__syncthreads();
	}

	// Have to check y bounds on last iteration.
	if ( x <= img_w )
		integf[x] += *prev;
}

// Do regular integral and integral-squared images.
void svlCudaIntegral(const svlCudaPitch *pin, svlCudaPitch *integ, svlCudaPitch *temp, svlCudaPitch *integ2, int img_w, int img_h)
{
	if ( img_w == -1 ) img_w = pin->w;
	if ( img_h == -1 ) img_h = pin->h;
	// Do row integration.
	int dim_w = min( 512, svlCudaMath::iAlignUpPow2(img_w+1) );
	if ( abs(img_w - iAlignUp(img_w,dim_w) ) > dim_w/2 ) {
		// If error is greater than one power of 2 down, go down to that power.
		dim_w /= 2;
	}
	//printf("dim_w: %d\n", dim_w);
	dim3 row_block( dim_w/2, 1 );
	dim3 row_grid( iDivUp(img_w+1, dim_w), img_h );
	/*printf("row block: %d,%d\n", row_block.x, row_block.y);
	printf("row grid: %d,%d\n", row_grid.x, row_grid.y);*/

	svlCudaTimer::tic();
	// Do first-pass integration across rows.
	integral_row_slice <false> <<< row_grid, dim_w/2 >>>(pin->data, pin->pitch, temp->data, temp->pitch, img_w, img_h);

	// Do second pass down columns.
	dim3 col_block( 16, 16 );
	dim3 col_grid( iDivUp(img_w+1, col_block.x), iDivUp(img_h+1, 2*col_block.y) );
	integral_col_slice<<< col_grid, col_block >>>(temp->data, temp->pitch, integ->data, integ->pitch, img_w, img_h);

	// Integrate the sub-blocks over columns ...
	dim3 col_sum_block( 16, 32 );
	integral_col_sum_slice<<< col_grid.x, col_sum_block >>>(integ->data, integ->pitch, img_w, img_h, col_grid.y);
	// ... and rows.
	dim3 row_sum_grid( 1, img_h+1 );
	integral_row_sum_slice<<< row_sum_grid, dim_w >>>(integ->data, integ->pitch, img_w, row_grid.x);

	if ( integ2 ) {
		// Compute squared-integral image.
		integral_row_slice <true> <<< row_grid, dim_w/2 >>>(pin->data, pin->pitch, temp->data, temp->pitch, img_w, img_h);
		integral_col_slice<<< col_grid, col_block >>>(temp->data, temp->pitch, integ2->data, integ2->pitch, img_w, img_h);
		integral_col_sum_slice<<< col_grid.x, col_sum_block >>>(integ2->data, integ2->pitch, img_w, img_h, col_grid.y);
		integral_row_sum_slice<<< row_sum_grid, dim_w >>>(integ2->data, integ2->pitch, img_w, row_grid.x);
	}
	svlCudaTimer::toc();
}


