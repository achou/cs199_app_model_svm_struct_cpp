/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaSlidingWindowDetector.h
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**              Sid Batra <sidbatra@stanford.edu>
**              Ian Goodfellow <ia3n@cs.stanford.edu>
**              Olga Russakovsky <olga@cs.stanford.edu>
**				Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  Defines a multi-channel interface for sliding-window boosted-classifier 
**  object detectors using Cuda for feature extraction and decision tree
**  evaluation.
**
*****************************************************************************/

#pragma once

#include <cassert>
#include <iostream>
#include <vector>
#include <map>
#include <string>

#include "cv.h"
#include "cxcore.h"
#include "ml.h"

#include "svlVision.h"
#include "svlBase.h"
#include "svlCudaCommon.h"
#include "svlCudaConvolution.h"
#include "svlCudaDecisionTreeSet.h"
#include "svlCudaMemHandler.h"
#include "svlCudaMath.h"
#include "svlFinalPatchDictionary.h"

using namespace std;

// svlCudaSlidingWindowDetector ---------------------------------------------------


#if 0
typedef vector<vector<double> > Tsamples;

class svlCudaSlidingWindowDetector {
protected:
	// This order of declarations ensures no warnings on constructors.
	int _windowWidth, _windowHeight;
	string _objectName;
	float _threshold;
	int _delta_x, _delta_y;
	float _delta_scale;

	svlFinalPatchDictionary dict;
	bool patches_cached;
	int max_channels;
	svlCudaLine *cachedLine;
	vector<CachedPatch> *cachedPatches;
	svlCudaDecisionTreeSet *dtree;

	// Pitch identifiers
	svlCudaPitch *_IMG, *_INTEG, *_INTEG2,
		*_CONV, *_RESP, *_FACTORS,
		*_TEMP1, *_TEMP2, *_TEMP3, *_TEMP4;

	// Cached patch binning structure.
	vector<vector<vector<pair<int,svlPatch*> > > > patchBins;

public:
	svlCudaSlidingWindowDetector(int max_img_w, int max_img_h, const char *name = NULL, int w = 32, int h = 32);
	virtual ~svlCudaSlidingWindowDetector();

	// Evaluate classifer over all shifts and scales
	void classifyImage(const vector<IplImage *>& images_in,
		svlObject2dFrame *objects = NULL,
		ScaleLocations *locs = NULL,
		ScaleFeatures *F = NULL,
		const svlObject2dFrame *posFrames = NULL,
		const svlObject2dFrame *negFrames = NULL,
		bool lasmeds = true);

	// Labeling functions.
	virtual void computeLabels(vector<CvPoint> &locations, double scale, 
		const svlObject2dFrame *posFrames, 
		const svlObject2dFrame *negFrames,
		vector<int> &labels);
	virtual void featurePostProcessing(const vector<CvPoint> &locations,
		const vector<int> &labels,
		const vector<vector<float> > F);

	// Find interesting windows.
	void createInterestingWindowLocations(const svlCudaPitch *img, int img_w, int img_h, vector<CvPoint>& locations) const;
	void createInterestingWindowLocations(IplImage *image, vector<CvPoint>& locations) const;

	// Pad patches and cache GPU-side for only device-to-device copies during computation.
	void padAndCachePatches();
	void padAndCachePatches8U();
	svlFinalPatchDictionary* getDict() {
		return &dict;
	}
	vector<CachedPatch>* getCachedPatches() {
		return cachedPatches;
	}

	// Load a decision tree.
	void loadCudaDTree(const char *filename);
	void loadCudaDTree15(const char *filename);

	// Other members.
	inline void setThreshold(float t) {
		_threshold = t;
	}
	inline void setDeltas(int dx, int dy, double dscale) {
		_delta_x = dx; _delta_y = dy; _delta_scale = dscale;
	}
	inline bool readDictionary(const char *filename, bool normalize = false) {
		return dict.read(filename, normalize);
	}
	inline void printDictionary() {
		dict.print();
	}
};

#endif


