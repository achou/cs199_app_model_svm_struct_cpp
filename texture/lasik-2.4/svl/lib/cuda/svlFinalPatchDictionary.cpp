/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlFinalPatchDictionary.cpp
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
**
*****************************************************************************/


#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <limits>
#include <string>
#include <sstream>
#include "cv.h"
#include "cxcore.h"
#include "highgui.h"

#include "svlFinalPatchDictionary.h"

#if defined(_WIN32)||defined(WIN32)||defined(__WIN32__)
#undef max
#undef USE_THREADS
#endif

using namespace std;


// Initialize by reading from file.
svlFinalPatchDictionary::svlFinalPatchDictionary(const char *file)
{
	read(file);
	_entries = new vector<svlPatch*>();
}

// Initialize window size.
svlFinalPatchDictionary::svlFinalPatchDictionary(unsigned width, unsigned height)
{
	_windowSize = cvSize(width, height);
	_entries = new vector<svlPatch*>();
}

// Initialize from another dictionary.
svlFinalPatchDictionary::svlFinalPatchDictionary(const svlFinalPatchDictionary& d)
: _entries(d._entries)
{
	_windowSize = d._windowSize;
}

svlFinalPatchDictionary::~svlFinalPatchDictionary()
{
	// Delete patches.
	if ( _entries != NULL ) {
		for ( vector<svlPatch*>::iterator it=_entries->begin(); it!=_entries->end(); ++it )
			delete *it;
		delete _entries;
	}
}

void svlFinalPatchDictionary::clear()
{
	if ( _entries != NULL ) {
		for ( vector<svlPatch*>::iterator it=_entries->begin(); it!=_entries->end(); ++it )
			delete *it;
		_entries->clear();
	} else {
		_entries = new vector<svlPatch*>();
	}
}

// Ready dictionary entries out of a file.
bool svlFinalPatchDictionary::read(const char *filename, bool normalize)
{
	XMLNode root = XMLNode::parseFile(filename);
	bool ret = read(root, normalize);
	if ( !ret ) {
		cout << "Could not read patch dictionary file " << filename << endl;
	}
	return ret;
}

// Read a general patch from an XML node.
svlPatch* svlFinalPatchDictionary::readPatch(XMLNode& node, bool normalize)
{
	svlPatch *ret = new svlPatch();

	// read valid region
	if (node.getAttribute("validRect")) {
		vector<int> v;
		parseString<int>(node.getAttribute("validRect"), v);
		assert(v.size() == 4);
		ret->rect = cvRect(v[0], v[1], v[2], v[3]);
	} else {
		ret->rect = cvRect(0, 0, 0, 0);
	}

	// read channel
	if (node.getAttribute("channel")) {
		ret->channel = atoi(node.getAttribute("channel"));
	} else {
		ret->channel = 0;
	}

	// read patch size
	int w = 0;
	int h = 0;
	if (node.getAttribute("size")) {
		vector<int> v;
		parseString<int>(node.getAttribute("size"), v);
		assert(v.size() == 2);
		w = v[0]; h = v[1];
	}

	// read patch data
	if ( w > 0 && h > 0 ) {
		ret->patch = cvCreateImage(cvSize(w, h), IPL_DEPTH_32F, 1);
		vector<float> v;
		parseString(node.getText(), v);
		assert(v.size() == (unsigned)(w * h));
		unsigned n = 0;
		float mean = 0.0f;
		for (unsigned y = 0; y < (unsigned)ret->patch->height; y++) {
			float *p = &CV_IMAGE_ELEM(ret->patch, float, y, 0);
			for (unsigned x = 0; x < (unsigned)ret->patch->width; x++) {
				mean += p[x] = v[n++];
			}
		}
		if ( normalize ) {
			mean /= float(ret->patch->width * ret->patch->height);
			for ( int r=0; r<int(ret->patch->height); ++r ) {
				for ( int c=0; c<int(ret->patch->width); ++c ) {
					CV_IMAGE_ELEM(ret->patch, float, r, c) -= mean;
				}
			}
		}
	}

	return ret;
}

// Ready dictionary entries out of an XML tree.
bool svlFinalPatchDictionary::read(XMLNode &root, bool normalize)
{
	clear();
	if (root.isEmpty()) {
		cout << "Patch dictionary file is missing or has incorrect root.\n";
		return false;
	}
	if (string(root.getName()) != "PatchDictionary" ) {
		cout << "Attempt to read patch dictionary from XMLNode that is not a patch dictionary.\n";
		return false;
	}
	// Read window size.
	_windowSize.width = atoi(root.getAttribute("width"));
	_windowSize.height = atoi(root.getAttribute("height"));
	int n = atoi(root.getAttribute("numEntries"));

	SVL_ASSERT(root.nChildNode("PatchDefinition") == n);

	// Read in patches.
	for ( int i=0; i<n; ++i ) {
		XMLNode node = root.getChildNode("PatchDefinition", i);
		const char *t = node.getAttribute("type");
		if ( !strcmp(t, "intensity") ) {
			//_entries->push_back(readIntensityPatch(node));
			_entries->push_back(readPatch(node, normalize));
		} else if (!strcmp(t, "depth")) {
			//_entries->push_back(readDepthPatch(node));
			_entries->push_back(readPatch(node, normalize));
		} else if (!strcmp(t, "abs")) {
			cout << "svlFinalPatchDictionary::read: Tried to read an abs patch.\n"; //assert(false);
		} else {
			cout << "Unknown patch type " << t << endl;
		}
	}

	// Compute min/max patch width/height.
	min_width = _entries->front()->patch->width;
	max_width = _entries->front()->patch->width;
	min_height = _entries->front()->patch->height;
	max_height = _entries->front()->patch->height;
	for ( vector<svlPatch*>::const_iterator it=_entries->begin(); it!=_entries->end(); ++it ) {
		min_width = min(min_width, (*it)->patch->width);
		max_width = max(max_width, (*it)->patch->width);
		min_height = min(min_height, (*it)->patch->height);
		max_height = max(max_height, (*it)->patch->height);
	}
	//printf("min/max, w/h: %d,%d %d,%d\n", min_width, max_width, min_height, max_height);

	return true;
}

// Print all patches.
void svlFinalPatchDictionary::print()
{
	int i = 0;
	for ( vector<svlPatch*>::const_iterator it=_entries->begin(); it!=_entries->end(); ++it ) {
		printf("Patch %d:\n", i++);
		(*it)->print();
	}
	printf("Dictionary window size (w,h): %d,%d\n", _windowSize.width, _windowSize.height);
}

// Print first n patches.
void svlFinalPatchDictionary::print(int n)
{
	for ( int i=0; i<n; ++i ) {
		printf("Patch %d:\n", i);
		(*_entries)[i]->print();
	}
}

// Print [n,m) patches.
void svlFinalPatchDictionary::print(int n, int m)
{
	for ( int i=n; i<m; ++i ) {
		printf("Patch %d:\n", i);
		(*_entries)[i]->print();
	}
}

// Decimate
void svlFinalPatchDictionary::decimate(const vector<int> &ix)
{
	vector<int>::const_iterator i = ix.begin();
	int j = 0;
	for ( vector<svlPatch*>::iterator it=_entries->begin(); it!=_entries->end(); ) {
		if ( j == *i ) {
			++it; // Keep.
			++i; // Advance to next ix-to-keep number.
			if ( i == ix.end() ) {
				// Free all remaining patches since we're out of indices to keep.
				for ( vector<svlPatch*>::iterator it2=it; it2!=_entries->end(); )
					it2 = _entries->erase(it2);
				break;
			}
		} else {
			it = _entries->erase(it); // Remove.
		}
		++j;
	}

	// Recompute min/max patch width/height.
	min_width = _entries->front()->patch->width;
	max_width = _entries->front()->patch->width;
	min_height = _entries->front()->patch->height;
	max_height = _entries->front()->patch->height;
	for ( vector<svlPatch*>::const_iterator it=_entries->begin(); it!=_entries->end(); ++it ) {
		min_width = min(min_width, (*it)->patch->width);
		max_width = max(max_width, (*it)->patch->width);
		min_height = min(min_height, (*it)->patch->height);
		max_height = max(max_height, (*it)->patch->height);
	}
	//printf("min/max, w/h: %d,%d %d,%d\n", min_width, max_width, min_height, max_height);
}


