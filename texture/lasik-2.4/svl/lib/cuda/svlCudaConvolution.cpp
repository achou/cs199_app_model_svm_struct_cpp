/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaConvolution.cpp
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  Handles NVIDIA Cuda memory interface.
**
*****************************************************************************/

#include "svlBase.h"
#include "svlCudaConvolution.h"


svlCudaConvolution::svlCudaConvolution() {}

svlCudaConvolution::~svlCudaConvolution() {}

// Pad in width.
pair<int,int> svlCudaConvolution::halfPad(const float *patch, int pw, int ph, float* &pp, int sub_w, int sub_h, int w_off, int h_off)
{
	if ( sub_w == -1 ) sub_w = pw;
	if ( sub_h == -1 ) sub_h = ph;
	// Bin patch into a proper size and then convert to linear memory.
	int patch_w = -1, patch_h = sub_h;
	if ( sub_w <= 4 ) {
		patch_w = 4;
	} else if ( sub_w <= 8 ) {
		patch_w = 8;
	} else if ( sub_w <= 16 ) {
		patch_w = 16;
	} else {
		assert(false);
	}

	// Construct patch padded to above size.
	pp = (float*)malloc(sizeof(float)*patch_w*patch_h);
	float *ptr = pp;
	int r, c;
	// Do on-patch rows
	for ( r=0; r<patch_h; ++r ) {
		// Do on-patch columns
		for ( c=0; c<min(patch_w,pw-w_off); ++c ) {
			*ptr++ =  patch[ pw*( r + h_off ) + c + w_off ];
		}
		// Zeralize remainder columns
		for ( ; c<patch_w; ++c ) {
			*ptr++ = 0.0f;
		}
	}

	return pair<int,int>(patch_w,patch_h);
}
// The above with IplImage.
pair<int,int> svlCudaConvolution::halfPad(const IplImage *patch, float* &pp, int w_off, int h_off)
{
	// Bin patch into a proper size and then convert to linear memory.
	int patch_w = -1, patch_h = patch->height - h_off;
	if ( patch->width - w_off <= 4 ) {
		patch_w = 4;
	} else if ( patch->width - w_off <= 8 ) {
		patch_w = 8;
	} else if ( patch->width - w_off  <= 16 ) {
		patch_w = 16;
	} else {
		assert(false);
	}

	// Construct patch padded to above size.
	pp = (float*)malloc(sizeof(float)*patch_w*patch_h);
	float *ptr = pp;
	int r, c;
	// Do on-patch rows
	for ( r=0; r<patch->height; ++r ) {
		// Do on-patch columns
		for ( c=0; c<patch->width; ++c ) {
			*ptr++ =  CV_IMAGE_ELEM(patch, float, r + h_off, c + w_off);
		}
		// Zeralize remainder columns
		for ( ; c<patch_w; ++c ) {
			*ptr++ = 0.0f;
		}
	}
	// Zeralize remainder rows
	for ( ; r<patch_h; ++r ) {
		for ( c=0; c<patch_w; ++c ) {
			*ptr++ = 0.0f;
		}
	}

	return pair<int,int>(patch_w,patch_h);
}
// Pad in width to the left.
pair<int,int> svlCudaConvolution::halfPadLeft(const float *patch, int pw, int ph, float* &pp)
{
	// Bin patch into a proper size and then convert to linear memory.
	int patch_w = -1, patch_h = ph;
	if ( pw <= 4 ) {
		patch_w = 4;
	} else if ( pw <= 8 ) {
		patch_w = 8;
	} else if ( pw  <= 16 ) {
		patch_w = 16;
	} else {
		assert(false);
	}
	int buf = patch_w - pw;

	// Construct patch padded to above size.
	pp = (float*)malloc(sizeof(float)*patch_w*patch_h);
	float *ptr = pp;
	int r, c;
	// Do on-patch rows
	for ( r=0; r<ph; ++r ) {
		// Buffer column.
		for ( c=0; c<buf; ++c )
			*ptr++ = 0.0f;
		for ( c=buf; c<patch_w; ++c )
			*ptr++ =  patch[ pw*r + c - buf ];
	}
	// Zeralize remainder rows
	for ( ; r<patch_h; ++r )
		for ( c=0; c<patch_w; ++c )
			*ptr++ = 0.0f;

	return pair<int,int>(patch_w,patch_h);
}
// The above with an IplImage.
pair<int,int> svlCudaConvolution::halfPadLeft(const IplImage *patch, float* &pp)
{
	// Bin patch into a proper size and then convert to linear memory.
	int patch_w = -1, patch_h = patch->height;
	if ( patch->width <= 4 ) {
		patch_w = 4;
	} else if ( patch->width <= 8 ) {
		patch_w = 8;
	} else if ( patch->width  <= 16 ) {
		patch_w = 16;
	} else {
		assert(false);
	}
	int buf = patch_w - patch->width;

	// Construct patch padded to above size.
	pp = (float*)malloc(sizeof(float)*patch_w*patch_h);
	float *ptr = pp;
	int r, c;
	// Do on-patch rows
	for ( r=0; r<patch->height; ++r ) {
		// Buffer column.
		for ( c=0; c<buf; ++c )
			*ptr++ = 0.0f;
		for ( c=buf; c<patch_w; ++c )
			*ptr++ = CV_IMAGE_ELEM(patch, float, r, c-buf);
	}
	// Zeralize remainder rows
	for ( ; r<patch_h; ++r )
		for ( c=0; c<patch_w; ++c )
			*ptr++ = 0.0f;

	return pair<int,int>(patch_w,patch_h);
}

// Pad a super-16-by-16, sub-32-by-32 patch into four patches.
void svlCudaConvolution::halfPad32(const float *patch, int pw, int ph, vector<PaddedPatch> &pps)
{
	pps.clear();
	float *pp;
	int w, h;
	pair<int,int> wh;

	if ( pw <= 16 ) { // Null second and fourth patches.
		wh = halfPad(patch, pw, ph, pp,  pw, h = min(16,ph), 0, 0);
		pps.push_back(PaddedPatch(pp, wh.first, wh.second, pw, h));
		pps.push_back(PaddedPatch()); // Second patch.
		if ( h < ph ) {
			wh = halfPad(patch, pw, ph, pp,  pw, ph-h, 0, h);
			pps.push_back(PaddedPatch(pp, wh.first, wh.second, pw, ph-h));
		}
		pps.push_back(PaddedPatch()); // Fourth patch.
	} else if ( ph <= 16 ) { // Null third and fourth patches.
		wh = halfPad(patch, pw, ph, pp,  w = min(16,pw), ph, 0, 0);
		pps.push_back(PaddedPatch(pp, wh.first, wh.second, pw, ph));
		if ( w < pw ) {
			wh = halfPad(patch, pw, ph, pp,  pw-w, ph, w, 0);
			pps.push_back(PaddedPatch(pp, wh.first, wh.second, pw-w, ph));
		}
		pps.push_back(PaddedPatch()); // Third patch.
		pps.push_back(PaddedPatch()); // Fourth patch.
	} else { // All of them.
		// First +0,+0
		wh = halfPad(patch, pw, ph, pp,  w = 16, h = 16, 0, 0);
		pps.push_back(PaddedPatch(pp, wh.first, wh.second, w, h));
		// Second +w,+0
		wh = halfPad(patch, pw, ph, pp,  pw-w, h, w, 0);
		pps.push_back(PaddedPatch(pp, wh.first, wh.second, pw-w, h));
		// Third +0,+h
		wh = halfPad(patch, pw, ph, pp,  w, ph-h, 0, h);
		pps.push_back(PaddedPatch(pp, wh.first, wh.second, w, ph-h));
		// Fourth +w,+h
		wh = halfPad(patch, pw, ph, pp,  pw-w, ph-h, w, h);
		pps.push_back(PaddedPatch(pp, wh.first, wh.second, pw-w, ph-h));
	}

	if ( pw == 32 && ph == 32 ) {
		// Coalesce patches into a 4k line to push onto end of array.
		pp = (float*)malloc(sizeof(float)*1024);
		memset(pp, 0, sizeof(float)*1024);
		for ( unsigned i=0; i<4; ++i )
			if ( pps[i].data )
				memcpy(pp + 256*i, pps[i].data, sizeof(float)*256);
		pps.push_back(PaddedPatch(pp, 32, 32, 32, 32));
	}

#if 0
	// Output patches.
	char c[32];
	for ( unsigned i=0; i<4; ++i ) {
		if ( pps[i].data ) {
			sprintf(c,"p%d.out",i);
			FILE *fo = fopen(c,"wb");
			fwrite(&pps[i].w,sizeof(int),1,fo);
			fwrite(&pps[i].h,sizeof(int),1,fo);
			fwrite(pps[i].data,sizeof(float),pps[i].w*pps[i].h,fo);
			fclose(fo);
		}
	}
	FILE *fo = fopen("p4.out","wb");
	fwrite(&pps.back().w,sizeof(int),1,fo);
	fwrite(&pps.back().h,sizeof(int),1,fo);
	fwrite(pps.back().data,sizeof(float),pps.back().w*pps.back().h,fo);
	fclose(fo);
#endif
}


// Convolve a pitch with a patch described as an IplImage and output to another pitch.
//void svlCudaConvolution::ccoeffNormed(const svlCudaPitch *pitch_in, const IplImage *patch, svlCudaPitch *pitch_out,
//								   int img_w, int img_h, float patch_mean, float patch_std, svlCudaCoveringGrid *cudagrid)
//{
//	float *pp, *pmask;
//	pair<int,int> wh = fullPad(patch, pp, pmask);
//	int patch_w = wh.first, patch_h = wh.second;
//	// Run convolution.
//	svlCudaCcoeffNormed2d(*pitch_in, *pitch_out, pp, pmask, patch_w, patch_h, patch->height*patch->width, patch_mean, patch_std, img_w, img_h, cudagrid);
//	free(pp);
//	free(pmask);
//}

// Get padded patch size.
pair<int,int> svlCudaConvolution::paddedPatchSize(const IplImage *patch)
{
	pair<int,int> ret;
	ret.second = min(16, patch->height);
	if ( patch->width <= 4 ) {
		ret.first = 4;
	} else if ( patch->width <= 8 ) {
		ret.first = 8;
	} else if ( patch->width <= 16 ) {
		ret.first = 16;
	} else {
		assert(false);
	}
	return ret;
}

// Pad the patch to device convolution size and store it and its mask in the provided memory.
void svlCudaConvolution::padPatchWithMask(const IplImage *patch, float *mem)
{
	// Bin patch into a proper size and then convert to linear memory.
	pair<int,int> pad_wh = paddedPatchSize(patch);
	int sz = pad_wh.first * pad_wh.second;

	// Construct patch padded to above size.
	float *pdata = (float*)malloc(sizeof(float)*sz*2);
	float *ptr = pdata, *ptrm = pdata + sz;
	int r, c;
	// Do on-patch rows
	for ( r=0; r<patch->height; ++r ) {
		// Do on-patch columns
		for ( c=0; c<patch->width; ++c ) {
			if ( patch->depth == IPL_DEPTH_8U ) {
				*ptr++ = (float)CV_IMAGE_ELEM(patch, unsigned char, r, c);
			} else if ( patch->depth == IPL_DEPTH_32F ) {
				*ptr++ = CV_IMAGE_ELEM(patch, float, r, c);
			}
			*ptrm++ = 1.0f;
		}
		// Zeralize remainder columns
		for ( ; c<pad_wh.first; ++c ) {
			*ptr++ = 0.0f;
			*ptrm++ = 0.0f;
		}
	}
	// Zeralize remainder rows
	for ( ; r<pad_wh.second; ++r ) {
		for ( c=0; c<pad_wh.first; ++c ) {
			*ptr++ = 0.0f;
			*ptrm++ = 0.0f;
		}
	}
	memcpy(mem, pdata, sizeof(float)*sz*2);
	free(pdata);
}

// Pad the patch to device convolution size and store it in the provided memory.
void svlCudaConvolution::padPatchOnly(const IplImage *patch, float *mem)
{
	// Bin patch into a proper size and then convert to linear memory.
	pair<int,int> pad_wh = paddedPatchSize(patch);
	int sz = pad_wh.first * pad_wh.second;

	// Construct patch padded to above size.
	float *pdata = (float*)malloc(sizeof(float)*sz);
	float *ptr = pdata;
	int r, c;
	// Do on-patch rows
	for ( r=0; r<pad_wh.second; ++r ) {
		// Do on-patch columns
		for ( c=0; c<patch->width; ++c ) {
			if ( patch->depth == IPL_DEPTH_8U ) {
				*ptr++ = (float)CV_IMAGE_ELEM(patch, unsigned char, r, c);
			} else if ( patch->depth == IPL_DEPTH_32F ) {
				*ptr++ = CV_IMAGE_ELEM(patch, float, r, c);
			}
		}
		// Zeralize remainder columns
		for ( ; c<pad_wh.first; ++c )
			*ptr++ = 0.0f;
	}
	// Zeralize remainder rows
	for ( ; r<pad_wh.second; ++r )
		for ( c=0; c<pad_wh.first; ++c )
			*ptr++ = 0.0f;
	
	memcpy(mem, pdata, sizeof(float)*sz);
	free(pdata);
}

// Performs CUDA_TM_CCOEFF_NORMED with direct summation over the image to compute the image statistics.
void svlCudaConvolution::ccoeffNormed(const svlCudaPitch *pin, const float *patch, int pw, int ph, svlCudaPitch *pout, int img_w, int img_h, int size)
{
	float *pp;
	pair<int,int> wh = halfPad(patch, pw, ph, pp);
	const int patch_w = wh.first, patch_h = wh.second, N = pw*ph;

	// Create mask.
	int psz = patch_w*patch_h;
	float *pmask = (float*)malloc(sizeof(float)*psz);
	memset(pmask, 0, sizeof(float)*psz);
	for ( int r=0; r<ph; ++r )
		for ( int c=0; c<pw; ++c )
			pmask[ r*patch_w + c ] = 1.0f;

	// Compute mean and standard deviation.
	float mean = 0.0f, std = 0.0f;
	for ( int i=0; i<psz; ++i ) {
		mean += pp[i];
		std += pp[i] * pp[i];
	}
	mean /= float(N);
	std = sqrtf( std - float(N)*mean*mean );

	// Run convolution.
	switch ( size ) {
		case CUDA_TM_SAME:
			svlCudaCcoeffNormed2d_v2(pin, pout, pp, pmask, patch_w, patch_h, N, mean, std, img_w, img_h);
			break;
		case CUDA_TM_VALID:
			if ( img_w == -1 ) img_w = pin->w;
			if ( img_h == -1 ) img_h = pin->h;
			svlCudaCcoeffNormed2d_v2(pin, pout, pp, pmask, patch_w, patch_h, N, mean, std, img_w, img_h, img_w - pw + 1, img_h - ph + 1);
			break;
	}
	free(pp);
	free(pmask);
}

// The above with IplImage input.
void svlCudaConvolution::ccoeffNormed(const svlCudaPitch *pin, const IplImage *patch, svlCudaPitch *pout, int img_w, int img_h, int size)
{
	float *pp;
	pair<int,int> wh = halfPad(patch, pp);
	const int patch_w = wh.first, patch_h = wh.second, N = patch->width*patch->height;

	// Create mask.
	int psz = patch_w*patch_h;
	float *pmask = (float*)malloc(sizeof(float)*psz);
	memset(pmask, 0, sizeof(float)*psz);
	for ( int r=0; r<patch->height; ++r )
		for ( int c=0; c<patch->width; ++c )
			pmask[ r*patch_w + c ] = 1.0f;

	// Compute mean and standard deviation.
	float mean = 0.0f, std = 0.0f;
	for ( int i=0; i<psz; ++i ) {
		mean += pp[i];
		std += pp[i] * pp[i];
	}
	mean /= float(N);
	std = sqrtf( std - float(N)*mean*mean );

	// Run convolution.
	switch ( size ) {
		case CUDA_TM_SAME:
			svlCudaCcoeffNormed2d_v2(pin, pout, pp, pmask, patch_w, patch_h, N, mean, std, img_w, img_h);
			break;
		case CUDA_TM_VALID:
			if ( img_w == -1 ) img_w = pin->w;
			if ( img_h == -1 ) img_h = pin->h;
			svlCudaCcoeffNormed2d_v2(pin, pout, pp, pmask, patch_w, patch_h, N, mean, std, img_w, img_h, img_w - patch->width + 1, img_h - patch->height + 1);
			break;
	}
	free(pp);
	free(pmask);
}

// *** Max 16-by-16 template versions ****
void svlCudaConvolution::cudaMatchTemplate_16(const svlCudaPitch *pin, const float *patch, int pw, int ph, svlCudaPitch *pout, int type, int size, int img_w, int img_h)
{
	float *pp;
	pair<int,int> wh;
	if ( img_w == -1 ) img_w = pin->w;
	if ( img_h == -1 ) img_h = pin->h;
	switch ( type ) {
		case CUDA_TM_CCORR:
			switch ( size ) {
				case CUDA_TM_SAME:
					wh = halfPad(patch, pw, ph, pp);
					svlCudaPlainConv2d_16(pin, pout, pp, wh.first, wh.second, img_w, img_h);
					free(pp);
					break;
				case CUDA_TM_VALID:
					wh = halfPad(patch, pw, ph, pp);
					svlCudaPlainConv2d_16(pin, pout, pp, wh.first, wh.second, img_w, img_h, img_w - pw + 1, img_h - ph + 1);
					free(pp);
					break;
				case CUDA_TM_FULL:
					wh = halfPadLeft(patch, pw, ph, pp);
					svlCudaPlainConv2d_full(pin, pout, pp, wh.first, wh.second, img_w, img_h);
					free(pp);
					break;
			}
			break;
		case CUDA_TM_CCOEFF_NORMED:
			ccoeffNormed(pin, patch, pw, ph, pout, img_w, img_h, size);
			break;
		default:
			SVL_LOG(SVL_LOG_FATAL, "Invalid type: " << type);
	}
}
// The above but accepting an IplImage for the patch.
void svlCudaConvolution::cudaMatchTemplate_16(const svlCudaPitch *pin, const IplImage *patch, svlCudaPitch *pout, int type, int size, int img_w, int img_h)
{
	svlCudaTimer::push("cudaMatchTemplate");
	float *pp;
	pair<int,int> wh;
	if ( img_w == -1 ) img_w = pin->w;
	if ( img_h == -1 ) img_h = pin->h;
	switch ( type ) {
		case CUDA_TM_CCORR:
			switch ( size ) {
				case CUDA_TM_SAME:
					wh = halfPad(patch, pp);
					svlCudaPlainConv2d_16(pin, pout, pp, wh.first, wh.second, img_w, img_h);
					free(pp);
					break;
				case CUDA_TM_VALID:
					wh = halfPad(patch, pp);
					svlCudaPlainConv2d_16(pin, pout, pp, wh.first, wh.second, img_w, img_h, img_w - patch->width + 1, img_h - patch->height + 1);
					free(pp);
					break;
				case CUDA_TM_FULL:
					wh = halfPadLeft(patch, pp);
					svlCudaPlainConv2d_full(pin, pout, pp, wh.first, wh.second, img_w, img_h);
					free(pp);
					break;
			}
			break;
		case CUDA_TM_CCOEFF_NORMED:
			ccoeffNormed(pin, patch, pout, img_w, img_h, size);
			break;
		default:
			SVL_LOG(SVL_LOG_FATAL, "Invalid type: " << type);
	}
	svlCudaTimer::pop();
}


// *** Max 32-by-32-sized template versions ***
void svlCudaConvolution::cudaMatchTemplate_32(const svlCudaPitch *pin, const float *patch, int pw, int ph, svlCudaPitch *pout, svlCudaPitch *temp,
											  int type, int size, int img_w, int img_h)
{
	SVL_ASSERT_MSG(temp, "Need a temporary pitch for >16-by-16 patch sizes");
	SVL_ASSERT_MSG(temp->w >= pin->w && temp->h >= pin->h, "Temporary pitch must have at least input image size");
	vector<PaddedPatch> pps;
	if ( img_w == -1 ) img_w = pin->w;
	if ( img_h == -1 ) img_h = pin->h;
	switch ( type ) {
		case CUDA_TM_CCORR:
			switch ( size ) {
				case CUDA_TM_SAME:
					halfPad32(patch, pw, ph, pps);
					svlCudaPlainConv2d_32(pin, pout, temp, pps, img_w, img_h);
					break;
				case CUDA_TM_VALID:
					halfPad32(patch, pw, ph, pps);
					svlCudaPlainConv2d_32(pin, pout, temp, pps, img_w, img_h, img_w - pw + 1, img_h - ph + 1);
					break;
				case CUDA_TM_FULL:
					SVL_LOG(SVL_LOG_FATAL, "Not implemented");
			}
			break;
		default:
			SVL_LOG(SVL_LOG_FATAL, "Invalid type: " << type);
	}
	// Free patches.
	for ( vector<PaddedPatch>::iterator it=pps.begin(); it!=pps.end(); ++it )
		if ( it->data )
			free(it->data);
}


// Convolve a pitch with a patch described as an IplImage and output to another pitch.
void svlCudaConvolution::cudaMatchTemplate(const svlCudaPitch *pin, const float *patch, int pw, int ph, svlCudaPitch *pout, int type, int size,
										   int img_w, int img_h, svlCudaPitch *temp)
{
	svlCudaTimer::push("cudaMatchTemplate");
	int max_size = max(pw,ph);
	if ( max_size <= 16 )
		cudaMatchTemplate_16(pin, patch, pw, ph, pout, type, size, img_w, img_h);
	else if ( max_size <= 32 )
		cudaMatchTemplate_32(pin, patch, pw, ph, pout, temp, type, size, img_w, img_h);
	else
		SVL_LOG(SVL_LOG_FATAL, "Over-sized template");
	svlCudaTimer::pop();
}
// Convolve a pitch with a patch described as an IplImage and output to another pitch.
void svlCudaConvolution::cudaMatchTemplate(const svlCudaPitch *pin, const IplImage *patch, svlCudaPitch *pout, int type, int size,
										   int img_w, int img_h, svlCudaPitch *temp)
{
	svlCudaTimer::push("cudaMatchTemplate");
	int max_size = max(patch->width,patch->height);
	if ( max_size <= 16 ) {
		cudaMatchTemplate_16(pin, patch, pout, type, size, img_w, img_h);
	} else if ( max_size <= 32 ) {
		SVL_LOG(SVL_LOG_FATAL, "Not implemented");
	} else {
		SVL_LOG(SVL_LOG_FATAL, "Over-sized template");
	}
	svlCudaTimer::pop();
}

// 3d pitch version of above.
void svlCudaConvolution::cudaMatchTemplate(const svlCudaPitch3d *pin, const float *patch, int pw, int ph, svlCudaPitch3d *pout, int type, int size, int img_w, int img_h)
{
	svlCudaTimer::push("cudaMatchTemplate");
	for ( int i=0; i<pin->z; ++i ) {
		svlCudaPitch pin2 = pin->getPitch(i);
		svlCudaPitch pout2 = pout->getPitch(i);
		svlCudaConvolution::cudaMatchTemplate(&pin2, patch, pw, ph, &pout2, type, size, img_w, img_h);
	}
	svlCudaTimer::pop();
}
// And with IplImages:
void svlCudaConvolution::cudaMatchTemplate(const svlCudaPitch3d *pin, const IplImage *patch, svlCudaPitch3d *pout, int type, int size, int img_w, int img_h)
{
	svlCudaTimer::push("cudaMatchTemplate");
	for ( int i=0; i<pin->z; ++i ) {
		svlCudaPitch pin2 = pin->getPitch(i);
		svlCudaPitch pout2 = pout->getPitch(i);
		svlCudaConvolution::cudaMatchTemplate(&pin2, patch, &pout2, type, size, img_w, img_h);
	}
	svlCudaTimer::pop();
}



// The above, but use v3.
// Performs CV_TM_CCOEFF_NORMED using row-integral images.
//void svlCudaConvolution::ccoeffNormed(const svlCudaPitch *pin, const IplImage *patch, svlCudaPitch *pout, svlCudaPitch *integ, svlCudaPitch *integ2, int img_w, int img_h)
//{
//	float *pp, *pmask;
//	pair<int,int> wh = halfPad(patch, pp, pmask);
//	int patch_w = wh.first, patch_h = wh.second;
//
//	// Run convolution.
//	svlCudaCcoeffNormed2d_v3(pin, pout, pp, pmask, integ, integ2, patch_w, patch_h, patch->height*patch->width, patch_mean, patch_std, img_w, img_h);
//	free(pp);
//	free(pmask);
//}

// Convolve a pitch with a patch described as an IplImage and output to another pitch.
//void svlCudaConvolution::normCCorr2(const svlCudaPitch *pitch_in, const IplImage *patch,
//									svlCudaPitch *pitch_out, svlCudaPitch *temp1, svlCudaPitch *temp2,
//									int img_w, int img_h, float patch_mean, float patch_std)
//{
//	float *pp, *pmask;
//	pair<int,int> wh = fullPad(patch, pp, pmask);
//	int patch_w = wh.first, patch_h = wh.second;
//
//	// Run convolution.
//	svlCudaCcoeffNormed2d_twostep(*pitch_in, *pitch_out, *temp1, *temp2,
//		pp, pmask, patch_w, patch_h, patch->height*patch->width, patch_mean, patch_std, img_w, img_h);
//	free(pp);
//	free(pmask);
//}

// Convolution between pitches.
void svlCudaConvolution::cudaPitchConv(const svlCudaPitch *pin, const svlCudaPitch *patch, svlCudaPitch *pout, const svlCudaPitch *orig_patch,
									   int img_w, int img_h, int out_w, int out_h)
{
	SVL_ASSERT_MSG(patch->w <= 128, "Patch is too large");
	SVL_ASSERT_MSG(patch->w%16 == 0, "Patch not properly padded");

	if ( img_w == -1 ) img_w = pin->w;
	if ( img_h == -1 ) img_h = pin->h;
	if ( orig_patch ) {
		// Use original patch size to convolve.
		if ( out_w == -1 ) out_w = img_w - orig_patch->w + 1;
		if ( out_h == -1 ) out_h = img_h - orig_patch->h + 1;
	} else {
		if ( out_w == -1 ) out_w = img_w - patch->w + 1;
		if ( out_h == -1 ) out_h = img_h - patch->h + 1;
	}
	//printf("$> %d,%d %d,%d\n", img_w, img_h, out_w, out_h);
	svlCudaTimer::push("cudaPitchConv");
	svlCudaSuperSizeConv(pin, patch, pout, img_w, img_h, out_w, out_h);
	svlCudaTimer::pop();
}


