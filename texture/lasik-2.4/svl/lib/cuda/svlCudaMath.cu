/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2009, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaMath.cu
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  Cuda math functions.
**
*****************************************************************************/

#include "svlCudaCommon.h"
#include "svlCudaMemHandler.h"
#include <limits>

inline bool sizesMatch(svlCudaCoveringGrid *cudagrid, dim3 &grid) {
	return cudagrid->grid_x == grid.x
		&& cudagrid->grid_y == grid.y;
}

// Device memory.
//__device__ __constant__ float covering_grid[ 1536 ]; // For holding bits on whether to operation on this block or not.
__device__ __constant__ float covering_grid2[ 1536 ]; // For holding bits on whether to operation on this block or not.


// Two-pitch primitive arithmetic functions.

// Perform <a += fac*b;>.
__global__ void add_slice(float *a, int a_pitch, const float *b, int b_pitch, float fac, int data_w, int data_h)
{
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.
	if ( x >= data_w || y >= data_h )
		return;
	
	float *a_out = PITCH_GET(a, a_pitch, y);
	const float *b_out = PITCH_GET(b, b_pitch, y);
	a_out[x] += fac * b_out[x];
}
void svlCudaAdd(svlCudaPitch *a, const svlCudaPitch *b, float fac, int img_w, int img_h)
{
	if ( img_w == -1 ) img_w = a->w;
	if ( img_h == -1 ) img_h = a->h;
	dim3 block(16, 16);
	dim3 grid( iDivUp(img_w, block.x), iDivUp(img_h, block.y) );
	add_slice<<< grid, block >>>(a->data, a->pitch, b->data, b->pitch, fac, img_w, img_h);
}

// Perform above with b offsets: <a += fac*b[+w,+h];>.
__global__ void add_off_slice(float *a, int a_pitch, const float *b, int b_pitch, float fac, int a_w, int a_h, int b_w, int b_h, int off_x, int off_y)
{
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.
	if ( x >= a_w || y >= a_h // Off of bounds of a
		|| x+off_x < 0 || x+off_x >= b_w // Off width bounds of b
		|| y+off_y < 0 || y+off_y >= b_h // Off height bounds of b
		)
		return;
	
	float *a_out = PITCH_GET(a, a_pitch, y);
	const float *b_out = PITCH_GET(b, b_pitch, y + off_y);
	a_out[x] += fac * b_out[ x + off_x ];
}
void svlCudaAddOff(svlCudaPitch *a, const svlCudaPitch *b, float fac, int img_w, int img_h, int off_x, int off_y)
{
	if ( img_w == -1 ) img_w = a->w;
	if ( img_h == -1 ) img_h = a->h;
	dim3 block(16, 16);
	dim3 grid( iDivUp(img_w, block.x), iDivUp(img_h, block.y) );
	add_off_slice<<< grid, block >>>(a->data, a->pitch, b->data, b->pitch, fac, img_w, img_h, b->w, b->h, off_x, off_y);
}

// Perform <a *= fac*b;>.
__global__ void mult_slice(float *a, int a_pitch, const float *b, int b_pitch, float fac, int data_w, int data_h)
{
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.
	if ( x >= data_w || y >= data_h )
		return;
	
	float *a_out = PITCH_GET(a, a_pitch, y);
	const float *b_out = PITCH_GET(b, b_pitch, y);
	a_out[x] *= fac * b_out[x];
}
void svlCudaMult(svlCudaPitch *a, const svlCudaPitch *b, float fac, int img_w, int img_h)
{
	if ( img_w == -1 ) img_w = a->w;
	if ( img_h == -1 ) img_h = a->h;
	dim3 block(16, 16);
	dim3 grid( iDivUp(img_w, block.x), iDivUp(img_h, block.y) );
	mult_slice<<< grid, block >>>(a->data, a->pitch, b->data, b->pitch, fac, img_w, img_h);
}

// Perform <a *= fac/b;>.
__global__ void div_slice(float *a, int a_pitch, const float *b, int b_pitch, float fac, int data_w, int data_h)
{
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.
	if ( x >= data_w || y >= data_h )
		return;
	
	float *a_out = PITCH_GET(a, a_pitch, y);
	const float *b_out = PITCH_GET(b, b_pitch, y);
	a_out[x] *= __fdividef( fac, b_out[x] );
}
void svlCudaDiv(svlCudaPitch *a, const svlCudaPitch *b, float fac, int img_w, int img_h)
{
	if ( img_w == -1 ) img_w = a->w;
	if ( img_h == -1 ) img_h = a->h;
	dim3 block(16, 16);
	dim3 grid( iDivUp(img_w, block.x), iDivUp(img_h, block.y) );
	div_slice<<< grid, block >>>(a->data, a->pitch, b->data, b->pitch, fac, img_w, img_h);
}


// Three-pitch primitive arithmetic functions.
// Perform <out = fac_a*a + fac_b*b;>.
__global__ void add3_slice(float *dout, int dpitch, const float *a, int apitch, const float *b, int bpitch, float fac_a, float fac_b, int data_w, int data_h)
{
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.
	if ( x >= data_w || y >= data_h )
		return;
	
	const float *af = PITCH_GET(a, apitch, y);
	const float *bf = PITCH_GET(b, bpitch, y);
	float *doutf = PITCH_GET(dout, dpitch, y);
	doutf[x] = fac_a*af[x] + fac_b*bf[x];
}
void svlCudaAdd3(svlCudaPitch *out, const svlCudaPitch *a, const svlCudaPitch *b, float fac_a, float fac_b, int img_w, int img_h)
{
	if ( img_w == -1 ) img_w = out->w;
	if ( img_h == -1 ) img_h = out->h;
	dim3 block(16, 16);
	dim3 grid( iDivUp(img_w, block.x), iDivUp(img_h, block.y) );
	add3_slice<<< grid, block >>>(out->data, out->pitch, a->data, a->pitch, b->data, b->pitch, fac_a, fac_b, img_w, img_h);
}

// Perform <out = fac*a*b;>.
__global__ void mult3_slice(float *dout, int dpitch, const float *a, int apitch, const float *b, int bpitch, float fac, int data_w, int data_h)
{
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.
	if ( x >= data_w || y >= data_h )
		return;
	
	const float *af = PITCH_GET(a, apitch, y);
	const float *bf = PITCH_GET(b, bpitch, y);
	float *doutf = PITCH_GET(dout, dpitch, y);
	doutf[x] = fac * af[x] * bf[x];
}
void svlCudaMult3(svlCudaPitch *out, const svlCudaPitch *a, const svlCudaPitch *b, float fac, int img_w, int img_h)
{
	if ( img_w == -1 ) img_w = out->w;
	if ( img_h == -1 ) img_h = out->h;
	dim3 block(16, 16);
	dim3 grid( iDivUp(img_w, block.x), iDivUp(img_h, block.y) );
	mult3_slice<<< grid, block >>>(out->data, out->pitch, a->data, a->pitch, b->data, b->pitch, fac, img_w, img_h);
}

// Perform <out = fac*a/b;>.
__global__ void div3_slice(float *dout, int dpitch, const float *a, int apitch, const float *b, int bpitch, float fac, int data_w, int data_h)
{
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.
	if ( x >= data_w || y >= data_h )
		return;
	
	const float *af = PITCH_GET(a, apitch, y);
	const float *bf = PITCH_GET(b, bpitch, y);
	float *doutf = PITCH_GET(dout, dpitch, y);
	doutf[x] = fac * __fdividef(af[x], bf[x]);
}
void svlCudaDiv3(svlCudaPitch *out, const svlCudaPitch *a, const svlCudaPitch *b, float fac, int img_w, int img_h)
{
	if ( img_w == -1 ) img_w = out->w;
	if ( img_h == -1 ) img_h = out->h;
	dim3 block(16, 16);
	dim3 grid( iDivUp(img_w, block.x), iDivUp(img_h, block.y) );
	div3_slice<<< grid, block >>>(out->data, out->pitch, a->data, a->pitch, b->data, b->pitch, fac, img_w, img_h);
}


// Scale and offset a pitch: <a = scale*a + offset;>.
__global__ void scoff_slice(float *a, int apitch, float scale, float offset, int data_w, int data_h)
{
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.
	if ( x >= data_w || y >= data_h )
		return;
	
	float *af = PITCH_GET(a, apitch, y);
	af[x] = scale * af[x] + offset;
}
void svlCudaScaleOffset(svlCudaPitch *a, float scale, float offset, int img_w, int img_h)
{
	if ( img_w == -1 ) img_w = a->w;
	if ( img_h == -1 ) img_h = a->h;
	dim3 block(16, 16);
	dim3 grid( iDivUp(img_w, block.x), iDivUp(img_h, block.y) );
	scoff_slice<<< grid, block >>>(a->data, a->pitch, scale, offset, img_w, img_h);
}

// Decimate in x and y.
__global__ void decimate_slice(const float *in_data, int in_pitch, float *out_data, int out_pitch, int delta_x, int delta_y, int img_w, int img_h, int sub_w, int sub_h)
{
	const int out_x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int out_y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.
	if ( out_x < sub_w && out_y < sub_h ) {
		const int in_x  = IMUL(delta_x, out_x);
		const int in_y  = IMUL(delta_y, out_y);
		const float *fin = PITCH_GET(in_data, in_pitch, in_y);
		float *fout = PITCH_GET(out_data, out_pitch, out_y);
		fout[out_x] = fin[in_x];
	}
}

// Decimate by factors in x and y.
void svlCudaDecimate(const svlCudaPitch *a, svlCudaPitch *b, int delta_x, int delta_y, int img_w, int img_h)
{
	if ( img_w == -1 ) img_w = a->w;
	if ( img_h == -1 ) img_h = b->h;
	dim3 block(16, 16);
	int sub_w = img_w/delta_x, sub_h = img_h/delta_y;
	dim3 grid( iDivUp( sub_w, block.x), iDivUp( sub_h, block.y) );
	decimate_slice<<< grid, block >>>(a->data, a->pitch, b->data, b->pitch, delta_x, delta_y, img_w, img_h, sub_w, sub_h);
}

// Calculate max over an upper-left-centered window.
//template<int NUM_COLS, int NUM_ROWS>
__device__ void max_slice(float *din, int din_pitch, float *res, int res_pitch, int data_w, int data_h, int NUM_COLS, int NUM_ROWS)
{
	// Load din data into shared cache.
	__shared__ float data[ 1024 ];
	const int data_width = 32;
	const int data_size = IMUL(data_width, blockDim.y);
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.

	// Read in data.
	const int ix = IMUL(data_width, threadIdx.y) + threadIdx.x;		// Shared thread memory index.
	const float *my_din = PITCH_GET(din, din_pitch, y);				// Calculate my row of din.
	data[ix] = my_din[x];
	// Load rightwards data
	const bool right_on = x + blockDim.x < data_w;
	data[ ix + blockDim.x ] = right_on ? my_din[ x + blockDim.x ] : -FLT_MAX;
	// Load downwards data
	if ( threadIdx.y < NUM_ROWS ) {
		if ( y + blockDim.y < data_h ) {
			// Read in upper entries
			const float *din_down = PITCH_GET(din, din_pitch, y + blockDim.y);
			data[ ix + data_size ] = din_down[x];
			// Read in rightward entry.
			data[ ix + data_size + blockDim.x ] = right_on ? din_down[x + blockDim.x] : -FLT_MAX;
		} else {
			data[ ix + data_size ] = data[ ix + data_size + blockDim.x ] = -FLT_MAX;
		}
	}

	__syncthreads();

	// Compute my max result.
	float gf = -FLT_MAX;
#pragma unroll
	for ( int i=0; i<NUM_ROWS; ++i ) {
#pragma unroll
		for ( int j=0; j<NUM_COLS; ++j ) {
			gf = fmaxf(gf, data[ ix + IMUL(data_width, i) + j ]);
			/*if ( data[ ix + IMUL(data_width, i) + j ] > gf )
				gf = data[ ix + IMUL(data_width, i) + j ];*/
		}
	}

	// Calculate my row of the results.
	float *my_res = PITCH_GET(res, res_pitch, y);
	my_res[x] = gf;
}

// The above without a covering grid (no penalty for inlining a __device__ function).
__global__ void max_NoCG_slice(float *din, int din_pitch, float *res, int res_pitch, int data_w, int data_h, int NUM_COLS, int NUM_ROWS)
{
	max_slice(din, din_pitch, res, res_pitch, data_w, data_h, NUM_COLS, NUM_ROWS);
}
// The above with a covering grid.
__global__ void max_CG_slice(float *din, int din_pitch, float *res, int res_pitch, int data_w, int data_h, int NUM_COLS, int NUM_ROWS)
{
	if ( covering_grid2[ IMUL(blockIdx.y, gridDim.x) + blockIdx.x ] != 0.0f ) {
		max_slice(din, din_pitch, res, res_pitch, data_w, data_h, NUM_COLS, NUM_ROWS);
	}
}

// Calculate max over an upper-left-centered window.
template<int NUM_COLS, int NUM_ROWS>
__device__ void max_template_slice(float *din, int din_pitch, float *res, int res_pitch, int data_w, int data_h)
{
	// Load din data into shared cache.
	__shared__ float data[ 1024 ];
	const int data_width = 32;
	const int data_size = IMUL(data_width, blockDim.y);
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.

	// Read in data.
	const int ix = IMUL(data_width, threadIdx.y) + threadIdx.x;		// Shared thread memory index.
	const float *my_din = PITCH_GET(din, din_pitch, y);				// Calculate my row of din.
	data[ix] = my_din[x];
	// Load rightwards data
	const bool right_on = x + blockDim.x < data_w;
	data[ ix + blockDim.x ] = right_on ? my_din[ x + blockDim.x ] : -FLT_MAX;
	// Load downwards data
	if ( threadIdx.y < NUM_ROWS ) {
		if ( y + blockDim.y < data_h ) {
			// Read in upper entries
			const float *din_down = PITCH_GET(din, din_pitch, y + blockDim.y);
			data[ ix + data_size ] = din_down[x];
			// Read in rightward entry.
			data[ ix + data_size + blockDim.x ] = right_on ? din_down[x + blockDim.x] : -FLT_MAX;
		} else {
			data[ ix + data_size ] = data[ ix + data_size + blockDim.x ] = -FLT_MAX;
		}
	}

	__syncthreads();

	// Compute my max result.
	float gf = -FLT_MAX;
#pragma unroll
	for ( int i=0; i<NUM_ROWS; ++i ) {
#pragma unroll
		for ( int j=0; j<NUM_COLS; ++j ) {
			gf = fmaxf(gf, data[ ix + IMUL(data_width, i) + j ]);
			/*if ( data[ ix + IMUL(data_width, i) + j ] > gf )
				gf = data[ ix + IMUL(data_width, i) + j ];*/
		}
	}

	// Calculate my row of the results.
	float *my_res = PITCH_GET(res, res_pitch, y);
	my_res[x] = gf;
}

// The above without a covering grid (no penalty for inlining a __device__ function).
template<int NUM_COLS, int NUM_ROWS>
__global__ void max_template_NoCG_slice(float *din, int din_pitch, float *res, int res_pitch, int data_w, int data_h)
{
	max_template_slice<NUM_COLS, NUM_ROWS>(din, din_pitch, res, res_pitch, data_w, data_h);
}
// The above with a covering grid.
template<int NUM_COLS, int NUM_ROWS>
__global__ void max_template_CG_slice(float *din, int din_pitch, float *res, int res_pitch, int data_w, int data_h)
{
	if ( covering_grid2[ IMUL(blockIdx.y, gridDim.x) + blockIdx.x ] != 0.0f ) {
		max_template_slice<NUM_COLS, NUM_ROWS>(din, din_pitch, res, res_pitch, data_w, data_h);
	}
}

// Perform window-based max.
void svlCudaMax(svlCudaPitch &pitch_in, svlCudaPitch &pitch_out, int max_w, int max_h, int img_w, int img_h, svlCudaCoveringGrid *cudagrid)
{
	dim3 block(16, 16);
	if ( img_w == -1 ) img_w = pitch_in.w;
	if ( img_h == -1 ) img_h = pitch_in.h;
	dim3 grid( iDivUp(img_w, block.x), iDivUp(img_h, block.y) );

	// Verify that grids are of the same size.
	if ( cudagrid == NULL || !sizesMatch(cudagrid, grid) ) {
		if ( max_w == 7 && max_h == 7 ) {
			max_template_NoCG_slice <7,7> <<< grid, block >>>(pitch_in.data, pitch_in.pitch, pitch_out.data, pitch_out.pitch, img_w, img_h);
		}
		else if ( max_w == 6 && max_h == 7 ) {
			max_template_NoCG_slice <6,7> <<< grid, block >>>(pitch_in.data, pitch_in.pitch, pitch_out.data, pitch_out.pitch, img_w, img_h);
		}
		else if ( max_w == 7 && max_h == 6 ) {
			max_template_NoCG_slice <7,6> <<< grid, block >>>(pitch_in.data, pitch_in.pitch, pitch_out.data, pitch_out.pitch, img_w, img_h);
		}
		else if ( max_w == 7 && max_h == 5 ) {
			max_template_NoCG_slice <7,5> <<< grid, block >>>(pitch_in.data, pitch_in.pitch, pitch_out.data, pitch_out.pitch, img_w, img_h);
		}
		else if ( max_w == 5 && max_h == 7 ) {
			max_template_NoCG_slice <5,7> <<< grid, block >>>(pitch_in.data, pitch_in.pitch, pitch_out.data, pitch_out.pitch, img_w, img_h);
		}
		else {
			max_NoCG_slice<<< grid, block >>>(pitch_in.data, pitch_in.pitch, pitch_out.data, pitch_out.pitch, img_w, img_h, max_w, max_h);
		}
	} else {
		// Use covering grid functions.

		// Copy grid to device and use grid'ed version.
		CUDA_SAFE_CALL( cudaMemcpyToSymbol( covering_grid2, cudagrid->data, cudagrid->size()*sizeof(float), 0, cudaMemcpyHostToDevice) );

		//max_CG_slice<<< grid, block >>>(pitch_in.data, pitch_in.pitch, pitch_out.data, pitch_out.pitch, img_w, img_h, max_w, max_h);
		if ( max_w == 7 && max_h == 7 ) {
			max_template_CG_slice <7,7> <<< grid, block >>>(pitch_in.data, pitch_in.pitch, pitch_out.data, pitch_out.pitch, img_w, img_h);
		}
		else if ( max_w == 6 && max_h == 7 ) {
			max_template_CG_slice <6,7> <<< grid, block >>>(pitch_in.data, pitch_in.pitch, pitch_out.data, pitch_out.pitch, img_w, img_h);
		}
		else if ( max_w == 7 && max_h == 6 ) {
			max_template_CG_slice <7,6> <<< grid, block >>>(pitch_in.data, pitch_in.pitch, pitch_out.data, pitch_out.pitch, img_w, img_h);
		}
		else if ( max_w == 7 && max_h == 5 ) {
			max_template_CG_slice <7,5> <<< grid, block >>>(pitch_in.data, pitch_in.pitch, pitch_out.data, pitch_out.pitch, img_w, img_h);
		}
		else if ( max_w == 5 && max_h == 7 ) {
			max_template_CG_slice <5,7> <<< grid, block >>>(pitch_in.data, pitch_in.pitch, pitch_out.data, pitch_out.pitch, img_w, img_h);
		}
		else {
			max_CG_slice<<< grid, block >>>(pitch_in.data, pitch_in.pitch, pitch_out.data, pitch_out.pitch, img_w, img_h, max_w, max_h);
		}
	}
}

// Calculate max in 7-by-7 centered window directly.
__global__ void max_7x7_cent_slice(float *din, int din_pitch, float *res, int res_pitch, int data_w, int data_h)
{
	// Load din data into shared cache.
	__shared__ float data[ 1056 ];
	const int data_width = 48;
	const int data_size = IMUL(data_width, blockDim.y);
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.

	// Read in data.
	const int ix = IMUL(data_width, threadIdx.y + 3) + threadIdx.x + 16;		// Shared thread memory index.
	const float *my_din = PITCH_GET(din, din_pitch, y);				// Calculate my row of din.
	data[ix] = my_din[x];
	// Load rightwards data
	const bool right_on = x + blockDim.x < data_w;
	data[ ix + blockDim.x ] = right_on ? my_din[ x + blockDim.x ] : -FLT_MAX;
	// Load leftwards data
	const bool left_on = x >= blockDim.x;
	data[ ix - blockDim.x ] = left_on ? my_din[ x - blockDim.x ] : -FLT_MAX;
	// Load upwards data
	if ( threadIdx.y < 3 ) {
		const int y_off = (threadIdx.y<<1) + 1;
		if ( y - y_off >= 0 ) {
			const float *din_up = PITCH_GET(din, din_pitch, y - y_off);
			data[ ix - IMUL(data_width, y_off) ] = din_up[x];
			// Load sidewards-up data
			data[ ix - IMUL(data_width, y_off) - blockDim.x ] =
				left_on ? din_up[ x - blockDim.x ] : -FLT_MAX;
			data[ ix - IMUL(data_width, y_off) + blockDim.x ] =
				right_on ? din_up[ x + blockDim.x ] : -FLT_MAX;
		} else {
			data[ ix - IMUL(data_width, y_off) ] = -FLT_MAX;
			data[ ix - IMUL(data_width, y_off) - blockDim.x ] = -FLT_MAX;
			data[ ix - IMUL(data_width, y_off) + blockDim.x ] = -FLT_MAX;
		}
	}
	// Load downwards data
	if ( threadIdx.y >= blockDim.y - 3 ) {
		const int y_off = ((blockDim.y - threadIdx.y) <<1) - 1;
		if ( y + y_off < data_h ) {
			const float *din_down = PITCH_GET(din, din_pitch, y + y_off);
			data[ ix + IMUL(data_width, y_off) ] = din_down[x];
			// Load sidewards-up data
			data[ ix + IMUL(data_width, y_off) - blockDim.x ] =
				left_on ? din_down[ x - blockDim.x ] : -FLT_MAX;
			data[ ix + IMUL(data_width, y_off) + blockDim.x ] =
				right_on ? din_down[ x + blockDim.x ] : -FLT_MAX;
		} else {
			data[ ix + IMUL(data_width, y_off) ] = -FLT_MAX;
			data[ ix + IMUL(data_width, y_off) - blockDim.x ] = -FLT_MAX;
			data[ ix + IMUL(data_width, y_off) + blockDim.x ] = -FLT_MAX;
		}
	}

	__syncthreads();

	// Compute my max result.
	float gf = -FLT_MAX;
	//#pragma unroll
	for ( int i=-3; i<=3; ++i ) {
		//#pragma unroll
		for ( int j=-3; j<=3; ++j ) {
			//gf = fmaxf(gf, data[ ix + IMUL(data_width, i) + j ]);
			if ( data[ ix + IMUL(data_width, i) + j ] > gf )
				gf = data[ ix + IMUL(data_width, i) + j ];
		}
	}
	// Calculate my row of the results.
	float *my_res = PITCH_GET(res, res_pitch, y);
	my_res[x] = gf;
}

// Perform window-based normalization.
void svlCudaMax7x7Cent(svlCudaPitch &pitch_in, svlCudaPitch &pitch_out, int img_w, int img_h)
{
	dim3 block(16, 16);
	if ( img_w == -1 ) img_w = pitch_in.w;
	if ( img_h == -1 ) img_h = pitch_in.h;
	dim3 grid( iDivUp(img_w, block.x), iDivUp(img_h, block.y) );
	max_7x7_cent_slice<<< grid, block >>>(pitch_in.data, pitch_in.pitch, pitch_out.data, pitch_out.pitch, img_w, img_h);
}

//// Calculate max in 7-by-7 centered window directly.
//__global__ void max_7x7_slice(float *din, int din_pitch, float *res, int res_pitch, int data_w, int data_h)
//{
//	// Load din data into shared cache.
//	__shared__ float data[ 768 ];
//	const int data_width = 32;
//	const int data_size = IMUL(data_width, blockDim.y);
//	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
//	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.
//
//	// Read in data.
//	const int ix = IMUL(data_width, threadIdx.y) + threadIdx.x;		// Shared thread memory index.
//	const float *my_din = PITCH_GET(din, din_pitch, y);				// Calculate my row of din.
//	data[ix] = my_din[x];
//	// Load rightwards data
//	const bool right_on = x + blockDim.x < data_w;
//	data[ ix + blockDim.x ] = right_on ? my_din[ x + blockDim.x ] : -FLT_MAX;
//	// Load downwards data
//	if ( threadIdx.y >= blockDim.y - 6 ) {
//		const int y_off = ((blockDim.y - threadIdx.y) <<1) - 1;
//		if ( y + y_off < data_h ) {
//			const float *din_down = PITCH_GET(din, din_pitch, y + y_off);
//			data[ ix + IMUL(data_width, y_off) ] = din_down[x];
//			// Load sidewards-up data
//			data[ ix + IMUL(data_width, y_off) + blockDim.x ] =
//				right_on ? din_down[ x + blockDim.x ] : -FLT_MAX;
//		} else {
//			data[ ix + IMUL(data_width, y_off) ] = -FLT_MAX;
//			data[ ix + IMUL(data_width, y_off) + blockDim.x ] = -FLT_MAX;
//		}
//	}
//
//	__syncthreads();
//
//	// Compute my max result.
//	float gf = -FLT_MAX;
//#pragma unroll
//	for ( int i=0; i<7; ++i ) {
//#pragma unroll
//		for ( int j=0; j<7; ++j ) {
//			//gf = fmaxf(gf, data[ ix + IMUL(data_width, i) + j ]);
//			if ( data[ ix + IMUL(data_width, i) + j ] > gf )
//				gf = data[ ix + IMUL(data_width, i) + j ];
//		}
//	}
//	// Calculate my row of the results.
//	float *my_res = PITCH_GET(res, res_pitch, y);
//	my_res[x] = gf;
//}

// Calculate max in 7-by-7 upper-left-based window directly.
__device__ void max_7x7_slice(float *din, int din_pitch, float *res, int res_pitch, int data_w, int data_h)
{
	// Load din data into shared cache.
	__shared__ float data[ 768 ];
	const int data_width = 32;
	const int data_size = IMUL(data_width, blockDim.y);
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.

	// Read in data.
	const int ix = IMUL(data_width, threadIdx.y) + threadIdx.x;		// Shared thread memory index.
	const float *my_din = PITCH_GET(din, din_pitch, y);				// Calculate my row of din.
	data[ix] = my_din[x];
	// Load rightwards data
	const bool right_on = x + blockDim.x < data_w;
	data[ ix + blockDim.x ] = right_on ? my_din[ x + blockDim.x ] : -FLT_MAX;
	// Load downwards data
	if ( threadIdx.y >= blockDim.y - 6 ) {
		const int y_off = ((blockDim.y - threadIdx.y) <<1) - 1;
		if ( y + y_off < data_h ) {
			const float *din_down = PITCH_GET(din, din_pitch, y + y_off);
			data[ ix + IMUL(data_width, y_off) ] = din_down[x];
			// Load sidewards-up data
			data[ ix + IMUL(data_width, y_off) + blockDim.x ] =
				right_on ? din_down[ x + blockDim.x ] : -FLT_MAX;
		} else {
			data[ ix + IMUL(data_width, y_off) ] = -FLT_MAX;
			data[ ix + IMUL(data_width, y_off) + blockDim.x ] = -FLT_MAX;
		}
	}

	__syncthreads();

	// Compute my max result.
	float gf = -FLT_MAX;
#pragma unroll
	for ( int i=0; i<7; ++i ) {
#pragma unroll
		for ( int j=0; j<7; ++j ) {
			//gf = fmaxf(gf, data[ ix + IMUL(data_width, i) + j ]);
			if ( data[ ix + IMUL(data_width, i) + j ] > gf )
				gf = data[ ix + IMUL(data_width, i) + j ];
		}
	}
	// Calculate my row of the results.
	float *my_res = PITCH_GET(res, res_pitch, y);
	my_res[x] = gf;
}

// The above without a covering grid (no penalty for inlining a __device__ function).
__global__ void max_7x7_NoCG_slice(float *din, int din_pitch, float *res, int res_pitch, int data_w, int data_h)
{
	max_7x7_slice(din, din_pitch, res, res_pitch, data_w, data_h);
}
// The above with a covering grid.
__global__ void max_7x7_CG_slice(float *din, int din_pitch, float *res, int res_pitch, int data_w, int data_h)
{
	if ( covering_grid2[ IMUL(blockIdx.y, gridDim.x) + blockIdx.x ] != 0.0f ) {
		max_7x7_slice(din, din_pitch, res, res_pitch, data_w, data_h);
	}
}

// Perform window-based normalization.
void svlCudaMax7x7(svlCudaPitch &pitch_in, svlCudaPitch &pitch_out, int img_w, int img_h, svlCudaCoveringGrid *cudagrid)
{
	dim3 block(16, 16);
	if ( img_w == -1 ) img_w = pitch_in.w;
	if ( img_h == -1 ) img_h = pitch_in.h;
	dim3 grid( iDivUp(img_w, block.x), iDivUp(img_h, block.y) );

	// Verify that grids are of the same size.
	if ( cudagrid != NULL && sizesMatch(cudagrid, grid) ) {
		// Copy grid to device and use grid'ed version.
		CUDA_SAFE_CALL( cudaMemcpyToSymbol( covering_grid2, cudagrid->data, cudagrid->grid_x*cudagrid->grid_y*sizeof(float), 0, cudaMemcpyHostToDevice) );
		max_7x7_CG_slice<<< grid, block >>>(pitch_in.data, pitch_in.pitch, pitch_out.data, pitch_out.pitch, img_w, img_h);
	} else {
		max_7x7_NoCG_slice<<< grid, block >>>(pitch_in.data, pitch_in.pitch, pitch_out.data, pitch_out.pitch, img_w, img_h);
	}
}


// Calculate min and max in 4-by-4 window directly.
// TODO: Templatize and benchmark?
__global__ void minmax_4x4_slice(const float *din, int din_pitch, float *min_res, int min_pitch, float *max_res, int max_pitch, int data_w, int data_h)
{
	// Load din data into shared cache.
	__shared__ float data[ 768 ];
	const int data_width = 32;
	const int data_size = IMUL(data_width, blockDim.y);
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.

	// Read in data.
	const int ix = IMUL(data_width, threadIdx.y) + threadIdx.x;		// Shared thread memory index.
	const float *my_din = PITCH_GET(din, din_pitch, min(y, data_h - 1) );				// Calculate my row of din.
	const bool on_on = x < data_w;
	data[ix] = on_on ? my_din[x] : my_din[ data_w - 1 ];
	// Load rightwards data
	const bool right_on = x + blockDim.x < data_w;
	if ( threadIdx.x < 4 ) {
		data[ ix + blockDim.x ] = right_on ? my_din[ x + blockDim.x ] : my_din[ data_w - 1 ];
	}
	// Load downwards data
	if ( threadIdx.y < 4 ) {
		const float *din_down = PITCH_GET(din, din_pitch, min(y + blockDim.y, data_h - 1) );
		data[ ix + IMUL(data_width, blockDim.y) ] = on_on ? din_down[x] : din_down[ data_w - 1 ];
		// Load sidewards-down data
		if ( threadIdx.x < 4 ) {
			data[ ix + IMUL(data_width, blockDim.y) + blockDim.x ] = right_on ? din_down[ x + blockDim.x ] : din_down[ data_w - 1 ];
		}
	}

	if ( x >= data_w || y >= data_h )
		return;

	__syncthreads();

	// Compute my max result.
	float gf_min = data[ix];
	float gf_max = data[ix];
	if ( min_res && max_res ) {
		#pragma unroll
		for ( int i=0; i<4; ++i ) {
			#pragma unroll
			for ( int j=0; j<4; ++j ) {
				gf_min = fminf(gf_min, data[ ix + IMUL(data_width, i) + j ]);
				gf_max = fmaxf(gf_max, data[ ix + IMUL(data_width, i) + j ]);
			}
		}
	} else if ( min_res ) {
		#pragma unroll
		for ( int i=0; i<4; ++i )
			#pragma unroll
			for ( int j=0; j<4; ++j )
				gf_min = fminf(gf_min, data[ ix + IMUL(data_width, i) + j ]);
	} else if ( max_res ) {
		#pragma unroll
		for ( int i=0; i<4; ++i )
			#pragma unroll
			for ( int j=0; j<4; ++j )
				gf_max = fmaxf(gf_max, data[ ix + IMUL(data_width, i) + j ]);
	}

	// Calculate my row of the results.
	if ( min_res ) {
		float *min_out = PITCH_GET(min_res, min_pitch, y);
		min_out[x] = gf_min;
	}
	if ( max_res ) {
		float *max_out = PITCH_GET(max_res, max_pitch, y);
		max_out[x] = gf_max;
	}
}

// Calculate min and max over 4-by-4 windows.
void svlCudaMinMax4x4(const svlCudaPitch *pitch_in, svlCudaPitch *min_out, svlCudaPitch *max_out, int img_w, int img_h)
{
	dim3 block(16, 16);
	if ( img_w == -1 ) img_w = pitch_in->w;
	if ( img_h == -1 ) img_h = pitch_in->h;
	dim3 grid( iDivUp(img_w, block.x), iDivUp(img_h, block.y) );
	minmax_4x4_slice<<< grid, block >>>(pitch_in->data, pitch_in->pitch,
		min_out ? min_out->data : NULL, min_out ? min_out->pitch : 0,
		max_out ? max_out->data : NULL, max_out ? max_out->pitch : 0, img_w, img_h);
}


// ****** 32-by-32 min-max functions ******

// Calculate 32-wide min across rows assuming the image has already been min'ed in 4-by-4 windows.
__global__ void min_row32by4_slice(const float *min_res, int min_pitch, float *dout, int dpitch, int data_w, int valid_w)
{
	// Load row into shared memory.
	__shared__ float data[ 512 ];
	const int x = IMUL(valid_w, blockIdx.x) + threadIdx.x;	// x index to patch.
	const float *min_out = PITCH_GET(min_res, min_pitch, blockIdx.y);
	const bool on = x < data_w;
	data[threadIdx.x] = on ? min_out[x] : min_out[data_w-1];

	if ( threadIdx.x >= valid_w || !on )
		return; // Early exit for off-of-computation threads.
	
	__syncthreads();

	// Calculate min over 32 spaces by leapfrogging through sub 4-by-4 mins.
	float gf = data[threadIdx.x];
#pragma unroll 7
	for ( int i=1; i<=7; ++i )
		gf = fminf(gf, data[ threadIdx.x + (i<<2) ]);

	float *doutf = PITCH_GET(dout, dpitch, blockIdx.y);
	doutf[x] = gf;
}

// Calculate 32-wide max across rows assuming the image has already been min'ed in 4-by-4 windows.
__global__ void max_row32by4_slice(const float *max_res, int max_pitch, float *dout, int dpitch, int data_w, int valid_w)
{
	// Load row into shared memory.
	__shared__ float data[ 512 ];
	const int x = IMUL(valid_w, blockIdx.x) + threadIdx.x;	// x index to patch.
	const float *max_out = PITCH_GET(max_res, max_pitch, blockIdx.y);
	const bool on = x < data_w;

	data[threadIdx.x] = on ? max_out[x] : max_out[data_w-1];
	if ( threadIdx.x >= valid_w || !on )
		return; // Early exit for off-of-computation threads.
	
	__syncthreads();

	// Calculate max over 32 spaces by leapfrogging through sub 4-by-4 maxes.
	float gf = data[threadIdx.x];
#pragma unroll 7
	for ( int i=1; i<=7; ++i )
		gf = fmaxf(gf, data[ threadIdx.x + (i<<2) ]);

	// Write out result.
	float *doutf = PITCH_GET(dout, dpitch, blockIdx.y);
	doutf[x] = gf;
}

// Calculate 32-wide min down columns assuming the image has already been min'ed in 4-by-4 windows.
__global__ void min_col32by4_slice(const float *min_res, int min_pitch, float *dout, int dpitch, int data_w, int data_h, int valid_h, int y_off)
{
	// Load row into shared memory.
	__shared__ float data[ 512 ];
	const int data_width = 16;
	// Use stuttering order of threads in y.
	const int y = y_off + ((IMUL(valid_h, blockIdx.y) + threadIdx.y)<<2);	// Crazy y get.
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;			// Standard x value get.

	if ( x >= data_w )
		return;

	// Shared data index
	const int ix = IMUL(data_width, threadIdx.y) + threadIdx.x;
	// Read in data
	const float *min_out = y < data_h ? PITCH_GET(min_res, min_pitch, y) : PITCH_GET(min_res, min_pitch, data_h-1);
	data[ix] = min_out[x];

	if ( threadIdx.y >= valid_h || y >= data_h )
		return; // Early exit for off-of-computation threads.
	
	__syncthreads();

	// Calculate min over 32 spaces by leapfrogging through sub 4-by-4 mins.
	float gf = data[ix];
#pragma unroll 7
	for ( int i=1; i<=7; ++i )
		gf = fminf(data[ ix + IMUL(data_width,i) ], gf);

	float *doutf = PITCH_GET(dout, dpitch, y);
	doutf[x] = gf;
}

// Calculate 32-wide max down columns assumaxg the image has already been max'ed in 4-by-4 windows.
__global__ void max_col32by4_slice(const float *max_res, int max_pitch, float *dout, int dpitch, int data_w, int data_h, int valid_h, int y_off)
{
	// Load row into shared memory.
	__shared__ float data[ 512 ];
	const int data_width = 16;
	// Use stuttering order of threads in y.
	const int y = y_off + ((IMUL(valid_h, blockIdx.y) + threadIdx.y)<<2);	// Crazy y get.
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;			// Standard x value get.
	if ( x >= data_w )
		return;

	// Shared data index
	const int ix = IMUL(data_width, threadIdx.y) + threadIdx.x;
	// Read in data
	const float *max_out = y < data_h ? PITCH_GET(max_res, max_pitch, y) : PITCH_GET(max_res, max_pitch, data_h-1);
	data[ix] = max_out[x];

	if ( threadIdx.y >= valid_h || y >= data_h )
		return; // Early exit for off-of-computation threads.
	
	__syncthreads();

	// Calculate max over 32 spaces by leapfrogging through sub 4-by-4 maxes.
	float gf = data[ix];
#pragma unroll 7
	for ( int i=1; i<=7; ++i )
		gf = fmaxf(data[ ix + IMUL(data_width,i) ], gf);

	float *doutf = PITCH_GET(dout, dpitch, y);
	doutf[x] = gf;
}

// Calculate min and max over 32-by-32 windows.
void svlCudaMinMax32x32(const svlCudaPitch *pin, svlCudaPitch *temp, svlCudaPitch *min_out, svlCudaPitch *max_out, int img_w, int img_h)
{
	// First calculate internal 4-by-4 min-max windows.
	dim3 block4(16, 16);
	if ( img_w == -1 ) img_w = pin->w;
	if ( img_h == -1 ) img_h = pin->h;
	dim3 grid4( iDivUp(img_w, block4.x), iDivUp(img_h, block4.y) );
	minmax_4x4_slice<<< grid4, block4 >>>(pin->data, pin->pitch,
		min_out ? min_out->data : NULL, min_out ? min_out->pitch : 0,
		max_out ? max_out->data : NULL, max_out ? max_out->pitch : 0, img_w, img_h);

	// Do row-wise sub operation to get 4-by-32-block min-maxes.
	dim3 grid_row32( 2, img_h );
	int valid_w = (img_w>>1) + (img_w%2);
	dim3 block_row32( iAlignUp( valid_w + 28, 16), 1 );
		// Do col-wise sub operation to get final 32-by-32-block min-maxes.
	dim3 block_col32( 16, 16 );
	int valid_h = block_col32.y - 7;
	dim3 grid_col32( iDivUp( img_w, 16), iDivUp( (img_h>>2), valid_h) );

	if ( min_out ) {
		min_row32by4_slice<<< grid_row32, block_row32 >>>(min_out->data, min_out->pitch, temp->data, temp->pitch, img_w, valid_w);
		// Since we are using stuttered addresses in y, we need to run this four times for the four offsets.
		min_col32by4_slice<<< grid_col32, block_col32 >>>(temp->data, temp->pitch, min_out->data, min_out->pitch, img_w, img_h, valid_h, 0);
		min_col32by4_slice<<< grid_col32, block_col32 >>>(temp->data, temp->pitch, min_out->data, min_out->pitch, img_w, img_h, valid_h, 1);
		min_col32by4_slice<<< grid_col32, block_col32 >>>(temp->data, temp->pitch, min_out->data, min_out->pitch, img_w, img_h, valid_h, 2);
		min_col32by4_slice<<< grid_col32, block_col32 >>>(temp->data, temp->pitch, min_out->data, min_out->pitch, img_w, img_h, valid_h, 3);
	}
	if ( max_out ) {
		max_row32by4_slice<<< grid_row32, block_row32 >>>(max_out->data, max_out->pitch, temp->data, temp->pitch, img_w, valid_w);
		max_col32by4_slice<<< grid_col32, block_col32 >>>(temp->data, temp->pitch, max_out->data, max_out->pitch, img_w, img_h, valid_h, 0);
		max_col32by4_slice<<< grid_col32, block_col32 >>>(temp->data, temp->pitch, max_out->data, max_out->pitch, img_w, img_h, valid_h, 1);
		max_col32by4_slice<<< grid_col32, block_col32 >>>(temp->data, temp->pitch, max_out->data, max_out->pitch, img_w, img_h, valid_h, 2);
		max_col32by4_slice<<< grid_col32, block_col32 >>>(temp->data, temp->pitch, max_out->data, max_out->pitch, img_w, img_h, valid_h, 3);
	}
}

// Add two pitches in quadrature.
__global__ void add_quad_slice(float *din1, int din1_pitch, const float *din2, int din2_pitch, int img_w, int img_h)
{
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;

	if ( x < img_w && y < img_h ) {
		// Compute result.
		float *f1 = PITCH_GET(din1, din1_pitch, y);
		const float *f2 = PITCH_GET(din2, din2_pitch, y);
		const float ff1 = f1[x] * f1[x];
		const float ff2 = f2[x] * f2[x];
		f1[x] = __powf( ff1 + ff2, 0.5f );
	}
}

// Add two pitches in quadrature.
void svlCudaAddQuadrature(svlCudaPitch *a, const svlCudaPitch *b, int img_w, int img_h)
{
	dim3 block(16, 16);
	if ( img_w == -1 ) img_w = a->w;
	if ( img_h == -1 ) img_h = a->h;
	dim3 grid( iDivUp(img_w, block.x), iDivUp(img_h, block.y) );
	add_quad_slice<<< grid, block >>>(a->data, a->pitch, b->data, b->pitch, img_w, img_h);
}

// Calculate mean in 4-by-4 window directly.
__global__ void mean_4x4_slice(const float *din, int din_pitch, float *res, int rpitch, int data_w, int data_h)
{
	// Load din data into shared cache.
	__shared__ float data[ 768 ];
	const int data_width = 32;
	const int data_size = IMUL(data_width, blockDim.y);
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.

	// Read in data.
	const int ix = IMUL(data_width, threadIdx.y) + threadIdx.x;		// Shared thread memory index.
	const float *my_din = PITCH_GET(din, din_pitch, y);				// Calculate my row of din.
	const bool on_on = x < data_w && y < data_h;
	data[ix] = on_on ? my_din[x] : 0.0f;
	// Load rightwards data
	const bool right_on = on_on && x + blockDim.x < data_w;
	if ( threadIdx.x < 4 ) {
		data[ ix + blockDim.x ] = right_on ? my_din[ x + blockDim.x ] : 0.0f;
	}
	// Load downwards data
	if ( threadIdx.y < 4 ) {
		const float *din_down = PITCH_GET(din, din_pitch, y + blockDim.y);
		const bool up_on = on_on && y + blockDim.y < data_h;
		data[ ix + IMUL(data_width, blockDim.y) ] = up_on ? din_down[x] : 0.0f;
		// Load sidewards-down data
		if ( threadIdx.x < 4 ) {
			data[ ix + IMUL(data_width, blockDim.y) + blockDim.x ] = up_on && right_on ? din_down[ x + blockDim.x ] : 0.0f;
		}
	}

	if ( x >= data_w || y >= data_h )
		return;

	__syncthreads();

	// Compute my max result.
	float gf = 0.0f;
#pragma unroll
	for ( int i=0; i<4; ++i ) {
#pragma unroll
		for ( int j=0; j<4; ++j ) {
			gf += data[ ix + IMUL(data_width, i) + j ];
		}
	}

	// Calculate my row of the results.
	float *rout = PITCH_GET(res, rpitch, y);
	rout[x] = 0.0625f * gf;
}

// Calculate 32-wide mean across rows assuming the image has already been mean'ed in 4-by-4 windows.
__global__ void mean_row32by4_slice(const float *mean, int mpitch, float *dout, int dpitch, int data_w, int valid_w)
{
	// Load row into shared memory.
	__shared__ float data[ 512 ];
	const int x = IMUL(valid_w, blockIdx.x) + threadIdx.x;	// x index to patch.
	const float *mout = PITCH_GET(mean, mpitch, blockIdx.y);
	const bool on = x < data_w;
	data[threadIdx.x] = on ? mout[x] : 0.0f;

	if ( threadIdx.x >= valid_w || !on )
		return; // Early exit for off-of-computation threads.
	
	__syncthreads();

	// Calculate mean over 32 spaces by leapfrogging through sub 4-by-4 means.
	float gf = data[threadIdx.x];
#pragma unroll 7
	for ( int i=4; i<=28; i+=4 )
		gf += data[ threadIdx.x + i ];
	
	float *doutf = PITCH_GET(dout, dpitch, blockIdx.y);
	doutf[x] = 0.1250f * gf; // Write out result.
}

// Calculate 32-wide mean down columns assuming the image has already been mean'ed in 4-by-4 windows.
__global__ void mean_col32by4_slice(const float *mean, int mpitch, float *dout, int dpitch, int data_w, int data_h, int valid_h, int y_off)
{
	// Load cols into into shared memory.
	__shared__ float data[ 512 ];
	const int data_width = 16;
	// Use stuttering order of threads in y.
	const int y = y_off + ((IMUL(valid_h, blockIdx.y) + threadIdx.y)<<2);	// Crazy y get.
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;			// Standard x value get.
	if ( x >= data_w )
		return;

	const int ix = IMUL(data_width, threadIdx.y) + threadIdx.x;		// Shared data index
	// Read in data
	const float *mout = PITCH_GET(mean, mpitch, y);
	const bool on = y < data_h && x < data_w;
	data[ix] = on ? mout[x] : 0.0f;

	if ( threadIdx.y >= valid_h || !on )
		return; // Early exit for off-of-computation threads.
	
	__syncthreads();

	// Calculate min over 32 spaces by leapfrogging through sub 4-by-4 mins.
	float gf = data[ix];
#pragma unroll 7
	for ( int i=1; i<=7; ++i )
		gf += data[ ix + IMUL(data_width,i) ];
	
	float *doutf = PITCH_GET(dout, dpitch, y);
	doutf[x] = 0.1250f * gf; // Write out result.
}


// Calculate mean over 32-by-32 windows.
void svlCudaMean32x32(const svlCudaPitch *pin, svlCudaPitch *mean, svlCudaPitch *temp, int img_w, int img_h)
{
	// First calculate internal 4-by-4 mean windows.
	dim3 block4(16, 16);
	if ( img_w == -1 ) img_w = pin->w;
	if ( img_h == -1 ) img_h = pin->h;
	dim3 grid4( iDivUp(img_w, block4.x), iDivUp(img_h, block4.y) );
	mean_4x4_slice<<< grid4, block4 >>>(pin->data, pin->pitch, mean->data, mean->pitch, img_w, img_h);	
	
	//cm->pitch2file("pre.out", mean);
	// Do row-wise sub operation to get 4-by-32-block means.
	// TODO: Don't always need to halve image size: only when over thread limit.
	int valid_w = (img_w>>1) + (img_w%2);
	dim3 block_row32( iAlignUp( valid_w + 28, 16), 1 );
	dim3 grid_row32( 2, img_h );
	mean_row32by4_slice<<< grid_row32, block_row32 >>>(mean->data, mean->pitch, temp->data, temp->pitch, img_w, valid_w);

	// Do col-wise sub operation to get final 32-by-32-block min-maxes.
	dim3 block_col32( 16, 16 );
	int valid_h = block_col32.y - 7;
	dim3 grid_col32( iDivUp( img_w, 16), iDivUp( (img_h>>2), valid_h) );
	// Since we are using stuttered addresses in y, we need to run this four times for the four offsets.
	mean_col32by4_slice<<< grid_col32, block_col32 >>>(temp->data, temp->pitch, mean->data, mean->pitch, img_w, img_h, valid_h, 0);
	mean_col32by4_slice<<< grid_col32, block_col32 >>>(temp->data, temp->pitch, mean->data, mean->pitch, img_w, img_h, valid_h, 1);
	mean_col32by4_slice<<< grid_col32, block_col32 >>>(temp->data, temp->pitch, mean->data, mean->pitch, img_w, img_h, valid_h, 2);
	mean_col32by4_slice<<< grid_col32, block_col32 >>>(temp->data, temp->pitch, mean->data, mean->pitch, img_w, img_h, valid_h, 3);
}


// Calculate max over an upper-left-centered window.
__global__ void lasik_cmax_slice(const float *conv, int cpitch, const float *sum, int spitch, const float *sum2, int s2pitch, const float *correct, int copitch,
								 float *res, int rpitch, float N, float patchSum, float patchNorm, int data_w, int data_h, int NUM_COLS, int NUM_ROWS)
{
	// Load din data into shared cache.
	__shared__ float data[ 1024 ];
	__shared__ float sumd[ 1024 ];
	__shared__ float sum2d[ 1024 ];
	const int data_width = 32;
	const int data_size = IMUL(data_width, blockDim.y);
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.

	// Read in data
	const int ix = IMUL(data_width, threadIdx.y) + threadIdx.x;		// Shared thread memory index.
	const float *convf = PITCH_GET(conv, cpitch, y);
	const float *sumf = PITCH_GET(sum, spitch, y);
	const float *sum2f = PITCH_GET(sum2, s2pitch, y);
	const bool on_on = x < data_w && y < data_h;
	if ( on_on ) {
		data[ix] = convf[x];
		sumd[ix] = sumf[x];
		sum2d[ix] = sum2f[x];
	} else {
		sumd[ix] = sum2d[ix] = data[ix] = -FLT_MAX;
	}
	// Load rightwards data
	const bool right_on = on_on && x + blockDim.x < data_w;
	if ( right_on ) {
		data[ ix + blockDim.x ] = convf[ x + blockDim.x ];
		sumd[ ix + blockDim.x ] = sumf[ x + blockDim.x ];
		sum2d[ ix + blockDim.x ] = sum2f[ x + blockDim.x ];
	} else {
		sumd[ ix + blockDim.x ] = sum2d[ ix + blockDim.x ] = data[ ix + blockDim.x ] = -FLT_MAX;
	}
	// Load downwards data
	if ( threadIdx.y < NUM_ROWS ) {
		if ( y + blockDim.y < data_h ) {
			// Read in upper entries
			const float *convf = PITCH_GET(conv, cpitch, y + blockDim.y);
			const float *sumf = PITCH_GET(sum, spitch, y + blockDim.y);
			const float *sum2f = PITCH_GET(sum2, s2pitch, y + blockDim.y);
			if ( on_on ) {
				data[ ix + data_size ] = convf[x];
				sumd[ ix + data_size ] = sumf[x];
				sum2d[ ix + data_size ] = sum2f[x];
			} else {
				sumd[ ix + data_size ] = sum2d[ ix + data_size ] = data[ ix + data_size ] = -FLT_MAX;
			}
			// Read in rightward entry.
			if ( right_on ) {
				data[ ix + data_size + blockDim.x ] = convf[ x + blockDim.x ];
				sumd[ ix + data_size + blockDim.x ] = sumf[ x + blockDim.x ];
				sum2d[ ix + data_size + blockDim.x ] = sum2f[ x + blockDim.x ];
			} else {
				sumd[ ix + data_size + blockDim.x ] = 
				sum2d[ ix + data_size + blockDim.x ] =
				data[ ix + data_size + blockDim.x ] = -FLT_MAX;
			}
		} else {
			sumd[ ix + data_size ]  = sumd[ ix + data_size + blockDim.x ]  =
			sum2d[ ix + data_size ] = sum2d[ ix + data_size + blockDim.x ] =
			data[ ix + data_size ]  = data[ ix + data_size + blockDim.x ]  = -FLT_MAX;
		}
	}

	// Read in correction factor.
	const float *corf = PITCH_GET(correct, copitch, y);
	const float corfac = corf[x];

	__syncthreads();

	// Compute my max result.
	float gf = -FLT_MAX;
#pragma unroll
	for ( int i=0; i<NUM_ROWS; ++i ) {
#pragma unroll
		for ( int j=0; j<NUM_COLS; ++j ) {
			const int thix = ix + IMUL(data_width, i) + j;
			gf = fmaxf( gf, __fdividef( data[thix] - patchSum * corfac, sqrtf( patchNorm * ( sum2d[thix] + corfac*(N*corfac - 2.0f*sumd[thix]) ) ) ) );
		}
	}

	// Calculate my row of the results.
	float *resf = PITCH_GET(res, rpitch, y);
	resf[x] = gf;
}
// Templated version of the above.
template<int NUM_COLS, int NUM_ROWS>
__global__ void lasik_cmaxT_slice(const float *conv, int cpitch, const float *sum, int spitch, const float *sum2, int s2pitch, const float *correct, int copitch,
								 float *res, int rpitch, float N, float patchSum, float patchNorm, int data_w, int data_h)
{
	// Load din data into shared cache.
	__shared__ float data[ 1024 ];
	__shared__ float sumd[ 1024 ];
	__shared__ float sum2d[ 1024 ];
	const int data_width = 32;
	const int data_size = IMUL(data_width, blockDim.y);
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.

	// Read in data
	const int ix = IMUL(data_width, threadIdx.y) + threadIdx.x;		// Shared thread memory index.
	const float *convf = PITCH_GET(conv, cpitch, min(y, data_h - 1));
	const float *sumf = PITCH_GET(sum, spitch, min(y, data_h - 1));
	const float *sum2f = PITCH_GET(sum2, s2pitch, min(y, data_h - 1));
	const bool on_x = x < data_w;
	data[ix] = on_x ? convf[x] : convf[ data_w - 1 ];
	sumd[ix] = on_x ? sumf[x] : sumf[ data_w - 1 ];
	sum2d[ix] = on_x ? sum2f[x] : sum2f[ data_w - 1 ];

	// Load rightwards data
	const bool right_on = on_x && x + blockDim.x < data_w;
	data[ ix + blockDim.x ] = right_on ? convf[ x + blockDim.x ] : convf[ data_w - 1 ];
	sumd[ ix + blockDim.x ] = right_on ? sumf[ x + blockDim.x ] : sumf[ data_w - 1 ];
	sum2d[ ix + blockDim.x ] = right_on ? sum2f[ x + blockDim.x ] : sum2f[ data_w - 1 ];

	// Load downwards data
	if ( threadIdx.y < NUM_ROWS ) {
		// Read in upper entries
		const float *convf = PITCH_GET(conv, cpitch, min(y + blockDim.y, data_h - 1));
		const float *sumf = PITCH_GET(sum, spitch, min(y + blockDim.y, data_h - 1));
		const float *sum2f = PITCH_GET(sum2, s2pitch, min(y + blockDim.y, data_h - 1));
		data[ ix + data_size ] = on_x ? convf[x] : convf[ data_w - 1 ];
		sumd[ ix + data_size ] = on_x ? sumf[x] : sumf[ data_w - 1 ];
		sum2d[ ix + data_size ] = on_x ? sum2f[x] : sum2f[ data_w - 1 ];

		// Read in rightward entry.
		data[ ix + data_size + blockDim.x ] = right_on ? convf[ x + blockDim.x ] : convf[ data_w - 1 ];
		sumd[ ix + data_size + blockDim.x ] = right_on ? sumf[ x + blockDim.x ] : sumf[ data_w - 1 ];
		sum2d[ ix + data_size + blockDim.x ] = right_on ? sum2f[ x + blockDim.x ] : sum2f[ data_w - 1 ];
	}

	// Read in correction factor.
	const float *corf = PITCH_GET(correct, copitch, min(y, data_h - 1));
	const float corfac = on_x ? corf[x] : corf[ data_w - 1 ];

	if ( !on_x || y >= data_h )
		return;

	__syncthreads();

	// Compute my max result.
	float gf = -FLT_MAX;
#pragma unroll
	for ( int i=0; i<NUM_ROWS; ++i ) {
#pragma unroll
		for ( int j=0; j<NUM_COLS; ++j ) {
			const int thix = ix + IMUL(data_width, i) + j;
			gf = fmaxf( gf, __fdividef( data[thix] - patchSum * corfac, sqrtf( patchNorm * ( sum2d[thix] + corfac*(N*corfac - 2.0f*sumd[thix]) ) ) ) );
		}
	}

	if ( isnan(gf) || isinf(gf) )
		gf = 0.0f;

	// Calculate my row of the results.
	float *resf = PITCH_GET(res, rpitch, y);
	resf[x] = gf;
}

// Perform window-based max Lasik-corrected max.
void svlCudaLasikCorrectedMax(const svlCudaPitch *conv, const svlCudaPitch *sum, const svlCudaPitch *sum2, const svlCudaPitch *correct,
							  svlCudaPitch *maxres, float N, float patchSum, float patchNorm, int max_w, int max_h, int img_w, int img_h)
{
	dim3 block(16, 16);
	if ( img_w == -1 ) img_w = conv->w;
	if ( img_h == -1 ) img_h = conv->h;
	dim3 grid( iDivUp(img_w, block.x), iDivUp(img_h, block.y) );

	// Verify that grids are of the same size.
	if ( max_w == 4 && max_h == 4 )
		lasik_cmaxT_slice <4,4> <<< grid, block >>>(conv->data, conv->pitch, sum->data, sum->pitch, sum2->data, sum2->pitch, correct->data, correct->pitch,
													maxres->data, maxres->pitch, N, patchSum, patchNorm, img_w, img_h);
	else if ( max_w == 7 && max_h == 7 )
		lasik_cmaxT_slice <7,7> <<< grid, block >>>(conv->data, conv->pitch, sum->data, sum->pitch, sum2->data, sum2->pitch, correct->data, correct->pitch,
													maxres->data, maxres->pitch, N, patchSum, patchNorm, img_w, img_h);
	else if ( max_w == 6 && max_h == 7 )
		lasik_cmaxT_slice <6,7> <<< grid, block >>>(conv->data, conv->pitch, sum->data, sum->pitch, sum2->data, sum2->pitch, correct->data, correct->pitch,
													maxres->data, maxres->pitch, N, patchSum, patchNorm, img_w, img_h);
	else if ( max_w == 7 && max_h == 6 )
		lasik_cmaxT_slice <7,6> <<< grid, block >>>(conv->data, conv->pitch, sum->data, sum->pitch, sum2->data, sum2->pitch, correct->data, correct->pitch,
													maxres->data, maxres->pitch, N, patchSum, patchNorm, img_w, img_h);
	else if ( max_w == 7 && max_h == 5 )
		lasik_cmaxT_slice <7,5> <<< grid, block >>>(conv->data, conv->pitch, sum->data, sum->pitch, sum2->data, sum2->pitch, correct->data, correct->pitch,
													maxres->data, maxres->pitch, N, patchSum, patchNorm, img_w, img_h);
	else if ( max_w == 5 && max_h == 7 )
		lasik_cmaxT_slice <5,7> <<< grid, block >>>(conv->data, conv->pitch, sum->data, sum->pitch, sum2->data, sum2->pitch, correct->data, correct->pitch,
													maxres->data, maxres->pitch, N, patchSum, patchNorm, img_w, img_h);
	else
		lasik_cmax_slice<<< grid, block >>>(conv->data, conv->pitch, sum->data, sum->pitch, sum2->data, sum2->pitch, correct->data, correct->pitch,
											maxres->data, maxres->pitch, N, patchSum, patchNorm, img_w, img_h, max_w, max_h);
}

// Do a max in four points around each point.
__global__ void point4_max_slice(const float *pin, int ipitch, float *pout, int ppitch, int NUM_COLS, int NUM_ROWS, int data_w, int data_h)
{
	// Load din data into shared cache.
	__shared__ float data[ 1024 ];
	const int data_width = 32;
	const int data_size = IMUL(data_width, blockDim.y);
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.

	// Read in data
	const int ix = IMUL(data_width, threadIdx.y) + threadIdx.x;		// Shared thread memory index.
	const float *convf = PITCH_GET(pin, ipitch, y);
	const bool on_on = x < data_w && y < data_h;
	data[ix] = on_on ? convf[x] : -FLT_MAX;
	// Load rightwards data
	const bool right_on = on_on && x + blockDim.x < data_w;
	data[ ix + blockDim.x ] = right_on ? convf[ x + blockDim.x ] : -FLT_MAX;
	
	// Load downwards data
	if ( threadIdx.y < NUM_ROWS ) {
		if ( y + blockDim.y < data_h ) {
			// Read in upper entries
			const float *convf = PITCH_GET(pin, ipitch, y + blockDim.y);
			data[ ix + data_size ] = on_on ? convf[x] : -FLT_MAX;
			// Read in rightward entry.
			data[ ix + data_size + blockDim.x ] = right_on ? convf[ x + blockDim.x ] : -FLT_MAX;
		} else {
			data[ ix + data_size ] = data[ ix + data_size + blockDim.x ]  = -FLT_MAX;
		}
	}
	if ( !on_on )
		return;

	if ( !on_on )
		return;

	__syncthreads();

	// Output result.
	float *poutf = PITCH_GET(pout, ppitch, y);
	poutf[x] = fmaxf( fmaxf( data[ix], data[ ix + NUM_COLS ] ),
					  fmaxf( data[ ix + IMUL(data_width, NUM_ROWS) ], data[ ix + IMUL(data_width, NUM_ROWS) + NUM_COLS ] ) );
}

// Do a max in four points around each point.
void svlCudaFourPointMax(const svlCudaPitch *pin, svlCudaPitch *pout, int max_w, int max_h, int img_w, int img_h)
{
	dim3 block(16, 16);
	if ( img_w == -1 ) img_w = pin->w;
	if ( img_h == -1 ) img_h = pin->h;
	dim3 grid( iDivUp(img_w, block.x), iDivUp(img_h, block.y) );
	
	point4_max_slice<<< grid, block >>>(pin->data, pin->pitch, pout->data, pout->pitch, max_w, max_h, img_w, img_h);
}


