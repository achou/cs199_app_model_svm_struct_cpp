/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaTimer.cpp
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
**
*****************************************************************************/

#include "svlBase.h"
#include "svlCudaTimer.h"

// Static members.
bool svlCudaTimer::_enabled = false;
bool svlCudaTimer::_inited = false;
stack<const char*> svlCudaTimer::_names = stack<const char*>();
const char *svlCudaTimer::_lastName = NULL;
map<const char*,vector<float>*> svlCudaTimer::_elapseds = map<const char*,vector<float>*>();
vector<clock_t> svlCudaTimer::times = vector<clock_t>();


// Free memory.
svlCudaTimer::~svlCudaTimer()
{
	for ( map<const char*,vector<float>*>::iterator it=_elapseds.begin(); it!=_elapseds.end(); ++it )
		if ( it->second )
			delete it->second;
	if ( _inited )
		svlCudaTimerDestruct();
}

// Set the enabled, and init if necessary.
void svlCudaTimer::setEnabled(bool enabled)
{
	if ( enabled ) {
		if ( !_inited ) {
			// Initialize timers.
			svlCudaTimerInit();
			_inited = true;
		}
		_names.push(_lastName = "*"); // Catch all.
	}
	_enabled = enabled;
}

// Add a time to the current track.
void svlCudaTimer::add_time(float e)
{
	if ( _names.empty() ) {
		SVL_LOG(SVL_LOG_FATAL, "Not tracking any Cuda function");
		return;
	}
	vector<float> *vec = _elapseds[_names.top()];
	if ( !vec ) {
		_elapseds[_names.top()] = vec = new vector<float>();
	}
	vec->push_back(e);
}


