/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaDecisionTreeSet.cpp
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
**
*****************************************************************************/

#include "svlCudaDecisionTreeSet.h"

extern svlCudaMemHandler *cm;
typedef unsigned char uchar;


//void printTree15(int *iptr, float *fptr)
//{
//	printf("%d %d %d    %0.4f %0.4f %0.4f %0.4f %0.4f %0.4f %0.4f %0.4f\n",
//		*iptr, *(iptr+1), *(iptr+2),
//		*fptr, *(fptr+2), *(fptr+3), *(fptr+4), *(fptr+5), *(fptr+6), *(fptr+7), *(fptr+8));
//}

// Default constructor.
svlCudaDecisionTreeSet::svlCudaDecisionTreeSet()
{
	treei = NULL;
	treef = NULL;
	dtreei = NULL;
	dtreef = NULL;
}   

// Read in model from file.
// TODO paulb: Deprecate and read in from XML.
svlCudaDecisionTreeSet::svlCudaDecisionTreeSet(const char *file)
{
	treei = NULL;
	treef = NULL;
	dtreei = NULL;
	dtreef = NULL;

	FILE *fp = fopen(file,"rb");
	if ( !fp ) {
		printf("Initialized svlCudaDecisionTreeSet15 with NULL FILE: %s.\n", file);
		return;
	}

#if 1
	// Read in total tree.
	fp = fopen(file,"rb");
	fread(&_nTrees,sizeof(int),1,fp);
	treei = new int[_nTrees*7];
	treef = new float[_nTrees*21];
	fread(treei,sizeof(int),_nTrees*7,fp);
	fread(treef,sizeof(float),_nTrees*21,fp);
	fclose(fp);

	//// Debug print
	//for ( int i=0; i<_nTrees; ++i )
	//	printTree15(itree + i*3, ftree + i*8);

	// Allocate device memory and transfer in.
	dtreei = cm->allocLine(_nTrees*7, "decision tree ints");
	dtreef = cm->allocLine(_nTrees*21, "decision tree floats");
	cm->lineToGPU(dtreei, (float*)treei);
	cm->lineToGPU(dtreef, treef);
#else
	XMLNode root = XMLNode::parseFile(file);
	load(root);
#endif
}

svlCudaDecisionTreeSet::~svlCudaDecisionTreeSet()
{
	freeMem();
}


void svlCudaDecisionTreeSet::freeMem()
{
	// Free host memory.
	if ( treei ) {
		delete[] treei;
		treei = NULL;
		delete[] treef;
		treef = NULL;
	}
	// Free device memory.
	if ( dtreei ) {
		cm->freeLine(dtreei);
		dtreei = NULL;
		cm->freeLine(dtreef);
		dtreef = NULL;
	}
}

void svlCudaDecisionTreeSet::readFromDecisionTree(svlDecisionTree *dt, double w, int *iptr, float *fptr)
{
	svlDecisionTreeNode *root = dt->getRoot();
	*iptr++ = root ? root->_split_var : -1; // Root split
	*fptr++ = root ? root->_split_val : 0.0f; // Root comparator
	
	svlDecisionTreeNode *L = root ? root->_L : NULL;
	*iptr++ = L ? L->_split_var : -1; // Left split
	*fptr++ = L ? L->_split_val : 0.0f; // Left comparator
	*fptr++ = root ? w * root->_Lp : 0.0f; // Left value

	svlDecisionTreeNode *LL = L ? L->_L : NULL;
	*iptr++ = LL ? LL->_split_var : -1; // Left-left split
	*fptr++ = LL ? LL->_split_val : 0.0f; // Left-left comparator
	*fptr++ = L ? w * L->_Lp : 0.0f; // Left-left value

	//svlDecisionTreeNode *LLL = LL ? LL->_L : NULL;
	*fptr++ = LL ? w * LL->_Lp : 0.0f; // Left-left-left value
	//svlDecisionTreeNode *LLR = LL ? LL->_R : NULL;
	*fptr++ = LL ? w * LL->_Rp : 0.0f; // Left-left-right value

	svlDecisionTreeNode *LR = L ? L->_R : NULL;
	*iptr++ = LR ? LR->_split_var : -1; // Left-right split
	*fptr++ = LR ? LR->_split_val : 0.0f; // Left-right comparator
	*fptr++ = L ? w * L->_Rp : 0.0f; // Left-right value

	//svlDecisionTreeNode *LRL = LR ? LR->_L : NULL;
	*fptr++ = LR ? w * LR->_Lp : 0.0f; // Left-right-left value
	//svlDecisionTreeNode *LRR = LR ? LR->_R : NULL;
	*fptr++ = LR ? w * LR->_Rp : 0.0f; // Left-right-right value

	svlDecisionTreeNode *R = root ? root->_R : NULL;
	*iptr++ = R ? R->_split_var : -1; // Right split
	*fptr++ = R ? R->_split_val : 0.0f; // Right comparator
	*fptr++ = root ? w * root->_Rp : 0.0f; // Right value

	svlDecisionTreeNode *RL = R ? R->_L : NULL;
	*iptr++ = RL ? RL->_split_var : -1; // Right-left split
	*fptr++ = RL ? RL->_split_val : 0.0f; // Right-left comparator
	*fptr++ = R ? w * R->_Lp : 0.0f; // Right-left value

	//svlDecisionTreeNode *RLL = RL ? RL->_L : NULL;
	*fptr++ = RL ? w * RL->_Lp : 0.0f; // Right-left-left value
	//svlDecisionTreeNode *RLR = RL ? RL->_R : NULL;
	*fptr++ = RL ? w * RL->_Rp : 0.0f; // Right-left-right value

	svlDecisionTreeNode *RR = R ? R->_R : NULL;
	*iptr++ = RR ? RR->_split_var : -1; // Right-right split
	*fptr++ = RR ? RR->_split_val : 0.0f; // Right-right comparator
	*fptr++ = R ? w * R->_Rp : 0.0f; // Right-right value

	//svlDecisionTreeNode *RRL = RL ? RL->_L : NULL;
	*fptr++ = RR ? w * RR->_Lp : 0.0f; // Right-right-left value
	//svlDecisionTreeNode *RRR = RL ? RL->_R : NULL;
	*fptr++ = RR ? w * RR->_Rp : 0.0f; // Right-right-right value
}

// Load from an XML node.
bool svlCudaDecisionTreeSet::load(XMLNode &node)
{
	if ( !this->svlDecisionTreeSet::load(node) ) {
		SVL_LOG(SVL_LOG_WARNING, "Classifier failed to load");
		return false;
	}
	
	// Convert trees to arrays of floats and ints.
	treei = new int[7*_nTrees];
	treef = new float[21*_nTrees];
	int *iptr = treei;
	float *fptr = treef;
	for ( int i=0; i<_nTrees; ++i ) {
		// Read out data members.
		readFromDecisionTree(_trees[i], _weights[i], iptr, fptr);
		// Advance pointers.
		iptr += 7;
		fptr += 21;
	}

	// Allocate device memory and transfer.
	dtreei = cm->allocLine(_nTrees*7, "decision tree ints");
	dtreef = cm->allocLine(_nTrees*21, "decision tree floats");
	cm->lineToGPU(dtreei, (float*)treei);
	cm->lineToGPU(dtreef, treef);
	/*cm->line2file("i.out", dtreei);
	cm->line2file("f.out", dtreef);*/

	return true;
}

// Evaluate the decision tree host-side.
void svlCudaDecisionTreeSet::hostEvaluate(const float *features, float *invalid, float *res, int feature_len, int feature_stride, int n_fvs) const
{
	// Clear invalid bits.
	if ( invalid )
		memset(invalid, 0, sizeof(float)*n_fvs);

	// Compute response of tree on host.
	for ( int i=0; i<n_fvs; ++i ) {
		//printf("%d/%d\n", i, n_fvs);
		float gf = 0.0f; // Response over trees.
		const float *data = features + feature_stride * i;

		// TODO paulb: Restore this.
		/*for ( int j=0; j<feature_len; ++j )
			if ( isinf(data[j]) || isnan(data[j]) )
				invalid[i] = 1.0f;*/

		for ( int t=0; t<_nTrees; ++t ) {
			int tixi = 7*t, tixf = 21*t;

			if ( data[ treei[tixi] ] <= treef[tixf] ) {
				// L
				if ( treei[tixi + 1] < 0 )
					gf += treef[tixf + 2];
				else if ( data[ treei[tixi + 1] ] <= treef[tixf + 1] ) {
					// LL
					if ( treei[tixi + 2] < 0 )
						gf += treef[tixf + 4];
					else if ( data[ treei[tixi + 2] ] <= treef[tixf + 3] )
						gf += treef[tixf + 5];
					else
						gf += treef[tixf + 6];
				} else {
					// LR
					if ( treei[tixi + 3] < 0 )
						gf += treef[tixf + 8];
					else if ( data[ treei[tixi + 3] ] <= treef[tixf + 7] )
						gf += treef[tixf + 9];
					else
						gf += treef[tixf + 10];
				}
			} else {
				// R
				if ( treei[tixi + 4] < 0 )
					gf += treef[tixf + 12];
				else if ( data[ treei[tixi + 4] ] <= treef[tixf + 11] ) {
					// RL
					if ( treei[tixi + 5] < 0 )
						gf += treef[tixf + 14];
					else if ( data[ treei[tixi + 5] ] <= treef[tixf + 13] )
						gf += treef[tixf + 15];
					else
						gf += treef[tixf + 16];
				} else {
					// RR
					if ( treei[tixi + 6] < 0 )
						gf += treef[tixf + 18];
					else if ( data[ treei[tixi + 6] ] <= treef[tixf + 17] )
						gf += treef[tixf + 19];
					else
						gf += treef[tixf + 20];
				}
			}
		}
		
		// Write out result.
		res[i] = gf;
	}
}

// Evaluate using the DecisionTree class on an Eigen matrix.
void svlCudaDecisionTreeSet::hostEvaluate(const MatrixXd &X, float *res) const
{
	for ( int i=0; i<X.rows(); ++i ) {
		VectorXd v = X.row(i);
		res[i] = getScore(v);
	}
}


