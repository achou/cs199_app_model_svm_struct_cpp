/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaMultiPitch.cpp
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  See the .h file.
**
*****************************************************************************/

#include "svlBase.h"
#include "svlCudaMultiPitch.h"

extern svlCudaMemHandler *cm;

svlCudaMultiPitch::svlCudaMultiPitch()
	: _w(-1), _h(-1), _num_pitches(0)
{
	// Allocate empty baseline vector.
	_pitchMap[PII(0,0)] = new vector<svlCudaPitch*>();
}

svlCudaMultiPitch::svlCudaMultiPitch(int w, int h, int n)
	: _w(w), _h(h), _num_pitches(0)
{
	// Allocate pitches.
	vector<svlCudaPitch*> *vec = new vector<svlCudaPitch*>();
	vec->reserve(n);
	_num_pitches += n;
	for ( int i=0; i<n; ++i )
		vec->push_back(cm->allocPitch(_w, _h));
	// Save vector into map.
	_pitchMap[PII(0,0)] = vec;
}

svlCudaMultiPitch::~svlCudaMultiPitch()
{
	clear();
}

// Set baseline image size.
void svlCudaMultiPitch::setImageSize(int w, int h)
{
	if ( w <= 0 || h <= 0 ) {
		SVL_LOG(SVL_LOG_FATAL, "Poor width and/or height: " << w << ", " << h);
		return;
	}
	// Check if we must free pre-existing data.
	if ( _w > 0 && _h > 0 )
		clear();

	_w = w;
	_h = h;

	// Allocate empty baseline vector.
	_pitchMap[PII(0,0)] = new vector<svlCudaPitch*>();
}

// Allocate different-sized pitches.
void svlCudaMultiPitch::alloc(int dw, int dh, int n, const char *name, bool enumerate)
{
	if ( _w <= 0 || _h <= 0 ) {
		SVL_LOG(SVL_LOG_FATAL, "Width and/or height undefined: " << _w << ", " << _h);
		return;
	}
	if ( _w+dw <= 0 || _w+dh <= 0 ) {
		SVL_LOG(SVL_LOG_FATAL, "Delta width/heights results in null image size: " << (_w+dw) << ", " << (_h+dh));
		return;
	}

	vector<svlCudaPitch*> *vec = _pitchMap[PII(dw,dh)];
	// Allocate vector if missing.
	if ( !vec ) {
		vec = _pitchMap[PII(dw,dh)] = new vector<svlCudaPitch*>();
	}
	
	_num_pitches += n;
	for ( int i=0; i<n; ++i ) {
		if ( !name )
			vec->push_back(cm->allocPitch(_w+dw, _h+dh));
		else if ( !enumerate )
			vec->push_back(cm->allocPitch(_w+dw, _h+dh, name));
		else {
			char *c = (char*)malloc(32); // Memory leak.
			sprintf(c,"%s %d", name, i);
			vec->push_back(cm->allocPitch(_w+dw, _h+dh, c));
		}
	}
}

// Get list of all allocated offset sizes.
void svlCudaMultiPitch::getKeys(vector<PII> &ret)
{
	ret.clear();
	ret.reserve(_pitchMap.size());
	for ( svlCudaMultiPitchMap::iterator it=_pitchMap.begin(); it!=_pitchMap.end(); ++it )
		ret.push_back(it->first);
}

// Get list of all allocated actual sizes.
void svlCudaMultiPitch::getKeySizes(vector<PII> &ret)
{
	ret.clear();
	ret.reserve(_pitchMap.size());
	for ( svlCudaMultiPitchMap::iterator it=_pitchMap.begin(); it!=_pitchMap.end(); ++it )
		ret.push_back(PII(it->first.first + _w, it->first.second + _h));
}

// Clear memory.
void svlCudaMultiPitch::clear()
{
	// Free device memory.
	_num_pitches = 0;
	for ( svlCudaMultiPitchMap::iterator it=_pitchMap.begin(); it!=_pitchMap.end(); ++it ) {
		for ( vector<svlCudaPitch*>::iterator p=it->second->begin(); p!=it->second->end(); ++p )
			cm->freePitch(*p);
		delete it->second;
		it->second = NULL;
	}
}

// Clear pitches of a certain offset size.
void svlCudaMultiPitch::clear(int dw, int dh)
{
	vector<svlCudaPitch*> *vec = _pitchMap[PII(dw,dh)];
	if ( vec ) {
		for ( vector<svlCudaPitch*>::iterator p=vec->begin(); p!=vec->end(); ++p )
			cm->freePitch(*p);
		_num_pitches -= vec->size();
		delete vec;
		_pitchMap[PII(dw,dh)] = NULL;
	}
}

// Clear pitches of an absolute size.
void svlCudaMultiPitch::clearSize(int w, int h)
{
	vector<svlCudaPitch*> *vec = _pitchMap[PII(w - _w,h - _h)];
	if ( vec ) {
		for ( vector<svlCudaPitch*>::iterator p=vec->begin(); p!=vec->end(); ++p )
			cm->freePitch(*p);
		_num_pitches -= vec->size();
		delete vec;
		_pitchMap[PII(w - _w,h - _h)] = NULL;
	}
}

//// Get at least n of minimum size.
//bool svlCudaMultiPitch::getMinSizeAtLeast(int dw, int dh, int n, vector<svlCudaPitch*> &pitches)
//{
//	pitches.clear();
//	if ( n > _num_pithces )
//		return false; // That was easy.
//
//	for ( svlCudaMultiPitchMap::iterator it=_pitchMap.begin(); it!=_pitchMap.end(); ++it ) {
//		if ( it->first.first >= dw && it->first.second >= dh ) {
//			for ( vector<svlCudaPitch*>::iterator p=it->second->begin(); p!=it->second->end(); ++p )
//				cm->freePitch(*p);
//			delete it->second;
//			it->second = NULL;
//		}
//	}
//
//}



