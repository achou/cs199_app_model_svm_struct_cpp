/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2009, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaCommon.cu
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  Asdf ...
**
*****************************************************************************/

#include "cv.h"
#include "cxcore.h"

#include "svlCudaCommon.h"

#define FROM_GPU_COPY(d,p) CUDA_SAFE_CALL( cudaMemcpy2D(d, p.w*sizeof(float), p.data, p.pitch, p.w*sizeof(float), p.h, cudaMemcpyDeviceToHost) )
#define TO_GPU_COPY(p,d) CUDA_SAFE_CALL( cudaMemcpy2D(p.data, p.pitch, d, p.w*sizeof(float), p.w*sizeof(float), p.h, cudaMemcpyDeviceToHost) )

using namespace std;


// Initialize CUDA. TODO paulb: Might be needed in the future.
void initCuda(int argc, char **argv)
{
    CUT_DEVICE_INIT(argc, argv);
}

// Allocate linear memory.
void svlCudaAllocLine(int w, svlCudaLine &n)
{
	n.w = w;
	CUDA_SAFE_CALL( cudaMalloc((void**)&(n.data), w*sizeof(float)) );
}

// Free linear memory.
void svlCudaFreeLine(svlCudaLine &n)
{
	CUDA_SAFE_CALL( cudaFree((void*)n.data) );
}

// Send a line to the GPU.
void svlCudaLineToGPU(svlCudaLine &n, const float *src, int w)
{
	CUDA_SAFE_CALL( cudaMemcpy(n.data, src, (w == -1 ? n.w : w)*sizeof(float), cudaMemcpyHostToDevice) );
}

// Get a line from the GPU.
void svlCudaLineFromGPU(float *dest, const svlCudaLine &n, int w)
{
	CUDA_SAFE_CALL( cudaMemcpy(dest, n.data, (w == -1 ? n.w : w)*sizeof(float), cudaMemcpyDeviceToHost) );
}

// Allocate a pitch.
void svlCudaAllocPitch(int w, int h, svlCudaPitch *pitch)
{
	pitch->w = w;
	pitch->h = h;
	size_t sz;
	CUDA_SAFE_CALL( cudaMallocPitch((void**)&(pitch->data), (size_t*)&sz, w * sizeof(float), h) );
	pitch->pitch = int(sz);
}

// Free device memory in the form of a pitch.
void svlCudaFreePitch(svlCudaPitch *p)
{
	CUDA_SAFE_CALL( cudaFree((void*)p->data) );
}

// Allocate a 3d pitch.
void svlCudaAllocPitch3d(int w, int h, int z, svlCudaPitch3d *pitch)
{
	pitch->w = w;
	pitch->h = h;
	pitch->z = z;
	size_t sz;
	CUDA_SAFE_CALL( cudaMallocPitch((void**)&(pitch->data), (size_t*)&sz, w * sizeof(float), h*z) );
	pitch->pitch = int(sz);
}

// Free device memory in the form of a pitch.
void svlCudaFreePitch3d(svlCudaPitch3d *p)
{
	CUDA_SAFE_CALL( cudaFree((void*)p->data) );
}

// 1D memory set on the GPU.
void svlCudaMemset(svlCudaLine *p, int value, int w)
{
	CUDA_SAFE_CALL( cudaMemset(p->data, value, ( w == -1 ? p->w : w )*sizeof(float) ) );
}

// 2D memory set on the GPU.
void svlCudaMemset2D(svlCudaPitch *p, int value, int w, int h)
{
	CUDA_SAFE_CALL( cudaMemset2D(p->data, p->pitch, value, ( w == -1 ? p->w : w )*sizeof(float), ( h == -1 ? p->h : h ) ) );
}

// Overload for to the GPU.
void svlCudaPitchToGPU(svlCudaPitch *p, const float *src, int w, int h)
{
	//CUDA_SAFE_CALL( cudaMemcpy2D(p.data, p.pitch, src, p.w*sizeof(float), p.w*sizeof(float), p.h, cudaMemcpyHostToDevice) );
	CUDA_SAFE_CALL( cudaMemcpy2D(p->data, p->pitch, src, (w == -1 ? p->w : w)*sizeof(float), (w == -1 ? p->w : w)*sizeof(float), (h == -1 ? p->h : h), cudaMemcpyHostToDevice) );
}

// Overload for to the GPU.
void svlCudaPitch3dToGPU(svlCudaPitch3d *p, const float *src, int w, int h)
{
	if ( h == -1 ) {
		// All one call.
		CUDA_SAFE_CALL( cudaMemcpy2D(p->data, p->pitch, src, (w == -1 ? p->w : w)*sizeof(float),
			(w == -1 ? p->w : w)*sizeof(float), p->h*p->z, cudaMemcpyHostToDevice) );
	} else {
		// Have to stagger starting addresses.
		for ( int i=0; i<p->z; ++i ) {
			CUDA_SAFE_CALL( cudaMemcpy2D( p->get(i), p->pitch, src, (w == -1 ? p->w : w)*sizeof(float),
				(w == -1 ? p->w : w)*sizeof(float), (h == -1 ? p->h : h), cudaMemcpyHostToDevice) );
		}
	}
}

// The above with the async command.
void svlCudaPitchToGPUasync(svlCudaPitch &p, const float *src, int w, int h)
{
	CUDA_SAFE_CALL( cudaMemcpy2DAsync(p.data, p.pitch, src, (w == -1 ? p.w : w)*sizeof(float), (w == -1 ? p.w : w)*sizeof(float), (h == -1 ? p.h : h), cudaMemcpyHostToDevice, 0) );
}

// Copy pitch to the GPU with differential source pitch.
void svlCudaPitchToGPU_all(svlCudaPitch *p, const float *src, int s_pitch, int w, int h)
{
	if ( w == -1 ) w = p->w;
	if ( h == -1 ) h = p->h;
	CUDA_SAFE_CALL( cudaMemcpy2D(p->data, p->pitch, src, s_pitch, w*sizeof(float), h, cudaMemcpyHostToDevice) );
}

// Copy pitch memory from GPU.
void svlCudaPitchFromGPU(float *dest, const svlCudaPitch *p, int w, int h)
{
	CUDA_SAFE_CALL( cudaMemcpy2D(dest, (w == -1 ? p->w : w)*sizeof(float),
		p->data, p->pitch, (w == -1 ? p->w : w)*sizeof(float), (h == -1 ? p->h : h), cudaMemcpyDeviceToHost) );
}

// Copy 3d pitch memory from GPU.
void svlCudaPitch3dFromGPU(float *dest, const svlCudaPitch3d *p, int w, int h)
{
	if ( h == -1 ) {
		CUDA_SAFE_CALL( cudaMemcpy2D(dest, (w == -1 ? p->w : w)*sizeof(float), p->data, p->pitch,
			(w == -1 ? p->w : w)*sizeof(float), p->h*p->z, cudaMemcpyDeviceToHost) );
	} else {
		assert(false);
	}
}

// Copy pitch memory from GPU with access to destination pitch.
void svlCudaPitchFromGPU_all(float *dest, int d_pitch, int w, int h, const svlCudaPitch *p)
{
	CUDA_SAFE_CALL( cudaMemcpy2D(dest, d_pitch, p->data, p->pitch, (w == -1 ? p->w : w)*sizeof(float), (h == -1 ? p->h : h), cudaMemcpyDeviceToHost) );
}

// Copy bewteen pitches on the GPU.
void svlCudaPitchToPitch(svlCudaPitch *dest, const svlCudaPitch *src, int w, int h)
{
	CUDA_SAFE_CALL( cudaMemcpy2D(dest->data, dest->pitch, src->data, src->pitch, (w == -1 ? src->w : w)*sizeof(float), (h == -1 ? src->h : h), cudaMemcpyDeviceToDevice) );
}

// Read in addresses along a row and collapse them to the first indices.
__global__ void row_address_slice(float *data, int data_pitch, int *addy, int addy_pitch, int addy_w)
{
	// Read in the index this thread is suppose to read in.
	const int *addy_in = INT_PITCH_GET(addy, addy_pitch, blockIdx.y);
	const int ix = addy_in[threadIdx.x];
	if ( threadIdx.x >= addy_w || ix == -1 )
		return; // Nothing to read in.

	// Else, read in the value we must shift down.
	float *data_in = PITCH_GET(data, data_pitch, blockIdx.y);
	const float ans = data_in[ix];

	__syncthreads(); // Wait for everyone to have grabbed their value before writing them back.

	// Write our value back at the thread index.
	data_in[threadIdx.x] = ans;
	//data_in[threadIdx.x] = -10.0f;
}

// Do row-collected random access. 
void svlCudaRowRandomAccess(const IplImage *ixes, IplImage *data_out, const vector<int> row_counts, float *dest, svlCudaPitch &data, svlCudaPitch &addys)
{
	// Copy indices onto addys pitch.
	CUDA_SAFE_CALL( cudaMemcpy2D(addys.data, addys.pitch, ixes->imageData, ixes->widthStep, ixes->width*sizeof(int), ixes->height, cudaMemcpyHostToDevice) );

	dim3 block( iAlignUp( ixes->width, 16 ), 1 ); // Alloc enough x threads to cover max index.
	dim3 grid( 1, data.h ); // Tile blocks over all image rows.
	/*printf("Block: %d,%d\n", block.x, block.y);
	printf("Grid: %d,%d\n", grid.x, grid.y);*/
	row_address_slice<<< grid, block >>>(data.data, data.pitch, (int*)addys.data, addys.pitch, ixes->width);

	// Copy data back to output image.
	CUDA_SAFE_CALL( cudaMemcpy2D(data_out->imageData, data_out->widthStep, data.data, data.pitch, data_out->width*sizeof(int), ixes->height, cudaMemcpyDeviceToHost) );
}

// Read in addresses along a row and collapse them to the first indices.
__global__ void linear_address_slice(float *data_in, int data_in_pitch, int *addy, int addy_pitch, float *data_out, int data_out_pitch)
{
	// Read in the index this thread is suppose to read in.
	const int *addy_in = INT_PITCH_GET(addy, addy_pitch, blockIdx.y);
	const int base_ix = threadIdx.x << 1;
	const int row = addy_in[base_ix];
	if ( row == -1 )
		return; // Nothing to read in.
	const int col = addy_in[ base_ix + 1 ];

	// Else, read in the value we must shift in.
	const float *data_inf = PITCH_GET(data_in, data_in_pitch, row);
	const float ans = data_inf[col];

	// Write value back out.
	const int row_out = blockIdx.y >> 1;
	const int col_out = blockIdx.y % 2 == 0 ? threadIdx.x : blockDim.x + threadIdx.x;
	float *data_outf = PITCH_GET(data_out, data_out_pitch, row_out);
	data_outf[col_out] = ans;
}


// Do linear-collected random access.
void svlCudaLinearRandomAccess(const IplImage *ixes, int max_row, float *dest, svlCudaPitch &data, svlCudaPitch &addys, svlCudaPitch &data_out)
{
	// Copy indices onto addys pitch.
	CUDA_SAFE_CALL( cudaMemcpy2D(addys.data, addys.pitch, ixes->imageData, ixes->widthStep, ixes->width*sizeof(int), max_row, cudaMemcpyHostToDevice) );

	dim3 block( ixes->width>>1, 1 ); // Alloc enough x threads to cover max index (div 2 because of row-column storage).
	dim3 grid( 1, max_row ); // Tile blocks over all active image rows.
	/*printf("Block: %d,%d\n", block.x, block.y);
	printf("Grid: %d,%d\n", grid.x, grid.y);*/
	linear_address_slice<<< grid, block >>>(data.data, data.pitch, (int*)addys.data, addys.pitch, data_out.data, data_out.pitch);

	//printf("Copying out w,h: %d,%d\n", ixes->width, max_row);
	// Copy data back out to destination.
	CUDA_SAFE_CALL( cudaMemcpy2D(dest, ixes->width*sizeof(float), data_out.data, data_out.pitch, ixes->width*sizeof(float), iDivUp(max_row, 2), cudaMemcpyDeviceToHost) );
}

// Do indexing from a line into a pitch, saving to a line.
__global__ void line_2Daddress_slice(const float *data, int data_pitch, const int *addy, float *res_out, int line_off, int line_stride, int w, bool yx)
{
	// Find this thread's index.
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;
	if ( x >= w )
		return;

	// Read indices we are supposed to read.
	const int read_r = addy[ 2*x + ( yx ? 0 : 1 ) ];
	const int read_c = addy[ 2*x + ( yx ? 1 : 0 ) ];

	// Read in value.
	const float *dataf = PITCH_GET(data, data_pitch, read_r);
	const float ans = dataf[read_c];

	// Write out value.
	res_out[ IMUL(x, line_stride) + line_off ] = ans;
}

// Do linear-collected random access from a line into a pitch, saving into a line with a stride.
// TODO: Add yx support (right now default is true).
//void svlCudaLine2DRandomAccess(const svlCudaLine *ixline, const svlCudaPitch *data, svlCudaLine *res, int res_off, int res_stride, int res_n)
void svlCudaPitchIx2line(const svlCudaLine *ixline, const svlCudaPitch *data, svlCudaLine *res, int w, bool yx, int line_off, int line_stride)
{
	if ( w == -1 ) w = ixline->w/2;
	dim3 block( 256 );
	dim3 grid( iDivUp(w, block.x) );
	line_2Daddress_slice<<< grid, block >>>(data->data, data->pitch, (int*)ixline->data, res->data, line_off, line_stride, w, yx);
}

// Add values to a line.
__global__ void add_point2line_slice(const int *lin, int add1, int add2, int *lout, int w)
{
	const int x = 2 * ( IMUL(blockDim.x, blockIdx.x) + threadIdx.x );
	if ( x >= w )
		return;
	lout[x] = lin[x] + add1;
	lout[x+1] = lin[x+1] + add2;
}

// Add x-y values to a line.
//void svlCudaAddCvPoint2Line(const svlCudaLine *lin, CvPoint &pt, svlCudaLine *lout, int w, bool yx)
void svlCudaXYpoint2Line(const svlCudaLine *lin, int x, int y, svlCudaLine *lout, int w, bool yx)
{
	if ( w == -1 ) w = lin->w;
	dim3 block( 256 );
	dim3 grid( iDivUp(w/2, block.x ) );
	add_point2line_slice<<< grid, block >>>((int*)lin->data,
		yx ? y : x,
		yx ? x : y,
		(int*)lout->data, w);
}

// Fill an indexed pitch from a line.
template<class C>
__global__ void fill_2DRA_slice(float *dout, int dpitch, const int *ixes, const C *res, int w, bool yx)
{
	// Load index.
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;
	if ( x >= w )
		return;

	const int out_y = ixes[ 2*x + ( yx ? 0 : 1 ) ];
	const int out_x = ixes[ 2*x + ( yx ? 1 : 0 ) ];
	// Output result.
	float *doutf = PITCH_GET(dout, dpitch, out_y);
	doutf[out_x] = float(res[x]);
}

// Fill a 2D image using random acess.
void svlCudaFill2DRandomAccessInt(svlCudaPitch *pout, const svlCudaLine *ixline, const svlCudaLine *res, int w, bool yx)
{
	if ( w == -1 ) w = ixline->w/2;
	dim3 block( 256 );
	dim3 grid( iDivUp(w, block.x ) );
	fill_2DRA_slice <int> <<< grid, block >>>(pout->data, pout->pitch, (int*)ixline->data, (int*)res->data, w, yx);
}

// Fill a 2D image using random acess.
//void svlCudaFill2DRandomAccessFloat(svlCudaPitch *pout, const svlCudaLine *ixline, const svlCudaLine *res, int w, bool yx)
void svlCudaLine2pitchIx(svlCudaPitch *pout, const svlCudaLine *ixline, const svlCudaLine *res, int w, bool yx)
{
	if ( w == -1 ) w = ixline->w/2;
	dim3 block( 256 );
	dim3 grid( iDivUp(w, block.x ) );
	fill_2DRA_slice <float> <<< grid, block >>>(pout->data, pout->pitch, (int*)ixline->data, res->data, w, yx);
}


