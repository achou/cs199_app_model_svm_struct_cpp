/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2009, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaMatrix.cu
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  Cuda matrix-style functions.
**
*****************************************************************************/

#include "svlCudaCommon.h"
#include "svlCudaMemHandler.h"

extern svlCudaMemHandler *cm;


// Set the diagonal of a matrix (pitch) to some value.
__global__ void set_diag_slice(float *dat, int dpitch, float val, int n)
{
	int i = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;
	if ( i < n ) {
		float *df = PITCH_GET(dat, dpitch, i);
		df[i] = val;
	}
}

// Set a matrix (pitch) to the identity matrix.
void svlCudaEye(svlCudaPitch *M)
{
	// Set all to zero.
	CUDA_SAFE_CALL( cudaMemset2D(M->data, M->pitch, 0, M->w*sizeof(float), M->h) );
	// Set the diagonal to 1.
	dim3 block(1), grid(1);
	int n = MIN(M->w, M->h);
	if ( n < 256 ) {
		block.x = iAlignUp(n, 32);
	} else {
		block.x = 256;
		grid.x = iDivUp(n, block.x);
	}
	printf("block: %d\n", block.x);
	printf("grid: %d\n", grid.x);
	set_diag_slice <<< grid, block >>> (M->data, M->pitch, 1.0f, n);
}

// Calculate a matrix inverse using Gauss-Jordan elimination.

// Set pivots for use in Guass-Jordan elimination.
__global__ void set_pivot_slice(const float *A, int Apitch, const float *I, int Ipitch, float *pivots, float *prow, int n, int i)
{
	int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;
	if ( x >= n )
		return;

	// Read in pivot row value.
	const float *Apiv = PITCH_GET(A, Apitch, i);
	const float piv = Apiv[i];
	// Read in my output row's starting value (x,i)'th element.
	const float *Ax = PITCH_GET(A, Apitch, x);
	const float gf = Ax[i];
	// Output whatever the pivot scaling value should be.
	if ( x == i ) {
		// Want to subtract from pivot row such that the scaled output is 1.
		pivots[x] = __fdividef( 1.0f-piv,piv);
	} else {
		// Make output 0.
		pivots[x] = -__fdividef(gf,piv);
	}

	// Save pivotal row to prow.
	const float *Ipiv = PITCH_GET(I, Ipitch, i);
	prow[x] = Apiv[x];
	prow[n + x] = Ipiv[x];
}

// Do Gauss-Jordan elimination with row i.
__global__ void gauss_jordan_slice(float *A, int Apitch, float *I, int Ipitch, const float *pivots, const float *prow, int n, int i, int row_mult)
{
	__shared__ float pivrow[512]; // Data for our slice of the pivot row (first A, then I).
	int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;
	int y = IMUL(row_mult, blockIdx.y);

	if ( x >= n || y >= n )
		return;

	// Read in slice of the pivot row.
	pivrow[threadIdx.x] = prow[x];
	pivrow[256 + threadIdx.x] = prow[n + x];

	// Loop over the rows.
	for ( int iy=0; iy<row_mult && y<n; ++iy, ++y ) {
		// Read in pivot value.
		const float piv = pivots[y];

		float *Af = PITCH_GET(A, Apitch, y);
		Af[x] += piv * pivrow[threadIdx.x]; // Scale by relevant entry.
		// And mirror operation on inverse matrix.
		float *If = PITCH_GET(I, Ipitch, y);
		If[x] += piv * pivrow[256 + threadIdx.x]; // Scale by relevant entry.
	}
}

// Take the inverse of a matrix using temporary matrix.
// TODO paulb: Check if A and T are the same, in which case it should be acknowledged to be destructive. And if T is NULL, too.
void svlCudaInverse(svlCudaPitch *A, svlCudaPitch *Ainv, svlCudaPitch *T)
{
	// Get size of inverse.
	int n = A->w;
	// Allocate memory for pivot values.
	svlCudaLine *pivots = cm->allocLine(n);
	svlCudaLine *prow = cm->allocLine(2*n);

	// First copy A into T (temp) to operate on it.
	printf("pitch: %d,%d\n", A->pitch, T->pitch);
	printf("w,h: %d,%d\n", A->w, A->h);
	CUDA_SAFE_CALL( cudaMemcpy2D(T->data, T->pitch, A->data, A->pitch, A->w*sizeof(float), A->h, cudaMemcpyDeviceToDevice) );

	// Set Ainv to the identity matrix.
	svlCudaEye(Ainv);

	// Set block and grid stuff.

	// Pivot-setting dimensions.
	dim3 p_block(1), p_grid(1);
	if ( n < 256 ) {
		p_block.x = iAlignUp(n,32);
	} else {
		p_block.x = 256;
	}
	p_grid.x = iDivUp(n, p_block.x);

	// Gauss-Jordan slice dimensions.
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	float elapsedTime;
//#define ROW_COL_BENCH
#ifdef ROW_COL_BENCH
	for ( int row_mult=1; row_mult<=64; ++row_mult ) {
		for ( int col_max=32; col_max<=512; col_max<<=1 ) {
#else
	int row_mult = 64;
	int col_max = 512;
#endif
			dim3 gj_block(1), gj_grid(1);
			if ( n < col_max ) {
				gj_block.x = iAlignUp(n,32);
			} else {
				gj_block.x = col_max;
			}
			gj_grid.x = iDivUp(n, gj_block.x);
			gj_grid.y = iDivUp(n, row_mult);
			/*printf("gj_block: %d,%d\n", gj_block.x, gj_block.y);
			printf("gj_grid: %d,%d\n", gj_grid.x, gj_grid.y);*/

			// Do the inverse calculation.
			cudaEventRecord(start, 0);
			for ( int i=0; i<n; ++i ) {
				set_pivot_slice <<< p_grid, p_block >>> (T->data, T->pitch, Ainv->data, Ainv->pitch, pivots->data, prow->data,
					n, i);
				gauss_jordan_slice <<< gj_grid, gj_block >>>(T->data, T->pitch, Ainv->data, Ainv->pitch, pivots->data, prow->data, 
					n, i, row_mult);
			}
			cudaEventRecord(stop, 0);
			cudaEventSynchronize(stop);
		
			cudaEventElapsedTime(&elapsedTime, start, stop);
			//printf("Rows: %d;  cols: %d;  elapsed: %0.2f ms\n", row_mult, col_max, elapsedTime);
			printf("%0.4f\n", elapsedTime);
#ifdef ROW_COL_BENCH
		}
	}
#endif
	cudaEventDestroy(start);
	cudaEventDestroy(stop);

	cout << "sanity!\n";
	cm->pitch2file("A.out", A);
	cm->pitch2file("T.out", T);
	cm->pitch2file("inv.out", Ainv);

	cm->freeLine(pivots);
	cm->freeLine(prow);
}


// Transpose kernel.
#if 0
// Naive implementation.
__global__ void transpose_slice(const float *Pin, int ppitch, float *Dout, int dpitch, int img_w, int img_h)
{
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;
	if ( x >= img_w || y >= img_h )
		return;

	const float *Pf = PITCH_GET(Pin, ppitch, y);
	float *Df = PITCH_GET(Dout, dpitch, x);
	Df[y] = Pf[x];
}
#elif 1
// Coalesces reads and writes by performing tranposition in shared memory.
__global__ void transpose_slice(const float *Pin, int ppitch, float *Dout, int dpitch, int img_w, int img_h)
{
	__shared__ float data[256];
	const int in_x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;
	const int in_y = IMUL(blockDim.x, blockIdx.y) + threadIdx.y;
	// Load data and saved to transposed index.
	const int tix = IMUL(blockDim.x, threadIdx.x) + threadIdx.y;
	const int ix = IMUL(blockDim.x, threadIdx.y) + threadIdx.x;
	const float *Pf = PITCH_GET(Pin, ppitch, in_y);
	data[tix] = in_x < img_w && in_y < img_h ? Pf[in_x] : 0.0f;
	__syncthreads();

	const int out_x = IMUL(blockDim.x, blockIdx.y) + threadIdx.x;
	const int out_y = IMUL(blockDim.x, blockIdx.x) + threadIdx.y;
	float *Df = PITCH_GET(Dout, dpitch, out_y);
	Df[out_x] = data[ix];
}
#endif

// Transpose a pitch to another pitch.
void svlCudaTranspose(const svlCudaPitch *pin, svlCudaPitch *pout, int img_w, int img_h)
{
	if ( img_w == -1 ) img_w = pin->w;	
	if ( img_h == -1 ) img_h = pin->h;

	dim3 block(16,16);
	dim3 grid( iDivUp(img_w, block.x), iDivUp(img_h, block.y) );
	transpose_slice<<< grid, block >>>(pin->data, pin->pitch, pout->data, pout->pitch, img_w, img_h);
}


