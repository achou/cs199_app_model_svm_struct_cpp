/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2009, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaMedian.cu
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  Cuda median calculation functions.
**
*****************************************************************************/

#include "svlCudaCommon.h"

#if 0
extern __device__ inline void scan_inner_loop_int(int n, int ai, int bi, int thid, int *temp_data);
#else
__device__ inline void scan_inner_loop_int(int n, int ai, int bi, int thid, int *temp_data)
{
	int offset = 1;
	// Build the sum in place up the tree
    for (int d = n/2; d > 0; d >>= 1) {
        __syncthreads();
        if (thid < d) {
            int ai = offset*(2*thid+1)-1;
            int bi = offset*(2*thid+2)-1;
            ai += CONFLICT_FREE_OFFSET(ai);
            bi += CONFLICT_FREE_OFFSET(bi);
            temp_data[bi] += temp_data[ai];
        }
        offset *= 2;
    }

    // Scan back down the tree
    // Clear the last element
    if ( thid == 0 ) {
        int index = n - 1;
        index += CONFLICT_FREE_OFFSET(index);
        temp_data[index] = 0;
    }
    // Traverse down the tree building the scan in place
    for (int d = 1; d < n; d *= 2) {
        offset /= 2;
        __syncthreads();
        if (thid < d) {
            int ai = offset*(2*thid+1)-1;
            int bi = offset*(2*thid+2)-1;
            ai += CONFLICT_FREE_OFFSET(ai);
            bi += CONFLICT_FREE_OFFSET(bi);
            int t = temp_data[ai];
            temp_data[ai] = temp_data[bi];
            temp_data[bi] += t;
        }
    }
    __syncthreads();
}
#endif


// Tagged increment with 5-bit tags.
#define TAG_MASK_5B 0x07ffffff
__device__ inline void incrementTaggedLoc5B(volatile unsigned int *d, unsigned int tag)
{
	unsigned int gi;
	do {
		gi = *d & TAG_MASK_5B; // Read location and remove thread tag.
		gi = (gi+1) | tag; // Increment value and apply own tag,
		*d = gi; // Write.
	} while ( *d != gi ); // While write failed.
}

// 8-bit histogram over 1024-byte range.
__global__ void hist8U_1024_slice(unsigned int *din, unsigned int *dout, int w)
{
	// Need a sub histogram per every 32-thread sub-warp (256/32 = 8).
	volatile __shared__ unsigned int hist[ 256*8 ];

	const int ix = threadIdx.x;
	const unsigned int tag = ix << 27;

	// Initialize histogram data.
	const int warp_i = ix >> 5;
	volatile unsigned int *sub_hist = hist + IMUL(256, warp_i);
	#pragma unroll
	for ( int i=0; i<8; ++i ) {
		hist[ ix + IMUL(256, i) ] = 0;
	}
	
	__syncthreads();

	// Bin my values in the sub-histogram.
	unsigned int d = din[ix];
	incrementTaggedLoc5B( sub_hist + (int)((d>> 0) & 0xff), tag );
	incrementTaggedLoc5B( sub_hist + (int)((d>> 8) & 0xff), tag );
	incrementTaggedLoc5B( sub_hist + (int)((d>>16) & 0xff), tag );
	incrementTaggedLoc5B( sub_hist + (int)((d>>24) & 0xff), tag );

	__syncthreads();

	// Sum up sub histograms.
	// TODO: Make binary summation for large compaction factor.
	hist[ix] &= TAG_MASK_5B;
	#pragma unroll
	for ( int i=1; i<8; ++i ) {
		hist[ix] += hist[ ix + IMUL(256, i) ] & TAG_MASK_5B;
	}

	// Write out results to dout.
	dout[ix] = hist[ix];
}

// Median select using 8-bit histogram over 1024-byte range.
__global__ void median8U_1024_slice(unsigned int *din, unsigned int *dout, int w)
{
	// Need a sub histogram per every 32-thread sub-warp (256/32 = 8).
	volatile __shared__ unsigned int hist[ 256*8 ];
	__shared__ int sum_data[ 512 ];

	const int ix = threadIdx.x;
	const unsigned int tag = ix << 27;

	// Initialize histogram data.
	const int warp_i = ix >> 5;
	volatile unsigned int *sub_hist = hist + IMUL(256, warp_i);
	#pragma unroll
	for ( int i=0; i<8; ++i ) {
		hist[ ix + IMUL(256, i) ] = 0;
	}
	
	__syncthreads();

	// Bin my values in the sub-histogram.
	unsigned int d = din[ix];
	incrementTaggedLoc5B( sub_hist + (int)((d>> 0) & 0xff), tag );
	incrementTaggedLoc5B( sub_hist + (int)((d>> 8) & 0xff), tag );
	incrementTaggedLoc5B( sub_hist + (int)((d>>16) & 0xff), tag );
	incrementTaggedLoc5B( sub_hist + (int)((d>>24) & 0xff), tag );

	__syncthreads();

	// Sum up sub histograms.
	hist[ix] &= TAG_MASK_5B;
	#pragma unroll
	for ( int i=1; i<8; ++i ) {
		hist[ix] += hist[ ix + IMUL(256, i) ] & TAG_MASK_5B;
	}

	// Now perform cumsum operation on hist[0..256].
	if ( threadIdx.x >= 128 )
		return; // Only need the first 128 threads.

	int thid = threadIdx.x;
    int ai = thid;
    int bi = thid + 128;

    // Compute spacing to avoid bank conflicts
    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);

	// Move the histogram over to smaller, non-volatile memory.
	sum_data[ai + bankOffsetA] = hist[ai];
    sum_data[bi + bankOffsetB] = hist[bi];

	scan_inner_loop_int(256, ai, bi, thid, sum_data);

#if 0
    // Write results to global memory
	dout[ai] = sum_data[ai + bankOffsetA] + hist[ai];
    dout[bi] = sum_data[bi + bankOffsetB] + hist[bi];
#else
	// Conduct swap to put elements in proper order.
	const int aout = sum_data[ai + bankOffsetA] + hist[ai];
	const int bout = sum_data[bi + bankOffsetB] + hist[bi];
	__syncthreads();
	sum_data[ai] = aout;
	sum_data[bi] = bout;
	__syncthreads();
#if 0
	// Output real data.
	dout[ai] = sum_data[ai];
    dout[bi] = sum_data[bi];
#endif

	// Find median by binary search using only one thread.
	if ( threadIdx.x == 0 ) {
		int med = 128;
		for ( int d = 64; d >= 1; d /= 2 ) {
			if ( sum_data[med] < 512 )
				med += d;
			else if ( sum_data[med-1] < 512 && sum_data[med] >= 512 )
				break; // Found median.
			else
				med -= d;
		}

		// Write out single result.
		*dout = med;
	}
#endif
}

// Find median of 8-bit data in a 1024-entry segment.
void svlCudaMedian8U_1024(svlCudaLine *ln, svlCudaLine *lout, int w)
{
	if ( w == -1 ) w = ln->w;
	dim3 block( 256 );
	dim3 grid( 1 );

	median8U_1024_slice<<< grid, block >>>((unsigned int*)ln->data, (unsigned int*)lout->data, w);
}

// Compute medians of multiple 32-by-32 windows.
__global__ void median_window8U_32x32_slice(unsigned int *pin, int pitch, int *ixes, unsigned int *medians, int n, bool yx)
{
	// Need a sub histogram per every 32-thread sub-warp (256/32 = 8).
	volatile __shared__ unsigned int hist[ 256*8 ];
	__shared__ int sum_data[ 512 ];

	const int ix = threadIdx.x;
	const unsigned int tag = ix << 27;

	// Initialize histograms to 0.
	const int warp_i = ix >> 5;
	volatile unsigned int *sub_hist = hist + IMUL(256, warp_i);
	#pragma unroll
	for ( int i=0; i<8; ++i ) {
		hist[ ix + IMUL(256, i) ] = 0;
	}

	// Read in my index.
	const int x_in = ixes[ 2*blockIdx.x + ( yx ? 1 : 0 ) ];
	const int y_in = ixes[ 2*blockIdx.x + ( yx ? 0 : 1 ) ];
	__syncthreads();

	// Bin values in the sub-histogram.
	const unsigned int *pinin = UINT_PITCH_GET(pin, pitch, y_in + threadIdx.x/8 );
	unsigned int d = pinin[ x_in/4 + threadIdx.x%8 ];
	const unsigned int x_mod_4 = x_in%4;
	if ( false && threadIdx.x == 0 && x_mod_4 != 0 ) {
		switch (x_mod_4) {
			case 1: incrementTaggedLoc5B( sub_hist + (int)((d>> 8) & 0xff), tag );
			case 2: incrementTaggedLoc5B( sub_hist + (int)((d>>16) & 0xff), tag );
			case 3: incrementTaggedLoc5B( sub_hist + (int)((d>>24) & 0xff), tag );
		}
		d = pinin[ x_in/4 + threadIdx.x%8 + 8 ];
		switch (x_mod_4) {
			case 3: incrementTaggedLoc5B( sub_hist + (int)((d>>16) & 0xff), tag );
			case 2: incrementTaggedLoc5B( sub_hist + (int)((d>> 8) & 0xff), tag );
			case 1: incrementTaggedLoc5B( sub_hist + (int)((d>> 0) & 0xff), tag );
		}
	} else {
		incrementTaggedLoc5B( sub_hist + (int)((d>> 0) & 0xff), tag );
		incrementTaggedLoc5B( sub_hist + (int)((d>> 8) & 0xff), tag );
		incrementTaggedLoc5B( sub_hist + (int)((d>>16) & 0xff), tag );
		incrementTaggedLoc5B( sub_hist + (int)((d>>24) & 0xff), tag );
	}
	__syncthreads();

	// Sum up sub histograms.
	hist[ix] &= TAG_MASK_5B;
	#pragma unroll
	for ( int i=1; i<8; ++i ) {
		hist[ix] += hist[ ix + IMUL(256, i) ] & TAG_MASK_5B;
	}

	// Now perform cumsum operation on hist[0..256].
	if ( threadIdx.x >= 128 )
		return; // Only need the first 128 threads.

	int thid = threadIdx.x;
    int ai = thid;
    int bi = thid + 128;

    // Compute spacing to avoid bank conflicts
    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);

	// Move the histogram over to smaller, non-volatile memory.
	sum_data[ai + bankOffsetA] = hist[ai];
    sum_data[bi + bankOffsetB] = hist[bi];

	scan_inner_loop_int(256, ai, bi, thid, sum_data);

	// Conduct swap to put elements in proper order.
	const int aout = sum_data[ai + bankOffsetA] + hist[ai];
	const int bout = sum_data[bi + bankOffsetB] + hist[bi];
	__syncthreads();
	sum_data[ai] = aout;
	sum_data[bi] = bout;
	__syncthreads();

	// Find median by binary search using only one thread.
	if ( threadIdx.x == 0 ) {
		int med = 128;
		for ( int d = 64; d >= 1; d /= 2 ) {
			if ( sum_data[med] < 512 )
				med += d;
			else if ( sum_data[med-1] < 512 && sum_data[med] >= 512 )
				break; // Found median.
			else
				med -= d;
		}

		// Write out single result.
		medians[blockIdx.x] = med;
	}
}

// Compute medians of multiple 32-by-32 windows.
void svlCudaWindowMedians8U(svlCudaPitch* pin, svlCudaLine *ixes, svlCudaLine *medians, int n, bool yx)
{
	if ( n == -1 ) n = ixes->w / 2;
	dim3 block( 256 );
	dim3 grid( n );
	median_window8U_32x32_slice<<< grid, block >>>((unsigned int*)pin->data, pin->pitch, (int*)ixes->data, (unsigned int*)medians->data, n, yx);
}

// Compute medians of multiple 32-by-32 windows from 32F data.
__global__ void median_window32F_32x32_slice(float *pin, int pitch, int *ixes, float *medians, float gmin, float gmax, int n, bool yx)
{
	// Need a sub histogram per every 32-thread sub-warp (256/32 = 8).
	volatile __shared__ unsigned int hist[ 256*8 ];
	__shared__ int sum_data[ 512 ];

	const int ix = threadIdx.x;
	const unsigned int tag = ix << 27;

	// Initialize histograms to 0.
	const int warp_i = ix >> 5;
	volatile unsigned int *sub_hist = hist + IMUL(256, warp_i);
	#pragma unroll
	for ( int i=0; i<8; ++i ) {
		hist[ ix + IMUL(256, i) ] = 0;
	}

	// Read in my index.
	const int x_in = ixes[ 2*blockIdx.x + ( yx ? 1 : 0 ) ];
	const int y_in = ixes[ 2*blockIdx.x + ( yx ? 0 : 1 ) ];
	__syncthreads();

	// Bin values in the sub-histogram.
	const int ix_y = y_in + threadIdx.x/32;
	const int ix_mod = x_in + threadIdx.x%32;
	const float fac = 255.0f / (gmax-gmin);
#pragma unroll 4
	for ( int i=0; i<4; ++i ) {
		const float *pinf = PITCH_GET(pin, pitch, ix_y + 8*i );
		float gf = pinf[ix_mod];
		gf = fmaxf(gmin, fminf(gmax, gf) ) - gmin;
		incrementTaggedLoc5B( sub_hist + int(fac*gf), tag );
	}
	__syncthreads();

	// Sum up sub histograms.
	hist[ix] &= TAG_MASK_5B;
	#pragma unroll
	for ( int i=1; i<8; ++i ) {
		hist[ix] += hist[ ix + IMUL(256, i) ] & TAG_MASK_5B;
	}

	// Now perform cumsum operation on hist[0..256].
	if ( threadIdx.x >= 128 )
		return; // Only need the first 128 threads.

	int thid = threadIdx.x;
    int ai = thid;
    int bi = thid + 128;

    // Compute spacing to avoid bank conflicts
    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);

	// Move the histogram over to smaller, non-volatile memory.
	sum_data[ai + bankOffsetA] = hist[ai];
    sum_data[bi + bankOffsetB] = hist[bi];

	scan_inner_loop_int(256, ai, bi, thid, sum_data);

	// Conduct swap to put elements in proper order.
	const int aout = sum_data[ai + bankOffsetA] + hist[ai];
	const int bout = sum_data[bi + bankOffsetB] + hist[bi];
	__syncthreads();
	sum_data[ai] = aout;
	sum_data[bi] = bout;
	__syncthreads();

	// Find median by binary search using only one thread.
	if ( threadIdx.x == 0 ) {
		int med = 128;
		for ( int d = 64; d >= 1; d /= 2 ) {
			if ( sum_data[med] < 512 )
				med += d;
			else if ( sum_data[med-1] < 512 && sum_data[med] >= 512 )
				break; // Found median.
			else
				med -= d;
		}

		// Write out single result.
		medians[blockIdx.x] = float(med) / fac + gmin;
	}
}

// Compute medians of multiple 32-by-32 windows in 32F data using global thresholds.
void svlCudaWindowMedians32F(svlCudaPitch* pin, svlCudaLine *ixes, svlCudaLine *medians, float gmin, float gmax, int n, bool yx)
{
	if ( n == -1 ) n = ixes->w / 2;
	dim3 block( 256 );
	dim3 grid( n );
	median_window32F_32x32_slice<<< grid, block >>>(pin->data, pin->pitch, (int*)ixes->data, medians->data, gmin, gmax, n, yx);
}


