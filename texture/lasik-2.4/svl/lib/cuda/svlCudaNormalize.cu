/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2009, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaNormalize.cu
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  Cuda convolution and related functions.
**
*****************************************************************************/

#include "svlCudaCommon.h"


// Window-based normalization.
__global__ void window_normalize_slice(float *din, int din_pitch, float *integral, int integral_pitch, float *integral2, int integral2_pitch, int data_w, int data_h,
									   float patch_mean, float patch_std, int patch_w, int patch_h)
{
	// Load din data into shared cache.
	__shared__ float int_data[1536]; // 1089
	__shared__ float int2_data[1536]; // 1089
	const int data_width = 33;
	const int data_size = IMUL(data_width, blockDim.y);
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.

	// Shared memory index.
	const int ix = IMUL(data_width, threadIdx.y + 1) + threadIdx.x + 1;	// Shared thread memory index.
	// Load on-block data.
	const float *int_in = PITCH_GET(integral, integral_pitch, y);
	int_data[ix] = int_in[x];
	const float *int2_in = PITCH_GET(integral2, integral2_pitch, y);
	int2_data[ix] = int2_in[x];
	// Load rightwards data.
	if ( threadIdx.x < 16 ) {
		if ( x + 16 < data_w ) {
			int_data[ix + 16] = int_in[x + 16];
			int2_data[ix + 16] = int2_in[x + 16];
		} else {
			int_data[ix + 16] = int_in[data_w-1];
			int2_data[ix + 16] = int2_in[data_w-1];
		}
	}
	// Load leftwards column.
	if ( threadIdx.x == 0 ) {
		if ( x > 0 ) {
			int_data[ix - 1] = int_in[x - 1];
			int2_data[ix - 1] = int2_in[x - 1];
		} else {
			int_data[ix - 1] = 0.0f;
			int2_data[ix - 1] = 0.0f;
		}
	}
	// Load upwards row.
	if ( threadIdx.y == 0 ) {
		if ( y > 0 ) {
			const float *int_in_under = PITCH_GET(integral, integral_pitch, y - 1);
			int_data[ ix - data_width ] = int_in_under[x];
			const float *int2_in_under = PITCH_GET(integral2, integral2_pitch, y - 1);
			int2_data[ ix - data_width ] = int2_in_under[x];

			// Load rightwards-upwards data.
			if ( threadIdx.x < 16 ) {
				const bool on_block = x < data_w;
				int_data[ ix - data_width + blockDim.x ] = on_block ? int_in_under[x + 16] : int_in_under[data_w-1];
				int2_data[ ix - data_width + blockDim.x ] = on_block ? int2_in_under[x + 16] : int2_in_under[data_w-1];
			}
			// Load leftwards-upwards dot.
			if ( threadIdx.x == 0 ) {
				if ( x > 0 ) {
					*int_data = int_in_under[x-1];
					*int2_data = int2_in_under[x-1];
				} else {
					*int_data = 0.0f;
					*int2_data = 0.0f;
				}
			}
		} else {
			int_data[ ix - data_width ] = 0.0f;
			int2_data[ ix - data_width ] = 0.0f;
			// Set leftwards-upwards dot.
			if ( threadIdx.x == 0 ) {
				*int_data = 0.0f;
				*int2_data = 0.0f;
			}
		}
	}
	// Load bottomwards and diagonalwards data.
	if ( threadIdx.y < patch_h ) {
		const int rows_add = IMUL(data_width, blockDim.y);
		if ( y + blockDim.y < data_h ) {
			const float *int_in_over = PITCH_GET(integral, integral_pitch, y + blockDim.y);
			int_data[ ix + rows_add ] = int_in_over[x];
			const float *int2_in_over = PITCH_GET(integral2, integral2_pitch, y + blockDim.y);
			int2_data[ ix + rows_add ] = int2_in_over[x];

			// Load diagonalwise data.
			if ( threadIdx.x < 16 ) {
				const bool on_block = x < data_w;
				int_data[ ix + rows_add + blockDim.x ] = on_block ? int_in_over[x + 16] : int_in_over[data_w-1];
				int2_data[ ix + rows_add + blockDim.x ] = on_block ? int2_in_over[x + 16] : int2_in_over[data_w-1];
			}
			// Load leftwards column.
			if ( threadIdx.x == 0 ) {
				if ( x > 0 ) {
					int_data[ix + rows_add - 1] = int_in_over[x - 1];
					int2_data[ix + rows_add - 1] = int2_in_over[x - 1];
				} else {
					int_data[ix + rows_add - 1] = 0.0f;
					int2_data[ix + rows_add - 1] = 0.0f;
				}
			}
		} else {
			// We need not worry about things off the image, since we stop caring about the result at that point.
			// But still zeralize correctly.
			int_data[ ix + rows_add ] = 0.0f;
			int2_data[ ix + rows_add ] = 0.0f;
			// Load diagonalwise data.
			if ( threadIdx.x < 16 ) {
				int_data[ ix + rows_add + 16 ] = 0.0f;
				int2_data[ ix + rows_add + 16 ] = 0.0f;
			}
			// Load leftwards column.
			if ( threadIdx.x == 0 ) {
				int_data[ix + rows_add - 1] = 0.0f;
				int2_data[ix + rows_add - 1] = 0.0f;
			}
		}
	}

	__syncthreads();

	float *inout = PITCH_GET(din, din_pitch, y);
	// Compute window sum and squared sums.
	const float window_sum =
		int_data[ ix + IMUL(data_width, patch_h - 1) + patch_w - 1 ]	// Add on super integral
	- int_data[ ix + IMUL(data_width, patch_h - 1) - 1 ]	// Subtract side integrals
	- int_data[ ix - data_width + patch_w - 1 ]
	+ int_data[ ix - data_width - 1 ];	// Add back doubly subtracted integral
	const float window_sq_sum =
		int2_data[ ix + IMUL(data_width, patch_h - 1) + patch_w - 1 ]	// Add on super integral
	- int2_data[ ix + IMUL(data_width, patch_h - 1) - 1 ]	// Subtract side integrals
	- int2_data[ ix - data_width + patch_w - 1 ]
	+ int2_data[ ix - data_width - 1 ];	// Add back doubly subtracted integral

	//inout[x] = ( inout[x] - patch_mean * window_sum ) / ( sqrt( patch_std * ( window_sq_sum - window_sum*window_sum/float(patch_w*patch_h) ) ) );
	const float subtraction = inout[x] - patch_mean * window_sum;
	const float denominator = window_sq_sum - window_sum * window_sum / float(patch_w*patch_h);
	inout[x] = subtraction / ( patch_std * sqrt( denominator ) );
}

// Perform window-based normalization.
void svlCudaWindowNormalize(svlCudaPitch &pitch_in, const svlCudaPitch &integral, const svlCudaPitch &integral2,
							float patch_mean, float patch_std, int patch_w, int patch_h, int img_w, int img_h)
{
	dim3 block(16, 16);
	//dim3 grid( iDivUp(pitch_in.w, block.x), iDivUp(pitch_in.h, block.y) );
	dim3 grid( iDivUp( ( img_w == -1 ? pitch_in.w : img_w ), block.x), iDivUp( ( img_h == -1 ? pitch_in.h : img_h ), block.y) );
	//printf("svlCudaWindowNormalize :: got (%d,%d) => (%d,%d)\n", img_w, img_h, grid.x, grid.y);

	window_normalize_slice<<< grid, block >>>(pitch_in.data, pitch_in.pitch, integral.data, integral.pitch, integral2.data, integral2.pitch,
		pitch_in.w, pitch_in.h, patch_mean, patch_std, patch_w, patch_h);
}

__global__ void normalize1_slice(float *din, int din_pitch, float *integ, int integ_pitch, float *integ2, int integ2_pitch,
								 int data_w, int data_h, int patch_w, int patch_h, float patch_mean, float patch_inv_std, float N_inv)
{
	// Load din data into shared cache.
	__shared__ float integ_data[ 1536 ];
	__shared__ float integ2_data[ 1536 ];

	const int data_width = 32;
	const int data_size = IMUL(data_width, blockDim.y);
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.

	const int ix = IMUL(data_width, threadIdx.y) + threadIdx.x;		// Shared thread memory index.
	const float *integ_in = PITCH_GET(integ, integ_pitch, y);
	const float *integ2_in = PITCH_GET(integ2, integ2_pitch, y);
	// Load on-block data.
	const bool on_on = x <= data_w;
	integ_data[ix] = on_on ? integ_in[x] : integ_in[data_w];
	integ2_data[ix] = on_on ? integ2_in[x] : integ2_in[data_w];
	// Load rightwards data.
	const bool right_on = on_on && x + blockDim.x <= data_w;
	integ_data[ ix + blockDim.x ] = right_on ? integ_in[ x + blockDim.x ] : integ_in[data_w];
	integ2_data[ ix + blockDim.x ] = right_on ? integ2_in[ x + blockDim.x ] : integ2_in[data_w];
	// Load in upwards data.
	if ( threadIdx.y < patch_h ) {
		const bool up_on = y + blockDim.y <= data_h;
		const float *integ_in_up = up_on
			? PITCH_GET(integ, integ_pitch, y + blockDim.y)
			: PITCH_GET(integ, integ_pitch, data_h);
		const float *integ2_in_up = up_on
			? PITCH_GET(integ2, integ2_pitch, y + blockDim.y)
			: PITCH_GET(integ2, integ2_pitch, data_h);
		// Right in upwards data.
		integ_data[ ix + data_size ] = integ_in_up[x];
		integ2_data[ ix + data_size ] = integ2_in_up[x];
		// Read in updwards-rightwards data.
		integ_data[ ix + data_size + blockDim.x ] = right_on ? integ_in_up[ x + blockDim.x ] : integ_in_up[data_w];
		integ2_data[ ix + data_size + blockDim.x ] = right_on ? integ2_in_up[ x + blockDim.x ] : integ2_in_up[data_w];
	}

	__syncthreads();

	// Compute window statistics.
	const float window_sum =
		integ_data[ ix + IMUL(data_width, patch_h) + patch_w ]	// Add on super integral
	- integ_data[ ix + IMUL(data_width, patch_h) ]	// Subtract side integrals
	- integ_data[ ix + patch_w ]
	+ integ_data[ ix ];	// Add back doubly subtracted integral
	const float window_sq_sum =
		integ2_data[ ix + IMUL(data_width, patch_h) + patch_w ]	// Add on super integral
	- integ2_data[ ix + IMUL(data_width, patch_h) ]	// Subtract side integrals
	- integ2_data[ ix + patch_w ]
	+ integ2_data[ ix ];	// Add back doubly subtracted integral

	// Get data value and normalize.
	float *my_din = PITCH_GET(din, din_pitch, y); // Calculate my row of din.
	const float gf = my_din[x];
	my_din[x] = __fdividef( gf - patch_mean * window_sum, sqrtf( N_inv * ( window_sq_sum - N_inv * window_sum * window_sum ) ) ) * patch_inv_std * N_inv;
}

// Normalize over a window using off-by-one integral images.
void svlCudaWindowNormalize1(svlCudaPitch &pitch_in, const svlCudaPitch &integral, const svlCudaPitch &integral2,
							 float patch_mean, float patch_std, int patch_w, int patch_h, int img_w, int img_h)
{
	dim3 block(16, 16);
	dim3 grid( iDivUp( ( img_w == -1 ? pitch_in.w : img_w ), block.x), iDivUp( ( img_h == -1 ? pitch_in.h : img_h ), block.y) );

	normalize1_slice<<< grid, block >>>(pitch_in.data, pitch_in.pitch, integral.data, integral.pitch, integral2.data, integral2.pitch,
		img_w, img_h, patch_w, patch_h, patch_mean, 1.0f/float(patch_std), 1.0f/float(patch_w*patch_h));
}

// Normalize with off-by-one, dim-by-dim-sub integral images.
__global__ void normalize1_dimsub_slice(float *din, int din_pitch, float *integ, int integ_pitch, float *integ2, int integ2_pitch,
										int data_w, int data_h, int dim, int patch_w, int patch_h, float patch_mean, float patch_inv_std, float N_inv)
{
	// Load din data into shared cache.
	__shared__ float integ_data[ 1536 ];
	__shared__ float integ2_data[ 1536 ];

	const int data_width = 32;
	const int data_size = IMUL(data_width, blockDim.y);
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;	// x index to patch.
	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;	// y index to patch.

	const int ix = IMUL(data_width, threadIdx.y) + threadIdx.x;		// Shared thread memory index.
	const float *integ_in = PITCH_GET(integ, integ_pitch, y);
	const float *integ2_in = PITCH_GET(integ2, integ2_pitch, y);
	// Load on-block data.
	const bool on_on = x <= data_w;
	integ_data[ix] = on_on ? integ_in[x] : integ_in[data_w];
	integ2_data[ix] = on_on ? integ2_in[x] : integ2_in[data_w];
	// Load rightwards data.
	const bool right_on = on_on && x + blockDim.x <= data_w;
	integ_data[ ix + blockDim.x ] = right_on ? integ_in[ x + blockDim.x ] : integ_in[data_w];
	integ2_data[ ix + blockDim.x ] = right_on ? integ2_in[ x + blockDim.x ] : integ2_in[data_w];
	// Load in upwards data.
	if ( threadIdx.y < patch_h ) {
		const bool up_on = y + blockDim.y <= data_h;
		const float *integ_in_up = up_on
			? PITCH_GET(integ, integ_pitch, y + blockDim.y)
			: PITCH_GET(integ, integ_pitch, data_h);
		const float *integ2_in_up = up_on
			? PITCH_GET(integ2, integ2_pitch, y + blockDim.y)
			: PITCH_GET(integ2, integ2_pitch, data_h);
		// Right in upwards data.
		integ_data[ ix + data_size ] = integ_in_up[x];
		integ2_data[ ix + data_size ] = integ2_in_up[x];
		// Read in updwards-rightwards data.
		integ_data[ ix + data_size + blockDim.x ] = right_on ? integ_in_up[ x + blockDim.x ] : integ_in_up[data_w];
		integ2_data[ ix + data_size + blockDim.x ] = right_on ? integ2_in_up[ x + blockDim.x ] : integ2_in_up[data_w];
	}

	// Reciprocal is so slow, so cache these values.
	const int x_upon_dim_dim = IMUL(dim, (x/dim));
	const int y_upon_dim_dim = IMUL(dim, (y/dim));
	// Compute bools for correction conditions.
	const bool on_top = y - y_upon_dim_dim == 0; // True if top values are at the start of the window (and so reference the previous window).
	const bool on_left = x - x_upon_dim_dim == 0; // True if the left values are ... TRIO.
	const bool right_over = (x - 1)/dim != (x + patch_w - 1)/dim; // True if the x values are in different sub windows.
	const bool up_over = (y - 1)/dim != (y + patch_h - 1)/dim; // True if the y values are in different sub windows.
	// And offsets to intermediate values.
	int right_over_x_add = x_upon_dim_dim + dim - x,
		up_over_y_add    = IMUL(data_width, (y_upon_dim_dim + dim - y));

	__syncthreads();

	// Compute window statistics.
	patch_h *= data_width;

	// Start with diagonal entries, since these are the only values that will have to be added.
	float window_sum = integ_data[ ix + patch_h + patch_w ],
		window_sq_sum = integ2_data[ ix + patch_h + patch_w ];
	if ( on_top ) {
		// Don't need sum or sum_right (set to zero).
		if ( !on_left ) {
			// As we're not on left, have to subtract non-zero sum_up value.
			window_sum -= integ_data[ ix + patch_h ]; // -= sum_up;
			window_sq_sum -= integ2_data[ ix + patch_h ];
			// Check if right_over since up_over is impossible (with on_top);
			if ( right_over ) {
				// Add in intermediate value with sum_right_up.
				window_sum += integ_data[ ix + patch_h + right_over_x_add ];
				window_sq_sum += integ2_data[ ix + patch_h + right_over_x_add ];
			}
		}
		// else, on_top and on_left, so done with sum_right_up.
	}
	else if ( on_left ) {
		// Only on_left, not on_top, so subtract sum_right;
		window_sum -= integ_data[ ix + patch_w ]; // -= sum_right;
		window_sq_sum -= integ2_data[ ix + patch_w ];
		// Check if up_over is true (right_over ruled out by on_left).
		if ( up_over ) {
			// Add in intermediate value with sum_right_up.
			window_sum += integ_data[ ix + up_over_y_add + patch_w ];
			window_sq_sum += integ2_data[ ix + up_over_y_add + patch_w ];
		}
	} else {
		// No guarantees, so add in all other values.
		window_sum += integ_data[ ix ] - integ_data[ ix + patch_w ] - integ_data[ ix + patch_h ]; // + sum - sum_right - sum_up
		window_sq_sum += integ2_data[ ix ] - integ2_data[ ix + patch_w ] - integ2_data[ ix + patch_h ]; // + sum - sum_right - sum_up
		// Check all other conditions for adding corrections.
		if ( right_over ) {
			window_sum -= integ_data[ ix + right_over_x_add ];
			window_sq_sum -= integ2_data[ ix + right_over_x_add ];
			if ( !up_over ) {
				window_sum += integ_data[ ix + patch_h + right_over_x_add ];
				window_sq_sum += integ2_data[ ix + patch_h + right_over_x_add ];
			}
		}
		if ( up_over ) {
			window_sum -= integ_data[ ix + up_over_y_add ];
			window_sq_sum -= integ2_data[ ix + up_over_y_add ];
			if ( !right_over ) {
				window_sum += integ_data[ ix + up_over_y_add + patch_w ];
				window_sq_sum += integ2_data[ ix + up_over_y_add + patch_w ];
			}
		}
		if ( right_over && up_over ) {
			window_sum += integ_data[ ix + up_over_y_add + right_over_x_add ]
			+ integ_data[ ix + up_over_y_add + patch_w ]
			+ integ_data[ ix + patch_h + right_over_x_add ];
			window_sq_sum += integ2_data[ ix + up_over_y_add + right_over_x_add ]
			+ integ2_data[ ix + up_over_y_add + patch_w ]
			+ integ2_data[ ix + patch_h + right_over_x_add ];
		}
	}

	// Get data value and normalize.
	float *my_din = PITCH_GET(din, din_pitch, y); // Calculate my row of din.
	const float gf = my_din[x];
	//my_din[x] = window_sq_sum;
	my_din[x] = __fdividef( gf - patch_mean * window_sum, sqrtf( N_inv * ( window_sq_sum - N_inv * window_sum * window_sum ) ) ) * patch_inv_std * N_inv;
}

// Normalize over a window using off-by-one, dim-by-dim-sub-window integral images.
void svlCudaWindowNormalize1DimSub(svlCudaPitch &pitch_in, const svlCudaPitch &integral, const svlCudaPitch &integral2,
								   float patch_mean, float patch_std, int patch_w, int patch_h, int img_w, int img_h, int dim)
{
	dim3 block(16, 16);
	dim3 grid( iDivUp( ( img_w == -1 ? pitch_in.w : img_w ), block.x), iDivUp( ( img_h == -1 ? pitch_in.h : img_h ), block.y) );
	/*printf("grid: %d,%d\n", grid.x, grid.y);
	printf("img w,h: %d,%d; dim: %d\n", img_w, img_h, dim);*/
	normalize1_dimsub_slice<<< grid, block >>>(pitch_in.data, pitch_in.pitch, integral.data, integral.pitch, integral2.data, integral2.pitch,
		img_w, img_h, dim, patch_w, patch_h, patch_mean, 1.0f/float(patch_std), 1.0f/float(patch_w*patch_h));
}

// Compute a patch x normalization over row integral images.
__global__ void row_normalize_slice(float *din, int din_pitch, float *dout, int dout_pitch, int img_w, int patch_w, int dim)
{
	__shared__ float data[512];

	const int x = IMUL(dim, blockIdx.x) + threadIdx.x;	// x index to patch.
	const bool off_img = x > img_w;
#if 1
	if ( off_img )
		return;

	const float *dinf = PITCH_GET(din, din_pitch, blockIdx.y);
	data[threadIdx.x] = dinf[x];

	if ( threadIdx.x >= dim )
		return;
#else
	// Use this for correct sums at the edges of the image.
	const float *dinf = PITCH_GET(din, din_pitch, blockIdx.y);
	data[threadIdx.x] = off_img ? dinf[img_w] : dinf[x];

	if ( off_img || threadIdx.x >= dim )
		return;
#endif

	__syncthreads();

	// This will explode at the edges of the image, but we don't ask for those values anyway.
	float gf = data[ threadIdx.x + patch_w ]; //x + patch_w <= img_w ? data[ threadIdx.x + patch_w ] : dinf[blockDim.x];
	if ( threadIdx.x > 0 ) {
		// Subtract minus term.
		gf -= data[threadIdx.x];

		if ( threadIdx.x > dim - patch_w ) {
			// Normalize by adding in middle term.
			gf += data[dim];
		}
	}
	float *doutf = PITCH_GET(dout, dout_pitch, blockIdx.y);
	doutf[x] = gf;
}

// Compute window normalization using row-based integral images.
void svlCudaWindowNormalizeRow(svlCudaPitch *pin, svlCudaPitch *pout, int patch_w, int img_w, int img_h, int dim)
{
	//dim3 block( iAlignUp(img_w, 16) );
	dim3 block( dim + iAlignUp(patch_w,16) );
	dim3 grid( iDivUp(img_w, dim), img_h );
	row_normalize_slice<<< grid, block >>>(pin->data, pin->pitch, pout->data, pout->pitch, img_w, patch_w, dim);
}



