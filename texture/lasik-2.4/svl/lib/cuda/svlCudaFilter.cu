/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2009, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaFilter.cu
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  Cuda filtering.
**
*****************************************************************************/

#include "svlCudaCommon.h"


// Do column components of Sobel filter.
__global__ void sobel_col_slice(const float *din, int din_pitch, float *dout0, int dout0_pitch, float *dout2, int dout2_pitch, int img_h, int y_stride)
{
	__shared__ float data[ 288 ];
	const int data_width = 16;
	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;
	const int y = IMUL(y_stride, blockIdx.y) + threadIdx.y - 1;
	const int ix = IMUL(data_width, threadIdx.y) + threadIdx.x;

	// Read in shared data.
	const float *dinf = PITCH_GET(din, din_pitch, y);
	const bool y_off = y < 0 || y >= img_h;
	data[ix] = y_off ? 0.0f : dinf[x];

	if ( y_off || threadIdx.y == 0 || threadIdx.y == blockDim.y-1 )
		return; // Exit if off of image or in first or last row.

	__syncthreads();

	// Write out results.
	float *doutf0 = PITCH_GET(dout0, dout0_pitch, y);
	doutf0[x] = data[ ix - data_width ] - data[ ix + data_width ];
	float *doutf2 = PITCH_GET(dout2, dout2_pitch, y);
	doutf2[x] = data[ ix - data_width ] + 2.0f * data[ix] + data[ ix + data_width ];
}

// Do row component of Sobel filter.
__global__ void sobel_row_slice(const float *din, int din_pitch, float *dout, int dout_pitch, int img_w, int row_stride, bool do_101)
{
	__shared__ float data[ 544 ];
	const int x = IMUL(row_stride, blockIdx.x) + threadIdx.x - 16;
	const int ix = threadIdx.x;

	// Load shared data.
	const float *dinf = PITCH_GET(din, din_pitch, blockIdx.y);
	const bool is_off = x < 0 || x >= img_w;
	data[ix] = is_off ? 0.0f : dinf[x];

	if ( is_off || threadIdx.x < 16 || threadIdx.x >= blockDim.x - 16 )
		return;

	__syncthreads();

	// Calculate and output result.
	float *doutf = PITCH_GET(dout, dout_pitch, blockIdx.y);
	//doutf[x] = -2.3f;
	if ( do_101 ) {
		doutf[x] = data[ ix - 1 ] - data[ ix + 1 ];
	} else {
		doutf[x] = data[ ix - 1 ] + 2.0f * data[ix] + data[ ix + 1 ];
	}
}

// Sobel convolution
void svlCudaSobel(const svlCudaPitch *pin, svlCudaPitch *edge, svlCudaPitch *temp1, svlCudaPitch *temp2, int img_w, int img_h)
{
	if ( img_w == -1 ) img_w = pin->w;
	if ( img_h == -1 ) img_h = pin->h;

	// Column pass.
	int y_stride = 16;
	dim3 col_block( 16, 18 );
	dim3 col_grid( iDivUp(img_w, col_block.x), iDivUp(img_h, y_stride) );
	/*printf("grid: %d,%d\n", col_grid.x, col_grid.y);
	printf("block: %d,%d\n", col_block.x, col_block.y);*/
	// Col [1 0 -1] -> temp1; col [1 2 1] -> temp2.
	sobel_col_slice<<< col_grid, col_block >>>(pin->data, pin->pitch, temp1->data, temp1->pitch, temp2->data, temp2->pitch, img_h, y_stride);

	// Row pass.
	int x_stride = iAlignUp(img_w, 16);
	if ( img_w > 480 ) {
		x_stride = iAlignUp(img_w / iDivUp(img_w, 480), 16);
	}
	dim3 row_block( x_stride + 32 );
	dim3 row_grid( iDivUp(img_w, x_stride), img_h );
	/*printf("grid: %d,%d\n", row_grid.x, row_grid.y);
	printf("block: %d,%d\n", row_block.x, row_block.y);*/
	// Col [1 0 -1] (temp1) * row [1 2 1] -> edge
	sobel_row_slice<<< row_grid, row_block >>>(temp1->data, temp1->pitch, edge->data, edge->pitch, img_w, x_stride, false);
	// Col [1 2 1] (temp2) * row [1 0 -1] -> temp1
	sobel_row_slice<<< row_grid, row_block >>>(temp2->data, temp2->pitch, temp1->data, temp1->pitch, img_w, x_stride, true);

	// Add in quadrature ( edge <- (edge, temp1) ).
	svlCudaAddQuadrature(edge, temp1, img_w, img_h);
}



// Resize a pitch in width by a factor.
__global__ void resize_row_slice(const float *Pin, int ppitch, float *Rout, int rpitch, float fac, int P_w, int R_w)
{
	__shared__ float data[1024];

	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;
	const int y = blockIdx.y;
	const int ix = threadIdx.x;
	const int read_base = int(fac*float(IMUL(blockDim.x, blockIdx.x)));

	// Read in data.
	// How many tiles of blockDim size we'll have to read in to the right.
	const int max_ix = ( ceil(fac * float( min(R_w, blockDim.x) )) + blockDim.x ) / blockDim.x;
	const float *Pf = PITCH_GET(Pin, ppitch, y);
	for ( int i=0; i<max_ix; ++i ) {
		const int read_x = read_base + ix + IMUL(blockDim.x, i);
		data[ ix + IMUL(blockDim.x, i) ] = read_x < P_w ? Pf[read_x] : 0.0f;
	}
	// TODO paulb: Make this read in more conservatively.
	if ( x >= R_w )
		return;
	__syncthreads();

	// Compute pixel convolution bounds.
	float ifl = fac*float(x) - float(read_base), jfl = fac*float(x+1) - float(read_base);
	int ii = int(ifl), jj = int(jfl);
	float gf, w = 0.0f;
	// Accumulate result: lead pixel
	float res = data[ii] * ( gf = (floorf(ifl+1.0f)-ifl) );
	w += gf; // Accumulate weight.
	// Middle, whole pixels.
	for ( int i=ii+1; i<jj; ++i )
		res += data[ i ];
	w += float(jj-ii-1);
	// End pixel.
	res += data[jj] * ( gf = (jfl-floorf(jfl)) );
	w += gf;

	// Output.
	float *Rf = PITCH_GET(Rout, rpitch, y);
	Rf[x] = res/w;
}

//// Resize a pitch in width by a factor.
//__global__ void resize_col_slice(const float *Pin, int ppitch, float *Rout, int rpitch, float fac, int P_h, int P_w, int R_h)
//{
//	__shared__ float data[10240];
//
//	const int x = IMUL(blockDim.x, blockIdx.x) + threadIdx.x;
//	const int on_x = x < P_w;
//	const int y = IMUL(blockDim.y, blockIdx.y) + threadIdx.y;
//	const int ix = 16*threadIdx.y + threadIdx.x;
//	const int read_base = int(fac*float(IMUL(blockDim.y, blockIdx.y)));
//	const int data_size = 16*blockDim.y;
//
//	// Read in data.
//	// How many tiles of blockDim size we'll have to read in to the right.
//	int max_ix = ( ceil(fac * float( min(R_h, blockDim.y) )) + blockDim.y ) / blockDim.y;
//	for ( int i=0; i<max_ix; ++i ) {
//		const float *Pf = PITCH_GET(Pin, ppitch, y
//			);
//		const int read_x = read_base + ix + IMUL(blockDim.x, i);
//		data[ ix + IMUL(data_size, i) ] = on_x ? Pf[read_x] : 0.0f;
//	}
//	// TODO paulb: Make this read in more conservatively.
//	if ( x >= R_w )
//		return;
//	__syncthreads();
//
//	// Compute pixel convolution bounds.
//	float ifl = fac*float(x) - float(read_base), jfl = fac*float(x+1) - float(read_base);
//	int ii = int(ifl), jj = int(jfl);
//	float gf, w = 0.0f;
//	// Accumulate result: lead pixel
//	float res = data[ii] * ( gf = (floorf(ifl+1.0f)-ifl) );
//	w += gf; // Accumulate weight.
//	// Middle, whole pixels.
//	for ( int i=ii+1; i<jj; ++i )
//		res += data[ i ];
//	w += float(jj-ii-1);
//	// End pixel.
//	res += data[jj] * ( gf = (jfl-floorf(jfl)) );
//	w += gf;
//
//	// Output.
//	float *Rf = PITCH_GET(Rout, rpitch, y);
//	Rf[x] = res/w;
//}

// Resize a pitch.
void svlCudaResize(const svlCudaPitch *pin, svlCudaPitch *pout, int new_w, int new_h, svlCudaPitch *temp1, svlCudaPitch *temp2, int img_w, int img_h)
{
	if ( img_w == -1 ) img_w = pin->w;
	if ( img_h == -1 ) img_h = pin->h;
	const bool resize_w = new_w != img_w; // Do row-wise pass to resize in width.
	const bool resize_h = new_h != img_h; // Do col-wise pass to resize in height.

	if ( !resize_w && !resize_h ) {
		// Copy pin to pout.
		svlCudaPitchToPitch(pout, pin, img_w, img_h);
		return; // That was easy.
	}
#if 1
	const svlCudaPitch *tin = pin;
	if ( resize_w ) {
		// Resize in width from pin -> pout.
		dim3 block( 256, 1 );
		dim3 grid( iDivUp(new_w, block.x), img_h );
		resize_row_slice<<< grid, block >>>(pin->data, pin->pitch, pout->data, pout->pitch, float(img_w)/float(new_w), img_w, new_w);

		if ( resize_h )
			tin = pout; // Set input to pout for next step.
	}
	if ( resize_h ) {
		// Transpose input image: tin -> temp1.
		svlCudaTranspose(tin, temp1, new_w, img_h); // temp1 now holds img_h-by-new_w image.
		// Resize in width -> temp2.
		dim3 block( 256, 1 );
		dim3 grid( iDivUp(new_h, block.x), new_w );
		resize_row_slice<<< grid, block >>>(temp1->data, temp1->pitch, temp2->data, temp2->pitch, float(img_h)/float(new_h), img_h, new_h);
		// Transpose to pout.
		svlCudaTranspose(temp2, pout, new_h, new_w); // pout now holds new_w-by-new_h image.
	}
#elif 0
	const svlCudaPitch *tin = pin;
	if ( resize_w ) {
		// Resize in width from pin -> pout.
		dim3 block( 256, 1 );
		dim3 grid( iDivUp(new_w, block.x), img_h );
		resize_row_slice<<< grid, block >>>(pin->data, pin->pitch, temp1->data, temp1->pitch, float(img_w)/float(new_w), img_w, new_w);

		if ( resize_h )
			tin = temp1; // Set input to pout for next step.
	}
	if ( resize_w ) {
		// Resize in width from pin -> pout.
		dim3 block( 16, 32 );
		dim3 grid( iDivUp(new_w, block.x), iDivUp(new_h, block.y) );
		resize_col_slice<<< grid, block >>>(tin->data, tin->pitch, pout->data, pout->pitch, float(img_w)/float(new_w), img_w, new_w);
	}
#endif
}
