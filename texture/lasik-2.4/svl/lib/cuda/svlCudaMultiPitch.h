/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaMultiPitch.h
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  A multi-channel pitch with a base size and slightly different sizes.
**
*****************************************************************************/


#pragma once

#ifndef __SVL_CUDA_MULTI_PITCH_H
#define __SVL_CUDA_MULTI_PITCH_H

#include <iostream>
#include <vector>
#include <map>

#include "svlCudaCommon.h"
#include "svlCudaMemHandler.h"

using namespace std;

typedef pair<int,int> PII;
typedef map<pair<int,int>, vector<svlCudaPitch*>*> svlCudaMultiPitchMap;

class svlCudaMultiPitch {
protected:
	int _w, _h;
	size_t _num_pitches;
	svlCudaMultiPitchMap _pitchMap;

public:
	svlCudaMultiPitch();
	svlCudaMultiPitch(int w, int h, int n = 1);
	virtual ~svlCudaMultiPitch();

	// Set base size.
	void setImageSize(int w, int h);
	inline void setImageSize(const IplImage *img) {
		setImageSize(img->width, img->height);
	}
	
	// Allocate different-sized pitch. Options to use a name and enumerate them ("name 1", "name 2", etc.).
	void alloc(int dw, int dh, int n = 1, const char *name = NULL, bool enumerate = false);
	// Clear memory.
	void clear();
	void clear(int dw, int dh);
	void clearSize(int w, int h);
	
	// Status; comparators.
	inline bool empty() { return _num_pitches == 0; }
	inline bool matchesSize(int w, int h) { return w == _w && h == _h; }
	inline bool matchesSize(const IplImage *img) { return img->width == _w && img->height == _h; }
	inline bool matchesSize(CvSize &sz) { return _w == sz.width && _h == sz.height; }
	// Whether the MultiPitch has size larger than or equal to what is requested.
	inline bool accommodatesSize(int w, int h) { return _w >= w && _h >= h; }

	// Accessors.
	inline int getWidth() { return _w; }
	inline int getHeight() { return _h; }

	// Get count of pitches in various ways.
	inline size_t getNumPitches() { return _num_pitches; }
	inline size_t getNumPitches(int dw, int dh) {
		vector<svlCudaPitch*> *vec = _pitchMap[PII(dw,dh)];
		return !vec ? 0 : vec->size();
	}
	inline size_t getNumPitchesOfSize(int w, int h) {
		vector<svlCudaPitch*> *vec = _pitchMap[PII(w - _w,h - _h)];
		return !vec ? 0 : vec->size();
	}

	// Get all pitches of a certain size as a vector.
	inline vector<svlCudaPitch*>* getAll(int dw, int dh) {
		return _pitchMap[PII(dw,dh)];
	}
	// Get at least n pitches of a minimum size. Return false if not enough pitches.
	//inline bool getMinSizeAtLeast(int dw, int dh, int n, vector<svlCudaPitch*> &pitches);
	// Get a single indexed pitch. Default returns first base-size pitch.
	inline svlCudaPitch* get(int dw = 0, int dh = 0, int i = 0) {
		vector<svlCudaPitch*> *vec = _pitchMap[PII(dw,dh)];
		return !vec || i >= int(vec->size()) ? NULL : (*vec)[i];
	}

	// Get key values as offsets or absolute sizes.
	void getKeys(vector<PII> &ret);
	void getKeySizes(vector<PII> &ret);
};

#endif


