/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaMemTimer.h
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  Allows precise timing of Cuda events through Cuda API.
**
*****************************************************************************/

#pragma once

#ifndef __SVL_CUDA_TIMER_H
#define __SVL_CUDA_TIMER_H

#include <iostream>
#include <vector>
#include <map>
#include <stack>
#include <algorithm>
#include <time.h>

#include "svlCudaCommon.h"
using namespace std;

//#define DOES_NOT_WORK

class svlCudaTimer {
protected:
	static bool _enabled, _inited;
	static stack<const char*> _names;
	static const char *_lastName;
	static map<const char*,vector<float>*> _elapseds;
	static vector<clock_t> times;

public:
	svlCudaTimer() {}
	virtual ~svlCudaTimer();

	// Enabled accessor and setter.
	inline static bool enabled() { return _enabled; }
	static void setEnabled(bool enabled);

	// Adding and removing names.
	inline static void push(const char *name) {
		if ( _enabled ) _names.push(_lastName = name);
	}
	inline static void pop() {
		if ( _enabled ) _names.pop();
	}

	// Save a benchmark time.
	static void add_time(float e);
	// Tic and toc functions.
	inline static void tic() {
		if ( _enabled ) svlCudaTimerTic();
	}
	inline static void toc() {
		if ( _enabled ) add_time(svlCudaTimerToc());
	}

	// Clearing all times.
	inline static void clear() {
		if ( _enabled )
			for ( map<const char*,vector<float>*>::iterator it=_elapseds.begin(); it!=_elapseds.end(); ++it )
				if ( it->second ) {
					delete it->second;
					it->second = NULL;
				}
	}
	// Clear last set of times.
	inline static void clearLast() {
		if ( _enabled && _lastName && _elapseds[_lastName] ) {
			delete _elapseds[_lastName];
			_elapseds[_lastName] = NULL;
		}
	}
	// Clear a certain set of times.
	inline static void clear(const char *name) {
		if ( _enabled && name && _elapseds[name] ) {
			delete _elapseds[name];
			_elapseds[name] = NULL;
		}
	}

	// Get last names or times out.
	inline static const char* getLastName() { return _enabled ? _lastName : NULL; }
	inline static vector<float>* getLastTimes() { return _enabled && _lastName ? _elapseds[_lastName] : NULL; }
	inline static vector<float>* getTimes(const char *name) { return _enabled ? _elapseds[name] : NULL; }

	// Return mean times.
	static float getMean(const char *name) {
		if ( !_enabled || !name || !_elapseds[name] || _elapseds[name]->size() == 0 )
			return -1.0f;
		float ret = 0.0f;
		vector<float> *vec = _elapseds[name];
		for ( vector<float>::const_iterator it=vec->begin(); it!=vec->end(); ++it )
			ret += *it;
		return ret / float(vec->size());
	}
	inline static float getLastMean() {
		return getMean(_lastName);	
	}

	// Return median times.
	static float getMedian(const char *name) {
		if ( !_enabled || !name || !_elapseds[name] || _elapseds[name]->size() == 0 )
			return -1.0f;
		vector<float> *vec = _elapseds[name];
		// TODO paulb: This destroys the order of vec. Use a non-destructive version instead?
		nth_element(vec->begin(), vec->begin() + (vec->size()/2), vec->end());
		return (*vec)[vec->size()/2];
	}
	inline static float getLastMedian() {
		return getMedian(_lastName);
	}
	inline static float getLastMedianClear() {
		float ret = getMedian(_lastName);
		clearLast();
		return ret;
	}
};

#endif


