/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaSWD.h
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**  Defines a multi-channel interface for sliding-window boosted-classifier 
**  object detectors using Cuda for feature extraction and decision tree
**  evaluation.
**
*****************************************************************************/

#pragma once

#include <cassert>
#include <iostream>
#include <vector>
#include <map>
#include <string>

#include "cv.h"
#include "cxcore.h"
#include "ml.h"

#include "svlVision.h"
#include "svlBase.h"
#include "svlCudaCommon.h"
#include "svlCudaConvolution.h"
#include "svlCudaDecisionTreeSet.h"
#include "svlCudaMemHandler.h"
#include "svlCudaMath.h"
#include "svlCudaMultiPitch.h"
#include "svlCudaPatchDictionary.h"
#include "svlFinalPatchDictionary.h"

using namespace std;

// svlCudaSWD ---------------------------------------------------

//typedef vector<vector<double> > Tsamples;

class svlCudaSWD : public svlSlidingWindowDetector {
protected:
	// Explicit Cuda version of extractor.
	svlCudaPatchDictionary *_cpdExtractor;
	// TODO paulb: Owns?

	// Cuda decision tree.
	svlCudaDecisionTreeSet *_dtree;

	// Device memory.
	svlCudaMultiPitch _MP;

public:
	svlCudaSWD(const char *name = NULL, int w = 32, int h = 32);
	svlCudaSWD(const svlCudaSWD& c);
	// Does not make deep copies; does not free memory later.
	svlCudaSWD(const svlBinaryClassifier *c, const svlFeatureExtractor *e);
	void init();
	virtual ~svlCudaSWD();

	// Run classification/feature extraction.
	virtual void classifyImage(const vector<IplImage*>& imagesin,
		svlObject2dFrame *objects,
		svlFeatureVectors *vecs = NULL);
	// float (actual) version
	virtual void classifyImageF(const vector<IplImage*>& imagesin,
		svlObject2dFrame *objects,
		svlFeatureVectorsF *vecs = NULL);

	// Eevaluate classifier at base scale and given locations.
	virtual void classifyImage(const vector<IplImage *>& images,
		const vector<CvPoint>& locations,
		svlObject2dFrame *objects,
		svlFeatureVectors *vecs = NULL,
		bool sparse = false);
	// float (actual) version
	virtual void classifyImageF(const vector<IplImage *>& images,
		const vector<CvPoint>& locations,
		svlObject2dFrame *objects,
		svlFeatureVectorsF *vecs = NULL,
		bool sparse = false);

	// Create interesting window locations.
	void createInterestingLocations(const svlCudaPitch *img, svlCudaMultiPitch &mp, int img_w, int img_h, vector<CvPoint>& locations);

	// Pre-allocates device memory of a certain size.
	void presetImageSize(CvSize &sz);
	// Clears device memory and accepts all sizes.
	void clearImageSize();
	// Returns true is image size is set; fills in pointer value if so and pointer is not NULL.
	CvSize getPresetImageSize();

	// does free mamory later
	bool load(const char *extractorFilename, const char *modelFilename = NULL, const char *pruningFilename = NULL);

};


