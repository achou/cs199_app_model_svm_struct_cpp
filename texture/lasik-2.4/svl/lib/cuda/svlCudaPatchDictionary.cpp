/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaPatchDictionary.cpp
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
**
*****************************************************************************/

#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <sys/types.h>
#include <limits>

#include "cxcore.h"
#include "svlCudaConvolution.h"
#include "svlCudaMath.h"
#include "svlCudaMemHandler.h"
#include "svlCudaPatchDictionary.h"

#if defined(_WIN32)||defined(WIN32)||defined(__WIN32__)
#define _CRT_SECURE_NO_DEPRECATE
#include "win32/dirent.h"
#else
#include <dirent.h>
#endif

extern svlCudaMemHandler *cm;

svlCudaPatchDictionary::svlCudaPatchDictionary(unsigned width, unsigned height)
: svlPatchDictionary(width, height)
{
	init();
}

svlCudaPatchDictionary::svlCudaPatchDictionary(const svlFeatureExtractor *d)
: svlPatchDictionary( *dynamic_cast<const svlPatchDictionary*>(d) )
{
	init();
}

void svlCudaPatchDictionary::init()
{
	_patches_cached = false;
	_cachedLine = NULL;
	_ixline = NULL;
	_locline = NULL;
	_facline = NULL;
}

svlCudaPatchDictionary::~svlCudaPatchDictionary()
{
	if ( _cachedLine )
		cm->Free(_cachedLine);
	if ( _ixline )
		cm->Free(_ixline);
	if ( _locline )
		cm->Free(_locline);
	if ( _facline )
		cm->Free(_facline);
}

// Pad all the patches in the dictionary for device convolution, then send to GPU.
bool svlCudaPatchDictionary::padAndCache()
{
	// Manage memory.
	if ( _cachedLine != NULL && _cachedLine->data != NULL )
		cm->freeLine(_cachedLine);
	_cachedPatches.clear();
	_cachedPatches.reserve(_entries.size());

	// Calculate needed line size (sum of linear, padded lengths)
	vector<int> paddedSizes;
	paddedSizes.reserve(_entries.size());
	int total_sz = 0, max_chan = 0, max_width;
	pair<int,int> pad_wh;
	const IplImage *img;
	for ( unsigned i=0; i<_entries.size(); ++i ) {
		pad_wh = svlCudaConvolution::paddedPatchSize( img = _entries[i]->getTemplate() );
		max_width = max( max_width, img->width );
		max_chan = max( max_chan, _entries[i]->getValidChannel() );

		_cachedPatches.push_back(CachedPatch());
		_cachedPatches.back().w = pad_wh.first;
		_cachedPatches.back().h = pad_wh.second;
		_cachedPatches.back().N = _entries[i]->getTemplateSize(); // Original numel.

		// Calculate patch statistics.
		if ( img->depth != IPL_DEPTH_32F ) {
			SVL_LOG(SVL_LOG_MESSAGE, "Unexpected svlPatch depth: " << img->depth);
		} else {
			float gf, patchSum = 0.0, patchNorm = 0.0;
			for ( int y=0; y<img->height; ++y ) {
				for ( int x=0; x<img->width; ++x ) {
					patchSum += gf = CV_IMAGE_ELEM(img, float, y, x);
					patchNorm += gf * gf;
				}
			}
			_cachedPatches.back().mean = patchSum;
			_cachedPatches.back().std = patchNorm;
		}

		paddedSizes.push_back( pad_wh.first * pad_wh.second );
		total_sz += paddedSizes.back();
	}
	++max_chan;

	// Allocate memory.
	float *patchData = (float*)malloc(sizeof(float)*total_sz);
	float *ptr = patchData;
	// Pad patches.
	vector<int>::const_iterator szs = paddedSizes.begin();
	for ( unsigned i=0; i<_entries.size(); ++i ) {
		svlCudaConvolution::padPatchOnly( _entries[i]->getTemplate(), ptr );
		ptr += *szs; // Advance pointer by padded size.
		++szs;
	}

	// Allocate device meory.
	_cachedLine = cm->allocLine(total_sz, "patch cache");
	// Copy over to GPU.
	cm->lineToGPU(_cachedLine, patchData, total_sz);

	// Free host-side memory.
	free(patchData);

	// Create individual svlCudaLine pointers.
	float *dev_ptr = _cachedLine->data;
	for ( unsigned i=0; i<_cachedPatches.size(); ++i ) {
		// Patch line.
		_cachedPatches[i].patch = new svlCudaLine();
		_cachedPatches[i].patch->w = paddedSizes[i];
		_cachedPatches[i].patch->data = dev_ptr;
		dev_ptr += paddedSizes[i];
#if 0 // If a same-sized entry for the mask is needed.
		// Mask line.
		_cachedPatches[i].mask = new svlCudaLine();
		_cachedPatches[i].mask->w = paddedSizes[i];
		_cachedPatches[i].mask->data = dev_ptr;
		dev_ptr += paddedSizes[i];
#else
		_cachedPatches[i].mask = NULL; // Do not use masks.
#endif
	}
	// Clear this flag.
	_patches_cached = true;

	// Bin the patches by template width and channel.
	_patchBins.clear();
	for ( int w=0; w<=max_width; ++w ) {
		_patchBins.push_back(vector<vector<int> >());
		for ( int chan=0; chan<max_chan; ++chan )
			_patchBins.back().push_back(vector<int>());
	}
	for ( unsigned i=0; i<_entries.size(); ++i ) {
		_patchBins[ _entries[i]->getTemplateWidth() ][ _entries[i]->getValidChannel() ].push_back(i);
	}

	return true;
}

// Allocate pitch memory.
void svlCudaPatchDictionary::allocPitches(svlCudaMultiPitch &mp)
{
	// Need 3 same-size pitches for the image, 'same'-style convolution, and factors.
	if ( mp.getNumPitches(0,0) < 3 ) {
		mp.alloc(0, 0, 3-mp.getNumPitches(0,0));
	}

	// Need 7 (+1,0) pitches: two for row-integral images; four temporaries; one response image.
	if ( mp.getNumPitches(1,0) < 7 ) {
		mp.alloc(1, 0, 7-mp.getNumPitches(1,0));
	}
	// Zeralize the first three for use as the factor and integral images.
	for ( int i=0; i<3; ++i )
		cm->pitchSet( mp.get(1,0, i), 0 );
}

// Run feature extraction with all patches.
void svlCudaPatchDictionary::extract(const vector<IplImage*> &scaledImages,
									 const vector<CvPoint> &locations,
									 svlCudaMultiPitch &MP,
									 svlCudaFeatureLine &features,
									 bool cmax_opt, int cached_image)
{
	if ( !_patches_cached ) {
		SVL_LOG(SVL_LOG_FATAL, "Patches not cached on device"); return;
	}
	if ( MP.getNumPitches(0,0) < 3 || MP.getNumPitches(1,0) < 7 ) {
		SVL_LOG(SVL_LOG_FATAL, "Require at least 3 (0,0) pitches and 7 (+1,0) pitches"); return;
	}
	if ( locations.empty() )
		return;

	// Profiler handles and other variables.
	const int medh = svlCodeProfiler::getHandle("CPD_extract :: medians");
	const int inner_cuda = svlCodeProfiler::getHandle("CPD_extract :: CUDA inner loop");
	int n_locs = int(locations.size()), scaled_w = scaledImages[0]->width, scaled_h = scaledImages[0]->height;
	int dim = 256;	// Integration dimension (must be power of 2).
	bool yx = true;	// Whether CvPoints are stored (y,x) or (x,y) on the GPU.
	
	// Dyanmically allocate feature space.
	features.alloc(n_locs);
	// Allocate other lines if first time or if old data structures are too small (which is unlikely).
	if ( _ixline == NULL || _ixline->w < 2*n_locs ) {
		if ( _ixline ) {
			cm->freeLine(_ixline);
			cm->freeLine(_locline);
			cm->freeLine(_facline);
		}
		_ixline = cm->allocLine(2*n_locs, "ixline" );
		_locline = cm->allocLine(2*n_locs, "locline" );
		_facline = cm->allocLine(n_locs, "facline");
	}
	svlCudaMemHandler::CvPoint2IntLine(locations, _locline, yx); // Place permanent locations in "_locline."

	// Create aliases to multi pitch memory.
	svlCudaPitch *IMG = MP.get(0,0, 0), *CONV = MP.get(0,0, 1), *FACTORS = MP.get(0,0, 2),
				 *INTEG = MP.get(1,0, 0), *INTEG2 = MP.get(1,0, 1), *RESP = MP.get(1,0, 2),
				 *TEMP1 = MP.get(1,0, 3), *TEMP2 = MP.get(1,0, 4), *TEMP3 = MP.get(1,0, 5),
				 *TEMP4 = MP.get(1,0, 6);

	SVL_LOG(SVL_LOG_VERBOSE, "Computing features over " << locations.size() << " windows for image size "
		<< scaled_w << " x " << scaled_h << "...");

	// Compute feature responses from dictionary.
	for ( int chan = 0; chan < int(scaledImages.size()); ++chan ) {
		// Copy over to GPU.
		if ( cached_image != chan ) {
			cm->IplToGPU(IMG, scaledImages[chan]);
			cached_image = chan;
		}
		svlCudaMath::rowIntegralBlock(IMG, INTEG, INTEG2, scaled_w, scaled_h, dim); // Row-integrate

		// Compute window normalization constants.
		if ( chan != 2 ) {
			// 32-by-32 means.
			svlCudaMath::mean32x32(IMG, TEMP3, TEMP1, scaled_w, scaled_h);
			svlCudaMemHandler::pitchIx2line(TEMP3, _locline, _facline, n_locs, yx);
		} else {
			// Medians.
			svlCodeProfiler::tic(medh);
			vector<float> consts;
			computeNormalizationConstants(scaledImages[2], SVL_DEPTH_PATCH, locations, cvSize(32,32), consts, NULL);
			//svlPatchDictionary::computeNormalizationConstantsStatic(scaledImages[2], SVL_DEPTH_PATCH, locations, cvSize(32,32), consts);
			cm->lineToGPU(_facline, (float*)&consts[0], n_locs);
			svlCodeProfiler::toc(medh);
		}

		// Loop over cached patches: first over width to do only one normalization pass; then over channel, to coalesce transfers.
		for ( int w=0; w<int(_patchBins.size()); ++w ) {
			if ( int(_patchBins[w][chan].size()) == 0 )
				continue;
			svlCudaConvolution::windowNormalizeRow(INTEG, TEMP1, w, scaled_w, scaled_h, dim);
			svlCudaConvolution::windowNormalizeRow(INTEG2, TEMP2, w, scaled_w, scaled_h, dim);

			for ( vector<int>::const_iterator pp = _patchBins[w][chan].begin(); pp != _patchBins[w][chan].end(); ++pp )
			{
				svlCodeProfiler::tic(inner_cuda);
				CachedPatch *cp = &_cachedPatches[*pp]; // Use device-cached patch.
				CvRect rect = _entries[*pp]->getValidRect();
				// Add rect offset to locations stored on the GPU ("_locline") to update "_ixline."
				svlCudaMemHandler::addXYpoint2Line(_locline,  rect.x, rect.y, _ixline, 2*n_locs, yx);

				if ( cmax_opt ) {
					// Fill other indices and provide offsets.
					int x_add = rect.width - 4, 
						y_add = rect.height - 4;
					svlCudaMemHandler::addXYpoint2Line(_locline, rect.x, rect.y + y_add, _ixline, 2*n_locs, yx);
					svlCudaMemHandler::line2pitchIx(FACTORS, _ixline, _facline, n_locs, yx);
					svlCudaMemHandler::addXYpoint2Line(_locline, rect.x + x_add, rect.y, _ixline, 2*n_locs, yx);
					svlCudaMemHandler::line2pitchIx(FACTORS, _ixline, _facline, n_locs, yx);
					svlCudaMemHandler::addXYpoint2Line(_locline, rect.x + x_add, rect.y + y_add, _ixline, 2*n_locs, yx);
					svlCudaMemHandler::line2pitchIx(FACTORS, _ixline, _facline, n_locs, yx);
				}
				// Add rect offset to locations stored on the GPU ("_locline") to update "_ixline."
				svlCudaMemHandler::addXYpoint2Line(_locline, rect.x, rect.y, _ixline, 2*n_locs, yx);
				svlCudaMemHandler::line2pitchIx(FACTORS, _ixline, _facline, n_locs, yx);

				// Three-output convolution.
				svlCudaConvolution::lasikPatchConv2(IMG, cp, TEMP1, TEMP2, CONV, TEMP3, TEMP4, scaled_w, scaled_h);
				// Correction.
				if ( !cmax_opt ) {
					svlCudaMath::lasikCorrectedMax(CONV, TEMP3, TEMP4, FACTORS, RESP, cp->N, cp->mean, cp->std, rect.width, rect.height, scaled_w, scaled_h);
					svlCudaMemHandler::pitchIx2line(RESP, _ixline, features._features, n_locs, yx, *pp, features._feature_stride);
				} else {
					// 4-by-4 max; then do 4-point max to get proper size.
					svlCudaMath::lasikCorrectedMax(CONV, TEMP3, TEMP4, FACTORS, RESP, cp->N, cp->mean, cp->std, 4, 4, scaled_w, scaled_h);
					svlCudaMath::fourPointMax(RESP, TEMP3, rect.width - 4, rect.height - 4, scaled_w, scaled_h);
					svlCudaMemHandler::pitchIx2line(TEMP3, _ixline, features._features, n_locs, yx, *pp, features._feature_stride);
				}
				svlCodeProfiler::toc(inner_cuda);
			}
		}
	}
	SVL_LOG(SVL_LOG_VERBOSE, "...done");
}

// Prune dictionary.
void svlCudaPatchDictionary::prune(const char *fname)
{
	FILE *fp = fopen(fname,"rb");
	if ( !fp ) {
		SVL_LOG(SVL_LOG_FATAL, "Null pruning file");
	}
	int n;
	fread(&n,sizeof(int),1,fp);
	int *ixes = (int*)malloc(sizeof(int)*n);
	fread(ixes,sizeof(int),n,fp);
	fclose(fp);
	int *ptr = ixes;

	int j = 0;
	printf("From size %d ...", _entries.size());
	for ( vector<svlSmartPointer<svlPatchDefinition> >::iterator it=_entries.begin(); it!=_entries.end(); ) {
		if ( j == *ptr ) {
			++it; // Keep.
			++ptr;
			if ( ptr-ixes == n ) {
				// Free all remaining patches since we're out of indices to keep.
				for ( vector<svlSmartPointer<svlPatchDefinition> >::iterator it2=it; it2!=_entries.end(); )
					it2 = _entries.erase(it2);
				break;
			}
		} else {
			it = _entries.erase(it); // Remove.
		}
		++j;
	}
	printf("to %d\n", _entries.size());
}


