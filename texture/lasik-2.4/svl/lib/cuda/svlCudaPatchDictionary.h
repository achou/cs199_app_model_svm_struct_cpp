/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlCudaPatchDictionary.h
** AUTHOR(S):   Paul Baumstarck <pbaumstarck@stanford.edu>
** DESCRIPTION:
**   Stores a patch dictionary. Each entry is defined by a multichannel image
**   patch of size W-by-H, and a window defined relative to some image/region.
**   Also implements a simple multi-channel patch-based object detector with
**   Cuda.
** 
*****************************************************************************/

#pragma once

#include <cassert>
#include <iostream>
#include <vector>

#include "cv.h"
#include "cxcore.h"

#include "svlBase.h"
#include "svlVision.h"
#include "svlCudaCommon.h"
#include "svlCudaDecisionTreeSet.h"
#include "svlCudaMultiPitch.h"

using namespace std;

// svlCudaPatchDictionary ---------------------------------------------------------

class svlCudaPatchDictionary : public svlPatchDictionary
{
protected:
	// Cached patches on the device.
	svlCudaLine *_cachedLine;
	vector<CachedPatch> _cachedPatches;
	// Cached patch binning structure.
	vector<vector<vector<int> > > _patchBins;

	// Whether patches are present on the device.
	bool _patches_cached;

	// Device lines for feature extraction.
	svlCudaLine *_ixline, *_locline, *_facline;

public:
	svlCudaPatchDictionary(unsigned width = 0, unsigned height = 0);
	svlCudaPatchDictionary(const svlFeatureExtractor *d);
	void init();
	virtual ~svlCudaPatchDictionary();

	// Pad and cache patches on the device.
	bool padAndCache();

	// Allocate required pitches in the given data structure.
	void allocPitches(svlCudaMultiPitch &mp);

	// Run feature extraction.
	void extract(const vector<IplImage*> &scaledImages,
				 const vector<CvPoint> &locations,
				 svlCudaMultiPitch &MP,
				 svlCudaFeatureLine &features,
				 bool cmax_opt = false, int cached_image = -1);

	// Prune patches in dictionary.
	void prune(const char *fname);

	// Accessors.
	inline bool getPatchesCached() { return _patches_cached; }
};
