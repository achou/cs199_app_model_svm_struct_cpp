/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlDualDecomposition.h
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
** DESCRIPTION:
**  Implements dual decomposition MAP inference (see Komodakis and Paragios, 
**  CVPR 2008 and works cited therein). Note that this does not handle very
**  large higher-order cliques since that would require specialized sparse or
**  pattern svlFactor classes.
**
*****************************************************************************/

#pragma once

#include <cassert>
#include <vector>

#include "svlFactor.h"
#include "svlFactorOperations.h"
#include "svlClusterGraph.h"
#include "svlGraphUtils.h"

using namespace std;

// svlDualDecompositionAlgorithms ------------------------------------------

typedef enum _svlDualDecompositionAlgorithms {
    SVL_DD_NONE,
    SVL_DD_GENERIC,
    SVL_DD_GENERIC_REWEIGHT,         // re-distributes unary potentials
    SVL_DD_TREES                     // construct max-spanning trees
} svlDualDecompositionAlgorithms;

string toString(svlDualDecompositionAlgorithms& dda);
svlDualDecompositionAlgorithms decodeDualDecompositionAlgorithm(const char *s);

// svlDualDecompositionInference Class -------------------------------------

class svlDualDecompositionInference {
 public:
    static double dualityEpsilon;

 protected:
    // cluster graph for inference (includes initial clique potentials)
    svlClusterGraph _graph;
    
    svlDualDecompositionAlgorithms _algorithm;

    // schedule
    double _initialAlpha;

    // slave problems and dual variables
    vector<set<int> > _varSlaveMapping;
    vector<svlClusterGraph *> _slaves;

 public:
    svlDualDecompositionInference(svlClusterGraph& graph);
    virtual ~svlDualDecompositionInference();
    
    void reset();

    // Run inference.
    double inference(vector<int>& mapAssignment,
        svlDualDecompositionAlgorithms ddAlgorithm = SVL_DD_GENERIC,
        double alpha = 1.0, int maxIterations = 1000);

 protected:
    void genericDualDecomposition(vector<int>& mapAssignment, int maxIterations);
    void buildTreeSlaves();

    bool dualDecompositionLoop(vector<int>& mapAssignment, int maxIterations);
};
