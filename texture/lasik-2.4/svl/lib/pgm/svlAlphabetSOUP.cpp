/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlAlphabetSOUP.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**
*****************************************************************************/

#include <stdlib.h>
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <deque>
#include <queue>
#include <algorithm>
#include <limits>

#include "svlBase.h"
#include "svlFactor.h"
#include "svlClusterGraph.h"
#include "svlMessagePassing.h"
#include "svlAlphabetSOUP.h"

using namespace std;

// svlAlphabetSOUPInference Class -------------------------------------------

svlAlphabetSOUPInference::svlAlphabetSOUPInference(svlClusterGraph& graph) :
    _graph(graph)
{
#if 0
    // check running intersection property (slow)
    SVL_ASSERT(_graph.checkRunIntProp());
#endif
}

svlAlphabetSOUPInference::~svlAlphabetSOUPInference()
{
    reset();
}

void svlAlphabetSOUPInference::reset()
{
    // do nothing
}

double svlAlphabetSOUPInference::inference(vector<int>& mapAssignment,
    vector<vector<vector<int> > >& kValueSets, int maxIterations)
{
    int numValueSets = (int)kValueSets.size();

    // initialize to best singleton assignments
    if (mapAssignment.empty()) {
        mapAssignment.resize(_graph.numVariables(), 0);
        for (int i = 0; i < _graph.numCliques(); i++) {
            if (_graph.getClique(i).size() == 1) {
                int v = *_graph.getClique(i).begin();
                mapAssignment[v] = _graph.getCliquePotential(i).indexOfMax();
            }
        }
    }
    SVL_ASSERT((int)mapAssignment.size() == _graph.numVariables());

    vector<pair<int, int> > graphEdges;
    graphEdges.reserve(_graph.numEdges());
    for (int i = 0; i < _graph.numEdges(); i++) {
        graphEdges.push_back(_graph.getEdge(i));
    }

    double minEnergy = _graph.getEnergy(mapAssignment);
    if (maxIterations <= 0) {
        return minEnergy;
    }

    // handles for profiling
    int hConstruction = svlCodeProfiler::getHandle("alphabetExpansion.constructGraph");
    int hInference = svlCodeProfiler::getHandle("alphabetExpansion.iteration");

    // iterate until convergence
    int lastAssignmentChange = numValueSets - 1;
    int nCycle = 0; int nIteration = -1;
    vector<int> kCards(_graph.numVariables(), -1);
    SVL_LOG(SVL_LOG_MESSAGE, "Starting alphabet-expansion loop...");
    while (1) {
        nIteration = (nIteration + 1) % numValueSets;
        nCycle += (nIteration == 0);

        SVL_LOG(SVL_LOG_MESSAGE, "...cycle " << nCycle
            << ", iteration " << nIteration
            << ", energy " << minEnergy << "...");

        // construct value sets
        vector<vector<int> > valueSets(kValueSets[nIteration]);
        for (int i = 0; i < (int)valueSets.size(); i++) {
            if (find(valueSets[i].begin(), valueSets[i].end(), mapAssignment[i]) ==
                valueSets[i].end()) {
                valueSets[i].push_back(mapAssignment[i]);
            }
        }

        // construct graph for this iteration
        svlCodeProfiler::tic(hConstruction);
        for (int i = 0; i < _graph.numVariables(); i++) {
            kCards[i] = (int)valueSets[i].size();
        }
        svlClusterGraph kGraph(_graph.numVariables(), kCards);

        SVL_LOG(SVL_LOG_DEBUG, "...projecting factors onto "
            << _graph.numCliques() << " cliques");
        for (int i = 0; i < _graph.numCliques(); i++) {
            kGraph.addClique(_graph.getClique(i),
                projectFactor(_graph[i], valueSets));
        }
        kGraph.connectGraph(graphEdges);
        svlCodeProfiler::toc(hConstruction);

        // run inference
        svlCodeProfiler::tic(hInference);
        svlMessagePassingInference infObject(kGraph);
        infObject.inference(SVL_MP_ASYNCLOGMAXPROD_LAZY, maxIterations);
        svlCodeProfiler::toc(hInference);

        // extract MAP
        vector<int> xHat(_graph.numVariables(), -1);
        for (int i = 0; i < _graph.numCliques(); i++) {
            svlFactor phi = infObject[i];
            if (phi.numVars() == 1) {
                int varId = phi.variableId(0);
                xHat[varId] = valueSets[varId][phi.indexOfMax()];
            }
        }
        for (int i = 0; i < _graph.numVariables(); i++) {
            SVL_ASSERT((xHat[i] >= 0) && (xHat[i] < _graph.getCardinality(i)));
        }

        // compute energy
        double energy = _graph.getEnergy(xHat);
        if (energy < minEnergy) {
            minEnergy = energy;
            mapAssignment = xHat;
            lastAssignmentChange = nIteration;
        } else if (lastAssignmentChange == nIteration)
            break;

    }

    return minEnergy;
}

// private member functions -------------------------------------------------

svlFactor svlAlphabetSOUPInference::projectFactor(const svlFactor& phi,
    const vector<vector<int> >& valueSets)
{
    svlFactor projectedPhi;

    // check for empty factors
    if (phi.empty())
        return projectedPhi;

    // add variables to projected factor
    vector<int> vars = phi.vars();
    vector<int> cards(vars.size(), -1);

    for (int i = 0; i < (int)vars.size(); i++) {
        int card = valueSets[vars[i]].size();
        SVL_ASSERT(card <= phi.cards()[i]);
        projectedPhi.addVariable(vars[i], card);
    }

    // populate factor table
    vector<int> assignment(0, vars.size());
    for (int i = 0; i < (int)projectedPhi.size(); i++) {
        projectedPhi.assignmentOf(i, assignment);
        for (int j = 0; j < (int)vars.size(); j++) {
            SVL_ASSERT((unsigned)assignment[j] < valueSets[vars[j]].size());
            assignment[j] = valueSets[vars[j]][assignment[j]];
        }
        int k = phi.indexOf(assignment);

        SVL_ASSERT((k >= 0) && (k < phi.size()));
        projectedPhi[i] = phi[k];
    }

    return projectedPhi;
}

// project a list of factors onto reduced value-space
vector<svlFactor> svlAlphabetSOUPInference::projectFactors(const vector<svlFactor>& phi,
    const vector<vector<int> >& valueSets)
{
    vector<svlFactor> projectedPhi;

    projectedPhi.reserve(phi.size());
    for (int i = 0; i < (int)phi.size(); i++) {
        projectedPhi.push_back(projectFactor(phi[i], valueSets));
    }

    return projectedPhi;
}
