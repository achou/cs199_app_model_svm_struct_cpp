/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlGraphCuts.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**
*****************************************************************************/

#include <stdlib.h>
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <deque>
#include <queue>
#include <algorithm>
#include <limits>

#include "svlBase.h"
#include "svlFactor.h"
#include "svlClusterGraph.h"
#include "svlGraphCuts.h"

// This should be defined in make.local if Vladimir Kolmogorov's maxflow
// code is installed.
#ifdef KOLMOGOROV_MAXFLOW
#include "../../../external/maxflow-v3.0.src/graph.h"
#endif

using namespace std;

// svlGraphCutsAlgorithm utility functions -----------------------------------

string toString(svlGraphCutsAlgorithms& gca)
{
    string infAlgoStr = string("NONE");
    switch (gca) {
    case SVL_GC_ALPHAEXPANSION:
        infAlgoStr = string("ALPHAEXPANSION"); break;
    case SVL_GC_ALPHABETASWAP:
        infAlgoStr = string("ALPHABETASWAP"); break;
    default:
        SVL_ASSERT(false);
    }

    return infAlgoStr;
}

svlGraphCutsAlgorithms decodeGraphCutsAlgorithm(const char *s)
{
    svlGraphCutsAlgorithms infAlgorithm = SVL_GC_NONE;

    if (!strcasecmp(s, "ALPHAEXPANSION")) {
        infAlgorithm = SVL_GC_ALPHAEXPANSION;
    } else if (!strcasecmp(s, "ALPHABETASWAP")) {
        infAlgorithm = SVL_GC_ALPHABETASWAP;
    }

    return infAlgorithm;
}

// svlGraphCutsInference public members -------------------------------------

svlGraphCutsInference::svlGraphCutsInference(svlClusterGraph& graph) :
    _graph(graph), _algorithm(SVL_GC_NONE)
{
#if 0
    // check running intersection property (slow)
    SVL_ASSERT(_graph.checkRunIntProp());
#endif
}

svlGraphCutsInference::~svlGraphCutsInference()
{
    // do nothing
}

void svlGraphCutsInference::reset()
{
    _edges.clear();
    _incidentEdges.clear();
    _edgeLookup.clear();
}

void svlGraphCutsInference::inference(vector<int>& mapAssignment,
    svlGraphCutsAlgorithms gcAlgorithm)
{
    _algorithm = gcAlgorithm;

    switch (_algorithm) {
    case SVL_GC_ALPHAEXPANSION:
        alphaExpansion(mapAssignment);
        break;
    case SVL_GC_ALPHABETASWAP:
        alphaBetaSwap(mapAssignment);
        break;
    default:
        SVL_ASSERT(_algorithm == SVL_GC_NONE);
        break;
    }
}

bool svlGraphCutsInference::testBench()
{
    // test min-cut algorithm
    reset();
    _graph = svlClusterGraph(5);
    addGraphCutEdge(0, 1, 3.0);
    addGraphCutEdge(2, 0, 3.0);
    addGraphCutEdge(0, 3, 3.0);
    addGraphCutEdge(1, 2, 4.0);
    addGraphCutEdge(4, 1, 1.0);
    addGraphCutEdge(2, 3, 1.0);
    addGraphCutEdge(2, 4, 2.0);
    addGraphCutEdge(3, 4, 2.0);
    addGraphCutEdge(3, 5, 6.0);
    addGraphCutEdge(4, 6, 1.0);
    addGraphCutEdge(5, 6, 9.0);
    initializeIncidentEdges();

    SVL_LOG(SVL_LOG_VERBOSE, "Running min-cut...");
    vector<int> cut;
    double f = minCutEdmondsKarp(cut);

    SVL_LOG(SVL_LOG_MESSAGE, "max-flow has value " << f);
    SVL_LOG(SVL_LOG_MESSAGE, "cut is " << toString(cut));

    // test 2
    reset();
    _graph = svlClusterGraph(2);
    addGraphCutEdge(0, 1, 1000.0);
    addGraphCutEdge(0, 2, 1000.0);
    addGraphCutEdge(1, 2, 1.0);
    addGraphCutEdge(1, 3, 1000.0);
    addGraphCutEdge(2, 3, 1000.0);
    initializeIncidentEdges();

    SVL_LOG(SVL_LOG_VERBOSE, "Running min-cut...");
    f = minCutEdmondsKarp(cut);

    SVL_LOG(SVL_LOG_MESSAGE, "max-flow has value " << f);
    SVL_LOG(SVL_LOG_MESSAGE, "cut is " << toString(cut));

    return true;
}

// svlGraphCutsInference protected members ----------------------------------

void svlGraphCutsInference::initializeIncidentEdges()
{
    _incidentEdges.clear();

    int nEdges = _edges.size();
    int nNodes = _graph.numVariables() + 2;
    SVL_ASSERT(nNodes > 2);

    _incidentEdges.resize(nNodes);
    for (int i = 0; i < nEdges; i++) {
        SVL_ASSERT(_edges[i].nodeA < _edges[i].nodeB);
        if ((_edges[i].wAB + _edges[i].wBA > SVL_EPSILON)) {
            _incidentEdges[_edges[i].nodeA].push_back(&_edges[i]);
            _incidentEdges[_edges[i].nodeB].push_back(&_edges[i]);
        }
    }
}

void svlGraphCutsInference::addGraphCutEdge(int u, int v, double w)
{
    if (v < u) {
        std::swap(u, v);
        w = -w;
    }

    if (_edgeLookup.empty()) {
        _edgeLookup.resize(_graph.numVariables() + 2);
        _edges.reserve(4 * _graph.numVariables());
    }

    map<int, int>::const_iterator it = _edgeLookup[u].find(v);
    if (it == _edgeLookup[u].end()) {
        _edgeLookup[u].insert(make_pair(v, (int)_edges.size()));
        if (w > 0) {
            _edges.push_back(svlWeightedEdge(u, v, w));
        } else {
            _edges.push_back(svlWeightedEdge(u, v, 0.0, -w));
        }
    } else {
        if (w > 0) {
            _edges[it->second].wAB += w;
        } else {
            _edges[it->second].wBA -= w;
        }
    }
}

// Notes: Overwrites original weights by redistributing capacities
// inplace. S is assumed to be 0. T is assumed to be the last node.
double svlGraphCutsInference::minCutEdmondsKarp(vector<int>& cut)
{
    SVL_ASSERT(_incidentEdges.size() > 2);

    // get number of nodes
    int nEdges = _edges.size();
    int nNodes = _incidentEdges.size();
    SVL_LOG(SVL_LOG_VERBOSE, "...runing min-cut on graph with " <<
        nNodes << " nodes and " << nEdges << " edges");

    int s = 0;
    int t = nNodes - 1;
    double flowValue = 0.0;

#if 1
    // pre-augment paths s->u->t
    for (int u = 1; u < nNodes - 1; u++) {
        map<int, int>::const_iterator e_su = _edgeLookup[s].find(u);
        map<int, int>::const_iterator e_ut = _edgeLookup[u].find(t);

        if ((e_su != _edgeLookup[s].end()) && (e_ut != _edgeLookup[u].end())) {
            double w = std::min(_edges[e_su->second].wAB, _edges[e_ut->second].wAB);
            _edges[e_su->second].wAB -= w;
            _edges[e_su->second].wBA += w;
            _edges[e_ut->second].wAB -= w;
            _edges[e_ut->second].wBA += w;
            flowValue += w;
        }
    }
#endif

    // find max-flow
    vector<svlWeightedEdge *> parentEdges(nNodes);
    vector<int> frontier(nNodes);

    int hAugmentingPath = svlCodeProfiler::getHandle("minCutEdmondsKarp.augmentingPath");
    while (true) {
        svlCodeProfiler::tic(hAugmentingPath);
        // find augmenting path (BFS)
        int frontierHead = 0;
        int frontierTail = 0;
        frontier[frontierTail++] = s;
        fill(parentEdges.begin(), parentEdges.end(), (svlWeightedEdge *)NULL);
        parentEdges[s] = (svlWeightedEdge *)0xffffffff;

#if 1
        for (vector<svlWeightedEdge *>::const_iterator it = _incidentEdges[s].begin();
             it != _incidentEdges[s].end(); ++it) {
            svlWeightedEdge * const p = *it;
            if (p->wAB > 0.0) {
                frontier[frontierTail++] = p->nodeB;
                parentEdges[(*it)->nodeB] = p;
            }
        }
        frontierHead++;
#endif
        while ((frontierHead != frontierTail) && (parentEdges[t] == NULL)) {
            int u = frontier[frontierHead++]; // BFS
            //int u = frontier[--frontierTail]; // DFS

            for (vector<svlWeightedEdge *>::const_iterator it = _incidentEdges[u].begin();
                 it != _incidentEdges[u].end(); ++it) {

                svlWeightedEdge * const p = *it;
                if (p->nodeA == u) {
                    if ((parentEdges[p->nodeB] == NULL) && (p->wAB > 0.0)) {
                        frontier[frontierTail++] = p->nodeB;
                        parentEdges[p->nodeB] = p;
                    }
                } else {
                    if ((parentEdges[p->nodeA] == NULL) && (p->wBA > 0.0)) {
                        frontier[frontierTail++] = p->nodeA;
                        parentEdges[p->nodeA] = p;
                        //if (p->nodeA == t)
                        //    break;
                    }
                }
            }
        }
        svlCodeProfiler::toc(hAugmentingPath);

        if (frontierHead == frontierTail) break;

        // update residuals
        double c = numeric_limits<double>::max();
        int u = t;
        while (u != s) {
            svlWeightedEdge * const e = parentEdges[u];
            if (e->nodeB == u) {
                u = e->nodeA;
                if (e->wAB < c)
                    c = e->wAB;
            } else {
                u = e->nodeB;
                if (e->wBA < c)
                    c = e->wBA;
            }
        }

        SVL_ASSERT(c > 0.0);
        u = t;
        while (u != s) {
            svlWeightedEdge * const e = parentEdges[u];
            if (e->nodeB == u) {
                u = e->nodeA;
                e->wAB -= c;
                e->wBA += c;
            } else {
                u = e->nodeB;
                e->wAB += c;
                e->wBA -= c;
            }
        }

        flowValue = flowValue + c;
    }

    {
    // find min-cut
    cut.resize(nNodes);
    fill(cut.begin(), cut.end(), 1);
    deque<int> frontier;
    frontier.push_front(s);
    cut[s] = 0;
    while (!frontier.empty()) {
        int u = frontier.front();
        frontier.pop_front();

        for (vector<svlWeightedEdge *>::const_iterator it = _incidentEdges[u].begin();
             it != _incidentEdges[u].end(); ++it) {

            svlWeightedEdge * const p = *it;
            if (p->nodeA == u) {
                if ((p->wAB > 0.0) && (cut[p->nodeB] == 1)) {
                    frontier.push_back(p->nodeB);
                    cut[p->nodeB] = 0;
                }
            } else {
                if ((p->wBA > 0.0) && (cut[p->nodeA] == 1)) {
                    frontier.push_back(p->nodeA);
                    cut[p->nodeA] = 0;
                }
            }
        }
    }
    SVL_ASSERT(cut[t] == 1);
    }

    return flowValue;
}

#ifndef KOLMOGOROV_MAXFLOW
// naive version of graph-cuts
void svlGraphCutsInference::alphaExpansion(vector<int>& mapAssignment)
{
    mapAssignment.resize(_graph.numVariables());
    fill(mapAssignment.begin(), mapAssignment.end(), 0);

    // check cardinality
    int maxAlpha = _graph.getCardinality(0);
    for (int i = 1; i < _graph.numVariables(); i++) {
        SVL_ASSERT_MSG(_graph.getCardinality(i) == maxAlpha, "not all variables have the same cardinality");
    }

    // initialize using singletons
    for (int i = 0; i < _graph.numCliques(); i++) {
        svlFactor const &phi = _graph.getCliquePotential(i);
        if (phi.numVars() == 1) {
            int u = phi.variableId(0);
            mapAssignment[u] = phi.indexOfMax();
        }
    }

    int hConstructGraph = svlCodeProfiler::getHandle("alphaExpansion.constructGraph");
    int hMinCutIteration = svlCodeProfiler::getHandle("alphaExpansion.mincut");
    double minEnergy = _graph.getEnergy(mapAssignment);
    vector<int> assignment(mapAssignment.size());
    vector<int> cut;
    bool bChanged = true;
    int lastChanged = -1;
    for (int nCycle = 0; bChanged; nCycle += 1) {
        bChanged = false;
        for (int alpha = 0; alpha < maxAlpha; alpha++) {
            if (alpha == lastChanged)
                break;

            SVL_LOG(SVL_LOG_VERBOSE, "Cycle " << nCycle << ", iteration "
                << alpha << ", energy " << minEnergy);

            svlCodeProfiler::tic(hConstructGraph);
            alphaExpansionConstructGraph(mapAssignment, alpha);
            initializeIncidentEdges();
            svlCodeProfiler::toc(hConstructGraph);
            svlCodeProfiler::tic(hMinCutIteration);
            minCutEdmondsKarp(cut);

            for (int i = 0; i < (int)assignment.size(); i++) {
                assignment[i] = (cut[i + 1] == 0) ? alpha : mapAssignment[i];
            }
            double e = _graph.getEnergy(assignment);
            SVL_LOG(SVL_LOG_VERBOSE, "...min-cut has energy " << e);
            if (e < minEnergy) {
                minEnergy = e;
                mapAssignment = assignment;
                lastChanged = alpha;
                bChanged = true;
            }
            svlCodeProfiler::toc(hMinCutIteration);
        }
    }
}

void svlGraphCutsInference::alphaBetaSwap(vector<int>& mapAssignment)
{
    mapAssignment.resize(_graph.numVariables());
    fill(mapAssignment.begin(), mapAssignment.end(), 0);

    // check cardinality
    int maxAlpha = _graph.getCardinality(0);
    for (int i = 1; i < _graph.numVariables(); i++) {
        SVL_ASSERT_MSG(_graph.getCardinality(i) == maxAlpha, "not all variables have the same cardinality");
    }

    // initialize using singletons
    for (int i = 0; i < _graph.numCliques(); i++) {
        svlFactor const &phi = _graph.getCliquePotential(i);
        if (phi.numVars() == 1) {
            int u = phi.variableId(0);
            mapAssignment[u] = phi.indexOfMax();
        }
    }

    int hConstructGraph = svlCodeProfiler::getHandle("alphaBetaSwap.constructGraph");
    int hMinCutIteration = svlCodeProfiler::getHandle("alphaBetaSwap.mincut");
    double minEnergy = _graph.getEnergy(mapAssignment);
    vector<int> assignment(mapAssignment.size());
    vector<int> cut;
    bool bChanged = true;
    for (int nCycle = 0; bChanged; nCycle += 1) {
        bChanged = false;
        for (int alpha = 0; alpha < maxAlpha; alpha++) {
            for (int beta = alpha + 1; beta < maxAlpha; beta++) {

                SVL_LOG(SVL_LOG_VERBOSE, "Cycle " << nCycle << ", iteration <"
                    << alpha << ", " << beta << ">, energy " << minEnergy);

                svlCodeProfiler::tic(hConstructGraph);
                alphaBetaSwapConstructGraph(mapAssignment, alpha, beta);
                initializeIncidentEdges();
                svlCodeProfiler::toc(hConstructGraph);
                svlCodeProfiler::tic(hMinCutIteration);
                minCutEdmondsKarp(cut);

                int cutIndx = 1;
                for (int i = 0; i < (int)assignment.size(); i++) {
                    if ((mapAssignment[i] != alpha) && (mapAssignment[i] != beta)) {
                        assignment[i] = mapAssignment[i];
                        continue;
                    }
                    assignment[i] = (cut[cutIndx++] == 0) ? alpha : beta;
                }
                double e = _graph.getEnergy(assignment);
                SVL_LOG(SVL_LOG_VERBOSE, "...min-cut has energy " << e);
                if (e < minEnergy) {
                    minEnergy = e;
                    mapAssignment = assignment;
                    bChanged = true;
                }
                svlCodeProfiler::toc(hMinCutIteration);
            }
        }
    }
}
#else
// kolmogorov's version of graph-cuts
void svlGraphCutsInference::alphaExpansion(vector<int>& mapAssignment)
{
    mapAssignment.resize(_graph.numVariables());
    fill(mapAssignment.begin(), mapAssignment.end(), 0);

    // check cardinality
    int maxAlpha = _graph.getCardinality(0);
    for (int i = 1; i < _graph.numVariables(); i++) {
        SVL_ASSERT_MSG(_graph.getCardinality(i) == maxAlpha, "not all variables have the same cardinality");
    }

    // initialize using singletons
    for (int i = 0; i < _graph.numCliques(); i++) {
        svlFactor phi(_graph.getCliquePotential(i));
        if (phi.numVars() == 1) {
            int u = phi.variableId(0);
            mapAssignment[u] = phi.indexOfMax();
        }
    }

    int hConstructGraph = svlCodeProfiler::getHandle("alphaExpansion.constructGraph");
    int hMinCutIteration = svlCodeProfiler::getHandle("alphaExpansion.mincut");
    double minEnergy = _graph.getEnergy(mapAssignment);
    vector<int> assignment(mapAssignment.size());
    vector<int> cut;
    bool bChanged = true;
    int lastChanged = -1;
    for (int nCycle = 0; bChanged; nCycle += 1) {
        bChanged = false;
        for (int alpha = 0; alpha < maxAlpha; alpha++) {
            if (alpha == lastChanged)
                break;

            SVL_LOG(SVL_LOG_VERBOSE, "Cycle " << nCycle << ", iteration "
                << alpha << ", energy " << minEnergy);

            svlCodeProfiler::tic(hConstructGraph);
            typedef Graph<double, double, double> GraphType;
            GraphType *g = new GraphType(mapAssignment.size(), 8 * mapAssignment.size());

            g->add_node(mapAssignment.size());

            // construct graph
            int numNonSubmodular = 0;
            for (int c = 0; c < _graph.numCliques(); c++) {
                svlFactor const &phi = _graph.getCliquePotential(c);

                if (phi.numVars() == 1) {
                    int v = phi.variableId(0);
                    g->add_tweights(v, -phi[mapAssignment[v]], -phi[alpha]);
                } else if (phi.numVars() == 2) {
                    int u = phi.variableId(0);
                    int v = phi.variableId(1);

#if 0
                    double A = -phi[phi.indexOf(v, alpha, phi.indexOf(u, alpha))];
                    double B = -phi[phi.indexOf(v, mapAssignment[v], phi.indexOf(u, alpha))];
                    double C = -phi[phi.indexOf(v, alpha, phi.indexOf(u, mapAssignment[u]))];
                    double D = -phi[phi.indexOf(v, mapAssignment[v], phi.indexOf(u, mapAssignment[u]))];
#else
                    double A = -phi[alpha * maxAlpha + alpha];
                    double B = -phi[mapAssignment[v] * maxAlpha + alpha];
                    double C = -phi[alpha * maxAlpha + mapAssignment[u]];
                    double D = -phi[mapAssignment[v] * maxAlpha + mapAssignment[u]];
#endif

                    // check for submodularity
                    if (!subModularityCheck(A, B, C, D)) {
                        numNonSubmodular += 1;
                    }

                    g->add_tweights(u, D, A);

                    B -= A; C -= D;
                    SVL_ASSERT_MSG(B + C >= 0, "B = " << B << ", C = " << C);
                    if (B < 0) {
                        g->add_tweights(u, 0, B);
                        g->add_tweights(v, 0, -B);
                        g->add_edge(u, v, 0, B + C);
                    } else if (C < 0) {
                        g->add_tweights(u, 0, -C);
                        g->add_tweights(v, 0, C);
                        g->add_edge(u, v, B + C, 0);
                    } else {
                        g->add_edge(u, v, B, C);
                    }
                } else {
                    SVL_ASSERT_MSG(false, "alpha-expansion only supports pairwise cliques");
                }
            }

            if (numNonSubmodular > 0) {
                SVL_LOG(SVL_LOG_WARNING, numNonSubmodular << " non-submodular potentials");
            }
            svlCodeProfiler::toc(hConstructGraph);
            svlCodeProfiler::tic(hMinCutIteration);
            g->maxflow();

            for (int i = 0; i < (int)assignment.size(); i++) {
                assignment[i] = (g->what_segment(i) == GraphType::SOURCE) ?
                    alpha : mapAssignment[i];
            }
            double e = _graph.getEnergy(assignment);
            SVL_LOG(SVL_LOG_VERBOSE, "...min-cut has energy " << e);
            if (e < minEnergy) {
                minEnergy = e;
                mapAssignment = assignment;
                lastChanged = alpha;
                bChanged = true;
            }

            delete g;
            svlCodeProfiler::toc(hMinCutIteration);
        }
    }
}

void svlGraphCutsInference::alphaBetaSwap(vector<int>& mapAssignment)
{
    mapAssignment.resize(_graph.numVariables());
    fill(mapAssignment.begin(), mapAssignment.end(), 0);

    // check cardinality
    int maxAlpha = _graph.getCardinality(0);
    for (int i = 1; i < _graph.numVariables(); i++) {
        SVL_ASSERT_MSG(_graph.getCardinality(i) == maxAlpha, "not all variables have the same cardinality");
    }

    // initialize using singletons
    for (int i = 0; i < _graph.numCliques(); i++) {
        svlFactor phi(_graph.getCliquePotential(i));
        if (phi.numVars() == 1) {
            int u = phi.variableId(0);
            mapAssignment[u] = phi.indexOfMax();
        }
    }

    int hConstructGraph = svlCodeProfiler::getHandle("alphaBetaSwap.constructGraph");
    int hMinCutIteration = svlCodeProfiler::getHandle("alphaBetaSwap.mincut");
    double minEnergy = _graph.getEnergy(mapAssignment);
    vector<int> assignment(mapAssignment.size());
    vector<int> varsInMove(mapAssignment.size(), -1);
    vector<int> cut;
    bool bChanged = true;
    for (int nCycle = 0; bChanged; nCycle += 1) {
        bChanged = false;
        for (int alpha = 0; alpha < maxAlpha; alpha++) {
            for (int beta = alpha + 1; beta < maxAlpha; beta++) {

                SVL_LOG(SVL_LOG_VERBOSE, "Cycle " << nCycle << ", iteration <"
                    << alpha << ", " << beta << ">, energy " << minEnergy);

                int numVarsInMove = 0;
                fill(varsInMove.begin(), varsInMove.end(), -1);
                for (int i = 0; i < _graph.numVariables(); i++) {
                    if ((mapAssignment[i] == alpha) || (mapAssignment[i] == beta)) {
                        varsInMove[i] = numVarsInMove++;
                    }
                }

                if (numVarsInMove == 0) {
                    SVL_LOG(SVL_LOG_VERBOSE, "...no variables with values " << alpha << " or " << beta);
                    continue;
                }

                svlCodeProfiler::tic(hConstructGraph);
                typedef Graph<double, double, double> GraphType;
                GraphType *g = new GraphType(numVarsInMove, 8 * numVarsInMove);

                g->add_node(numVarsInMove);

                // construct graph
                int numNonSubmodular = 0;
                for (int c = 0; c < _graph.numCliques(); c++) {
                    svlFactor const &phi = _graph.getCliquePotential(c);
                    if (phi.numVars() == 1) {
                        int v = varsInMove[phi.variableId(0)];
                        if (v != -1) {
                            g->add_tweights(v, -phi[beta], -phi[alpha]);
                        }
                    } else if (phi.numVars() == 2) {
                        int u = varsInMove[phi.variableId(0)];
                        int v = varsInMove[phi.variableId(1)];

                        if ((u != -1) && (v != -1)) {
                            double A = -phi[alpha * maxAlpha + alpha];
                            double B = -phi[beta * maxAlpha + alpha];
                            double C = -phi[alpha * maxAlpha + beta];
                            double D = -phi[beta * maxAlpha + beta];

                            // check for submodularity
                            if (!subModularityCheck(A, B, C, D)) {
                                numNonSubmodular += 1;
                            }

                            g->add_tweights(u, D, A);

                            B -= A; C -= D;
                            SVL_ASSERT_MSG(B + C >= 0, "B = " << B << ", C = " << C);
                            if (B < 0) {
                                g->add_tweights(u, 0, B);
                                g->add_tweights(v, 0, -B);
                                g->add_edge(u, v, 0, B + C);
                            } else if (C < 0) {
                                g->add_tweights(u, 0, -C);
                                g->add_tweights(v, 0, C);
                                g->add_edge(u, v, B + C, 0);
                            } else {
                                g->add_edge(u, v, B, C);
                            }

                        } else if (u != -1) {
                            double A = -phi[mapAssignment[phi.variableId(1)] * maxAlpha + alpha];
                            double B = -phi[mapAssignment[phi.variableId(1)] * maxAlpha + beta];
                            g->add_tweights(u, B, A);

                        } else if (v != -1) {
                            double A = -phi[mapAssignment[phi.variableId(0)] + alpha * maxAlpha];
                            double B = -phi[mapAssignment[phi.variableId(0)] + beta * maxAlpha];
                            g->add_tweights(v, B, A);
                        }
                        
                    } else {
                        SVL_ASSERT_MSG(false, "alpha-beta-swap only supports pairwise cliques");
                    }
                }

                svlCodeProfiler::toc(hConstructGraph);
                svlCodeProfiler::tic(hMinCutIteration);
                g->maxflow();

                for (int i = 0; i < (int)assignment.size(); i++) {
                    int v = varsInMove[i];
                    if (v != -1) {
                        assignment[i] = mapAssignment[i];
                    } else {
                        assignment[i] = (g->what_segment(v) == GraphType::SOURCE) ? alpha : beta;
                    }
                }
                double e = _graph.getEnergy(assignment);
                SVL_LOG(SVL_LOG_VERBOSE, "...min-cut has energy " << e);
                if (e < minEnergy) {
                    minEnergy = e;
                    mapAssignment = assignment;
                    bChanged = true;
                }

                delete g;
                svlCodeProfiler::toc(hMinCutIteration);
            }
        }
    }
}
#endif

void svlGraphCutsInference::alphaExpansionConstructGraph(const vector<int>& assignment, int alpha)
{
    SVL_ASSERT((int)assignment.size() == _graph.numVariables());
    const int s = 0;
    const int t = _graph.numVariables() + 1;

    // clear weights on existing edges
    if (_edges.empty())
        _edges.reserve(4 * _graph.numVariables());
    for (int i = 0; i < (int)_edges.size(); i++) {
        _edges[i].wAB = _edges[i].wBA = 0.0;
    }

    // compute new weights
    int numNonSubmodular = 0;
    for (int c = 0; c < _graph.numCliques(); c++) {
        svlFactor const &phi = _graph.getCliquePotential(c);

        if (phi.numVars() == 1) {
            int v = phi.variableId(0);
            addGraphCutEdge(s, v + 1, -phi[assignment[v]]);
            addGraphCutEdge(v + 1, t, -phi[alpha]);
        } else if (phi.numVars() == 2) {
            int u = phi.variableId(0);
            int v = phi.variableId(1);

            double A = -phi[phi.indexOf(v, alpha, phi.indexOf(u, alpha))];
            double B = -phi[phi.indexOf(v, assignment[v], phi.indexOf(u, alpha))];
            double C = -phi[phi.indexOf(v, alpha, phi.indexOf(u, assignment[u]))];
            double D = -phi[phi.indexOf(v, assignment[v], phi.indexOf(u, assignment[u]))];

            // check for submodularity
            if (!subModularityCheck(A, B, C, D)) {
                numNonSubmodular += 1;
            }

            addGraphCutEdge(s, u + 1, D);
            addGraphCutEdge(u + 1, t, A);

            B -= A; C -= D;
            SVL_ASSERT_MSG(B + C >= 0, "B = " << B << ", C = " << C);
            if (B < 0) {
                addGraphCutEdge(u + 1, t, B);
                addGraphCutEdge(v + 1, t, -B);
                addGraphCutEdge(v + 1, u + 1, B + C);
            } else if (C < 0) {
                addGraphCutEdge(u + 1, t, -C);
                addGraphCutEdge(v + 1, t, C);
                addGraphCutEdge(u + 1, v + 1, B + C);
            } else {
                addGraphCutEdge(u + 1, v + 1, B);
                addGraphCutEdge(v + 1, u + 1, C);
            }

        } else {
            SVL_ASSERT_MSG(false, "alpha-expansion only supports pairwise cliques");
        }
    }

    if (numNonSubmodular > 0) {
        SVL_LOG(SVL_LOG_WARNING, numNonSubmodular << " non-submodular potentials");
    }
}

 void svlGraphCutsInference::alphaBetaSwapConstructGraph(const vector<int>& assignment,
     int alpha, int beta)
{
    SVL_ASSERT((int)assignment.size() == _graph.numVariables());
    
    int numVarsInMove = 0;
    vector<int> varsInMove(assignment.size(), -1);
    for (int i = 0; i < (int)assignment.size(); i++) {
        if ((assignment[i] == alpha) || (assignment[i] == beta)) {
            varsInMove[i] = numVarsInMove++;
        }
    }

    if (numVarsInMove == 0) {
        SVL_LOG(SVL_LOG_VERBOSE, "...no variables with values " << alpha << " or " << beta);
        return;
    }

    const int s = 0;
    const int t = numVarsInMove + 1;
    const int maxAlpha = _graph.getCardinality(0);

    // clear weights
    _edges.clear();

    // construct graph
    int numNonSubmodular = 0;
    for (int c = 0; c < _graph.numCliques(); c++) {
        svlFactor const &phi = _graph.getCliquePotential(c);
        if (phi.numVars() == 1) {
            int v = varsInMove[phi.variableId(0)];
            if (v != -1) {
                addGraphCutEdge(s, v + 1, -phi[beta]);
                addGraphCutEdge(v + 1, t, -phi[alpha]);
            }
        } else if (phi.numVars() == 2) {
            int u = varsInMove[phi.variableId(0)];
            int v = varsInMove[phi.variableId(1)];

            if ((u != -1) && (v != -1)) {
                double A = -phi[alpha * maxAlpha + alpha];
                double B = -phi[beta * maxAlpha + alpha];
                double C = -phi[alpha * maxAlpha + beta];
                double D = -phi[beta * maxAlpha + beta];

                // check for submodularity
                if (!subModularityCheck(A, B, C, D)) {
                    numNonSubmodular += 1;
                }
                
                addGraphCutEdge(s, u + 1, D);
                addGraphCutEdge(u + 1, t, A);

                B -= A; C -= D;
                SVL_ASSERT_MSG(B + C >= 0, "B = " << B << ", C = " << C);
                if (B < 0) {
                    addGraphCutEdge(u + 1, t, B);
                    addGraphCutEdge(v + 1, t, -B);
                    addGraphCutEdge(v + 1, u + 1, B + C);
                } else if (C < 0) {
                    addGraphCutEdge(u + 1, t, -C);
                    addGraphCutEdge(v + 1, t, C);
                    addGraphCutEdge(u + 1, v + 1, B + C);
                } else {
                    addGraphCutEdge(u + 1, v + 1, B);
                    addGraphCutEdge(v + 1, u + 1, C);
                }

            } else if (u != -1) {
                double A = -phi[assignment[phi.variableId(1)] * maxAlpha + alpha];
                double B = -phi[assignment[phi.variableId(1)] * maxAlpha + beta];

                addGraphCutEdge(s, v + 1, B);
                addGraphCutEdge(v + 1, t, A);
            } else if (v != -1) {
                double A = -phi[assignment[phi.variableId(0)] + alpha * maxAlpha];
                double B = -phi[assignment[phi.variableId(0)] + beta * maxAlpha];

                addGraphCutEdge(s, v + 1, B);
                addGraphCutEdge(v + 1, t, A);
            }
            
        } else {
            SVL_ASSERT_MSG(false, "alpha-beta-swap only supports pairwise cliques");
        }
    }
}

bool svlGraphCutsInference::subModularityCheck(double &A, double &B, double &C, double &D) const
{
    if (A + D > C + B) {
        // truncate non-submodular functions
        double delta = A + D - C - B;
        A -= delta / 3 - SVL_EPSILON;
        C += delta / 3 + SVL_EPSILON;
        B = A + D - C + SVL_EPSILON;
        return false;
    }
    
    return true;
}
