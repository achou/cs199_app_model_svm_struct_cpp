/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlAlphabetSOUP.h
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
** DESCRIPTION:
**   Naive implementation of Alphabet SOUP with max-product message passing
**   as the inner loop. See Gould et al., CVPR 2009 for details. Potentials
**   must be in log-space. A singleton node for each variable must exist in
**   the cluster graph.
**
** TODO:
**   1. don't construct computation tree each iteration; rather use index
**      sets to decide which message entries to compute.
*****************************************************************************/

#pragma once

#include <vector>

#include "svlFactor.h"
#include "svlFactorOperations.h"
#include "svlClusterGraph.h"
#include "svlMessagePassing.h"

using namespace std;

// svlAlphabetSOUPInference Class -------------------------------------------

class svlAlphabetSOUPInference {
 protected:
    // cluster graph for inference (includes initial clique potentials)
    svlClusterGraph _graph;
    
 public:
    // Constructors and destructors. A separate inference object needs to be 
    // contructed for each cluster graph.
    svlAlphabetSOUPInference(svlClusterGraph& graph);
    virtual ~svlAlphabetSOUPInference();
    
    // Return reference to cluster graph so that factors can be changed.
    // This is useful during training iterations or if the graph structure
    // stays the same, but evidence (features) change.
    inline svlClusterGraph& graph() { return _graph; }

    // Clear all messages, computation tree and label sets. This method 
    // should be called if you plan to keep the inference object around 
    // but want to conserve memory between calls.
    void reset();

    // Run AlphabetSOUP with message passing to find the MAP assignment.
    // kValueSets is a set of gamma-expansion moves, each move has a vector
    // of values for each variable in the mode, i.e. kValueSets has size
    // K \times N \times |A_i^k|.
    double inference(vector<int>& mapAssignment,
        vector<vector<vector<int> > >& kValueSets, int maxIterations = 100);

 protected:
    // Project a factor onto reduced value-space.
    svlFactor projectFactor(const svlFactor& phi, 
        const vector<vector<int> >& valueSets);

    // Project a list of factors onto reduced value-space
    vector<svlFactor> projectFactors(const vector<svlFactor>& phi, 
        const vector<vector<int> >& valueSets);    
};


