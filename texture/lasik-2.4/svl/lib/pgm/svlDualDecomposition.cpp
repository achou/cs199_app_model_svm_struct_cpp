/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlDualDecomposition.cpp
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
**
*****************************************************************************/

#include <stdlib.h>
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <deque>
#include <queue>
#include <algorithm>
#include <limits>

#include "svlBase.h"
#include "svlFactor.h"
#include "svlClusterGraph.h"
#include "svlMessagePassing.h"
#include "svlDualDecomposition.h"

using namespace std;

// svlDualDecompositionAlgorithms utility functions --------------------------

string toString(svlDualDecompositionAlgorithms& dda)
{
    string infAlgoStr = string("NONE");
    switch (dda) {
    case SVL_DD_GENERIC:
        infAlgoStr = string("DUAL_DECOMPOSITION"); break;
    case SVL_DD_GENERIC_REWEIGHT:
        infAlgoStr = string("REWEIGHTED_DUAL_DECOMPOSITION"); break;
    case SVL_DD_TREES:
        infAlgoStr = string("DUAL_DECOMPOSITION_TREES"); break;
    default:
        SVL_ASSERT(false);
    }

    return infAlgoStr;
}

svlDualDecompositionAlgorithms decodeDualDecompositionAlgorithm(const char *s)
{
    svlDualDecompositionAlgorithms infAlgorithm = SVL_DD_NONE;

    if (!strcasecmp(s, "DUAL_DECOMPOSITION")) {
        infAlgorithm = SVL_DD_GENERIC;
    } else if (!strcasecmp(s, "REWEIGHTED_DUAL_DECOMPOSITION")) {
        infAlgorithm = SVL_DD_GENERIC_REWEIGHT;
    } else if (!strcasecmp(s, "DUAL_DECOMPOSITION_TREES")) {
        infAlgorithm = SVL_DD_TREES;
    }

    return infAlgorithm;
}

// svlDualDecompositionInference Class --------------------------------------

double svlDualDecompositionInference::dualityEpsilon = 1.0e-6;

svlDualDecompositionInference::svlDualDecompositionInference(svlClusterGraph& graph) :
    _graph(graph), _algorithm(SVL_DD_NONE)
{
    // do nothing
}

svlDualDecompositionInference::~svlDualDecompositionInference()
{
    reset();
}

void svlDualDecompositionInference::reset()
{
    // clear slaves
    for (int i = 0; i < (int)_slaves.size(); i++) {
        delete _slaves[i];
    }
    _slaves.clear();
    _varSlaveMapping.clear();
}

double svlDualDecompositionInference::inference(vector<int>& mapAssignment,
    svlDualDecompositionAlgorithms ddAlgorithm, double alpha, int maxIterations)
{
    _algorithm = ddAlgorithm;
    _initialAlpha = alpha;
    reset();

    switch (_algorithm) {
    case SVL_DD_GENERIC:
    case SVL_DD_GENERIC_REWEIGHT:
        genericDualDecomposition(mapAssignment, maxIterations);
        break;
    case SVL_DD_TREES:
        buildTreeSlaves();
        dualDecompositionLoop(mapAssignment, maxIterations);
        break;
    default:
        SVL_ASSERT(_algorithm == SVL_DD_NONE);
        break;
    }

    return _graph.getEnergy(mapAssignment, true);
}

// private member functions -------------------------------------------------

void svlDualDecompositionInference::genericDualDecomposition(vector<int>& mapAssignment,
    int maxIterations)
{
    // initial assignment
    mapAssignment.resize(_graph.numVariables());
    fill(mapAssignment.begin(), mapAssignment.end(), 0);
    double bestEnergy = _graph.getEnergy(mapAssignment);
    double bestDualEnergy = -SVL_DBL_MAX;

    // variable to clique mappings
    _varSlaveMapping.clear();
    _varSlaveMapping.resize(_graph.numVariables());
    for (int c = 0; c < _graph.numCliques(); c++) {
        svlClique C = _graph.getClique(c);
        for (svlClique::const_iterator q = C.begin(); q != C.end(); q++) {
            _varSlaveMapping[*q].insert(c);
        }
    }

    // distribute unary weight over higher-order terms
    if (_algorithm == SVL_DD_GENERIC_REWEIGHT) {
        // TODO: clean up (own function?)
        for (int q = 0; q < _graph.numVariables(); q++) {
            svlFactor dU(q, _graph.getCardinality(q));
            dU.fill(0.0);
            int nUnaryCount = 0;
            for (set<int>::const_iterator it = _varSlaveMapping[q].begin();
                 it != _varSlaveMapping[q].end(); it++) {
                if (_graph[*it].numVars() == 1) {
                    dU.add(_graph[*it]);
                    nUnaryCount += 1;
                }
            }

            if ((nUnaryCount == 0) || (nUnaryCount == (int)_varSlaveMapping[q].size()))
                continue;
            SVL_ASSERT(dU.numVars() == 1);

            dU.scale(1.0 / (double)(_varSlaveMapping[q].size() - nUnaryCount));
            for (set<int>::iterator it = _varSlaveMapping[q].begin(); it != _varSlaveMapping[q].end(); ) {
                if (_graph[*it].numVars() == 1) {
                    _graph[*it].fill(0.0);
                    set<int>::iterator jt = it++;
                    _varSlaveMapping[q].erase(jt);
                } else {
                    _graph[*it].add(dU);
                    it++;
                }
            }
        }
    }

#if 1
    for (int i = 0; i < _graph.numVariables(); i++) {
        SVL_LOG(SVL_LOG_DEBUG, "X_" << i << " appears in C_" << toString(_varSlaveMapping[i]));
    }
#endif

    // assignments
    vector<vector<int> > slaveAssignments(_graph.numCliques(),
        vector<int>(_graph.numVariables(), -1));

    // gradients
    vector<vector<double> > delta(_graph.numVariables(), vector<double>());
    for (int q = 0; q < _graph.numVariables(); q++) {
        delta[q].resize(_graph.getCardinality(q));
    }
    vector<bool> bVarConverged(_graph.numVariables());

    // repeat until convergence
    bool bConverged = false;
    int nIterations = 0;
    double dualityGap = 1.0;
    while (!bConverged && (nIterations < maxIterations)) {
        //SVL_LOG(SVL_LOG_VERBOSE, "...iteration " << nIterations);

        // optimize each slave
        double dualEnergy = 0.0;
        for (int c = 0; c < _graph.numCliques(); c++) {
            const svlFactor& phi = _graph[c];
            vector<int> assignment;
            int indx = phi.indexOfMax();
            dualEnergy -= phi[indx];
            phi.assignmentOf(indx, assignment);
            for (int i = 0; i < phi.numVars(); i++) {
                slaveAssignments[c][phi.variableId(i)] = assignment[i];
            }

            SVL_LOG(SVL_LOG_DEBUG, "X^{(" << c << ")} = " << toString(slaveAssignments[c])
                << "; e = " << phi[phi.indexOfMax()]);
        }
        if (dualEnergy > bestDualEnergy) {
            bestDualEnergy = dualEnergy;
            dualityGap = bestEnergy - bestDualEnergy;
        }
        if (dualityGap <= dualityEpsilon)
            break;

        // update prices (and check for convergence)
        bConverged = true;
        for (int q = 0; q < _graph.numVariables(); q++) {
            fill(delta[q].begin(), delta[q].end(), 0.0);
            for (set<int>::const_iterator c = _varSlaveMapping[q].begin();
                 c != _varSlaveMapping[q].end(); c++) {
                delta[q][slaveAssignments[*c][q]] += 1.0;
            }

            bVarConverged[q] = true;
            for (int v = 0; v < _graph.getCardinality(q); v++) {
                bVarConverged[q] = bVarConverged[q] &&
                    ((delta[q][v] == 0.0) || (delta[q][v] == (double)_varSlaveMapping[q].size()));
                delta[q][v] /= (double)_varSlaveMapping[q].size();
            }

            if (bVarConverged[q]) {
                SVL_LOG(SVL_LOG_DEBUG, "...all slaves agree on variable " << q);
            } else {
                bConverged = false;
            }
        }

#if 0
        double alpha = _initialAlpha / (double)(nIterations + 1);
#elif 1
        // update alpha as dualityGap / dg^2
        double dg2 = 0.0;
        for (int q = 0; q < _graph.numVariables(); q++) {
            dg2 += (double)_varSlaveMapping[q].size();
        }
        double alpha = _initialAlpha * dualityGap / dg2;
#else
        // update alpha as dualityGap / [dg^2]_lambda
        double dg2 = 0.0;
        for (int q = 0; q < _graph.numVariables(); q++) {
            if (bVarConverged[q]) continue;
            dg2 += (double)_varSlaveMapping[q].size();
            for (int v = 0; v < _graph.getCardinality(q); v++) {
                dg2 += _varSlaveMapping[q].size() * delta[q][v] * delta[q][v];
            }
            for (set<int>::const_iterator c = _varSlaveMapping[q].begin();
                 c != _varSlaveMapping[q].end(); c++) {
                dg2 -= 2.0 * delta[q][slaveAssignments[*c][q]];
            }
        }
        double alpha = _initialAlpha * dualityGap / dg2;
        //double alpha = _initialAlpha * (bestEnergy - bestDualEnergy) / dg2;
#endif

        // some assignments may be impossible
        if (!isfinite(dualityGap)) {
            alpha = _initialAlpha;
        }

        alpha /= sqrt((double)nIterations + 1.0);

        // take gradient step
        vector<int> assignment(_graph.numVariables(), -1);
        for (int q = 0; q < _graph.numVariables(); q++) {
            assignment[q] = argmax<double>(delta[q]);
            //assignment[q] = argrand(delta[q]); // random rounding
            if (bVarConverged[q]) continue;

            for (int v = 0; v < _graph.getCardinality(q); v++) {
                delta[q][v] *= alpha;
            }

            // variable has not converged, so update prices
            svlFactor dU(q, _graph.getCardinality(q));
            for (set<int>::const_iterator c = _varSlaveMapping[q].begin();
                 c != _varSlaveMapping[q].end(); c++) {
                for (int v = 0; v < _graph.getCardinality(q); v++) {
                    dU[v] = delta[q][v] - (slaveAssignments[*c][q] == v ? alpha : 0.0);
                }

                _graph[*c].add(dU);
            }
        }

        // next iteration
        double primalEnergy = _graph.getEnergy(assignment, true);
        if (primalEnergy < bestEnergy) {
            bestEnergy = primalEnergy;
            mapAssignment = assignment;
        }
        dualityGap = bestEnergy - bestDualEnergy;
        SVL_LOG(SVL_LOG_MESSAGE, "...iteration " << nIterations << " dual: " << dualEnergy
            << "; integrality gap: " << dualityGap);
        nIterations += 1;
    }

    if (nIterations >= maxIterations) {
        SVL_LOG(SVL_LOG_WARNING, "dual decomposition failed to converge after " << nIterations << " iterations");
    } else {
        SVL_LOG(SVL_LOG_WARNING, "dual decomposition converged after " << nIterations << " iterations");
    }
}

void svlDualDecompositionInference::buildTreeSlaves()
{
    // create slave MRFs and variable-to-slave mapping
    _varSlaveMapping.clear();
    _varSlaveMapping.resize(_graph.numVariables());

#if 0
    // DEBUGGING: single cliques
    for (int c = 0; c < _graph.numCliques(); c++) {
        _slaves.push_back(new svlClusterGraph(_graph.numVariables(),
                              _graph.getCardinalities()));
        svlClique C = _graph.getClique(c);
        _slaves.back()->addClique(C, _graph[c]);
        for (svlClique::const_iterator qi = C.begin(); qi != C.end(); qi++) {
            _varSlaveMapping[*qi].insert(_slaves.size() - 1);
        }
    }
#else
    // construct one slave tree rooted at each clique ensuring
    // running intersection property
    vector<vector<pair<int, int> > > slaveTrees;
    vector<set<int> > cliqueTreeMapping(_graph.numCliques());
    for (int a = 0; a < _graph.numCliques(); a++) {
        if (!cliqueTreeMapping[a].empty())
            continue;
        
        slaveTrees.push_back(vector<pair<int, int> >(1, make_pair(-1, a)));
        cliqueTreeMapping[a].insert(slaveTrees.size() - 1);
        // TODO: randomly permute ordering

        //#error "Running Intersection Property"

#if 1
        // DEBUGGING: one child
        int bestChild = -1;
        int bestSepset = 0;
        svlClique Ca = _graph.getClique(a);
        for (int b = 0; b < _graph.numCliques(); b++) {
            if (b == a) continue;
            svlClique Cb = _graph.getClique(b);
            svlClique S;
            set_intersection(Ca.begin(), Ca.end(), Cb.begin(), Cb.end(),
                std::inserter(S, S.begin()));
            if ((int)S.size() > bestSepset) {
                bestSepset = (int)S.size();
                bestChild = b;
            }
        }

        if (bestChild >= 0) {
            slaveTrees.back().push_back(make_pair(a, bestChild));
            cliqueTreeMapping[bestChild].insert(slaveTrees.size() - 1);
        }
#else
        for (int b = 0; b < _graph.numCliques(); b++) {
            if (b == a) continue;
            svlClique Cb = _graph.getClique(b);
            if (Cb.size() == 1) continue;
            int bestParent = -1;
            int bestSepset = 0;
            for (int i = 0; i < (int)slaveTrees.back().size(); i++) {
                svlClique Ci = _graph.getClique(slaveTrees.back()[i].second);
                svlClique S;
                set_intersection(Ci.begin(), Ci.end(),
                    Cb.begin(), Cb.end(),
                    std::inserter(S, S.begin()));
                if ((int)S.size() > bestSepset) {
                    bestSepset = (int)S.size();
                    bestParent = slaveTrees.back()[i].second;
                }
            }

            if (bestParent >= 0) {
                slaveTrees.back().push_back(make_pair(bestParent, b));
                cliqueTreeMapping[b].insert(slaveTrees.size() - 1);
            }
        }
#endif
    }

    // build MRFs
    for (vector<vector<pair<int, int> > >::const_iterator it = slaveTrees.begin();
         it != slaveTrees.end(); it++) {

        _slaves.push_back(new svlClusterGraph(_graph.numVariables(),
                              _graph.getCardinalities()));

        vector<int> cliqueIndexMapping(_graph.numCliques(), -1);
        for (vector<pair<int, int> >::const_iterator jt = it->begin(); jt != it->end(); jt++) {
            svlClique C = _graph.getClique(jt->second);
            svlFactor phi = _graph[jt->second];
            for (int j = 0; j < phi.size(); j++) {
                phi[j] /= (double)cliqueTreeMapping[jt->second].size();
            }
            _slaves.back()->addClique(C, phi);

            for (svlClique::const_iterator qi = C.begin(); qi != C.end(); qi++) {
                _varSlaveMapping[*qi].insert((int)_slaves.size() - 1);
            }

            cliqueIndexMapping[jt->second] = _slaves.back()->numCliques() - 1;
        }

        SVL_LOG(SVL_LOG_DEBUG, "slave " << _slaves.size() << " has " <<
            _slaves.back()->numCliques() << " cliques");

        vector<pair<int, int> > edges;
        for (vector<pair<int, int> >::const_iterator jt = it->begin(); jt != it->end(); jt++) {
            if (jt->first != -1) {
                SVL_ASSERT(cliqueIndexMapping[jt->first] != -1);
                edges.push_back(make_pair(cliqueIndexMapping[jt->first],
                                    cliqueIndexMapping[jt->second]));
                SVL_LOG(SVL_LOG_DEBUG, "... edge " << toString(edges.back()));
            }
        }
        _slaves.back()->connectGraph(edges);
    }
#endif
}

bool svlDualDecompositionInference::dualDecompositionLoop(vector<int>& mapAssignment,
    int maxIterations)
{
    // initial assignment
    mapAssignment.resize(_graph.numVariables());
    fill(mapAssignment.begin(), mapAssignment.end(), 0);
    double bestEnergy = _graph.getEnergy(mapAssignment);
    double bestDualEnergy = -SVL_DBL_MAX;

    // slave assignments
    vector<vector<int> > slaveAssignments(_slaves.size(),
        vector<int>(_graph.numVariables(), -1));

    // gradients
    vector<vector<double> > delta(_graph.numVariables(), vector<double>());
    for (int q = 0; q < _graph.numVariables(); q++) {
        delta[q].resize(_graph.getCardinality(q));
    }
    vector<bool> bVarConverged(_graph.numVariables());

    // repeat until convergence
    bool bConverged = false;
    int nIterations = 0;
    double dualityGap = 1.0;
    while (!bConverged && (nIterations < maxIterations)) {
        //SVL_LOG(SVL_LOG_VERBOSE, "...iteration " << nIterations);

        // optimize each slave
        double dualEnergy = 0.0;
        for (int i = 0; i < (int)_slaves.size(); i++) {
            // TODO: have slaves save infObjects
            svlMessagePassingInference infObject(*_slaves[i]);
            infObject.inference(SVL_MP_LOGMAXPROD, 2 * _slaves[i]->numCliques());

            fill(slaveAssignments[i].begin(), slaveAssignments[i].end(), -1);
            for (int c = 0; c < _slaves[i]->numCliques(); c++) {
                svlFactor phi = infObject[c];
                vector<int> vars = phi.vars();
                for (vector<int>::const_iterator v = vars.begin(); v != vars.end(); v++) {
                    if (slaveAssignments[i][*v] != -1) {
                        phi.reduce(*v, slaveAssignments[i][*v]);
                    }
                }

                vector<int> a;
                phi.assignmentOf(phi.indexOfMax(), a);
                for (int k = 0; k < phi.numVars(); k++) {
                    slaveAssignments[i][phi.variableId(k)] = a[k];
                }
            }

            dualEnergy += _slaves[i]->getEnergy(slaveAssignments[i], true);
            SVL_LOG(SVL_LOG_DEBUG, "X^{(" << i << ")} = " << toString(slaveAssignments[i])
                << "; e = " << _slaves[i]->getEnergy(slaveAssignments[i], true));
        }

        if (dualEnergy > bestDualEnergy) {
            bestDualEnergy = dualEnergy;
            dualityGap = bestEnergy - bestDualEnergy;
        }
        if (dualityGap <= dualityEpsilon)
            break;

        // update prices (and check for convergence)
        bConverged = true;
        for (int q = 0; q < _graph.numVariables(); q++) {
            fill(delta[q].begin(), delta[q].end(), 0.0);
            for (set<int>::const_iterator c = _varSlaveMapping[q].begin();
                 c != _varSlaveMapping[q].end(); c++) {
                SVL_ASSERT(slaveAssignments[*c][q] != -1);
                delta[q][slaveAssignments[*c][q]] += 1.0;
            }

            bVarConverged[q] = true;
            for (int v = 0; v < _graph.getCardinality(q); v++) {
                bVarConverged[q] = bVarConverged[q] &&
                    ((delta[q][v] == 0.0) || (delta[q][v] == (double)_varSlaveMapping[q].size()));
                delta[q][v] /= (double)_varSlaveMapping[q].size();
            }

            if (bVarConverged[q]) {
                SVL_LOG(SVL_LOG_DEBUG, "...all slaves agree on variable " << q);
            } else {
                bConverged = false;
            }
        }

#if 0
        double alpha = _initialAlpha / (double)(nIterations + 1);
#elif 1
        // update alpha as dualityGap / dg^2
        double dg2 = 0.0;
        for (int q = 0; q < _graph.numVariables(); q++) {
            dg2 += (double)_varSlaveMapping[q].size();
        }
        double alpha = _initialAlpha * dualityGap / dg2;
#else
        // update alpha as dualityGap / [dg^2]_lambda
        double dg2 = 0.0;
        for (int q = 0; q < _graph.numVariables(); q++) {
            if (bVarConverged[q]) continue;
            dg2 += (double)_varSlaveMapping[q].size();
            for (int v = 0; v < _graph.getCardinality(q); v++) {
                dg2 += _varSlaveMapping[q].size() * delta[q][v] * delta[q][v];
            }
            for (set<int>::const_iterator c = _varSlaveMapping[q].begin();
                 c != _varSlaveMapping[q].end(); c++) {
                dg2 -= 2.0 * delta[q][slaveAssignments[*c][q]];
            }
        }
        double alpha = _initialAlpha * dualityGap / dg2;
#endif

        // take gradient step and compute primal assignment
        vector<int> assignment(_graph.numVariables(), -1);
        for (int q = 0; q < _graph.numVariables(); q++) {
            // TODO: use random rounding
            assignment[q] = argmax<double>(delta[q]);
            if (bVarConverged[q]) continue;

            for (int v = 0; v < _graph.getCardinality(q); v++) {
                delta[q][v] *= alpha;
            }

            // variable has not converged, so update prices
            svlFactor dU(q, _graph.getCardinality(q));
            for (set<int>::const_iterator c = _varSlaveMapping[q].begin();
                 c != _varSlaveMapping[q].end(); c++) {
                for (int v = 0; v < _graph.getCardinality(q); v++) {
                    dU[v] = delta[q][v] - (slaveAssignments[*c][q] == v ? alpha : 0.0);
                }

                // find clique to add
                for (int i = 0; i < _slaves[*c]->numCliques(); i++) {
                    // TODO: cache this
                    if ((*_slaves[*c])[i].hasVariable(q)) {
                        (*_slaves[*c])[i].add(dU);
                        break;
                    }
                }
            }
        }

        // TODO: DOES THIS HELP?
        alpha /= sqrt((double)nIterations + 1.0);

        // next iteration
        double primalEnergy = _graph.getEnergy(assignment, true);
        if (primalEnergy < bestEnergy) {
            bestEnergy = primalEnergy;
            mapAssignment = assignment;
        }
        dualityGap = bestEnergy - bestDualEnergy;
        SVL_LOG(SVL_LOG_MESSAGE, "...iteration " << nIterations << " dual: " << dualEnergy
            << "; integrality gap: " << dualityGap);
        nIterations += 1;
    }

    if (nIterations >= maxIterations) {
        SVL_LOG(SVL_LOG_WARNING, "dual decomposition failed to converge after " << nIterations << " iterations");
        return false;
    }

    SVL_LOG(SVL_LOG_MESSAGE, "dual decomposition converged after " << nIterations << " iterations");
    return true;
}

// configuration --------------------------------------------------------

class svlDualDecompositionInferenceConfig : public svlConfigurableModule {
public:
    svlDualDecompositionInferenceConfig() :
        svlConfigurableModule("svlPGM.svlDualDecompositionInference") { }

    void usage(ostream &os) const {
        os << "      eps          :: convergence integrality gap (default: "
           << svlDualDecompositionInference::dualityEpsilon << ")\n";
    }

    void setConfiguration(const char *name, const char *value) {
        // factor operation cache
        if (!strcmp(name, "eps")) {
            svlDualDecompositionInference::dualityEpsilon = atof(value);
        } else {
            SVL_LOG(SVL_LOG_FATAL, "unrecognized configuration option for " << this->name());
        }
    }
};

static svlDualDecompositionInferenceConfig gDualDecompositionInferenceConfig;
