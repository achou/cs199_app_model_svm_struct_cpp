/*****************************************************************************
** STAIR VISION LIBRARY
** Copyright (c) 2007-2010, Stephen Gould
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Stanford University nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
** EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
******************************************************************************
** FILENAME:    svlGraphCuts.h
** AUTHOR(S):   Stephen Gould <sgould@stanford.edu>
** DESCRIPTION:
**  Graph-cuts MAP inference algorithms. Not that the cluster graph should be
**  defined in terms of _negative_ energy.
**
** TODO:
**  1. Change _edge to be pointers to svlWeightedEdges.
**
*****************************************************************************/

#pragma once

#include <cassert>
#include <vector>

#include "svlFactor.h"
#include "svlFactorOperations.h"
#include "svlClusterGraph.h"
#include "svlGraphUtils.h"

using namespace std;

// svlGraphCutsAlgorithms --------------------------------------------------

typedef enum _svlGraphCutsAlgorithms {
    SVL_GC_NONE,
    SVL_GC_ALPHAEXPANSION,
    SVL_GC_ALPHABETASWAP
} svlGraphCutsAlgorithms;

string toString(svlGraphCutsAlgorithms& gca);
svlGraphCutsAlgorithms decodeGraphCutsAlgorithm(const char *s);

// svlGraphCutsInference Class ---------------------------------------------

class svlGraphCutsInference {
 protected:
    // cluster graph for inference (includes initial clique potentials)
    svlClusterGraph _graph;
    
    svlGraphCutsAlgorithms _algorithm;

    // st-graph definition (edges are directed (u to v > u))
    vector<svlWeightedEdge> _edges;
    vector<vector<svlWeightedEdge *> > _incidentEdges;
    vector<map<int, int> > _edgeLookup;  // _edgeLookup[u][v] is _edge containing (u,v)

 public:
    svlGraphCutsInference(svlClusterGraph& graph);
    virtual ~svlGraphCutsInference();
    
    void reset();

    // Run inference.
    void inference(vector<int>& mapAssignment,
        svlGraphCutsAlgorithms gcAlgorithm = SVL_GC_ALPHAEXPANSION);

    // Regression testing
    bool testBench();

 protected:
    void initializeIncidentEdges();
    void addGraphCutEdge(int u, int v, double w);

    double minCutEdmondsKarp(vector<int>& cut);

    void alphaExpansion(vector<int>& mapAssignment);
    void alphaExpansionConstructGraph(const vector<int>& assignment, int alpha);

    void alphaBetaSwap(vector<int>& mapAssignment);
    void alphaBetaSwapConstructGraph(const vector<int>& assignment, int alpha, int beta);

    bool subModularityCheck(double &A, double &B, double &C, double &D) const;
};
