#include <iostream>
#include "cv.h"
#include "highgui.h"
#include "opencv2/video/background_segm.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "lasik-2.4/svl/lib/vision/svlTextonExtractor.h"

#define N_BUCKETS 16

int main( int argc, char** argv ) {
	if (argc != 1) {
		cout << "Usage: ./extract_texture17\n";
		return 0;
	}
  vector<double> maxs;
  vector<double> mins;
  for(int i=0; i<17; i++){
    maxs.push_back(-1e10);
    mins.push_back(1e10);
  }
  for(int iter=0; iter<2; iter++){
    for(int imgi=1; imgi<=305; imgi++){
      for(int i=0; i<17; i++){
        //printf("%d, %lf, %lf\n", i, maxs[i], mins[i]);
      }
      stringstream ss (stringstream::in | stringstream::out);
      ss << "/home/andrew/classes/cs199/cs199_appearance_model/Ramanan_image/im0";
      if(imgi<100) ss << "0";
      if(imgi<10) ss << "0";
      ss << imgi << ".png";
      cout << ss.str() <<endl;
      IplImage *img = cvLoadImage(ss.str().c_str());
      if(!img){
        printf("Could not load image file: %s\n", ss.str().c_str());
        exit(0);
      }
      //TODO use HSL see background.cpp
      vector<IplImage *> responses;
      svlTextonExtractor ste;
      ste.preprocessImage( responses, img);
      //void getDescriptor(const vector<IplImage *> &responses, CvPoint loc,
      //             vector<double> &output, int offset) const;
      // TODO what is offset?
      ofstream out;
      if(iter==1){
        stringstream ss2 (stringstream::in | stringstream::out);
        ss2 << "textures/text_" << imgi;
        out.open(ss2.str().c_str());
      }
      printf("%d %d %d\n", img->width, img->height, img->width*img->height);
      CvPoint pixloc;
      for(int x = 0; x<img->width; x++){
        for(int y = 0; y<img->height; y++){
          pixloc.x = x;
          pixloc.y = y;
          vector<double> output;
          int offset = 0;
          ste.getDescriptor( responses, pixloc, output, offset);
          assert(17==output.size());
          for(unsigned int i=0; i<output.size(); i++){
            if(iter==0){
              maxs[i] = max(maxs[i], output[i]);
              mins[i] = min(mins[i], output[i]);
            }else{
              double pct = (output[i]-mins[i]) / (maxs[i]-mins[i]);
              assert(pct>=0 && pct<=1);
              pct = min(pct, 0.9999);
              int bucket = (int)(pct*N_BUCKETS);
              out << bucket << " ";
            }
            //printf("%lf\n", output[i]);
          }
          if(iter==1) out << endl;
        }
      }
      if(iter==1) out.close();
    }
  }
  return 0;
}
