/***********************************************************************/
/*                                                                     */
/*   svm_struct_api_types.h                                            */
/*                                                                     */
/*   Definition of API for attaching implementing SVM learning of      */
/*   structures (e.g. parsing, multi-label classification, HMM)        */
/*                                                                     */
/*   Author: Thorsten Joachims                                         */
/*   Date: 13.10.03                                                    */
/*                                                                     */
/*   Copyright (c) 2003  Thorsten Joachims - All rights reserved       */
/*                                                                     */
/*   This software is available for non-commercial use only. It must   */
/*   not be modified and distributed without prior permission of the   */
/*   author. The author is not responsible for implications from the   */
/*   use of this software.                                             */
/*                                                                     */
/***********************************************************************/

#include <string>
using namespace std;


#ifndef svm_struct_api_types
#define svm_struct_api_types

// This funciton is used in inference since sometimes we want min energy and
// sometimes we want max (like in the case of find_most_violated_constraint).
// Returns true iff cur is a "better" factor value than old is.
// Otherwise returns false.
typedef int (*curIsBetterFactorCallback)(double old, double cur);

#define RAMANAN      1
#define ONLY_ONE_SCALE 0
#define NSCALES    (RAMANAN ? 5 : 1)
#define NJOINTS    (RAMANAN ? 17 : 5)
#define NTEXTURES  0
#define NCOLOR     4 // LAB, L, A, B
#define NAPP       (NCOLOR+NTEXTURES)
#define NSYMS      (RAMANAN ? 0 : 0)
#define E         (NJOINTS+NSYMS)
#define NPARTS    (RAMANAN ? 18 : 10)
#define NHYPS     (RAMANAN ? 20 : 20)
// arm overlaps only for RAMANAN
#define NOVERLAP  (RAMANAN ? 16 : (NPARTS*(NPARTS-1)/2))
#define SUBPART_LEVELS (RAMANAN ? 0 : 1)
const int MAX_PARTS = SUBPART_LEVELS*SUBPART_LEVELS;
#define NUNIQUE_SINGLETON (RAMANAN ? 10 : 6)
//(RAMANAN ? 13 : 5)
#define NUNIQUE_PAIRWISE 1
//define NUNIQUE_APPEARANCE (NAPP)
#define NUNIQUE_APPEARANCE 4
#define NUNIQUE_OVERLAP (RAMANAN ? 1 : 1)
// energies for: singleton, pairwise, app, and 4 symmetry weights.
#define NFEATURES (NUNIQUE_SINGLETON+NUNIQUE_PAIRWISE+NUNIQUE_APPEARANCE+NSYMS+NUNIQUE_OVERLAP)
// For some reason it doesn't like losses outside the [0,1] range
// so we rescale the loss. This shouldnt matter since the weights
// will be scaled accordingly
#define LOSS_SCALE (1.0/NPARTS)
const double NO_FG_PENALTY = 0;

# include "svm_light/svm_common.h"
# include "svm_light/svm_learn.h"

# define INST_NAME          "Appearance Model API"
# define INST_VERSION       "V0.00"
# define INST_VERSION_DATE  "05.31.12"

/* default precision for solving the optimization problem */
# define DEFAULT_EPS         0.1
/* default loss rescaling method: 1=slack_rescaling, 2=margin_rescaling */
# define DEFAULT_RESCALING   2
/* default loss function: */
# define DEFAULT_LOSS_FCT    0
/* default optimization algorithm to use: */
# define DEFAULT_ALG_TYPE    3
/* store Psi(x,y) (for ALG_TYPE 1) instead of recomputing it every time: */
# define USE_FYCACHE         1
/* decide whether to evaluate sum before storing vectors in constraint
   cache:
   0 = NO,
   1 = YES (best, if sparse vectors and long vector lists),
   2 = YES (best, if short vector lists),
   3 = YES (best, if dense vectors and long vector lists) */
# define COMPACT_CACHED_VECTORS 1
/* minimum absolute value below which values in sparse vectors are
   rounded to zero. Values are stored in the FVAL type defined in svm_common.h
   RECOMMENDATION: assuming you use FVAL=float, use
     10E-15 if COMPACT_CACHED_VECTORS is 1
     10E-10 if COMPACT_CACHED_VECTORS is 2 or 3
*/
# define COMPACT_ROUNDING_THRESH 10E-15

typedef unsigned char color;
typedef struct map_entry{
  int pid; // part id
  int subparts[SUBPART_LEVELS]; // subparts[0]==pid
  int hid; // hypothesis id
  int pixel; //pixel num
  //double prob; // the probability of the pixel being in the foreground
} MAPENTRY;

typedef struct pattern {
  /* this defines the x-part of a training example, e.g. the structure
     for storing a natural language sentence in NLP parsing */
  int scale;
  int imgnum;
  double singleton[NPARTS][NHYPS];//should be NPARTS x NHYPS ptr
  //should be M x NHYPS x NHYPS ptr, where M is the number of non-symmetric joints
  //double pairwise[NJOINTS][NHYPS][NHYPS];
  // cache syms
  double symAndPair[E][NHYPS][NHYPS];
  // The number of pixels.
  int imgsize;
  // There are 3*imagesize colors in the image, since it's rgb
  color *image;
  int (*texture)[NTEXTURES];
  // list of foreground pixels in the image for each part and hypothesis.
  MAPENTRY *fgmap;
  // the index at which entries for each part/hyp pair start.
  int fgmapIndices[NPARTS][NHYPS];
  // the number of pixel mappings. this is not necessarily the same as imgsize.
  int numMappings;
  // This is a hack, so that LABEL can just have 1 assignment of hypotheses
  // and we can use this to compute the loss.
  int y[NPARTS][NHYPS];
  // overlap between hypotheses.
  double (*overlap)[NHYPS][NHYPS];
  // pixel buckets
  struct buckets *allbuckets;
} PATTERN;

typedef struct label {
  /* this defines the y-part (the label) of a training example,
     e.g. the parse tree of the corresponding sentence. */
  //int y[NPARTS][NHYPS];
  int scale;
  int y[NPARTS];
} LABEL;
// Used to pass the loss values scaled by C into minSumWithAppearance.
typedef struct loss {
  double l[NPARTS][NHYPS];
} LOSS;

typedef struct structmodel {
  double *w;          /* pointer to the learned weights */
  MODEL  *svm_model;  /* the learned SVM model */
  long   sizePsi;     /* maximum number of weights in w */
  //double walpha;
  /* other information that is needed for the stuctural model can be
     added here, e.g. the grammar rules for NLP parsing */
  //int add_your_variables_here;
} STRUCTMODEL;

typedef struct app_model_configs {
  int em_iters;
  int model_blur_inv;
  int num_color_buckets;
  int app_model_prior;
  int use_symmetry;
  int approx_inference;
  int inference_iters;
  int model_blur;
  int hillclimb;
  int plot_inference;
  int inference_method;
  int abcd_block_size;
  int abcd_iters;
  int abcd_inf_time;
  int writeLog;
  int include_block;
  int naive_init;
  int sample_random_features, sample_learning_features;
  double singleton_weights[NPARTS];
  double singleton_means[NPARTS];
  double singleton_sdevinv[NPARTS];
  double pairwise_weights[NJOINTS];
  double pairwise_means[NJOINTS];
  double pairwise_sdevinv[NJOINTS];
  double appearance_weights[NAPP];
  double appearance_means[NAPP];
  double appearance_sdevinv[NAPP];
  double symmetric_weights[NSYMS];
  double symmetric_means[NSYMS];
  double symmetric_sdevinv[NSYMS];
  double overlap_weights[NOVERLAP];
  double overlap_means[NOVERLAP];
  double overlap_sdevinv[NOVERLAP];
  double worst;
  curIsBetterFactorCallback isBetter;
} CONFIGS;
typedef struct joint{
  int j1,j2;
} JOINT;
typedef struct struct_learn_parm {
  double epsilon;              /* precision for which to solve
				  quadratic program */
  double newconstretrain;      /* number of new constraints to
				  accumulate before recomputing the QP
				  solution (used in w=1 algorithm) */
  int    ccache_size;          /* maximum number of constraints to
				  cache for each example (used in w=4
				  algorithm) */
  double batch_size;           /* size of the mini batches in percent
				  of training set size (used in w=4
				  algorithm) */
  double C;                    /* trade-off between margin and loss */
  char   custom_argv[50][300]; /* storage for the --* command line options */
  int    custom_argc;          /* number of --* command line options */
  int    slack_norm;           /* norm to use in objective function
                                  for slack variables; 1 -> L1-norm,
				  2 -> L2-norm */
  int    loss_type;            /* selected loss type from -r
				  command line option. Select between
				  slack rescaling (1) and margin
				  rescaling (2) */
  int    loss_function;        /* select between different loss
				  functions via -l command line
				  option */
  /* further parameters that are passed to init_struct_model() */
  int nparts; // number of parts
  int nhyp; //number of hypotheses per part
  CONFIGS constraint_config;
  JOINT *joints; // physical joints
  int njoints; // number of joints, not including symmetric appearance.
  JOINT *syms; // symmetric appearance joints
  int nsyms;
  double optloss; //this is a hack to return extra data from the loss function
  string weights_dir;
  string example_dir;
  string configfile;
  string logname;// where to log energies
  // The id of the last training example, so we can know when to
  // update the means and std devs.
  int end;
} STRUCT_LEARN_PARM;

typedef struct struct_test_stats {
  /* you can add variables for keeping statistics when evaluating the
     test predictions in svm_struct_classify. This can be used in the
     function eval_prediction and print_struct_testing_stats. */
  int n; // # of test examples
  double loss; // loss
  double optloss; // best possible loss that could have been achieved
} STRUCT_TEST_STATS;

typedef struct features{
  double singleton[NPARTS];
  double pairwise[NJOINTS];
  double appearance[NAPP];
  double sym[NSYMS];
  double overlap[NOVERLAP];
} FEATURES;

typedef struct returnenergy{
  double energy, factorEnergy;
} RETENERGY;

#endif
