#include "common.h"
#include "features.h"
#include <vector>
#include <algorithm>
#include <assert.h>

int *Overlap_edges;
int num_ignored;

// all parts and hyps are 0 based
double computeOverlap( PATTERN *x, int p1, int h1, int p2, int h2){
  if (!(p1>=0 && p1<NPARTS && p2>=0 && p2<NPARTS)) {
    printf("%d, %d\n", p1, p2);
    assert(p1>=0 && p1<NPARTS && p2>=0 && p2<NPARTS);
  }
  assert(h1>=0 && h1<NHYPS && h2>=0 && h2<NHYPS);
  int index1 = x->fgmapIndices[p1][h1];
  assert(index1>=0 && index1<x->numMappings);
  int nextIndex1 = getNextIndex(x, p1, h1);
  assert(nextIndex1>0 && nextIndex1<=x->numMappings);
  int index2 = x->fgmapIndices[p2][h2];
  assert(index2>=0 && index2<x->numMappings);
  int nextIndex2 = getNextIndex(x, p2, h2);
  assert(nextIndex2>0 && nextIndex2<=x->numMappings);
  if(index1==nextIndex1){
    //printf("Warning: ignoring part %d, hyp %d, because it has no fg.\n", p1, h1);
    num_ignored++;
    return 0; // no overlap
  }
  if(index2==nextIndex2){
    //printf("Warning: ignoring part %d, hyp %d, because it has no fg.\n", p2, h2);
    num_ignored++;
    return 0; // no overlap
  }
  vector<int> pixels1(nextIndex1-index1);
  vector<int> pixels2(nextIndex2-index2);
  for(int i=0; i<nextIndex1-index1; i++)
    pixels1[i] = x->fgmap[i+index1].pixel;
  for(int i=0; i<nextIndex2-index2; i++)
    pixels2[i] = x->fgmap[i+index2].pixel;
  sort(pixels1.begin(), pixels1.begin());
  sort(pixels2.begin(), pixels2.begin());
  vector<int> result(nextIndex1-index1+nextIndex2-index2);
  vector<int>::iterator it = set_intersection(
      pixels1.begin(), pixels1.end(),
      pixels2.begin(), pixels2.end(), result.begin());
  int size = it - result.begin();
  double retval = size / sqrt(pixels1.size()*pixels2.size());
  if(retval != retval){
    printf("%d %d %d %d %d %lf\n", p1, h1, p2, h2, size, retval);
    printf("%lu, %lu, %u\n", pixels1.size(), pixels2.size(), size);
    printf("%d %d %d %d\n", index1, nextIndex1, index2, nextIndex2);
    Error("problem in computeOverlap");
  }
  return retval;
}
void computeOverlap( PATTERN *x){
  x->overlap = (double(*)[NHYPS][NHYPS])
    malloc(sizeof(double)*NOVERLAP*NHYPS*NHYPS);
  num_ignored = 0;
  for(int o=0; o<NOVERLAP; o++){
    for(int h1=0; h1<NHYPS; h1++){
      for(int h2=0; h2<NHYPS; h2++){
        x->overlap[o][h1][h2] = computeOverlap( x, Overlap_edges[o], h1,
            Overlap_edges[o+NOVERLAP], h2);
        /*
        if(x->overlap[o][h1][h2] > 0.0){
          printf("omg %f\n", x->overlap[o][h1][h2]);
        }
        */
      }
    }
  }
  //printf("Warning: ignoring %d hyps.\n", num_ignored);
}
void freeOverlap( PATTERN *x){
  free(x->overlap);
}
void initOverlapEdgesRamanan(){
  /*
  int p1[] = { 3, 3, 3, 3,
               4, 4, 4, 4,
               5, 5, 5, 5,
               6, 6, 6, 6,
               10,10,10,
               11,11,11,11,
               12,12,12,12,
               13,13,13,13};
  int p2[] = {15,16,17,18,
              15,16,17,18,
              15,16,17,18,
              15,16,17,18,
              23,24,25,
              22,23,24,25,
              22,23,24,25,
              22,23,24,25};
              */
  int p1[] = { 4, 4, 4, 4,
               5, 5, 5, 5,
               6, 6, 6, 6,
               7, 7, 7, 7};
  int p2[] = {12,13,14,15,
              12,13,14,15,
              12,13,14,15,
              12,13,14,15};
  Overlap_edges = (int*) malloc(sizeof(int)*2*NOVERLAP);
  for(int e=0; e<NOVERLAP; e++){
    Overlap_edges[e] = p1[e];
    Overlap_edges[e+NOVERLAP] = p2[e];
  }
}
void initOverlapEdges(){
  Overlap_edges = (int*) malloc(sizeof(int)*2*NOVERLAP);
  int e = 0;
  for(int p1=0; p1<NPARTS; p1++){
    for(int p2=p1+1; p2<NPARTS; p2++){
      Overlap_edges[e] = p1;
      Overlap_edges[e+NOVERLAP] = p2;
      e++;
    }
  }
}
void freeOverlapEdges(){
  free(Overlap_edges);
}
// the main function that is called to compute all features
void extraFeatures( EXAMPLE *example, int s){
  PATTERN *x = example->x + s;
  //printf("s: %d\n", s);
  computeOverlap(x);
}
void freeFeatures( EXAMPLE *example, int s){
  PATTERN *x = example->x + s;
  freeOverlap(x);
}
