# Makefile for empty SVM-struct API, 03.10.06

#Call 'make' using the following line to make CYGWIN produce stand-alone Windows executables
#		make 'SFLAGS=-mno-cygwin'

#Use the following to compile under unix or cygwin
OPT = -O3
CC = g++ 
LD = g++
CFLAGS =   $(SFLAGS) ${OPT} -g -fomit-frame-pointer -Wall -ansi -funroll-loops 
#-ffast-math 
LDFLAGS =  $(SFLAGS) ${OPT} -g -lm -Wall -Wreturn-type
LIBS =  -L/usr/lib -lm -lpthread
#CFLAGS =  $(SFLAGS) -pg -Wall
#LDFLAGS = $(SFLAGS) -pg -lm -Wall 
NO_EXES = svm_light_hideo_noexe svm_struct_noexe graphs_noexe
GRAPHS_OBJS = inference/graphs.o inference/factors_heaps.o inference/fib.o 
GRAPHS_OBJS += inference/factor.o inference/doublepool.o
CUSTOM_OBJS = minSumWithAppearance.o features.o common.o featureBank.o 
CUSTOM_OBJS += computeU.h

all: svm_empty_learn svm_empty_classify

.PHONY: clean
clean: svm_light_clean svm_struct_clean graphs_clean
	rm -f *.o *.tcov *.d core gmon.out *.stackdump 
	rm -f svm_empty_learn svm_empty_classify
#-----------------------#
#----   SVM-light   ----#
#-----------------------#
svm_light_hideo_noexe: 
	cd svm_light; make svm_learn_hideo_noexe
svm_light_clean: 
	cd svm_light; make clean
#----------------------#
#----  STRUCT SVM  ----#
#----------------------#
svm_struct_noexe: 
	cd svm_struct; make svm_struct_noexe
svm_struct_clean: 
	cd svm_struct; make clean
#----------------------#
#----  GRAPHS  ----#
#----------------------#
graphs_noexe: 
	cd inference; make
graphs_clean: 
	cd inference; make clean
#-------------------------#
#----  SVM empty API  ----#
#-------------------------#
svm_empty_classify: ${NO_EXES} svm_struct_api.o svm_struct/svm_struct_classify.o svm_struct/svm_struct_common.o svm_struct/svm_struct_main.o ${CUSTOM_OBJS} ${GRAPHS_OBJS}
	$(LD) $(LDFLAGS) svm_struct_api.o svm_struct/svm_struct_classify.o svm_light/svm_common.o svm_struct/svm_struct_common.o ${CUSTOM_OBJS} ${GRAPHS_OBJS} -o svm_empty_classify $(LIBS)

svm_empty_learn: ${NO_EXES} svm_struct_api.o svm_struct_learn_custom.o svm_struct/svm_struct_learn.o svm_struct/svm_struct_common.o svm_struct/svm_struct_main.o ${CUSTOM_OBJS} ${GRAPHS_OBJS}
	$(LD) $(LDFLAGS) svm_struct/svm_struct_learn.o svm_struct_learn_custom.o svm_struct_api.o svm_light/svm_hideo.o svm_light/svm_learn.o svm_light/svm_common.o svm_struct/svm_struct_common.o svm_struct/svm_struct_main.o ${CUSTOM_OBJS} ${GRAPHS_OBJS} -o svm_empty_learn $(LIBS)

svm_struct_api.o: svm_struct_api.cpp svm_struct_api.h svm_struct_api_types.h svm_struct/svm_struct_common.h minSumWithAppearance.h featureBank.h common.h
	$(CC) -c $(CFLAGS) svm_struct_api.cpp -o svm_struct_api.o

svm_struct_learn_custom.o: svm_struct_learn_custom.cpp svm_struct_api.h svm_light/svm_common.h svm_struct_api_types.h svm_struct/svm_struct_common.h minSumWithAppearance.h
	$(CC) -c $(CFLAGS) svm_struct_learn_custom.cpp -o svm_struct_learn_custom.o

minSumWithAppearance.o: minSumWithAppearance.cpp svm_struct_api_types.h ${GRAPHS_OBJS} ${CUSTOM_OBJS}
	$(CC) -c $(CFLAGS) minSumWithAppearance.cpp -o minSumWithAppearance.o
features.o: features.cpp svm_struct_api_types.h features.h
	$(CC) -c $(CFLAGS) features.cpp -o features.o
common.o: common.cpp common.h
	$(CC) -c $(CFLAGS) common.cpp -o common.o
featureBank.o: featureBank.cpp featureBank.h
	$(CC) -c $(CFLAGS) featureBank.cpp -o featureBank.o
