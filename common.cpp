#include "common.h"
#include "computeU.h"
#include "svm_struct_api_types.h"
#include <sys/time.h>
#include <stdio.h>
#include <assert.h>

void assertEqual(double a, double b){
  if(fabs(a-b) > FLT_EPSILON){
    printf("values not equal: %lf, %lf\n", a, b);
    assert(0);
  }
}
void Error(const char *s){
  printf("%s\n", s);
  assert(0);
}
int64_t GetTimeInMicrosecs(void){
  struct timeval curtime;
  int err = gettimeofday(&curtime, (struct timezone *) NULL);
  if (err < 0) Error("Can't read time of day.");
  return  ((curtime.tv_sec * (int64_t) 1000000) + curtime.tv_usec);
}
double* allocLogs(SAMPLE *samp, int model_blur_inv){
  int maxlogs = 0;
  for(int i=0; i<samp->n; i++){
    for(int s=0; s<NSCALES; s++){
      PATTERN *x = &samp->examples[i].x[s];
      maxlogs = max(maxlogs,
          1+(30+model_blur_inv)*(x->imgsize + x->numMappings/NHYPS));
    }
  }
  //printf("maxlogs: %d\n", maxlogs);
  double *logs = (double*) malloc(sizeof(double)*maxlogs);
  return logs;
}
double* allocLogs(PATTERN *x, int model_blur_inv){
  int maxlogs = 1+(30+model_blur_inv)*(x->imgsize + x->numMappings/NHYPS);
  double *logs = (double*) malloc(sizeof(double)*maxlogs);
  //printf("maxlogs: %d\n", maxlogs);
  if(!logs){
    printf("maxlogs: %d\n", maxlogs);
    Error("Couldnt allocate space for logs!");
  }
  return logs;
}
void setLogsn(double *logs, double *nlogn, int *cur,
    int *curnlogn, int want){
  if(*cur < want){
    if( *cur &/*bitwise*/ 1){ // if odd
      for(int i=*cur+2; i<=want; i+=2)
        logs[i] = log2(i);
      //calculate the even logs using the odds
      for(int i=*cur+1; i<=want; i+=2)
        logs[i] = 1+logs[i>>1];
    } else {
      for(int i=*cur+1; i<=want; i+=2)
        logs[i] = log2(i);
      //calculate the even logs using the odds
      for(int i=*cur+2; i<=want; i+=2)
        logs[i] = 1+logs[i>>1];
    }
    *cur = want;
  }
  if(*curnlogn < want){
    for(int i=*curnlogn+1; i<=want; i++){
      nlogn[i] = i*logs[i];
      //printf("%d, %lf\n", i, nlogn[i]);
    }
    *curnlogn = want;
  }
}
// PID is in the range [0,NPARTS-1]
// HID is in the range [0,NHYPS-1]
int getNextIndex(PATTERN *x, int pid, int hid){
  if(pid<0 || hid<0 || pid>=NPARTS || hid>=NHYPS)
    Error("Problem in getNextIndex.");
  if(NPARTS == pid+1 && NHYPS == hid+1) return x->numMappings;
  if(NHYPS == hid+1){
    hid = 0;
    pid++;
  } else {
    hid++;
  }
  return x->fgmapIndices[pid][hid];
}
void printSymAndPair(double symAndPair[E][NHYPS][NHYPS], bool symonly){
  printf("symAndPair:\n");
  for(int i=0;i<E;i++){
    if(symonly && i>=NSYMS) break;
    for(int h1=0;h1<NHYPS;h1++){
      for(int h2=0; h2<NHYPS; h2++){
        printf("%f, ", symAndPair[i][h1][h2]);
      }
      printf("\n");
    }
    printf("%d _________________________________________________\n", i);
  }
}
int getModelTotal(Appmodel *m){
  int total = 0;
  for(int i=0; i<NUM_COLOR_BUCKETS; i++){
    for(int j=0; j<NUM_COLOR_BUCKETS; j++){
      for(int k=0; k<NUM_COLOR_BUCKETS; k++){
        total += m->m[i][j][k];
      }
    }
  }
  return total;
}
//TODO optimize
void setModel(Appmodel *m, int initval){
  for(int i=0; i<NUM_COLOR_BUCKETS; i++){
    for(int j=0; j<NUM_COLOR_BUCKETS; j++){
      for(int k=0; k<NUM_COLOR_BUCKETS; k++){
        m->m[i][j][k] = initval;
      }
    }
  }
}
void discretize(struct buckets *b, unsigned char *img, int pixel, int calledFrom){
  b->r = ((unsigned int)img[3*pixel+0])/NUM_VALS_PER_BUCKET;
  b->g = ((unsigned int)img[3*pixel+1])/NUM_VALS_PER_BUCKET;
  b->b = ((unsigned int)img[3*pixel+2])/NUM_VALS_PER_BUCKET;
}
void setSymTotals(int totals[NPARTS][NHYPS][SUBPART_LEVELS][MAX_PARTS],
    Appmodel sym_models[NPARTS][NHYPS][SUBPART_LEVELS][MAX_PARTS]){
  for(int p=0; p<NPARTS; p++){
    for(int h=0; h<NHYPS; h++){
      for(int spl=0; spl<SUBPART_LEVELS; spl++)
        for(int sp=0; sp<(spl+1)*(spl+1); sp++)
          totals[p][h][spl][sp] = getModelTotal(sym_models[p][h][spl]+sp);
      //printf("%d, ", totals[p][h]);
    }
    //printf("\n");
  }
  //Error("TESTING");
}
//TODO appmodels is the same as the first level of sym_models.
void computeModels(PATTERN *x, //Appmodel appmodels[NPARTS][NHYPS],
    Appmodel sym_models[NPARTS][NHYPS][SUBPART_LEVELS][MAX_PARTS],
    CONFIGS *config){
  /*
  if(appmodels)
    for(int i=0; i<NPARTS; i++)
      for(int j=0; j<NHYPS; j++)
        initModel(&appmodels[i][j]);
        */
  if(sym_models)
    //TODO optimize
    for(int i=0; i<NPARTS; i++)
      for(int j=0; j<NHYPS; j++)
        for(int spl=0; spl<SUBPART_LEVELS; spl++)
          for(int sp=0; sp<(spl+1)*(spl+1); sp++){
            //printf("%d %d %d %d\n", i,j,spl,sp);
            setModel(sym_models[i][j][spl]+sp, config->app_model_prior);
          }
  // Loop through the hypotheses for each part and add some weight to the foregound
  // for pixels in the map for that part/hypothesis.
  for(int i=0; i<x->numMappings; i++){
    int p = (int)x->fgmap[i].pid-1;//subtract 1 to convert to 0 indexed system
    int h = (int)x->fgmap[i].hid-1;
    if(p<0 || p>=NPARTS || h<0 || h>=NHYPS){
      printf("p %d, h %d\n", p,h);
      Error("problem in computeModels.");
    }
    int *subparts = x->fgmap[i].subparts;
    int pixel = (int)x->fgmap[i].pixel-1;
    struct buckets buck;
    discretize(&buck, x->image, pixel, 5);
    if(sym_models){
      for(int j=0; j<SUBPART_LEVELS; j++){
        assert(subparts[j]>=0 && subparts[j]<(j+1)*(j+1));
        sym_models[p][h][j][subparts[j]].m[buck.r][buck.g][buck.b] += config->model_blur_inv;
      }
    }
    if(config->model_blur){
      for(int r=MAX(0,buck.r-1); r<=MIN(NUM_COLOR_BUCKETS-1,buck.r+1); r++){
      for(int g=MAX(0,buck.g-1); g<=MIN(NUM_COLOR_BUCKETS-1,buck.g+1); g++){
      for(int b=MAX(0,buck.b-1); b<=MIN(NUM_COLOR_BUCKETS-1,buck.b+1); b++){
        //if(appmodels)
        //  appmodels[p][h].m[r][g][b] += 1;
        if(sym_models){
          for(int j=0; j<SUBPART_LEVELS; j++){
            assert(subparts[j]>=0 && subparts[j]<(j+1)*(j+1));
            sym_models[p][h][j][subparts[j]].m[r][g][b] += 1;
          }
        }
      }}}
      // Add extra weight on the diagonals to emphasize "color" rather than lighting
      if(buck.r+1<NUM_COLOR_BUCKETS && buck.g+1<NUM_COLOR_BUCKETS &&
          buck.b+1<NUM_COLOR_BUCKETS){
        //if(appmodels)
        //  appmodels[p][h].m[buck.r+1][buck.g+1][buck.b+1] += 2;
        if(sym_models){
          for(int j=0; j<SUBPART_LEVELS; j++){
            assert(subparts[j]>=0 && subparts[j]<(j+1)*(j+1));
            sym_models[p][h][j][subparts[j]].m[buck.r+1][buck.g+1][buck.b+1] += 2;
          }
        }
      }
      if(buck.r>0 && buck.g>0 && buck.b>0){
        //if(appmodels)
        //  appmodels[p][h].m[buck.r-1][buck.g-1][buck.b-1] += 2;
        if(sym_models){
          for(int j=0; j<SUBPART_LEVELS; j++){
            assert(subparts[j]>=0 && subparts[j]<(j+1)*(j+1));
            sym_models[p][h][j][subparts[j]].m[buck.r-1][buck.g-1][buck.b-1] += 2;
          }
        }
      }
    }
  }
}
double computeModelDivergence(Appmodel *m1, Appmodel *m2,
    int total1, int total2){
  if(total1<=0 || total2<=0){
    printf("total1 %d, total2 %d\n", total1, total2);
    Error("Error in computeModelDivergence.");
  }
  double div = 0.0;
  for(int i=0; i<NUM_COLOR_BUCKETS; i++){
    for(int j=0; j<NUM_COLOR_BUCKETS; j++){
      for(int k=0; k<NUM_COLOR_BUCKETS; k++){
        div += divergence2(
            ((double)m1->m[i][j][k])/total1,
            ((double)m2->m[i][j][k])/total2);
      }
    }
  }
  if(div != div){ // checks if it's nan
    //printf("Error: %d, %d, %d, %f, %f, totals: %f, %f\n",
    //    i,j,k, m1->m[i][j][k], m2->m[i][j][k], total1, total2);
    Error("div is NaN in computeModelDivergence.");
  }
  return div;
}
void computeModelDivergences( PATTERN *x,
    Appmodel sym_models[NPARTS][NHYPS][SUBPART_LEVELS][MAX_PARTS],
    JOINT *symm_joints, double *nlogn){
  assert(nlogn);
  // cache the totals beforehand
  int (*totals)[NHYPS][SUBPART_LEVELS][MAX_PARTS] =
    (int(*)[NHYPS][SUBPART_LEVELS][MAX_PARTS])
    malloc(sizeof(int)*NPARTS*NHYPS*SUBPART_LEVELS*MAX_PARTS);
  setSymTotals(totals, sym_models);
  for(int i=0;i<NSYMS;i++){
    // symm_joints is 1 indexed
    int p1 = symm_joints[i].j1 - 1;
    int p2 = symm_joints[i].j2 - 1;
    assert(p1>=0 && p2>=0 && p1<NPARTS && p2<NPARTS);
    for(int h1=0; h1<NHYPS; h1++){// iterate through hypotheses for part 1
      for(int h2=0; h2<NHYPS; h2++){// iterate through hypotheses for part 2
        x->symAndPair[i][h1][h2] = 0.0;
        for(int spl=0; spl<SUBPART_LEVELS; spl++){
          for(int sp=0; sp<(spl+1)*(spl+1); sp++){
            x->symAndPair[i][h1][h2] +=
              computeModelDivergence(
                  sym_models[p1][h1][spl]+sp,
                  sym_models[p2][h2][spl]+sp,
              //computeU7(
                  //(int*) sym_models[p1][h1][spl][sp].m,
                  //(int*) sym_models[p2][h2][spl][sp].m,
                  totals[p1][h1][spl][sp],
                  totals[p2][h2][spl][sp]);
                  //nlogn);
          }
        }
        if(x->symAndPair[i][h1][h2] != x->symAndPair[i][h1][h2]){
          printf("symAndPair i %d, h1 %d, h2 %d, %lf\n",
              i, h1, h2, x->symAndPair[i][h1][h2]);
          assert(0);
        }
        //computeModelDivergence( appmodels[p1]+h1, appmodels[p2]+h2,
        //    totals[p1][h1], totals[p2][h2]);
      }
    }
  }
  free(totals);
  // For debugging
  if(0) printSymAndPair( x->symAndPair, false);
}
