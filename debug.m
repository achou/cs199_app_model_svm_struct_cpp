function [] = debug()
h=20;
for incb=0:1
  v1=zeros(2,2);
  v2=zeros(102,2);
  v3=zeros(102,2);
  v4=zeros(102,2);
  v5=zeros(102,2);
  v6=zeros(102,2);
  v7=zeros(102,2);
  v8=zeros(102,2);
  v9=zeros(102,2);
  v10=zeros(102,2);
  for i=1:100
    v1 = v1+ readFile(h, incb, 1, i);
    v2 = v2+ readFile(h, incb, 2, i);
    v3 = v3+ readFile(h, incb, 3, i);
    v4 = v4+ readFile(h, incb, 4, i);
    v5 = v5+ readFile(h, incb, 5, i);
    v6 = v6+ readFile(h, incb, 6, i);
    v7 = v7+ readFile(h, incb, 7, i);
    v8 = v8+ readFile(h, incb, 8, i);
    v9 = v9+ readFile(h, incb, 9, i);
    v10 = v10+ readFile(h, incb, 10, i);
  end
  v1 = v1(:,1);
  v2 = v2(:,1);
  v3 = v3(:,1);
  v4 = v4(:,1);
  v5 = v5(:,1);
  v6 = v6(:,1);
  v7 = v7(:,1);
  v8 = v8(:,1);
  v9 = v9(:,1);
  v10 = v10(:,1);

  len = length(v2);
  v1 = [ v1; v1(end)*ones(len-length(v1),1)];

  x = 1:len;

  figure
  plot( x, v1, x, v2, x, v3, x, v4, x, v5,...
    x, v6, x, v7, x, v8, x, v9, x, v10);
  legend('hc','2','3','4','5','6','7','8','9','10', 'Location', 'SouthEast');
  if incb==1
    title(['Including block part in appearance (', int2str(h), ' hyps)']);
  else
    title(['NOT Including block part in appearance (', int2str(h), ' hyps)']);
  end
  xlabel('iterations');
  ylabel('objective function');
end
end
function v = readFile(h, incb, b, i)
name = ['logsincb/', 'logsincb', int2str(incb), '_'];
name = [name, int2str(h), '_', int2str(b), '/log_', int2str(i), '.txt'];
v = load(name);
end
