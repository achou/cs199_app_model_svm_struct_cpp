#ifndef DOUBLEPOOL_H
#define DOUBLEPOOL_H

#include <stdlib.h>
#include <vector>
using namespace std;

class doublepool;
class doublepool{ /* one heap for incoming msg, one heap for initial factors */


public:
  // for simplicity, always assume the first factor in fset is the incoming msg, all others are initial factors

  vector<int> f;

  int KK;
  vector<double> msg;                          // 1 table

  vector< vector< void* > > handle;            // 2 tables
  struct fibheap *data_msg;
  struct fibheap *data_init;

  vector< vector<int> > inpool;                // 2 tables

  double min_key_msg;
  double min_key_init;

  void reset_incoming_msg();

  doublepool(): data_msg(NULL), data_init(NULL) {}
  //doublepool(factorset & _fset, int first_node);
  doublepool(const doublepool & dp);
  ~doublepool();
  void free_mem();
  void add(int fid, long eid, double val);
  void add_init_factor(int fid, long eid, double val);
  void decrease_key(int factor_id, long entry_id, double val);
  void decrease_key_init_factor(int factor_id, long entry_id, double val);
  void extract_min(int & factor_id, long & entry_id, double min_incoming_msg, double minf); // this returns the actual factor value
  double min_key(double min_incoming_msg, double minf); // this returns (actual value + min_incoming_msg) for intial factors
};

#endif
