#ifndef FACTOR_H
#define FACTOR_H
#include <vector>
using namespace std;
class factor {  /* minus log factors (energy terms, to be added and minimized) */

public:

  int size;      // num of variables
  vector<long> stride;
  vector<long> card;    // cardinality for each variable
  vector<int> id;      // identify for each variable
  vector<double> phi;  // actual numbers
  long n;        // dim of phi

  factor();
  //factor(int _size, int *_id, long *_card, double val);
  factor(int _size, vector<int> _id, vector<long> _card, double val);
  factor(int _size, int K);
  factor(int _size, int K, double val);
  factor(int _size, int K, const vector<double> & data);

  /* if val >= 0, all entries = val, else, all entries = rand */

  factor(const factor & f);

  ~factor();

  void set_to_inf();

  double get(long *assignment);
  void set(long *assignment, double val);
  void set_1factor(long idx, double val);
  void set_2factor(long idx1, long idx2, double val);

  double minimize(long *assignment) const;
  double subtract_min();
  void get_assignment(long idx, long *assignment) const;
  void get_assignment(long idx, vector<long> & assignment) const;
  long get_idx(vector<int>  & assignment);

  void swap_ij(); /* for size two factors only */

  void reduce1(); /* minimize of the first variable for a factor of size 2 */
  void reduce2(); /* minimize of the second variable for a factor of size 2 */
  void reduce(int variable_id); /* minimize over one variable */
  void subtract(factor & f);
  void add(factor & f, double coefficient);
  void add_i_to_ij(factor & f); /* fast version of add for special case */
  void add_j_to_ij(factor & f); /* fast version of add for special case */
  void add_ij_to_ij(factor & f); /* fast version of add for special case */
  void add(factor & f);

  void subtract_const(double c);

  factor & operator=(const factor & rhs);

  void print();

private:
  int find(int a, int n, vector<int> & v);
};

#endif
