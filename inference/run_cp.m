
output = 'log.txt';

num_iter = 1000;  % max num of iter

graph_size = 5;   % num of variables
cardinality = 100; % size of state space

graph_type = 2; % 0: nxn grid, 1: bicycle, 2: fully connected

%random_seed = 228;
random_seed = floor(rand()*10000);

% flag = 0: singletons=zeros, pairwise=U[0,1]   (harder problems)
% flag = 1: singletons=U[0,1], pairwise=U[0,1]  (easier problems)
flag = 1;

cmd = sprintf('./run_cp %d %s %d %d %d %d %d', num_iter, output, graph_size, ...
	      cardinality, flag, graph_type, random_seed);

system(cmd);

plot_primal_dual;
