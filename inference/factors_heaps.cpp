// Circle Pursuit
// by Huayan Wang <huayanw@cs.stanford.edu>

// factors_heaps.cpp: implements data structures and basic operations for factors and heaps

#include "factors_heaps.h"
using namespace std;

void bounded_pff_min_sum_old(int K, const vector<double> & Mij, const vector<double> & Mjk, vector<double> & Mik, double min_energy, double & time)
{
  factor ij(2, K, Mij);
  factor jk(2, K, Mjk);
  factor ik(2, K, Mik);

  Mik.assign(K*K, INF);

  double min_ij = ij.minimize(NULL);
  double min_jk = jk.minimize(NULL);
  double min_ik = ik.minimize(NULL);

  for(int i=0; i<ij.n; i++)
    ij.phi[i] -= min_ij;
  for(int i=0; i<jk.n; i++)
    jk.phi[i] -= min_jk;

  min_energy -= (min_ij+min_jk);

  //int id[] = {ij.id[0], jk.id[1]};
  //long card[] = {K, K}; // assuming all variables has the same cardinality

  factorset fset = factorset(ij);
  fset.add_factor(jk);
  pool P = pool(fset);
  int fid, assign0,assign1;
  long eid,e0,e1;
  double sum;

  clock_t t = clock();
  while(P.min_key() <= min_energy-min_ik + 1e-8){
    double minval = P.extract_min(fid, eid);
    //num_pops++;
    assert(fid < 2);
    assign0 = (int)floor((double)eid)%K;
    assign1 = (int)floor((double)eid/K)%K;
    if(fid == 0){
      e0 = assign1; e1 = assign0;
    }else{
      e0 = assign0*K; e1 =assign1*K;
    }
    for(int idx=0; idx < K; idx++){

      //      if(e0==50 && e1==57)
      //        assert(1==0);

      if(!(P.inpool[1-fid][e0])){
        sum = minval + P.fset.factors[1-fid].phi[e0];
        Mik[e1] = MIN( Mik[e1], sum + ik.phi[e1]);
        //if( fabs(sum + ik.phi[e1] - min_energy) < 1e-6)
          //cout << endl << "YES" << endl;
      }
      if(fid == 0){
        e0 += K; e1 += K; // e1 = assign0 + idx*K
      }else{
        e0 ++; e1 ++;  // e1 = idx + assign1*K
      }
    }
  }

  //assert(fabs(minimize(Mik)-min_energy) < 1e-6);

  for(int i=0; i<K*K; i++)
    Mik[i] += (min_ij + min_jk);

  time = ((double)clock()-t)/CLOCKS_PER_SEC;
}

void bounded_pff_min_sum(int K, const vector<double> & Mij, const vector<double> & Mjk, vector<double> & Mik, double min_energy, double & time)
{
  factor ij(2, K, Mij);
  factor jk(2, K, Mjk);
  factor ik(2, K, Mik);

  Mik.assign(K*K, INF);

  double min_ij = ij.minimize(NULL);
  double min_jk = jk.minimize(NULL);
  double min_ik = ik.minimize(NULL);

  for(int i=0; i<ij.n; i++)
    ij.phi[i] -= min_ij;
  for(int i=0; i<jk.n; i++)
    jk.phi[i] -= min_jk;

  min_energy -= (min_ij+min_jk);

  //int id[] = {ij.id[0], jk.id[1]};
  //long card[] = {K, K}; // assuming all variables has the same cardinality

  factorset fset = factorset(ij);
  fset.add_factor(jk);
  multipool P = multipool(fset);
  int fid, assign0,assign1;
  long eid,e0,e1;
  double sum;

  clock_t t = clock();
  while(P.min_key() <= min_energy-min_ik + 1e-8){
    double minval = P.extract_min(fid, eid);
    //num_pops++;
    assert(fid < 2);
    assign0 = (int)floor((double)eid)%K;
    assign1 = (int)floor((double)eid/K)%K;
    if(fid == 0){
      e0 = assign1; e1 = assign0;
    }else{
      e0 = assign0*K; e1 =assign1*K;
    }
    for(int idx=0; idx < K; idx++){

      if(!(P.inpool[1-fid][e0])){
        sum = minval + P.fset.factors[1-fid].phi[e0];
        Mik[e1] = MIN( Mik[e1], sum + ik.phi[e1]);
        //if( fabs(sum + ik.phi[e1] - min_energy) < 1e-6)
          //cout << endl << "YES" << endl;
      }
      if(fid == 0){
        e0 += K; e1 += K; // e1 = assign0 + idx*K
      }else{
        e0 ++; e1 ++;  // e1 = idx + assign1*K
      }
    }
  }

  //assert(fabs(minimize(Mik)-min_energy) < 1e-6);

  for(int i=0; i<K*K; i++)
    Mik[i] += (min_ij + min_jk);

  time = ((double)clock()-t)/CLOCKS_PER_SEC;
}

vector<double> pff_min_sum(int K, const vector<double> & Mij, const vector<double> & Mjk, vector<int> & argmin_ik)
{

  factor ij(2, K, Mij);
  factor jk(2, K, Mjk);

  double min_ij = ij.minimize(NULL);
  double min_jk = jk.minimize(NULL);
  for(int i=0; i<ij.n; i++)
    ij.phi[i] -= min_ij;
  for(int i=0; i<jk.n; i++)
    jk.phi[i] -= min_jk;

  //long num_pops = 0;

  long computed = 0;
  vector<int> id(2,0);
  id[0] = ij.id[0];
  id[1] = jk.id[1];

  //int id[] = {ij.id[0], jk.id[1]};
  vector<long> card(2,K);
  //long card[] = {K, K}; // assuming all variables has the same cardinality
  factor ik = factor(2, id, card, INF);
  factorset fset = factorset(ij);
  fset.add_factor(jk);
  fset.add_factor(ik);
  pool P = pool(fset);
  int fid, assign0,assign1;
  long eid,e0,e1;
  double sum;
  while(computed < ik.n && IsNotInf(P.min_key())){
    double minval = P.extract_min(fid, eid);
    //num_pops++;
    if (fid < 2){
      assign0 = (int)floor((double)eid)%K;
      assign1 = (int)floor((double)eid/K)%K;
      if(fid == 0){
        e0 = assign1; e1 = assign0;
      }else{
        e0 = assign0*K; e1 =assign1*K;
      }
      for(int idx=0; idx < K; idx++){
        if(!(P.inpool[1-fid][e0])){
          sum = minval + P.fset.factors[1-fid].phi[e0];
          if ( sum < P.fset.factors[2].phi[e1] ){
            P.decrease_key(2, e1, sum);
            argmin_ik[e1] = (fid==0?assign1:assign0);
          }
        }
        if(fid == 0){
          e0 += K; e1 += K; // e1 = assign0 + idx*K
        }else{
          e0 ++; e1 ++;  // e1 = idx + assign1*K
        }
      }
    }else{
      computed++;
    }
  }
  vector<double> Mik(K*K, 0);
  assert(P.fset.factors[2].n == K*K);
  for(int i=0; i<P.fset.factors[2].n; i++)
    Mik[i] = P.fset.factors[2].phi[i] + min_ij + min_jk;
  return Mik;
}

factorset::factorset()
{

}

factorset::factorset(const factorset & f)
{
  id = f.id;
  factors = f.factors;
}

factorset::factorset(factor f)
{
  for(int i=0; i<f.size; i++)
    id.push_back(f.id[i]);
  factors.push_back(f);
}

void factorset::add_factor(factor f)
{

  factors.push_back(f);
  for(int i=0; i<f.size; i++){
    int exist = 0;
    for(int j=0; j<id.size(); j++){
      if (id[j] == f.id[i]){
        exist = 1;
        break;
      }
    }
    if (!exist)
      id.push_back(f.id[i]);
  }
}

void factorset::add_factor(factor f, int pos)
{

  factors.insert(factors.begin()+pos, f);
  for(int i=0; i<f.size; i++){
    int exist = 0;
    for(int j=0; j<id.size(); j++){
      if (id[j] == f.id[i]){
        exist = 1;
        break;
      }
    }
    if (!exist)
      id.push_back(f.id[i]);
  }
}

factor factorset::sum_up_all(){

  factor result = factor(factors[0]);
  for (int i=1; i<factors.size(); i++){
    result.add(factors[i]);
  }
  return result;
}

void factorset::combine_factors(int size_limit){ /* combine factors s.t. the size of resulted factors does not exceed limit */

}

double factorset::simple_lower_bound(){ /* find lower bound by adding up factor-mins */

  double result = 0;
  for (int i=0; i<factors.size(); i++)
    result += factors[i].minimize(NULL); /* assignment is ignored */
  return result;

}

void factorset::print(){

 cout << " --------------------------------------- " << endl;
  for (int i=0; i<factors.size(); i++){
    factors[i].print();
    cout << " --------------------------------------- " << endl;
  }
}

shelf::shelf(int N, int K){

  height = 3;
  for(int i=0;i<height;i++){
    shelf_pos[i].assign(K*K,-1);
    data_val.assign(height, vector<double>());
    data_id.assign(height, vector<long>());
    data_assignment.assign(height, vector< vector<long> >());
  }
}

shelf::shelf(factorset & _fset){

  fset = _fset;
  int n = fset.factors.size();
  height = n;
  data_val.assign(n, vector<double>());
  data_id.assign(n, vector<long>());
  data_assignment.assign(n, vector< vector<long> >());
  iterator_state = -1;
  iterator_pos.assign(n,-1);
  id.assign(n, vector<int>());
  id_union = fset.id;
  full_assignment.assign(fset.id.size(),-1);
  //onshelf.assign(n, vector<int>());
  for(int i=0; i<n; i++){
    //onshelf[i].assign(fset.factors[i].n, 0);
    id[i].clear();
    for(int j=0; j<fset.factors[i].size; j++)
      id[i].push_back(fset.factors[i].id[j]);
  }
}

int match(vector<int> a, vector<int> b)
{
  if (a.size()!=b.size())
    return 0;
  for(int i=0; i<a.size(); i++)
    if (a[i] != b[i])
      return 0;
  return 1;
}

void shelf::pop(int factor_id, long entry_id){
  long pos = shelf_pos[factor_id][entry_id];
  shelf_pos[factor_id][entry_id] = -1;
  data_id[factor_id].erase(data_id[factor_id].begin()+pos);
  data_val[factor_id].erase(data_val[factor_id].begin()+pos);
  data_assignment[factor_id].erase(data_assignment[factor_id].begin()+pos);
}

void shelf::push(int factor_id, long entry_id, double val, const vector<long> & xi){

  data_val[factor_id].push_back(val);
  data_id[factor_id].push_back(entry_id);
  data_assignment[factor_id].push_back(xi);
  shelf_pos[factor_id][entry_id] = data_val[factor_id].size()-1;
}


factorset & factorset::operator=(const factorset & rhs)
{
  id.clear();
  id = rhs.id;
  factors.clear();
  factors = rhs.factors;
  return *this;
}

void shelf::init_iterator(int factor_id, long entry_id, double val, int output_factor_id)
{
  output_assignment.assign(id[output_factor_id].size() , -1);

  current_val = val;
  current_entry_id = id[factor_id];
  current_entry_assignment.assign(current_entry_id.size(), 0);
  fset.factors[factor_id].get_assignment(entry_id, current_entry_assignment);

  iterator_state = 0; /* valid */
  output_factor = output_factor_id;

  iterator_pos[output_factor_id] = -2; /* output factor */
  iterator_pos[factor_id] = -2; /* output factor */

  for(int i=0; i<height; i++){
    if (i==output_factor_id || i==factor_id)
      iterator_pos[i] = -2;
    else{
      if (data_val[i].size() > 0)
        iterator_pos[i] = 0;
      else{
        iterator_pos[i] = -1;
        iterator_state = 1; /* terminated */
      }
    }
  }
 }


int commit_assignment(vector<long> & full_assignment, const vector<int> & id_union,
                      const vector<long> & assign, const vector<int> & id)
{
  for(int i=0; i<assign.size(); i++){
    for(int j=0; j<full_assignment.size(); j++){
      if(id_union[j] == id[i]){
        if(full_assignment[j] < 0)
          full_assignment[j] = assign[i];
        else if(full_assignment[j] != assign[i])
          return 0;
      }
    }
  }
  return 1;
}

double shelf::find_next_sum(long & output_entry_id) /* return -1 when the iteration has terminated */
{
  if (iterator_state != 0)
    return -1;

  full_assignment.assign(id_union.size(), -1);
  commit_assignment(full_assignment, id_union,
                    current_entry_assignment, current_entry_id);


  /* compute sum at the current position of iterator */
  double val = current_val;
  for (int i=0; i<height; i++){

    if(iterator_pos[i] == -1){
      iterator_state = 1;
      return -1;
      assert(1==0); /* this should not happen */
    }

    if(iterator_pos[i] >= 0){
      if(commit_assignment(full_assignment, id_union,
                           data_assignment[i][iterator_pos[i]],
                           id[i]) == 0){
        val = INF;
        break;
      }else{
        val += data_val[i][iterator_pos[i]];
      }
    }
  }

  /* find output_entry_id */
  if(val < INF){
    //if (id[output_factor].size() == 0){
    //  outptu_entry_id = 0;
    //}else{
      for (int i=0; i<id_union.size(); i++){
        for (int j=0; j<id[output_factor].size(); j++){
          if(id_union[i] == id[output_factor][j]){
            output_assignment[j] = full_assignment[i];
          }
        }
      }
      output_entry_id = fset.factors[output_factor].get_idx(output_assignment);
      //}
  }

  /* advance the iterator */
  bool valid = false;
  for (int i=0; i<height; i++){
    if(iterator_pos[i] == -2) continue; /* self */
    iterator_pos[i]++;
    if(iterator_pos[i] == data_val[i].size()){
      iterator_pos[i] = 0;
    }else{
      valid = true;
      break;
    }
  }
  if(!valid){
    iterator_state = 1;
  }

  return val;
}



pool::pool()
{

}

pool::~pool()
{
  fh_deleteheap(data);
}


multipool::multipool()
{
  n = 0;
}

void multipool::free_mem()
{
  for(int i=0; i<n; i++)
    if(data[i]!=NULL)
      fh_deleteheap(data[i]);
}


multipool::~multipool()
{
}

spool::spool()
{

}

spool::~spool()
{
  fh_deleteheap(data);
}

pool::pool(factorset & _fset)
{
  n =  _fset.factors.size();
  fset = _fset;
  data = fh_makekeyheap();
  inpool.assign(n, vector<int>());
  onshelf.assign(n, vector<int>());
  handle.assign(n, vector<void*>());
  for(int i=0; i<n; i++){
    inpool[i].assign(fset.factors[i].n, 1);
    onshelf[i].assign(fset.factors[i].n, 0);
    for(int j=0; j<fset.factors[i].n; j++){
      handle[i].push_back(fh_insertkey(data, fset.factors[i].phi[j], (void*)(j*n+i)));
    }
  }
}

multipool::multipool(int _n, int K)
{
  int size = K*K;
  n = _n;
  fset = factorset(factor(2,K,INF));
  fset.add_factor(factor(2,K,INF));
  data.assign(n, NULL);
  handle.assign(n, vector<void*>(size, (void*)0));
  inpool.assign(n,vector<int>(size, 0));
  onshelf.assign(n,vector<int>(size, 0));
  for(int i=0; i<n; i++)
    data[i] = fh_makekeyheap();
  cover_value.assign(n,INF);
  min_value.assign(n,INF);
  min_key_val.assign(n,INF);
}

multipool::multipool(factorset & _fset)
{
  n = _fset.factors.size();
  fset = _fset;
  data.assign(n, NULL);
  for(int i=0; i<n; i++)
    data[i] = fh_makekeyheap();
  inpool.assign(n,vector<int>());
  onshelf.assign(n,vector<int>());
  handle.assign(n, vector<void*>());
  cover_value.assign(n,0);
  min_value.assign(n,0);
  min_key_val.assign(n,INF);
  //double sum=0;
  for(int i=0; i<n; i++){
    inpool[i].assign(fset.factors[i].n, 0);
    onshelf[i].assign(fset.factors[i].n, 0);
    handle[i].assign(fset.factors[i].n, (fibheap_el*)NULL);
    for(int j=0; j<fset.factors[i].n; j++){
      if(IsNotInf(fset.factors[i].phi[j])){
        handle[i][j] = fh_insertkey(data[i], fset.factors[i].phi[j], (void*)(j*n+i));
        inpool[i][j] = 1;
      }
    }
    min_value[i] = minimize(fset.factors[i].phi);
    min_key_val[i] = fh_minkey(data[i]);
    assert(min_key_val[i] == min_value[i]);
    assert(min_key_val[i] == fh_minkey(data[i]));
    //sum += min_value[i];
  }
  update_cover_values();
  //for(int i=0; i<n; i++)
  //cover_value[i] = sum - min_value[i];
}

spool::spool(factor & _f, int put_in_pool)
{
  f = _f;
  data = fh_makekeyheap();
  onshelf.assign(f.n,0);
  if(put_in_pool){
    inpool.assign(f.n, 1);
    for(int j=0; j<f.n; j++){
      handle.push_back(fh_insertkey(data, f.phi[j], (void*)(j)));;
    }
    cnt = f.n;
  }else{
    inpool.assign(f.n, 0);
    handle.assign(f.n, (fibheap_el*)NULL);
    cnt = 0;
  }
}

pool::pool(factorset & _fset, vector<int> put_in_pool)
{
  n =  _fset.factors.size();
  fset = _fset;
  data = fh_makekeyheap();
  inpool.assign(n, vector<int>());
  onshelf.assign(n, vector<int>());
  handle.assign(n, vector<void*>());
  for(int i=0; i<n; i++){
    onshelf[i].assign(fset.factors[i].n, 0);
    if(put_in_pool[i]){
      inpool[i].assign(fset.factors[i].n, 1);
      for(int j=0; j<fset.factors[i].n; j++){
        handle[i].push_back(fh_insertkey(data, fset.factors[i].phi[j], (void*)(j*n+i)));
      }
    }else{
      inpool[i].assign(fset.factors[i].n, 0);
      handle[i].assign(fset.factors[i].n, (fibheap_el*)NULL);
    }
  }
}


void pool::add(int fid, long eid, double val)
{
  int i = fid;
  long j = eid;
  fset.factors[i].phi[j] = val;
  handle[i][j] = fh_insertkey(data, val, (void*)(j*n+i));
  inpool[i][j] = 1;
}

void multipool::add(int fid, long eid, double val)
{
  handle[fid][eid] = fh_insertkey(data[fid], val, (void*)(eid*n+fid));
  fset.factors[fid].phi[eid] = val;
  inpool[fid][eid] = 1;
  onshelf[fid][eid] = 0;
  if(val < min_value[fid]){
    min_value[fid] = val;
    update_cover_values();
  }
  min_key_val[fid] = fh_minkey(data[fid]);
  assert(min_key_val[fid] == fh_minkey(data[fid]));

}

void multipool::update_cover_values()
{
  cover_value.assign(n,0);
  assert(n==2);
  cover_value[0] = min_value[1];
  cover_value[1] = min_value[0];
    /*
  for(int i=0; i<n; i++)
    for(int j=0; j<n; j++)
      if(j!=i)
        cover_value[i] += min_value[j];
  // this may appear stupid, but its necessary to maintain numerical stability with possible inf's
  */
}

void spool::add(long eid, double val)
{
  f.phi[eid] = val;
  handle[eid] = fh_insertkey(data, val, (void*)(eid));
  inpool[eid] = 1;
  cnt++;
}

double spool::min_key()
{
  return fh_minkey(data);
}

double pool::min_key()
{
  return fh_minkey(data);
}

double multipool::min_key()
{
  double key = min_key_val[0]+cover_value[0];
  int min_id = 0;
  for(int i=1; i<n; i++)
    if(key > min_key_val[i]+cover_value[i]){
      key = min_key_val[i]+cover_value[i];
      min_id = i;
    }
  return key;
}


double spool::peek_min(long & entry_id)
{
  entry_id = (long)fh_min(data);
  return f.phi[entry_id];
}

double pool::peek_min(int & factor_id, long & entry_id)
{
  long idx = (long)fh_min(data);
  factor_id = idx%n;
  entry_id = (idx-factor_id)/n;
  return fset.factors[factor_id].phi[entry_id];
}

double spool::extract_min(long & entry_id)
{
  entry_id = (long)fh_extractmin(data);
  inpool[entry_id] = 0;
  cnt--;
  return f.phi[entry_id];
}

double pool::extract_min(int & factor_id, long & entry_id)
{
  long idx = (long)fh_extractmin(data);
  factor_id = idx%n;
  entry_id = (idx-factor_id)/n;
  inpool[factor_id][entry_id] = 0;
  return fset.factors[factor_id].phi[entry_id];
}

void multipool::empty_heap(int fid)
{
  int size = inpool[fid].size();
  inpool[fid].assign(size,0);
  while(IsNotInf(fh_minkey(data[fid])))
    fh_extractmin(data[fid]);
}

double multipool::extract_min(int & factor_id, long & entry_id)
{
  assert(fh_minkey(data[0]) == min_key_val[0]);
  assert(fh_minkey(data[1]) == min_key_val[1]);

  double key = min_key_val[0]+cover_value[0];
  int min_id = 0;
  for(int i=1; i<n; i++)
    if(key > min_key_val[i]+cover_value[i]){
      key = min_key_val[i]+cover_value[i];
      min_id = i;
    }
  factor_id = min_id;

  //assert(fh_minkey(data[min_id]) == min_key_val[min_id]);

  long idx = (long)fh_extractmin(data[min_id]);
  entry_id = (idx-factor_id)/n;
  inpool[factor_id][entry_id] = 0;
  onshelf[factor_id][entry_id] = 1;
  double val = min_key_val[min_id];
  min_key_val[min_id] =  fh_minkey(data[min_id]);

  assert(min_key_val[min_id] >= val);

  return fset.factors[factor_id].phi[entry_id];
}

void spool::decrease_key(long entry_id, double val)
{
  f.phi[entry_id] = val;
  fh_replacekey(data, (fibheap_el*)handle[entry_id], val);
}


void pool::decrease_key(int factor_id, long entry_id, double val)
{
  fset.factors[factor_id].phi[entry_id] = val;
  if(inpool[factor_id][entry_id])
    fh_replacekey(data, (fibheap_el*)handle[factor_id][entry_id], val);
}

void multipool::decrease_key(int factor_id, long entry_id, double val)
{
  fset.factors[factor_id].phi[entry_id] = val;
  fh_replacekey(data[factor_id], (fibheap_el*)handle[factor_id][entry_id], val);
  if(val < min_value[factor_id]){
    min_value[factor_id] = val;
    update_cover_values();
  }
  min_key_val[factor_id] = fh_minkey(data[factor_id]);
  assert(min_key_val[factor_id] == fh_minkey(data[factor_id]));
}


id_set::id_set(vector<int> a)
{
  data = a;
}

void id_set::intersect(vector<int> a)
{
  int i=0;
  while(i<data.size()){
    bool exist = 0;
    for(int j=0; j<a.size(); j++){
      if(a[j] == data[i]){
        exist = 1;
        break;
      }
    }
    if(!exist){
      data.erase(data.begin()+i);
    }else{
      i++;
    }
  }

}
