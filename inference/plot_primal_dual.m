
output_dir = './plots/';
mkdir(output_dir);

% read running log from log.txt
A = load('log.txt');

num_records = 5; % primal , dual , time , iter, num of cycle
num_settings = size(A,2)/num_records;

line_spec_dual = {'-r', '-g', '-b', '-k', '-c', '-m', '-y'};
line_spec_primal = {'--r', '--g', '--b', '--k', '--c', '--m', '--y'};
%line_spec_primal = {':r', ':g', ':b', ':k', ':c', ':m', ':y'};
line_wid = 1;
font_size = 20;

ht = figure;
hold on;
for i=1:num_settings
  record = A(:,(i-1)*num_records+1 : i*num_records);
  % plot dual vs primal against iter
  plot(record(:,4), record(:,1), line_spec_primal{i}, 'LineWidth', ...
       line_wid);
  plot(record(:,4), record(:,2), line_spec_dual{i}, 'LineWidth', ...
       line_wid);
end
ti = title('Colors: R, G, B, Black, Cyan, Magenta, Yellow');
yl = ylabel('Objective');
xl = xlabel('Iterations');
set(ti, 'FontSize', font_size);
set(yl, 'FontSize', font_size);
set(xl, 'FontSize', font_size);
filename = 'iter';
print(ht,'-dpdf',[output_dir filename]);
print(ht,'-dpng',[output_dir filename]);

hi = figure;
hold on;
for i=1:num_settings
  record = A(:,(i-1)*num_records+1 : i*num_records);
  % plot dual vs primal against time
  plot(record(:,3), record(:,1), line_spec_primal{i}, 'LineWidth', ...
       line_wid);
  plot(record(:,3), record(:,2), line_spec_dual{i}, 'LineWidth', ...
       line_wid);
end
ti = title('Colors: R, G, B, Black, Cyan, Magenta, Yellow');
yl = ylabel('Objective');
xl = xlabel('Time (seconds)');
set(ti, 'FontSize', font_size);
set(yl, 'FontSize', font_size);
set(xl, 'FontSize', font_size);
filename = 'time';
print(hi,'-dpdf',[output_dir filename]);
print(hi,'-dpng',[output_dir filename]);









