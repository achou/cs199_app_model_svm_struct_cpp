// Circle Pursuit
// by Huayan Wang <huayanw@cs.stanford.edu>

// run_cp.cpp: implements main algorithm

#include "factors_heaps.h"
#include "graphs.h"
#include <time.h>

/*
void test_covering_tree(){
  cp_graph G = cp_graph(5);
  G.random_weights(10, 0);
  G.show_connectivity();
  G.convert_into_covering_tree(0, 0);
  G.show_connectivity();
  G.show_msg_pass_order();
}

void test_covering_tree_calibrate(){

  cp_graph G = cp_graph(5);
  G.random_weights(10, 0);
  cp_graph T1 = cp_graph(G);
  cp_graph T2 = cp_graph(G);
  cp_graph T3 = cp_graph(G);

  T1.convert_into_covering_tree(3,0);
  T2.convert_into_covering_tree(3,7);
  T3.convert_into_covering_tree(3,24);

  double e1 = T1.min_sum_bp_covering_tree();
  double e2 = T2.min_sum_bp_covering_tree();
  double e3 = T3.min_sum_bp_covering_tree();

  cout << e1 << endl;
  cout << e2 << endl;
  cout << e3 << endl;
  print_vector(T1.assignment);
  print_vector(T2.assignment);
  print_vector(T3.assignment);
  }*/

void test_random_iterator()
{
  vector<int> ite = random_iterator(100);
  print_vector("ite", ite);
}

/*
void test_cycle_inference()
{
  clock_t t;
  for(int i=0; i<1; i++){

    cout << "ite " << i << endl;

    int N = 4;
    int K = 100;

    // create random problem
    cp_cycle c1(N, K, 1);
    cp_cycle c0(c1);
    cp_cycle c2(c1);

    c0.maintain_heaps = 0;
    c0.init_heaps();

    c1.maintain_heaps = 0;
    c1.init_heaps();

    c2.maintain_heaps = 2;
    c2.init_heaps();

    t = clock();
    double E, time;
    double E0 = c0.naive_min_sum_bp();
    printf("%.3f seconds\n", ((double)clock()-t)/CLOCKS_PER_SEC);

    t = clock();
    E = c0.fast_min_sum_bp_with_minmarginals(time);
    assert(fabs(E-E0)<EPS);
    vector<int> a = c0.assignment;
    printf("%.3f seconds\n", ((double)clock()-t)/CLOCKS_PER_SEC);

    t = clock();
    c1.assignment.assign(N+1,-1);
    E = c1.incomplete_bp_with_minmarginals(time);
    assert(fabs(E-E0)<EPS);
    for(int i=0; i<=N; i++)
      assert(c0.assignment[i] == a[i]);
    printf("%.3f seconds\n", ((double)clock()-t)/CLOCKS_PER_SEC);

    //

    t = clock();
    c2.assignment.assign(N+1,-1);
    E = c2.incomplete_bp_warmstart_with_minmarginals(time);
    assert(fabs(E-E0)<EPS);
    for(int i=0; i<=N; i++)
      assert(c2.assignment[i] == a[i]);
    printf("%.3f seconds\n", ((double)clock()-t)/CLOCKS_PER_SEC);

    //write_heap_state(c2.cy_P[0], c2.cy_min_incoming_msg[0], c2.cy_rlb[0], c2.cy_ub[0], c2.assignment, "c2.txt");

    double t0=0;
    double t1=0;
    double t2 = 0;
    double t0_basic = 0;
    double t1_basic = 0;
    double t2_basic = 0;

    int subset = 2;

     for(int perturb = 0; perturb < 10000; perturb++){
      if(subset == 0){ // perturbing factor entries
	int idx = rand()%N;
	printf("%d: (%d)\n", perturb, idx);
	for(int j=0; j< K*K/10; j++){
	  //for(int j=0; j<1; j++){
	  int l = rand()%(K*K);
	  double delta = randu(-0.05, 0.05);
	  c0.add_pairwise_update_heaps(idx,l,delta);
	  c1.add_pairwise_update_heaps(idx,l,delta);
	  c2.add_pairwise_update_heaps(idx,l,delta);
	}
      }else if(subset == 1){ // perturbing the entire factor
	int idx = rand()%N;
	//printf("%d: (%d)\n", perturb, idx);
	vector<double> delta(K*K, 0);
	for(int l=0; l<K*K; l++){
	  delta[l] = randu(-0.05, 0.05);
	}
	//c0.add_vec_pairwise_update_heaps(idx,delta);
	//c1.add_vec_pairwise_update_heaps(idx,delta);
	//c2.add_vec_pairwise_update_heaps(idx,delta);
      }else if(subset == 2){ // perturbing singletons
	int idx = rand()%N;
	for(int j=0; j< K/2; j++){
	  int l = rand()%K;
	  double delta = randu(-0.05, 0.05);
	  c0.add_singleton_update_heaps(idx,l,delta);
	  c1.add_singleton_update_heaps(idx,l,delta);
	  c2.add_singleton_update_heaps(idx,l,delta);
	}
      }

      c0.assignment.assign(N+1,-1);
      t = clock();
      E0 = c0.fast_min_sum_bp_with_minmarginals(time);
      t0 += ((double)clock()-t)/CLOCKS_PER_SEC;
      t0_basic += time;
      a = c0.assignment;

      c1.assignment.assign(N+1,-1);
      t = clock();
      E = c1.incomplete_bp_with_minmarginals(time);
      t1 += ((double)clock()-t)/CLOCKS_PER_SEC;
      t1_basic += time;
      assert(fabs(E - E0)<EPS);
      for(int i=0; i<=N; i++)
	assert(c1.assignment[i] == a[i] && c1.assignment[i] >= 0);

      c2.assignment.assign(N+1,-1);
      t = clock();
      E = c2.incomplete_bp_warmstart_with_minmarginals(time);
      t2 += ((double)clock()-t)/CLOCKS_PER_SEC;
      t2_basic += time;
      assert(fabs(E - E0)<EPS);
      for(int i=0; i<=N; i++)
	assert(c2.assignment[i] == a[i] && c2.assignment[i] >= 0);

      c2.check_heaps(0);
      c2.check_heaps(1);

      printf("%d: %.3f(%.3f) : %.3f(%.3f) : %.3f(%.3f)\n", perturb, t0, t0_basic, t1, t1_basic, t2, t2_basic);

      // compare the minmarginals obtained in c0(exact), c1, and c2
      // compare_minmarginals(c0, c1, c2);
      // cin.ignore();
    }
  }
}

// BUG with fibo heap: cannot increase key
void test_fibo_heap(){
  while(1){
    struct fibheap *data = NULL;
    data = fh_makekeyheap();
    vector< void* > handle(1000, NULL);
    for(int i=0; i<1000; i++)
      handle[i] = fh_insertkey(data, randu(0,1), (void*)((long)i));

    for(int j=0; j<100; j++){
      int idx = rand()%1000;
      fibheap_el* tmp = (fibheap_el*)handle[idx];
      fh_replacekey(data, (fibheap_el*)handle[idx], tmp->fhe_key - randu(0.0,0.1));
    }

    for(int i=0; i<10; i++){
      cout << fh_minkey(data) << endl;
      cout << (int)((long)fh_extractmin(data)) << endl;
    }
    fh_deleteheap(data);
  }
}*/

void test_dd(int graph_type, int size,
			     int cardinality, int flag, const char* output, long num_iter){


  cp_graph G = cp_graph(graph_type, size);  // line 1
  G.random_weights(cardinality, flag);      // line 2
  //G.read_weights("vals.txt", cardinality);

  /////////////// ANDREW ///////////////////////////////////////////////////
  // a random problem is specified above in G
  // you should have G specifying the problem you want to solve
  // specifically, if graph_type = 2, line 1 creates the fully connected graph
  // structure with number of variables given by "size"
  // then, line 2 assign random numbers to the potentials, all you need to do is
  // to go to that function and replace the singletons and pairwise potentials
  // with your numbers
  // if you were to write a function to replace "random_weights", you need to
  // closely follow what's been done in there.
  //////////////////////////////////////////////////////////////////////////

  //cout << G.graph_type << endl;

  int N = 10; // number of experiment settings (max 7: num of colors that Matlab can plot)
  int i=0;
  vector<cp_problem> P(N, cp_problem());

  int running_mode = 0; // 0==iteration, 1==time
  double running_duration = 1; // iterations / seconds

  /*
  {
    P[i] = cp_problem(G);
    P[i].cycle_method = 0;

    P[i].dual_method = 0;
    P[i].subgradient_stepsize = 0.01;
    }*/

  /*
  {
    i++;
    P[i] = cp_problem(G);
    P[i].cycle_method = 2;

    P[i].dual_method = 0;
    P[i].subgradient_stepsize = 0.01;
    P[i].warmstart_cycle = 0;
    }*/

  /*
  {
    i++;
    P[i] = cp_problem(G);
    P[i].cycle_method = 2;

    P[i].dual_method = 1;
    P[i].pbca_minmarginal = 0;
    P[i].damping = 0;
    P[i].warmstart_cycle = 0;
  }
  */


  {
    //i++;
    P[i] = cp_problem(G);
    P[i].cycle_method = 2;

    P[i].dual_method = 1;
    P[i].pbca_minmarginal = 2;
    P[i].damping = 0.5;
    P[i].warmstart_cycle = 1;
  }

  /*
  {
    i++;
    P[i] = cp_problem(G);
    P[i].cycle_method = 2;

    P[i].dual_method = 1;
    P[i].pbca_minmarginal = 2;
    P[i].damping = 0.2;
    P[i].warmstart_cycle = 1;
  }

  
  {
    i++;
    P[i] = cp_problem(G);
    P[i].cycle_method = 2;

    P[i].dual_method = 1;
    P[i].pbca_minmarginal = 2;
    P[i].damping = 0.5;
    P[i].warmstart_cycle = 1;
  }

  {
    i++;
    P[i] = cp_problem(G);
    P[i].cycle_method = 2;

    P[i].dual_method = 1;
    P[i].pbca_minmarginal = 2;
    P[i].damping = 0.7;
    P[i].warmstart_cycle = 1;
    }*/


  /*
  {
    i++;
    P[i] = cp_problem(G);
    P[i].cycle_method = 2;

    P[i].dual_method = 1;
    P[i].pbca_minmarginal = 2;
    P[i].damping = 0.5;
    P[i].warmstart_cycle = 1;
    }*/



  printf("comparing %d experimental settings\n", i+1);
  printf("running for %ld iterations\n", num_iter);

  for(int p=0; p<=i; p++){
    P[p].initialize();
  }

  FILE *fp = fopen(output, "w");
  fclose(fp);

  for(int ite=0; ite<num_iter; ite++){
    FILE *fp = fopen(output, "a");
    printf("%d: ", ite);
    fflush(stdout);
    for(int p=0; p<=i; p++){

      P[p].run(running_mode, running_duration);
      fprintf(fp, "%f %f %f %ld %d ", P[p].primal_obj, P[p].dual_obj, P[p].running_time, P[p].iter, (int)P[p].CYsize); // keep log
      if (i <= 4)
	printf("| (%d, %.1f) %.3f:%.3f ", P[p].cycles_solved, P[p].running_time_per_iteration, P[p].primal_obj, P[p].dual_obj);
      else
	printf("| (%.1f) %.3f ", P[p].running_time_per_iteration, P[p].dual_obj);
      fflush(stdout);

      if(i==0 &&  fabs(P[0].primal_obj-P[0].dual_obj)<EPS) // only one experiment setting is running and it's converged
	break;
    }
    fprintf(fp, "\n");
    printf("\n");
    fclose(fp);
    if(i==0 &&  fabs(P[0].primal_obj-P[0].dual_obj)<EPS)   // only one experiment setting is running and it's converged
      break;
  }

  ///////// ANDREW ///////////////////////////////////////////////
  // P[0].assignment is the optimal assignment
  // P[0].obj is minimal energy
  ////////////////////////////////////////////////////////////////

  for(int p=0; p<=i; p++)
    P[p].print_statistics();

  P[0].print_assignment_and_energy();

  /*
  cout << "press any key to do exhaustive search for debugging" << endl;
  cin.ignore();
  P[0].exhaustive_search();
  */

}

int main(int argc, char* argv[]){

#if DEBUG_MODE
  cout << "WARNING: DEBUG_MODE is on (very slow)" << endl;
#endif

  char output[200];

  int size = 5;

  long num_iter = 1000;
  //long num_iter = 200;

  int cardinality = 100;
  //int cardinality = 10;

  int graph_type = 2; // 0: nxn grid, 1: bicycle, 2: fully connected

  int flag = 1;
  // flag = 0: singletons=zeros, pairwise=U[0,1]  (hard problems)
  // flag = 1: singletons=U[0,1], pairwise=U[0,1] (easy problems)

  if(argc >= 2)
    num_iter = atol(argv[1]);
  if(argc >= 3)
    sprintf(output, "%s", argv[2]);
  else
    sprintf(output, "%s", "log.txt"); // default output filename
  if(argc >= 4)
    size = atoi(argv[3]);
  if(argc >= 5)
    cardinality = atoi(argv[4]);
  if(argc >= 6)
    flag = atoi(argv[5]);
  if(argc >= 7)
    graph_type = atoi(argv[6]);
  if(argc >= 8)
    srand(atoi(argv[7]));
  else
    srand(228);
  //srand(time(NULL));

  clock_t tic, toc;
  tic = clock();
  test_dd(graph_type, size, cardinality, flag, output, num_iter);
  toc = clock();
  printf("%.3f second\n", (double)(toc-tic)/CLOCKS_PER_SEC);
}
