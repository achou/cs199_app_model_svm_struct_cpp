#include "factor.h"
#include "fib.h"
#include <iostream>
#include <math.h>
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <algorithm>

factor::factor(){
  size = 0;
  n = 0;
}

factor::factor(int _size, int K)
{
  factor(_size, K, 0.0);
}

factor::factor(int _size, int K, double val)
{
  size = _size;
  if (size > 0){
    id.assign(size,0);
    card.assign(size,0);
    stride.assign(size,0);
    for (int i=0; i<size; i++){
      id[i] = i+1;
      card[i] = K;
    }
    stride[0] = 1;
    for(int i=1; i<size; i++)
      stride[i] = stride[i-1]*card[i-1];
    n = stride[size-1]*card[size-1];
  }else{
    n = 1;
  }
  phi.assign(n,val);
}


factor::factor(int _size, int K, const vector<double> & data)
{
  size = _size;
  if (size > 0){
    id.assign(size,0);
    card.assign(size,0);
    stride.assign(size,0);
    for (int i=0; i<size; i++){
      id[i] = i+1;
      card[i] = K;
    }
    stride[0] = 1;
    for(int i=1; i<size; i++)
      stride[i] = stride[i-1]*card[i-1];
    n = stride[size-1]*card[size-1];
  }else{
    n = 1;
  }
  phi = data;
}

/*
factor::factor(int _size, int *_id, long *_card, double val){

    size = _size;
    if (size > 0){
      id = (int*)malloc(sizeof(int)*size);
      card = (long*)malloc(sizeof(long)*size);
      stride = (long*)malloc(sizeof(long)*size);
      for (int i=0; i<size; i++){
        id[i] = _id[i];
        card[i] = _card[i];
      }
      stride[0] = 1;
      for(int i=1; i<size; i++)
        stride[i] = stride[i-1]*card[i-1];
      n = stride[size-1]*card[size-1];
    }else{
      n = 1;
    }
    phi = (double*)malloc(sizeof(double)*n);
    if (val >= 0){
      for (long i=0; i<n; i++)
        phi[i] = val;
    }else if(val == -1){
      for (long i=0; i<n; i++)
        phi[i] = (rand()%1000000)/1000000.0;
    }else if(val == -2){
      for (long i=0; i<n; i++)
        phi[i] = -log((rand()%1000000)/1000000.0);
    }else{
      cout << "Flag not recognized" << endl;
      exit(0);
    }
}
*/

factor::factor(int _size, vector<int> _id, vector<long> _card, double val){
    size = _size;
    if (size > 0){
      id = _id;
      card = _card;
      stride.assign(size,1);
      for(int i=1; i<size; i++)
        stride[i] = stride[i-1]*card[i-1];
      n = stride[size-1]*card[size-1];
    }else{
      n = 1;
    }
    phi.assign(n,val);
    if(val == -1){
      for (long i=0; i<n; i++)
        phi[i] = (rand()%1000000)/1000000.0;
    }else if(val == -2){
      for (long i=0; i<n; i++)
        phi[i] = -log((rand()%1000000)/1000000.0);
    }
}


factor::factor(const factor & f){
    size = f.size;
    n = f.n;
    if(size > 0){
      id = f.id;
      card = f.card;
      stride = f.stride;
    }
    phi = f.phi;
}


double factor::get(long *assignment){
  long idx=0;
  for (int i=0; i<size; i++){
    idx += assignment[i]*stride[i];
  }
  return phi[idx];
}

void factor::set_1factor(long idx, double val){
  assert(size == 1);
  phi[idx] = val;
}

void factor::set_2factor(long idx1, long idx2, double val){
  assert(size == 2);
  phi[idx1 + idx2*stride[1]] = val;
}

long factor::get_idx(vector<int> & assignment){
  long idx=0;
  for (int i=0; i<size; i++){
    idx += assignment[i]*stride[i];
  }
  return idx;
}

void factor::set(long *assignment, double val){
    long idx=0;
    for (int i=0; i<size; i++){
      idx += assignment[i]*stride[i];
    }
    phi[idx] = val;
}

double factor::subtract_min(){

    double minval = phi[0];
    for (long i=1; i<n; i++)
      minval = min(minval, phi[i]);
    for (long i=0; i<n; i++)
      phi[i] -= minval;
    return minval;
}

double factor::minimize(long *assignment) const{

    double minval = phi[0];
    long minidx = 0;
    for (long i=1; i<n; i++){
      if(phi[i] < minval){
        minval = phi[i];
        minidx = i;
      }
    }
    if (assignment != NULL)
      get_assignment(minidx, assignment);
    return minval;
}

void factor::get_assignment(long idx, long *assignment) const{
  for(int i=0; i<size; i++)
    assignment[i] = (long)floor((double)idx/stride[i])%card[i];
}

void factor::get_assignment(long idx, vector<long> & assignment) const{
  for(int i=0; i<size; i++)
    assignment[i] = (long)floor((double)idx/stride[i])%card[i];
}

void factor::reduce1()
{
  long idx = 0;
  for(long i=0; i<card[1]; i++){
    //double minval = phi[0+i*stride[1]];
    double minval = phi[idx++];
    for (long j=1; j<card[0]; j++){
      //if(phi[j+i*stride[1]] < minval)
      //        minval = phi[j+i*stride[1]];
      if(phi[idx++] < minval)
        minval = phi[idx-1];
    }
    phi[i] = minval;
  }
  size = 1;
  id[0] = id[1];
  card[0] = card[1];
  n = card[0];
}

void factor::reduce2()
{
  for(long i=0; i<card[0]; i++){
    double minval = phi[i+0*stride[1]];
    long idx = i+stride[1];
    for (long j=1; j<card[1]; j++){
      if(phi[idx] < minval)
        minval = phi[idx];
      idx += stride[1];
    }
    phi[i] = minval;
  }
  size = 1;
  n = card[0];
}

void factor::reduce(int variable_id){ /* minimize over one variable */

  //cout << "warning: reduce operator to be re-implemented" << endl;

  if (size == 1){
    assert(variable_id == id[0]);
    phi[0] = minimize(NULL);
    size = 0;
    return;
  }

  vector<long> _card(size-1,0);
  vector<int> _id(size-1,0);
  vector<long> _stride(size-1,0);
  vector<long> f_assign(size,0);
  vector<long> g_assign(size-1,0);
  int k,j=0;
  for(int i=0; i<size; i++){
    if(id[i] != variable_id){
      _id[j] = id[i];
      _card[j] = card[i];
      j++;
    }
  }
  if(j!=size-1){
    printf("variable to be reduced is not in the scope of the factor!\n");
    exit(0);
  }

  _stride[0] = 1;
  for(int i=1; i<size-1; i++)
    _stride[i] = _stride[i-1]*_card[i-1];
  long _n = _stride[size-2]*_card[size-2];
  vector<double> _phi(_n, 0);
  for(long i=0; i<_n; i++)
    _phi[i] = INF;

  double tmp;
  for (long i=0; i<n; i++){
    get_assignment(i, f_assign);
    k=0;
    for(int j=0; j<size; j++){
      if(id[j] != variable_id){
        g_assign[k] = f_assign[j];
        k++;
      }
    }
    tmp = phi[i];
    long idx = 0;
    for(int j=0; j<size-1; j++)
      idx += g_assign[j]*_stride[j];
    if(tmp < _phi[idx]){
      _phi[idx] = tmp;
    }
  }
  card = _card;
  id = _id;
  stride = _stride;
  size = size-1;
  n = _n;
  phi = _phi;
}

void factor::subtract(factor & f){

  cout << "warning: subtract operator to be re-implemented" << endl;
  exit(0);
    long* assignment = (long*)malloc(sizeof(long)*size);
    long* assignment_f = (long*)malloc(sizeof(long)*f.size);
    for(int i=0; i<n; i++){

      // convert i to assignment[]
      get_assignment(i, assignment);

      // bind assignment to f
      for(int j=0; j<f.size; j++){
        for(int k=0; k<size; k++){
          if(id[k] == f.id[j]){
            assignment_f[j] = assignment[k];
            break;
          }
        }
      }
      phi[i] -= f.get(assignment_f);
    }
    free(assignment);
    free(assignment_f);
}

void factor::add(factor & f){
  add(f, 1.0);
}

void factor::add_ij_to_ij(factor & f){
  assert(id[0] == f.id[0]);
  if(size>1)
    assert(id[1] == f.id[1]);
  for(long i=0; i<n; i++)
    phi[i] += f.phi[i];
}

void factor::add_i_to_ij(factor & f){
  long idx = 0;
  assert(f.id[0] == id[0]);
  for(long j=0; j<card[1]; j++)
    for(long i=0; i<card[0]; i++)
      phi[idx++] += f.phi[i];
  //phi[i+j*stride[1]] += f.phi[i];
}

void factor::add_j_to_ij(factor & f){
  long idx = 0;
  assert(f.id[0] == id[1]);
  for(long j=0; j<card[1]; j++)
    for(long i=0; i<card[0]; i++)
      phi[idx++] += f.phi[j];
  //phi[i+j*stride[1]] += f.phi[i];
}

void factor::swap_ij(){
  long tmp;
  vector<double> _phi(n,0);
  for(long i=0; i<card[0]; i++)
    for(long j=0; j<card[1]; j++)
      _phi[j+i*stride[1]] = phi[i+j*stride[1]];

  tmp = id[0];
  id[0] = id[1];
  id[1] = tmp;

  tmp = card[0];
  card[0] = card[1];
  card[1] = tmp;

  stride[0] = 1;
  stride[1] = card[0];

  phi = _phi;
}

void factor::add(factor & f, double coefficient){

  if(f.size == 0){
    for(long i=0; i<n; i++)
      phi[i] += f.phi[0] * coefficient;
    return;
  }

    int newsize = size;
    for(int i=0; i<f.size; i++){
      if(find(f.id[i], size, id)<0)
         newsize++;
    }
    if (newsize>size){
      card.resize(newsize, 0);
      stride.resize(newsize,0);
      id.resize(newsize,0);
      long idx = size-1;
      for(int i=0; i<f.size; i++){
        if(find(f.id[i], size, id)<0){
          id[++idx] = f.id[i];
          card[idx] = f.card[i];
        }
      }
      for(int i=size; i<newsize; i++){
        stride[i] = stride[i-1]*card[i-1];
      }
    }
    vector<long> oldstride(newsize,0);
    vector<long> fstride(newsize,0);
    for (int i=0; i<newsize; i++){
      if (i<size)
        oldstride[i] = stride[i];
      else
        oldstride[i] = 0;
      if (find(id[i], f.size, f.id)<0)
        fstride[i] = 0;
      else
        fstride[i] = f.stride[find(id[i], f.size, f.id)];
    }

    long newn = stride[newsize-1]*card[newsize-1];
    vector<double> newphi(newn,0);
    vector<long> assignment(newsize,0);

    long j=0, k=0;
    for(int l=0; l<newsize; l++)
      assignment[l] = 0;
    for(long i=0; i<newn; i++){
      newphi[i] = phi[j] + coefficient*f.phi[k];
      for(int l=0; l<newsize; l++){
        assignment[l]++;
        if(assignment[l] == card[l]){
          j -= (card[l]-1)*oldstride[l];
          k -= (card[l]-1)*fstride[l];
          assignment[l] = 0;
        }else{
          j += oldstride[l];
          k += fstride[l];
          break;
        }
      }
    }
    phi = newphi;
    n = newn;
    size = newsize;
}


void factor::print(){
    int idx;
    vector<long> assignment(size,0);
    printf("factor:\n");
    for(long i=0; i<n; i++){
      get_assignment(i, assignment);
      printf("(");
      for(int j=0; j<size; j++)
        printf(" %d:%ld ", id[j], assignment[j]);
      printf("): %f\n", phi[i]);
    }
    printf("\n");
}

int factor::find(int a, int n, vector<int> & v){
  for (int i=0; i<n; i++){
    if(a == v[i])
      return i;
  }
  return -1;
}

factor::~factor()
{
  size = 0;
}

factor & factor::operator=(const factor & f)
{
  size = f.size;
  id = f.id;
  stride = f.stride;
  card = f.card;
  phi = f.phi;
  n = f.n;
  return *this;
}
void factor::subtract_const(double c)
{
  for(long i=0; i<n; i++)
    phi[i] -= c;
}

