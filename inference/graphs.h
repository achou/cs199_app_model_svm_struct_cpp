// Circle  Pursuit
// by Huayan Wang <huayanw@cs.stanford.edu>

// graphs.h: implements data structure and basic operations for graphs

#ifndef GRAPHS_H
#define GRAPHS_

#include "block.h"
#include "../svm_struct_api_types.h"

#include "doublepool.h"
#include "factor.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <assert.h>
#include <vector>
#include <stack>
#include <queue>

#define DEBUG_MODE 0
#define WRITE_HEAP 0

using namespace std;

class cp_graph;
class cp_cycle;
class cp_problem;

double randu(double lowerbound, double upperbound);  // U[l,u]
double randn(double mu, double sigma);               // N(mu, sigma^2)

int find_in_vector(int a, const vector<int> & v);  // find the position of a in v
double vector_lp_norm(const vector<double> & v, double p); // L_p form of vector, p=[1, INF]
void multiply_s_v(double s, vector<double> & v); // multiply scalar to vector
void add_s_v(double s, vector<double> & v); // add scalar to vector
vector<double>  add_s_v_const(double s, const vector<double> & v); // add scalar to vector
vector<double> multiply_s_v_const(double s, const vector<double> & v); // multiply scalar to vector
vector<double> add_vector(const vector<double> & v1, const vector<double> & v2);
void add_vector(vector<double> & tar, const vector<double> & v, double s); // tar += s*v
void add_vector(vector<double> & tar, const vector<double> & v, double s, double t); // tar += s*v + t
void add_matrix(int K, vector<double> & tar, const vector<double> & M, double s); // tar += s*M (tar,s are KxK matrices)
void add_matrixT(int K, vector<double> & tar, const vector<double> & M, double s); // tar += s*M' (tar,s are KxK matrices)
vector<double> matrix_transpose(int K, const vector<double> & M);
void add_i2ij(const vector<double> & v, vector<double> & M, double factor); // add factor (i) to factor (i,j)
void add_j2ij(const vector<double> & v, vector<double> & M, double factor); // add factor (j) to factor (i,j)
void add_i2i(const vector<double> & v1, vector<double> & v2); // add factor (i) to factor (i)
void reduce_i(int K, vector<double> & M, vector<int> & argmin); // reduce i from factor (i,j), keep argmin
void reduce_j(int K, vector<double> & M, vector<int> & argmin); // reduce j from factor (i,j), keep argmin
void reduce_i(int K, vector<double> & M); // reduce i from factor (i,j)
void reduce_j(int K, vector<double> & M); // reduce j from factor (i,j)
vector<double> reduce_j_const(int K, const vector<double> & M); // reduce j from factor (i,j)
vector<double> reduce_i_const(int K, const vector<double> & M); // reduce i from factor (i,j)
void reduce_i_const(int K, const vector<double> & M, vector<double> & v);
bool IsPermutation(const vector<int> & l, int N);
void minus_min(vector<double> & X);
void write_messages(vector< vector<double> > & msg, vector<int> & assignment, int forward, const char * filename);
vector<double> project_i(int K, vector<double> & M, int l); // factor(i,j) --> factor(i=l, j)
vector<double> project_j(int K, vector<double> & M, int l); // factor(i,j) --> factor(i, j=l)
vector<int> random_iterator(int n); // randomly shuffle [0, 1, 2, ..., n-1], for going thru a list in random order
void my_quick_sort(vector<double> & w, vector<int> & a, int s, int e);
inline int reverse_pairwise_label(int l, int K){  return (l%K)*K + (l-(l%K))/K; }
int matrix_is_a_plus_b(int K, const vector<double> & M);
vector<double> matrix_row(int K, const vector<double> & M, int i);
vector<double> matrix_column(int K, const vector<double> & M, int i);
vector<double> vector_diff(const vector<double> & v1, const vector<double> & v2);
int is_const_vector(const vector<double> & v);
void print_float(double f);
int vector_cnt_pos(const vector<int> & v);
void get_squares_for_grid(vector<cp_cycle> & CY, int n);
void get_rectangles_for_grid(vector<cp_cycle> & CY, int n);
void get_triangles_for_fully_connected(vector<cp_cycle> & CY, int N);
void get_quads_for_fully_connected(vector<cp_cycle> & CY, int N);
void compare_minmarginals(cp_cycle & c0, cp_cycle & c1, cp_cycle & c2);
int same_cycle(const cp_cycle & c1, const cp_cycle & c2);
cp_cycle reverse_cycle(const cp_cycle & c);

vector<int> rotate_cycle_assignment(const vector<int> & a, int offset);

vector<double> replicate_rows(const vector<double> & a, int K, double s);
vector<double> replicate_columns(const vector<double> & a, int K, double s);

double solve_single_edge(cp_graph & G);
double solve_cycle(cp_graph & G);

void damp_pbca_update(vector< vector <double> > & current, vector< vector <double> > & last, double lambda);

class cp_graph {
 public:

  int status; // 0="original problem". 1="covering tree"

  int graph_type;

  int N; // number of variables (each could have multiple "copies" if status=1)
  int L; // number of nodes (each is a copy of some variable, L=N if status=0)
  int M; // number of edges
  int K; // variable cardinality

  vector<int> nc; // number of copies for each node
  vector< vector<int> > nb; // nb[i*N+j] is the list of neighbors of the i-th copy of variable j
  vector< vector<int> > pid; // abs(pid[x][j])-1 is the index into p, for the potential of the edge (x,nb[x][j]),
                             // if pid[x][j]<0, the matrix needs to be transposed.
                             // specifically, let a be the assignment to x, b be the assignment to nb[x][j]
                             // if pid[x][j]>0, the pairwise cost is p[abs(pid[x][j])-1][a+b*K];
                             // elseif pid[x][j]<0, the pairwise cost is p[abs(pid[x][j])-1][b+a*K];

  vector<int> upstream; // only applicable when status=1, upstream node leading to root

  // potentials
  vector< vector<double> > s; // singleton potentials (K x 1 tables)
  vector< vector<double> > p; // pairwise potentials (K x K tables),



  // the three arrays (nb, s, and pid) share the same length and index (one entry for each node)

  vector<double> edge_weight; // used for sampling cycles, share the same index with 'p'

  vector< vector<int> > edge_nodes; // pointing to the two nodes connected by each edge
  void update_edge_nodes(); // make sure the 'edge_nodes' points to the correct node indexes

  vector<int> assignment;



  vector< vector<double> > node_mmarginal;
  vector< vector<double> > edge_mmarginal;

  double obj;
  double get_obj();

  cp_graph();

  // graph_type = 0: n x n grid
  // graph_type = 1: bicycle, each cycle has 4n-4 nodes, and they have n nodes in common (two squares sharing one edge)
  // graph_type = 2: fully connected graph with n nodes
  // graph_type = ...
  cp_graph(int graph_type, int n);

  cp_graph(const cp_graph & g); // make a copy

  ~cp_graph();

  int find_edge(int id1, int id2); // finds an element in 'pid' given two variables, avoids storing the NxN adjacency matrix

  void random_weights(int cardinality, int flag); // create random problem, flag=0: zero singletons
  void read_weights(const char* filename, int cardinality);

  void set_weights(int cardinality, struct block *b,
      vector< vector<double> > &factor,
      vector< vector<double> > &condensedPairwise,
      double blockpairs[NPARTS][NPARTS][NHYPS][NHYPS],
      double symAndPairEnergies[E][NHYPS][NHYPS],
      vector< vector<int> > &edgeIndices);
  double getBestHypotheses(int *h1, int *h2);
  double getEnergyForHypotheses(int *hids, struct block *b);

  // NOTE: update_edge_nodes() needs to be called at the end of any func that creates MRF parameters (by reading from input or randomly)
  // just like in the end of random_weights(int,int)

  void convert_into_covering_tree(int root_covering_tree, int root_msg_passing);
  // set both roots to be 0 (first node) for simplicity (all choices are equivalent)
  // different choices are for debugging purpose only

  void show_connectivity();
  void show_msg_pass_order();
  void show_assignment();
  double min_sum_bp_covering_tree();

  double min_sum_bp_covering_tree_get_minmarginals();

  // find smallest cycle through given node
  void find_smallest_cycle(int node, vector<int> & cycle);

  // sample a cycle through given node, smaller cycles have higher changes
  void sample_small_cycle(int node, vector<int> & cycle, int cycle_distribution);


  //double evaluate_node_perturbation_locally(int i, int l);
  //double evaluate_node_perturbation_globally(int i, int l);
  //double evaluate_edge_perturbation_locally(int i, int l);
  //double evaluate_edge_perturbation_globally(int i, int l);

private:
  void create_grid(int n);
  void create_bicycle(int n);
  void create_fully_connected_graph(int n);
  vector<int> shortest_path_ignoring_direct_edge(int a, int b);
  double shortest_weighted_path_ignoring_direct_edge(int a, int b, vector<int> & c);
  void random_edge_weights(int flag);
  void unit_edge_weights();

  // these needs to be access in *reverse* order for upstream msg passing
  vector<int> msg_from; // idx into singleton potentials
  vector<int> msg_to;   // idx into singleton potentials
  vector<int> msg_via;  // abs(msg_via[])-1 is idx into pairwise potentials, negative means transpose
                        // specifically, let a be the assignment to msg_from[i], b be the assignment to msg_to[i]
                        // if msg_via[i]>0, the pairwise cost is p[abs(msg_via[i])-1][a+b*K]
                        // elseif msg_via[i]<0, the pairwise cost is p[abs(msg_via[i])-1][b+a*K]

  void msg_passing_ordering_for_covering_tree(int root);
};

class cp_cycle {

public:
  cp_cycle();

  cp_cycle(const cp_cycle & c);

  cp_cycle(const cp_graph & g);

  // create a cycle with random weights, for debugging only
  cp_cycle(int size, int cardinality, int flag);

  // specify a cycle (in the original graph of the MAP inference problem)
  cp_cycle(vector<int> _nodes);

  ~cp_cycle();

  int K;
  int N;
  vector<int> nodes; // ids of nodes (in the original graph), connectivity: 0-1-2-...-N-0
  vector<int> edges; // ids of edges (in the original graph), for 0-1, 1-2, ....
                     // this avoids storing NxN adjacency matrix of the original graph
  int allzero;
  vector< vector<double> > p; // pairwise potentials (K x K tables)  // size: N tables
  vector<double> min_factor;
  void update_min_factor();

  double constant;

  void write_heap_state(int d, const char * filename);

  int maintain_heaps;
  //////////////////////////////////////////////////////////////////////
  /*
  vector<doublepool> cy_P;
  vector<factor> cy_argM;
  double cy_ub;
  vector<double> cy_rlb;
  vector<double> cy_min_incoming_msg;
  factor cy_last_factor;
  long cy_final_assignment;
  int cy_rlb_needs_update;
  double cy_ub_needs_update;
  int ready;
  vector<double> node_mmarginal; // node margial of node '1'
  vector<double> edge_mmarginal; // edge margial of edge '61'
  vector<int> assignment;
  int reset_msg_from; // 0, 1, 2, ... L-1
  ///////////////////////////////////////////////////////////////
  */

  vector< vector<doublepool> > cy_P;                                 // size N x (N-2) x 5 tables
  vector< vector< vector<int> > > cy_argM;                           // size N x (N-2)

  vector<double> cy_ub;
  vector<vector<double> > cy_rlb;
  vector< vector<double> > cy_min_incoming_msg;
  vector<int> cy_last_factor_id; // always transpose
  vector<int> cy_final_assignment;
  vector<int> cy_rlb_needs_update;
  vector<double> cy_ub_needs_update;
  vector<int> cy_reset_msg_from; // 0, 1, 2, ... L-1

  vector<int> ready;
  vector<vector<double> > node_mmarginal; // node margial of node '1',
  vector<vector<double> > edge_mmarginal; // edge margial of edge '61',   // N tables

  //fvector< vector<int> > factor_updated;

  /////////////////////////////////////////////////////////////////////


  int idx_next_mmarginal_needed;
  vector<int> next_mmarginal_needed; // [0,...N-1]:nodes,  [N,...2*N-1]:edges
  vector<int> assignment;
  double obj;

  void add_singleton(int i, int l, double a);
  //void reset_msg(int d, int from);

  void init_heaps();
  int init_heaps_called;

  void check_heaps(int d);
  void check_critical_path(int d);

  double get_obj();
  doublepool construct_doublepool(int d, int i, const vector<double> & minf);
  vector<double> lower_bounds(int d);
  // vector<double> lower_bounds(int d, const vector<double> & minf);

  double run_map_inference(int dual_method, int pbca_minmarginal, int warmstart_cycle, int d, double & time);

  // This is for debugging puspose only,
  // it only returns the MAP energy, WITHOUT getting the assignment!!!
  double naive_min_sum_bp();

  double fast_min_sum_bp_with_minmarginals(int d, double & time);
  double incomplete_bp(int d, double & time);
  double incomplete_bp_warmstart(int d, double & time);

  void add_vec_pairwise_update_heaps(int i, const vector<double> & a);
  void add_vec_singleton_update_heaps(int i, const vector<double> & a);

  void reparameterize_singleton(int i);

  //void add_pairwise_update_heaps(int idx, int l, double a);
  //void add_singleton_update_heaps(int i, int l, double a);

  void show_assignment();
  void print();

private:
  factor cycle_factor(int a, int b);

};


class cp_problem {

public:

  int N;
  int M;
  int K;
  int CYsize;

  double constant;

  cp_graph G;
  cp_graph CT;              // covering tree
  vector<cp_cycle> CY;      // cycles

  vector< vector<double> > unary_subproblem; // only used in pbca method
  vector<int> unary_subproblem_allzero;      // indicator that the unary_subproblem is not active
  vector<int> unary_subproblem_assignment;   // minimizer
  vector<double> unary_subproblem_min;   // min values

  vector< vector< vector<double> > >  last_update; // last_update[ block index ][ index of var/edge copy ][1:K or 1:K^2]

  int graph_type;

  // for quickly access all copies of each variable and edge in CY (copies in CT are easy to access)
  vector< vector<int> > var_cp_cy;    // var_cp_cy[i][j] is the index into CY to find the j-th copy of var i
  vector< vector<int> > var_cp_id;    // var_cp_id[i][j] is the index into CY[].nodes/s to find the j-th copy of var i
  vector< vector<int> > edge_cp_cy;   // edge_cp_cy[i][j] is the index into CY to find the j-th copy of edge i
  vector< vector<int> > edge_cp_id;   // edge_cp_id[i][j] is to find the j-th copy of edge i in CY[].edges/p
                                      // edge_cp_id[i][j] can be positive or negative, using the same encoding rule as 'pid'

  vector<int> current_assignment; // primal solution
  int next_assignment(int l);

  double dual_obj;    // updated in solve_subproblems();

  //vector<int> assignment; // primal solution

  double current_primal_obj;  // updated in apply_subgradient();

  // best solution so far;
  vector<int> assignment;
  double primal_obj;

  long iter; // to record the current iteration number

  int cycles_solved;
  double running_time; // to record CPU time spent on this problem so far
  double running_time_per_iteration; // to record CPU time spent on this problem so far

  double running_time_addcycle;
  double running_time_solve_cp;
  double running_time_solve_cy;
  double running_time_solve_cy_kernel;
  double running_time_update_dual;

  //int pbca_updated;

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
  // experiment setting, controls what to do when run(int) is invoked
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////

  int cycle_method;
  // 0: no cycles
  // 1: add cycles as we go
  // 2: running with fixed set of cycles (smaller set)
  // 3: running with fixed set of cycles (larger set)

  // only used when cycle_method == 1
  int add_cycle_method; // see add_cycle(int,int) for explanations
  int add_cycle_init;   // see add_cycle(int,int) for explanations
  int max_num_cycle; // maximum number of cycles
  int cycle_distribution; // 0: highly concentrated on small cycles. 1: less skewed
  int add_cycle_interval; // if set to '1', a cycle is added in every iteration
  int add_cycle_batch;    // number of cycles added each time

  int dual_method; // 0: subgradient 1: pbca (pseudo block coordinate ascent)

  // only used when dual_method == 0
  double subgradient_stepsize;  // stepsize/sqrt(iter);

  // only used when dual_method == 1
  int pbca_normalize;   // 0: unnormalized mmarginals, 1: normalized mmarginals
  int pbca_update;      // 0: one var/edge at a time. 1: all
  int pbca_minmarginal; // 0: exact, 1: sparse approx, 2: approx

  int warmstart_cycle;

  double damping;

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////

  cp_problem();
  cp_problem(const cp_graph & g);

  void initialize();

  void set_needed_mmarginals_in_cycles();

  //mode==0: run for a given number of seconds
  //mode==1: run for a given number of iterations
  void run(int mode, double duration);

  // this updates 'dual_obj'
  int solve_subproblems();

  // alpha is step size,
  // it calls primal_sol_by_voting() if get_primal_sol=1 OR converged.
  // return 1 if converged
  int apply_subgradient(double alpha, int get_primal_sol);


  //int compute_sparse_approx_mmarginals(int block, int node_id);

  // pseudo block coordinate ascent method
  int pbca(int block, int node_id);

  //int get_next_block_for_pbca();
  //void init_block_iterator_for_pbca();
  vector<int> block_iterator;
  int block_iterator_idx;
  int sample_node_id(int block);

  double get_primal_obj();

  void exhaustive_search(); // for debugging

  int add_cycle(int method, int init);

  // method=
  // 0: given one detached var (that disagrees on CT), find the path via covering tree
  // 1: given one detached var (that disagrees on CT), find the shortest path via original graph (typically a square or triangle)
  // init =
  // 0: initialize energies to be zero, and copy assignment from CT (this does not change the dual obj)
  // 1: initialize energies by spliting 1/(n+1) from each existing n copies of the nodes(edges) (does this improve the dual obj?)

  int check_dual_variables(); // for debug:  make sure that the parameters of slave problems add up to the original parameters

  void remove_repeated_active_labels(vector<int> & active, int maxlabel);

  void  print_assignment_and_energy();

  void print_statistics();

private:
  // this updates 'primal_obj' and 'assignment', called in apply_subgradient()
  void primal_sol_by_voting(const vector< vector<int> > & x, const vector< vector<int> > & active);
  void primal_sol_by_voting();
  int get_cycle_thru_one_var_large(vector<int> & disagree);
  int get_cycle_thru_one_var_small(vector<int> & disagree);
  void init_cycle_zero();
  void init_cycle_split(int all);
  void update_cycle_indexes(int update_all);
  int cycle_exists(const cp_cycle & c); // check if cycle exists in CY[]
};

#endif
