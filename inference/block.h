#ifndef BLOCK_H
#define BLOCK_H
#include <vector>
#include "../svm_struct_api_types.h"
using namespace std;

struct block{
  // index in p of each part or -1 if not in the block.
  int i[NPARTS];
  vector<int> p; // a list of the parts in the block.
  bool b[NPARTS];// a bitmap of the parts in the block.
};
#endif
