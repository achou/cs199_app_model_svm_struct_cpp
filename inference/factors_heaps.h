// Circle Pursuit
// by Huayan Wang <huayanw@cs.stanford.edu>

// factors_heaps.h: implements data structures and basic operations for factors and heaps

#ifndef FACTOR_HEAPS_H
#define FACTOR_HEAPS_H

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <assert.h>
#include <vector>
#include <stack>
#include <queue>

#include "fib.h"
#include "factor.h"

#define	MAX(A, B)	((A) > (B) ? (A) : (B))
#define	MIN(A, B)	((A) < (B) ? (A) : (B))

#define MOVEON_THRESHOLD 0.00001

using namespace std;


inline bool IsNotInf(double a)
{
  return fabs(a)<1e6;  // TODO this could be problematic if actual energy values are in this scale
}

double minimize(const vector<double> & v, int & i); // minimization, with argmin
double minimize(const vector<double> & v); // minimization
double maximize(const vector<double> & v, int & i); // maximization
double maximize(const vector<double> & v); // maximization
double vector_sum(const vector<double> & v);
int vector_sum(const vector<int> & v);


class id_set {

public:
  vector<int> data;
  id_set(vector<int> a);
  void intersect(vector<int> a);
};

class factorset;
class factorset{

public:

  vector<int> id;    // identify for the variables (union of all factors)
  vector<factor> factors;

  factorset();

  factorset(factor f);
  factorset(const factorset & f);

  void add_factor(factor f);
  void add_factor(factor f, int pos);
  factor sum_up_all(); /* create a huge factor by adding up all factors */
  void combine_factors(int size_limit); /* combine factors s.t. the size of resulted factors does not exceed limit */
  double simple_lower_bound(); /* find lower bound by adding up factor-mins */
  factorset & operator=(const factorset & rhs);

  void print();

};

class multipool{ /* according to description in NIPS'12 paper */

public:
  int n;
  factorset fset;

  vector< vector< void* > > handle;
  vector< struct fibheap* > data;

  vector< vector<int> > inpool;
  vector< vector<int> > onshelf;

  vector<double> min_value;
  vector<double> min_key_val;
  vector<double> cover_value; // the sum of min_value from all other heaps

  //multipool(): n(0) {}
  multipool();
  multipool(factorset & _fset);
  multipool(int _n, int K);
  ~multipool();
  void free_mem();
  void update_cover_values();
  void empty_heap(int fid);
  void add(int fid, long eid, double val);
  void decrease_key(int factor_id, long entry_id, double val);
  double extract_min(int & factor_id, long & entry_id);
  double min_key();
};

//void write_heap_state(vector<doublepool> & P, vector<double> & min_incoming_msg, vector<double> & rlb, double ub,  vector<int> & assignment, const char * filename);


class pool{ /* a fibonacci heap containing entries from a factor set */

public:
  int n;
  factorset fset;
  struct fibheap *data;
  vector< vector< void* > > handle;

  vector< vector<int> > inpool;
  vector< vector<int> > onshelf;

  pool();
  pool(factorset & _fset);
  pool(factorset & _fset, vector<int> put_in_pool);
  ~pool();
  void add(int fid, long eid, double val);
  void decrease_key(int factor_id, long entry_id, double val);
  double extract_min(int & factor_id, long & entry_id);
  double peek_min(int & factor_id, long & entry_id);
  double min_key();

};


class spool{ /* obsolete */

public:
  long cnt;
  factor f;
  struct fibheap *data;
  vector< void* > handle;
  vector<int> inpool;
  vector<int> onshelf;

  spool();
  spool(factor & _f, int put_in_pool);
  ~spool();
  void add(long eid, double val);
  void decrease_key(long entry_id, double val);
  double extract_min(long & entry_id);
  double peek_min(long & entry_id);
  double min_key();
};

class shelf{ /* a set of entries from a factorset */

public:

  int height; /* number of rows */

  vector< vector<double> > data_val;
  vector< vector<long> > data_id;
  vector< vector< vector<long> > > data_assignment;

  vector< vector<int> > id; /* variables in the factor of each row */
  vector<int> id_union;
  vector<long> full_assignment;
  vector<int> output_assignment;
  vector<int> iterator_pos; /* a marker on each array indicating the state of iterator */
  int iterator_state; /* not-initialized(-1), valid(0), terminated(1) */

  vector<int> current_entry_id;
  vector<long> current_entry_assignment;
  double current_val;

  factorset fset;
  int output_factor;
  vector< vector<long> > shelf_pos;

  // initialize by a factorset
  shelf(factorset & _fset);
  shelf(int num_f, int K);
  void push(int factor_id, long entry_id, double val, const vector<long> & xi);
  void pop(int factor_id, long entry_id);
  void init_iterator(int factor_id, long entry_id, double val, int output_factor_id);
  double find_next_sum(long & output_entry_id); /* return -1 when the iteration has terminated */
  void print();

};


int match(vector<int> a, vector<int> b);
int commit_assignment(vector<long> & full_assignment, const vector<int> & id_union,
		      const vector<long> & assign, const vector<int> & id);
template<class T> void print_vector(vector<T> & v);
template<class T> void print_vector(const char* name, vector<T> & v);

vector<double> pff_min_sum(int K, const vector<double> & Mij, const vector<double> & Mjk, vector<int> & argmin_ik);
void bounded_pff_min_sum(int K, const vector<double> & Mij, const vector<double> & Mjk, vector<double> & Mik, double min_energy, double & time);

template<class T> void print_vector(vector<T> & v)
{
  for(int i=0; i<v.size(); i++)
    cout << v[i] << ",";
  cout << endl;
}

template<class T> void print_vector(const char* name, vector<T> & v)
{
  printf("%s", name);
  printf(": ");
  for(int i=0; i<v.size(); i++)
    cout << v[i] << ",";
  cout << endl;
}
#endif
