#include "doublepool.h"
#include "fib.h"
#include <math.h>
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <algorithm>

doublepool::doublepool(const doublepool & dp)
{
  //cout << endl << "doublepool(const doublepool & dp)" << endl;

  assert(dp.data_msg == NULL);
  assert(dp.data_init == NULL);

  data_msg = NULL;
  data_init = NULL;

  //assert(0==1);
  //n = dp.n;
  //fset = dp.fset;
  //handle = dp.handle;
  //data_msg = fh_copykeyheap(dp.data_msg);
  //data_init = fh_copykeyheap(dp.data_init);
  //inpool = dp.inpool;
  //onshelf = dp.onshelf;
  //min_key_msg = dp.min_key_msg;
  //min_key_init = dp.min_key_init;
}
void doublepool::free_mem()
{
  if(data_msg!=NULL)
    fh_deleteheap(data_msg);
  if(data_init!=NULL)
    fh_deleteheap(data_init);
}
doublepool::~doublepool()
{

  //cout << "destructor" << endl;
  /*  if(data_msg!=NULL)
    fh_deleteheap(data_msg);
  if(data_init!=NULL)
    fh_deleteheap(data_init);
  */
}

void doublepool::reset_incoming_msg()
{
  msg.assign(KK, INF);
  inpool[0].assign(KK, 0);
  handle[0].assign(KK,(fibheap_el*)NULL);
  fh_deleteheap(data_msg);
  data_msg = fh_makekeyheap();
  min_key_msg = fh_minkey(data_msg);
}

/*
doublepool::doublepool(factorset & _fset, int first_node)
{

  n =  _fset.factors.size();
  fset = _fset;
  data_msg = fh_makekeyheap();
  data_init = fh_makekeyheap();
  inpool.assign(n, vector<int>());
  onshelf.assign(n, vector<int>());
  handle.assign(n, vector<void*>());

  if (first_node){
    fh_insertkey(data_msg, INF, (void*)(0));
    for(int i=0; i<n; i++){
      inpool[i].assign(fset.factors[i].n, 1);
      onshelf[i].assign(fset.factors[i].n, 0);
      for(int j=0; j<fset.factors[i].n; j++){
        handle[i].push_back(fh_insertkey(data_init, fset.factors[i].phi[j], (void*)(j*n+i)));
      }
    }
  }else{
    inpool[0].assign(fset.factors[0].n, 0);
    onshelf[0].assign(fset.factors[0].n, 0);
    handle[0].assign(fset.factors[0].n,(fibheap_el*)NULL);
    for(int i=1; i<n; i++){
      inpool[i].assign(fset.factors[i].n, 1);
      onshelf[i].assign(fset.factors[i].n, 0);
      for(int j=0; j<fset.factors[i].n; j++){
        handle[i].push_back(fh_insertkey(data_init, fset.factors[i].phi[j], (void*)(j*n+i)));
      }
    }
  }
  min_key_msg = fh_minkey(data_msg);
  min_key_init = fh_minkey(data_init);
}
*/

void doublepool::add(int fid, long eid, double val)
{
  //assert(fid == 0);
  handle[fid][eid] = fh_insertkey(data_msg, val, (void*)(eid*2+fid));
  if(f[0] < 0)
    msg[eid] = val;
  inpool[fid][eid] = 1;
  min_key_msg = fh_minkey(data_msg);
}

void doublepool::add_init_factor(int fid, long eid, double val)
{
  handle[fid][eid] = fh_insertkey(data_init, val, (void*)(eid*2+fid));
  inpool[fid][eid] = 1;
  min_key_init = fh_minkey(data_init);
}

double doublepool::min_key(double min_incoming_msg, double minf)
{
  return min(min_key_msg + minf, min_key_init + min_incoming_msg);
}

void doublepool::extract_min(int & factor_id, long & entry_id, double min_incoming_msg, double minf)
{
  if(min_key_msg + minf < min_key_init+min_incoming_msg){ // extract from msg heap
    long idx = (long)fh_extractmin(data_msg);
    factor_id = 0;
    entry_id = idx/2;
    inpool[0][entry_id] = 0;
    //double val = min_key_msg;
    min_key_msg = fh_minkey(data_msg);
    //return val;
  }else{ // extract from init factor heap
    long idx = (long)fh_extractmin(data_init);
    factor_id = idx%2;
    entry_id = (idx-factor_id)/2;
    inpool[factor_id][entry_id] = 0;
    //double val = min_key_init;
    min_key_init = fh_minkey(data_init);
    //return val;
  }
}

void doublepool::decrease_key(int factor_id, long entry_id, double val)
{
  if(f[0]<0)
    msg[entry_id] = val;
  fh_replacekey(data_msg, (fibheap_el*)handle[factor_id][entry_id], val);
  min_key_msg = fh_minkey(data_msg);
}

void doublepool::decrease_key_init_factor(int factor_id, long entry_id, double val)
{
  fh_replacekey(data_init, (fibheap_el*)handle[factor_id][entry_id], val);
  min_key_init = fh_minkey(data_init);
}

