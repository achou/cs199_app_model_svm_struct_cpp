// Circle  Pursuit
// by Huayan Wang <huayanw@cs.stanford.edu>

// graphs.cpp: implements data structure and basic operations for graphs


#include "factors_heaps.h"
#include "graphs.h"

using namespace std;

void cp_problem::run(int mode, double duration) // run subgradient for a number of iterations
{

  if(fabs(dual_obj - primal_obj) < EPS) // solved
    return;

  if(1){
    if(N == 2){
      primal_obj = constant + solve_single_edge(G);
      assignment = G.assignment;
      dual_obj = primal_obj;
      return;
    }else if((N == 3 && graph_type==2) || (N==4 && graph_type==0)){
      primal_obj = constant + solve_cycle(G);
      assignment = G.assignment;
      dual_obj = primal_obj;
      return;
    }
  }

  srand((long)(dual_obj*10000.0)); // such that different experiment settings does not affect each other

  int block, node_id;
  int target_iter = iter + (int)duration;
  double target_time = running_time + duration;

  while((mode==0 && iter < target_iter) || (mode==1 && running_time < target_time)){

    clock_t tic, toc, t;
    tic = clock();

    t = clock();
    if(cycle_method == 1){ // add cycles as we go
      cout << "adding cycle not supported, need to re-implement to accommodate replicated cycles" << endl; exit(1);
      if (iter > 0 && iter%add_cycle_interval == 0){
        assert(add_cycle_batch > 0);
        for(int j=0; j<add_cycle_batch; j++)
          add_cycle(add_cycle_method, add_cycle_init);
      }
    }
    running_time_addcycle += ((double)clock()-t)/CLOCKS_PER_SEC;





    // udpate dual
    cycles_solved = solve_subproblems();
    int updated = 0;

    t = clock();

    // update primal
    if(dual_method==0){ // subgradient
        apply_subgradient(subgradient_stepsize/sqrt(double(iter+1)), 1);
        updated = 1;
    }else if(dual_method==1){ // pseudo block coordinate ascent

      // get primal solution
      primal_sol_by_voting();

      block = iter%(N+M); //get_next_block_for_pbca();

      node_id = sample_node_id(block);

      if(pbca_update==0){ /* update one block */

        updated = pbca(block, node_id);

      }else if(pbca_update==1){  /* update all blocks, not principled, will ocilliate */

        cout << "this is obsolete" << endl; exit(0);

        for(block = 0; block<N+M; block++){
          node_id = sample_node_id(block);
          pbca(block, node_id);
        }

      }else{
        cout << "unspecified pbca_update" << endl; exit(0);
      }

    }else{
      cout << "unspecified dual_method" << endl; exit(0);
    }

    running_time_update_dual += ((double)clock()-t)/CLOCKS_PER_SEC;

    toc = clock();

    running_time_per_iteration =  ((double)toc-tic)/CLOCKS_PER_SEC;
    running_time +=   running_time_per_iteration;
    iter++;

#if DEBUG_MODE
    check_dual_variables(); //  correctness check, can be removed for speed
#endif

  }
}

int cp_problem::sample_node_id(int block)
{

  if (block < N){
    int t = rand()%CT.nc[block];
    return block + t*N;
  }else{
    return -1;
  }
}

vector<double> cp_cycle::lower_bounds(int d){

  assert(d>=0 && d<N);
  int L = N-2;
  vector<double> rlb(L,INF);
  vector<double> v = reduce_j_const(K,p[((N-1)+d)%N]);
  rlb[L-1] = minimize(v);
  for(int i=N-2; i>=2; i--){
    vector<double> M = p[(i+d)%N];
    add_j2ij(v, M, 1.0);
    v = reduce_j_const(K,M);
    rlb[i-2] = minimize(v);
  }
  return rlb;
}

/*
vector<double> cp_cycle::lower_bounds(int d, const vector<double> & minf){

  assert(d>=0 && d<N);
  int L = N-2;
  vector<double> rlb(L,INF);
  rlb[L-1] = minimize(p[(N-1+d)%N]);
  vector<double> v = reduce_j_const(K,p[((N-1)+d)%N]);
  add_s_v(-minf[((N-1)+d)%N], v);
  for(int i=N-2; i>=2; i--){
    vector<double> M = p[(i+d)%N];
    add_j2ij(v, M, 1.0);
    v = reduce_j_const(K,M);
    add_s_v(-minf[(i+d)%N], v);
    rlb[i-2] = minimize(v);
  }
  return rlb;
}
*/

/*
double cp_cycle::evaluate_node_perturbation_globally(int i, int l)
{
  // OPT write this more efficiently

  vector<double> tmp(K,s[i][l]);
  add_vector(tmp, project_i(K,p[i],l), 1.0);
  add_vector(tmp, s[(i+1)%N], 1.0);
  vector<double> tmp2 = p[(i+1)%N];
  add_i2ij(tmp, tmp2, 1.0);
  reduce_i(K, tmp2);
  vector<double> tmp3;
  int idx = (i+2)%N;
  while(idx != (i-1+N)%N){
    tmp3 = p[idx];
    add_i2ij(tmp2, tmp3, 1.0);
    add_i2ij(s[idx], tmp3, 1.0);
    reduce_i(K, tmp3);
    tmp2 = tmp3;
    idx = (idx+1)%N;
  }
  vector<double> tmp4 = p[(i-1+N)%N];
  add_i2ij(tmp2, tmp4, 1.0);
  add_i2ij(s[(i-1+N)%N], tmp4, 1.0);
  double E = minimize(project_j(K,tmp4,l));

  //////////////////////////////////  comment out
  // replace s[i] with deterministic potential

  vector<double> d(K, INF);
  vector<double> t = s[i];
  vector<int> a = assignment;
  double old_obj = obj;
  int old_allzero = allzero;
  d[l] = s[i][l];
  s[i] = d;
  allzero = 0;
  double new_obj = fast_min_sum_bp();
  assert(assignment[i] == l);
  // undo replace
  s[i] = t;
  obj = old_obj;
  assignment = a;
  allzero = old_allzero;
  assert(fabs(new_obj - E)<EPS);

  assert(fabs(E-node_mmarginal[i][l]) < EPS);


  /////////////////////////////////////////

  //return E-obj;
  return E;
}

double cp_cycle::evaluate_edge_perturbation_globally(int i, int l)
{
  int a0 = l%K;
  int a1 = (l-a0)/K;
  vector<double> tmp2(K,s[i][a0]+s[(i+1)%N][a1]+p[i][l]);
  add_vector(tmp2, project_i(K, p[(i+1)%N], a1), 1.0);  // now tmp2 is (i+2)
  int idx = (i+2)%N;
  vector<double> tmp3;
  while(idx != (i-1+N)%N){
    tmp3 = p[idx];
    add_i2ij(tmp2, tmp3, 1.0);
    add_i2ij(s[idx], tmp3, 1.0);
    reduce_i(K, tmp3);
    tmp2 = tmp3;
    idx = (idx+1)%N;
  }
  vector<double> tmp4 = project_j(K, p[(i-1+N)%N], a0);  // tmp4 is (i-1)
  add_vector(tmp2, s[(i-1+N)%N], 1.0);
  add_vector(tmp2, tmp4, 1.0);
  double E = minimize(tmp2);

  ////////////////////////////////// comment out
  // replace s[i] with deterministic potential

  vector<double> d(K*K, INF);
  vector<double> t = p[i];
  vector<int> a = assignment;
  double old_obj = obj;
  int old_allzero = allzero;
  d[l] = p[i][l];
  p[i] = d;
  allzero = 0;
  double new_obj = fast_min_sum_bp();
  assert(assignment[i] == a0 && assignment[i+1] == a1);
  // undo replace
  p[i] = t;
  obj = old_obj;
  assignment = a;
  allzero = old_allzero;
  assert(fabs(new_obj - E)<EPS);

  assert(fabs(E-edge_mmarginal[i][l]) < EPS);

  /////////////////////////////////////////

  //return E-obj;
  return E;
}
*/


// make sure CY[].edges points to the id of edges in G
// make sure var_cp_cy, var_cp_id, edge_cp_cy, edge_cy_id are update-to-date
// update_all = 0: only update the newly added cycle
void cp_problem::update_cycle_indexes(int update_all)
{
  if(update_all){
    assert(var_cp_cy.size() == G.N);
    assert(var_cp_id.size() == G.N);
    for(int i=0; i<G.N; i++){
      var_cp_cy[i].clear();
      var_cp_id[i].clear();
    }
    assert(edge_cp_cy.size()== G.M);
    assert(edge_cp_id.size()== G.M);
    for(int i=0; i<G.M; i++){
      edge_cp_cy[i].clear();
      edge_cp_id[i].clear();
    }
  }

  for(int c=0; c<CY.size(); c++){
    if(update_all || c== CY.size()-1){
      CY[c].edges.clear();
      int i;
      for(i=0; i<CY[c].nodes.size()-1; i++){
        assert(CY[c].nodes[i]>=0 && CY[c].nodes[i] < N);
        int id1 = CY[c].nodes[i];
        int id2 = CY[c].nodes[i+1];
        int edge = G.find_edge(id1, id2);
        CY[c].edges.push_back(G.find_edge(id1, id2));

        var_cp_cy[id1].push_back(c);
        var_cp_id[id1].push_back(i);
        edge_cp_cy[abs(edge)-1].push_back(c);
        if(edge>0)
          edge_cp_id[abs(edge)-1].push_back(i+1);
        else
          edge_cp_id[abs(edge)-1].push_back(-(i+1));

      }
      //print_vector("nodes", CY[c].nodes);
      //print_vector("edges", CY[c].edges);
      assert(CY[c].nodes[i]>=0 && CY[c].nodes[i] < N);
    }
  }
}

// return 0 if cannot find a cycle that has not been added
int cp_problem::get_cycle_thru_one_var_large(vector<int> & disagree)
{

  for(int n=0; n<disagree.size(); n++){

    int node = disagree[n];

    //int node = disagree[rand()%disagree.size()];

    vector<int> p0; // the path from node%N (its original copy) to root
    vector<int> p1; // the path from 'node' to root

    // find then path between 'node' and its original copy (node%N)

    int i=node; // find the path from 'node' to root
    p1.push_back(i);
    while(CT.upstream[i]>=0){
      p1.push_back(CT.upstream[i]);
      i = CT.upstream[i];
    }
    i = node%N; // find the path from the original copy of 'node' to root
    p0.push_back(i);
    while(CT.upstream[i]>=0){
      p0.push_back(CT.upstream[i]);
      i = CT.upstream[i];
    }

    i=0;
    int meet;  // find out where p0 and p1 meet
    assert(p0[p0.size()-1] == p1[p1.size()-1]);
    while(p0[p0.size()-1-i] == p1[p1.size()-1-i]){
      meet = p0[p0.size()-1-i];
      i++;
    }
    int j=p1.size()-1-i;
    vector<int> p; // construct path
    i=0;
    while(p0[i]!=meet)
      p.push_back(p0[i++]);
    p.push_back(meet);
    for(i=j; i>0; i--)
      p.push_back(p1[i]);
    p.push_back(node%N);

    // construct cycle from p

    cp_cycle c = cp_cycle(p);
    if(!cycle_exists(c)){
      CY.push_back(c);
      update_cycle_indexes(0);
      return 1;
    }
  }
  return 0;

  /*
  cout << "node" << node << endl;
  cout << "meet" << meet << endl;
  cout << "upstream" << endl;
  print_vector(CT.upstream);
  cout << "p0:" << endl;
  print_vector(p0);
  cout << "p1:" << endl;
  print_vector(p1);
  cout << "p:" << endl;
  print_vector(p);
  */
}

// find shorteast path from b to a, ignoring the direct edge a--b
vector<int> cp_graph::shortest_path_ignoring_direct_edge(int a, int b) /* obsolete */
{
  // BFS
  queue<int> Q;
  vector<int> visited(N,0);
  vector<int> previous(N,-1);
  vector<int> P;
  P.clear();

  visited[a] = 1;
  for(int i=0; i<nb[a].size(); i++){
    if(nb[a][i] != b){
      Q.push(nb[a][i]);
      previous[nb[a][i]] = a;
      visited[nb[a][i]] = 1;
    }
  }
  while(!Q.empty()){
    int current_node = Q.front();
    for(int i=0; i<nb[current_node].size(); i++){
      if(!visited[nb[current_node][i]]){
        if(nb[current_node][i] == b){ // we are done
          P.push_back(b);
          P.push_back(current_node);
          do{
            current_node = previous[current_node];
            assert(current_node >= 0);
            P.push_back(current_node);
          }while(current_node!=a);
          return P;
        }else{
          Q.push(nb[current_node][i]);
          visited[nb[current_node][i]] = 1;
          previous[nb[current_node][i]] = current_node;
        }
      }
    }
  }
  return P;
}

// sample a cycle through given node, smaller cycles have higher changes
void cp_graph::sample_small_cycle(int node, vector<int> & cycle, int cycle_distribution)
{
  random_edge_weights(cycle_distribution);
  double min_len = 2*N;
  for(int i=0; i<nb[node].size(); i++){
    vector<int> c;
    double len = shortest_weighted_path_ignoring_direct_edge(nb[node][i], node, c);
    if(len + edge_weight[abs(pid[node][i])-1] < min_len){
      min_len = len + edge_weight[abs(pid[node][i])-1];
      cycle = c;
    }
  }
  cycle.push_back(node);
}

void cp_graph::find_smallest_cycle(int node, vector<int> & cycle) /* obsolete */
{
  int min_len = 2*N;
  for(int i=0; i<nb[node].size(); i++){
    vector<int> c = shortest_path_ignoring_direct_edge(nb[node][i], node);
    if(c.size() < min_len){
      min_len = c.size();
      cycle = c;
    }
  }
  cycle.push_back(node);
}

// return 0 if did not find a cycle that has not been added
int cp_problem::get_cycle_thru_one_var_small(vector<int> & disagree)
{
  vector<int> iterator = random_iterator(disagree.size());
  for(int i=0; i<disagree.size(); i++){
    int node = disagree[iterator[i]]%N;
    vector<int> p;

    G.sample_small_cycle(node, p, cycle_distribution);

    if(p.size() == 0)
      continue;

    assert(p.size() >= 4); // the cycle is at least a triangle '0-1-2-1'

    // construct cycle from p
    cp_cycle c = cp_cycle(p);
    if(!cycle_exists(c)){
      CY.push_back(c);
      update_cycle_indexes(0);
      return 1;
    }
  }
  return 0;
}

int cp_problem::cycle_exists(const cp_cycle & c)
{
  for(int i=0; i<CY.size(); i++)
    if (same_cycle(c, CY[i]) || same_cycle(reverse_cycle(c), CY[i]))
      return 1;
  return 0;
}

void cp_problem::init_cycle_zero()
{

  assert(warmstart_cycle == 0);

  int i = CY.size();
  assert(i>=0);
  i--;
  CY[i].K = K;
  //assert(CY[i].s.size()==0); // haven't been initilized
  assert(CY[i].p.size()==0);
  int n = CY[i].nodes.size()-1;
  //CY[i].s.assign(n, vector<double>(K,0));
  CY[i].p.assign(n, vector<double>(K*K,0));

  // copy assignment from covering tree 'CT' (which is optimal for zero parameters)
  CY[i].assignment.assign(n+1, -1);
  for(int j=0; j<n+1; j++){
    CY[i].assignment[j] = CT.assignment[CY[i].nodes[j]];
    assert(CY[i].assignment[j]>=0);
  }
  assert(CY[i].assignment[0] == CY[i].assignment[n]);
  CY[i].allzero = 1;
}

void cp_problem::init_cycle_split(int all)
{

  if (all == 1){ // split weights from CT and add to all cycles
    for(int i=0; i<CY.size(); i++){
      if(warmstart_cycle == 0)
        CY[i].maintain_heaps = 0;
      else
        CY[i].maintain_heaps = 1;
      CY[i].K = K;
      //assert(CY[i].s.size()==0); // haven't been initilized
      assert(CY[i].p.size()==0);
      int n = CY[i].nodes.size()-1;
      //CY[i].s.assign(n, vector<double>(K,0));
      CY[i].p.assign(n, vector<double>(K*K,0));
      //CY[i].assignment.assign(n+1, -1); // MAP solver always called right after this
      CY[i].allzero = 0;
    }

    // for each var, traverse all copies and split weights
    for(int var_id=0; var_id<N; var_id++){
      int m = CT.nc[var_id] + var_cp_cy[var_id].size(); // num of copies
      if(!unary_subproblem_allzero[var_id])
        m++;
      for(int k=0; k<CT.nc[var_id]; k++){
        for(int j=0; j<var_cp_cy[var_id].size(); j++){
          int c = var_cp_cy[var_id][j];
          int idx = var_cp_id[var_id][j];
          vector<double> tmp = multiply_s_v_const(1.0/m,  CT.s[var_id+k*N]);

          //add_vector(CY[c].s[idx], tmp, 1.0);
          for(int l=0; l<K; l++)
            CY[c].add_singleton(idx, l, tmp[l]);

          add_vector(CT.s[var_id+k*N], tmp, -1.0);

        }
        //multiply_s_v((double)(m-var_cp_cy[var_id].size())/m, CT.s[var_id+k*N]);
      }
      if(!unary_subproblem_allzero[var_id]){
        for(int j=0; j<var_cp_cy[var_id].size(); j++){
          int c = var_cp_cy[var_id][j];
          int idx = var_cp_id[var_id][j];
          vector<double> tmp = multiply_s_v_const(1.0/m, unary_subproblem[var_id]);

          //add_vector(CY[c].s[idx], tmp, 1.0)
          for(int l=0; l<K; l++)
            CY[c].add_singleton(idx, l, tmp[l]);

          add_vector(unary_subproblem[var_id], tmp, -1.0);
        }
        //multiply_s_v((double)(m-var_cp_cy[var_id].size())/m, unary_subproblem[var_id]);
      }
    }

    // for each edge, traverse all copies and split weights
    for(int edge_id = 0; edge_id<M; edge_id++){
      int m = 1+edge_cp_cy[edge_id].size();
      for(int j=0; j<edge_cp_cy[edge_id].size(); j++){
        int c = edge_cp_cy[edge_id][j];
        int idx = abs(edge_cp_id[edge_id][j])-1;
        vector<double> tmp = multiply_s_v_const(1.0/m, CT.p[edge_id]);
        if(edge_cp_id[edge_id][j] > 0){
          add_matrix(K, CY[c].p[idx], tmp,  1.0);
        }else{
          add_matrixT(K, CY[c].p[idx], tmp,  1.0);
        }
        add_matrix(K, CT.p[edge_id], tmp, -1.0);
      }
      //multiply_s_v(1.0/m, CT.p[edge_id]);
    }

    //for(int i=0; i<CY.size(); i++)
    //CY[i].init_heaps();

  }else{ // split weights and add to the last cycle (newly added)

    int i = CY.size();
    assert(i>=0);
    i--;
    if(warmstart_cycle == 0)
      CY[i].maintain_heaps = 0;
    else
      CY[i].maintain_heaps = 1;
    CY[i].K = K;
    //assert(CY[i].s.size()==0); // haven't been initilized
    assert(CY[i].p.size()==0);
    int n = CY[i].nodes.size()-1;
    //CY[i].s.assign(n, vector<double>(K,0));
    CY[i].p.assign(n, vector<double>(K*K,0));

    // split weights
    for(int j=0; j<n; j++){

      int var_id = CY[i].nodes[j];
      int m = var_cp_cy[var_id].size()-1; // number of existing copies in cycles
      assert(var_cp_cy[var_id][m] == i);
      assert(var_cp_id[var_id][m] == j);
      m += CT.nc[var_id]; // plus number of existing copies in trees

      if(!unary_subproblem_allzero[var_id])
        m++;

      // travese all existing m copies of the variable, split the weight 1/(m+1) vs m/(m+1)

      /*
      for(int k=0; k<var_cp_cy[var_id].size()-1; k++){
        int c = var_cp_cy[var_id][k];
        int idx = var_cp_id[var_id][k];

        vector<double> tmp = multiply_s_v_const(1.0/(m+1), CY[c].s[idx]);

        // CY[i].s[j] += CY[c].s[idx] * 1/(m+1)

        //add_vector(CY[i].s[j], tmp, 1.0);
        for(int l=0; l<K; l++)
          CY[i].add_singleton(j, l, tmp[l]);

        // CY[c].s[idx] *= m/(m+1)
        //add_vector(CY[c].s[idx], tmp, -1.0);
        for(int l=0; l<K; l++)
          CY[c].add_singleton(idx, l, -tmp[l]);

        //multiply_s_v((double)m/(m+1), CY[c].s[idx]);
      }
      */

      for(int k=0; k<CT.nc[var_id]; k++){
        vector<double> tmp =  multiply_s_v_const(1.0/(m+1), CT.s[var_id+k*N]);
        // CY[i].s[j] += CT.s[var_id+k*N] * 1/(m+1)
        //add_vector(CY[i].s[j], tmp, 1.0);
        for(int l=0; l<K; l++)
          CY[i].add_singleton(j, l, tmp[l]);
        add_vector(CT.s[var_id+k*N], tmp, -1.0);
        // CT.s[var_id+k*N] *= m/(m+1);
        //multiply_s_v((double)m/(m+1), CT.s[var_id+k*N]);
      }

      if(!unary_subproblem_allzero[var_id]){
        vector<double> tmp = multiply_s_v_const(1.0/(m+1), unary_subproblem[var_id]);
        //add_vector(CY[i].s[j], tmp, 1.0);
        for(int l=0; l<K; l++)
          CY[i].add_singleton(j, l, tmp[l]);
        add_vector(unary_subproblem[var_id], tmp, -1.0);
        //multiply_s_v((double)m/(m+1), unary_subproblem[var_id]);
      }

      // edge
      int edge_id = abs(CY[i].edges[j])-1;
      // travese all existing m copies of the edge, split the weight 1/(m+1) vs m/(m+1)
      m = edge_cp_cy[edge_id].size()-1; // number of existing copies in cycles
      assert(edge_cp_cy[edge_id][m] == i);
      assert(abs(edge_cp_id[edge_id][m])-1 == j);
      m += 1; // plus one copy in the covering tree

      for(int k=0; k<edge_cp_cy[edge_id].size()-1; k++){
        int c = edge_cp_cy[edge_id][k];
        int idx = abs(edge_cp_id[edge_id][k])-1;

        vector<double> tmp =         multiply_s_v_const(1.0/(m+1), CY[c].p[idx]);

        if(edge_cp_id[edge_id][k] * CY[i].edges[j] > 0){
          // CY[i].p[j] += CY[c].p[idx] * 1/(m+1)
          add_matrix(K, CY[i].p[j], tmp,  1.0);
        }else{
          // CY[i].p[j] += CY[c].p[idx]' * 1/(m+1)
          add_matrixT(K, CY[i].p[j], tmp,  1.0);
        }
        // CY[c].p[idx] *= m/(m+1)`
        add_matrixT(K,  CY[c].p[idx], tmp,  -1.0);
        //multiply_s_v((double)m/(m+1), CY[c].p[idx]);
      }

      vector<double> tmp = multiply_s_v_const(1.0/(m+1), CT.p[edge_id]);
      if(CY[i].edges[j] > 0){
        // CY[i].p[j] += CT.p[edge_id] * 1/(m+1)
        add_matrix(K, CY[i].p[j], tmp, 1.0);
      }else{
        // CY[i].p[j] += CT.p[edge_id]' * 1/(m+1)
        add_matrixT(K, CY[i].p[j], tmp, 1.0);
      }
      // CT.p[edge_id] *= m/(m+1)
      add_matrixT(K,  CT.p[edge_id], tmp, -1.0);
        //multiply_s_v((double)m/(m+1), CT.p[edge_id]);
    }
    CY[i].assignment.assign(n+1, -1); // MAP solver always called right after this
    CY[i].allzero = 0;

    CY[i].init_heaps();

  }
}

int cp_problem::add_cycle(int method, int init)
{
  if(CY.size() == max_num_cycle)
    return 0;

  assert(CYsize == CY.size());

  // find detached vars that disagrees on CT
  vector<int> disagree;
  for(int i=0; i<N; i++){
    int cp = 0;
    while(CT.nb[i+cp*N].size()>0){ // check all copies of the variable
      if(CT.assignment[i+cp*N] != CT.assignment[i]){
        disagree.push_back(i+cp*N);
      }
      if(i+(++cp)*N >= CT.nb.size())
        break;
    }
  }
  if(disagree.size()==0)
    return 0;

  //cout<< "disagree nodes:" << endl;
  //print_vector(disagree);

  // add a cycle
  if(method == 0){
    if(!get_cycle_thru_one_var_large(disagree))
      return 0;
  }else if(method == 1){
    if(!get_cycle_thru_one_var_small(disagree))
      return 0;
  }else{
    cout << "unspecified add_cycle_method" << endl; exit(0);
  }

  //cout << endl;
  //CY[CY.size()-1].print();
  //cout << endl;
  //cin.ignore();

  // initialize parameters for the newly added cycle
  if(init == 0){
    init_cycle_zero();
  }else if(init == 1){
    init_cycle_split(0);
  }else{
    cout << "unspecified add_cycle_init" << endl; exit(0);
  }
  return 1;
}

cp_cycle::cp_cycle(){
  maintain_heaps = 0;
  init_heaps_called = 0;
}

cp_cycle::cp_cycle(const cp_cycle & c){

  K = c.K;
  N = c.N;
  nodes = c.nodes;
  edges = c.edges;
  allzero = c.allzero;
  if(c.p.size()>0){
    p.assign(N,vector<double>());
    for(int i=0; i<N; i++)
      p[i] = c.p[i];
  }
  // constant = c.constant;
  //assert(c.init_heaps_called == 0);
  //assert(c.min_factor.size() == 0);
  init_heaps_called = c.init_heaps_called;
  maintain_heaps = c.maintain_heaps;
}

cp_cycle::~cp_cycle()
{
  if(maintain_heaps)
     for(int d=0; d<N; d++)
      for(int i=0; i<cy_P[d].size(); i++)
        cy_P[d][i].free_mem();
}

cp_cycle::cp_cycle(int size, int cardinality, int flag)
{
  N = size;
  K = cardinality;
  init_heaps_called = 0;
  nodes.assign(N,0);
  edges.assign(N,0);
  allzero = 0;
  p.assign(N, vector<double>(K*K,0.0));
  maintain_heaps = 0;
  for(int i=0; i<N; i++){
    nodes[i] = i+1;
    if(flag == 1){
      for(int j=0; j<K; j++){
        double s = randu(0.0, 1.0);
        add_singleton(i, j, s);
      }
    }
    for(int j=0; j<K*K; j++)
      p[i][j] = randu(0.0, 1.0);
  }
  assignment.assign(N,-1);
}

void cp_cycle::add_singleton(int i, int l, double a)
{
  for(int k=0; k<K; k++){
    p[i][l+k*K] += a;
    //p[i][l+k*K] += 0.5*a;
    //p[(N+i-1)%N][k+l*K] += 0.5*a;
  }
}

cp_cycle::cp_cycle(vector<int> _nodes)
{
  nodes = _nodes;
  N = nodes.size()-1;
  maintain_heaps = 0;
  init_heaps_called = 0;
  assert(N >= 3);
}

void cp_cycle::show_assignment()
{
  printf("\n cycle assignment: ");
  for(int i=0; i<nodes.size(); i++)
    printf("%d(%d) ", nodes[i], assignment[i]);
  printf("\n");
}

void cp_graph::show_assignment(){

  assert(status==1);
  printf("\n tree assignment: ");
  for(int i=0; i<nb.size(); i++)
    if(nb[i].size()>0)
      printf("%d(%d) ", i, assignment[i]);
  printf("\n");
}

void cp_cycle::print()
{
  assert(edges.size()+1 == nodes.size());
  printf("cycle: ");
  for(int i=0; i<nodes.size()-1; i++)
    printf("%d--(%d)--", nodes[i], edges[i]);
  printf("%d\n", nodes[nodes.size()-1]);
}

factor cp_cycle::cycle_factor(int a, int b)
{
  assert(b == a+1 || b == a-1 || (a==1 && b==N) || (a==N && b==1));
  vector<int> id(2,0);
  vector<long> card(2,K);
  id[0] = nodes[a-1];
  id[1] = nodes[b-1];
  factor f = factor(2, id, card, 0); // half singleton of 'a' plus half singleton of 'b' plus pairwise (a,b)
  if(b==a+1){
    f.phi = p[a-1];
  }else if(b==a-1){
    f.phi = matrix_transpose(K, p[b-1]);
  }else if (a==1 && b==N){
    f.phi = matrix_transpose(K, p[N-1]);
  }else{
    f.phi = p[N-1];
  }
  return f;
}

 // get assignment, node_minmarginal, edge_minmarginal
double cp_cycle::fast_min_sum_bp_with_minmarginals(int d, double & time)
{

  assert(maintain_heaps == 0);
  assert(allzero==0);
  assert(d>=0 && d<N);
  clock_t t;
  t = clock();

  vector< vector<double> > forward_msg(N-1, vector<double>());
  vector< vector<int> > forward_argmin(N-1, vector<int>(K*K,-1));

  edge_mmarginal[d] = p[(N-1+d)%N];
  forward_msg[0] = p[d];

  // forward pass
  for(int i=1; i < N-1; i++){
    forward_msg[i] = pff_min_sum(K, forward_msg[i-1], p[(i+d)%N], forward_argmin[i]);
  }

  // (forward_msg[N-2])' + p[N-1] -> (1,N)
  add_i2i(matrix_transpose(K, forward_msg[N-2]), edge_mmarginal[d]);

  int l;
  double min_energy = minimize(edge_mmarginal[d], l);


  vector<int> rot_assignment(N+1,-1);
  rot_assignment[N-1]=l%K;
  rot_assignment[0] = (l-rot_assignment[N-1])/K;
  rot_assignment[N] = rot_assignment[0];
  for(int i=N-2; i>=1; i--)
    rot_assignment[i] = forward_argmin[i][rot_assignment[0] + rot_assignment[i+1]*K];
  assignment = rotate_cycle_assignment(rot_assignment, -d);

  node_mmarginal[d] = reduce_i_const(K, edge_mmarginal[d]);
  ready[d] = 1;

  time = ((double)clock()-t)/CLOCKS_PER_SEC;

  obj = min_energy + constant;

  /////////////////////////////////////////////////////////////////////////////////
  // correctness check
#if DEBUG_MODE
  assert(fabs(obj-naive_min_sum_bp()) < EPS);
  assert(fabs(get_obj()-obj) < EPS);
  assert(fabs(minimize(node_mmarginal[d])-min_energy) < EPS);
  assert(fabs(node_mmarginal[d][assignment[d]]-min_energy) < EPS);
  assert(fabs(edge_mmarginal[d][assignment[(N-1+d)%N]+K*assignment[d]] - min_energy) < EPS);
#endif
  //////////////////////////////////////////////////////////////////////////////////////////////////////


  return obj;
}

// This is for debugging puspose only,
// it only return the MAP energy, WITHOUT getting the assignment!!!
double cp_cycle::naive_min_sum_bp()
{
  if (allzero)
    return 0.0;
  vector<double> current_factor = p[N-1];
  for(int i=0; i<N-2; i++){
    vector<double> result(K*K, INF);
    for(int k1=0; k1<K; k1++)
      for(int k2=0; k2<K; k2++)
        for(int k3=0; k3<K; k3++)
          result[k1+k3*K] = MIN(result[k1+k3*K], /*s[i][k2]+ */ p[i][k2+k3*K]+current_factor[k1+k2*K]);
    current_factor = result;
  }
  double min_energy = INF;
  for(int k1=0; k1<K; k1++)
    for(int k2=0; k2<K; k2++)
      min_energy = MIN(min_energy, /*s[N-2][k2]+s[N-1][k1]+ */ current_factor[k1+k2*K]+p[N-2][k2+k1*K]);
  return min_energy + constant;
}

doublepool cp_cycle::construct_doublepool(int d, int i, const vector<double> & minf)
{
  assert(d>=0 && d<N);
  doublepool P;
  P.KK = K*K;
  P.data_msg = fh_makekeyheap();
  P.data_init = fh_makekeyheap();
  P.inpool.assign(2, vector<int>());
  P.handle.assign(2, vector<void*>());
  P.f.assign(2,-1);
  if(i==0){
    P.f[0] = d;
    P.f[1] = (d+1)%N;
    //fh_insertkey(P.data_msg, INF, (void*)(0));
    //for(int i=0; i<2; i++){
    int i=0;
    P.inpool[i].assign(K*K, 1);
    for(int j=0; j<K*K; j++){
      double val = p[P.f[i]][j]; //  - minf[P.f[i]];
      P.handle[i].push_back(fh_insertkey(P.data_msg, val, (void*)(j*2+i)));
    }
    i=1;
    P.inpool[i].assign(K*K, 1);
    for(int j=0; j<K*K; j++){
      double val = p[P.f[i]][j]; //  - minf[P.f[i]];
      P.handle[i].push_back(fh_insertkey(P.data_init, val, (void*)(j*2+i)));
    }
  }else{
    P.f[1] = (i-1+d)%N;
    P.msg.assign(K*K,INF);
    P.inpool[0].assign(K*K, 0);
    P.handle[0].assign(K*K,(fibheap_el*)NULL);
    P.inpool[1].assign(K*K, 1);
    int i=1;
    for(int j=0; j<K*K; j++){
      double val = p[P.f[i]][j]; // - minf[P.f[i]];
      P.handle[i].push_back(fh_insertkey(P.data_init, val, (void*)(j*2+i)));
    }
  }
  P.min_key_msg = fh_minkey(P.data_msg);
  P.min_key_init = fh_minkey(P.data_init);
  return P;
}

double cp_cycle::incomplete_bp(int d, double & time)
{
  if(allzero){
    node_mmarginal[d].assign(K,0);
    edge_mmarginal[d].assign(K*K,0);
    obj = 0;
    ready[d] = 1;
    return 0.0;
  }
  ///////////////////////////////////
  double moveon_threshold = MOVEON_THRESHOLD*K*K*K;
  long moveon = MAX(1, (long)moveon_threshold);
  ///////////////////////////////////
  int L = N-2;
  int e0,e1;
  bool done;

  double ub = INF;  // ub: upper bound on minimum energy
  clock_t t;

  vector<double> minf(N,0);

  vector<double> rlb = lower_bounds(d);

  //double sum_min_init_factor = vector_sum(minf);


  vector<vector<int> > argM(L, vector<int>(K*K, (int)INF));

  vector<doublepool> P(N-2, doublepool());
  P.assign(N-2,doublepool());
  int cnt=0;
  P[cnt++] = construct_doublepool(d, 0, minf);
  for(int i=3; i<=N-1; i++)
    P[cnt++] = construct_doublepool(d, i, minf);

  int last_factor_id = (N-1+d)%N;

  for(int i=0; i<N; i++)
    minf[i] = minimize(p[i]);

  edge_mmarginal[d].assign(K*K,INF);

  vector<double> min_incoming_msg;
  min_incoming_msg.assign(L,INF);
  min_incoming_msg[0] = minf[P[0].f[0]]; //MIN(minf[P[0].f[0]], minf[P[0].f[1]]);
  int fid,assign0,assign1;
  long eid,tb;
  double sum;
  long updated;
  long final_assignment = -1;
  t = clock();
  do{
    done = true;
    for (int i=0; i<L; i++){ /* forward pass */
      updated = 0;
      double min_key = P[i].min_key(min_incoming_msg[i], minf[P[i].f[1]]);
      while(IsNotInf(min_key) && min_key <= ub-rlb[i] && updated < moveon){

        done = false;
        P[i].extract_min(fid, eid, min_incoming_msg[i], minf[P[i].f[1]]);

        //double minval = P[i].f[fid]>=0?(p[P[i].f[fid]][eid] - minf[P[i].f[fid]]):P[i].msg[eid];
        double minval = P[i].f[fid]>=0?p[P[i].f[fid]][eid]:P[i].msg[eid];

        assign0 = (int)floor((double)eid)%K;
        assign1 = (int)floor((double)eid/K)%K;
        if(fid==0){
          tb = assign1;
          e0 = assign1;
          e1 = assign0;
        }else{
          e0 = assign0*K;
          e1 = assign1*K;
          tb = assign0;
        }
        for(int idx=0; idx < K; idx++){

          if(P[i].inpool[1-fid][e0] <= 0){

            //double val = P[i].f[1-fid]>=0?(p[P[i].f[1-fid]][e0] - minf[P[i].f[1-fid]]):P[i].msg[e0];
            double val = P[i].f[1-fid]>=0?p[P[i].f[1-fid]][e0]:P[i].msg[e0];
            sum = minval + val;

            //assert(sum >= min_key);

            if(i < L-1){
              if ( sum < P[i+1].msg[e1] && sum < ub-rlb[i]){ // THIS DOES NOT PRESERVE A<=B<=A'

                updated++;
                argM[i][e1] = tb; // keep argmin for tracing back
                min_incoming_msg[i+1] = MIN(sum, min_incoming_msg[i+1]);
                if(P[i+1].inpool[0][e1] <= 0){
                  P[i+1].add(0, e1, sum);
                }else{
                  P[i+1].decrease_key(0, e1, sum);
                }
              }
            }else{
              if(IsNotInf(sum)){
                int e1t = reverse_pairwise_label(e1,K);
                // double last_factor = p[last_factor_id][e1t] - minf[last_factor_id];
                double last_factor = p[last_factor_id][e1t];
                sum += last_factor;
                edge_mmarginal[d][e1t] = MIN(edge_mmarginal[d][e1t],
                                             sum );//+ sum_min_init_factor);
                if( sum < ub ){
                  updated++;
                  argM[i][e1] = tb; // keep argmin for tracing back
                  final_assignment = e1;
                  ub = sum;
                }
              }
            }
          }
          if(fid==0){
            e0 += K; e1 += K;
          }else{
            e0++; e1++;
          }
        }
        min_key = P[i].min_key(min_incoming_msg[i], minf[P[i].f[1]]);
        //min_key = MIN(min_key,  min_key + minf[i+1]);
      }
    }
  }while(!done);
  time = ((double)clock()-t)/CLOCKS_PER_SEC;

  node_mmarginal[d] = reduce_i_const(K, edge_mmarginal[d]);

  assert(final_assignment >= 0);

  // trace back to get MAP assignment
  vector<int> rot_assignment(N+1,-1);
  rot_assignment[0] = final_assignment%K;
  rot_assignment[N-1] = (final_assignment-rot_assignment[0])/K;
  for(int i=N-2; i>=1; i--){
    long idx = rot_assignment[0] + rot_assignment[i+1]*K;
    rot_assignment[i] = argM[i-1][idx];
  }
  rot_assignment[N] = rot_assignment[0];
  assignment = rotate_cycle_assignment(rot_assignment, -d);

  for(int i=0; i<P.size(); i++){
    P[i].free_mem();
  }

  ready[d] = 1;
  obj = ub + constant;//+sum_min_init_factor;

  /////////////////////////////////////////////////////////////////////////////////
  // correctness check
#if DEBUG_MODE
  assert(fabs(get_obj()-obj) < EPS);
  assert(fabs(obj-naive_min_sum_bp()) < EPS);
  assert(fabs(minimize(node_mmarginal[d])-ub) < EPS);
  assert(fabs(node_mmarginal[d][assignment[d]]-ub) < EPS);
  assert(fabs(edge_mmarginal[d][assignment[(N-1+d)%N]+K*assignment[d]] - ub) < EPS);
#endif
  //////////////////////////////////////////////////////////////////////////////////////////////////////

  return obj;
}

cp_problem::cp_problem()
{
}

cp_problem::cp_problem(const cp_graph & g)
{
  graph_type = g.graph_type;

  N = g.N;
  K = g.K;
  M = g.M;
  G = cp_graph(g);
  constant = 0;
  for(int i=0; i<G.s.size(); i++)
    {
      double minval = minimize(G.s[i]);
      constant += minval;
      add_s_v(-minval, G.s[i]);
    }
  for(int i=0; i<G.p.size(); i++)
    {
      double minval = minimize(G.p[i]);
      constant += minval;
      add_s_v(-minval, G.p[i]);
    }

  if(N<=3 && graph_type == 2)
    {
      return;
    }

  CT = cp_graph(G);
  CT.convert_into_covering_tree(0, 0);

  max_num_cycle = -1;
  add_cycle_method = -1;
  add_cycle_init = -1;
  subgradient_stepsize = 0.01;
  cycle_distribution = -1;
  add_cycle_interval = -2;
  add_cycle_batch = -1;
  dual_method = -1;
  pbca_update = 0;
  pbca_minmarginal = -1;
  pbca_normalize = 0;
  warmstart_cycle = 0;
  damping = 0;

}

void cp_problem::print_statistics()
{
  printf("add_cy: %.3f, cp: %.3f, cy: %.3f(%.3f), dual: %.3f\n", running_time_addcycle,
         running_time_solve_cp, running_time_solve_cy, running_time_solve_cy_kernel, running_time_update_dual);
}

void cp_problem::initialize()
{


  if(pbca_update==1){
    cout << "pbca_update==1" << endl;
    cout << "are you sure?" << endl;
    exit(0);
  }

  assert(pbca_minmarginal != 1);

  //cout << "initializing ... " << endl;
  //printf("initializing problem: graph_type=%d, cycle_method=%d, dual_method=%d\n", graph_type, cycle_method, dual_method);
  iter = 0;
  running_time = 0;
  running_time_addcycle = 0;
  running_time_solve_cp = 0;
  running_time_solve_cy = 0;
  running_time_solve_cy_kernel = 0;
  running_time_update_dual = 0;
  primal_obj = INF;   current_primal_obj = INF;
  dual_obj = 0;

  if((N==2) || (N == 3 && graph_type==2) || (N==4 && graph_type==0)) // edge or triangle or square
    return;

  if(damping > 0)
    last_update.assign(N+M, vector< vector<double> >());

  //pbca_updated = 1;

  var_cp_cy.assign(N, vector<int>());
  var_cp_id.assign(N, vector<int>());
  edge_cp_cy.assign(M, vector<int>());
  edge_cp_id.assign(M, vector<int>());
  current_assignment.assign(N,-1);
  assignment.assign(N,-1);
  assert(cycle_method >= 0 && cycle_method <= 3);
  assert(dual_method >= 0 && dual_method <= 1);

  unary_subproblem_allzero.assign(N,1);
  unary_subproblem_min.assign(N,0);

  if(dual_method == 1){
    unary_subproblem.assign(N, vector<double>(K,0));
    unary_subproblem_assignment.assign(N,-1);
    for(int i=0; i<N; i++){
      if(CT.nc[i] > 1){
        unary_subproblem_allzero[i] = 0;
        int m = CT.nc[i]+1;
        for(int j=0; j<CT.nc[i]; j++){
          vector<double> tmp = multiply_s_v_const(1.0/(double)m, CT.s[i+j*N]);
          add_vector(unary_subproblem[i], tmp, 1.0);
          add_vector( CT.s[i+j*N], tmp, -1.0);
        }
        minimize(unary_subproblem[i], unary_subproblem_assignment[i]);
      }
    }
  }
  int maxN = 1;
  if(cycle_method >= 2){ // add first level cycles
    if(graph_type == 0){  // nxn grid
      get_squares_for_grid(CY, (int)sqrt(N));
      maxN = 4;
    }else if(graph_type == 1){ // bicyle
      cout << "not implemented" << endl; exit(0);
    }else if(graph_type == 2){ // fully connected
      get_triangles_for_fully_connected(CY, N);
      maxN = 3;
    }else{
      cout << "unrecognized graph_type" << endl; exit(0);
    }
  }

  if(cycle_method >= 3){ // add second level cycles
    if(graph_type == 0){  // nxn grid
      get_rectangles_for_grid(CY, (int)sqrt(N));
      maxN = 6;
    }else if(graph_type == 1){ // bicyle
      cout << "not implemented" << endl; exit(0);
    }else if(graph_type == 2){ // fully connected
      get_quads_for_fully_connected(CY, N);
      maxN = 4;
    }else{
      cout << "unrecognized graph_type" << endl; exit(0);
    }
  } // add even more cycles when cycle_method >= 4 ?

  CYsize = CY.size(); // obsolete

  if(CY.size()>0){
    update_cycle_indexes(1);
    init_cycle_split(1);
    /*
    if(dual_method == 1){
      for(int j=1; j<maxN; j++){
        for(int i=0; i<CYsize; i++){
          if(j < CY[i].N){
            CY.push_back(CY[i].rotate(j));
          }else{
            CY.push_back(cp_cycle());
          }
        }
      }
    */
    set_needed_mmarginals_in_cycles();
  }


  for(int i=0; i<CY.size(); i++)
    CY[i].init_heaps();

  for(int i=0; i<CY.size(); i++)
    assert(CY[i].init_heaps_called == 1);

  if(0) printf("dual_method:%d cycle_method:%d marginals:%d warmstart:%d damping:%.2f #cycle:%d\n",
         dual_method, cycle_method, pbca_minmarginal, warmstart_cycle, damping, CYsize);
}

double cp_cycle::run_map_inference(int dual_method, int pbca_minmarginal, int warmstart_cycle, int d, double & time)
{
  assert(!ready[d]);

  /*
  printf("\n");
  printf("factor_updated: ");
  for(int i=0; i<N; i++)
    printf("%d ", factor_updated[d][i]);
  printf("\n");
  factor_updated[d].assign(N,0);
  */

  // TODO apply a condition on it

  int repara_cycle = 1;

  for(int idx=0; idx < repara_cycle*N ; idx++)
    reparameterize_singleton(idx%N);


  if(dual_method == 1 && pbca_minmarginal == 0){
    return fast_min_sum_bp_with_minmarginals(d, time);
  }else{
    if(warmstart_cycle)
      return incomplete_bp_warmstart(d, time);
    else
      return incomplete_bp(d, time);
  }
  //factor_updated[d].assign(N,0);
}

int cp_problem::solve_subproblems()
{
  clock_t t;
  t = clock();
  dual_obj = constant;
  if(dual_method == 1)
    dual_obj += CT.min_sum_bp_covering_tree_get_minmarginals();
  else
    dual_obj += CT.min_sum_bp_covering_tree();
  for(int i=0; i<N; i++){
    if(!unary_subproblem_allzero[i]){
      unary_subproblem_min[i] = minimize(unary_subproblem[i],
                                         unary_subproblem_assignment[i]);
      dual_obj += unary_subproblem_min[i];
    }
  }
  running_time_solve_cp += ((double)clock()-t)/CLOCKS_PER_SEC;

  t = clock();
  double time;

  int cycle_solved = 0;
  for(int i=0; i< CYsize; i++){
    int d = 0;
    if(dual_method==1){
      int idx_needed = CY[i].next_mmarginal_needed[CY[i].idx_next_mmarginal_needed];
      d = (idx_needed<CY[i].N)?idx_needed:(idx_needed+1)%CY[i].N;
    }
    assert(d>=0 && d<CY[i].N);
    if(CY[i].ready[d] == 0){
      cycle_solved++;
      dual_obj += CY[i].run_map_inference(dual_method, pbca_minmarginal, warmstart_cycle, d, time);
    }else{
      time = 0;
      dual_obj += CY[i].obj;
    }
    running_time_solve_cy_kernel += time;
  }
  running_time_solve_cy += ((double)clock()-t)/CLOCKS_PER_SEC;

  return cycle_solved;
}


int cp_problem::check_dual_variables()
{
  for(int i=0; i<M; i++){ // for all edges

    vector<double> original_p = G.p[i];

    // go thru all copies, substract parameters from original_p
    // subtract CT.p[i] from original_p
    add_matrix(K, original_p, CT.p[i], -1.0);

    for(int j=0; j<edge_cp_cy[i].size(); j++){ // find all its copies in cycles
      int c = edge_cp_cy[i][j];
      int idx = abs(edge_cp_id[i][j])-1;
      // substract CY[c].p[idx] from original_p (transpose if necessary)
      if(edge_cp_id[i][j] > 0)
        add_matrix(K, original_p, CY[c].p[idx], -1.0);
      else
        add_matrixT(K, original_p, CY[c].p[idx], -1.0);
    }

    assert(matrix_is_a_plus_b(K, original_p));
  }
  return 1;
}

/*
int cp_problem::get_next_block_for_pbca()
{
  int block = block_iterator[block_iterator_idx++];
  if (block_iterator_idx == N+M){
    init_block_iterator_for_pbca();
  }
  return block;
}

void cp_problem::init_block_iterator_for_pbca()
{
  block_iterator = random_iterator(N+M);
  block_iterator_idx = 0;
}
*/

/*
int cp_problem::compute_sparse_approx_mmarginals(int block, int node_id)
{
  if(block < N){
    int m = 1 + var_cp_cy[block].size(); // num of copies of this var
    if(!unary_subproblem_allzero[block])
      m++;
    if (m == 1)
      return 0;
    vector<int> active;

    if(1){
      for(int t=0; t<CT.nc[block]; t++){
        int idx = block + t*N;
        active.push_back(CT.assignment[idx]);
      }
    }else{
      active.push_back(CT.assignment[node_id]);
    }

    for(int k=0; k<var_cp_cy[block].size(); k++){
      int c = var_cp_cy[block][k];
      int idx = var_cp_id[block][k];
      assert(CY[c].nodes[idx] == block);
      active.push_back(CY[c].assignment[idx]);
    }

    if(!unary_subproblem_allzero[block]){
      assert(unary_subproblem_assignment[block] >= 0);
      for(int l=0; l<K; l++)
        assert(unary_subproblem[block][unary_subproblem_assignment[block]] - unary_subproblem[block][l] <= EPS);
      active.push_back(unary_subproblem_assignment[block]);
    }

    remove_repeated_active_labels(active, K);

    //if(active.size()==1) // even that we should perform some update
    //  return 0;

     for(int k=0; k<var_cp_cy[block].size(); k++){
       int c = var_cp_cy[block][k];
       int idx = var_cp_id[block][k];
       assert(CY[c].nodes[idx] == block);

       CY[c].node_mmarginal[idx].assign(K, INF);

       for(int j=0; j<active.size(); j++){
         int l = active[j];
         //CY[c].node_mmarginal[idx][l] = CY[c].evaluate_node_perturbation_globally(idx, l);
         cout << "not valid" << endl;          exit(0);
       }
     }
     return 1;
  }else{  // edge

    int edge_id = block-N;
    int id0 = CT.edge_nodes[edge_id][0];
    int id1 = CT.edge_nodes[edge_id][1];
    int m = 1 + edge_cp_cy[edge_id].size(); // num of copies of this edge

    if(m==1)
      return 0;

    vector<int> active;
    // go over all copies of the edge, collect active labelings

    active.push_back(CT.assignment[id0]+CT.assignment[id1]*K);

    for(int k=0; k<edge_cp_cy[edge_id].size(); k++){
      int c = edge_cp_cy[edge_id][k];
      int idx = abs(edge_cp_id[edge_id][k])-1;
      if(edge_cp_id[edge_id][k]>0){
        id0 = idx;
        id1 = idx+1;
        assert(CY[c].nodes[id0] ==  CT.edge_nodes[edge_id][0]%N);
        assert(CY[c].nodes[id1] ==  CT.edge_nodes[edge_id][1]%N);
      }else{
        id0 = idx+1;
        id1 = idx;
        assert(CY[c].nodes[id0] ==  CT.edge_nodes[edge_id][0]%N);
        assert(CY[c].nodes[id1] ==  CT.edge_nodes[edge_id][1]%N);
      }
      active.push_back(CY[c].assignment[id0]+CY[c].assignment[id1]*K);
    }

    remove_repeated_active_labels(active, K*K);

    for(int k=0; k<edge_cp_cy[edge_id].size(); k++){
      int c = edge_cp_cy[edge_id][k];
      int idx = abs(edge_cp_id[edge_id][k])-1;

      CY[c].edge_mmarginal.assign(K*K, INF);

      for(int j=0; j<active.size(); j++){
        int l;
        if(edge_cp_id[edge_id][k]>0)
          l = active[j];
        else{
          l = reverse_pairwise_label(active[j],K);
        }
        //CY[c].edge_mmarginal[idx][l] = CY[c].evaluate_edge_perturbation_globally(idx, l);
        cout << "not valid" << endl;          exit(0);
      }
    }
    return 1;
  }
}
*/

// 'block' points to the block(var/edge) to update
// which is an index into [0,1,...,N-1,N,N+1,N+2,...,N+M-1]
int cp_problem::pbca(int block, int node_id)
{
  assert(pbca_normalize == 0);
  int updated = 0;

  if(block < N){
      int m = 1 + var_cp_cy[block].size(); // num of copies of this var
      if(!unary_subproblem_allzero[block])
        m++;

      if (m == 1)
        return 0;

      vector<double> average(K,0);
      vector< vector<double> > X(m, vector<double>(K,0));

      // go over all copies of the var, collect min-marginals
      assert(CT.node_mmarginal[node_id].size() == K);
      for(int l=0; l<K; l++)
        X[0][l] = CT.node_mmarginal[node_id][l] - (CT.obj * pbca_normalize);

      for(int k=0; k<var_cp_cy[block].size(); k++){

        int c = var_cp_cy[block][k];
        int idx = var_cp_id[block][k];

        assert(CY[c].nodes[idx] == block);
        assert(CY[c].ready[idx]);

        CY[c].idx_next_mmarginal_needed = (CY[c].idx_next_mmarginal_needed+1)%(CY[c].N*2);
        for(int l=0; l<K; l++){
          X[k+1][l] = CY[c].node_mmarginal[idx][l] + CY[c].constant - (CY[c].obj * pbca_normalize);
        }
      }

      if(!unary_subproblem_allzero[block])
        for(int l=0; l<K; l++)
          X[m-1][l] = unary_subproblem[block][l] - (unary_subproblem_min[block] * pbca_normalize);

      ///////////////////////////////////////
      vector<bool> all_identical(K,1);
      vector<bool> all_inf(K,1);
      for(int l=0; l<K; l++){
        int not_inf_cnt = 0;
        double last_val = INF;
        for(int k=0; k<m; k++){
          if(IsNotInf(X[k][l])){
            not_inf_cnt++;
            average[l] += X[k][l];
            all_inf[l] = false;
            if(fabs(X[k][l] - last_val) > EPS && IsNotInf(last_val))
              all_identical[l] = 0;
            last_val = X[k][l];
          }
        }
        average[l] /= (double)not_inf_cnt;
        if(not_inf_cnt == 1)
          assert(all_identical[l]);
        if(!all_identical[l] && !all_inf[l])
          updated = 1;
      }

      ///////////////////////////////////////////////////////////////////
      if(updated == 0 && damping == 0){
        return 0;
      }
      vector< vector<double> > pbca_move(m, vector<double>(K,0));

      // compute update to be performed
      for(int l=0; l<K; l++)
        for(int i=1; i<m; i++)
          if(IsNotInf(X[i][l])){
            pbca_move[i][l] = average[l] - X[i][l];
            pbca_move[0][l] -= pbca_move[i][l];
          }

      // if damping, combine it with last_update, and save update to last update
      if(damping > 0)
        damp_pbca_update(pbca_move, last_update[block], damping);
      /////////////////////////////////////////////////////////////////////

      for(int k=0; k<var_cp_cy[block].size(); k++){
        int c = var_cp_cy[block][k];
        int idx = var_cp_id[block][k];

	/*
        for(int d=0; d<CY[c].N; d++){
          CY[c].factor_updated[d][idx]++;
          CY[c].factor_updated[d][(idx+1)%CY[c].N]++;
        }
	*/

        CY[c].add_vec_singleton_update_heaps(idx, pbca_move[k+1]);
      }

      if(!unary_subproblem_allzero[block])
        for(int l=0; l<K; l++)
          if( pbca_move[m-1][l] != 0){
            unary_subproblem[block][l] += pbca_move[m-1][l];
          }

      // go over all copies of the var, make update using the "average min-marginals"
      assert(CT.s[node_id].size() == K);
      for(int l=0; l<K; l++){
        CT.s[node_id][l] += pbca_move[0][l];
      }

    }else{ // edge

      int edge_id = block-N;
      int id0 = CT.edge_nodes[edge_id][0];
      int id1 = CT.edge_nodes[edge_id][1];
      int m = 1 + edge_cp_cy[edge_id].size(); // num of copies of this edge

      if(m == 1)
        return 0;

      vector<double> average(K*K,0);
      vector< vector<double> > X(m, vector<double>(K*K,0));

      for(int l=0; l<K*K; l++)
        X[0][l] = CT.edge_mmarginal[edge_id][l] - (CT.obj * pbca_normalize);

      for(int k=0; k<edge_cp_cy[edge_id].size(); k++){

        int idx = abs(edge_cp_id[edge_id][k])-1;
        int c = edge_cp_cy[edge_id][k];
        int d = ((idx+1)%CY[c].N);

        assert(CY[c].ready[d]);

        CY[c].idx_next_mmarginal_needed = (CY[c].idx_next_mmarginal_needed+1) % (CY[c].N*2);

        ///////////// comment out
        //int l1 = CY[c].assignment[idx]+CY[c].assignment[idx+1]*K;
        //assert(fabs(CY[c].edge_mmarginal[d][l1] - CY[c].obj)<EPS);
        /////////////////////////////////////////

        for(int l=0; l<K*K; l++){
          int l0;
          if(edge_cp_id[edge_id][k]>0)
            l0 = l;
          else
            l0 = reverse_pairwise_label(l,K);
          X[k+1][l0] = CY[c].edge_mmarginal[d][l] + CY[c].constant - (CY[c].obj * pbca_normalize);
        }
      }

      ///////////////////////////////////////
      vector<bool> all_identical(K*K,1);
      vector<bool> all_inf(K*K,1);
      for(int l=0; l<K*K; l++){
        int not_inf_cnt = 0;
        double last_val = INF;
        for(int k=0; k<m; k++){
          if(IsNotInf(X[k][l])){
            not_inf_cnt++;
            average[l] += X[k][l];
            all_inf[l] = false;
            if(fabs(X[k][l] - last_val) > EPS && IsNotInf(last_val))
              all_identical[l] = 0;
            last_val = X[k][l];
          }
        }
        average[l] /= (double)not_inf_cnt;
        if(not_inf_cnt == 1)
          assert(all_identical[l]);
        if(!all_identical[l] && !all_inf[l])
          updated = 1;
      }
      //////////////////////////////////////////////////////////////////
      if(updated == 0 && damping == 0){
        return 0;
      }
      vector< vector<double> > pbca_move(m, vector<double>(K*K,0));

      // compute update to be performed
      for(int l=0; l<K*K; l++)
        for(int i=1; i<m; i++)
          if(IsNotInf(X[i][l])){
            pbca_move[i][l] = average[l] - X[i][l];
            pbca_move[0][l] -= pbca_move[i][l];
          }

      // if damping, combine it with last_update, and save update to last update
      if(damping > 0)
        damp_pbca_update(pbca_move, last_update[block], damping);
      /////////////////////////////////////////////////////////////////////


      for(int k=0; k<edge_cp_cy[edge_id].size(); k++){
        int c = edge_cp_cy[edge_id][k];
        int idx = abs(edge_cp_id[edge_id][k])-1;

	/*
        for(int d=0; d<CY[c].N; d++)
          CY[c].factor_updated[d][idx]++;
	*/

        if(edge_cp_id[edge_id][k]>0)
          CY[c].add_vec_pairwise_update_heaps(idx, pbca_move[k+1]);
        else
          CY[c].add_vec_pairwise_update_heaps(idx, matrix_transpose(K, pbca_move[k+1]));

        /*
        for(int l=0; l<K*K; l++){
          int l0;
          if(edge_cp_id[edge_id][k]>0)
            l0 = l;
          else
            l0 = reverse_pairwise_label(l,K);
          if( pbca_move[k+1][l0] != 0){
            CY[c].add_pairwise_update_heaps(idx, l, pbca_move[k+1][l0]);
          }
          }*/
      }

      for(int l=0; l<K*K; l++){
        CT.p[edge_id][l] += pbca_move[0][l];
      }
  }
  return updated;
}

void cp_problem::remove_repeated_active_labels(vector<int> & active, int maxlabel)
{
  vector<int> labels(maxlabel, 0);
  int j=0;
  while(j<active.size()){
    if(labels[active[j]] == 0){
      labels[active[j]] = 1;
      j++;
    }else{
      active.erase(active.begin()+j);
    }
  }
}

// alpha is step size, primal solution and obj will be updated if get_primal_sol=1 OR converged.
int cp_problem::apply_subgradient(double alpha, int get_primal_sol) // return 1 if subgradient=0 (converged)
{
  // "average" unary assignment indicator variables
  vector< vector<int> > smallx(N, vector<int>(K, 0));
  // "average" pairwise assignment indicator variables
  vector< vector<int> > bigX(M, vector<int>(K*K, 0));
  vector< int > smallx_count(N,0); // counter, used as denominator
  vector< int > bigX_count(M,0);   // counter, used as denominator

  // keep track of active "states" for faster operations
  vector< vector<int> > active_smallx(N, vector<int>());
  vector< vector<int> > active_bigX(M, vector<int>());

  bool converged = true;

  /////////////////////////////////////////////////////////
  // go through sub-problems to collect "average" solution
  /////////////////////////////////////////////////////////
  for(int i=0; i<CT.nb.size(); i++){ // collect solution from covering tree
    if(CT.assignment[i] < 0) continue; // vacant index
    int var_id = i%N;
    smallx[var_id][CT.assignment[i]] ++;
    smallx_count[var_id]++;
    active_smallx[var_id].push_back(CT.assignment[i]);
    for(int j=0; j<CT.nb[i].size(); j++){
      if(CT.pid[i][j]>0){ // ignore negative indexes as we only need to visit each edge once
        int edge_id = CT.pid[i][j]-1;
        bigX[edge_id][ CT.assignment[i] + CT.assignment[CT.nb[i][j]]*K ]++;
        bigX_count[edge_id]++;
        active_bigX[edge_id].push_back( CT.assignment[i] + CT.assignment[CT.nb[i][j]]*K );
      }
    }
  }
  for(int c=0; c<CYsize; c++){ // collect solutions from cycles

    for(int i=0; i<CY[c].nodes.size()-1; i++){
      int var_id = CY[c].nodes[i]%N;
      smallx[var_id][CY[c].assignment[i]] ++;
      smallx_count[var_id]++;
      active_smallx[var_id].push_back(CY[c].assignment[i]);

      //int nb_id = CY[c].nodes[i+1]%N;
      // edge: (var_id, nb_id)

      int edge_id = abs(CY[c].edges[i])-1;
      if(CY[c].edges[i] > 0){
        bigX[edge_id][ CY[c].assignment[i] + CY[c].assignment[i+1]*K ]++;
        bigX_count[edge_id]++;
        active_bigX[edge_id].push_back( CY[c].assignment[i] + CY[c].assignment[i+1]*K );
      }else{
        bigX[edge_id][ CY[c].assignment[i+1] + CY[c].assignment[i]*K ]++;
        bigX_count[edge_id]++;
        active_bigX[edge_id].push_back( CY[c].assignment[i+1] + CY[c].assignment[i]*K );
      }

    }
  }

  // remove repeated items from lists of active assignments
  for(int i=0; i<N; i++)
    remove_repeated_active_labels(active_smallx[i], K);
  for(int i=0; i<M; i++)
    remove_repeated_active_labels(active_bigX[i], K*K);

  ///////////////////////////////////////////////////////
  // go through sub-problems again to update parameters
  ///////////////////////////////////////////////////////
  for(int i=0; i<CT.nb.size(); i++){ // update parameters for covering tree
    if(CT.assignment[i] < 0) continue; // vacant index
    int var_id = i%N;
    // update singleton potentials
    for(int kidx=0; kidx<active_smallx[var_id].size(); kidx++){
      int k = active_smallx[var_id][kidx];
      double subgradient = ((double)(CT.assignment[i]==k)-(double)smallx[var_id][k]/(double)smallx_count[var_id]);
      converged = (converged && subgradient==0); // converged if we have agreement at all unary indicator variables
      CT.s[i][k] += alpha * subgradient;
    }

    for(int j=0; j<CT.nb[i].size(); j++){
      if(CT.pid[i][j]>0){ // ignore negative indexes as we only need to visit each edge once
        int edge_id = CT.pid[i][j]-1;
        // update pairwise potentials
        for(int kidx=0; kidx<active_bigX[edge_id].size(); kidx++){
          int k = active_bigX[edge_id][kidx];
          double subgradient = ((double)(CT.assignment[i]==k%K && CT.assignment[CT.nb[i][j]]==(k-k%K)/K)-
                                (double)bigX[edge_id][k]/(double)bigX_count[edge_id]);
          if(CYsize == 0)
            assert(subgradient == 0); // because we have only one copy per edge in covering tree

          //if(edge_id==3 && subgradient != 0)
          //  printf("\n subgradient at CT: k=%d, (%d=%d, %d=%d):%f\n", k, i, k%K,
          //   CT.nb[i][j], (k-k%K)/K, subgradient);

          CT.p[edge_id][k] += alpha * subgradient;

        }
      }
    }
  }
  for(int c=0; c<CYsize; c++){ // update parameter for cycles
    CY[c].allzero = 0;
    CY[c].ready.assign(N,0);
    for(int i=0; i<CY[c].nodes.size()-1; i++){
      int var_id = CY[c].nodes[i]%N;
      vector<double> a(K,0);
      for(int kidx=0; kidx<active_smallx[var_id].size(); kidx++){
        int k = active_smallx[var_id][kidx];
        double subgradient = ((double)(CY[c].assignment[i]==k)-(double)smallx[var_id][k]/(double)smallx_count[var_id]);
        converged = (converged && subgradient==0); // converged if we have agreement at all unary indicator variables
        a[k] = alpha*subgradient;
      }
      CY[c].add_vec_singleton_update_heaps(i, a);

      int edge_id = abs(CY[c].edges[i])-1;
      // update pairwise potentials

      vector<double> A(K*K,0);
      for(int kidx=0; kidx<active_bigX[edge_id].size(); kidx++){
        int k = active_bigX[edge_id][kidx];
        if(CY[c].edges[i] > 0){
          double subgradient = ((double)(CY[c].assignment[i]==k%K && CY[c].assignment[i+1]==(k-k%K)/K)-
                                (double)bigX[edge_id][k]/(double)bigX_count[edge_id]);
          A[k] = alpha * subgradient;
        }else{
          double subgradient = ((double)(CY[c].assignment[i]==(k-k%K)/K && CY[c].assignment[i+1]==k%K)-
                                (double)bigX[edge_id][k]/(double)bigX_count[edge_id]);
          A[K*(k%K) +(k-k%K)/K] = alpha * subgradient;
        }
      }
      CY[c].add_vec_pairwise_update_heaps(i, A);
    }
  }
  if(converged || get_primal_sol){ // do this inside subgradient subroutine so we can re-use "smallx"
    primal_sol_by_voting(smallx, active_smallx);

    ///////// check correctness ////////////////// OPT comment out when correctness is confirmed
    //vector<int> a = assignment;
    //primal_sol_by_voting();
    //for(int i=0; i<N; i++)
    //  assert(a[i] == assignment[i]);
    /////////////////////////////////////////////
  }

  return converged;
}

void cp_problem::primal_sol_by_voting()
{
  // assignment by majority vote
  current_assignment.assign(N,-1);
  for(int i=0; i<N; i++){

    vector<int> x(K,0);

    if(!unary_subproblem_allzero[i]){
      assert(unary_subproblem_assignment[i]>=0);
      x[unary_subproblem_assignment[i]]++;
    }

    // go thru all copies of the variable and collect solutions
    for(int k=0; k<CT.nc[i]; k++){
      int node_id = i + k*N;
      x[CT.assignment[node_id]]++;
    }
    for(int k=0; k<var_cp_cy[i].size(); k++){
      int c = var_cp_cy[i][k];
      int idx = var_cp_id[i][k];
      assert(CY[c].nodes[idx] == i);
      x[CY[c].assignment[idx]]++;
    }
    double maxval = 0;
    for(int k=0; k<K; k++){
      if (x[k] > maxval){
        maxval = x[k];
        current_assignment[i] = k;
      }else if(x[k] == maxval /*&& k < assignment[i]*/){
        if(rand()%10 < 5)
          current_assignment[i] = k; // break ties randomly, which could help to bring the primal obj down faster
      }
    }
    assert(current_assignment[i]>=0);
  }
  current_primal_obj = get_primal_obj();
  if(current_primal_obj < primal_obj)     {primal_obj = current_primal_obj; assignment = current_assignment; }
}

void cp_problem::primal_sol_by_voting(const vector< vector<int> > & x, const vector< vector<int> > & active)
{
  // assignment by majority vote
  current_assignment.assign(N,-1);
  for(int i=0; i<N; i++){
    double maxval = 0;
    for(int j=0; j<active[i].size(); j++){
      int k=active[i][j];
      if (x[i][k] > maxval){
        maxval = x[i][k];
        current_assignment[i] = k;
      }else if(x[i][k] == maxval /*&& k < assignment[i]*/){
        if(rand()%10 < 5)
          current_assignment[i] = k; // break ties randomly, which could help to bring the primal obj down faster
      }
    }
    assert(current_assignment[i]>=0);
  }
  current_primal_obj = get_primal_obj();
  if(current_primal_obj < primal_obj)     {primal_obj = current_primal_obj; assignment = current_assignment; }
}

double cp_problem::get_primal_obj(){
  double new_primal_obj = constant;
  assert(G.s.size() == N);
  assert(G.nb.size() == N);
  for(int i=0; i<N; i++){
    new_primal_obj += G.s[i][current_assignment[i]];
    for(int j=0; j<G.nb[i].size(); j++){
      if(G.pid[i][j] > 0){ // visit each edge only once
        new_primal_obj += G.p[G.pid[i][j]-1][current_assignment[i] + current_assignment[G.nb[i][j]]*K];
      }
    }
  }
  return new_primal_obj;
}

cp_graph::cp_graph()
{

}


cp_graph::cp_graph(const cp_graph & g) // make a copy
{
  graph_type = g.graph_type;
  status = g.status;
  N = g.N;
  L = g.L;
  M = g.M;
  K = g.K;
  nc = g.nc;
  edge_nodes = g.edge_nodes;

  nb.assign(g.nb.size(), vector<int>());
  pid.assign(g.pid.size(), vector<int>());
  s.assign(g.s.size(), vector<double>());
  p.assign(g.p.size(), vector<double>());

  for(int i=0; i<nb.size(); i++){
    nb[i] = g.nb[i];
    s[i] = g.s[i];
    pid[i] = g.pid[i];
  }
  for(int i=0; i<p.size(); i++)
    p[i] = g.p[i];

  if(status==1){
    msg_from = g.msg_from;
    msg_to = g.msg_to;
    msg_via = g.msg_via;
  }

  edge_weight = g.edge_weight;
}

cp_graph::cp_graph(int _graph_type, int n){

  graph_type = _graph_type;
  if(graph_type == 0)
    create_grid(n);
  else if(graph_type == 1)
    create_bicycle(n);
  else if(graph_type == 2)
    create_fully_connected_graph(n);
  else
    cout << "graph_type not recognized" << endl;

  status = 0;

  unit_edge_weights();

}

void cp_graph::create_grid(int n){
    N = n*n;
    L = N;
    M = 0;
    nb.assign(N, vector<int>());
    nc.assign(N, 1);
    for(int i=0; i<n; i++){
      for(int j=0; j<n; j++){
        if(i==0 && j==0){
          nb[i*n+j].assign(2,0);
          M += 2;
          nb[i*n+j][0] = (i+1)*n+j;
          nb[i*n+j][1] = i*n+j+1;
        }else if(i==n-1 && j==n-1){
          nb[i*n+j].assign(2,0);
        M += 2;
        nb[i*n+j][0] = (i-1)*n+j;
        nb[i*n+j][1] = i*n+j-1;
      }else if(i==0 && j==n-1){
        nb[i*n+j].assign(2,0);
        M += 2;
        nb[i*n+j][0] = (i+1)*n+j;
        nb[i*n+j][1] = i*n+j-1;
      }else if(i==n-1 && j==0){
        nb[i*n+j].assign(2,0);
        M += 2;
        nb[i*n+j][0] = (i-1)*n+j;
        nb[i*n+j][1] = i*n+j+1;
      }else if(i==0){
        nb[i*n+j].assign(3,0);
        M += 3;
        nb[i*n+j][0] = (i+1)*n+j;
        nb[i*n+j][1] = i*n+j-1;
        nb[i*n+j][2] = i*n+j+1;
      }else if(j==0){
        nb[i*n+j].assign(3,0);
        M += 3;
        nb[i*n+j][0] = (i-1)*n+j;
        nb[i*n+j][1] = (i+1)*n+j;
        nb[i*n+j][2] = i*n+j+1;
      }else if(i==n-1){
        nb[i*n+j].assign(3,0);
        M += 3;
        nb[i*n+j][0] = (i-1)*n+j;
        nb[i*n+j][1] = i*n+j-1;
        nb[i*n+j][2] = i*n+j+1;
      }else if(j==n-1){
        nb[i*n+j].assign(3,0);
        M += 3;
        nb[i*n+j][0] = (i-1)*n+j;
        nb[i*n+j][1] = (i+1)*n+j;
        nb[i*n+j][2] = i*n+j-1;
      }else{
        nb[i*n+j].assign(4,0);
        M += 4;
        nb[i*n+j][0] = (i-1)*n+j;
        nb[i*n+j][1] = (i+1)*n+j;
        nb[i*n+j][2] = i*n+j-1;
        nb[i*n+j][3] = i*n+j+1;
      }
    }
  }
  M /= 2;
}

void cp_graph::create_bicycle(int n){
  N = 7*n-8;
  L = N;
  M = 0;
  nb.assign(N, vector<int>());
  nc.assign(N, 1);

  int i=0;
  nb[i].push_back(1);
  nb[i].push_back(4*n-5);
  M+=2;

  for(i=1; i<=n-2; i++){
    nb[i].push_back(i-1);
    nb[i].push_back(i+1);
    M+=2;
  }
  nb[i].push_back(i-1);
  nb[i].push_back(i+1);
  nb[i].push_back(4*n-4);
  M+=3;
  for(i=n; i<=2*n-3; i++){
    nb[i].push_back(i-1);
    nb[i].push_back(i+1);
    M+=2;
  }
  nb[i].push_back(i-1);
  nb[i].push_back(i+1);
  nb[i].push_back(7*n-9);
  M+=3;
  for(i=2*n-1; i<=4*n-6; i++){
    nb[i].push_back(i-1);
    nb[i].push_back(i+1);
    M+=2;
  }
  nb[i].push_back(i-1);
  nb[i].push_back(0);
  M+=2;
  i = 4*n-4;
  nb[i].push_back(n-1);
  nb[i].push_back(i+1);
  M+=2;
  for(i=4*n-3; i<=7*n-10; i++){
    nb[i].push_back(i-1);
    nb[i].push_back(i+1);
    M+=2;
  }
  nb[i].push_back(i-1);
  nb[i].push_back(2*n-2);
  M+=2;
  M /= 2;
  assert(M == 7*n-7);
}

void cp_graph::create_fully_connected_graph(int n)
{
  N = n;
  L = N;
  M = (N*(N-1))/2;
  nb.assign(N,vector<int>());
  nc.assign(N,1);
  for(int i=0; i<N; i++)
    for(int j=0; j<N; j++)
      if(i!=j)
        nb[i].push_back(j);
}

// find indentity of edge (+/-(index into 'p'+1)), avoids storing the NxN adjacency matrix
int cp_graph::find_edge(int id1, int id2)
{
  int index=-1;
  for(int i=0; i<nb[id1].size(); i++){
    if(nb[id1][i] == id2){
      index = i;
      break;
    }
  }
  assert(index>=0);
  return pid[id1][index];
}

// flag == 0: singletons=zeros, pairwise=U[0,1]
// flag == 1: singletons=U[0,1], pairwise=U[0,1]
void cp_graph::random_weights(int cardinality, int flag){

  K = cardinality;
  assert(status == 0); // this function is only used to create a problem, on the original graph structure
  ///assert(N == nb.size());
  s.assign(N, vector<double>());
  p.assign(M, vector<double>());
  pid.assign(N, vector<int>());
  int e=0; // counting edges
  for(int i=0; i<N; i++){
    s[i].assign(K,0);
    if(flag==1){
      for(int j=0; j<K; j++)
        s[i][j] = randu(0.0,1.0);  ////////////////// ANDREW, singleton potentials
    }
    for(int j=0; j<nb[i].size(); j++){

      if(i < nb[i][j]){
        pid[i].push_back(e+1);
        p[e].assign(K*K, 0);
        for(int k=0; k<K*K; k++){   //// ANDREW, potential for edge (i, nb[i][j])
          p[e][k] = randu(0.0,1.0); //// ANDREW, pairwise poteintials, k=s+t*K, s for variable i, t for variable nb[i][j]
        }
        e++;
      }else if(i > nb[i][j]){
        int idx = find_in_vector(i, nb[nb[i][j]]);
        int pid_t = pid[nb[i][j]][idx];
        pid[i].push_back(-pid_t);
      }else{
        printf("ERR: edge i--i\n");
      }
    }
  }
  assert(e==M);
  update_edge_nodes();
}

void cp_graph::read_weights(const char* filename, int cardinality){

  K = cardinality;
  assert(status == 0); // this function is only used to create a problem, on the original graph structure
  ///assert(N == nb.size());
  s.assign(N, vector<double>());
  p.assign(M, vector<double>());
  pid.assign(N, vector<int>());
  int e=0; // counting edges

  FILE* fp = fopen(filename, "r");
  for(int i=0; i<N; i++){
    s[i].assign(K,0);
    for(int j=0; j<K; j++)
      fscanf(fp, "%lf", &(s[i][j]));
    for(int j=0; j<nb[i].size(); j++){

      if(i < nb[i][j]){
        pid[i].push_back(e+1);
        p[e].assign(K*K, 0);
        //for(int k=0; k<K*K; k++){   //// ANDREW, potential for edge (i, nb[i][j])
	//  fscanf(fp, "%lf", &(p[e][k]));
        //}
	for(int l=0; l<K; l++){     // I also tried to switch these
	  for(int k=0; k<K; k++){   // two lines, the algorithm also runs correctly
	    fscanf(fp, "%lf ", &(p[e][l+k*K]));
	  }
	}
        e++;
      }else if(i > nb[i][j]){
        int idx = find_in_vector(i, nb[nb[i][j]]);
        int pid_t = pid[nb[i][j]][idx];
        pid[i].push_back(-pid_t);
      }else{
        printf("ERR: edge i--i\n");
      }
    }
  }
  fclose(fp);
  assert(e==M);
  update_edge_nodes();
}


/*
 * CARDINALITY is the number of hypotheses per node
 * B contains the indices of the parts in the block to optimize
 * FACTOR contains the singletons plus the loss
 * CONDENSEDPAIRWISE contains the pairwise weights between the fixed
 *    parts and the parts in the block. These weights are condensed
 *    down into a singleton-esque structure because some of the parts
 *    are fixed.
 * BLOCKPAIRS has the edge energies for the block
 * SYMANDPAIR has the symmetric and pairwise weights
 * EDGEINDICES has the indices of the edges between each pair of parts.
 *    And a negative index for non-existant edges.
 */
void cp_graph::set_weights(int cardinality, struct block *b,
    vector< vector<double> > &factor,
    vector< vector<double> > &condensedPairwise,
    double blockpairs[NPARTS][NPARTS][NHYPS][NHYPS],
    double symAndPairEnergies[E][NHYPS][NHYPS],
    vector< vector<int> > &edgeIndices){
  int FLIP = -1;
  K = cardinality;
  // This function is only used to create a problem on the original
  // graph structure.
  assert(status == 0);
  assert(condensedPairwise.size() == (unsigned int) N);
  assert(condensedPairwise[0].size() == (unsigned int) K);
  //assert(N == nb.size());
  //TODO optimize these constructors by specifying K
  s.assign(N, vector<double>());
  p.assign(M, vector<double>());
  pid.assign(N, vector<int>());
  int e=0; // counting edges
  int debug = 0;
  if(debug) printf("set weights singleton:\n");
  for(int i=0; i<N; i++){
    int blockpid = b->p[i];
    s[i].assign(K,0);
    //////////////// ANDREW, singleton potentials
    for(int j=0; j<K; j++){
      s[i][j] = FLIP*
        (factor[blockpid][j] + condensedPairwise[i][j]);
      if(debug) printf("%lf ", s[i][j]);
      //if(debug) printf("[%lf %lf] ",
      //    FLIP*factor[blockpid][j], FLIP*condensedPairwise[i][j]);
    }
    // nb[i] is the list of neighbors of
    for(unsigned int j=0; j<nb[i].size(); j++){
      if(i < nb[i][j]){
        pid[i].push_back(e+1);
        p[e].assign(K*K, 0);
        int blocknbpid = b->p[nb[i][j]];
        assert(blockpid < blocknbpid);
        int edgei = edgeIndices[blockpid][blocknbpid];
        if(edgei >= 0){
          for(int hnb=0; hnb<K; hnb++){
            for(int h=0; h<K; h++){
              // ANDREW, pairwise poteintials, k=s+t*K, s for variable i, t for variable nb[i][j]
              // divide by M (# of edges) to eliminate double counting
              p[e][h+hnb*K] = FLIP*
                blockpairs[blockpid][blocknbpid][h][hnb] / M;
              p[e][h+hnb*K] += FLIP*symAndPairEnergies[edgei][h][hnb];
            }
          }
        }else{
          edgei = edgeIndices[blocknbpid][blockpid];
          for(int hnb=0; hnb<K; hnb++){
            for(int h=0; h<K; h++){
              // ANDREW, pairwise poteintials, k=s+t*K, s for variable i, t for variable nb[i][j]
              // divide by M (# of edges) to eliminate double counting
              p[e][h+hnb*K] = FLIP*
                blockpairs[blockpid][blocknbpid][h][hnb] / M;
              if(edgei >= 0){
                // the edge is reversed so reverse h and hnb
                p[e][h+hnb*K] += FLIP*symAndPairEnergies[edgei][hnb][h];
              }
            }
          }
        }
        // ANDREW, potential for edge (i, nb[i][j])
        e++;
      }else if(i > nb[i][j]){
        int idx = find_in_vector(i, nb[nb[i][j]]);
        int pid_t = pid[nb[i][j]][idx];
        pid[i].push_back(-pid_t);
      }else{
        printf("ERR: edge i--i\n");
      }
    }
    if(debug) printf("\n");
  }
  if(debug){//print out the pairwise
    printf("pairwise: N: %d, M: %d\n", N, M);
    printf("dim1: %d, dim2: %d\n", p.size(), p[0].size());
    for(int i=0; i<e; i++){
      printf("edge %d\n", i);
      for(int j=0; j<K; j++){
        for(int k=0; k<K; k++){
          printf("%lf ", p[i][j+k*K]);
        }
        printf("\n");
      }
      printf("\n");
    }
  }
  assert(e==M);
  update_edge_nodes();
}


double cp_graph::getBestHypotheses(int *h1, int *h2){
  assert(N == 2 && M == 1);//since then we just have 1 edge
  double vals[K][K];
  // copy over pairwise vals
  for(int i=0; i<K; i++)
    for(int j=0; j<K; j++)
      vals[i][j] = p[0][i+j*K];
  // add the singeltons
  for(int i=0; i<K; i++){
    for(int j=0; j<K; j++){
      vals[i][j] += s[0][i];
      vals[i][j] += s[1][j];
    }
  }
  // find the min
  double minval = INFINITY;
  int mini = -1;
  int minj = -1;
  for(int i=0; i<K; i++){
    for(int j=0; j<K; j++){
      if(vals[i][j] < minval){
        minval = vals[i][j];
        mini = i;
        minj = j;
      }
    }
  }
  assert(mini >= 0 && minj >= 0);
  *h1 = mini;
  *h2 = minj;
  return minval;
}
double cp_graph::getEnergyForHypotheses(int *hids, struct block *b){
  for(int i=0; i<NPARTS; i++){
    if(hids[i]<=0 || hids[i]>NHYPS){
      printf("i: %d, hid: %d\n", i, hids[i]);
      printf("problem in cp_graph::getEnergyForHypotheses\n");
      exit(0);
    }
  }
  assert(K == NHYPS);
  double energy = 0.0;
  //////// Energy for parts in the block ////////////
  // add the singleton
  for(int i=0; i<N; i++){
    int blockpid = b->p[i];
    assert(blockpid>=0 && blockpid<NPARTS);
    int h = hids[blockpid] - 1;
    assert(h>=0 && h<NHYPS);
    energy += s[i][h];
  }
  // add the pairwise
  int e = 0;
  for(int i=0; i<N; i++){
    int blockpid = b->p[i];
    assert(blockpid>=0 && blockpid<NPARTS);
    int h = hids[blockpid] - 1;
    assert(h>=0 && h<NHYPS);
    // nb[i] is the list of neighbors of
    for(unsigned int j=0; j<nb[i].size(); j++){
      if(i < nb[i][j]){
        int blocknbpid = b->p[nb[i][j]];
        assert(blockpid < blocknbpid);
        // ANDREW, potential for edge (i, nb[i][j])
        int hnb = hids[blocknbpid] - 1;
        assert(hnb>=0 && hnb<NHYPS);
        energy += p[e][h+hnb*K];
        e++;
      }
    }
  }
  return energy;
}

void cp_problem::print_assignment_and_energy()
{
  assert(G.N == N);
  assert(assignment.size() == N);
  print_vector("assignment", assignment);
  printf("energy = %f\n", primal_obj);
}

int cp_problem::next_assignment(int l)
{
  if(l<0)
    return 0;
  if(current_assignment[l] < K-1){
    current_assignment[l]++;
    return 1;
  }else{
    current_assignment[l] = 0;
    return next_assignment(l-1);
  }
}

void cp_problem::exhaustive_search(){ // for debugging

  current_assignment.assign(N,0);
  do{
    print_vector("assignment", current_assignment);
    assert(get_primal_obj() + EPS >= primal_obj);
  }while(next_assignment(N-1));
  cout << "exhaustive search passed" << endl;
}

void cp_graph::convert_into_covering_tree(int root_covering_tree, int root_msg_passing){

  // traverse graph in breadth-first ordering, break circles by replicating nodes
  vector<int> visited = vector<int>(N,0);
  vector<int> edge_visited = vector<int>(p.size(),0);
  queue<int> Q; // for breadth-first travesal
  queue<int> PQ; // caches the previous node in the path from the starting node
  queue<int> EQ; // caches the edge id
  Q.push(root_covering_tree);
  PQ.push(-1);
  EQ.push(0);
  int previous_node = -1;
  while (L < M+1){ // when L=M+1, we have a tree

    assert(!Q.empty());

    // pop from queue,
    int current_node = Q.front();
    int previous_node = PQ.front();
    int edge_id = EQ.front();
    Q.pop();
    PQ.pop();
    EQ.pop();

    // skip if the edge has been visited
    if(edge_id!=0){
      if(edge_visited[abs(edge_id)-1])
        continue;
      else
        edge_visited[abs(edge_id)-1] = 1;
    }

    // check if there is circle in current path
    if(visited[current_node]){ // there is a circle, we need to break it

      // the current path be .....-ai-aj (ai = previous_node, aj = current_node), where aj as been visited before
      // make a copy of aj, let it be aj' (aj' = aj + N)
      int current_var = current_node%N;
      nc[current_var]++;
      int current_node_old = current_node;
      current_node = current_var + N*(nc[current_var]-1);

      // remove ai from the neighbors of aj, pid needs to be updated at the same time
      int id = find_in_vector(previous_node, nb[current_node_old]);
      nb[current_node_old].erase(nb[current_node_old].begin()+id);
      pid[current_node_old].erase(pid[current_node_old].begin()+id);

      // assign ai as a neighbor of aj',
      if(current_node >= nb.size()){
        nb.resize(current_node+1, vector<int>());
        pid.resize(current_node+1, vector<int>());
      }
      nb[current_node] = vector<int>(1,previous_node);
      pid[current_node] = vector<int>(1,-edge_id);

      // also need to change aj into aj' in ai's neighbors
      id = find_in_vector(current_node_old, nb[previous_node]);
      nb[previous_node][id] = current_node;
      assert(pid[previous_node][id] == edge_id);

      L++; // total number of nodes

    }else{ // any newly created node does not have any neighbor other than previous_node
      // push all neighbors of current_node into queue (excluding previous_node)
      // and also cache current_node in PQ (will pop out as "previous_node" later on)
      for(int i=0; i<nb[current_node].size(); i++){
        if (nb[current_node][i] == previous_node) continue;
        Q.push(nb[current_node][i]);
        PQ.push(current_node);
        EQ.push(pid[current_node][i]);
      }
    }
    if(current_node >= visited.size()){
      visited.resize(current_node+1, 0);
    }
    visited[current_node] = 1;
  }

  // update s (singleton potentials)
  s.resize(nb.size(), vector<double>());
  for(int i=0; i<N; i++){ // for each variable, split singleton among all copies (nodes)
    if (nc[i]>1){
      multiply_s_v(1.0/nc[i], s[i]);
      for(int j=1; j<nc[i]; j++){
        s[i+j*N] = s[i];
      }
    }
  }
  status = 1;
  update_edge_nodes();
  msg_passing_ordering_for_covering_tree(root_msg_passing);
}

void cp_graph::msg_passing_ordering_for_covering_tree(int root)
{
  assert(status == 1); // only works for covering tree
  stack<int> S;
  stack<int> PS;
  stack<int> ES;
  upstream.assign(nb.size(), -1);
  S.push(root);
  PS.push(-1);
  ES.push(0);
  while(!S.empty()){ // depth first traversal, record the edge along the way, the resulted
                     // msg_from/to/via should be visited in reverse order to get upstream pass

    int current_node = S.top();
    int previous_node = PS.top();
    int edge_id = ES.top();
    S.pop();
    PS.pop();
    ES.pop();
    if(previous_node>=0){
      //assert(current_node >=0 && current_node < L);
      upstream[current_node] = previous_node;
      msg_from.push_back(current_node);
      msg_to.push_back(previous_node);
      msg_via.push_back(-edge_id);
    }

    // add all neighbors of current_node to stack, excluding previous_node
    for(int i=0; i<nb[current_node].size(); i++){
      if(nb[current_node][i] == previous_node) continue;
      S.push(nb[current_node][i]);
      PS.push(current_node);
      ES.push(pid[current_node][i]);
    }
  }
  //print_vector("upstream", upstream);
}

// two passes, get MAP assigment and minmarginals
double cp_graph::min_sum_bp_covering_tree_get_minmarginals()
{
  //assert(status==1); // only works for covering tree

  vector< vector<double> > msg; // cache the msgs for down stream pass
  node_mmarginal.assign(s.size(), vector<double>());
  edge_mmarginal.assign(p.size(), vector<double>());

  for(int i=0; i<s.size(); i++)
    node_mmarginal[i] = s[i];

  vector< vector<int> > back_tracer; // keep track of the argmin in each msg passing step
  back_tracer.assign(msg_from.size(), vector<int>());
  msg.assign(msg_from.size(), vector<double>());

  // pass upstream min-sum messages
  for(int i=msg_from.size()-1; i>=0; i--){
    int f = msg_from[i];
    int t = msg_to[i];
    int e = msg_via[i];

    back_tracer[i].assign(K, -1);

    // add mmarginal[f] to p[e], and eliminate f  // OPT avoid repeated computation
    vector<double> M = p[abs(e)-1];
    if(e>0){

      add_i2ij(node_mmarginal[f], M, 1.0);
      reduce_i(K, M, back_tracer[i]);

    }else{
      add_j2ij(node_mmarginal[f], M, 1.0);
      reduce_j(K, M, back_tracer[i]);
    }
    msg[i] = M;
    // add to mmarginal[t]
    add_i2i(M, node_mmarginal[t]);
  }

  assignment.assign(nb.size(), -1);
  double min_energy = minimize(node_mmarginal[msg_to[0]], assignment[msg_to[0]]);
  // down stream back tracing to get assignment
  for(int i=0; i<msg_from.size(); i++){
    //assert(assignment[msg_to[i]] >= 0);
    assignment[msg_from[i]] = back_tracer[i][assignment[msg_to[i]]];
  }

  for(int i=0; i<msg_from.size(); i++){
    int f = msg_to[i];
    int t = msg_from[i];
    int e = msg_via[i];
    vector<double> M = p[abs(e)-1];
    vector<double> tmp = node_mmarginal[f];
    add_vector(tmp, msg[i], -1.0);

    if(e<0){
      add_i2ij(tmp, M, 1.0);
      edge_mmarginal[abs(e)-1] = M;
      add_j2ij(node_mmarginal[t], edge_mmarginal[abs(e)-1], 1.0);
      reduce_i(K, M, back_tracer[i]);
    }else{
      add_j2ij(tmp, M, 1.0);
      edge_mmarginal[abs(e)-1] = M;
      add_i2ij(node_mmarginal[t], edge_mmarginal[abs(e)-1], 1.0);
      reduce_j(K, M, back_tracer[i]);
    }
    add_i2i(M, node_mmarginal[t]);
  }

  //////////////////////////////////////////////////////////////////////////////
  // comment out correctness check
  /*
  for(int i=0; i< s.size(); i++){
    if(nb[i].size() > 0){
      assert(assignment[i] >= 0);
      assert(fabs(min_energy - node_mmarginal[i][assignment[i]]) < EPS);
      for(int j=0; j<K; j++)
        assert(min_energy - node_mmarginal[i][j] <= EPS);
    }
  }
  for(int i=0; i < M; i++){
    int l = assignment[edge_nodes[i][0]] + assignment[edge_nodes[i][1]]*K;
    assert(fabs(min_energy - edge_mmarginal[i][l]) < EPS);
    for(int j=0; j<K*K; j++)
        assert(min_energy - edge_mmarginal[i][j] <= EPS);

    vector<double> tmp = reduce_i_const(K, edge_mmarginal[i]);
    for(int j=0; j<K; j++)
      assert(fabs(tmp[j] - node_mmarginal[edge_nodes[i][1]][j]) < EPS);
    tmp = reduce_j_const(K, edge_mmarginal[i]);
    for(int j=0; j<K; j++)
      assert(fabs(tmp[j] - node_mmarginal[edge_nodes[i][0]][j]) < EPS);
  }
  */
  ///////////////////////////////////////////////////////////////////////////////

  obj = min_energy;
  return min_energy;
}

// we only perform one upstream pass, as we only need the MAP assignment but not min-marginals
double cp_graph::min_sum_bp_covering_tree()
{
  //assert(status==1); // only works for covering tree

  vector< vector<double> > b; // beliefs, initialize by singleton potentials
  b.assign(s.size(), vector<double>());
  for(int i=0; i<s.size(); i++)
    b[i] = s[i];

  vector< vector<int> > back_tracer; // keep track of the argmin in each msg passing step
  back_tracer.assign(msg_from.size(), vector<int>());

  // pass upstream min-sum messages
  for(int i=msg_from.size()-1; i>=0; i--){
    int f = msg_from[i];
    int t = msg_to[i];
    int e = msg_via[i];

    back_tracer[i].assign(K, -1);

    // add b[f] to p[e], and eliminate f
    vector<double> M = p[abs(e)-1];
    if(e>0){
      add_i2ij(b[f], M, 1.0);
      reduce_i(K, M, back_tracer[i]);
    }else{
      add_j2ij(b[f], M, 1.0);
      reduce_j(K, M, back_tracer[i]);
    }
    // add to b[t]
    add_i2i(M, b[t]);
  }

  assignment.assign(nb.size(), -1);
  double min_energy = minimize(b[msg_to[0]], assignment[msg_to[0]]);
  // down stream back tracing to get assignment
  for(int i=0; i<msg_from.size(); i++){
    //assert(assignment[msg_to[i]] >= 0);
    assignment[msg_from[i]] = back_tracer[i][assignment[msg_to[i]]];
  }

  obj = min_energy;
  return min_energy;
}

double cp_graph::get_obj(){

  double obj = 0.0;
  for(int i=0; i<nb.size(); i++){
    if(nb[i].size()>0){
      obj += s[i][assignment[i]];
      for(int j=0; j<nb[i].size(); j++){
        if(pid[i][j] > 0){ // visit each edge only once
          obj += p[pid[i][j]-1][assignment[i] + assignment[nb[i][j]]*K];
        }
      }
    }
  }
  return obj;
}

double cp_cycle::get_obj(){

  double obj = constant;
  for(int i=0; i<N; i++)
    obj += p[i][assignment[i]+assignment[i+1]*K];
  return obj;
}

void cp_graph::show_connectivity(){

  printf("\n--------------------\n");
  printf("N=%d, M=%d, L=%d\n", N, M, L);
  printf("--------------------\n");
  for(int i=0; i<N; i++)
    printf("%d ", nc[i]);
  printf("\n");
  for(int i=0; i<nb.size(); i++){
    if(nb[i].size()>0){
      printf("node %d: ",i);
      for(int j=0; j<nb[i].size(); j++){
        printf("%d(%d) ", nb[i][j], pid[i][j]);
      }
      printf("\n");
    }
  }
  printf("--------------------\n");
}

void cp_graph::show_msg_pass_order(){
  printf("\n--------------------\n");
  for(int i=msg_from.size()-1; i>=0; i--){
    printf("from %d to %d via %d\n", msg_from[i], msg_to[i], msg_via[i]);
  }
  printf("--------------------\n");
}

cp_graph::~cp_graph()
{
}


int find_in_vector(int a, const vector<int> & v)  // find the position of a in v
{
  for(int i=0; i<v.size(); i++)
    if(v[i] == a)
      return i;
  return -1;
}

void multiply_s_v(double s, vector<double> & v) // multiply scalar to vector
{
  for(int i=0; i<v.size(); i++)
    v[i] *= s;
}

vector<double> multiply_s_v_const(double s, const vector<double> & v) // multiply scalar to vector
{
  vector<double> result = v;
  for(int i=0; i<v.size(); i++)
    result[i] *= s;
  return result;
}

void add_i2ij(const vector<double> & v, vector<double> & M, double factor) // add factor (i) to factor (i,j)
{
  long idx=0;
  int K = v.size();
  for(int j=0; j<K; j++)
    for(int i=0; i<K; i++)
      M[idx++] += v[i]*factor;
}

void add_j2ij(const vector<double> & v, vector<double> & M, double factor) // add factor (j) to factor (i,j)
{
  long idx=0;
  int K = v.size();
  for(int j=0; j<K; j++)
    for(int i=0; i<K; i++)
      M[idx++] += v[j]*factor;
}

void add_i2i(const vector<double> & v1, vector<double> & v2) // add factor (i) to factor (i)
{
  assert(v1.size() == v2.size());
  for(long i=0; i<v1.size(); i++)
    v2[i] += v1[i];
}

void reduce_j(int K, vector<double> & M, vector<int> & argmin) // reduce i from factor (i,j), keep argmin
{
  long idx;
  for(int i=0; i<K; i++){
    double minval = M[i];
    idx = i + K;
    argmin[i] = 0;
    for(int j=1; j<K; j++){
      if(M[idx] < minval){
        minval = M[idx];
        argmin[i] = j;
      }
      idx += K;
    }
    M[i] = minval;
  }
  M.resize(K);
}

vector<double> reduce_j_const(int K, const vector<double> & M)
{
  vector<double> result(K,0);
  long idx;
  for(int i=0; i<K; i++){
    double minval = M[i];
    idx = i + K;
    for(int j=1; j<K; j++){
      if(M[idx] < minval){
        minval = M[idx];
      }
      idx += K;
    }
   result[i] = minval;
  }
  return result;
}

void reduce_j(int K, vector<double> & M) // reduce i from factor (i,j)
{
  long idx;
  for(int i=0; i<K; i++){
    double minval = M[i];
    idx = i + K;
    for(int j=1; j<K; j++){
      if(M[idx] < minval){
        minval = M[idx];
      }
      idx += K;
    }
    M[i] = minval;
  }
  M.resize(K);
}

vector<double> reduce_i_const(int K, const vector<double> & M)
{
  vector<double> result(K,0);
  long idx=0;
  for(int j=0; j<K; j++){
    double minval = M[idx++];
    //argmin[j] = 0;
    for(int i=1; i<K; i++){
      if(M[idx++] < minval){
        minval = M[idx-1];
        //argmin[j] = i;
      }
    }
    result[j] = minval;
  }
  return result;
}

void reduce_i_const(int K, const vector<double> & M, vector<double> & v)
{
  long idx=0;
  for(int j=0; j<K; j++){
    double minval = M[idx++];
    for(int i=1; i<K; i++){
      if(M[idx++] < minval){
        minval = M[idx-1];
      }
    }
    //result[j] = minval;
    v[j] = MIN(v[j], minval);
  }
  //  return result;
}

void reduce_i(int K, vector<double> & M, vector<int> & argmin) // reduce j from factor (i,j), keep argmin
{
  long idx=0;
  for(int j=0; j<K; j++){
    double minval = M[idx++];
    argmin[j] = 0;
    for(int i=1; i<K; i++){
      if(M[idx++] < minval){
        minval = M[idx-1];
        argmin[j] = i;
      }
    }
    M[j] = minval;
  }
  M.resize(K);
}

void reduce_i(int K, vector<double> & M) // reduce j from factor (i,j),
{
  long idx=0;
  for(int j=0; j<K; j++){
    double minval = M[idx++];
    for(int i=1; i<K; i++){
      if(M[idx++] < minval){
        minval = M[idx-1];
      }
    }
    M[j] = minval;
  }
  M.resize(K);
}

double minimize(const vector<double> & v, int & assignment) // minimization
{
  double minval = v[0];
  assignment = 0;
  for(int i=1; i<v.size(); i++){
    if(v[i] < minval){
      minval = v[i];
      assignment = i;
    }
  }
  return minval;
}

double maximize(const vector<double> & v, int & assignment) // minimization
{
  double maxval = v[0];
  assignment = 0;
  for(int i=1; i<v.size(); i++){
    if(v[i] > maxval){
      maxval = v[i];
      assignment = i;
    }
  }
  return maxval;
}



double minimize(const vector<double> & v)
{
  double minval = v[0];
  for(int i=1; i<v.size(); i++)
    minval = MIN(minval, v[i]);
  return minval;
}

double maximize(const vector<double> & v)
{
  double maxval = v[0];
  for(int i=1; i<v.size(); i++)
    maxval = MAX(maxval, v[i]);
  return maxval;
}

// tar += s*v
void add_vector(vector<double> & tar, const vector<double> & v, double s){
  assert(tar.size() == v.size());
  for(int i=0; i<tar.size(); i++)
    tar[i] += s*v[i];
}

void add_vector(vector<double> & tar, const vector<double> & v, double s, double t){
  assert(tar.size() == v.size());
  for(int i=0; i<tar.size(); i++)
    tar[i] += s*v[i]+t;
}

// tar += s*M (tar,s are KxK matrices)
void add_matrix(int K, vector<double> & tar, const vector<double> & M, double s){
  assert(tar.size() == M.size());
  assert(tar.size() == K*K);
  for(int i=0; i<tar.size(); i++)
    tar[i] += s*M[i];
}

// tar += s*M' (tar,s are KxK mtarices)
void add_matrixT(int K, vector<double> & tar, const vector<double> & M, double s){
  assert(tar.size() == M.size());
  assert(tar.size() == K*K);
  for(int k=0; k<K; k++)
    for(int l=0; l<K; l++)
      tar[k+l*K] += s*M[l+k*K]; // OPT: faster index operation
}

// L_p form of vector, p=[1, INF]
double vector_lp_norm(const vector<double> & v, double p){

  if(p == INF){
    double maxel = 0;
    for(int i=0; i<v.size(); i++)
      maxel = MAX(maxel, fabs(v[i]));
    return maxel;
  }else{
    double norm = 0;
    for(int i=0; i<v.size(); i++)
      norm += pow(fabs(v[i]), p);
    return pow(norm, 1.0/p);
  }
}

int same_cycle(const cp_cycle & c1, const cp_cycle & c2)
{
  int N = c1.nodes.size()-1;
  if (N!=c2.nodes.size()-1)
    return 0;
  int i;
  for(i=0; i<N; i++)
    if(c2.nodes[i] == c1.nodes[0])
      break;
  if(i == N)
    return 0;
  for(int j=0; j<N; j++)
    if(c1.nodes[j] != c2.nodes[(i+j)%(N)])
      return 0;
  return 1;
}

// return INF if no path exists
double cp_graph::shortest_weighted_path_ignoring_direct_edge(int a, int b, vector<int> & P)
{
  // perform A-star search (with zero heuristic function)

  struct fibheap *data;
  data = fh_makekeyheap();
  vector< void* > handle(N, NULL);

  vector<int> visited(N,0);
  vector<int> previous(N,-1);
  P.clear();
  int heap_cnt = 0;

  visited[a] = 1;
  for(int i=0; i<nb[a].size(); i++){
    if(nb[a][i] != b){
      handle[nb[a][i]] = fh_insertkey(data, edge_weight[abs(pid[a][i])-1], (void*)((long)nb[a][i]));
      heap_cnt++;
      previous[nb[a][i]] = a;
      visited[nb[a][i]] = 1;
    }
  }
  double min_len = INF;
  while(heap_cnt>0){
    double current_len = fh_minkey(data);
    int current_node = (int)((long)fh_extractmin(data));
    for(int i=0; i<nb[current_node].size(); i++){
      if(!visited[nb[current_node][i]]){
        if(nb[current_node][i] == b){ // we are done
          min_len = current_len + edge_weight[abs(pid[current_node][i])-1];
          P.push_back(b);
          P.push_back(current_node);
          do{
            current_node = previous[current_node];
            assert(current_node >= 0);
            P.push_back(current_node);
          }while(current_node!=a);
          return min_len;
        }else{
          handle[nb[current_node][i]] =
            fh_insertkey(data,
                         current_len + edge_weight[abs(pid[current_node][i])-1],
                         (void*)((long)nb[current_node][i]));
          visited[nb[current_node][i]] = 1;
          previous[nb[current_node][i]] = current_node;
        }
      }
    }
  }
  fh_deleteheap(data);
  return min_len;
}

// flag = 0: sample from U(0,1), this yields a very skewed distribution prefering small cycles
// flag = 1: sample from abs(N(0,1)), this may yield slightly 'more uniform' distribution over the cycles
void cp_graph::random_edge_weights(int flag)
{
  assert(edge_weight.size() == M);
  if(flag == 0){
    for(int i=0; i<M; i++)
      edge_weight[i] = randu(0.0, 1.0);
  }else if(flag == 1){
    for(int i=0; i<M; i++)
      edge_weight[i] = fabs(randn(0.0, 1.0));
  }else{
    cout << "unspecified cycle_distribution" << endl; exit(0);
  }
}

void cp_graph::unit_edge_weights()
{
  edge_weight.assign(M,1.0);
}

// randomly shuffle [0, 1, 2, ..., n-1], for going thru a list in random order
vector<int> random_iterator(int n)
{
  vector<double> w(n,0);
  vector<int> iterator(n,0);
  for(int i=0; i<n; i++){
    iterator[i] = i;
    w[i] = randu(0.0, 1.0);
  }

  //print_vector("old    w", w);
  //print_vector("iterator", iterator);

  my_quick_sort(w, iterator, 0, n-1);

  //print_vector("sorted w", w);
  //print_vector("iterator", iterator);

  return iterator;
}

// quick sort w from index 's' to 'e', re-order a accordingly
void my_quick_sort(vector<double> & w, vector<int> & a, int s, int e)
{
  if (s==e)
    return;
  double pivotValue = w[e];
  int storeIndex = s;
  double tmpd;
  int tmpi;
  for(int i=s; i<e; i++){
    if(w[i] < pivotValue){
      // swap i and storeIndex
      tmpd = w[i];
      w[i] = w[storeIndex];
      w[storeIndex] = tmpd;
      tmpi = a[i];
      a[i] = a[storeIndex];
      a[storeIndex] = tmpi;
      storeIndex++;
    }
  }
  // swap storeIndex and e
  tmpd = w[e];
  w[e] = w[storeIndex];
  w[storeIndex] = tmpd;
  tmpi = a[e];
  a[e] = a[storeIndex];
  a[storeIndex] = tmpi;

  if(storeIndex-1 > s)
    my_quick_sort(w, a, s, storeIndex-1);
  if(storeIndex+1 < e)
    my_quick_sort(w, a, storeIndex+1, e);
}

void cp_graph::update_edge_nodes()
{
  edge_nodes.assign(M, vector<int>(2,0));
  for(int i=0; i<nb.size(); i++){
    for(int j=0; j<nb[i].size(); j++){
      if(pid[i][j] > 0){ // only visit each edge once;
        // edge betwee node 'i' and 'nb[i][j]'
        int edge_id = abs(pid[i][j])-1;
        edge_nodes[edge_id][0] = i;
        edge_nodes[edge_id][1] = nb[i][j];
      }
    }
  }
}

cp_cycle reverse_cycle(const cp_cycle & c)
{
  vector<int> n;
  for(int i=c.nodes.size()-1; i>=0; i--)
    n.push_back(c.nodes[i]);
  return cp_cycle(n);
}

double randu(double lowerbound, double upperbound){
  return lowerbound + (upperbound-lowerbound)*((rand()%1000000)/1000000.0);
}

double randn(double mu, double sigma) {
  static bool deviateAvailable=false;        //        flag
  static float storedDeviate;                        //        deviate from previous calculation
  double polar, rsquared, var1, var2;
  //        If no deviate has been stored, the polar Box-Muller transformation is
  //        performed, producing two independent normally-distributed random
  //        deviates.  One is stored for the next round, and one is returned.
  if (!deviateAvailable) {
    //        choose pairs of uniformly distributed deviates, discarding those
    //        that don't fall within the unit circle
    do {
      var1=2.0*( double(rand())/double(RAND_MAX) ) - 1.0;
      var2=2.0*( double(rand())/double(RAND_MAX) ) - 1.0;
      rsquared=var1*var1+var2*var2;
    } while ( rsquared>=1.0 || rsquared == 0.0);
    //        calculate polar tranformation for each deviate
    polar=sqrt(-2.0*log(rsquared)/rsquared);
    //        store first deviate and set flag
    storedDeviate=var1*polar;
    deviateAvailable=true;
    //        return second deviate
    return var2*polar*sigma + mu;
  }
  //        If a deviate is available from a previous call to this function, it is
  //        returned, and the flag is set to false.
  else {
    deviateAvailable=false;
    return storedDeviate*sigma + mu;
  }
}



// factor(i,j) --> factor(i=l, j)
vector<double> project_i(int K, vector<double> & M, int l)
{
  vector<double> f(K,0);
  for(int i=0; i<K; i++)
    f[i] = M[l+i*K];
  return f;
}

// factor(i,j) --> factor(i, j=l)
vector<double> project_j(int K, vector<double> & M, int l)
{
  vector<double> f(K,0);
  for(int i=0; i<K; i++)
    f[i] = M[i+l*K];
  return f;
}

void get_squares_for_grid(vector<cp_cycle> & CY, int n)
{
  for(int i=0; i<n-1; i++){
    for(int j=0; j<n-1; j++){
      vector<int> p;
      p.push_back(i+j*n);
      p.push_back(i+(j+1)*n);
      p.push_back(i+1+(j+1)*n);
      p.push_back(i+1+j*n);
      p.push_back(i+j*n);
      cp_cycle c = cp_cycle(p);
      CY.push_back(c);
    }
  }
}

void get_rectangles_for_grid(vector<cp_cycle> & CY, int n)
{
  for(int i=0; i<n-1; i++){
    for(int j=0; j<n-2; j++){
      vector<int> p;
      p.push_back(i+j*n);
      p.push_back(i+(j+1)*n);
      p.push_back(i+(j+2)*n);
      p.push_back(i+1+(j+2)*n);
      p.push_back(i+1+(j+1)*n);
      p.push_back(i+1+j*n);
      p.push_back(i+j*n);
      cp_cycle c = cp_cycle(p);
      CY.push_back(c);
    }
  }
  for(int i=0; i<n-2; i++){
    for(int j=0; j<n-1; j++){
      vector<int> p;
      p.push_back(i+j*n);
      p.push_back(i+1+j*n);
      p.push_back(i+2+j*n);
      p.push_back(i+2+(j+1)*n);
      p.push_back(i+1+(j+1)*n);
      p.push_back(i+(j+1)*n);
      p.push_back(i+j*n);
      cp_cycle c = cp_cycle(p);
      CY.push_back(c);
    }
  }
}

void get_triangles_for_fully_connected(vector<cp_cycle> & CY, int N)
{
  for(int i=0; i<N; i++){
    for(int j=0; j<i; j++){
      for(int k=0; k<j; k++){
        vector<int> p;
        p.push_back(i);
        p.push_back(j);
        p.push_back(k);
        p.push_back(i);
        cp_cycle c = cp_cycle(p);
        CY.push_back(c);
      }
    }
  }
}

void get_quads_for_fully_connected(vector<cp_cycle> & CY, int N)
{
  for(int i=0; i<N; i++){
    for(int j=0; j<i; j++){
      for(int k=0; k<j; k++){
        for(int l=0; l<k; l++){
          vector<int> p;
          p.push_back(i);
          p.push_back(j);
          p.push_back(k);
          p.push_back(l);
          p.push_back(i);
          cp_cycle c0 = cp_cycle(p);
          CY.push_back(c0);

          p.clear();

          p.push_back(i);
          p.push_back(k);
          p.push_back(j);
          p.push_back(l);
          p.push_back(i);
          cp_cycle c1 = cp_cycle(p);
          CY.push_back(c1);

          p.clear();

          p.push_back(i);
          p.push_back(j);
          p.push_back(l);
          p.push_back(k);
          p.push_back(i);
          cp_cycle c2 = cp_cycle(p);
          CY.push_back(c2);

        }
      }
    }
  }
}

void minus_min(vector<double> & X)
{
  double minval = INF;
  for(int i=0; i<X.size(); i++)
    minval = MIN(minval, X[i]);
  for(int i=0; i<X.size(); i++)
    X[i] -= minval;
}

vector<double> matrix_transpose(int K, const vector<double> & M)
{
  vector<double> Mt(K*K,0);
  for(int i=0; i<K; i++)
    for(int j=0; j<K; j++)
      Mt[i+j*K] = M[j+i*K];
  return Mt;
}

double vector_sum(const vector<double> & v)
{
  double result=0;
  for(int i=0; i<v.size(); i++)
    result += v[i];
  return result;
}

int vector_sum(const vector<int> & v)
{
  int result=0;
  for(int i=0; i<v.size(); i++)
    result += v[i];
  return result;
}

int vector_cnt_pos(const vector<int> & v)
{
  int result=0;
  for(int i=0; i<v.size(); i++)
    result += (int)(v[i]>0);
  return result;
}


void add_s_v(double s, vector<double> & v)
{
  for(int i=0; i<v.size(); i++)
    v[i] += s;
}

vector<double> add_s_v_const(double s, const vector<double> & v)
{
  vector<double> result(v.size(),0);
  for(int i=0; i<v.size(); i++)
    result[i] = v[i] + s;
  return result;
}

vector<double> matrix_row(int K, const vector<double> & M, int i)
{
  vector<double> v(K,0);
  for(int k=0; k<K; k++)
    v[k] = M[i+k*K];
  return v;
}

vector<double> matrix_column(int K, const vector<double> & M, int i)
{
  vector<double> v(K,0);
  for(int k=0; k<K; k++)
    v[k] = M[k+i*K];
  return v;
}

vector<double> vector_diff(const vector<double> & v1, const vector<double> & v2)
{
  assert(v1.size()==v2.size());
  vector<double> v(v1.size(),0);
  for(int k=0; k<v1.size(); k++)
    v[k] = v1[k]-v2[k];
  return v;
}

int matrix_is_a_plus_b(int K, const vector<double> & M)
{
  for(int i=0; i<K; i++)
    for(int j=0; j<i; j++)
      if(!is_const_vector(vector_diff(matrix_row(K,M,i),matrix_row(K,M,j))))
        return 0;
  for(int i=0; i<K; i++)
    for(int j=0; j<i; j++)
      if(!is_const_vector(vector_diff(matrix_column(K,M,i),matrix_column(K,M,j))))
        return 0;
  return 1;
}

int is_const_vector(const vector<double> & v)
{
  for(int i=0; i<v.size()-1; i++)
    if(fabs(v[i] - v[i+1])>EPS)
      return 0;
  return 1;
}

void cp_cycle::init_heaps()
{

  assert(maintain_heaps <= 1);
  ready.assign(N,0);
  assignment.assign(N+1,-1);
  obj = INF;
  constant = 0;
  update_min_factor();
  init_heaps_called = 1;

  edge_mmarginal.assign(N,vector<double>());
  node_mmarginal.assign(N,vector<double>());

  // factor_updated.assign(N,vector<int>(N,0));

  int L = N-2;
  if(maintain_heaps){
    edge_mmarginal.assign(N, vector<double>(K*K,INF));
    cy_P.assign(N, vector<doublepool>(L, doublepool()));
    cy_argM.assign(N, vector< vector<int> >(L, vector<int>(K*K,(int)INF)));
    cy_ub.assign(N, INF);
    cy_rlb.assign(N, vector<double>());
    cy_min_incoming_msg.assign(N, vector<double>(L,INF));
    cy_last_factor_id.assign(N,-1);
    cy_final_assignment.assign(N,0);
    cy_rlb_needs_update.assign(N,0);
    cy_ub_needs_update.assign(N,0);
    cy_reset_msg_from.assign(N,N);
    for(int d=0; d<N; d++){
      int cnt=0;
      cy_P[d][cnt++] = construct_doublepool(d, 0, min_factor);
      for(int i=3; i<=N-1; i++)
        cy_P[d][cnt++] = construct_doublepool(d, i, min_factor);
      cy_rlb[d] = lower_bounds(d);
      cy_min_incoming_msg[d][0] = min_factor[cy_P[d][0].f[0]]; // MIN(min_factor[cy_P[d][0].f[0]], min_factor[cy_P[d][0].f[1]]);
      cy_last_factor_id[d] = (N-1+d)%N;
    }
  }
}

// opt == 0: forward pass, without updating cy_msg[]
// opt == 1: forward pass, update cy_msg[0]
// opt == 2: backward pass, update cy_msg[1]
double cp_cycle::incomplete_bp_warmstart(int d, double & time)
{
  assert(maintain_heaps);
  if(allzero){
    node_mmarginal[d].assign(K,0);
    edge_mmarginal[d].assign(K*K,0);
    obj = 0;
    ready[d] = 1;
    return 0.0;
  }
  ///////////////////////////////////
  double moveon_threshold = MOVEON_THRESHOLD*K*K*K; // TODO try 1?
  long moveon = MAX(1, (long)moveon_threshold);
  ///////////////////////////////////

  int L = N-2;
  int e0,e1;
  bool done;
  clock_t t;
  int fid,assign0,assign1;
  long eid,tb;
  double sum;
  long updated;

  //////////////////////////////////////////////////////

  //if(cy_rlb_needs_update[d]){

  cy_rlb[d] = lower_bounds(d); // TODO further optimization?

    //  cy_rlb_needs_update[d] = 0;
    //}

  /*
  int from = cy_reset_msg_from[d];
  //reset_msg(d, cy_reset_msg_from[d]);
  cy_reset_msg_from[d] = N;

  if(from < 0 || from > L-1)
    from = 0;
  */


  //////////////////////////////////////////////////////////////////////
  // reset heap
  //if(factor_updated[d][cy_P[d][0].f[0]] > 0){
    for(int l=0; l<K*K; l++){
      if(cy_P[d][0].inpool[0][l] > K*K){// marked for decrease_key
        cy_P[d][0].decrease_key(0,l,p[cy_P[d][0].f[0]][l]);
        cy_P[d][0].inpool[0][l] = 1;
      }else if(cy_P[d][0].inpool[0][l] < -K*K){ // marked for add
        cy_P[d][0].add(0,l,p[cy_P[d][0].f[0]][l]);
      }
    }
    //}
  for(int i=0; i<L; i++){
    //if(factor_updated[d][cy_P[d][i].f[1]] > 0){
      for(int l=0; l<K*K; l++){
        if(cy_P[d][i].inpool[1][l] > K*K){// marked for decrease_key
          cy_P[d][i].decrease_key_init_factor(1,l,p[cy_P[d][i].f[1]][l]);
          cy_P[d][i].inpool[1][l] = 1;
        }else if(cy_P[d][i].inpool[1][l] < -K*K){ // marked for add
          cy_P[d][i].add_init_factor(1,l,p[cy_P[d][i].f[1]][l]);
        }
      }
      //}
  }
  //////////////////////////////////////////////////////////////////////



  /////////////////////////////
  cy_ub[d] = INF;
  edge_mmarginal[d].assign(K*K,INF);
  cy_final_assignment[d] = -1;
  for(int i=0; i<L; i++){
    //if(vector_sum(cy_P[d][i].inpool[1]) < K*K){
      for(int l=0; l<K*K; l++){
	if(cy_P[d][i].inpool[0][l] <= 0){
	  double val = cy_P[d][i].f[0]>=0?p[cy_P[d][i].f[0]][l]:cy_P[d][i].msg[l];
	  if(IsNotInf(val))
	    cy_P[d][i].add(0,l, val);
	}
      }
      //}
  }
  ///////////////////////////


  //double sum_min_init_factor = vector_sum(min_factor);


  //printf("p"); fflush(stdout);

  update_min_factor(); // TODO do this dynamically in update_heaps()

  cy_min_incoming_msg[d][0] = min_factor[cy_P[d][0].f[0]]; // MIN(min_factor[cy_P[d][0].f[0]], min_factor[cy_P[d][0].f[1]]);

  int last_factor_id = cy_last_factor_id[d];
  //cy_final_assignment[d] = -1;

#if WRITE_HEAP
  write_heap_state(d, "heap_state_before.txt");
#endif

  t = clock();
  do{
    done = true;

    // investigate heap states

    //for (int ii=from; ii<from + L; ii++){ /* forward pass starting from where msg had been reset */
    //int i = ii%L;
    for(int i=0; i<L; i++){

      updated = 0;

      double min_key = cy_P[d][i].min_key(cy_min_incoming_msg[d][i], min_factor[cy_P[d][i].f[1]]);
      while(IsNotInf(min_key) &&  min_key <= cy_ub[d]-cy_rlb[d][i] && updated < moveon){

        /*
        for(int j=0; j<L; j++){
          printf("(%ld, " , vector_sum(cy_P[j].onshelf[0]));
          printf("%ld)/%ld | " , vector_sum(cy_P[j].onshelf[1]), K*K);
        }
        printf("\n");
        */

        done = false;
        cy_P[d][i].extract_min(fid, eid, cy_min_incoming_msg[d][i], min_factor[cy_P[d][i].f[1]]);

        //double minval = cy_P[d][i].fset.factors[fid].phi[eid];
        double minval = cy_P[d][i].f[fid]>=0?(p[cy_P[d][i].f[fid]][eid] /*- min_factor[cy_P[d][i].f[fid]]*/):cy_P[d][i].msg[eid];

        //printf("entry off heap %d:%d:%d:%f, updated=%d\n", i, fid, eid, minval, updated);

        assign0 = (int)floor((double)eid)%K;
        assign1 = (int)floor((double)eid/K)%K;
        if(fid==0){
          tb = assign1;
          e0 = assign1;
          e1 = assign0;
        }else{
          e0 = assign0*K;
          e1 = assign1*K;
          tb = assign0;
        }
        for(int idx=0; idx < K; idx++){

          if(cy_P[d][i].inpool[1-fid][e0] <= 0){

            double val = cy_P[d][i].f[1-fid]>=0?(p[cy_P[d][i].f[1-fid]][e0] /*- min_factor[cy_P[d][i].f[1-fid]]*/):cy_P[d][i].msg[e0];

            sum = minval + val;  //cy_P[d][i].fset.factors[1-fid].phi[e0];

            if(i < L-1){
              if ( sum < cy_P[d][i+1].msg[e1] && sum < cy_ub[d] - cy_rlb[d][i]){ // THIS DOES NOT PRESERVE A<=B<=A'
                updated++;
                cy_argM[d][i][e1] = tb; // keep argmin for tracing back

                cy_min_incoming_msg[d][i+1] = MIN(sum, cy_min_incoming_msg[d][i+1]);

                if( cy_P[d][i+1].inpool[0][e1] <= 0){
                  cy_P[d][i+1].add(0, e1, sum);
                }else{
                  cy_P[d][i+1].decrease_key(0, e1, sum);
                }
              }
            }else{
              if(IsNotInf(sum)){
                // update approximate edge min-marginal
                int e1t = reverse_pairwise_label(e1,K);
                double last_factor = p[last_factor_id][e1t] ;//- min_factor[last_factor_id];
                sum += last_factor;
                if(sum < edge_mmarginal[d][e1t]){
                  edge_mmarginal[d][e1t] = sum;
                  cy_argM[d][i][e1] = tb; // keep argmin for tracing back
                  if( sum  < cy_ub[d] ){
                    updated++;
                    cy_final_assignment[d] = e1;
                    cy_ub[d] = sum;
                  }
                }
              }
            }
          }
          if(fid==0){
            e0 += K; e1 += K;
          }else{
            e0++; e1++;
          }
        }
        min_key = cy_P[d][i].min_key(cy_min_incoming_msg[d][i], min_factor[cy_P[d][i].f[1]]);
      }
    }
  }while(!done);
  //printf("q"); fflush(stdout);
  time = ((double)clock()-t)/CLOCKS_PER_SEC;

#if DEBUG_MODE
  check_critical_path(d);
#endif

  node_mmarginal[d] = reduce_i_const(K, edge_mmarginal[d]);

  // trace back to get MAP assignment

  obj = cy_ub[d] + constant;

  vector<int> rot_assignment(N+1,-1);
  rot_assignment[0] = cy_final_assignment[d]%K;
  rot_assignment[N-1] = (cy_final_assignment[d]-rot_assignment[0])/K;
  for(int i=N-2; i>=1; i--){
    long idx = rot_assignment[0] + rot_assignment[i+1]*K;
    rot_assignment[i] = cy_argM[d][i-1][idx];
  }
  rot_assignment[N] = rot_assignment[0];

  assignment = rotate_cycle_assignment(rot_assignment, -d);

#if WRITE_HEAP
  write_heap_state(d, "heap_state_after.txt");
#endif

  ready[d] = 1;

  /////////////////////////////////////////////////////////////////////////////////
  // correctness check
#if DEBUG_MODE
  assert(fabs(obj-naive_min_sum_bp()) < EPS);
  assert(fabs(get_obj()-obj) < EPS);
  assert(fabs(minimize(node_mmarginal[d])-cy_ub[d]) < EPS);
  assert(fabs(node_mmarginal[d][assignment[d]]-cy_ub[d]) < EPS);
  assert(fabs(edge_mmarginal[d][assignment[(N-1+d)%N]+K*assignment[d]] - cy_ub[d]) < EPS);
#endif
  //////////////////////////////////////////////////////////////////////////////////////////////////////

  return obj;
}

void cp_cycle::add_vec_pairwise_update_heaps(int idx, const vector<double> & a)
{

  /*
  int K0 = a.size();
  assert(K0 == K*K);
  for(int l=0; l<K0; l++)
    if(a[l]!=0)
      add_pairwise_update_heaps(idx, l, a[l]);
  */
  int maxid;
  double maxval = MAX(maximize(a, maxid), 0);
  constant += maxval;
  for(int j=0; j<K*K; j++)
    p[idx][j] -= maxval - a[j];

  allzero = 0;
  ready.assign(N,0);

  if(maintain_heaps==0)
    return;

  int L = N-2;
  for(int d=0; d<N; d++){

    int i = (N+idx-d)%N;
    assert(i>=0 && i<N);

    if(i==N-1){       //cout << "update last_factor" << endl;

      cy_reset_msg_from[d] = MIN(cy_reset_msg_from[d], L-1);
      /*
      cy_ub[d] = INF;
      for(int j=0; j<K*K; j++){
        edge_mmarginal[d][j] -= maxval - a[j];
        if(edge_mmarginal[d][j] < cy_ub[d]){
          cy_ub[d] = edge_mmarginal[d][j];
          cy_final_assignment[d] = j;
        }
      }
      cy_final_assignment[d] = reverse_pairwise_label(cy_final_assignment[d], K);
      */
    }else{
      //int l0 = l;
      int h, f;
      h = MAX(i-1,0);
      f = (i==0?0:1);  //d=0; i=0: (0,0)  i=1: (0,1)  i=2: (1,1) i=3: (2,1) ....  i=N-1: -last

      cy_reset_msg_from[d] = MIN(cy_reset_msg_from[d], h);
      /*if(maxval > 0){ // leaving one entry off heap is even worse (as we will need to reset all entries in the other heap)
	for(int j=0; j<K*K; j++){
	  if(cy_P[d][h].inpool[f][j]>0)
	    cy_P[d][h].inpool[f][j] = K*K+1; // mark for decrease key
	  else
	    cy_P[d][h].inpool[f][j] = -K*K-1; // mark for add
	}
	}else{*/
	for(int j=0; j<K*K; j++){
	  if(maxval - a[j] > 0){
	    if(cy_P[d][h].inpool[f][j]>0)
	      cy_P[d][h].inpool[f][j] = K*K+1; // mark for decrease key
	    else
	      cy_P[d][h].inpool[f][j] = -K*K-1; // mark for add
	  }
	}
	//}
    }
  }
}

/*

void cp_cycle::add_pairwise_update_heaps(int idx, int l, double a)
{

  p[idx][l] += a;

  //min_factor[idx] = MIN(min_factor[idx], p[idx][l]);


  allzero = 0;
  ready.assign(N,0);

  if(maintain_heaps==0)
    return;

  // if increase an entry off the critical path, solution won't change

  int L = N-2;


  if(a < 0)
    cy_rlb_needs_update.assign(N,1);

  int lt = reverse_pairwise_label(l,K);

  for(int d=0; d<N; d++){

    int i = (N+idx-d)%N;
    assert(i>=0 && i<N);

    if(i==N-1){       //cout << "update last_factor" << endl;


      //increase on critical path : update
      //                                   edge_mmarginal, recompute
      //       decrease on critical path : modify solution
      //       increase off critical path : no change
      //     decrease off critical path : compare two


      cy_reset_msg_from[d] = MIN(cy_reset_msg_from[d], L-1);



    }else{
      int l0 = l;
      int h, f;
      h = MAX(i-1,0);
      f = (i==0?0:1);  //d=0; i=0: (0,0)  i=1: (0,1)  i=2: (1,1) i=3: (2,1) ....  i=N-1: -last

      ////////////
      //cy_P[d][h].fset.factors[f].phi[l0] += a;
      ////////////

      if(cy_P[d][h].inpool[f][l0] == 0 && a < 0){ // decrease entry off heap

        //if(offset_applied)
          //cy_reset_msg_from[d] = MIN(cy_reset_msg_from[d], h);
        //}else{
          double val = p[cy_P[d][h].f[f]][l0]; // - min_factor[cy_P[d][h].f[f]];

          if(h==0 && f==0)
            cy_P[d][h].add(f, l0, val );
          else
            cy_P[d][h].add_init_factor(f, l0, val );

          //}

      }else if(cy_P[d][h].inpool[f][l0] == 0 && a > 0){ // increase entry off heap

        cy_reset_msg_from[d] = MIN(cy_reset_msg_from[d], h);

      }else if(cy_P[d][h].inpool[f][l0] == 1 && a < 0){  // decrease entry on heap

        //if(offset_applied)
        // cy_reset_msg_from[d] = MIN(cy_reset_msg_from[d], h);

        double val = p[cy_P[d][h].f[f]][l0] ; //- min_factor[cy_P[d][h].f[f]]; // set.factors[f].phi[l0]
        fibheap_el* tmp = (fibheap_el*)(cy_P[d][h].handle[f][l0]);
        if(tmp->fhe_key > val){
          if (h==0 && f==0)
            cy_P[d][h].decrease_key(f, l0, val);
          else
            cy_P[d][h].decrease_key_init_factor(f, l0, val);

        }

      }else if(cy_P[d][h].inpool[f][l0] == 1 && a > 0) { // increase entry on heap

        cy_reset_msg_from[d] = MIN(cy_reset_msg_from[d], h);
       }
      ////////////////////////////////
    }
  }
  }*/

void cp_cycle::check_heaps(int d)
{
  int L = N-2;

  // make sure that all heap values equals actual value
  for(int i=0; i<L; i++){
    for(int j=0; j<2; j++){
      for(int l=0; l<K; l++){
        for(int k=0; k<K; k++){
          if(cy_P[d][i].inpool[j][l+k*K] > 0){
            fibheap_el* tmp = (fibheap_el*)(cy_P[d][i].handle[j][l+k*K]);
            double heap_val = tmp->fhe_key;
            double val = cy_P[d][i].f[j]>=0?p[cy_P[d][i].f[j]][l+k*K]:cy_P[d][i].msg[l+k*K];
            assert(fabs(heap_val - val)<EPS);
          }
        }
      }
    }
  }

  /* check incomplete msg A <= B <= A' */
  for(int i=1; i<L; i++){
    for(int l=0; l<K; l++){
      for(int k=0; k<K; k++){
        double val = INF;
        for(int m=0; m<K; m++){

          double val1 = (cy_P[d][i-1].f[0]>=0)?(p[cy_P[d][i-1].f[0]][l+m*K] /*- min_factor[cy_P[d][i-1].f[0]]*/):cy_P[d][i-1].msg[l+m*K];
          double val2 = p[cy_P[d][i-1].f[1]][m+k*K];// - min_factor[cy_P[d][i-1].f[1]];

          val = MIN(val, val1+val2);
          if(cy_P[d][i-1].inpool[0][l+m*K]<=0 && cy_P[d][i-1].inpool[1][m+k*K]<=0){
            assert(cy_P[d][i].msg[l+k*K] - val1 - val2 <= EPS);                    //B <= A'

            //if equal sign holds, we must have the correct argmin
            /*
            if(fabs(cy_P[d][i].msg[l+k*K] - val1 - val2) < EPS){ // sometimes this holds by accident
              assert(cy_argM[d][i-1][l+k*K] == m);
            }
            */
          }
        }
        assert(cy_P[d][i].msg[l+k*K] - val >= -EPS);   // B >= A
      }
    }
  }

  double val = INF;
  int i = L;
  for(int l=0; l<K; l++){
    for(int k=0; k<K; k++){
      double last_factor = p[cy_last_factor_id[d]][k+l*K];// - min_factor[cy_last_factor_id[d]];
      for(int m=0; m<K; m++){

        double val1 = (cy_P[d][i-1].f[0]>=0)?(p[cy_P[d][i-1].f[0]][l+m*K] /*- min_factor[cy_P[d][i-1].f[0]]*/):cy_P[d][i-1].msg[l+m*K];
        double val2 = p[cy_P[d][i-1].f[1]][m+k*K] ;//- min_factor[cy_P[d][i-1].f[1]];

        val = MIN(val, val1+val2+last_factor);
        if(cy_P[d][L-1].inpool[0][l+m*K]<=0 && cy_P[d][L-1].inpool[1][m+k*K]<=0){
          assert(cy_ub[d] - last_factor - val1 - val2 <=EPS);

          /*
          if(fabs(cy_ub[d] - last_factor - val1 - val2) < EPS){  // sometimes this holds by accident
            assert(cy_argM[d][L-1][l+k*K] == m);
            assert(cy_final_assignment[d] == l+k*K);
          }
          */
        }
      }
    }
  }
  assert(!IsNotInf(cy_ub[d]) || cy_ub[d] - val >= -EPS);
}

/*
void cp_cycle::reset_msg(int d, int from)
{

  edge_mmarginal[d].assign(K*K,INF);
  cy_ub[d] = INF;

  // need to put all incoming msg elements at cy_P[from] back on heap
  int L = N-2;
  if(from < L){
    for(int l=0; l<K*K; l++){
      double val = from>0?cy_P[d][from].msg[l]:(p[cy_P[d][from].f[0]][l] );//- min_factor[cy_P[d][from].f[0]]);
      if(IsNotInf(val)){
        if(cy_P[d][from].inpool[0][l]==0){
          //if(from>0)
            cy_P[d][from].add(0, l, val);
          //else
          //  cy_P[d][from].add_init_factor(0, l, val);
        }
      }
    }
  }
  // reset all msgs from here to root
  for(int i=from+1; i<N-1; i++){
    //reset_cy_msg(d,i);
    cy_argM[d][i-1].assign(K*K,(int)INF);
  }
  for(int i=from+1; i<L; i++){
    cy_P[d][i].reset_incoming_msg();
    cy_min_incoming_msg[d][i] = INF;
  }
  }*/

/*
double cp_cycle::incomplete_bp_warmstart_with_minmarginals(double & time)
{
  assert(maintain_heaps ==);
  assert(!allzero);
  time = 0;
  double t;
  double min_energy = incomplete_bp_warmstart(1, t);
  time += t;

  obj_ready = 1;

  double min_energy2 = incomplete_bp_warmstart(2, t);
  time += t;

  double sum_min_init_factor = vector_sum(min_factor);

  if(edge_mmarginal_needed[N-1] || node_mmarginal_needed[N-1] || node_mmarginal_needed[0]){
     edge_mmarginal[N-1] = add_vector(cy_last_msg[0], p[N-1]);
     add_s_v(sum_min_init_factor - min_factor[N-1], edge_mmarginal[N-1]);
     edge_mmarginal_ready[N-1] = 1;
  }

  if(edge_mmarginal_needed[0] || node_mmarginal_needed[0] || node_mmarginal_needed[1]){
    edge_mmarginal[0] = add_vector(cy_last_msg[1], p[0]);
    add_s_v(sum_min_init_factor - min_factor[0], edge_mmarginal[0]);
    edge_mmarginal_ready[0] = 1;
  }

  // reset msgs
  for(int i=0; i<N-2; i++){
    if(edge_mmarginal_needed[i+1] || node_mmarginal_needed[i+1] || node_mmarginal_needed[i+2]){
      /*
      for(int j=0; j<2; j++){
        if(cy_Q_reset[i][j] > 1 ){

          cy_Q[i].onshelf[j].assign(K*K,0);

          // fh_deleteheap(cy_Q[i].data[j]); //
          // cy_Q[i].data[j] = fh_makekeyheap();

          cy_Q[i].empty_heap(j);

          cy_Q[i].min_value[j] = INF;
          cy_Q[i].cover_value[1-j] = INF;

          //cy_Q[i].min_key_val[j] = fh_minkey(cy_Q[i].data[j]);

          //if(cy_Q_reset[i][j] > 2)

          for(int l=0; l<K*K; l++)
            if(IsNotInf(cy_Q[i].fset.factors[j].phi[l]))
              cy_Q[i].add(j, l,  cy_Q[i].fset.factors[j].phi[l]);

          p_edge_mmarginal[i+1].assign(K*K,INF);

        }else if (cy_Q_reset[i][j] == 1){
          for(int l=0; l<K*K; l++)
            if(cy_Q_updated[i][j][l]){
              if(cy_Q[i].inpool[j][l]){
                fibheap_el* tmp = (fibheap_el*)(cy_Q[i].handle[j][l]);
                if(tmp->fhe_key >  cy_Q[i].fset.factors[j].phi[l])
                  cy_Q[i].decrease_key(j, l,  cy_Q[i].fset.factors[j].phi[l]);
              }else{
                if(IsNotInf(cy_Q[i].fset.factors[j].phi[l]))
                  cy_Q[i].add(j,l, cy_Q[i].fset.factors[j].phi[l]);
              }
            }

        }
      }

      //}
      //if(edge_mmarginal_needed[i+1] || (node_mmarginal_needed[i+1] && i+1 > 1)){
      combine_msg(i, t, sum_min_init_factor);
      time += t;

      edge_mmarginal[i+1] = p_edge_mmarginal[i+1];
      add_vector(edge_mmarginal[i+1], p[i+1], 1.0, sum_min_init_factor - min_factor[i+1]);



      edge_mmarginal[i+1] = p[i+1];

      bounded_pff_min_sum(K, matrix_transpose(K, cy_msg[0][i]),
                          cy_msg[1][N-3-i], edge_mmarginal[i+1], min_energy-sum_min_init_factor+min_factor[i+1], t); //
      time += t;

      add_s_v(sum_min_init_factor - min_factor[i+1], edge_mmarginal[i+1]);

      edge_mmarginal_ready[i+1] = 1;

      cy_Q_reset[i].assign(2,0);
      //cy_Q_updated[i].assign(K*K,0);
    }

  }

  for(int i=0; i<N; i++){
    if(node_mmarginal_needed[i]){
      assert(edge_mmarginal_ready[i]);
      assert(edge_mmarginal_ready[(i+N-1)%N]);
      node_mmarginal[i] = reduce_j_const(K, edge_mmarginal[i]);
      reduce_i_const(K, edge_mmarginal[(i+N-1)%N], node_mmarginal[i]);
      node_mmarginal_ready[i] = 1;
    }
  }

  ///////////////////////////////////////////////////////////////////////////////// // comment out
  // correctness check
  /*
  assert(fabs(get_obj()-min_energy) < EPS);
  assert(fabs(min_energy2-min_energy) < EPS);
  for(int i=0; i<N; i++){
    assert(assignment[i] >= 0);
    if(node_mmarginal_ready[i]){
      assert(fabs(minimize(node_mmarginal[i])-min_energy) < EPS);
      assert(fabs(node_mmarginal[i][assignment[i]]-min_energy) < EPS);
    }
    if(edge_mmarginal_ready[i])
      assert(fabs(edge_mmarginal[i][assignment[i]+K*assignment[i+1]] - min_energy) < EPS);
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////



  return min_energy;

  }*/

void print_float(double f)
{
  if(IsNotInf(f))
    printf("%.3f", f);
  else
    printf(" inf ");
}

 void compare_minmarginals(cp_cycle & c0, cp_cycle & c1, cp_cycle & c2, int d)
{
  int K = c0.K;
  printf("-------------------------unary-----------------------------------\n");
  for(int j=0; j<K; j++){
    printf("(");
    print_float(c0.node_mmarginal[d][j]);
    printf("/");
    print_float(c1.node_mmarginal[d][j]);
    printf("/");
    print_float(c2.node_mmarginal[d][j]);
    printf(")");
  }
  printf("-----------------------------------------------------------------\n");

  printf("------------------------------pairwise----------------------------\n");
  for(int j=0; j<K; j++){
    for(int l=0; l<K; l++){
      printf("(");
      print_float(c0.edge_mmarginal[d][l+j*K]);
      printf("/");
      print_float(c1.edge_mmarginal[d][l+j*K]);
      printf("/");
      print_float(c2.edge_mmarginal[d][l+j*K]);
      printf(")");
    }
  }
  printf("---------------------------------------------------------------\n");
}


void write_messages(vector< vector<double> > & msg, vector<int> & assignment, int forward, const char * filename)
{
  int N = msg.size()+1;
  int K = (int)sqrt(msg[0].size());
  FILE *fp = fopen(filename, "w");
  for(int i=0; i<N-1; i++){

    int a,b;
    if(forward){
      a = 0; b = i+1;
    }else{
      a = 0; b = N-i-1;
    }

    for(int l=0; l<K; l++){
      for(int k=0; k<K; k++){
        if (k==assignment[a] && l == assignment[b]){
          if(IsNotInf(msg[i][k+l*K])){
            fprintf(fp, "[%1.4f] ", msg[i][k+l*K]);
          }else{
            fprintf(fp, "[inf]    ");
          }
        }else{
            if(IsNotInf(msg[i][k+l*K])){
              fprintf(fp, " %1.4f  ", msg[i][k+l*K]);
            }else{
              fprintf(fp, " inf     ");
            }
          }
        }
        fprintf(fp, "\n");
      }
      fprintf(fp, "-------------------------------------------------\n");
  }
  fclose(fp);
}

void cp_cycle::reparameterize_singleton(int i)
{
  vector<double> v1 = reduce_j_const(K, p[i]);
  vector<double> v2 = reduce_i_const(K, p[(N+i-1)%N]);
  vector<double> u = vector_diff(v2, v1);
  vector<double> U1 = replicate_columns(u, K, 0.5);
  vector<double> U2 = replicate_rows(u, K, -0.5);
  add_vec_pairwise_update_heaps(i, U1);
  add_vec_pairwise_update_heaps((N+i-1)%N, U2);
}

void cp_cycle::add_vec_singleton_update_heaps(int i, const vector<double> & a)
{
  /*
  vector<double> A = replicate_columns(a, K, 0.5);
  vector<double> B = replicate_rows(a, K, 0.5);
  add_vec_pairwise_update_heaps(i, A);
  add_vec_pairwise_update_heaps((N+i-1)%N, B);
  */
  vector<double> A = replicate_columns(a, K, 1.0);
  add_vec_pairwise_update_heaps(i, A);
}

/*
void cp_cycle::add_singleton_update_heaps(int i, int l, double a)
{
  for(int m=0; m<K; m++)
 add_pairwise_update_heaps(i, l+m*K, a);
    }*/


/*
double cp_cycle::get_cy_msg(int d, int i, int j)
{


  int l = (d==1?j:reverse_pairwise_label(j,K));
  if(i==N-2)
    return cy_last_msg[d][l];
  int q = (d==0?i:N-i-3);
  return cy_Q[q].fset.factors[d].phi[l];

  return cy_msg[d][i][j];
}

void cp_cycle::update_cy_msg(int d, int i, int j, double val){


  int l = (d==1?j:reverse_pairwise_label(j,K));
  if(i==N-2){
    cy_last_msg[d][l] = val;
  }else{

    int q = (d==0?i:N-i-3);

    cy_Q_updated[q][d][l] = 1;

    if(val > cy_Q[q].fset.factors[d].phi[l])
        cy_Q_reset[q][d] = 3;

    cy_Q_reset[q][d] = MAX(cy_Q_reset[q][d], 1);

    cy_Q[q].fset.factors[d].phi[l] = val;
  }
  cy_msg[d][i][j] = val;
}

void cp_cycle::add_cy_msg(int d, int i, double val){

  if(i==N-2){
    add_s_v(val, cy_last_msg[d]);
  }else{


    int q = (d==0?i:N-i-3);
    add_s_v(val, cy_Q[q].fset.factors[d].phi);
    cy_Q_reset[q][d] = 3;

  }
  add_s_v(val, cy_msg[d][i]);
}

void cp_cycle::reset_cy_msg(int d, int i){

  if(i==N-2){
    cy_last_msg[d].assign(K*K, INF);
  }else{


    int q = (d==0?i:N-i-3);
    cy_Q[q].fset.factors[d].phi.assign(K*K,INF);
    cy_Q_reset[q][d] = 2;

  }

  cy_msg[d][i].assign(K*K,INF);
}

void cp_cycle::reset_cy_msg(int d, int i, const vector<double> & M)
{


  if(i==N-2){
    assert(0==1);
  }else{
    int q = (d==0?i:N-i-3);
    if(d==0)
      cy_Q[q].fset.factors[d].phi = matrix_transpose(K, M);
    else
      cy_Q[q].fset.factors[d].phi = M;
    cy_Q_reset[q][d] = 3;
    }
  cy_msg[d][i] = M;
}
*/

vector<double> add_vector(const vector<double> & v1, const vector<double> & v2)
{
  vector<double> result(v1.size(),0);
  for(int i=0; i<result.size(); i++)
    result[i] = v1[i]+v2[i];
  return result;
}

void cp_problem::set_needed_mmarginals_in_cycles()
{
  for(int block = 0; block < N+M; block++){
    if(block<N){
      int var_id = block;
      for(int i=0; i<var_cp_cy[var_id].size(); i++){
        int c = var_cp_cy[var_id][i];
        int idx = var_cp_id[var_id][i];
        CY[c].next_mmarginal_needed.push_back(idx);
      }
    }else{
      int edge_id = block-N;
      for(int i=0; i<edge_cp_cy[edge_id].size(); i++){
        int c = edge_cp_cy[edge_id][i];
        int idx = abs(edge_cp_id[edge_id][i])-1;
         CY[c].next_mmarginal_needed.push_back(CY[c].N+idx);
      }
    }
  }
  for(int i=0; i<CYsize; i++){
    CY[i].idx_next_mmarginal_needed = 0;
    assert(IsPermutation(CY[i].next_mmarginal_needed, 2*CY[i].N));
  }
}

bool IsPermutation(const vector<int> & l, int N)
{
  vector<int> tmp(N,0);
  for(int i=0; i<N; i++){
    if(l[i]<0 || l[i]>=N)
      return false;
    tmp[l[i]]++;
  }
  for(int i=0; i<N; i++)
    if(tmp[i] != 1)
      return false;
  return true;
}

void cp_cycle::update_min_factor()
{
  min_factor.assign(N,0);
  for(int i=0; i<N; i++)
    min_factor[i] = minimize(p[i]);
}

vector<int> rotate_cycle_assignment(const vector<int> & a, int offset)
{
  int N = a.size()-1;
  vector<int> result(N,-1);
  for(int i=0; i<N; i++)
    result[i] = a[(i+offset+N)%N];
  result.push_back(result[0]);
  return result;
}

void damp_pbca_update(vector< vector <double> > & current, vector< vector <double> > & last, double lambda)
{
  int m = current.size();
  if(0){                                 // TODO do this "sophisticaed" damping?
    int K = current[0].size();
    assert(lambda > 0.0 && lambda < 1.0);
    if(last.size() == 0){
      last.assign(m, vector<double>(K,0));
      for(int i=0; i<m; i++)
        for(int j=0; j<K; j++)
          last[i][j] = current[i][j];
    }else{
      assert(last.size() == m);
      assert(last[0].size() == K);
      for(int i=0; i<m; i++){
        for(int j=0; j<K; j++){
          current[i][j] = lambda*last[i][j] + (1-lambda)*current[i][j];
          last[i][j] = current[i][j];
        }
      }
    }
  }else{
    for(int i=0; i<m; i++)
      multiply_s_v(1-lambda, current[i]);

  }
}

vector<double> replicate_rows(const vector<double> & a, int K, double s)
{
  int K0 = a.size();
  vector<double> A(K0*K,0);
  for(int i=0; i<K0; i++)
    for(int j=0; j<K; j++)
      A[j+i*K] = a[i]*s;
  return A;
}

vector<double> replicate_columns(const vector<double> & a, int K, double s)
{
  int K0 = a.size();
  vector<double> A(K0*K,0);
  for(int i=0; i<K0; i++)
    for(int j=0; j<K; j++)
      A[i+j*K0] = a[i]*s;
  return A;
}


void cp_cycle::write_heap_state(int d, const char * filename)
{
  FILE *fp = fopen(filename, "w");
  fprintf(fp, "factors_updated: ");
  //for(int i=0; i<N; i++)
  //   fprintf(fp, "%d ", factor_updated[d][i]);
  fprintf(fp, "\n");
  fprintf(fp, "L=%d\n", cy_P[d].size());
  fprintf(fp, "final_assignment = %d, ub = %.4f, constant = %.4f\n", cy_final_assignment[d], cy_ub[d], constant);
  int L = cy_P[d].size();
  for(int i=0; i<L; i++){
    fprintf(fp, "-------------------------------------------------\n");
    fprintf(fp, "pool: %d/%d \n", vector_cnt_pos(cy_P[d][i].inpool[0]), (int)cy_P[d][i].inpool[0].size());
    fprintf(fp, "pool: %d/%d \n", vector_cnt_pos(cy_P[d][i].inpool[1]), (int)cy_P[d][i].inpool[1].size());
    fprintf(fp, "min_key_msg = %.4f, min_key_init = %.4f, min_incoming = %.4f, minf = %.4f, bound = %.4f\n",
            cy_P[d][i].min_key_msg, cy_P[d][i].min_key_init, cy_min_incoming_msg[d][i], min_factor[cy_P[d][i].f[1]], cy_ub[d]-cy_rlb[d][i]);
    fprintf(fp, "-------------------------------------------------\n");

    for(int j=0; j<2; j++){
      if(i>0 && j==0){
        int a = j==0?0:(i+1);
        int b = j==0?(i+1):i+2;
        a = (a+d)%N;
        b = (b+d)%N;
        for(int l=0; l<K; l++){
          for(int k=0; k<K; k++){
            assert(cy_P[d][i].f[j] < 0);
            double val = cy_P[d][i].msg[k+l*K]; //p[cy_P[d][i].f[j]][k+l*K];
            if (k==assignment[a] && l == assignment[b]){
              if(IsNotInf(val)){
                fprintf(fp, "[(%d)(%d)%1.4f] ", cy_P[d][i].inpool[j][k+l*K], cy_argM[d][i-1][k+l*K], val);
              }else{
                fprintf(fp, "[(%d)(N)inf]    ", cy_P[d][i].inpool[j][k+l*K]);
              }
            }else{
              if(IsNotInf(val)){
                fprintf(fp, " (%d)(%d)%1.4f  ", cy_P[d][i].inpool[j][k+l*K], cy_argM[d][i-1][k+l*K], val);
              }else{
                fprintf(fp, " (%d)(N)inf     ", cy_P[d][i].inpool[j][k+l*K]);
              }
            }
          }
          fprintf(fp, "\n");
        }
        fprintf(fp, "-------------------------------------------------\n");
      }else{
        int a = j==0?0:(i+1);
        int b = j==0?(i+1):i+2;
        a = (a+d)%N;
        b = (b+d)%N;
        for(int l=0; l<K; l++){
          for(int k=0; k<K; k++){
            double val = p[cy_P[d][i].f[j]][k+l*K];
            if (k==assignment[a] && l == assignment[b]){
              if(IsNotInf(val)){
                fprintf(fp, "[(%d)%1.4f]    ", cy_P[d][i].inpool[j][k+l*K], val);
              }else{
                fprintf(fp, "[(%d)inf]       ", cy_P[d][i].inpool[j][k+l*K]);
              }
            }else{
              if(IsNotInf(val)){
                fprintf(fp, " (%d)%1.4f     ", cy_P[d][i].inpool[j][k+l*K], val);
              }else{
                fprintf(fp, " (%d)inf        ", cy_P[d][i].inpool[j][k+l*K]);
              }
            }
          }
          fprintf(fp, "\n");
        }
        fprintf(fp, "-------------------------------------------------\n");
      }
    }
  }

  int a = (N-1+d)%N;
  int b = d;
  for(int l=0; l<K; l++){
    for(int k=0; k<K; k++){
      double val = edge_mmarginal[d][k+l*K];
      if (k==assignment[a] && l == assignment[b]){
        if(IsNotInf(val)){
          fprintf(fp, "[%1.4f] ", val);
        }else{
          fprintf(fp, "[ inf ] ");
        }
      }else{
        if(IsNotInf(val)){
          fprintf(fp, "%1.4f ", val);
        }else{
          fprintf(fp, " inf  ");
        }
      }
    }
    fprintf(fp, "\n");
  }
  fprintf(fp, "-------------------------------------------------\n");
  fclose(fp);
}

void cp_cycle::check_critical_path(int d)
{
  cp_cycle c = cp_cycle(*this);
  c.maintain_heaps = 0;
  c.init_heaps_called = 0;
  c.init_heaps();
  c.constant = constant;

  double time;
  c.incomplete_bp(0, time); // assuming this is correct

  assert(fabs(c.get_obj() - naive_min_sum_bp()) < EPS);

  double val = constant;
  for(int i=0; i<N-2; i++){
    for(int j=0; j<2; j++){
      if( j == 0 && i>0 ) continue;
      int a = j==0?0:(i+1);
      int b = j==0?(i+1):i+2;
      a = (a+d)%N;
      b = (b+d)%N;
      int k = c.assignment[a];
      int l = c.assignment[b];
      val += p[cy_P[d][i].f[j]][k+l*K];
      if(cy_P[d][i].inpool[j][k+l*K] > 0){

	fibheap_el* tmp = (fibheap_el*)(cy_P[d][i].handle[j][k+l*K]);
	assert(fabs(tmp->fhe_key - p[cy_P[d][i].f[j]][k+l*K]) < EPS);
	assert(cy_P[d][i].inpool[j][k+l*K] <= 0); // critical entry must be active
      }
      //TODO if not active, check the halting inequality
    }
  }
  int a = (N-1+d)%N;
  int b = d;
  int k = c.assignment[a];
  int l = c.assignment[b];
  val += p[cy_last_factor_id[d]][k+l*K];
  assert(fabs(val - c.get_obj()) < EPS);
}

double solve_single_edge(cp_graph & G)
{
  assert(G.N==2);
  assert(G.p.size() == 1);
  assert(G.edge_nodes.size()==1);
  int id0 = G.edge_nodes[0][0];
  int id1 = G.edge_nodes[0][1];
  assert(id0+id1 == 1 && id0*id1 == 0);
  vector<double> E1 = G.p[0];
  add_i2ij(G.s[id0], E1, 1.0);
  add_j2ij(G.s[id1], E1, 1.0);
  int idx; //  = a0 + a1*K
  double min_energy = minimize(E1, idx);
  G.assignment.assign(2,0);
  G.assignment[id0] = idx%G.K;
  G.assignment[id1] = (idx-(idx%G.K))/G.K;
  G.obj = min_energy;
  return min_energy;
}

double solve_cycle(cp_graph & G)
{
  cp_cycle c(G);
  double time;
  c.init_heaps();
  c.incomplete_bp(0,time);
  G.obj = c.obj;
  G.assignment.assign(G.N,0);
  for(int i=0; i<G.N; i++)
    G.assignment[c.nodes[i]] = c.assignment[i];
  return G.obj;
}

cp_cycle::cp_cycle(const cp_graph & g)
{
  assert(g.N == g.M);
  N = g.N;
  K = g.K;
  nodes.assign(N,0);
  edges.assign(N,0);
  p.assign(N, vector<double>());
  maintain_heaps = 0;
  allzero = 0;
  vector<int> visited(N,0);
  int previous_i;
  int i = 0;
  int cnt = 0;
  do{
    visited[i]++;
    previous_i = i;
    for(int j=0; j<g.nb[i].size(); j++){
      if(visited[g.nb[i][j]] == 0 || (g.nb[i][j]==0 && cnt>1)){
	if(g.pid[i][j] > 0)
	  p[cnt] = g.p[abs(g.pid[i][j])-1];
	else
	  p[cnt] = matrix_transpose(K, g.p[abs(g.pid[i][j])-1]);
	nodes[cnt] = i;
	edges[cnt] = g.pid[i][j];
	add_i2ij(g.s[i], p[cnt], 1.0);
	i = g.nb[i][j];
	cnt++;
	break;
      }
    }
  }while(i!=previous_i);
  assert(cnt == N);
  assert(visited[0]==2);
  for(i=1; i<N; i++)
    assert(visited[i]==1);  // assert g is a cycle
}
