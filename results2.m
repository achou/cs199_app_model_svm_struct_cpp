function [] = results()
% 20 hyps
basedir = 'weights20overlapnorm/weights20abcdoverlapnorm';
resultsdir = '/results.txt';
%name = [basedir, 'EM', resultsdir]
%em = loadResults(name)
name = [basedir, '1', resultsdir];
v1 = loadResults(name)
name = [basedir, '2', resultsdir];
v2 = loadResults(name)
name = [basedir, '3', resultsdir];
v3 = loadResults(name)
name = [basedir, '4', resultsdir];
v4 = loadResults(name)
name = [basedir, '5', resultsdir];
v5 = loadResults(name)
name = [basedir, '6', resultsdir];
v6 = loadResults(name)

figure
plot(...
  0.00+v1(:,1), v1(:,2),...
  0.01+v2(:,1), v2(:,2),...
  0.02+v3(:,1), v3(:,2),...
  0.03+v4(:,1), v4(:,2),...
  0.04+v5(:,1), v5(:,2),...
  0.05+v6(:,1), v6(:,2),...
  'LineWidth', 5)
xlabel('log(c)');
ylabel('error');
legend('hc','2','3','4','5','6','Location','NorthEast')
title('Performance using Learned Weights including Overlap (20 hyps)');

end
function [test, train] = loadResults(name, opt)
fid = fopen(name,'rt');
C = textscan(fid,'%s %f %s %s %f','Delimiter',' ','CollectOutput',1);
fclose(fid);
C
%keyboard
test  = [C{2}(1:2:end), C{4}(1:2:end)];
train = [C{2}(2:2:end), C{4}(2:2:end)];
if size(test)
  test(:,1) = log10(test(:,1)/10000);
end
if size(train)
  train(:,1) = log10(train(:,1)/10000);
end
end
