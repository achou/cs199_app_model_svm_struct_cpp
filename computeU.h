#ifndef COMPUTEU_H
#define COMPUTEU_H
#include "common.h"
#include "minSumWithAppearance.h"

inline double computeU(Appmodel *fg, Appmodel *bg, double fgtotal, double bgtotal){
  double u1 = 0.0;
  double u2 = 0.0;
  int *a1 = (int*)fg->m;
  int *a2 = (int*)bg->m;
  for(int i=0; i<TOTAL_COLOR_BUCKETS; i++){
    u1 += a1[i] * log2( a1[i]);
    u2 += a2[i] * log2( a2[i]);
  }
  double u = -(u1/fgtotal + u2/bgtotal);
  //printf("%lf, %lf\n", log2(fgtotal), log2(bgtotal));
  u += log2(fgtotal)/fgtotal + log2(bgtotal)/bgtotal;
  return u;
  /*
  double u = 0.0;
  int *a1 = (int*)fg->m;
  int *a2 = (int*)bg->m;
  for(int i=0; i<TOTAL_COLOR_BUCKETS; i++){
    u += (a2[i]-a1[i]) * log2( double(a1[i]) / a2[i]);
  }
  u /= (fgtotal+bgtotal);
  u += (bgtotal-fgtotal)*log2(double(bgtotal)/fgtotal)/(fgtotal+bgtotal);
  return u;
  */
  /*
  double u = 0.0;
  int *a1 = (int*)fg->m;
  int *a2 = (int*)bg->m;
  for(int i=0; i<TOTAL_COLOR_BUCKETS; i++){
    u += (a2[i]-a1[i]) * log2( double(a1[i]) / a2[i]);
  }
  u /= (fgtotal+bgtotal);
  u += (bgtotal-fgtotal)*log2(double(bgtotal)/fgtotal)/(fgtotal+bgtotal);
  return u;
  */
  /*
  double u = 0.0;
  int *a1 = (int*)fg->m;
  int *a2 = (int*)bg->m;
  for(int i=0; i<TOTAL_COLOR_BUCKETS; i++){
    u += a1[i] * log2( double(a2[i]) / a1[i]);
  }
  return u/fgtotal + log2(double(fgtotal)/bgtotal);
  */
  /*
  double u = 0.0;
  int *a1 = (int*)fg->m;
  int *a2 = (int*)bg->m;
  for(int i=0; i<TOTAL_COLOR_BUCKETS; i++){
    if(a1[i] <APP_MODEL_PRIOR || a2[i]<APP_MODEL_PRIOR){
      printf("%d, %d, %d, %lf, %lf\n", i, a1[i], a2[i], fgtotal, bgtotal);
      Error("non-positive weight");
    }
    u += (a1[i] + a2[i]) * log2( double(a2[i]) / a1[i]);
  }
  return u/(fgtotal+bgtotal) + log2(double(fgtotal)/bgtotal);
  */
  /*
  double u = 0.0;
  int *a1 = (int*)fg->m;
  int *a2 = (int*)bg->m;
  for(int i=0; i<TOTAL_COLOR_BUCKETS; i++){
    if(a1[i] <APP_MODEL_PRIOR || a2[i]<APP_MODEL_PRIOR){
      printf("%d, %d, %d, %lf, %lf\n", i, a1[i], a2[i], fgtotal, bgtotal);
      Error("non-positive weight");
    }
    u += a1[i]* log2( fgtotal / a1[i]);
    u += a2[i]* log2( bgtotal / a2[i]);
  }
  //printf("%lf ", u);
  return u/(fgtotal+bgtotal);
  */
}
inline double computeU2(Appmodel *fg, Appmodel *bg, int fgtotal, int bgtotal,
    double *logs, int nlog){
  Error("This does not match computeU");
  double u = 0.0;
  int *a1 = (int*)fg->m;
  int *a2 = (int*)bg->m;
  for(int i=0; i<TOTAL_COLOR_BUCKETS; i++){
    //if(a1[i] <=0 || a2[i]<=0) Error("non-positive weight");
    u += a1[i]* (logs[fgtotal] - logs[a1[i]]);
    u += a2[i]* (logs[bgtotal] - logs[a2[i]]);
  }
  //printf("%lf ", u);
  return u/(fgtotal+bgtotal);
}
inline double computeU3(Appmodel *fg, Appmodel *bg, int fgtotal, int bgtotal,
    double *logs, int nlog){
  Error("This does not match computeU");
  double u = 0.0;
  int *a1 = (int*)fg->m;
  int *a2 = (int*)bg->m;
  double lfg = logs[fgtotal];
  double lbg = logs[bgtotal];
  for(int i=0; i<TOTAL_COLOR_BUCKETS; i++){
    //if(a1[i] <=0 || a2[i]<=0) Error("non-positive weight");
    u += a1[i]* (lfg - logs[a1[i]]);
    u += a2[i]* (lbg - logs[a2[i]]);
    /*
    if(a1[i] == APP_MODEL_PRIOR){
      if(a2[i] == APP_MODEL_PRIOR){
        //nothing
      } else {
        n++;
        div += (a1[i]*total2 - a2[i]*total1)*
          (logs[total2-a2[i]] - logs[a2[i]] +vl1);
      }
    } else{
      n++;
      if(a2[i] == APP_MODEL_PRIOR){
        div += (a1[i]*total2 - a2[i]*total1)*
          (logs[a1[i]] - logs[total1-a1[i]] + vl2);
      } else {
        div += (a1[i]*total2 - a2[i]*total1)*
          (logs[a1[i]] - logs[a2[i]] + logs[total2-a2[i]] - logs[total1-a1[i]]);
      }
    }
    */
  }
  //printf("%lf ", u);
  return u/(fgtotal+bgtotal);
}
inline double computeU4(int *a1, int *a2, int fgtotal, int bgtotal,
    double *logs, int nlog){
  Error("This does not match computeU");
  double u = 0.0;
  double lfg = logs[fgtotal];
  double lbg = logs[bgtotal];
  for(int i=0; i<TOTAL_COLOR_BUCKETS; i++){
    u += a1[i]* (lfg - logs[a1[i]]);
    u += a2[i]* (lbg - logs[a2[i]]);
  }
  return u/(fgtotal+bgtotal);
}
inline double computeU7(int *a1, int *a2, int fgtotal, int bgtotal,
    double *nlogn){
  double u1 = 0.0;
  double u2 = 0.0;
  for(int i=0; i<TOTAL_COLOR_BUCKETS; i++){
    //printf("(%d, %d) ", a1[i], a2[i]);
    u1 += nlogn[a1[i]];
    u2 += nlogn[a2[i]];
  }
  double u = -(u1/fgtotal + u2/bgtotal);
  //printf("%lf, %lf\n", log2(fgtotal), log2(bgtotal));
  u += log2(fgtotal)/fgtotal + log2(bgtotal)/bgtotal;
  return u;
  /*
  Error("This does not match computeU");
  Error("Dont use computeU7 until it's updated to use the new app model.");
  double u = 0.0;
  for(int i=0; i<TOTAL_COLOR_BUCKETS; i++){
    u += nlogn[a1[i]];
    u += nlogn[a2[i]];
  }
  u = -u;
  u += nlogn[fgtotal];
  u += nlogn[bgtotal];
  return u/(fgtotal+bgtotal);
  */
}
inline double computeULAB(int *a1, int *a2){
  double u1 = 0.0;
  double u2 = 0.0;
  int total1 = 0;
  int total2 = 0;
  for(int i=0; i<NUM_COLOR_BUCKETS; i++){
    total1 += a1[i];
    total2 += a2[i];
    //printf("(%d, %d) ", a1[i], a2[i]);
    u1 += a1[i]*log2(a1[i]);
    u2 += a2[i]*log2(a2[i]);
  }
  //printf("%lf, %lf\n", log2(fgtotal), log2(bgtotal));
  double u = (log2(total1)-u1)/total1 + (log2(total2)-u2)/total2;
  return u;
}
inline double computeULAB2(int *a1, int *a2, double *nlogn){
  double u1 = 0.0;
  double u2 = 0.0;
  int total1 = 0;
  int total2 = 0;
  for(int i=0; i<NUM_COLOR_BUCKETS; i++){
    total1 += a1[i];
    total2 += a2[i];
    //printf("(%d, %d) ", a1[i], a2[i]);
    u1 += nlogn[a1[i]];
    u2 += nlogn[a2[i]];
  }
  //printf("%lf, %lf\n", log2(fgtotal), log2(bgtotal));
  double u = (log2(total1)-u1)/total1 + (log2(total2)-u2)/total2;
  return u;
}

#endif
