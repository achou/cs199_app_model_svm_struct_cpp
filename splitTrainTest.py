import os, sys

for nhyps in [10, 20, 50, 100, 500]:
  os.system('rm -rf test'+str(nhyps)+'/binary/*')
  continue
  os.system('rm -rf test'+str(nhyps)+'/images')
  os.system('ln -s ../test10/images test'+str(nhyps)+'/images')
  os.system('cd ..')

sys.exit()
for nhyps in [10, 20, 50, 100, 500]:
  print nhyps
  os.system('mkdir -p test'+str(nhyps))
  os.system('mkdir -p test'+str(nhyps)+'/binary')
  os.system('mkdir -p test'+str(nhyps)+'/fgmaps')
  os.system('mkdir -p test'+str(nhyps)+'/groundtruth')
  os.system('mkdir -p test'+str(nhyps)+'/images')
  os.system('mkdir -p test'+str(nhyps)+'/pairwise')
  os.system('mkdir -p test'+str(nhyps)+'/singletons')
  for i in range(101,306):
    name = str(nhyps)+'/binary/ex_'+str(i)
    os.system('mv train'+name+' test'+name)
    name = str(nhyps)+'/binary/fgmap_'+str(i)
    os.system('mv train'+name+' test'+name)
    name = str(nhyps)+'/binary/im_'+str(i)
    os.system('mv train'+name+' test'+name)
    name = str(nhyps)+'/fgmaps/fgmap_'+str(i)+'.txt'
    os.system('mv train'+name+' test'+name)
    name = str(nhyps)+'/groundtruth/groundtruth_'+str(i)+'.txt'
    os.system('mv train'+name+' test'+name)
    name = str(nhyps)+'/images/image_'+str(i)+'.txt'
    os.system('mv train'+name+' test'+name)
    name = str(nhyps)+'/pairwise/pairwise_'+str(i)+'.txt'
    os.system('mv train'+name+' test'+name)
    name = str(nhyps)+'/singletons/singleton_'+str(i)+'.txt'
    os.system('mv train'+name+' test'+name)
