#!/bin/sh
#for i in 5.0 6.0 7.0 8.0 10.0 12.0 15.0 20.0 25.0 30.0 40.0 50.0 70.0 100.0 200.0 400.0 600.0 1000.0
#for i in 0.002 0.003 0.005 0.007 0.009 0.012 0.015 0.02 0.025 0.04 0.05 0.07 0.09 0.12 0.15 0.19 0.22 0.25 0.35 0.45
#for i in 0.001 0.01 0.03 0.1 0.2 0.3 0.4 0.5 0.7 0.85 1.0 1.3 1.5 1.8  2.0 3.0 4.0 5.0 6.0 7.0 8.0 10.0 12.0 15.0 20.0 25.0 30.0 40.0 50.0 70.0 100.0 200.0 400.0 600.0 1000.0
RANDOM=`date '+%s'`
#for c in 0.001 0.003 0.01 0.03 0.1 0.3 1.0 3.0 10.0 30.0 100.0 300.0 1000.0
for incb in 0 #1
do
  for c in 0.01 0.1 1.0
  do
    for b in 1 2 3 4 5 #6 7 8 9 10
    do
      for i in 50 #100
      do
        #x=$[($RANDOM % 9) + 1] # in the range [1,9]
        x=$[($RANDOM % 3) + 2] # in the range [2,4]
        #if [ "$x" -ge "6" ] # move 6,7 up to 7,8
        #then
        #  x=`expr ${x} + 1`
        #fi
        qsub -q daglab -v cval=$c,bval=$b,ival=$i,incbval=$incb learn20.script -l nodes=dag$x.stanford.edu
        #qsub -q daglab -v cval=$c,bval=$b,ival=$i learn20.script -l nodes=dag$x.stanford.edu
        #qsub -q daglab -v cval=$c,bval=$b,ival=$i learn20.script -l nodes=dag4.stanford.edu
        #qsub -q daglab learn20tmp.script -l nodes=dag$x.stanford.edu
      done
    done
  done
done
