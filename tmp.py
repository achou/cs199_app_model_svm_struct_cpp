def getvals(name, nlines):
  vals = []
  f=open(name, 'rt')
  n = 0
  for line in f:
    line = line.split(',')
    line = line[0]
    #print line
    n+=1
    vals.append(float(line))
    if n==nlines: break
  f.close()
  return vals
def better(optapp, curapp):
  optbetter = 0
  optsame = 0
  optworse = 0
  for i in range(0,len(optapp)):
    if optapp[i] < curapp[i]: optbetter+=1
    elif optapp[i] == curapp[i]: optsame+=1
    else: optworse+=1
  print optbetter, optsame, optworse
def getAllVals(name, nlines):
  vals = []
  f=open(name, 'rt')
  n=0
  for line in f:
    line = line.split()
    line = [float(x) for x in line]
    n+=1
    vals.append(line)
    if n==nlines: break
  f.close()
  return vals
def betterAll(optapp, curapp):
  print 'XXXXXXXXXX'
  for j in range(0,41):
    optbetter = 0
    optsame = 0
    optworse = 0
    for i in range(0,len(optapp)):
      #print optapp[i][j], curapp[i][j]
      if optapp[i][j] < curapp[i][j]: optbetter+=1
      elif optapp[i][j] == curapp[i][j]: optsame+=1
      else: optworse+=1
    print optbetter, optsame, optworse
def better(optapp, curapp):
  optbetter = 0
  optsame = 0
  optworse = 0
  for i in range(0,len(optapp)):
    if optapp[i] < curapp[i]: optbetter+=1
    elif optapp[i] == curapp[i]: optsame+=1
    else: optworse+=1
  print optbetter, optsame, optworse

# app
#not normalized
optapp = getvals('d.txt', 205)
curapp = getvals('e.txt', 205)
better(optapp, curapp)
#normalized
optapp = getvals('j.txt', 205)
curapp = getvals('k.txt', 205)
better(optapp, curapp)

#pairwise test
optapp = getvals('l.txt', 205)
curapp = getvals('m.txt', 205)
print sum(optapp), sum(curapp)
better(optapp, curapp)
#pairwise train
optapp = getvals('n.txt', 100)
curapp = getvals('o.txt', 100)
print sum(optapp), sum(curapp)
better(optapp, curapp)

# all
#test
optapp = getAllVals('p.txt', 205)
curapp = getAllVals('q.txt', 205)
betterAll(optapp, curapp)
#training
optapp = getAllVals('r.txt', 100)
curapp = getAllVals('s.txt', 100)
betterAll(optapp, curapp)

# all except pairwise (including symmetry)
# NUM_COLOR_BUCKETS = 16
#training with SUBPART_LEVELS = 3
optapp = getAllVals('t3.txt', 100)
curapp = getAllVals('u3.txt', 100)
betterAll(optapp, curapp)
#test with SUBPART_LEVELS = 3
optapp = getAllVals('v3.txt', 205)
curapp = getAllVals('w3.txt', 205)
betterAll(optapp, curapp)
#training with SUBPART_LEVELS = 1
optapp = getAllVals('t1.txt', 100)
curapp = getAllVals('u1.txt', 100)
betterAll(optapp, curapp)
#test with SUBPART_LEVELS = 1
optapp = getAllVals('v1.txt', 205)
curapp = getAllVals('w1.txt', 205)
betterAll(optapp, curapp)

# NUM_COLOR_BUCKETS = 8
#training with SUBPART_LEVELS = 1
optapp = getAllVals('x1.txt', 100)
curapp = getAllVals('y1.txt', 100)
betterAll(optapp, curapp)
#test with SUBPART_LEVELS = 1
optapp = getAllVals('z1.txt', 205)
curapp = getAllVals('aa1.txt', 205)
betterAll(optapp, curapp)
#training with SUBPART_LEVELS = 3
optapp = getAllVals('x3.txt', 100)
curapp = getAllVals('y3.txt', 100)
betterAll(optapp, curapp)
#test with SUBPART_LEVELS = 3
optapp = getAllVals('z3.txt', 205)
curapp = getAllVals('aa3.txt', 205)
betterAll(optapp, curapp)

# texture NUM_COLOR_BUCKETS = 16
# with SUBPART_LEVELS = 3
# train
optapp = getAllVals('ad.txt', 100)
curapp = getAllVals('ab.txt', 100)
betterAll(optapp, curapp)
#test
optapp = getAllVals('ae.txt', 205)
curapp = getAllVals('ac.txt', 205)
betterAll(optapp, curapp)
