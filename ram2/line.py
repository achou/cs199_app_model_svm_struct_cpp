#!/usr/bin/python
from scipy import linspace, polyval, polyfit, sqrt, stats, randn
import os,sys, subprocess
from subprocess import Popen, PIPE, STDOUT
from math import sqrt
def run(alg, k, every, w, seen, train = True):
  tw = tuple(w)
  if tw in seen:
    print 'CACHED!'
    return seen[tw]
  argv = [alg,k,every]+w
  args = ' '.join([str(v) for v in argv])
  if train:
    cmd = './runtrain.py '+args
  else:
    cmd = './test.py '+args
  val = None
  popen = subprocess.Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, close_fds=True)
  for line in iter(popen.stdout.readline, ""):
    #print line.strip('\n')
    if line.startswith('Average PCP on test set:'):
      val = float(line.split()[-1])
  popen.wait()
  if val == None:
    print 'no valid lines for command:'
    print cmd
    sys.exit()
  seen[tw] = val
  return val
def geomean(l):
  small = min(l)
  big = max(l)
  if small*big < 0:
    return (small + big) / 2.0
  else:
    if small < 0:
      return -sqrt(small*big)
    return sqrt(small*big)
if __name__ == "__main__":
  k= 0
  #every= 50
  #NSING = 10
  NSING = 1
  NPAIR = 1
  #w = (NSING+NPAIR)*[-100.0] + [-1.0]
  #w = [-50.0, -100.0, -2.0]
  #w = [-1.0, -1.0, 0.0, 0.0]
  #w = [-53.4506704981, -81.6496580928, -3.04273114355]
  w = [-83.3, -144.0, -0.83]
  if len(sys.argv) != 3:
    print 'Usage: python line.py <every> <alg: 0=EM, 1-4 = BCD>'
    sys.exit()
  every= int(sys.argv[1])
  alg= int(sys.argv[2])
  #
  seen = {}
  print w
  prevval = run(alg, k, every, w, seen, True)
  print 'orig:', prevval
  j = 0
  j = 1 #TODO
  f = 1.5
  while f > 1.05:
    start = 0
    for i in range(start,len(w)):
      toTry = [w[i]/f, w[i], w[i]*f]
      print toTry
      vals = []
      bestval = -float('Inf')
      bestx = []
      for x in toTry:
        w[i] = x
        val = run(alg, k, every, w, seen, True)
        vals.append(val)
        if val > bestval:
          bestval = val
          print 'bestval:', bestval, 'j:', j, 'i:', i, 'w:', w
          bestx = [w[i]]
        elif val == bestval:
          print 'TIE bestval:', bestval, 'j:', j, 'i:', i, 'w:', w
          bestx.append(w[i])
        else:
          print 'val:    ', val, 'j:', j, 'i:', i, 'w:', w
        k += 1
      (ar,br,cr) = polyfit(toTry, vals, 2) #quadratic
      print 'COEFS:', ar, br, cr
      if len(bestx) == 3:
        w[i] = geomean(bestx)
        continue
      if ar >= 0.0:
        w[i] = geomean(bestx)
        continue
      w[i] = -br / (2.0*ar)
      val = run(alg, k, every, w, seen, True)
      print 'val:    ', val, 'j:', j, 'i:', i, 'w:', w
      if val < bestval:
        w[i] = geomean(bestx)
    print bestval, w
    if bestval == prevval:
      j += 1
    else:
      prevval = bestval
    f *= 0.9
  print ' '.join([str(x) for x in w])
  print 'TRAIN:', bestval
  testval = run(alg, k, 1, w, {}, False)
  print 'TEST: ', testval
