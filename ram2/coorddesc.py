#!/usr/bin/python
import os,sys, subprocess
from subprocess import Popen, PIPE, STDOUT
from math import sqrt
def run(alg, k, every, w, seen, train = True):
  tw = tuple(w)
  if tw in seen:
    print 'CACHED!'
    return seen[tw]
  argv = [alg,k,every]+w
  args = ' '.join([str(v) for v in argv])
  if train:
    cmd = './runtrain.py '+args
  else:
    cmd = './test.py '+args
  val = None
  popen = subprocess.Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, close_fds=True)
  for line in iter(popen.stdout.readline, ""):
    #print line.strip('\n')
    if line.startswith('Average PCP on test set:'):
      val = float(line.split()[-1])
  popen.wait()
  if val == None:
    print 'no valid lines for command:'
    print cmd
    sys.exit()
  seen[tw] = val
  return val
def readResult(k):
  name = 'weights'+str(k)
  assert(not name.endswith('/'))
  inf = open(name+'/results.txt')
  lines = [x for x in inf]
  inf.close()
  line = lines[-1]
  val = float(line.split()[-1])
  return val
def getValsToTry(w, j, allowSignSwitch = True):
  minval = 2e-5
  if abs(w) < 2e-5:
    w = 1.0
  n0 = -3
  n1 = 4
  #n0 = -4
  #n1 = 5
  if j == 0:
    toTry = [w*pow(10,x) for x in range(-3,3)]
  elif j == 1:
    toTry = [w*pow(2,x) for x in range(n0,n1)]
  elif j == 2:
    toTry = [w*pow(1.2,x) for x in range(n0,n1)]
  elif j == 3:
    toTry = [w*pow(1.05,x) for x in range(n0,n1)]
  else:
    print 'bad j:', j
    sys.exit()
  if allowSignSwitch:
    toTry += [-x for x in toTry]
  vals = []
  for v in toTry:
    if v > 0.0:
      vals.append(max(v,minval))
    else:
      vals.append(min(v, -minval))
  return vals

if __name__ == "__main__":
  k= 0
  #every= 50
  #NSING = 10
  NSING = 1
  NPAIR = 1
  w = (NSING+NPAIR)*[-100.0] + [-1.0]
  #w = [-1.0, -1.0, 0.0, 0.0]
  if len(sys.argv) != 3:
    print 'Usage: python coorddesc.py <every> <alg: 0=EM, 1-4 = BCD>'
    sys.exit()
  every= int(sys.argv[1])
  alg= int(sys.argv[2])
  #
  #readResult(i)
  seen = {}
  prevval = run(alg, k, every, w, seen, True)
  print w
  j = 0
  j = 1 #TODO
  while j < 4:
    start = 0
    for i in range(start,len(w)):
      if i < (NSING+NPAIR):
        toTry = getValsToTry(w[i], j, False)
      else:
        toTry = getValsToTry(w[i], j, True)
      print toTry
      bestval = -float('Inf')
      bestx = []
      for x in toTry:
        w[i] = x
        val = run(alg, k, every, w, seen, True)
        if val > bestval:
          bestval = val
          print 'bestval:', bestval, 'j:', j, 'i:', i, 'w:', w
          bestx = [w[i]]
        elif val == bestval:
          print 'TIE bestval:', bestval, 'j:', j, 'i:', i, 'w:', w
          bestx.append(w[i])
        k += 1
      assert(bool(bestx))
      small = min(bestx)
      big = max(bestx)
      if small*big < 0:
        w[i] = (small + big) / 2.0
      else:
        w[i] = sqrt(small*big)
        if small < 0:
          w[i] = -w[i]
    print bestval, w
    if bestval == prevval:
      j += 1
    else:
      prevval = bestval
  print ' '.join([str(x) for x in w])
  print 'TRAIN:', bestval
  testval = run(alg, k, every, w, {}, False)
  print 'TEST: ', testval
