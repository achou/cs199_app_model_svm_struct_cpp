from scipy import linspace, polyval, polyfit, sqrt, stats, randn

#Linear regression example
# This is a very simple example of using two scipy tools
# for linear regression, polyfit and stats.linregress

#Sample data creation
#number of points
n=50
t=linspace(-5,5,n)
#parameters
a=0.8; b=-4; c=2
x=polyval([a,b,c],t)
#add some noise
xn=x+randn(n)

#Linear regressison -polyfit - polyfit can be used other orders polys
(ar,br,cr)=polyfit(t,xn,2)
xr=polyval([ar,br,cr],t)
#compute the mean square error
err=sqrt(sum((xr-xn)**2)/n)
print err
print ar, br, cr
print a, b, c
