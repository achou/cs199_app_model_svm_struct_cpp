#!/usr/bin/python
import os,sys
from runtrain import createLogDir, writeWeights, readArgs, classify

if __name__ == "__main__":
  alg,iter,every,w = readArgs(sys.argv)
  logprefix = createLogDir(iter, alg)
  weightname = writeWeights(iter, w)
  classify('test', every, logprefix, weightname, alg)
