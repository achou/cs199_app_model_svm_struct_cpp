#!/usr/bin/python

import os,sys
def writeWeights(iter, w):
  weightname = 'weights'+str(iter)
  os.system('mkdir -p '+weightname)
  assert(not weightname.endswith('/'))
  outf = open(weightname+'/w_c_100_constraint00.txt', 'w')
  if len(w) == 3:
    for i in range(10):
      outf.write(str(w[0])+'\n')
    for i in range(1):
      outf.write(str(w[1])+'\n')
    for i in range(4):
      outf.write(str(w[2])+'\n')
    for i in range(1):
      outf.write(str(0)+'\n')
  else:
    print 'wrong w len'
    outf.close()
    sys.exit()
  outf.close()
  return weightname
def createLogDir(iter, alg):
  logprefix = 'logs/trial'+str(iter)+'_'
  cmd = 'mkdir -p '+logprefix+'20n_'+str(alg)
  print cmd
  os.system(cmd);
  return logprefix
def readArgs(argv):
  if len(argv) == 1:
    alg = 0
    iter= 0
    every= 50
    w = 11*[-1.0] + [0.0, 0.0]
  elif len(argv) in [7]:
    alg = int(argv[1])
    iter= int(argv[2])
    every= int(argv[3])
    w = [float(x) for x in argv[4:]]
  else:
    print 'Usage: python runtrain.py <alg> <i> <every> <w[0]> <w[1]> <w[2]>'
    sys.exit()
  return alg,iter,every,w
def classify(testset, every, logprefix, weightname, alg):
  exe = './classify'
  #exe = './classifyHids1'
  #exe = './classifyNonB'
  if alg == 0:
    config = 'constraint_configEM'
  elif alg in [1,2,3,4]:
    config = 'constraint_configB'+str(alg)
  else:
    print 'Invalid alg:', alg
    sys.exit()
  if every > 1:
    cmd = exe+' --c 0.01 --x '+config+' --l '+logprefix+' ../'+testset+str(every)+' '+weightname+' '+weightname+'/out.txt'
  else:
    cmd = exe+' --c 0.01 --x '+config+' --l '+logprefix+' ../'+testset+' '+weightname+' '+weightname+'/out.txt'
  print cmd
  os.system(cmd)

################################################################
################################################################
################################################################
if __name__ == "__main__":
  alg,iter,every,w = readArgs(sys.argv)
  logprefix = createLogDir(iter, alg)
  weightname = writeWeights(iter, w)
  classify('train', every, logprefix, weightname, alg)
