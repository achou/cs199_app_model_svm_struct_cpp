% mix in hc to the new ones
nv1=zeros(2,1); %naive
nv2=zeros(102,1); %naive
nv3=zeros(102,1); %naive
nv4=zeros(102,1); %naive
nv5=zeros(102,1); %naive
nv6=zeros(102,1); %naive
nv7=zeros(102,1); %naive
nv8=zeros(102,1); %naive
nv9=zeros(102,1); %naive
%nv10=zeros(102,1); %naive
nv1_20=zeros(2,1); %naive
nv2_20=zeros(102,1); %naive
nv3_20=zeros(102,1); %naive
nv4_20=zeros(102,1); %naive
nv5_20=zeros(102,1); %naive
nv6_20=zeros(102,1); %naive
nv7_20=zeros(102,1); %naive
%nv8_20=zeros(102,1); %naive
%nv9_20=zeros(102,1); %naive
%nv10_20=zeros(102,1); %naive
v1_20=zeros(2,2); %20 hyps
v2_20=zeros(102,2);
v3_20=zeros(102,1);
v4_20=zeros(102,2);
v5_20=zeros(102,1);
v6_20=zeros(102,2);
v1=zeros(2,1);
v2=zeros(102,1);
v3=zeros(102,1);
v4=zeros(102,1);
v5=zeros(102,1);
v6=zeros(102,1);
v7=zeros(102,1);
v8=zeros(102,1);
v9=zeros(102,1);
%v10=zeros(102,1);
v1_50=zeros(2,2); %50 hyps
v2_50=zeros(102,2);
v3_50=zeros(102,2);
v4_50=zeros(102,2);
v1_100=zeros(2,2); %100 hyps
v2_100=zeros(102,2);
for i=101:305
  name = ['naiveinitlogs10/logs10n_1/log_', int2str(i), '.txt'];
  nv1 = nv1 + load(name);
  name = ['naiveinitlogs10/logs10n_2/log_', int2str(i), '.txt'];
  nv2 = nv2 + load(name);
  name = ['naiveinitlogs10/logs10n_3/log_', int2str(i), '.txt'];
  nv3 = nv3 + load(name);
  name = ['naiveinitlogs10/logs10n_4/log_', int2str(i), '.txt'];
  nv4 = nv4 + load(name);
  name = ['naiveinitlogs10/logs10n_5/log_', int2str(i), '.txt'];
  nv5 = nv5 + load(name);
  name = ['naiveinitlogs10/logs10n_6/log_', int2str(i), '.txt'];
  nv6 = nv6 + load(name);
  name = ['naiveinitlogs10/logs10n_7/log_', int2str(i), '.txt'];
  nv7 = nv7 + load(name);
  name = ['naiveinitlogs10/logs10n_8/log_', int2str(i), '.txt'];
  nv8 = nv8 + load(name);
  name = ['naiveinitlogs10/logs10n_9/log_', int2str(i), '.txt'];
  nv9 = nv9 + load(name);
  name = ['naiveinitlogs20/logs1/log_', int2str(i), '.txt'];
  nv1_20 = nv1_20 + load(name);
  name = ['naiveinitlogs20/logs2/log_', int2str(i), '.txt'];
  nv2_20 = nv2_20 + load(name);
  name = ['naiveinitlogs20/logs3/log_', int2str(i), '.txt'];
  nv3_20 = nv3_20 + load(name);
  name = ['naiveinitlogs20/logs4/log_', int2str(i), '.txt'];
  nv4_20 = nv4_20 + load(name);
  name = ['naiveinitlogs20/logs5/log_', int2str(i), '.txt'];
  nv5_20 = nv5_20 + load(name);
  name = ['naiveinitlogs20/logs6/log_', int2str(i), '.txt'];
  nv6_20 = nv6_20 + load(name);
  name = ['naiveinitlogs20/logs7/log_', int2str(i), '.txt'];
  nv7_20 = nv7_20 + load(name);
  name = ['eminitlogs20/logs20_1/log_', int2str(i), '.txt'];
  v1_20 = v1_20 + load(name);
  name = ['eminitlogs20/logs20_2/log_', int2str(i), '.txt'];
  v2_20 = v2_20 + load(name);
  name = ['eminitlogs20/logs20_3/log_', int2str(i), '.txt'];
  v3_20 = v3_20 + load(name);
  name = ['eminitlogs20/logs20_4/log_', int2str(i), '.txt'];
  v4_20 = v4_20 + load(name);
  name = ['eminitlogs20/logs20_5/log_', int2str(i), '.txt'];
  v5_20 = v5_20 + load(name);
  name = ['eminitlogs20/logs20_6/log_', int2str(i), '.txt'];
  v6_20 = v6_20 + load(name);
  name = ['eminitlogs10/', 'logs10_1/log_', int2str(i), '.txt'];
  v1 = v1+ load(name);
  name = ['eminitlogs10/', 'logs10_2/log_', int2str(i), '.txt'];
  v2 = v2 + load(name);
  name = ['eminitlogs10/', 'logs10_3/log_', int2str(i), '.txt'];
  v3 = v3 + load(name);
  name = ['eminitlogs10/', 'logs10_4/log_', int2str(i), '.txt'];
  v4 = v4 + load(name);
  name = ['eminitlogs10/', 'logs10_5/log_', int2str(i), '.txt'];
  v5 = v5 + load(name);
  name = ['eminitlogs10/', 'logs10_6/log_', int2str(i), '.txt'];
  v6 = v6 + load(name);
  name = ['eminitlogs10/', 'logs10_7/log_', int2str(i), '.txt'];
  v7 = v7 + load(name);
  name = ['eminitlogs10/', 'logs10_8/log_', int2str(i), '.txt'];
  v8 = v8 + load(name);
  name = ['eminitlogs10/', 'logs10_9/log_', int2str(i), '.txt'];
  v9 = v9 + load(name);
  %name = ['eminitlogs10/', 'logs10_10/log_', int2str(i), '.txt'];
  %v10 = v10 + load(name);
  name = ['eminitlogs50/logs50_1/log_', int2str(i), '.txt'];
  v1_50 = v1_50 + load(name);
  name = ['eminitlogs50/logs50_2/log_', int2str(i), '.txt'];
  v2_50 = v2_50 + load(name);
  name = ['eminitlogs50/logs50_3/log_', int2str(i), '.txt'];
  v3_50 = v3_50 + load(name);
  name = ['eminitlogs50/logs50_4/log_', int2str(i), '.txt'];
  v4_50 = v4_50 + load(name);
  name = ['eminitlogs100/logs100_1/log_', int2str(i), '.txt'];
  v1_100 = v1_100 + load(name);
  name = ['eminitlogs100/logs100_2/log_', int2str(i), '.txt'];
  v2_100 = v2_100 + load(name);
end
v1_20 = v1_20(:,1);
v2_20 = v2_20(:,1);
v3_20 = v3_20(:,1);
v4_20 = v4_20(:,1);
v5_20 = v5_20(:,1);
v6_20 = v6_20(:,1);
%v1_50 = v1_50(:,1);
%v2_50 = v2_50(:,1);
%v3_50 = v3_50(:,1);
%v4_50 = v4_50(:,1);
len = length(v2);
x = 1:len;
nv1 = [nv1; nv1(end)*ones(len-length(nv1),1)];
nv1_20 = [nv1_20; nv1_20(end)*ones(len-length(nv1_20),1)];
v1 = [v1; v1(end)*ones(len-length(v1),1)];
v1_20 = [v1_20; v1_20(end)*ones(len-length(v1_20),1)];
v1_50 = [v1_50; ones(len-length(v1_50),1)*v1_50(end,:)];
v1_100 = [v1_100; ones(len-length(v1_100),1)*v1_100(end,:)];
baseline = v1(1)*ones(102,1);
ph=plot(x, baseline, x, v1, x, v2, x, v3, x, v4, x, v5, x, v6, x, v7);
set(ph, 'LineWidth', 3)
legend('Baseline','Blocksize 1, 10 Hyps','Blocksize 2, 10 Hyps','Blocksize 3, 10 Hyps','Blocksize 4, 10 Hyps','Blocksize 5, 10 Hyps','Blocksize 6, 10 Hyps','Blocksize 7, 10 Hyps',...
  'Location','SouthEast');
xlabel('Iteration', 'FontWeight','bold');
ylabel('Objective Function Sum over 205 Images (Higher is better)', 'FontWeight','bold');
title('Inference Methods for Optimizing the Objective (10 Hypotheses)', 'FontWeight','bold');
figure
ph=plot(x, baseline, x, v1_20, x, v2_20, x, v3_20, x, v4_20, x, v5_20);
set(ph, 'LineWidth', 3)
legend('Baseline','Blocksize 1, 20 Hyps','Blocksize 2, 20 Hyps',...
  'Blocksize 3, 20 Hyps','Blocksize 4, 20 Hyps','Blocksize 5, 20 Hyps','Location','SouthEast');
xlabel('Iteration', 'FontWeight','bold');
ylabel('Objective Function Sum over 205 Images (Higher is better)', 'FontWeight','bold');
title('Inference Methods for Optimizing the Objective (20 Hypotheses)', 'FontWeight','bold');
return

plot( x, v1, x, v3, x, nv1, x, nv3, x, v1_20, x, v3_20, x, nv1_20, x, nv3_20,...
  x, v1_50(:,1), x, v3_50(:,1), x, v1_100(:,1), x, v2_100(:,1));
  %x, v1, x, v2, x, v3, x, v4, x, v5, x, v6, x, v7, x, v8, x, v9);
  %x, nv1, x, nv2, x, nv3, x, nv4, x, nv5, x, nv6, x, nv7, x, nv8, x, nv9,...
  %x, nv1_20, x, nv2_20, x, nv3_20, x, nv4_20, x, nv5_20, x, nv6_20, x, nv7_20...
  %);
legend(...
  'hillclimbing, 10 hyps, EM start', 'block size 3, 10 hyps, EM start',...
  'hillclimbing, 10 hyps, naive start', 'blocksize 3, 10 hyps, naive start',...
  'hillclimbing, 20 hyps, EM start', 'block size 3, 20 hyps, EM start',...
  'hillclimbing, 20 hyps, naive start', 'blocksize 3, 20 hyps, naive start',...
  'hillclimbing, 50 hyps, EM start', 'block size 3, 50 hyps, EM start',...
  'hillclimbing, 100 hyps, EM start', 'block size 2, 100 hyps, EM start',...
  'Location','SouthEast');
xlabel('iterations');
ylabel('total energy over all images');
figure
plot( v1_50(:,2), v1_50(:,1), v2_50(:,2), v2_50(:,1), ...
  v3_50(:,2), v3_50(:,1), v4_50(:,2), v4_50(:,1));
legend(...
  'hillclimbing, 50 hyps, EM start',...
  'blocksize 2, 50 hyps, EM start',...
  'blocksize 3, 50 hyps, EM start',...
  'blocksize 4, 50 hyps, EM start',...
  'Location','SouthEast');
xlabel('micro seconds');
ylabel('total energy over all images');
  %'v1','v2','v3','v4','v5','v6','v7','v8','v9',...
  %'nv1','nv2','nv3','nv4','nv5','nv6','nv7','nv8','nv9',...
  %'nv1_20','nv2_20','nv3_20','nv4_20','nv5_20','nv6_20','nv7_20',...
%{
plot(x, v1, x, v2, x, v3, x, v4, x, v5, x, v6, x, v7, x, v8, x, v9, x, v10,...
  x, nv1, x, nv2, x, nv3, x, nv4, x, nv5, x, nv6, x, nv7, x, nv8,...
  x, v1_20, x, v2_20, x, v3_20, x, v4_20, x, v5_20);
legend('hc','2','3','4','5','6','7','8','9','10',...
  'nv1','nv2','nv3','nv4','nv5','nv6','nv7','nv8',...
  '1_20','2_20','3_20','4_20','5_20','Location','SouthEast');
  %}
%plot(x, v1, x, v2, x, v3, x, v4, x, v5, x, v6, x, v7, x, v8, ...
%  x, nv1, x, nv2, x, nv3, x, nv4);
%legend('hc','2','3','4','5','6','7','8','nv1','nv2','nv3','nv4','Location','SouthEast');
fprintf('EM 1:  %f\n', v1(end))
fprintf('EM 2:  %f\n', v2(end))
fprintf('EM 3:  %f\n', v3(end))
fprintf('EM 4:  %f\n', v4(end))
fprintf('EM 5:  %f\n', v5(end))
fprintf('EM 6:  %f\n', v6(end))
fprintf('EM 7:  %f\n', v7(end))
fprintf('EM 8:  %f\n', v8(end))
fprintf('EM 9:  %f\n', v9(end))
%fprintf('EM 10: %f\n', v10(end))
fprintf('naive 1:  %f\n', nv1(end))
fprintf('naive 2:  %f\n', nv2(end))
fprintf('naive 3:  %f\n', nv3(end))
fprintf('naive 4:  %f\n', nv4(end))
fprintf('naive 5:  %f\n', nv5(end))
fprintf('naive 6:  %f\n', nv6(end))
fprintf('naive 7:  %f\n', nv7(end))
%fprintf('naive 8:  %f\n', nv8(end))
%fprintf('naive 9:  %f\n', nv9(end))
%fprintf('naive 10: %f\n', nv10(end))
fprintf('naive 1_20:  %f\n', nv1_20(end))
fprintf('naive 2_20:  %f\n', nv2_20(end))
fprintf('naive 3_20:  %f\n', nv3_20(end))
fprintf('naive 4_20:  %f\n', nv4_20(end))
fprintf('naive 5_20:  %f\n', nv5_20(end))
fprintf('naive 6_20:  %f\n', nv6_20(end))
fprintf('naive 7_20:  %f\n', nv7_20(end))
%fprintf('EM 1_20:  %f\n', v1_20(end))
%fprintf('EM 2_20:  %f\n', v2_20(end))
%fprintf('EM 3_20:  %f\n', v3_20(end))
%fprintf('EM 4_20:  %f\n', v4_20(end))
%fprintf('EM 5_20:  %f\n', v5_20(end))
%{
fprintf('EM 6_20:  %f\n', v6_20(end))
fprintf('EM 7_20:  %f\n', v7_20(end))
fprintf('EM 8_20:  %f\n', v8_20(end))
fprintf('EM 9_20:  %f\n', v9_20(end))
fprintf('EM 10_20: %f\n', v10_20(end))
%}
fprintf('EM 1_50:  %f\n', v1_50(end,1))
fprintf('EM 2_50:  %f\n', v2_50(end,1))
fprintf('EM 3_50:  %f\n', v3_50(end,1))
fprintf('EM 4_50:  %f\n', v4_50(end,1))
fprintf('EM 1_100:  %f\n', v1_100(end,1))
fprintf('EM 2_100:  %f\n', v2_100(end,1))
return
% plot all together
index = 101;
for i=index:index+19
  name = ['logs1/log_', int2str(i), '.txt'];
  v1 = load(name);
  name = ['logs2/log_', int2str(i), '.txt'];
  v2 = load(name);
  name = ['logs3/log_', int2str(i), '.txt'];
  v3 = load(name);
  name = ['logs4/log_', int2str(i), '.txt'];
  v4 = load(name);
  name = ['logs5/log_', int2str(i), '.txt'];
  v5 = load(name);
  name = ['logs6/log_', int2str(i), '.txt'];
  v6 = load(name);
  name = ['logs7/log_', int2str(i), '.txt'];
  v7 = load(name);
  len = length(v2);
  x = 1:len;
  v1 = [v1; v1(end)*ones(len-length(v1),1)];
  subplot(5,4,i-index+1);
  plot(x, v1, x, v2, x, v3, x, v4, x, v5, x, v6, x, v7);
  legend('hc','2','3','4','5','6','7','Location','SouthEast');
end
return
% plot all together
index = 101;
for i=index:index+19
  name = ['logs1/log_', int2str(i), '.txt'];
  v1 = load(name);
  name = ['logs2/log_', int2str(i), '.txt'];
  v2 = load(name);
  name = ['logs3/log_', int2str(i), '.txt'];
  v3 = load(name);
  name = ['logs4/log_', int2str(i), '.txt'];
  v4 = load(name);
  name = ['logs5/log_', int2str(i), '.txt'];
  v5 = load(name);
  len = length(v2);
  x = 1:len;
  v1 = [v1; v1(end)*ones(len-length(v1),1)];
  subplot(5,4,i-index+1);
  plot(x, v1, x, v2, x, v3, x, v4, x, v5);
  legend('hc','2','3','4','5','Location','SouthEast');
end
return
index = 116;
for i=index:index+4
  name = ['logs2/log_', int2str(i), '.txt'];
  v2 = load(name);
  name = ['logs3/log_', int2str(i), '.txt'];
  v3 = load(name);
  name = ['logs4/log_', int2str(i), '.txt'];
  v4 = load(name);
  name = ['logs5/log_', int2str(i), '.txt'];
  v5 = load(name);
  subplot(5,4,1+(i-index)*4);
  plot(1:length(v2), v2);
  subplot(5,4,2+(i-index)*4);
  plot(1:length(v3), v3);
  subplot(5,4,3+(i-index)*4);
  plot(1:length(v4), v4);
  subplot(5,4,4+(i-index)*4);
  plot(1:length(v5), v5);
end
return
for i=101:115
  name = ['logs2/log_', int2str(i), '.txt'];
  v = load(name);
  subplot(3,5,i-100);
  plot(1:length(v), v);
end
