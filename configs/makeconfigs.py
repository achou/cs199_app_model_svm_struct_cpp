import os
for incblock in range(0,2):
  for blocksize in range(0,11):
    print blocksize
    for iters in [0, 1, 5, 10, 20, 30, 50, 100]:
      for samplerandfeatures in [0,1]:
        for samplelearningfeatures in [0,1]:
          filename = ''.join(['c_b_', str(blocksize), '_i_', str(iters), '_incb_', str(incblock)])
          filename += '_samp_' + str(samplerandfeatures) + str(samplelearningfeatures)
          print filename
          f = open(filename, 'w')
          f.write('16     num_color_buckets\n')
          f.write('40     EM_iters\n')
          f.write('0      use_app_model_blur\n')
          f.write('8      app_model_blur_inv\n')
          f.write('1      app_model_prior\n')
          f.write('0      use_symmetry\n')
          f.write('0      approx_inference\n')
          f.write('10     approx_inference_iter\n')
          f.write('0      hillclimb\n')
          f.write('0      plot_inference\n')
          if blocksize==0:
            f.write('1      inference_method_1EM_2ABCD\n')
          else:
            f.write('2      inference_method_1EM_2ABCD\n')
          f.write(str(blocksize)+'      abcd_blocksize\n')
          f.write(str(iters)+'     abcd_iters\n')
          f.write('1000   max_abcd_inference_time_for_1_iteration\n')
          f.write('1      write_logs\n')
          f.write(str(incblock)+'      include_block_in_app\n')
          f.write('0      naive_init\n')
          f.write(str(samplerandfeatures)+'      sample_random_features\n')
          f.write(str(samplelearningfeatures)+'      sample_learning_features\n')
          f.close()
