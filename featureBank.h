#ifndef FEATURE_BANK_H
#define FEATURE_BANK_H
#include "svm_struct_api_types.h"
#include "svm_struct/svm_struct_common.h"
#include <vector>
using namespace std;

void storeFeature(SVECTOR *sv, double loss);
void applyFeatureBank();
void saveFeatureBank(string outfile);
void loadFeatureBank(string infile);
vector<double> getLastMeans();
vector<double> getSdevinv();
#endif

