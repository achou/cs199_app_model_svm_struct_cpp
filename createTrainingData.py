import os
first = 1
last = 100
HS = [500, 100, 50, 20, 10]
for hi in range(len(HS)-1):
  ORIGH = HS[hi]
  NEWH  = HS[hi+1]
  print ORIGH, NEWH
  # build the directory structure
  os.system('mkdir -p train'+str(NEWH)+'/binary')
  os.system('mkdir -p train'+str(NEWH)+'/fgmaps')
  os.system('mkdir -p train'+str(NEWH)+'/groundtruth')
  #os.system('mkdir -p train'+str(NEWH)+'/images')
  os.system('mkdir -p train'+str(NEWH)+'/pairwise')
  os.system('mkdir -p train'+str(NEWH)+'/singletons')
  # singletons
  print 'singletons'
  for i in range(first,last+1):
    print i
    fout = open('train'+str(NEWH)+'/singletons/singleton_'+str(i)+'.txt', 'w')
    f = open('train'+str(ORIGH)+'/singletons/singleton_'+str(i)+'.txt', 'r')
    for line in f:
      data = line.split()
      data = data[0:NEWH]
      data = ' '.join(data)
      fout.write(data+'\n')
    f.close()
    fout.close()
  # pairwise
  print 'pairwise'
  npairs = 9
  for i in range(first,last+1):
    print i
    fout = open('train'+str(NEWH)+'/pairwise/pairwise_'+str(i)+'.txt', 'w')
    f = open('train'+str(ORIGH)+'/pairwise/pairwise_'+str(i)+'.txt', 'r')
    alllines = f.readlines()
    for j in range(0,npairs):
      lines = alllines[j*ORIGH:j*ORIGH+NEWH]
      lines = lines[0:NEWH]
      for line in lines:
        data = line.split()
        data = data[0:NEWH]
        data = ' '.join(data)
        fout.write(data+'\n')
    f.close()
    fout.close()
  # images
  #print 'images'
  #for i in range(first,last+1):
  #  os.system('cp train'+str(ORIGH)+'/images/image_'+str(i)+'.txt train'+str(NEWH)+'/images/image_'+str(i)+'.txt')
  # groundtruth
  print 'groundtruth'
  for i in range(first,last+1):
    print i
    fout = open('train'+str(NEWH)+'/groundtruth/groundtruth_'+str(i)+'.txt', 'w')
    f = open('train'+str(ORIGH)+'/groundtruth/groundtruth_'+str(i)+'.txt', 'r')
    for line in f:
      data = line.split()
      data = data[0:NEWH]
      data = ' '.join(data)
      fout.write(data+'\n')
    f.close()
    fout.close()
  # fgmap
  print 'fgmap'
  for i in range(first,last+1):
    print i
    fout = open('train'+str(NEWH)+'/fgmaps/fgmap_'+str(i)+'.txt', 'w')
    f = open('train'+str(ORIGH)+'/fgmaps/fgmap_'+str(i)+'.txt', 'r')
    f.readline()
    nlines = 0
    for line in f:
      data = line.split()
      data = [int(x) for x in data]
      if data[1] > NEWH:
        continue
      fout.write(line)
      nlines += 1
    f.close()
    fout.close()
    # rewrite with the number of pixel mappings at the beginning
    fout = open('train'+str(NEWH)+'/fgmaps/fgmap_'+str(i)+'.txt', 'r+')
    old = fout.read()
    fout.seek(0)
    fout.write(str(nlines)+'\n')
    fout.write(old)
    fout.close()
